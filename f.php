<?php
 
define('WR',dirname(__FILE__)); 
/**
 * 对文件 路径进行编码
 *
 * @param string $path
 */
function encodePath($path)
{
    $tmp_array = explode('/', $path);
    foreach ($tmp_array as $key => $value)
    {
        if ($value == '')           //删除空内容
            unset($tmp_array[$key]);
            $tmp_array[$key]=rawurlencode($value);
    }
    return implode("/", $tmp_array);
}

/**
 * 显示验证的输入窗口
 * @param string $user 用户名
 * @param string $pass 密码
 * @access public
 */
function webAuthenticate($user,$pass)
{
    if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])  || !isset($user)  || !isset($pass)
        || $_SERVER['PHP_AUTH_USER']!=$user | $_SERVER['PHP_AUTH_PW']!=$pass
        )
    {
        header('WWW-Authenticate: Basic realm="Authentication System"');
        header('HTTP/1.0 401 Unauthorized');
        echo "You must enter a valid login ID and password to access this resource ";
        exit;
    }
    return true;
}

/************按时间顺序输出文件夹中的文件******************/
/************按时间顺序输出文件夹中的文件******************/
function dir_time($dir) {
    $dh = @opendir ( $dir ); // 打开目录，返回一个目录流
    $return = array ();
    $return1 = array();
    $i = 0;
    while ( $file = @readdir ( $dh ) ) { // 循环读取目录下的文件
        if ($file != '.' and $file != '..') {
            $path = $dir . '/' . $file; // 设置目录，用于含有子目录的情况
            if (is_dir ( $path )) {
                $return1 [] = $file;
            } elseif (is_file ( $path )) {
                $filetime [] = date ( "Y-m-d H:i:s", filemtime ( $path ) ); // 获取文件最近修改日期
                $return [] = $file;
            }
        }
    }
    @closedir ( $dh ); // 关闭目录流
    array_multisort($filetime,SORT_DESC,SORT_STRING, $return);//按时间排序
    $return = array_merge($return1,$return);
    return $return; // 返回文件
}
//2005-4-11
//显示当前目录下的文件
$_CONFIG["SiteName"]="文件浏览 ";        //网站名称
$_CONFIG["SiteUrl"]="";            //网站地址

?>
<html>
<head>
<title>
<?php 
print($_CONFIG["SiteName"])." ".$_CONFIG["SiteUrl"];
?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
<!--


BODY {
    WORD-BREAK: break-all; LINE-HEIGHT: 150%
}
TD {
    FONT-SIZE: 12px; FONT-FAMILY: 宋体
}
-->
</style>
</head>

<body bgcolor="#FFFFFF" text="#000000">
<center><font color=#ee0000>
<?php 
print($_CONFIG["SiteName"]); 
?></font>
<br><a href=<?php print($_CONFIG["SiteUrl"]);?>><?print($_CONFIG["SiteUrl"]); ?></a></center>
<table border=1  width=98% align="center"  bordercolordark="#FFFFFF"  cellpadding="2" cellspacing="2">
<tr>
<?php

$_DIR_PATH="./userdata/screenshot/";

if(!empty($_GET["dir"]) && strlen($_GET["dir"])>3 && ".."!=substr($_GET["dir"], 0, 2))
{
    $prevRealpath=dirname($_GET["dir"]);    //得到上一层的目录
    if(substr($_GET["dir"], -1) != '/')
    {    $_GET["dir"] .= '/';
    }
    $_DIR_PATH=$_GET["dir"];

//    print($_DIR_PATH);
//    die();

    print("<td>当前目录路径：[<b>".$_DIR_PATH."</b>]</td>");
    print("<td align=right>");
    print("　<a href='?dir='>");
    print("[返回根目录]");
    print("</a>");
    
    print("　<a href='?dir=".rawurlencode($prevRealpath)."'>");
    print("返回上一层目录");
    print("</a>　");
    print("</td>");


}
$numb=0;
if(empty($_DIR_PATH)){
    $myfiles =  dir_time("./");
}else{
    $myfiles =  dir_time($_DIR_PATH);
}
$_DIR_PATH1 = WR.substr($_DIR_PATH,1);

?>
</tr></table>

<table border=1  width=98% align="center"  bordercolordark="#FFFFFF"  cellpadding="2" cellspacing="2">

<?php
foreach ($myfiles as $k=>$tmp_Str){
    if($tmp_Str!="."&&$tmp_Str!="..")
    {
        $numb++;
        print("<tr>");
        
        if(is_dir($_DIR_PATH1.$tmp_Str))        //是目录
        {
            print("<td align=center>");
            print(strftime("%Y-%m-%d %H:%M:%S",filemtime($_DIR_PATH.$tmp_Str)));
            print(" </td>");
            print("<td>");
            print("<a href='?dir=".encodePath($_DIR_PATH.$tmp_Str)."'>");
            print("[<font color=red>目录</font>] ");
            print("</a>");
            print(" </td>");
            
            print("<td>");
            print("<a href='?dir=".encodePath($_DIR_PATH.$tmp_Str)."'>");
            print($tmp_Str);
            print("</a>");
            print(" </td>");
        }
        else    //其他显示的文件
        {
            if(strstr($tmp_Str,".php") || strstr($tmp_Str,".asp")  )    //不显示 .php .asp的文件
                continue;
                print("<td align=center>");
                print(strftime("%Y-%m-%d %H:%M:%S",filemtime($_DIR_PATH.$tmp_Str)));
                print(" </td>");
                
                print("<td>");
                print(filesize($_DIR_PATH.$tmp_Str)."");
                $kbSize=round(filesize($_DIR_PATH.$tmp_Str)/1000,2);
                $mbSize=round($kbSize/1000,2);
                if($mbSize>1)
                    print("[".$mbSize."MB]");
                    else
                        print("[".$kbSize."KB]");
                        print(" </td>");
                        
                        print("<td>");
                        print("<a target=_blank href='".encodePath($_DIR_PATH.$tmp_Str)."'>");
                        print($tmp_Str);    //$_DIR_PATH.
                        print("</a>");
                        print(" </td>");
        }
        
        print("</tr>");
        //if($numb%5==0)
        //    print("</tr><tr>");
    }
}


?>
</table>
</body>
</html>