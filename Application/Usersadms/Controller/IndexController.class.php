<?php
class IndexController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}
	
	public function Index(){
        $id=session("AdminThreeUser")['id'];
        $msglist=M('admin_three_user')->where(array('id'=>$id))->find();

        //今天用户新增魅力值
        $TodayuserMoney = $this->addusermoney(0)['Count'];
        // //今天提成新增魅力值
        $TodayadminMoney = $this->addadminmoney(0)['Count'];

        $this->todayusermoney=$TodayuserMoney;
        $this->todayuserrelmoney=intval($TodayuserMoney*0.01*6.5);
        $this->todayadminmoney=$TodayadminMoney;
        $this->todayadminrelmoney=intval($TodayadminMoney*0.01*6.5);
        $this->msglist=$msglist;
		$this->name = '首页'; // 进行模板变量赋值
		$this->display();
	}

	public function  addusermoney($days="0"){
        $id=session("AdminThreeUser")['id'];
        if($days=="0"){
            $dateStart = strtotime(date("Y-m-d"));
            $dateEnd = strtotime(date("Y-m-d",strtotime("+1 day")));
        }else{
            $enddays=$days+1;
            $dateStart = strtotime(date("Y-m-d",strtotime($days." day")));
            $dateEnd = strtotime(date("Y-m-d",strtotime($enddays." day")));
        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        $where["pid"]=$id;
        $Wdata['type']=array('in',array(1,2,3,11));
        $totalmoney=M('p_money')->where($where)->sum('money');
        $res["days"] = $dateStart;
        $res["Count"] =$totalmoney;
        return $res;
	}
    public function  addadminmoney($days="0"){
        $id=session("AdminThreeUser")['id'];
        if($days=="0"){
            $dateStart = strtotime(date("Y-m-d"));
            $dateEnd = strtotime(date("Y-m-d",strtotime("+1 day")));
        }else{
            $enddays=$days+1;
            $dateStart = strtotime(date("Y-m-d",strtotime($days." day")));
            $dateEnd = strtotime(date("Y-m-d",strtotime($enddays." day")));
        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        $where['type']=array('in',array(8,9));
        $where['pid']=$id;
        $totalmoney=M('p_money')->where($where)->sum('money');
        $res["days"] = $dateStart;
        $res["Count"] =$totalmoney;
        return $res;
    }

    //统计一周用户魅力值
    public function TongjiYiZhouReg(){

        $days = array(-8,-7,-6,-5,-4,-2,-1,0);
        $res = array();
        $res["tooltip"]["trigger"] = "axis";
        $res["yAxis"]["type"] = "value";
        $res["yAxis"]["axisLabel"]["formatter"] = "{value}";


            $res["series"]["type"]="line";
            $res["series"]["name"]='用户魅力值';

            foreach($days as $v){
                $tongji1 = $this->addusermoney($v);
                $temp["x"] = date("m/d", $tongji1["days"]);
                $temp["y"] = intval($tongji1["Count"]);
                $res["xAxis"]["data"][] = $temp["x"];
                $res["series"]["data"][] = $temp["y"];
            }
        echo json_encode($res);

    }
    public function TongjiYiZhouCzlv(){

        $days = array(-8,-7,-6,-5,-4,-2,-1,0);
        $res = array();
        $res["tooltip"]["trigger"] = "axis";
        $res["yAxis"]["type"] = "value";
        $res["yAxis"]["axisLabel"]["formatter"] = "{value}";
            $res["series"]["type"] = "line";
            $res["series"]["name"] ='提成魅力值';

            foreach ($days as $v) {
                $tongji1 = $this->addadminmoney($v);

                $temp["x"] = date("m/d", $tongji1["days"]);
                $temp["y"] = $tongji1["Count"];
                $res["xAxis"]["data"][] = $temp["x"];
                $res["series"]["data"][] = $temp["y"];
            }

        echo json_encode($res);
    }

    //收入统计按月
    public function ShowruTongjiYue(){
        $days=array();
        for ($x=-11; $x<=0; $x++) {
            $days[] = $x;
        }
        $res = array();
        foreach($days as $v){
            $tongji1=$this->AllChongjineYiyue($v);
            $temp["x"]=date("m月",$tongji1["days"]);
            $temp["y"]=intval($tongji1["Count"]);
            $res["xAxis"][]=$temp["x"];
            $res["series"][]=$temp["y"];
        }
        //Dump($res);
        exit(json_encode ($res));

    }

    //统计一月充值金额
    public function AllChongjineYiyue($yue="0"){
        header("Content-type: text/html; charset=utf-8");
        $id=session("AdminThreeUser")['id'];
        if($yue=="0"){
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y"))));
        }else{
            $yue=-$yue;
            $enddays=$yue-1;
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m")-$yue,1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m")-$enddays ,0,date("Y"))));

        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        $where['type']=array('in',array(8,9));
        $where['pid']=$id;

        if($yue=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }

        $totalmoney=M('p_money')->where($where)->sum('money');
            //echo $sum_money."<br />";
            $res["days"] = $dateStart;
            $res["Count"] = round($totalmoney*6.5*0.01);

        return $res;
    }






}

?>
