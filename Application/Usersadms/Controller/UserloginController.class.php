<?php
use Think\Controller;
class userloginController extends Controller {
	public function __construct(){
		parent::__construct();
		session_start();
	}
		
	public function loginout(){
		session('AdminThreeUser',null);
		header("Location:/Usersadms/Userlogin/login.html");
	}
	public function Login(){
		$this->name = '登录'; // 进行模板变量赋值
			if(session("AdminThreeUser")){
			header("Location:/Usersadms/Index/index.html");
		}
		$this->action="Logind.html";
		$this->display();
	}
	public function Logind(){
		$where = array();
		$where['name'] = $_POST['username'];
		$where['password'] = $_POST['password'];
		

		$AdminThreeUser = M('admin_three_user')->where($where)->find();
		if(empty($AdminThreeUser)){
			$this->redirect("/Usersadms/userlogin/login");
		}else{
			$InData = array();
			$InData['admin_id'] = $AdminThreeUser['id'];
			$InData['admin_name'] = $AdminThreeUser['name'];
			$InData['ip'] = get_client_ip();
			$InData['login_time'] = time();
			//$AdminLoginIpM = new AdminLoginIpModel();
			//$AdminLoginIpM->addOne($InData);
			session('AdminThreeUser',$AdminThreeUser,time()+3600*24,'/');
			//session("AdminUser",$AdminUser);
            $_SESSION['user']=array(
                'id'=>$AdminThreeUser['id'],
                'username'=>$AdminThreeUser['name'],
                'expire'=>time()+3600*24
            );
			$this->redirect("/Usersadms/Index/index");
		}
		
	
	}
}

?>
