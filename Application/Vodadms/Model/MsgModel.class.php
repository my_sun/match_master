<?php
use Think\Model;
class MsgModel extends Model{
	public function __construct(){
		parent::__construct();
		//$this->copy_table('t_msg',20);
		//$this->alter_table('t_msg_box',20);
		
	}

 
   
   /**
    * 添加一条数据
    * @param array $data
    * @return bool|int
    */
    public function addOne($data){
        if($this->create($data)){
            $id = $this->add();
            if($id === false){
                $this->error = '插入数据错误';
                return false;
            }else{
                return $id;
            }
        }
        return false;
    }

    /**
     * 条件查询列表
     * @param $map
     * @return mixed
     */
    public function getList($map){
        return $this->where($map)->order('sendtime')->select();
    }
}
?>