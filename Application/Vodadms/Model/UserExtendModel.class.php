<?php
use Think\Model\AdvModel;
use Think\Model;
class UserExtendModel extends AdvModel {
	Protected $autoCheckFields = false;  //一定要关闭字段缓存，不然会报找不到表的错误
	protected $partition = array(
			'field' => 'uid',// 要分表的字段 通常数据会根据某个字段的值按照规则进行分表,我们这里按照用户的id进行分表
			'type' => 'mod',// 分表的规则 包括id year mod md5 函数 和首字母，此处选择mod（求余）的方式
			'expr' => '',// 分表辅助表达式 可选 配合不同的分表规则，这个参数没有深入研究
			'num' => '20',// 分表的数目 可选 实际分表的数量，在建表阶段就要确定好数量，后期不能增减表的数量
	);
	
	public function __construct(){
		parent::__construct();
		//$this->copy_table('t_user_base',20);
		//$this->alter_table('t_user_base',20);
	}
     /**
     * 计算在哪张表
     * @param array $data
     * @return \Think\Model
     */
   public function computeTable($data){
      $data = empty($data) ? $_POST : $data;
      $table = $this->getPartitionTableName($data);
      return $this->table($table);
   }
   
   /**
    * 添加一条数据
    * @param array $data
    * @return bool|int
    */
   public function addOne($data){

   	
   	//$data['uid'] = intval($this->max('uid')) + 1;
   	//$data['uid'] = $data['uid']>100000 ? $data['uid'] : 100001;
   	if($this->create($data)){
   		$id = $this->add();
   		if($id === false){
   			$this->error = '插入数据错误';
   			return false;
   		}else{
   			return $id;
   		}
   	}
   	return false;
   }
   /**
    * 获取所有记录
    */
   public function getAll(){
   	return $this->select();
   }
   /**
    * 条件查询列表
    * @param $map
    * @return mixed
    */
   public function getList($map){
   	return $this->where($map)->select();
   }
   /**
    * 条件查询列表分页
    * @param $map
    * @return mixed
    */
   public function getListPage($where,$pageNum,$pageSize,$map=array()){

   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   //	Dump($Page);exit;
   	$data = $this->where($where)->order("RAND()")->limit($Page->firstRow. ',' . $Page->listRows)->select();
	$retArray = array();
	$retArray['totalCount'] = $count;
	$retArray['pageSize'] = $pageSize;
	$retArray['pageNum'] = $pageNum;
	$retArray['listYuanfen'] = $data;
	return $retArray;
	
   }
      /**
    * 条件查询列表分页
    * @param $map
    * @return mixed
    */
   public function getListfield($where,$pageNum,$pageSize,$field="uid"){
   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   	$data = $this->field($field)->where($where)->order("RAND()")->limit($Page->firstRow. ',' . $Page->listRows)->select();

	return $data;
	
   }
   /**
    * 搜索用户
    * @param $map
    * @return mixed
    */
   public function getlistSearch($where,$pageNum,$pageSize,$map=array()){
   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   	
   	//	Dump($Page);exit;
   	$data = $this->where($where)->order("RAND()")->limit($Page->firstRow. ',' . $Page->listRows)->select();
   	$retArray = array();
   	$retArray['totalCount'] = $count;
   	$retArray['pageSize'] = $pageSize;
   	$retArray['pageNum'] = $pageNum;
   	$retArray['listSearch'] = $data;
   	return $retArray;
   
   }
      /**
    * 搜索用户
    * @param $map
    * @return mixed
    */
   public function peiliaogetlistSearch($where,$pageNum,$pageSize,$order='account asc'){
   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   	
   	//	Dump($Page);exit;
   	$data = $this->where($where)->order($order)->limit($Page->firstRow. ',' . $Page->listRows)->select();
   	$retArray = array();
   	$retArray['totalCount'] = $count;
   	$retArray['pageSize'] = $pageSize;
   	$retArray['pageNum'] = $pageNum;
   	$retArray['listSearch'] = $data;
   	return $retArray;
   
   }
 public function getlistOrder($where,$pageNum,$pageSize,$order="regtime desc",$map=array()){
   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   	
   	//	Dump($Page);exit;
   	$data = $this->where($where)->order($order)->limit($Page->firstRow. ',' . $Page->listRows)->select();
   	$retArray = array();
   	$retArray['totalCount'] = $count;
   	$retArray['pageSize'] = $pageSize;
   	$retArray['pageNum'] = $pageNum;
   	$retArray['listSearch'] = $data;
   	return $retArray;
   
   }
      public function GetCount($where){
		  
		 return $this->where($where)->count();    //计算总数
	  }
   /**
    * 附近的人
    * @param $map
    * @return mixed
    */
   public function GetNearbyUser($where,$pageNum,$pageSize,$map=array()){
   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   	//	Dump($Page);exit;
   	$data = $this->where($where)->order("RAND()")->limit($Page->firstRow. ',' . $Page->listRows)->select();
   	$retArray = array();
   	$retArray['totalCount'] = $count;
   	$retArray['pageSize'] = $pageSize;
   	$retArray['pageNum'] = $pageNum;
   	$retArray['listUser'] = $data;
   	return $retArray;
   	 
   }
      /**
    * 查询在线列表
    * @param $map
    * @return mixed
    */
   public function getOnline($where,$pageNum,$pageSize,$order="regtime desc"){

   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   	$data = $this->where($where)->order($order)->limit($Page->firstRow. ',' . $Page->listRows)->select();
	$retArray = array();
	$retArray['totalCount'] = $count;
	$retArray['pageSize'] = $pageSize;
	$retArray['pageNum'] = $pageNum;
	$retArray['list'] = $data;
	return $retArray;
	
   }
   public function getOnlineForUser($where,$pageNum,$pageSize,$map=array()){

   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   //	Dump($Page);exit;
   	$data = $this->where($where)->order("regtime asc")->limit($Page->firstRow. ',' . $Page->listRows)->select();
	$retArray = array();
	$retArray['totalCount'] = $count;
	$retArray['pageSize'] = $pageSize;
	$retArray['pageNum'] = $pageNum;
	$retArray['list'] = $data;
	return $retArray;
	
   }
   /**
    * 根据查询条件获取一条记录
    * @param $map
    * @return mixed
    */
   public function getOne($map){
   	return $this->where($map)->find();
   }
   /**
    * 根据查询条件获取一条随机数据
    * @param $map
    * @return mixed
    */
   public function getOneRand($map){
   	return $this->where($map)->order("RAND()")->find();
   }
   /**
    * 更新一条记录
    * @param $map
    * @param $data
    * @return bool
    */
   public function updateOne($map, $data){
   	if($this->create($data)){
   		$res = $this->where($map)->save();
   		if($res === false){
   			$this->error = '更新数据出错';
   		}else{
   			return $res;   //更新的数据条数
   		}
   	}
   	return false;
   }
   /**
    * 删除一条记录
    * @param $map
    * @return bool|mixed
    */
   public function delOne($map){
   	$res = $this->where($map)->delete();
   	if($res === false){
   		$this->error = '删除数据出错';
   		return false;
   	}else{
   		return $res;   //删除数据个数
   	}
   }
   
   /*
    *
   * 复制表
   */
   public function copy_table($table_name,$number){
   	for ($x=1; $x<=$number; $x++) {
   		$sql="create  table  ".$table_name."_".$x." like ".$table_name.";";
   		M()->execute($sql);
   	}
   	return true;
   }
   /*
    *
   * 给数据表添加字段
   */
   public function alter_table($table_name,$number){
   	for ($x=1; $x<=$number; $x++) {
   		//$sql = "ALTER TABLE ".$table_name."_".$x." ADD INDEX ( `gender` );";
		//$sql = "ALTER TABLE ".$table_name."_".$x." ADD `photo_number` int(4) default 0; ";
		//M()->execute($sql);
		//$sql = "ALTER TABLE ".$table_name."_".$x." ADD regtimeint int(11) DEFAULT 0;";
		//$sql = "alter table ".$table_name."_".$x." change product product int(11) DEFAULT 10001;";
		//M()->execute($sql);

   	}
   	return true;
   }
    /**
     * 查询一条记录
     * @param $map
     * @return bool|mixed
     */
    public function selOne($map){
        $res = $this->where($map)->find();
        if($res === false){
            $this->error = '查询数据出错';
            return false;
        }else{
            return $res;   //查询数据个数
        }
    }
    //插入一条数据
    public function insOne($map){
        $res = $this->data($map)->add();
        if($res === false){
            $this->error = '插入数据出错';
            return false;
        }else{
            return $res;   //插入数据个数
        }
    }
}
?>