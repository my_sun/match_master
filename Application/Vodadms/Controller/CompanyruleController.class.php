<?php
class CompanyruleController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
        $AdminThreeUser = session("AdminThreeUser");
        if($AdminThreeUser['type']!=0){
            echo "您没有权限";
            $this->redirect("/Vodadms/Index/index");die;
        }
	}
	/**
	 * 运营公司列表
	 */
	public function companylist(){
        //获取运营公司列表
        $where['type'] = 1;
        $pageNum = empty($_GET['page'])?1 :$_GET['page'];
        $pageSize = 20;
        $vodAdmin = new VodAdminModel();
        $list = $vodAdmin->getListPage($where,$pageNum,$pageSize);
        $this->DataList = $list;
        $this->Pages = $this->GetPages($list['list']);
        $this->name = '运营公司列表';
        $this->action = "/Vodadms/Companyrule/companylist.html";
        $this->display();


	}

	public function companyadd(){
        $this->name = '添加运营公司';

        if(!empty($_POST['account']) && !empty($_POST['password'])){
            $name = $_POST['name'];
            $account = $_POST['account'];
            $password = $_POST['password'];
            $password_two = $_POST['password_two'];
            $gold_pump = $_POST['gold_pump'];
            if($password !== $password_two){
                $error=1;
                $msg="密码不一致！";
                $this->error=$error;
                $this->msg=$msg;
                $this->display();

            }else{
                $AdminThreeUser = session("AdminThreeUser");
                $admin_vod = new VodAdminModel();
                $account_have = $admin_vod->getOne(array("name"=>$account));
                if($account_have){
                    $error=1;
                    $msg="账号已存在，请重新设置账号！";
                    $this->error=$error;
                    $this->msg=$msg;
                    $this->display();
                }else{
                    $admin_info = array();
                    $admin_info['company_name'] = $name;
                    $admin_info['name'] = $account;
                    $admin_info['password'] = $password;
                    $admin_info['type'] = 1;
                    $admin_info['gold_pump'] = $gold_pump;
                    $admin_info['time'] = time();
                    $admin_info['company_id'] = $AdminThreeUser['company_id'] ? $AdminThreeUser['company_id'] : 1;
                    $resid = $admin_vod->addOne($admin_info);
                    if(!empty($resid)){
                        $error=1;
                        $msg="添加运营公司成功！";
                    }else{
                        $error=1;
                        $msg="添加失败，请检查网络刷新后重新添加！";
                    }
                }
            }


        }

        $this->error=$error;
        $this->msg=$msg;
        $this->action = "/Vodadms/Companyrule/companyadd.html";
        $this->display();
    }


}

?>
