<?php
class SysController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();

	}
	/**
	 * 修改密码
	 */
	public function edit_pass(){
        $this->name = '修改密码';
		if($_POST['new_password']&&$_POST['old_password']){
			$id=cookie("AdminThreeUser")['id'];
			$password=M('admin_three_user')->where(array('id'=>$id))->getField('password');
			if($password==$_POST['old_password']){
                $password=M('admin_three_user')->where(array('id'=>$id))->save(array('password'=>$_POST['new_password']));
			if($password){
                $error=1;
                $msg="修改成功！";
			}
			}else{
                $error=1;
                $msg="原密码输入错误！";
			}
		}

        $this->error=$error;
        $this->msg=$msg;
        $this->action = "/Usersadms/Sys/edit_pass.html";
        $this->display();


	}

    /**
     * 提现管理
     */
    public function withdrawal(){
        $this->name = '提现申请';
        $id=session("AdminThreeUser")['id'];
        $totalbilimoney=0;
        //$alltotalmoney为分享所得的所有魅力值
        $alltotalbilimoney=M('p_money')->where(array('uid'=>$id,'type'=>9,'is_tixian'=>4))->select();
        foreach($alltotalbilimoney as $k=>$v){
            $totalbilimoney+=$v['money'];
        }

        $totalsharemoney=0;
        //$alltotalmoney为分享所得的所有魅力值
        $alltotalmoney=M('p_money')->where(array('uid'=>$id,'type'=>8,'is_tixian'=>4))->select();
        foreach($alltotalmoney as $k=>$v){
            $totalsharemoney+=$v['money'];
        }

        $leftmoney=$totalbilimoney+$totalsharemoney;
		if($leftmoney==0){
            $error=1;
            $msg="可提取魅力值为零！";
		}else{
            if($_POST['username']&&$_POST['pay']){
                $uid =$id;
                $percent=$_POST['percent'];
                $ret=array();
                $ret['uid']=$uid;
                $ret['type']=7;
                $ret['is_tixian']=1;
                $ret['account_type'] = $_POST["paytype"]?$_POST["paytype"]:1;//账户类型
                $ret['account']=$_POST['pay'];
                //day中存本次提现用户的比例
                $ret['days']=$percent;
                $ret['money']= $leftmoney;//提取魅力值
                $ret['paytime']= time();
                $beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));
                //查看截止到当前时间订单列表中是否有提现记录
                $mmo=array();
                $mmo['paytime']=array('between',array($beginThismonth,$ret['paytime']));
                $mmo['uid']=$uid;
                $mmo['_string'] ='type=7';
                $mmo['is_tixian']=array('neq',3);
                $mmo['account_type']=array('neq',4);
                $mmoneylist=M('p_money')->where($mmo)->find();
               /* if($mmoneylist){
                    $error=1;
                    $msg="本月已经提现一次！";
                }else{*/
                    $psusse=M('p_money')->add($ret);
                    if($psusse){
                        $ret2=array();
                        $ret3=array();
                        foreach($alltotalbilimoney as $k2=>$v2){
                            $ret2[$k2]['is_tixian']=1;
                            $ret2[$k2]['paytime']=$ret['paytime'];
                            M('p_money')->where(array('id'=>$v2['id']))->save($ret2[$k2]);
                        }
                        foreach($alltotalmoney as $k1=>$v1){
                            $ret3[$k1]['is_tixian']=1;
                            $ret3[$k1]['paytime']=$ret['paytime'];
                            M('p_money')->where(array('id'=>$v1['id']))->save($ret3[$k1]);
                        }
                        //将用户名和账号存入数据库中
                        $mmoneydata=array();
                        $mmoneydata['commonpay']=$_POST['username'];
                        if($_POST["paytype"]==1){
                            $mmoneydata['weixin']=$_POST['pay'];
                        }elseif($_POST["paytype"]==2){
                            $mmoneydata['zhifubao']=$_POST['pay'];
                        }elseif($_POST["paytype"]==3){
                            $mmoneydata['paypal']=$_POST['pay'];
                        }
                        M('m_money')->where(array('uid'=>$id))->save($mmoneydata);
                        $error=1;
                        $msg="申请成功，请耐心等待！";
                    }

				//}
            }else{
                $error=1;
                $msg="用户名与账号不能为空！";
            }
		}
		$this->totalsharemoney=$totalsharemoney;
        $this->totalsharerealmoney=$totalsharemoney*0.01*6.5;
        $this->totalbilimoney=$totalbilimoney;
        $this->totalbilirealmoney=$totalbilimoney*0.01*6.5;
        $this->leftmoney=$leftmoney;
        $this->leftrealmoney=$leftmoney*0.01*6.5;;
        $this->error=$error;
        $this->msg=$msg;
        $this->action = "/Usersadms/Sys/withdrawal.html";
        $this->display();
	}
    /**
     * 所属用户提现管理
     */
	public function userwithdrawal(){
        $this->name = '提现申请';
        $id=session("AdminThreeUser")['id'];
        //用户可提总魅力值
        $usermoney=0;
        $usermoneylist=M('p_money')->where(array('pid'=>$id,'type'=>10,'is_tixian'=>4))->select();
        foreach($usermoneylist as $k=>$v){
            $usermoney+=$v['money'];
        }
        if($usermoney==0){
            $error=1;
            $msg="可提取魅力值为零";
        }else{
            if($_POST['username']&&$_POST['pay']){
                $uid =$id;
                $ret=array();
                $ret['uid']=$uid;
                $ret['type']=6;
                $ret['is_tixian']=1;
                $ret['account_type'] = $_POST["paytype"]?$_POST["paytype"]:1;//账户类型
                $ret['account']=$_POST['pay'];
                $ret['money']= $usermoney;//提取魅力值
                $ret['paytime']= time();
                $beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));
                //查看截止到当前时间订单列表中是否有提现记录
                $mmo=array();
                $mmo['paytime']=array('between',array($beginThismonth,$ret['paytime']));
                $mmo['uid']=$uid;
                $mmo['_string'] ='type=6';
                $mmo['is_tixian']=array('neq',3);
                $mmo['account_type']=array('neq',4);
                $mmoneylist=M('p_money')->where($mmo)->find();
                /* if($mmoneylist){
                     $error=1;
                     $msg="本月已经提现一次！";
                 }else{*/
                $psusse=M('p_money')->add($ret);
                if($psusse){
                    $ret2=array();
                    foreach($usermoneylist as $k2=>$v2){
                        $ret2[$k2]['paytime']=$ret['paytime'];
                        $ret2[$k2]['is_tixian']=1;
                        M('p_money')->where(array('id'=>$v2['id']))->save($ret2[$k2]);
                    }
                    //将用户名和账号存入数据库中
                    $mmoneydata=array();
                    $mmoneydata['commonpay']=$_POST['username'];
                    if($_POST["paytype"]==1){
                        $mmoneydata['weixin']=$_POST['pay'];
                    }elseif($_POST["paytype"]==2){
                        $mmoneydata['zhifubao']=$_POST['pay'];
                    }elseif($_POST["paytype"]==3){
                        $mmoneydata['paypal']=$_POST['pay'];
                    }
                    M('m_money')->where(array('uid'=>$id))->save($mmoneydata);
                    $error=1;
                    $msg="申请成功，请耐心等待！";
                }
                //}
            }else{
                $error=1;
                $msg="用户名与账号不能为空！";
            }
        }
        $this->totalmoney=$usermoney;
        $this->reamoney=$usermoney*0.01*6.5;
        $this->error=$error;
        $this->msg=$msg;

        $this->action = "/Usersadms/Sys/userwithdrawal.html";
        $this->display();
    }
	/**
     * 提现申请记录
     */
    public function withdrawallist(){
        $this->name = '提现申请记录';
        $id=session("AdminThreeUser")['id'];
        $Wdata['type']=array('in',array(6,7));
        $Wdata['uid']=$id;
        $DataList=M('p_money')->where($Wdata)->select();
        foreach($DataList as $k=>$v){
            $DataList[$k]['money1']=$v['money']* 0.01 * 6.5;
        }
        $this->DataList=$DataList;
        $this->DataList=$DataList;
        $this->display();
    }
    /**
     * 所属用户提现申请详细记录
     */
    public function xiangxijilu(){
        $this->name = '提现所属用户提现记录';
        $paytime=$_GET['paytime'];
        $id=session("AdminThreeUser")['id'];
        $wdata['pid']=$id;
        $type=$_GET['type'];
        if($type==6){
            $wdata['type']=10;
        }elseif($type==7){
            $wdata['type']=array('in',array(8,9));
        }
        $wdata['paytime']=$paytime;
        $DataList=M('p_money')->where($wdata)->select();
        foreach($DataList as $k=>$v){
            $DataList[$k]['realmoney']=$v['money']*0.01*6.5;
        }
        $this->DataList=$DataList;
        if($type==7){
            $this->name = '提成提现详细记录';
            $this->display('xiangxijilu1');
        }else{
            $this->display();
        }
    }

    //增加修改比例值
    public function edit_bili(){
        $id=session("AdminThreeUser")['id'];
        if($_POST["bili"]) {
            if(!is_numeric($_POST["bili"])){
                $error=1;
                $msg="输入不合法！";
            }else{
                if($_POST["bili"]>15){
                    $_POST["bili"]=15;
                }
                $basedata['bili'] = $_POST['bili'];
                $data=M('admin_three_user')->where(array('id'=>$id))->save($basedata);

                    redirect('/Usersadms/index/index.html');

            }
            }



    }

    //提成记录
    public function sharelists(){
        $this->name = '提成记录'; // 进行模板变量赋值
        $id=session("AdminThreeUser")['id'];
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 200;
        $withd = new PMoneyModel();
        $Wdata = array();
        $Wdata['type']=array('in',array(8,9));
        $Wdata['uid']=$id;
        if(isset($_GET['is_tixian'])&&$_GET['is_tixian']!="")
        {$Wdata['is_tixian']=$_GET['is_tixian']?$_GET['is_tixian']:'';
        };
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|account|to_uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $withd->getListPage($Wdata, $Page, $PageSize);
        $totalmoney=0;
        $totalrealmoney=0;
        foreach ($ret["list"] as $k => $v) {
            $ret["list"][$k]['realmoney'] = $v['money'] * 0.01* 6.5;
            $totalmoney+=$v['money'];
            $totalrealmoney+= $v['money'] * 0.01* 6.5;
        }
        $this->ListData = $ret["list"];
        $this->status = $Wdata['is_tixian'];
        $this->totalmoney = $totalmoney;
        $this->totalrealmoney =$totalrealmoney;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display();
    }





}

?>
