<?php
class PrivatesetController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
        $AdminThreeUser = session("AdminThreeUser");
        if($AdminThreeUser['type']!=0){
            echo "您没有权限";
            $this->redirect("/Vodadms/Index/index");die;
        }
	}
	/**
	 * 配置基础信息
	 */
	public function index(){
        //获取公共配置表数据
        $commSet = new VodPrivateSetModel();

        if(!empty($_POST['id'])){

            $id = $_POST['id'];
            unset($_POST['id']);
            $_POST['endtime'] = strtotime($_POST['endtime']);

            $info = $_POST;
//            if($_POST['endtime'] < time()){
//                $error=1;
//                $msg="保存失败，活动结束时间小于当前时间！";
//            }else{
                $resid = $commSet->updateOne(array("id"=>$id),$info);
                if(!empty($resid)){
                    $error=1;
                    $msg="保存配置成功！";
                }else{
                    $error=1;
                    $msg="您并未进行数据更改！";
                }
           // }

        }
        $PrivateSet = $commSet->getOne("*");
        $this->vodcommset = $PrivateSet;
        $this->error=$error;
        $this->msg=$msg;
        $this->name = '公共参数配置';
        $this->action = "/Vodadms/Privateset/index";
        $this->display();


	}

	public function sysitemscore(){
        //获取公共配置表数据
        $commSet = new VodPrivateSetModel();

        if(!empty($_POST['id'])){

            $id = $_POST['id'];
            unset($_POST['id']);
            $info['systemscore'] = json_encode($_POST);
            $resid = $commSet->updateOne(array("id"=>$id),$info);
            if(!empty($resid)){
                $error=1;
                $msg="保存配置成功！";
            }else{
                $error=1;
                $msg="您并未进行数据更改！";
            }

        }
        $PrivateSet = $commSet->getOne("*");
        $PrivateSet['systemscore'] = json_decode($PrivateSet['systemscore'],true);
        $this->vodcommset = $PrivateSet;
        $this->error=$error;
        $this->msg=$msg;
        $this->name = '男性星级评价返回系统内置配置';
        $this->action = "/Vodadms/Privateset/sysitemscore";
        $this->display();
    }

    public function sysitemscoregirl(){
        //获取公共配置表数据
        $commSet = new VodPrivateSetModel();

        if(!empty($_POST['id'])){

            $id = $_POST['id'];
            unset($_POST['id']);
            $info['sysitemscoregirl'] = json_encode($_POST);
            $resid = $commSet->updateOne(array("id"=>$id),$info);
            if(!empty($resid)){
                $error=1;
                $msg="保存配置成功！";
            }else{
                $error=1;
                $msg="您并未进行数据更改！";
            }

        }
        $PrivateSet = $commSet->getOne("*");
        $PrivateSet['systemscore'] = json_decode($PrivateSet['sysitemscoregirl'],true);
        $this->vodcommset = $PrivateSet;
        $this->error=$error;
        $this->msg=$msg;
        $this->name = '女性星级评价返回系统内置配置';
        $this->action = "/Vodadms/Privateset/sysitemscoregirl";
        $this->display();
    }




    public function showuserinfo(){
        //获取当前时间天数
        $CompereNocall = new CompereNocallModel();
        $CompereTime = new CompereTimeModel();
        $date = date("Ymd");
        $map = array("ontime"=>$date);
        $pageNum  = $_GET["Page"] ? $_GET["Page"] : 1;
        $pageSize =100;
        if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
            $kerword = $_GET['kerword'];
            $map['uid']=array('like',"%".$kerword."%");
        }

        if(isset($_GET['dateStart'])&&$_GET['dateStart']!=""){
            $ontime = str_replace("-",'',$_GET['dateStart']);
            $map['ontime']  = $ontime;
            $date = $ontime;
        }
        $PrivateSet = $CompereNocall->getListPage($map,$pageNum,$pageSize);
        //循环获取在线时间
        if($PrivateSet['totalCount']> 0){
            foreach($PrivateSet['list'] as &$list){
                $utime = $CompereTime->getList(array("uid"=>$list['uid'],"ontime"=>$date));
                //计算时间
                $miao= 0;
                if(!empty($utime)){

                    foreach($utime as $value){
                        if($value['endtime'] != 0 && $value['endtime'] > $value['startime']){
                            $miao += $value['endtime'] - $value['startime'];
                        }
                    }
                }

                $list['time_f'] = ceil($miao/60);//分钟
            }
        }
        $this->DataList = $PrivateSet;
        $this->Pages = $this->GetPages($PrivateSet);
        $this->get = $_GET;
        $this->action = "/Vodadms/privateset/showuserinfo.html";
        $this->display();


    }

    public function showusertime(){

        $CompereTime = new CompereTimeModel();
        $date = date("Ymd");
        $map = array("ontime"=>$date);
        $uid = $_GET['uid'];
        if(isset($_GET['dateStart'])&&$_GET['dateStart']!=""){
            $ontime = str_replace("-",'',$_GET['dateStart']);
            $map['ontime']=$ontime;
            $date = $ontime;
        }

        $utime = $CompereTime->getList(array("uid"=>$uid,"ontime"=>$date));
        foreach($utime as &$value){
            $value['ontime'] = '';
            if($value['endtime'] != 0 && $value['endtime'] > $value['startime']){
                $time = $value['endtime'] - $value['startime'];
                $value['ontime'] = ceil($time/60)."分钟";
            }
            $value['startime'] = date("Y-m-d H:i:s",$value['startime']);
            $value['endtime'] = $value['endtime'] == 0 ?"在线中":date("Y-m-d H:i:s",$value['endtime']);
        }

        $this->DataList = $utime;
        $this->action = "/Vodadms/privateset/showusertime.html";
        $this->get = $_GET;
        $this->display();


    }



    //意向用户列表

    public function yixiang(){

        if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
            $kerword = $_GET['kerword'];
            $where['uid']=array('like',"%".$kerword."%");
        }
        $where['product'] = 25100;
       $yixiang = M("user_yixiang")->where($where)->select();
       $this->DataList = $yixiang;
       $this->display();


    }



}

?>
