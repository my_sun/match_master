<?php
class UserController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->Lan = $this->LangSet("zh-cn");
		//$this->sendsysmsg("383712", "系统通知");
	}
	/**
	 * 获取自定义数组内容
	 */
	protected function get_attrconf($showtype=1){
	    $path = WR.'/userdata/cache/attr/';
	    $cache_name = 'attradms'.$showtype;
	    //F($cache_name,NULL,$path);
	    if(F($cache_name,'',$path)){
	        
	        $res = F($cache_name,'',$path);
	    }else{
	        
	        $Attr = new AttrModel();
	        $AttrValue = new AttrValueModel();
	        $AttrList = $Attr->getList(array("showtype"=>$showtype));
	        $res = array();
	        foreach ($AttrList as $k=>$v){
	            $option = array();
	            $AttrValueList = $AttrValue->getList(array("uppercode"=>$v["code"]));
	            foreach ($AttrValueList as $attrv){
	                $option[$attrv["id"]] = $attrv["code"];
	            }
	            $res[] = array(
	                "field"=>$v["code"],
	                "name"=>$v["code"],
	                "type"=>$v["type"],
	                "option"=>$option
	            );
	        }
	        F($cache_name,$res,$path);
	    }
	    
	    foreach ($res as $k=>$v){
	        $res[$k]["name"] = $this->L($res[$k]["name"]);
	        foreach($v["option"] as $k1=>$v1){
	            $res[$k]["option"][$k1] = $this->L($res[$k]["option"][$k1]);
	        }
	    }
	    return $res;
	}
	public function L($name=null, $value=null) {
	    return $this->Lan->L($name,$value);
	}
	public function LangSet($langs){
	   
	    $file   =  C("LANG_PATH").$langs.'.txt';
	    
	    $cachefile   =  C("LANG_PATH")."cache/".$langs.'.php';
	    
	    if(!file_exists($cachefile)||(filemtime($file)>filemtime($cachefile))){
	        $TranslateM = new TranslateModel();
	        $Wdata = array();
	        $Wdata["lang"]=$langs;
	        $ret = $TranslateM->getList($Wdata);
	        $temp = array();
	        foreach ($ret as $k=>$v){
	            $temp[$v["code"]]=$v['content'];
	        }
	        
	        $temp = "<?php return ".var_export($temp, true).";";
	        mkdirs(dirname($cachefile));
	        file_put_contents($cachefile, $temp);
	    }
	    
	    import("Api.lib.Behavior.CheckLangBehavior");
	    $lang = new CheckLangBehavior();
	    
	    $lang->run($langs);
	   
	    return $lang;
	}
	public function adduser(){
        $this->name = '添加用户';
        if($_POST["age"]) {
            if ($_POST["age"]) {
                $basedata['age'] = $_POST["age"] ? $_POST["age"] : 1;
            }
            if ($_POST["gender"]) {
                $basedata['gender'] = $_POST["gender"] ? $_POST["gender"] : 1;
            }
            if ($_POST["product"]) {
                $basedata['product'] = $_POST["product"] ? $_POST["product"] : '';
            }
            if (isset($_POST['nickname'])) {
                $basedata['nickname'] = $_POST['nickname'];
            } else {
                //注册时  男性用户默认昵称为“男士”女的默认为“女士”
                if ($basedata['gender'] == 1) {
                    $basedata['nickname'] = $this->L("NANSHENG");
                } else {
                    $basedata['gender'] = 2;
                    $basedata['nickname'] = $this->L("NVSHENG");
                }
            }
            //$basedata['user_type'] = $_POST["usertype"] ? $_POST["usertype"] : 1;
            $basedata['user_type'] = 3;
            $redis = $this->redisconn();
            $redis->del('get_yingxiao_user-zx-status-1');
            $redis->del('get_yingxiao_user-zx-status-2');
            $basedata['puid']=session("AdminThreeUser")['id'];
            //$User_baseM = new UserBaseModel();
            $timenow = time();
            $basedata['password'] =$_POST["password"]?$_POST["password"]:123456;//初始密码
            $basedata['regtime'] = $timenow;
            $basedata['logintime'] = $timenow;
            $basedata['vipgrade']=rand(2,3);
            $basedata['vip']=rand(1000,2000);
            $basedata['viptime']=time()+60*60*24*$basedata['vip'];
            $data=M('user_base')->data($basedata)->add();
            if($data){
                $error=1;
                $msg="添加成功！";
            }
        }
        else{
            $error=1;
            $msg="年龄为空";
		}
		$id=session('AdminThreeUser')['id'];
		$product=M('admin_three_user')->where(array('id'=>$id))->getField('productids');
        $this->product=explode('|',$product);
        $this->error=$error;
        $this->msg=$msg;
        $this->action = "/adms/User/adduser.html";
        $this->display("adduser");


	}



//用户信息
    public function userInfo(){
        $this->name = '用户资料'; // 进行模板变量赋值
        $this->status = C("status");
        $Type = $_GET["type"] ? $_GET["type"] :"base";
        $urlshang = $_SERVER['HTTP_REFERER'];

        if($urlshang){
            $resj=strpos($urlshang,'userinfo');
            if($resj){
                $url=F('LASTURL'.$this->AdminUser["id"]);
            }else{
                $url=$urlshang;
                F('LASTURL'.$this->AdminUser["id"],$url);
            }

        }
        //清空缓存
        if($_GET["clearcache"]==1){
            $this->get_user($_GET["uid"],1);
        }
        if($_GET["uid"]){
            //禁止发信
            if($_GET["setusertype"]){
                if($_GET["setusertype"]==6){
                    $data['user_type'] = '6';
                    $user=M('user_base')->where(array('uid'=>$_GET['uid']))->save($data);
                    $this->get_user($_GET["uid"],1);
                }else{
                    $data['user_type'] = '1';
                    $user=M('user_base')->where(array('uid'=>$_GET['uid']))->save($data);
                    $this->get_user($_GET["uid"],1);
                }
            }
            $attrconf = $this->get_attrconf();
            $userCache = $this->get_diy_user_field($_GET["uid"],"*");
            $extinfo = array();
            foreach ($attrconf as $k=>$v){
                if($userCache[$v['field']]){
                    if($userCache[$v['field']]){
                        if($v["type"]==1){
                            $extinfo[]=array(
                                'name'=>$v["name"],
                                'value'=>$v["option"][$userCache[$v['field']]],
                            );
                        }else{
                            $userValue = explode("|", $userCache[$v['field']]);
                            $str = "";
                            foreach ($userValue as $v1){
                                if($str){
                                    $str.="|".$v["option"][$v1];
                                }else{
                                    $str=$v["option"][$v1];
                                }
                            }

                            $extinfo[]=array(
                                'name'=>$v["name"],
                                'value'=>$str,
                            );

                        }
                    }else{
                        $extinfo[]=array(
                            'name'=>$v["name"],
                            'value'=>$userCache[$v['field']],
                        );
                    }

                }
            }
            //聊天信息
            if($Type==msglist){
                //实例化消息对象
                $msgM=new MsgModel();
                //实例化信箱对象
                $msgboxM=new MsgBoxModel();
                //实例化礼物对象
                $giftM=new GiftModel();
                //解决当没有回复的时候对方的消息看不到的问题
                $msga=$msgboxM->getListbyfield(array('uid'=>$_GET["uid"]),"touid");
                $msga=$msga?$msga:array();
                $msgb=$msgboxM->getListbyfield(array('touid'=>$_GET["uid"]),"uid");
                $msgb=$msgb?$msgb:array();
                $msgc=array_unique(array_merge($msga,$msgb));
                //循环遍历信箱对象，获取聊天用户
                foreach($msgc as $k1=>$v1){
                    $res=$this->get_diy_user_field($v1,'nickname|head');
                    $msglist[$k1]['uid']=$v1;
                    $msglist[$k1]['nickname']=$res['nickname'];
                    $msglist[$k1]['head']=$res['head']['url'];
                    $m=M('msg');
                    $sql="SELECT * FROM `t_msg` WHERE (uid=\"$_GET[uid]\" AND touid=\"$v1\") or (uid=\"$v1\" AND touid=\"$_GET[uid]\")ORDER BY `sendtime`";
                    $msglist[$k1]['msg'] =$m->query($sql);
                    //循环遍历消息对象，获取与每位用户的聊天信息
                    foreach($msglist[$k1]['msg'] as $k=>$v){
                        $res=$this->get_diy_user_field($msglist[$k1]['msg'][$k]['uid'],'nickname');
                        $msglist[$k1]['msg'][$k]['nickname']=$res['nickname'];
                        if($msglist[$k1]['msg'][$k]['type']=='gift'||$msglist[$k1]['msg'][$k]['type']=='askgift'){
                            $restult=$giftM->getone(array('id'=>$msglist[$k1]['msg'][$k]['contentbody']));
                            $msglist[$k1]['msg'][$k]['gifturl']=$restult['url'];
                            $msglist[$k1]['msg'][$k]['gifttitle']=$restult['title'];
                        }
                    }
                }
            }
            //动态信息
            if($Type==dynamic){
                $Page = $_GET["Page"] ? $_GET["Page"] : 1;
                $PageSize = 20;
                $DynamicM = new DynamicModel();
                $Wdata = array();
                $Wdata['uid']=$_GET["uid"];
                $ret = $DynamicM->getListPage($Wdata, $Page, $PageSize,"time");
                foreach($ret["list"] as $k=>$v){
                    $ret["list"][$k]["user"]=$this->get_diy_user_field($v["uid"],"uid|head|nickname");
                    if($v["type"]==3){
                        if($v["ids"]){
                            $ret["list"][$k]['meiti'] = $this->get_videodynamic($v["ids"]);
                        }
                    }elseif($v["type"]==2){
                        $ids = explode("|", $v["ids"]);
                        foreach ($ids as $k1=>$v1){
                            if($v1)
                                $ret["list"][$k]['meiti'][$k1] = $this->get_photodynamic($v1);
                        }
                    }
                }
                $this->msgurl=C ("IMAGEURL");
                $this->DataList = $ret["list"];
                $all_page = ceil($ret['totalCount']/$PageSize);
                $ret["all_page"] = $all_page;
                $this->Pages = $this->GetPages($ret);
                $this->get = $_GET;
            }
            //获取魅力值信息
            $money=M('m_money')->where(array('uid'=>$_GET["uid"]))->find();
        }
        $this->gifturl1=C ("IMAGEURL");
        $this->extinfo = $extinfo;

        $this->UserInfo = $userCache;
/*        $onlineState=$this->get_onlineState($_GET["uid"],$userCache);*/
        $txkey=M('txmy')->where(array('product'=>$userCache['product']))->getField('miyao');
        $tentxunstatus = tencent_onlinestate(array($_GET["uid"]),$txkey);
        $onlineState=$tentxunstatus[0]['State'];
        $this->onlineState=$onlineState;
        $this->msglist = $msglist;
        $this->Type = $Type;
        $this->url=$url;
        $this->money=$money;
        $this->display();
    }
	public function UserList(){
		$this->name = '用户列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		$status = include WR."/Application/Usersadms/Conf/status.php";
		$this->status=$status['status'];

		$Wdata = array();
		if($_GET['user_type']&&$_GET['user_type']!="")
			$Wdata['user_type']=$_GET['user_type'];
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
        if(isset($_GET['product'])&&$_GET['product']!=""){
            $Wdata['product']=$_GET['product'];
		}
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword = $_GET['kerword'];
			$Wdata['uid|nickname|version|systemversion|phoneid|product']=array('like',"%".$kerword."%");
		}
        $Wdata['puid']=session("AdminThreeUser")['id'];
		$UserBaseM = new UserBaseModel();

		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$PageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
	
		foreach($ret['listSearch'] as $k=>$v){
			$userCache = $this->get_user($v["uid"]);
			$ret['listSearch'][$k]=array_merge($ret['listSearch'][$k],$userCache);
            //获取魅力值信息
            $ret['listSearch'][$k]['money']=M('m_money')->where(array('uid'=>$v["uid"]))->find();
		}
		//Dump($ret);exit;
		//Dump($this->get_user(137295));exit;
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Usersadms/User/userlist.html";
		$this->display();
	
	}
	
	public function UserShenhe(){
		$this->name = '用户列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		
		$Wdata = array();
		
		$Wdata['user_type']=2;
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerwordarr = explode(" ",$_GET['kerword']);
			$kerword = $_GET['kerword'];
			if(in_array($kerwordarr[0],C("_PRODUCTS"))){
				$Wdata['product']=$kerwordarr[0];
				if($kerwordarr[1]){
					$kerword = $kerwordarr[1];
				}else{
					$kerword = 0;
				}
			}
			if($kerword)
			$Wdata['uid|nick_name|channelid|pid|phonetype']=array('like',"%".$kerword."%");
			
		}
		if($_GET['channelid']&&$_GET['channelid']!="")
			$Wdata['channelid']=$_GET['channelid'];
		if(isset($_GET['dateStart'])&&$_GET['dateStart']!="")
		$Wdata["regtime"]=array(array('egt',$_GET["dateStart"]),array('elt',$_GET["dateEnd"]));
		$UserBaseM = new UserBaseModel();

		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$pageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
	
		foreach($ret['listSearch'] as $k=>$v){
			
			$ret['listSearch'][$k]=$this->get_user($v["uid"]);
			$ret['listSearch'][$k]["token"] = $v["token"];
			$ret['listSearch'][$k]["password"] = $v["password"];
			$ret['listSearch'][$k]["account"] = $v["account"];
			$ret['listSearch'][$k]["user_type"] = $v["user_type"];
			$ret['listSearch'][$k]["regtime"] =date("Y/m/d H:i:s",strtotime($v["regtime"]));
			$photo_num = 0;
			if(is_array($ret['listSearch'][$k]["listImage"])){
				$photo_num = count($ret['listSearch'][$k]["listImage"]);
			}
			$ret['listSearch'][$k]["photo_num"] = $photo_num;
			
		} 
		//Dump($ret);exit;
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Adms/User/UserShenhe.html";
		$this->display("UserShenhe");
	
	}
	public function MsgList(){
		$this->name = '用户信息'; // 进行模板变量赋值
				$uid = $_GET['uid'] ? $_GET['uid'] : 0;
		$myid = $uid;
		$msgboxlistkey = 'msgboxlist_'.$myid;	
		
		$boxlistall = $this->redis->lRange($msgboxlistkey, 0, -1);
		$BoxList = array();
		foreach($boxlistall as $k=>$v){
			$boxinfo = array();
			$boxinfo = object_array(json_decode($this->redis->get("msgboxlist_value_".$v)));
			if($boxinfo){
				$to_uid = $boxinfo['to_uid'];
				$touinfo = $this->get_user($to_uid);
				
				$uinfo = $this->get_user($myid);
				$touinfo["nickName"] = $touinfo["nickName"]."(".$to_uid.")";
				$uid_str = $myid."_".$to_uid;
				$msglistkey = 'msglist_'.$uid_str;	
				$Temp["boxinfo"] = $boxinfo;
				$Temp["toUname"] = $touinfo["nickName"];
				$msglist = $this->redis->lRange($msglistkey,0, -1);
				$Temp["boxlist"] = array();
				foreach($msglist as $k1=>$v1){
					$msg = object_array(json_decode($v1));
					$tuserinfo = $this->get_user($msg['uid']);
					$Temp["boxlist"][$k1]["nickName"]=$tuserinfo["nickName"]."(".$tuserinfo['id'].")";
					$Temp["boxlist"][$k1]["content"]=$msg['content'];
					$Temp["boxlist"][$k1]["time"]=date("Y-m-d H:i:s",$msg['sys_createdate']);
					
				}
				
			}
			$BoxList[] = $Temp;
		}
		//Dump($BoxList);exit;
		$this->BoxList = $BoxList;
		$this->display("MsgList");
	}
	public function SetUserProduct(){
			$uid = $_GET['uid'] ? $_GET['uid'] : 0;
			$product = $_GET['product'] ? $_GET['product'] : 10108;
			if($uid){
				$where = array('uid'=>$uid);
				$User_baseM = new UserBaseModel();
				$UserInfo = $User_baseM->updateOne($where,array("product"=>$product));
				$this->get_user($uid,1);//更新用户缓存信息
				file_get_contents("http://47.91.136.204:81/Adms/View/Setusercache?uid=".$uid);
				file_get_contents("http://47.89.47.92:81/Adms/View/Setusercache?uid=".$uid);
			}
			header('Location:'.$_SERVER['HTTP_REFERER']);
	}
	//删除用户
	public function deluser(){
		$uid=$_GET['uid'];
		if($uid){
            $this->del_user($_GET['uid']);
            $this->addlog();
            header('Location:'.$_SERVER['HTTP_REFERER']);
		}

	}
	//找回用户
	public function finduser(){
        $this->name = '用户找回'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $this->status = C("status");

        $Wdata = array();
        if($_GET['user_type']&&$_GET['user_type']!="")
            $Wdata['user_type']=$_GET['user_type'];
        if(isset($_GET['gender'])&&$_GET['gender']!="")
            $Wdata['gender']=$_GET['gender'];
        if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
            $kerword = $_GET['kerword'];
            $Wdata['uid|nickname']=array('like',"%".$kerword."%");
        }
        $UserBaseBakM = new UserBaseBakModel();

        $ret = $UserBaseBakM->getlistOrder($Wdata,$Page,$PageSize);

        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;

        foreach($ret['listSearch'] as $k=>$v){

            $userCache = $this->get_user($v["uid"]);

            $ret['listSearch'][$k]=array_merge($ret['listSearch'][$k],$userCache);


        }
        //Dump($ret);exit;
        //Dump($this->get_user(137295));exit;
        $this->DataList = $ret;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = "/Adms/User/finduser.html";
        $this->display();

	}

	//用户还原
	public function user_rbk(){
        if($_GET['uid'])
            $this->rbk_user($_GET['uid']);
        header('Location:'.$_SERVER['HTTP_REFERER']);
	}
	//还原用户方法
    private function rbk_user($id){
        $uid=array();
        $uid['uid']=$id;
        //实例化基本用户表模型
        $UserBaseM = new UserBaseModel();
        //实例化基本用户备份表模型
        $UserBaseBakM=new UserBaseBakModel();
        //实例化基本用户扩展表模型
        $UserExtendM = new UserExtendModel();
        //实例化基本用户扩展备份表模型
        $UserExtendBakM=new UserExtendBakModel();
        $data=$UserBaseBakM->selOne ($uid);
        $datak=$UserExtendBakM->selOne ($uid);
        $result=$UserBaseM->selOne ($uid);
        //查询源表中是否有相同uid，有的话执行更新操作
        if($result){
            $UserBaseM->updateAll ($data,$uid);
        }
        //执行插入操作
        $insert=$UserBaseM->insOne ($data);
        //扩展表执行插入操作
		if($datak!=null){
            $insertk=$UserExtendM->insOne ($datak);
            if($insertk){
                $UserExtendBakM->delOne ($uid);
            }else{
                echo "插入失败";
            }
		}

        if($insert){
            $UserBaseBakM->delOne ($uid);
        }else{
            echo "插入失败";
        }

    }

	//删除用户方法
	private function del_user($id){
		$uid=array();
		$uid['uid']=$id;
		//实例化基本用户表模型
       $UserBaseM = new UserBaseModel();
        //实例化基本用户备份表模型
        $UserBaseBakM=new UserBaseBakModel();
        //实例化基本用户扩展表模型
        $UserExtendM = new UserExtendModel();
        //实例化基本用户扩展备份表模型
        $UserExtendBakM=new UserExtendBakModel();

        $data=$UserBaseM->selOne ($uid);
        $datak=$UserExtendM->selone($uid);

		//执行插入操作
		$insert=$UserBaseBakM->insOne($data);
        if($insert){
            $UserBaseM->delOne ($uid);
            $this->addlog();
        }else{
            echo "插入失败";
        }
        //扩展表执行插入操作
        if($datak!=null){
            $insertk=$UserExtendBakM->insOne ($datak);
            if($insertk){
                $UserExtendM->delOne ($uid);
            }else{
                echo "插入失败";
            }
		}


	}

	/*
	 * 删除图片 /Adm/deleteImg
	*/
	public function deleteImg(){
		$imgid = $_POST['id'];
		$Deldata = array();
		$Deldata['id'] = $imgid;
		$UserPhotoM = new PhotoModel();
		$imgs = $UserPhotoM->getOne($Deldata);
		$uid = $imgs['uid'];
		/**
		 删除数据库
		 */
		 $UserPhotoM->delOne($Deldata);
		/*/**
			删除文件
		 */
        _unlink(WR.$imgs['url']);
        DelOss($imgs['url']);
        //删除相册文件缓存

        //更新头像缓存
        $this->get_user_ico($uid,1);
        $this->get_user_ico_all($uid,1);
        //更新相册缓存
        $this->get_user_photo($uid,1);
        $this->get_user_photo_all($uid,1);
        $reuslt = array(
            'status' =>'1',
            'message' => "已删除图片",
            'data' => '',
        );
        exit(json_encode ($reuslt));

	}
	//清理用户
	public function ClearUser(){
		$this->name = '删除用户'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		
		$Wdata = array();
		if($_GET['user_type']&&$_GET['user_type']!="")
			$Wdata['user_type']=$_GET['user_type'];
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
		if(isset($_GET['kerword'])&&$_GET['kerword']!="")
			$Wdata['uid|nick_name|channelid']=array('like',"%".$_GET['kerword']."%");
		if($_GET['channelid']&&$_GET['channelid']!="")
			$Wdata['channelid']=$_GET['channelid'];
		if(isset($_GET['dateStart'])&&$_GET['dateStart']!="")
		$Wdata["regtime"]=array(array('egt',$_GET["dateStart"]),array('elt',$_GET["dateEnd"]));
		$UserBaseM = new UserBaseModel();
		
		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$PageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
	
		foreach($ret['listSearch'] as $k=>$v){
			$ret['listSearch'][$k]=$this->get_user($v["uid"]);
			$ret['listSearch'][$k]["token"] = $v["token"];
			$ret['listSearch'][$k]["password"] = $v["password"];
			$ret['listSearch'][$k]["account"] = $v["account"];
			$ret['listSearch'][$k]["user_type"] = $v["user_type"];
			$photo_num = 0;
			if(is_array($ret['listSearch'][$k]["listImage"])){
				$photo_num = count($ret['listSearch'][$k]["listImage"]);
			}
			$ret['listSearch'][$k]["photo_num"] = $photo_num;
		}
		$this->getsurl = http_build_query($_GET);
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Adms/User/ClearUser.html";
		$this->display("ClearUser");
	}
	//清理用户
	public function DelAllUser(){
		$this->name = '批量删除用户'; // 进行模板变量赋值
		echo 1;exit;
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 100;
		
		$Wdata = array();
		if($_GET['user_type']&&$_GET['user_type']!="")
			$Wdata['user_type']=$_GET['user_type'];
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
		if(isset($_GET['kerword'])&&$_GET['kerword']!="")
			$Wdata['uid|nick_name|channelid']=array('like',"%".$_GET['kerword']."%");
		if($_GET['channelid']&&$_GET['channelid']!="")
			$Wdata['channelid']=$_GET['channelid'];
		if(isset($_GET['dateStart'])&&$_GET['dateStart']!="")
		$Wdata["regtime"]=array(array('egt',$_GET["dateStart"]),array('elt',$_GET["dateEnd"]));
		
		//$Wdata['product']=array('neq',"10008");
		//$Wdata['photo_number'] = array('gt',3);//照片数量大于三张的不删除
		//$Wdata['user_type'] = 1;//只删除注册用户
		$UserBaseM = new UserBaseModel();
		
		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$PageSize);
		//Dump($ret);exit;
		$all_page = ceil($ret['totalCount']/$PageSize);
		
		foreach($ret['listSearch'] as $k=>$v){
			$this->del_user($v["uid"]);
		}
		
		$getargc = $_GET;
		if($Page==1){
			$q_page = $all_page;
			$q_totalCount = $ret['totalCount'];
			$getargc["q_page"]=$all_page;
			$getargc["q_totalCount"]=$ret['totalCount'];
			$getargc["Page"]=2;
			$getargc["q_wancheng"] = $PageSize;
		}else{
			$getargc["q_wancheng"]=$getargc["q_wancheng"]+$PageSize;
			if($getargc["q_wancheng"]>$getargc["q_totalCount"]){
				$getargc["q_wancheng"] = $getargc["q_totalCount"];
			}
			$getargc["Page"]=$getargc["Page"]+1;
		}
		if($getargc["Page"]>$getargc["q_page"]){
			$this->yiwancheng = 100;
		}else{
			$this->yiwancheng = $getargc["q_wancheng"]/$getargc["q_totalCount"]*100;
		}
		
		$this->msg = '删除进度,共'.$getargc["q_totalCount"]."条;已删除".$getargc["q_wancheng"]; // 进行模板变量赋值
		$this->getsurl = http_build_query($getargc);
		$this->DataList = $ret;
		$this->get = $_GET;
		$this->action = "/Adms/User/DelAllUser.html";
		$this->display("DelAllUser");
	}
	public function UserListOnline(){
		$this->name = '用户列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		
		$Wdata = array();
		if($_GET['user_type']&&$_GET['user_type']!="")
			$Wdata['user_type']=$_GET['user_type'];
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
		if(isset($_GET['kerword'])&&$_GET['kerword']!="")
			$Wdata['uid|nick_name|channelid']=array('like',"%".$_GET['kerword']."%");
		if($_GET['channelid']&&$_GET['channelid']!="")
			$Wdata['channelid']=$_GET['channelid'];
		if(isset($_GET['dateStart'])&&$_GET['dateStart']!="")
		$Wdata["regtime"]=array(array('egt',$_GET["dateStart"]),array('elt',$_GET["dateEnd"]));
		$UserBaseM = new UserBaseModel();
		$Wdata['onlinestate']=1;
		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$pageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
	
		foreach($ret['listSearch'] as $k=>$v){
			$ret['listSearch'][$k]=$this->get_user($v["uid"]);
			$ret['listSearch'][$k]["token"] = $v["token"];
			$ret['listSearch'][$k]["password"] = $v["password"];
			$ret['listSearch'][$k]["account"] = $v["account"];
			$ret['listSearch'][$k]["user_type"] = $v["user_type"];
			$photo_num = 0;
			if(is_array($ret['listSearch'][$k]["listImage"])){
				$photo_num = count($ret['listSearch'][$k]["listImage"]);
			}
			$ret['listSearch'][$k]["photo_num"] = $photo_num;
		}
		
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Adms/User/UserListOnline.html";
		$this->display("UserList");
	
	}

	//会员充值
	public function recharge()
    {
        $this->name = '会员充值'; // 进行模板变量赋值
        $Wdata = array();
        if ($_POST["uid"]&&$_POST["count"]) {
            $uid = $_POST["uid"];
            $paytype = $_POST['type'];
            $huiyuantype = $_POST['type1'];
            $count = $_POST['count'];//充值天数或金币数量
            $paytime = time();
            $reData = array();
            $reData['uid'] = $uid;
            $reData['days'] = $count;
            $reData['paytime'] = $paytime;//服务器时间
            $reData['translateid'] = "测试订单";
            $reData['money'] = '100';
            if($paytype==1){
                $reData['type'] =3;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
            }else{
                $reData['type'] =1;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
            }

            $UserBaseM = new UserBaseModel();
            $RechargeM = new RechargeModel();


            $data = $UserBaseM->getone (array('uid' => $uid));
            if ($data) {
                if ($paytype == 1) {
                    $Wdata['gold'] = $count + $data['gold'];
                    $reData['vipgold'] = $count + $data['gold'];
                } else {
                    $Wdata['viptime'] = $count * 60 * 60 * 24;
                    $rtime = $data['viptime'];
                    if ($rtime == 0||$rtime<time()) {
                        $Wdata['viptime'] = $Wdata['viptime'] + time ();
                    }else {
                        $Wdata['viptime'] = $Wdata['viptime'] + $rtime;
                    }
                    $reData['vipgold'] =$Wdata['viptime'] ;

                }
                 $charid=$RechargeM->addOne($reData);
                if($huiyuantype){
                    $Wdata['vipgrade']=$huiyuantype;
				}
                $res = $UserBaseM->updateOne (array('uid' => $uid), $Wdata);
                if ($res) {
                    $this->get_user($uid,1);
                    $reuslt = array(
                        'status' => '1',
                        'message' => "充值成功",
                        'data' => '',
                    );
                    $this->addlog();
                    exit(json_encode ($reuslt));
                } else {
                    $reuslt = array(
                        'status' => '2',
                        'message' => "充值失败",
                        'data' => '',
                    );
                    exit(json_encode ($reuslt));
                }
            }else{
                $reuslt = array(
                    'status' => '3',
                    'message' => "没有找到相关的uid",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
        $this->action = "/Adms/User/recharge.html";
        $this->display ();

    }
//获取视频
    public function get_videodynamic($id,$reset='0'){
        $path = WR.'/userdata/cache/dynamic/video/';
        $cache_name = 'videoinfo_'.$id;
        if(F($cache_name,'',$path) && $reset == 0){
            $Info = F($cache_name,'',$path);
        }else{
            $VideoDynamicM = new VideoDynamicModel();
            $videoTemp = $VideoDynamicM->getOne(array("id"=>$id));
            $Info=array(
                "id"=>$videoTemp["id"],
                "ltime"=>$videoTemp["ltime"],
                "url"=>C("IMAGEURL").$videoTemp["url"],
                "imageurl"=>C("IMAGEURL").$videoTemp["imageurl"],
                "status"=>$videoTemp["status"]
            );
            F($cache_name,$Info,$path);
        }
        return $Info;
    }
    //获取动态图片
    public function get_photodynamic($id,$reset='0'){
        $path = WR.'/userdata/cache/dynamic/photo/';
        $cache_name = 'photoinfo_'.$id;
        if(F($cache_name,'',$path) && $reset == 0){
            $Info = F($cache_name,'',$path);
        }else{
            $PhotoDynamicM = new PhotoDynamicModel();
            $Temp = $PhotoDynamicM->getOne(array("id"=>$id));
            $Info=array(
                "id"=>$Temp["id"],
                "url"=>C("IMAGEURL").$Temp["url"],
                "thumbnaillarge"=>C("IMAGEURL").$Temp["url"],
                "thumbnailsmall"=>C("IMAGEURL").$Temp["url"],
                "status"=>$Temp["status"],
                "seetype"=>$Temp["seetype"]
            );
            F($cache_name,$Info,$path);
        }
        return $Info;
    }
    //动态审核通过后重新驳回
	public function judge(){
 		$Wdata = array();
        if ($_POST["status"]&&$_POST["id"]) {
            $status = $_POST["status"];
            $id=$_POST["id"];
            $uid=$_POST["uid"];
            $dynamicM= new DynamicModel();
            $PhotoDynamicM = new PhotoDynamicModel();
            $VideoDynamicM = new VideoDynamicModel();
            if($status==3){
            	$res1=$dynamicM->getOne(array('id'=>$id));
				$type=$res1['type'];
				if($type==2){
                   $photod= $PhotoDynamicM->where(array('danamicid'=>$id))->select();
                   foreach($photod as $k=>$v){
                       DelOss($v['url']);
				   }
                    $PhotoDynamicM->where(array('danamicid'=>$id))->delete();
				}
				if($type==3){
					$videod=$VideoDynamicM->getOne(array('danamicid'=>$id));
                    DelOss($videod['url']);
                    $res1=$VideoDynamicM->where(array('danamicid'=>$id))->delete();
				}
                $res=$dynamicM->delOne(array('id'=>$id));
		}
		if($res) {
            $reuslt = array(
                'status' => '1',
                'message' => "更新成功",
                'data' => '',
            );
        }
		//删除已经通过的动态，也需要将存在redis中的缓存删除，然后重新读取数据库
            $redis=$this->redisconn();
			$redis->del('dynamic');
        }
        exit(json_encode ($reuslt));
	}

	//获取在线用户
	public function getonlineStatelist()
    {
        import("Extend.Library.ORG.Util.Page");
        $this->name = '在线用户'; // 进行模板变量赋值
        $this->status = C("status");
        //获取平台号
		$getproduct1=M('products')->select();
		foreach($getproduct1 as $k=>$v){
            $getproduct[]=$v['product'];
		}
		$this->getproduct=$getproduct;
    	$arr['gender']= $_GET['gender']?$_GET['gender']:2;
    	$arr['product']= $_GET['product']?$_GET['product']:20110;
    	$arr['user_type']= $_GET['user_type']?$_GET['user_type']:3;
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize =15;
        $redis=$this->redisconn();
        $useronlinelistkey = "useronlinelist_".$arr['product']."_".$arr['user_type']."_".$arr['gender'];
        if($redis->exists($useronlinelistkey)){
            $lang=$redis->listSize($useronlinelistkey);
            //页总数
            $page_count = ceil($lang/$PageSize);
            $onlinelist=$redis->listLrange($useronlinelistkey,($Page-1)*$PageSize,(($Page-1)*$PageSize+$PageSize-1));
            //$onlinelist = $redis->listGet($useronlinelistkey, 0, -1);
            $retArray = array();
            $retArray['totalCount'] = $lang;
            $retArray['pageSize'] = $PageSize;
            $retArray['pageNum'] = $Page;
            $retArray['list'] = $onlinelist;
        }
        $ret=array();
        foreach($onlinelist as $k=>$v){
        	$ret[]=$this->get_user($v);
		}
        $this->DataList = $ret;
        $this->gender=$arr['gender'];
        $this->product=$arr['product'];
        $this->user_type=$arr['user_type'];
        $this->Pages = $this->GetPages($retArray);
        $this->display();
    }

	//魅力值记录
    public function meilizhijilu()
    {
        $this->name = '魅力值记录'; // 进行模板变量赋值
		$id=session("AdminThreeUser")['id'];
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $uid=$_GET["uid"];
        $PageSize = 30;
        $PMoneyM = new PMoneyModel();
        $Wdata = array();
        $Wdata['uid']=$uid;
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['to_uid|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['type']=array('in',array(1,2,3,11));
        $Wdata['money'] = array('gt',0);
        $ret = $PMoneyM->getListPage($Wdata, $Page, $PageSize);
        $totalmoney=0;
        foreach($ret['list'] as $k=>$v){
            /*$sharemoney=M('p_money')->where(
                array(
                    'uid'=>$id,
                    'type'=>8,
                    'paytime'=>$v['paytime'],
                    'translateid'=>$v['translateid'],
                ))->find();*/
            $sharemoney['money']=intval($v['money']/(9-9*$v['bili']*0.01));
            $ret['list'][$k]['sharemoney']=$sharemoney['money'];
      		$totalmoney+=$sharemoney['money'];
		}
        $totalbilimoney=0;
        foreach($ret['list'] as $k=>$v){
            /*$bilimoney=M('p_money')->where(
                array(
                    'uid'=>$id,
                    'type'=>9,
                    'paytime'=>$v['paytime'],
                    'translateid'=>$v['translateid'],

                ))->find();*/
         $bilimoney['money']=intval($v['money']*$v['bili']*0.01/(1-$v['bili']*0.01));
         $ret['list'][$k]['bilimoney']=$bilimoney['money'];
         $totalbilimoney+=$bilimoney['money'];
        }

       // Dump($ret);exit;
        $this->ListData = $ret["list"];
        $this->totalmoney = $totalmoney;
        $this->totalbilimoney = $totalbilimoney;
        $this->status = $Wdata['is_tixian'];
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->actionName =  ACTION_NAME;
        $this->display();
    }
}

?>
