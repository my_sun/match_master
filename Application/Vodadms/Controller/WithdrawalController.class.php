<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/27
 * Time: 17:25
 */

class WithdrawalController extends BaseAdmsController
{
    public function __construct()
    {
        parent::__construct();

    }

    //用户提现订单列表
    public function lists()
    {
        $this->name = '提现列表'; // 进行模板变量赋值
        $id=session("AdminThreeUser")['id'];
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 200;
        $withd = new PMoneyModel();
        $Wdata = array();
        $Wdata['_string'] = 'type=10';
        $Wdata['pid']=$id;
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid|type|money|to_uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $withd->getListPage($Wdata, $Page, $PageSize);
        foreach ($ret["list"] as $k => $v) {
            $ret["list"][$k]['realmoney'] = $v['money'] * 0.01* 6.5;
            if($v['account_type']==4){
                $ret["list"][$k]['realmoney'] = $v['money']* 0.2;
            }
            $msgmon = M('m_money')->where(array('uid' => $v['uid']))->find();
            $ret["list"][$k]['username']=$msgmon['commonpay']?$msgmon['commonpay']:" ";
            $ret["list"][$k]['phonenum']=$msgmon['otherpay']?$msgmon['otherpay']:" ";
        }
        $datamsg = M('m_money');
        $this->ListData = $ret["list"];
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display();
    }

    //所属用户魅力值记录
    public function usermoneylist(){
        $this->name = '用户魅力值记录'; // 进行模板变量赋值
        $id=session("AdminThreeUser")['id'];
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 200;
        $withd = new PMoneyModel();
        $Wdata = array();
        $Wdata['type']=array('in',array(1,2,3,11));
        $Wdata['money'] = array('gt',0);
        $Wdata['pid']=$id;
        if(isset($_GET['is_tixian'])&&$_GET['is_tixian']!="")
        {$Wdata['is_tixian']=$_GET['is_tixian']?$_GET['is_tixian']:'';
        };
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid|to_uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $withd->getListPage($Wdata, $Page, $PageSize);
        foreach ($ret["list"] as $k => $v) {
            $ret["list"][$k]['realmoney'] = $v['money'] * 0.01* 6.5;
        }
        $this->ListData = $ret["list"];
        $this->status = $Wdata['is_tixian'];
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display();
    }









    //魅力值排行
    public function paihang()
    {
        $yue = $_GET["yue"] ? $_GET["yue"] : date('Y-m');
        $this->name = $yue.'月排行'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $gender = $_GET["gender"] ? $_GET["gender"] : 0;
        $PageSize = 200;
        $PMoneyM = new PMoneyModel();
        $Wdata = array();
        $Wdata['type']=array('in',array(1,2,3));
        $Wdata['money'] = array('gt',0);
        
        
        if($yue){
            $yueArr = $this->getShiJianChuo($yue);
            $Wdata["paytime"]=array(array('egt',$yueArr["begin"]),array('elt',$yueArr["end"]));
        }
        if($_GET["gender"]){
            if($_GET["gender"]==1){
                $Wdata['gender'] = $_GET["gender"];
            }else{
                $Wdata['gender'] = 2;
            }
            
        }
        $ret = $PMoneyM->getListGroupOrder($Wdata);
        //echo $PMoneyM->getLastSql();
        //Dump($ret);exit;
        foreach ($ret as $k=>$v){
            $ret[$k]["user"]=$this->get_user($v["uid"]);
        }
        
        $yuelist = array();
        $currentTime = time();
        $cyear = floor(date("Y",$currentTime));
        $cMonth = floor(date("m",$currentTime));
        for($i=0;$i<6;$i++){
            $nMonth = $cMonth-$i;
            $cyear = $nMonth == 0 ? ($cyear-1) : $cyear;
            $nMonth = $nMonth <= 0 ? 12+$nMonth : $nMonth;
            $yuelist[]= $cyear."-".$nMonth;
        }
        
        $this->yuelist=array_reverse($yuelist);
        $this->ListData = $ret;
        $this->gender =$gender;
        $this->yue = $yue;
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display();
    }
   protected function getShiJianChuo($nianyue=0){
       $arr = explode("-", $nianyue);
       $nian = $arr[0];
       $yue = $arr[1];
        if(empty($nian) || empty($yue)){
            $now = time();
            $nian = date("Y",$now);
            $yue =  date("m",$now);
        }
        $time['begin'] = mktime(0,0,0,$yue,1,$nian);
        $time['end'] = mktime(23,59,59,($yue+1),0,$nian);
        return $time;
    }
    //魅力值记录
    public function meilizhijilu()
    {
        $this->name = '魅力值记录'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 30;
        $PMoneyM = new PMoneyModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['to_uid|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['type']=array('in',array(1,2,3,11));
        $Wdata['money'] = array('gt',0);
        $ret = $PMoneyM->getListPage($Wdata, $Page, $PageSize);
        
       // Dump($ret);exit;
        $this->ListData = $ret["list"];
        $this->status = $Wdata['is_tixian'];
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->actionName =  ACTION_NAME;
        $this->display();
    }
    public function delmeilizhi(){
        $id = $_GET['id'] ? $_GET['id'] : 0;
        $PMoneyM = new PMoneyModel();
        $PMoneyInfo = $PMoneyM->getOne(array('id'=>$id));
        //Dump($PeiliaoMoneyInfo);exit;
        if($PMoneyInfo){
            $this->m_money_dec($PMoneyInfo["uid"], $PMoneyInfo["money"]);
            $PMoneyDelM = new PMoneyDelModel();
            unset($PMoneyInfo["id"]);
            $PMoneyDelM->addOne($PMoneyInfo);
            $PMoneyM->delOne(array('id'=>$id));
        }
        echo '<script language="JavaScript">
			window.location.href="'.$_SERVER['HTTP_REFERER'].'";
			</script>';
        //header('Location:/adm/orderlist');
        
    }
    //魅力值找回
    public function meilizhijiludel()
    {
        $this->name = '魅力值找回'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 30;
        $PMoneyDelM = new PMoneyDelModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['to_uid|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['type']=array('in',array(1,2,3,8));
        $Wdata['money'] = array('gt',0);
        $ret = $PMoneyDelM->getListPage($Wdata, $Page, $PageSize);
        
        // Dump($ret);exit;
        $this->ListData = $ret["list"];
        $this->status = $Wdata['is_tixian'];
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->actionName =  ACTION_NAME;
        $this->display("meilizhijilu");
    }
    public function zhaohuid(){
        $id = $_GET['id'] ? $_GET['id'] : 0;
        $PMoneyM = new PMoneyModel();
        $PMoneyDelM = new PMoneyDelModel();
        $PeiliaoMoneyInfo = $PMoneyDelM->getOne(array('id'=>$id));
        //Dump($PeiliaoMoneyInfo);exit;
        if($PeiliaoMoneyInfo){
            $this->m_money_add($PeiliaoMoneyInfo["uid"], $PeiliaoMoneyInfo["money"]);
            unset($PeiliaoMoneyInfo["id"]);
            $PMoneyM->addOne($PeiliaoMoneyInfo);
            $PMoneyDelM->delOne(array('id'=>$id));
        }
        echo '<script language="JavaScript">
			window.location.href="'.$_SERVER['HTTP_REFERER'].'";
			</script>';
        //header('Location:/adm/orderlist');
        
    }	
    public function judge()
    {
        $Wdata = array();
        if ($_POST["is_tixian"] && $_POST["id"]) {
            $status = $_POST["is_tixian"];
            $id = $_POST["id"];
            $uid = $_POST["uid"];
            $userinfo = $this->get_user($uid);
            $money = $_POST['money'];
            $type=$_POST['type'];
            $withd = new PMoneyModel();
            $userbeseM=new UserBaseModel();
            $msgmon = M('m_money')->where(array('uid' => $uid))->find();
            $res = $withd->updateOne(array('id' => $id), array('is_tixian' => $status));
            if ($res) {
                if ($status == 3) {
                    $datamoney = array();
                    $datamoney['leftmoney'] = $msgmon['leftmoney'] + $money;
                    M('m_money')->where(array('uid' => $uid))->save($datamoney);
                }else{
                    if($type==4){
                        $lgold=$userbeseM->where(array('uid'=>$uid))->getField('gold');
                        if($userinfo["user_type"]==3){
                            $rgold=$lgold+$money*0.01*25*4;
                        }else{
                            $rgold=$lgold+$money*0.01*25*2;
                        }
                        $msgs=$userbeseM->updateOne(array('uid'=>$uid),array('gold'=>$rgold));
                        if($msgs){
                            $this->get_user($uid,1);
                            $this->sendsysmsg($uid, "魅力值轉換金幣已通過審核");
                        }
                    }
                }
                $reuslt = array(
                    'status' => '1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            } else {
                $reuslt = array(
                    'status' => '2',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            }
        }

    }
}