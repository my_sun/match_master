<?php
class IndexController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}
	
	public function Index(){
        $AdminThreeUser=session("AdminThreeUser");
        $admin = new VodAdminModel();
        $info = $admin->getOne(array("id"=>$AdminThreeUser['id']));
        $count_money = $info['count_money'];
        $money = $count_money - ($count_money*($info['gold_pump']/100));//获取抽成后平台展示应得魅力值
        //获取魅力值兑换rmb配置
        $to_rmb = M("vod_private_set")->field("to_rmb")->find();
        if($AdminThreeUser['type'] == 0){
            $todayusermoney = M("vod_admin")->field("sum(count_money) as count_money")->select();
            $this->todayusermoney = $todayusermoney[0]['count_money'];
            $this->todayuserrelmoney = ceil($todayusermoney[0]['count_money']/$to_rmb['to_rmb']);
        }else{
            $this->todayusermoney = $count_money;
            $this->todayuserrelmoney = ceil($money/$to_rmb['to_rmb']);
        }
        $this->admin_type = $AdminThreeUser['type'];
        $this->action = '/Vodadms/Index/adddraw';
		$this->name = '首页'; // 进行模板变量赋值
		$this->display();
	}


	public function adddraw(){
	    if(!isset($_POST['comdraw']) || empty($_POST['comdraw']) || $_POST['comdraw']<=0){
            $reuslt = array(
                'status' => '2',
                'message' => "请输入正确的提款金额",
                'data' => '',
            );
            exit(json_encode($reuslt));
        }
        $AdminThreeUser=session("AdminThreeUser");
        $admin = new VodAdminModel();
        $res = $admin->getOne(array("id"=>$AdminThreeUser['id']));
        $count_money = $res['count_money'];
//        $money = $count_money - ($count_money*($info['gold_pump']/100));
//        //获取魅力值兑换rmb配置
//        $to_rmb = M("vod_private_set")->field("to_rmb")->find();
//        $rmb_money = ceil($money/$to_rmb['to_rmb']);
        if($_POST['comdraw'] > $count_money){
            $reuslt = array(
                'status' => '2',
                'message' => "提现金额超出总收益",
                'data' => '',
            );
            exit(json_encode($reuslt));
        }else{
            //进行提现记录
            $name = $_POST['name']?$_POST['name']:'';
            $account = $_POST['account']?$_POST['account']:'';
            $pay_type = $_POST['pay_type']?$_POST['pay_type']:'';
            $comdraw = $_POST['comdraw']?$_POST['comdraw']:'';
            if(empty($name) || empty($account) || empty($pay_type) || empty($comdraw)){
                $reuslt = array(
                    'status' => '2',
                    'message' => "请完善提现信息",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            }else{
                //获取当前是否有上级id

                $info = array(
                    'draw_uid'=>$AdminThreeUser['id'],
                    'pid'=>$res['company_id'],
                    'comdraw'=>$comdraw,
                    'name'=>$name,
                    'account'=>$account,
                    'pay_type'=>$pay_type,
                    'ctime'=>time()
                );
                $vodadmdraw = new VodAdmdrawModel();
                $id = $vodadmdraw->addOne($info);
                if($id){
                    //扣除魅力值
                    $un_money = $count_money-$comdraw;
                    $monry_res = $admin->updateOne(array("id"=>$AdminThreeUser['id']),array('count_money'=>$un_money));

                    if($monry_res){
                        $reuslt = array(
                            'status' => '1',
                            'message' => "申请提现成功",
                            'data' => '',
                        );
                        exit(json_encode($reuslt));
                    }

                }
            }

        }
        $reuslt = array(
            'status' => '2',
            'message' => "操作失败",
            'data' => '',
        );
        exit(json_encode($reuslt));
    }




}

?>
