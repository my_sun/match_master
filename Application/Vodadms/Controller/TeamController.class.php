<?php
class TeamController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
        $AdminThreeUser = session("AdminThreeUser");
        if($AdminThreeUser['type']!=0 && $AdminThreeUser['type']!=1){
            echo "您没有权限";
            sleep(3);
            $this->redirect("/Vodadms/Index/index");die;
        }
	}
	/**
	 * 运营公司列表
	 */
	public function teamlist(){
        $AdminThreeUser = session("AdminThreeUser");
        //获取运营公司列表
        $where['type'] = 2;
        if($AdminThreeUser['type'] != 0){
            $where['company_id'] = $AdminThreeUser['id'];
        }

        $pageNum = empty($_GET['page'])?1 :$_GET['page'];
        $pageSize = 20;
        $vodAdmin = new VodAdminModel();
        $list = $vodAdmin->getListPage($where,$pageNum,$pageSize);
        if(!empty($list['list'])){
            foreach ($list['list'] as &$value){
                $Superior = $vodAdmin->getOne(array('id'=>$value['company_id']));
                $value['superior_name'] = $Superior['company_name']?$Superior['company_name']:"平台";
            }
        }
        $this->DataList = $list;
        $this->Pages = $this->GetPages($list['list']);
        $this->name = '运营团队列表';
        $this->action = "/Vodadms/Team/teamlist.html";
        $this->display();


	}

	public function teamadd(){
        $this->name = '添加运营团队';
        if(!empty($_POST['account']) && !empty($_POST['password'])){
            $name = $_POST['name'];
            $account = $_POST['account'];
            $password = $_POST['password'];
            $password_two = $_POST['password_two'];
            $gold_pump = $_POST['gold_pump'];
            if($password !== $password_two){
                $error=1;
                $msg="密码不一致！";
                $this->error=$error;
                $this->msg=$msg;
                $this->display();
            }else{
                $AdminThreeUser = session("AdminThreeUser");
                $admin_vod = new VodAdminModel();

                $account_have = $admin_vod->getOne(array("name"=>$account));
                if($account_have){
                    $error=1;
                    $msg="账号已存在，请重新设置账号！";
                    $this->error=$error;
                    $this->msg=$msg;
                    $this->display();
                }else{
                    $admin_info = array();
                    $admin_info['company_name'] = $name;
                    $admin_info['name'] = $account;
                    $admin_info['password'] = $password;
                    $admin_info['type'] = 2;
                    $admin_info['gold_pump'] = $gold_pump;
                    $admin_info['time'] = time();
                    $admin_info['company_id'] = $AdminThreeUser['id'];
                    $resid = $admin_vod->addOne($admin_info);
                    if(!empty($resid)){
                        $error=1;
                        $msg="添加运营团队成功！";
                    }else{
                        $error=1;
                        $msg="添加失败，请检查网络刷新后重新添加！";
                    }
                }
            }


        }

        $this->error=$error;
        $this->msg=$msg;
        $this->action = "/Vodadms/Team/teamadd.html";
        $this->display();
    }


}

?>
