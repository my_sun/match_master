<?php
class DrawController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
        $AdminThreeUser = session("AdminThreeUser");
//        if($AdminThreeUser['type']!=0){
//            echo "您没有权限";
//            $this->redirect("/Vodadms/Index/index");die;
//        }
	}
	/**
	 * 用户提现申请列表
	 */
	public function userdrawlist(){
        $Page = $_GET["page"] ? $_GET["page"] : 1;
        $PageSize = 20;
        $AdminThreeUser = session("AdminThreeUser");
        $superiorid = $AdminThreeUser['id'];
        //实例化提现model
        $drawData = new VodDrawModel();
        $map = array();
        if($AdminThreeUser['type']!=0){
            $map['superiorid'] = $superiorid;
        }

        $drawinfo = $drawData->getListPage($map,$Page,$PageSize);
        $all_page = ceil($drawinfo['totalCount']/$PageSize);
        $drawinfo["all_page"] = $all_page;
        if($drawinfo['totalCount'] > 0){
            foreach($drawinfo['list'] as &$val){
                $val['user_type'] = $val['user_type'] == 0 ?"普通用户":"后台添加用户";
                switch ($val['currency_type']){
                    case "1":
                        $val['currency_type'] = "支付宝";
                        break;
                    case "2":
                        $val['currency_type'] = "微信";
                        break;
                    case "3":
                        $val['currency_type'] = "Paypal";
                        break;
                    case "4":
                        $val['currency_type'] = $val['lpkname']."礼品卡";
                        break;
                }
                $val['superior'] = "平台";
                //实例化
                $vodadmin = new VodAdminModel();
                $adminame = $vodadmin->getOne(array("id"=>$val['superiorid']));
                $val['is_super'] = 2;
                if($val['superiorid'] != 0){
                    $val['superior'] = $adminame['company_name'];
                    //如果是平台账户  只允许查看不允许操作 将操作置空
                    $val['is_super'] = 1;
                    if($AdminThreeUser['id'] == $val['superiorid']){
                        $val['is_super'] = 2;
                    }
                }

                //获取聊天总时长
                $compre = new CompereInfoModel();
                $alltime = $compre->getOne(array("uid"=>$val['uid']));
                $val['alltime'] = $alltime['talk_time']+$alltime['video_time'];


            }
        }

        //var_dump($drawinfo);die;
        $this->DataList = $drawinfo;
        $this->Pages = $this->GetPages($drawinfo);
        $this->name = '用户提现申请表';
        $this->action = "/Vodadms/draw/userdrawlist";
        $this->display();

	}
	public function teamdrawlist(){
        $Page = $_GET["page"] ? $_GET["page"] : 1;
        $PageSize = 20;
        $AdminThreeUser = session("AdminThreeUser");
        $drawData = new VodAdmdrawModel();
        $map = array();
        if($AdminThreeUser['type']!=0){
            $map['pid'] = $AdminThreeUser['id'];
        }
        $drawinfo = $drawData->getListPage($map,$Page,$PageSize);
        $all_page = ceil($drawinfo['totalCount']/$PageSize);
        $drawinfo["all_page"] = $all_page;
        if($drawinfo['totalCount'] > 0){
            foreach($drawinfo['list'] as &$val) {
                //获取团队名称
                $vodadmin = new VodAdminModel();
                $adminame = $vodadmin->getOne(array("id"=>$val['draw_uid']));
                $val['user_name'] = $adminame['company_name'];
                $count_money = $adminame['count_money'];
                $val['money'] = $count_money;//剩余总魅力值
                $val['gold_pump'] = $adminame['gold_pump'];//提现比例
                $money = $count_money - ($count_money*($adminame['gold_pump']/100));//获取抽成后平台展示应得魅力值

                $money1 = $val['comdraw'] - $val['comdraw']*($adminame['gold_pump']/100);
                //获取魅力值兑换rmb配置
                $to_rmb = M("vod_private_set")->field("to_rmb")->find();
                $val['rmb_money'] = ($money1/$to_rmb['to_rmb']);//提现兑换完所得
                switch ($val['pay_type']) {
                    case "1":
                        $val['pay_type'] = "支付宝";
                        break;
                    case "2":
                        $val['pay_type'] = "微信";
                        break;
                    case "3":
                        $val['pay_type'] = "Paypal";
                        break;
                }
                $val['is_super'] = 1;
                if($adminame['type'] == 1){
                    if($AdminThreeUser['company_id'] == 0){
                        $val['is_super'] = 2;
                    }
                }elseif($adminame['type'] == 2){
                    if($AdminThreeUser['id'] == $val['pid']){
                        $val['is_super'] = 2;
                    }
                }


            }
        }
        //var_dump($drawinfo);die;
        $this->DataList = $drawinfo;
        $this->Pages = $this->GetPages($drawinfo);
        $this->name = '团队提现申请表';
        $this->action = "/Vodadms/draw/userdrawlist";
        $this->display();
    }


	public function toexamine(){
        if($_POST['id'] && $_POST['uid']){
            $is_success = $_POST["is_success"];
            $id = $_POST["id"];
            $money = $_POST["money"];
            $uid = $_POST["uid"];
            $VodDraw = new VodDrawModel();
            $msgmon = M('m_money')->where(array('uid' => $uid))->find();

            if ($is_success == 2) {
                $datamoney = array();
                $datamoney['totalmoney'] = $msgmon['totalmoney'] + $money;
                $datamoney['leftmoney'] = $msgmon['leftmoney'] + $money;
                $mon = M('m_money')->where(array('uid' => $uid))->save($datamoney);
                if($mon){
                    $redis=$this->redisconn();
                    $redis->delete('withdrawal_' . $uid);
                }

            }

            $res = $VodDraw->updateOne(array('id' => $id), array('is_success' => $is_success));
            if($res){
                $reuslt = array(
                    'status' => '1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            }else{
                $reuslt = array(
                    'status' => '2',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            }

    }

    }

    public function toexamine_team(){
        if($_POST['id'] && $_POST['uid']){
            $is_success = $_POST["is_success"];
            $id = $_POST["id"];
            $money = $_POST["money"];
            $uid = $_POST["uid"];
            $VodDraw = new VodAdmdrawModel();
            $msgmon = M('vod_admin')->where(array('id' => $id))->find();

            if ($is_success == 2) {
                $datamoney = array();
                $datamoney['count_money'] = $msgmon['count_money'] + $money;
                M('vod_admin')->where(array('id' => $id))->save($datamoney);

            }

            $res = $VodDraw->updateOne(array('id' => $id), array('is_success' => $is_success));
            if($res){
                $reuslt = array(
                    'status' => '1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            }else{
                $reuslt = array(
                    'status' => '2',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            }

        }

    }
}
?>