<?php
use Think\Controller;
class PaymentmethodController extends BaseAdmsController
{
    public function __construct()
    {
        parent::__construct();
        $this->basename = '支付方式-'; // 进行模板变量赋值
        $this->cachepath = WR . '/userdata/cache/face/';
        $this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";

    }
    
    //支付方式列表
    public function lists()
    {
        $this->name = $this->basename . '列表'; // 进行模板变量赋值

        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 12;
        $Paymentmethod = new PaymentmethodModel();
        //$status = include WR . "/userdata/publicvar/status.php";
        //$this->switch_type = $status["gift"]["type"];
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|payment_name|donationratio'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $Paymentmethod->getListPage ($Wdata, $Page, $PageSize);
        $this->msgurl=C ("IMAGEURL");
        $this->DataList= $ret["list"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display ();
    }

    //商品添加
    public function add()
    {
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        $status = C("status");
        //$this->usertypestatus = $status["gift"]["user_type"];
        if($_POST){
            if(!empty($_FILES)) {
                $PutOssarr=$this->_imgupload();
            }
            if($_POST["payment_name"]&&$_POST["donationratio"]){
                $Paymentmethod = new PaymentmethodModel();
                $map = array();
                $map["payment_name"] = $_POST["payment_name"];
                $map["donationratio"] = $_POST["donationratio"];
                $map["type"] = $_POST["type"];
                $map["url"] = $_POST["url"];
                $map["log"] =  $PutOssarr;
                $Paymentmethod->addOne($map);
                //连接redis
            $redis = new RedisModel();
            //设置key
            $key = "paymentmethod_key";
            // 删除缓存
            $redis->delete($key);
             
                $this->tip = $_POST["payment_name"]."已添加";

                header('Location:applist.html');

            }else{
                $this->tip = "不能为空！";
            }
        }
       
        $this->msgurl=C ("IMAGEURL");
        $this->action =  __ACTION__.".html";
        $this->display();
    }

    //商品状态
    public function updatestatus()
    {
        $Paymentmethod = new PaymentmethodModel();
        if ($_POST) {
            if ($_POST["gid"]) {
                $map=array();
                $map['status']=$_POST["status"];
                $return=array();
                $return['id']=$_POST["gid"];
                 $result=$Paymentmethod->updateAll ($map,$return);
                 //连接redis
            $redis = new RedisModel();
            //设置key
            $key = "paymentmethod_key";
            // 删除缓存
            $redis->delete($key);
                 if($result){
                     $content= array(
                         'status' => 1,
                         'gstatus'=>$map['status'],
                         'message' => '新增成功'
                     );
                     exit(json_encode ($content));
                 }else{
                     $content= array(
                         'status' => 2,
                         'message' => '修改失败！'
                     );
                     exit(json_encode ($content));
                 }
                //return json_encode ($map);
            }
        }
    }

    public function edit(){

        $this->name = $this->basename.'修改'; // 进行模板变量赋值
        $Paymentmethod  = new PaymentmethodModel();
        $id = $_REQUEST['id'];
        if($id){
            if($_POST){
                if($_POST["id"]){
                    if(!empty($_FILES)) {
                        $PutOssarr=$this->_imgupload();
                        // dump($PutOssarr);die;
                        if(!$PutOssarr){
                            $PutOssarr=$_POST["log"];
                        }/*else{
                            DelOss($_POST["tmpimg"]);

                        }*/

                    }else{
                        $PutOssarr=$_POST["log"];
                    }
                    $map = array();
                    $map["payment_name"] = $_POST["payment_name"];
                    $map["donationratio"] = $_POST["donationratio"];
                    $map["type"] = $_POST["type"];
                    $map["url"] = $_POST["url"];
                    $map["log"] =  $PutOssarr;
                    $this->msgurl=C ("IMAGEURL");
                    $tempT = $Paymentmethod->getOne($_POST["id"]);
                    $where["id"] = $_POST["id"];
                    //连接redis
            $redis = new RedisModel();
            //设置key
            $key = "paymentmethod_key";
            // 删除缓存
            $redis->delete($key);
                    if($tempT){
                        $Paymentmethod->updateOne($where,$map);
                    }else{
                        $this->tip = "错误！";
                    }
                }
                header('Location:applist.html');
                $this->tip = "已保存！";

            }
           
            $where = array();
            $where["id"] = $id;
            $temps= $Paymentmethod->getList($where);

            $Info = array();

            foreach($temps[0] as $k=>$v){
                $Info[$k]=$v;
            }
            $this->Info = $Info;
            $this->msgurl=C ("IMAGEURL");
            // Dump( $TranslateList);exit;
            //$this->code = $code;
        }else{
            echo "error";exit;
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }

    // /*
    //  * 删除
    //  */
    // public function del(){

    //     $id =$_REQUEST["id"];
    //     if($id){
    //         $Paymentmethod = new PaymentmethodModel();
    //         $map = array();
    //         $map["id"] = $id;
    //         $Paymentmethod->delOne($map);
    //     }
    //     header('Location:'.$_SERVER['HTTP_REFERER']);
    // }

    //图片上传方法
    protected function _upload($savePath) {
        import("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir($savePath);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize =10485760;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg();
            if($_REQUEST['ptesst']==1){
                dump($return);
            }
            //Push_data($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            //$uploadList = $uploadList[0];
            //$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
            //$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

            return $uploadList;
            //import("Extend.Library.ORG.Util.Image");
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            //Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
            //$_POST['image'] = $uploadList[0]['savename'];
        }

    }
    //图片上传封装
    public function _imgupload(){
        $filetype = "images";
        $saveUrl = $this->msgfileurl . $filetype . "/";
        $savePath = WR . $saveUrl;
        //echo $savePath;exit;
        $uploadList = $this->_upload ($savePath);
        if (!empty($uploadList)) {
            // $UserPhotoM = new PhotoModel();
            $PutOssarr = array();
            $returnurlarr = array();
            foreach ($uploadList as $k => $v) {
                $PutOssarr[] = $saveUrl . $v['savename'];
                $returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
            }
            PutOss ($PutOssarr);
            // print_r ($PutOssarr);exit;
            $return = array();
            $return["data"]["url"] = $PutOssarr[0];
            return $PutOssarr[0];
        }
    }

    public function uppershelf()
    {

        $this->name = $this->basename . '列表'; // 进行模板变量赋值

        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 12;
        $Paymentmethod = new UppershelfModel();
        //$status = include WR . "/userdata/publicvar/status.php";
        //$this->switch_type = $status["gift"]["type"];
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|isuppershelf|url'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $Paymentmethod->getListPage ($Wdata, $Page, $PageSize);
        $this->msgurl=C ("IMAGEURL");
        $this->DataList= $ret["list"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";

        $this->display();
    }

    public function updatess()
    {
        $Paymentmethod = new UppershelfModel();
        if ($_POST) {
            if ($_POST["id"]) {
                $map=array();
                $map['isuppershelf']=$_POST["isuppershelf"];
                $return=array();
                $return['id']=$_POST["id"];
                
                 $result=$Paymentmethod->updateAll ($map,$return);
                 
                 if($result){
                     $content= array(
                         'status' => 1,
                         'gstatus'=>$map['isuppershelf'],
                         'message' => '新增成功'
                     );
                     exit(json_encode ($content));
                 }else{
                     $content= array(
                         'status' => 2,
                         'message' => '修改失败！'
                     );
                     exit(json_encode ($content));
                 }
                //return json_encode ($map);
            }
        }
    }

}


