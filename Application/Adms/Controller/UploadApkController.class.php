<?php

/**
 * Created by PhpStorm.
 * User: 亮亮
 * Date: 2018/4/24
 * Time: 10:21
 */
class UploadApkController extends BaseAdmsController
{
    public function __construct ()
    {
        parent::__construct ();
        $this->basename = '上传apk'; // 进行模板变量赋值
        $this->Logofile =  "/userdate/admin/uploadapk/logo/";
        $this->Logofileurl = C("IMAGEURL").$this->Logofile;
    }

    public function lists ()
    {
        import("Sver.lib.Util.ApkParser");
        $appObj  = new Apkparser();
        
        $this->name = $this->basename . '列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 12;
        $Attr = new UploadApkModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|apkname'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $Attr->getListPage ($Wdata, $Page, $PageSize);
        foreach($ret["list"] as $k=>$v){
            $filename = WR."/userdata/apk/".$v["apkname"].".zip";
            if(file_exists($filename)){
                $appObj->open($filename);
                $newVersionName= $appObj->getVersionName();  // 版本名称
                $ret["list"][$k]["version"] = $newVersionName;
            }else{
                $ret["list"][$k]["version"] = 0;
            }
            
        }
       // exit;
        $this->DataList= $ret["list"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        //Dump($this->ListData);exit;
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display ();
    }


    public function add(){
        ini_set('max_execution_time', '1000000');
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        if($_POST){
            $err=0;
            $msg="";
            if(!$_POST["apkname"]){
                $err = 3;
                $msg.="名称不能为空<br />";
            }
            if($err==0){
                $Attr = new UploadApkModel();
                $map = array();
                    $map["apkname"] = $_POST["apkname"];
                    if($_FILES["logo"]&&$_FILES["logo"]["error"]!=4){
                        if($_FILES["logo"]["error"]==0){
                         if(!is_dir(WR."/userdata/apk/"))
                            {
                                mkdir(WR."/userdata/apk/");
                            }
                            $filename = WR."/userdata/apk/".$map["apkname"].".zip";
                            $res=move_uploaded_file($_FILES["logo"]["tmp_name"],$filename);
                            $map["logo"] =  $filename;
                        }else{
                            echo "文件上传错误,code:".$_FILES["logo"]["error"];exit;
                        }
                    }
                    $map['apkurl']='/Sver/Downapk/onlinedownapk?apkname=' . $map["apkname"];
                    $Attr->addOne($map);
                    header('Location:applist.html');
            }else{
                $this->tip = $msg;
            }
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }
    /*
	 * 删除
	 */
    public function del(){

        $code =$_REQUEST["id"];
        if($code){
            $AttrValueM = new UploadApkModel();
            $map = array();
            $map["id"] = $code;
            $AttrValue = $AttrValueM->getOne($map);
            
            if($AttrValue["logo"]){
                $command = "/bin/rm -rf " . $AttrValue["logo"] ." > /dev/null &";
                system($command);
            }
            $AttrValueM->delOne($map);
            
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }

    //修改
    public function edit(){
        ini_set('max_execution_time', '1000000');
        $this->name = $this->basename.'修改平台'; // 进行模板变量赋值
       // $status = C("status");
       // $langCode = $status["lang"]["code"];
        $GiftM  = new UploadApkModel();
        $id = $_REQUEST['id'];
        $err = 0;
        if($id){
            if($_POST){
                    if($_POST["id"]&&$err==0){
                        
                        $where=array();
                        $map = array();
                        $where['id']=$id;
                        //$where["code"] = $code;
                        $map["apkname"] =$_POST["apkname"];
                        if($_FILES["logo"]&&$_FILES["logo"]["error"]!=4){
                            import("Sver.lib.Util.ApkParser");
                            $appObj  = new Apkparser();
                            if($_FILES["logo"]["error"]==0){
                                $filename = WR."/userdata/apk/".$map["apkname"].".zip";
                                move_uploaded_file($_FILES["logo"]["tmp_name"],$filename);
                                $appObj->open($filename);
                                $newVersionName= $appObj->getVersionName();  // 版本名称
                                $map["version"] =  $newVersionName;
                                $map["logo"] =  $filename;
                            }else{
                                echo "文件上传错误,code:".$_FILES["logo"]["error"];exit;
                            }
                        }
                        $map['apkurl']='/Sver/Downapk/onlinedownapk?apkname=' . $map["apkname"];
                        $GiftM->updateOne($where,$map);
                    }

                    header('Location:applist.html');
                    $this->tip = "已保存！";
                }
        }else{
            echo "error1";exit;
        }
        $this->id = $id;
        $this->action =  __ACTION__.".html";
        $this->display("add");
    }



    //删除已上传的apk
    public function delApk(){

        if($_POST['apkname']){
            $apkname=$_POST['apkname'];
            $filename = WR."/userdata/apk/".$apkname.".zip";
            $command = "/bin/rm -rf " . $filename ." > /dev/null &";
            system($command);
            $reuslt = array(
                'status' =>'1',
                'message' => "已删除",
                'data' => '',
            );
            exit(json_encode ($reuslt));
        }else{
            $reuslt = array(
                'status' =>'0',
                'message' => "删除失败，请重新删除",
                'data' => '',
            );
            exit(json_encode ($reuslt));
        }
    }






}


