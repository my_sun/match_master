<?php
class AreaController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->basename = '地区-'; // 进行模板变量赋值
	}
	
	public function lists(){
		$this->name = $this->basename.'列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		$CountryM = new CountryModel();
		$RegionM = new RegionModel();
		$Wdata = array();
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword = $_GET['kerword'];
			if($kerword){
				$Wdata['region_name_c|upper_region']=array('like',"%".$kerword."%");
			}
		}
		$Wdata['level']=1;
		$ret = $RegionM->getListPage($Wdata,$Page,$PageSize);
		$this->DataList = $ret["list"];
		$ret["all_page"] = ceil($ret["totalCount"]/$ret["pageSize"]);
		$this->Pages = $this->GetPages($ret);
		
		//Dump($ret);exit;
		$this->get = $_GET;
		$this->action = __ACTION__.".html";
		$this->display();
	}
	public function Add(){
		$this->name = $this->basename.'添加'; // 进行模板变量赋值
		
		$this->get = $_GET;
		$this->action = __ACTION__.".html";
		$this->display();
	}	
	public function edit(){
		$this->name = $this->basename.'修改'; // 进行模板变量赋值
	}	
	public function del(){
		$this->name = $this->basename.'删除'; // 进行模板变量赋值
	}	
	public function ajax(){
		$type = $_POST["type"];
		switch ($type){
			case "updata_status":
				$id = (int)$_POST["id"];
				$status = $_POST["status"];
				if(is_integer($id)){
					$CountryM = new CountryModel();
					$CountryM->updateOne(array("id"=>$id), array("status"=>$status));
					$result["msg"] = "ok";
					$result["Succeed"] = 1;
					echo json_encode($result);exit;
				}else{
					$result["msg"] = "ID错误".$id;
					$result["Succeed"] = 0;
					echo json_encode($result);exit;
				}
				break; 
		}
		
	}	
}

?>
