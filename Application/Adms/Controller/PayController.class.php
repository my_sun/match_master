<?php

class PayController extends BaseAdmsController
{
    public function __construct()
    {
        parent::__construct();
        //echo file_get_contents("/opt/friend/UserData/logs/pay/GPA.3373-3315-3893-09832.txt");
        //exit;
        /*
        $packagename = "com.hunlianyuehui";
                $productid = "d30";
                $purchasetoken = "aelbkgnlddbdhiognenbhfhe.AO-J1OwKrjz8DEziW4WUrfLhUKGWkymXC-MdC8KdkrYwviqqIKCZ3pJhsrtSVJzabIiFePGOJmlMIsLfo872GrsayK5VQL6KgUin56H3E65Qd0iifc1PFao";
                $isauto = 1;
        $ores = PayStatus($packagename,$productid,$purchasetoken,$isauto);
        echo "GPA.3357-1582-8211-04580";
        Dump($ores);exit;
        */
    }

    public function CheckPayOne()
    {
        $this->name = '单个订单验证'; // 进行模板变量赋值
        $this->action = "/Adms/Pay/CheckPayOne.html";
        if ($_GET["kerword"]) {
            $PayDelM = new RechargeDelModel();
            $PayM = new RechargeModel();
            $where["translateid"] = $_GET["kerword"];
            $payInfo = $PayM->getOne($where);
            if (empty($payInfo)) {
                $payInfo = $PayDelM->getOne($where);
            }
            if (empty($payInfo)) {
                $msg = "数据库中没有这个订单";
            } else {
                $packagename = $payInfo["packagename"];
                $productid = $payInfo["productid"];
                $purchasetoken = $payInfo["purchasetoken"];
                $isauto = $payInfo["isauto"];
                $ores = PayStatus($packagename, $productid, $purchasetoken, $payInfo["product"], $isauto);
                if ($ores["expiryTimeMillis"]) {
                    $ores["translateid"] = $where["translateid"];
                    $msg = "";
                    foreach ($ores as $key => $v) {
                        $v = "[" . $v . "]&nbsp;&nbsp;";
                        if ($key == "autoRenewing") {
                            $v .= "(订阅是否在达到当前到期时间后自动更新。)";
                            $msg .= $key . "=>" . $v . "<br />";
                        } elseif ($key == "cancelReason") {
                            $v .= "(0：用户取消订阅 1：订单被系统取消，例如因为计费问题。)";
                            $msg .= $key . "=>" . $v . "<br />";
                        } elseif ($key == "paymentState") {
                            $v .= "(0付款等待中，1已收到付款，2免费试用)";
                            $msg .= $key . "=>" . $v . "<br />";
                        } elseif ($key == "consumptionState") {
                            $v .= "(0还有待消费，1消费)";
                            $msg .= $key . "=>" . $v . "<br />";
                        } elseif ($key == "purchaseState") {
                            $v .= "(0购买,1取消)";
                            $msg .= $key . "=>" . $v . "<br />";
                        } elseif ($key == "translateid") {
                            $msg .= $key . "=>" . $v . "<br />";
                        }

                    }
                } else {
                    Dump($ores);
                    exit;
                }

            }
            $this->error = 1;
            $this->msg = $msg;
        }
        $this->display("checkpayone");
    }

    public function Index()
    {
        $this->name = 'Login'; // 进行模板变量赋值
    }

    //检查订阅状态
    public function checkpayauto()
    {
        $this->name = '订单验证'; // 进行模板变量赋值
        $this->error = 0;
        $type = $_GET["type"] ? $_GET["type"] : "1";
        if ($type == 2) {
            $delOrder = $this->CheckPayAutod();
            if ($delOrder == 3) {
                $msg = "程序正在执行，请不要重复提交！";
            } else {
                $msg = "已删除订单";
                foreach ($delOrder as $v) {
                    $msg .= "[" . $v . "]";
                }
                $msg .= ",如果删除错误可以在订单找回中找回订单";
            }
            $this->error = 1;
            $this->msg = $msg;
        } else {
            $pidfile = WR . "/UserData/temp/CheckPayAuto.pid";

            if (file_exists($pidfile)) {
                $a = filemtime($pidfile);
                $this->error = 1;
                $this->msg = "提交时间：" . date("Y-m-d H:i:s", $a) . ",<br />程序正在执行中.......！";
            } else {
                $logsfilepath = WR . "/UserData/temp/";
                $temp_arr = dir_time($logsfilepath);
                $temp_arr2 = array();
                foreach ($temp_arr as $v) {
                    if (strstr($v, 'CheckPay') && strstr($v, '.txt')) {
                        $temp_arr2[] = $v;
                    }
                }
                $str = file_get_contents($logsfilepath . end($temp_arr2));
                $a = filemtime($logsfilepath . end($temp_arr2));
                //echo "修改时间：".date("Y-m-d H:i:s",$a);exit;
                if ($str) {
                    $this->error = 1;
                    $this->msg = "上次执行时间：" . date("Y-m-d H:i:s", $a) . ",删除的订单{$str}！";
                } else {
                    $this->error = 1;
                    $this->msg = "上次执行时间：" . date("Y-m-d H:i:s", $a) . ",没有退款订单！";
                }
            }

        }
        $this->display("CheckPayAuto");
    }

    private function checkpayautod()
    {
        ini_set('max_execution_time', '100000000');
        $pidfile = WR . "/UserData/temp/CheckPayAuto.pid";

        if (file_exists($pidfile)) {
            return 3;
        }
        touch($pidfile);


        $PayM = new PayModel();
        $pageNum = $_GET['pageNum'] ? $_GET['pageNum'] : 1;
        $pageSize = $_GET['pageSize'] ? $_GET['pageSize'] : 200;
        $Wdata["paytime"] = array("EGT", time() - 60 * 60 * 49);
        $Wdata["packagename"] = "com.hunlianyuehui";
        $Wdata["ischeck"] = "1";
        $list = $PayM->getListPage($Wdata, $pageNum, $pageSize);
        //Dump($list);exit;
        $list = $list["list"];


        $ores_status = array();
        $del_order = array();
        foreach ($list as $k => $v) {
            $isdel = 0;
            $packagename = $v["packagename"];
            $productid = $v["productid"];
            $purchasetoken = $v["purchasetoken"];
            $isauto = $v["isauto"];
            $ores = PayStatus($packagename, $productid, $purchasetoken, $isauto);

            if ($ores == 400) {
                $isdel = 1;
            } else {
                if ($v["isauto"] == 1) {
                    if ($ores["paymentState"] == NULL) {
                        $isdel = 1;
                    }
                } else {
                    if ($ores["purchaseState"] == 1) {
                        $isdel = 1;
                    }
                }
            }
            if ($isdel == 1) {
                $del_order[] = $v["translateid"];

                $oid = $v['id'];//translateid
                $PayM = new PayModel();
                $payInfo = $PayM->getOne(array('id' => $oid));
                $PeiliaoMoneyM = new PeiliaoMoneyModel();
                $PeiliaoMoneyInfo = $PeiliaoMoneyM->getOne(array('translateid' => $v["translateid"]));
                if ($payInfo) {
                    $PayDelM = new PayDelModel();
                    unset($payInfo["id"]);
                    $payInfo["memo"] = "用户取消订单";
                    $PayDelM->addOne($payInfo);
                    $PayM->delOne(array('id' => $oid));
                    $this->get_dayNum($payInfo['uid'], 1);
                }
                if ($PeiliaoMoneyInfo) {
                    $PeiliaoMoneyDelM = new PeiliaoMoneyDelModel();
                    unset($PeiliaoMoneyInfo["id"]);
                    $PeiliaoMoneyDelM->addOne($PeiliaoMoneyInfo);
                    $PeiliaoMoneyM->delOne(array('translateid' => $v["translateid"]));
                }

            }
            $ores["isdel"] = $isdel;
            $ores["isauto"] = $v["isauto"];
            $ores_status[$v["translateid"]] = $ores;
            usleep(10);
        }
        unlink($pidfile);
        $logsfile = WR . "/UserData/temp/CheckPay72" . date("Y-m-d H:i:s", time()) . ".txt";
        file_put_contents($logsfile, implode(",", $del_order));
        return $del_order;
        //Dump($ores_status);exit;
    }

    public function checkpay()
    {
        $this->name = '订单验证'; // 进行模板变量赋值
        $this->error = 0;
        $type = $_GET["type"] ? $_GET["type"] : "1";
        if ($type == 2) {
            $delOrder = $this->CheckPay72();
            if ($delOrder == 3) {
                $msg = "程序正在执行，请不要重复提交！";
            } else {
                $msg = "已删除订单";
                foreach ($delOrder as $v) {
                    $msg .= "[" . $v . "]";
                }
                $msg .= ",如果删除错误可以在订单找回中找回订单";
            }
            $this->error = 1;
            $this->msg = $msg;
        } elseif ($type == 3) {
            //订阅状态验证
            $product = 10008;
            //$this->CheckPayDingyue();

        } else {
            $pidfile = WR . "/UserData/temp/CheckPay/CheckPay.pid";

            if (file_exists($pidfile)) {
                $a = filemtime($pidfile);
                $this->error = 1;
                $this->msg = "提交时间：" . date("Y-m-d H:i:s", $a) . ",<br />程序正在执行中.......！";
            } else {
                $logsfilepath = WR . "/UserData/temp/CheckPay/";
                $temp_arr = dir_time($logsfilepath);
                $temp_arr2 = array();
                foreach ($temp_arr as $v) {
                    if (strstr($v, 'CheckPay') && strstr($v, '.txt')) {
                        $temp_arr2[] = $v;
                    }
                }
                $str = file_get_contents($logsfilepath . end($temp_arr2));
                $a = filemtime($logsfilepath . end($temp_arr2));
                //echo "修改时间：".date("Y-m-d H:i:s",$a);exit;
                if ($str) {
                    $this->error = 1;
                    $this->msg = "上次执行时间：" . date("Y-m-d H:i:s", $a) . ",删除的订单{$str}！";
                } else {
                    $this->error = 1;
                    $this->msg = "上次执行时间：" . date("Y-m-d H:i:s", $a) . ",没有退款订单！";
                }
            }

        }
        $this->display("CheckPay");
    }

    public function checkpaydingyue()
    {
        ini_set('max_execution_time', '100000000');

        //清空服务器缓存
      //  $web_root = WR . "/userdata/cache";
        //$command = "/bin/rm -rf " . $web_root . " > /dev/null &";
       // system($command);

        $pidfile = WR . "/userdata/temp/CheckPay/CheckPayDingyue.pid";
        $logfile = WR . "/userdata/temp/CheckPay/CheckPayDingyuelog.txt";
        if (file_exists($pidfile)) {
            return 3;
        }
        touch($pidfile);
        $str = "----------" . date("Y-m-d H:i:s", time()) . "开始执行--------------";
        file_put_contents($logfile, $str . "\r\n", FILE_APPEND);
        $PayM = new RechargeModel();
        $Wdata["paytime"] = array("ELT", time() - 60 * 60 * 24 * 6);
        //$Wdata["product"]="10008";
        $Wdata["isauto"] = "1";
        $Wdata["ischeck"] = "1";
        $Wdata['packagename'] = array('exp', 'is not null');
        $list = $PayM->getList($Wdata);
        //echo $PayM->getLastSql();
        //Dump($list);exit;


        foreach ($list as $k => $v) {
                $BaseAdmsC = new BaseAdmsController;
                $BaseAdmsC->get_user($v["uid"], 1);
                $userinfo = $BaseAdmsC->get_user($v["uid"]);
                //$list[$k]["phonetime"]=date("Y-m-d H:i:s",$v["paytime"]);
                if ($userinfo["vip"] < 1) {


                    $packagename = $v["packagename"];
                    $productid = $v["productid"];
                    $purchasetoken = $v["purchasetoken"];
                    $isauto = 1;
                    $ores = PayStatus($packagename, $productid, $purchasetoken, $v["product"], $isauto);
                    if ($ores["cancelReason"] === 0 || $ores["cancelReason"] === 1) {
                        //取消订阅的
                        $PayM->updateOne(array("id" => $v["id"]), array('ischeck' => 0));
                        $str = "取消订阅：" . $v["translateid"] . "|" . date("Y-m-d H:i:s", time());
                        file_put_contents($logfile, $str . "\r\n", FILE_APPEND);

                    } else {
                        if ($ores["paymentState"] === 1) {

                            $timenow = time();

                            $trlike = array();
                            $translateid = $v["translateid"];
                            $trlike['translateid'] = array('like', $translateid . '..%');
                            $temparr = $PayM->getList($trlike);
                            $newkey = count($temparr);
                            $new_pay = array();
                            $new_pay["translateid"] = $translateid . ".." . $newkey;
                            $temparr1 = $PayM->getOne($new_pay);

                            if (empty($temparr1)) {
                                $new_pay = $v;
                                $viptime = time() + $v["days"] * 24 * 60 * 60;
                                $new_pay["translateid"] = $translateid . ".." . $newkey;
                                $new_pay["phonetime"] = date("Y-m-d H:i:s", $timenow);
                                $new_pay["paytime"] = $timenow;
                                $new_pay["isauto"] = 0;
                                $new_pay["ischeck"] = 0;
                                $new_pay["vipgold"] = $viptime;
                                unset($new_pay["id"]);

                                $PayM->addOne($new_pay);

                                $uid = $v["uid"];
                                $UserBaseM = new UserBaseModel();

                                $UserBaseM->updateOne(array("uid" => $uid), array("viptime" => $viptime));
                                $redis = $this->redisconn();
                                //删除用户缓存
                                $keyexit = $redis->del('getuser-' . $uid);

                                $str = "新增订单：" . $new_pay["translateid"] . "|" . date("Y-m-d H:i:s", time());
                                file_put_contents($logfile, $str . "\r\n", FILE_APPEND);
                            }
                        } else {
                            $str = "付款等待：" . $v["translateid"] . "|" . date("Y-m-d H:i:s", time());
                            file_put_contents($logfile, $str . "\r\n", FILE_APPEND);
                        }
                    }
                }



            //Dump($userinfo);exit;
        }
        $str = "----------" . date("Y-m-d H:i:s", time()) . "执行完成--------------";
        file_put_contents($logfile, $str . "\r\n", FILE_APPEND);
        unlink($pidfile);
        return 1;
    }

    public function checkpay72()
    {
        ini_set('max_execution_time', '100000000');
        $pidfile = WR . "/userdata/temp/CheckPay/CheckPay.pid";

        if (file_exists($pidfile)) {
            return 3;
        }
        touch($pidfile);


        $PayM = new RechargeModel();
        $pageNum = $_GET['pageNum'] ? $_GET['pageNum'] : 1;
        $pageSize = $_GET['pageSize'] ? $_GET['pageSize'] : 200;
        $Wdata["paytime"] = array("EGT", time() - 60 * 60 * 49);
        $Wdata['packagename'] = array('exp', 'is not null');
        $Wdata["ischeck"] = "1";
        $list = $PayM->getListPage($Wdata, $pageNum, $pageSize);
        //Dump($list);exit;
        $list = $list["list"];
        /*
        $packagename = "com.hunlianyuehui";
                $productid = "d30";
                $purchasetoken = "pidjdmpielekcdnjmlpdahih.AO-J1OwpvAR_le-dXkmeN71mYBcX2SyaG-djBpGQ-41Lcx2Y1Wk435n9B9opbPTVGv-21Cxb5iBIBWpGkGqSsPT5LOodjWayNRox3AE8zikD9wppyj4rXos";
                $isauto = 1;
        $ores = PayStatus($packagename,$productid,$purchasetoken,$isauto);
        echo "GPA.3326-9846-9730-95205";
        Dump($ores);exit;
        */
        $allproduct=array();
        $orderdata=M('order_switch')->select();
        foreach($orderdata as $k1=>$v1){
            $allproduct[$k1]=$v1['product'];
        }

        $ores_status = array();
        $del_order = array();
        foreach ($list as $k => $v) {
            $isdel = 0;
            if(in_array($v['product'],$allproduct)){
                foreach($orderdata as $k2=>$v2){
                    if($v['product']==$v2['product']){
                        if($v2['isopenin']==1){
                            $packagename = $v["packagename"];
                            $productid = $v["productid"];
                            $purchasetoken = $v["purchasetoken"];
                            $isauto = $v["isauto"];
                            $ores = PayStatus($packagename, $productid, $purchasetoken, $v["product"], $isauto);

                            if ($ores == 400) {
                                $isdel = 1;
                            } else {
                                if ($v["isauto"] == 1) {
                                    if ($ores["paymentState"] == NULL) {
                                        $isdel = 1;
                                    }
                                } else {
                                    if ($ores["purchaseState"] == 1) {
                                        $isdel = 1;
                                    }
                                }
                            }
                        }else{
                            $isdel = 0;
                        }
                        break;
                    }

                }
            }else{
                $packagename = $v["packagename"];
                $productid = $v["productid"];
                $purchasetoken = $v["purchasetoken"];
                $isauto = $v["isauto"];
                $ores = PayStatus($packagename, $productid, $purchasetoken, $v["product"], $isauto);

                if ($ores == 400) {
                    $isdel = 1;
                } else {
                    if ($v["isauto"] == 1) {
                        if ($ores["paymentState"] == NULL) {
                            $isdel = 1;
                        }
                    } else {
                        if ($ores["purchaseState"] == 1) {
                            $isdel = 1;
                        }
                    }
                }
            }

            if ($isdel == 1) {
                $del_order[] = $v["translateid"];

                $oid = $v['id'];//translateid
                file_get_contents("http://127.0.0.1:81/Adms/Pay/delorder?oid=" . $oid . "&token=99123");
            }
            $ores["isdel"] = $isdel;
            $ores["isauto"] = $v["isauto"];
            $ores_status[$v["translateid"]] = $ores;
            usleep(10);
        }
        unlink($pidfile);
        $logsfile = WR . "/userdata/temp/CheckPay/CheckPay" . date("Y-m-d H:i:s", time()) . ".txt";
        file_put_contents($logsfile, implode(",", $del_order));
        return $del_order;
        //Dump($ores_status);exit;
    }

    public function Payblacklist()
    {
        $this->name = 'Login'; // 进行模板变量赋值
    }

    public function addPayblacklist()
    {
        $uid = $_GET["uid"];
        if ($uid) {
            $UserBaseM = new UserBaseModel();
            $UserInfo = $UserBaseM->getOne(array("uid" => $uid));
            $date = array();
            $date["uid"] = $UserInfo["uid"];
            $date["pid"] = $UserInfo["pid"];
            $PayblacklistM = new PayblacklistModel();
            $DataList = $PayblacklistM->addOne($date);

        }
        header('Location:' . $_SERVER['HTTP_REFERER']);
    }

    public function paylist()
    {
        $this->name = '订单列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $where = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $where['uid|translateid|product'] = array('like', "%" . $_GET['kerword'] . "%");
        }
        if (isset($_GET['dateStart']) && $_GET['dateStart'] != "")
            $where["phonetime"] = array(array('egt', $_GET["dateStart"]), array('elt', $_GET["dateEnd"]));
        if (isset($_GET['ischeck']) && $_GET['ischeck'] != "") {
            $where["ischeck"] = $_GET["ischeck"];
            $where["isauto"] = 1;
        }
        if (isset($_GET['isrecharge']) && $_GET['isrecharge'] != "") {
            $where["status"] = $_GET["isrecharge"];
        }
        $RechargeM = new RechargeModel();
        $DataList = $RechargeM->getListPage($where, $Page, $PageSize);
        foreach ($DataList["list"] as $k => $v) {
            $DataList["list"][$k]["user"] = $this->get_user($v["uid"]);
            $DataList["list"][$k]["money"] = $DataList["list"][$k]["money"] / 100;
        }
        $this->action = "/Adms/Pay/paylist.html";
        $this->Pages = $this->GetPages($DataList);
        $this->DataList = $DataList;
        $this->get = $_GET;
        $this->display("paylist");

    }

    public function paylistdel()
    {
        $this->name = '订单找回列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $where = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $where['uid'] = array('like', "%" . $_GET['kerword'] . "%");
        }
        if (isset($_GET['dateStart']) && $_GET['dateStart'] != "")
            $where["phonetime"] = array(array('egt', $_GET["dateStart"]), array('elt', $_GET["dateEnd"]));

        $PayDelM = new RechargeDelModel();
        $DataList = $PayDelM->getListPage($where, $Page, $PageSize);
        //Dump($DataList);exit;
        foreach ($DataList["list"] as $k => $v) {
            $DataList["list"][$k]["user"] = $this->get_user($v["uid"]);
            $DataList["list"][$k]["money"] = $DataList["list"][$k]["money"] / 100;
        }
        $this->action = "/Adms/Pay/paylistdel.html";
        $this->Pages = $this->GetPages($DataList);
        $this->get = $_GET;
        $this->DataList = $DataList;
        $this->display("paylistdel");

    }

    //订单取消，删除相关充值信息以及礼物、魅力值信息
    public function delorder()
    {
        $oid = $_GET['oid'] ? $_GET['oid'] : 183;
        $PayM = new RechargeModel();
        $userM = new UserBaseModel();
        $PMoneyM = new PMoneyModel();
        $PMoneyDelM = new PMoneyDelModel();
        $payInfo = $PayM->getOne(array('id' => $oid));
        if ($payInfo) {
            //删除订单、VIP以及金币开始
            $PayDelM = new RechargeDelModel();
            unset($payInfo["id"]);
            $payInfo["memo"] = "用户取消订单";
            $PayDelM->addOne($payInfo);
            $PayM->delOne(array('id' => $oid));
            //订单是用户给自己充值会员
            if ($payInfo['type'] == 1) {
                $lviptime = $userM->where(array('uid' => $payInfo['uid']))->getField('viptime');
                $rviptime = $lviptime - (($payInfo['days']+1)* 24 * 60 * 60);
                $rest = $userM->updateOne(array('uid' => $payInfo['uid']), array('viptime' => $rviptime,'vipgrade'=>0));
                //清除缓存
                $this->set_user_field($payInfo['uid'], 'viptime', $rviptime);
                $this->set_user_field($payInfo['uid'], 'vip', 0);
                $this->set_user_field($payInfo['uid'], 'vipgrade', 0);

                //扣除对应聊天用户的魅力值
                $pmoney = M('p_money');

                $res_pmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>1))->find();
                if($res_pmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_pmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] -$res_pmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] - $res_pmoney['money'];
                    $mmoney->where(array('uid' =>$res_pmoney['uid']))->save($resm);

                    $id=$res_pmoney["id"];
                    unset($res_pmoney["id"]);
                    $PMoneyDelM->addOne($res_pmoney);
                    $PMoneyM->delOne(array('id'=>$id));

                }
                //扣除对应聊天用户推荐人的魅力值
                $res_tmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>8))->find();
                if($res_tmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_tmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] -$res_tmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] - $res_tmoney['money'];
                    $mmoney->where(array('uid' =>$res_tmoney['uid']))->save($resm);

                    $id=$res_tmoney["id"];
                    unset($res_tmoney["id"]);
                    $PMoneyDelM->addOne($res_tmoney);
                    $PMoneyM->delOne(array('id'=>$id));
                }
                //扣除对应聊天用户推荐人代理商的魅力值
                $res_dmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>9))->find();
                if($res_dmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_dmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] -$res_dmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] - $res_dmoney['money'];
                    $mmoney->where(array('uid' =>$res_dmoney['uid']))->save($resm);

                    $id=$res_dmoney["id"];
                    unset($res_dmoney["id"]);
                    $PMoneyDelM->addOne($res_dmoney);
                    $PMoneyM->delOne(array('id'=>$id));
                }

            } //订单是用户给他人充值会员
            elseif ($payInfo['type'] == 2) {
                $lviptime = $userM->where(array('uid' => $payInfo['to_uid']))->getField('viptime');
                $rviptime = $lviptime - ($payInfo['days'] * 24 * 60 * 60);
                $userM->updateOne(array('uid' => $payInfo['to_uid']), array('viptime' => $rviptime,'vipgrade'=>0));
                //清除缓存
                $this->set_user_field($payInfo['to_uid'], 'viptime', $rviptime);
                $this->set_user_field($payInfo['to_uid'], 'vip', 0);
                $this->set_user_field($payInfo['to_uid'], 'vipgrade', 0);
                //扣除对应聊天用户的魅力值
                $pmoney = M('p_money');
                $res_pmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>1))->find();
                if($res_pmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_pmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] -$res_pmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] - $res_pmoney['money'];
                    $mmoney->where(array('uid' =>$res_pmoney['uid']))->save($resm);

                    $id=$res_pmoney["id"];
                    unset($res_pmoney["id"]);
                    $PMoneyDelM->addOne($res_pmoney);
                    $PMoneyM->delOne(array('id'=>$id));
                }
                //扣除对应聊天用户推荐人的魅力值
                $res_tmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>8))->find();
                if($res_tmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_tmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] -$res_tmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] - $res_tmoney['money'];
                    $mmoney->where(array('uid' =>$res_tmoney['uid']))->save($resm);

                    $id=$res_tmoney["id"];
                    unset($res_tmoney["id"]);
                    $PMoneyDelM->addOne($res_tmoney);
                    $PMoneyM->delOne(array('id'=>$id));
                }
                //扣除对应聊天用户推荐人代理商的魅力值
                $res_dmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>9))->find();
                if($res_dmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_dmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] -$res_dmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] - $res_dmoney['money'];
                    $mmoney->where(array('uid' =>$res_dmoney['uid']))->save($resm);
                    $id=$res_dmoney["id"];
                    unset($res_dmoney["id"]);
                    $PMoneyDelM->addOne($res_dmoney);
                    $PMoneyM->delOne(array('id'=>$id));
                }
            }
            //订单是用户给自己充值金币
            elseif ($payInfo['type'] == 3) {
                $lgold = $userM->where(array('uid' => $payInfo['uid']))->getField('gold');
                $rgold = $lgold - $payInfo['days'];
                $userM->updateOne(array('uid' => $payInfo['uid']), array('gold' => $rgold));
                //清除缓存
                $this->set_user_field($payInfo['uid'], 'gold', $rgold);

                //删除用户所送出的礼物以及魅力值信息
                if ($rgold < 0) {
                    //超出的金币即为发红包或者送礼物的金币,先从发送红包中扣除，然后从发送礼物魅力值扣除
                    $ret = array();
                    $deltime = time();
                    $cgold = abs($rgold);//将负值转变成为正值
                    $ptime = $payInfo['paytime'];
                    $ret['paytime'] = array('gt', $ptime);
                    $ret['uid'] = $payInfo['uid'];
                    $redpacketM = M('red_packet_consume');
                    $resredpkt = $redpacketM->where($ret)->order('paytime desc')->select();
                    if ($resredpkt) {
                        foreach ($resredpkt as $k => $v) {
                            if ($cgold > 0) {
                                //扣除魅力值
                                $cgold = $cgold - $v['money'];
                                $money = $v['money'] * 0.65 * 0.2;
                                $mmoney = M('m_money');
                                $mmon = $mmoney->where(array('uid' => $v['touid']))->find();
                                $resm = array();
                                $resm['totalmoney'] = $mmon['totalmoney'] - $money;
                                $resm['leftmoney'] = $mmon['leftmoney'] - $money;
                                $mmoney->where(array('uid' => $v['touid']))->save($resm);
                                $ConsumeM = M('consume');
                                //删除该用户送红包的消费记录并备份
                                $res = $ConsumeM->where(array('paytime' => $v['paytime']))->find();
                                if ($res) {
                                    $consumeD = M('consume_del');
                                    unset($res["paytime"]);
                                    $res['deltime'] = $deltime;
                                    $consumeD->add($res);
                                    $ConsumeM->where(array('paytime' => $v['paytime']))->delete();
                                }


                                $pmoneyM = M('p_money');
                                //删除被送红包用户所增长魅力值记录，并备份
                                $resp = $pmoneyM->where(array('paytime' => $v['paytime']))->find();
                                if ($resp) {
                                    $pmoneyD = M('p_money_del');

                                    $resp['paytime'] = $deltime;
                                    $pmoneyD->add($resp);
                                    $pmoneyM->where(array('paytime' => $v['paytime']))->delete();
                                }
                            } else {
                                $cgold = 0;
                                break;
                            }
                        }
                    }
                    //将金币从接收礼物的用户账户中扣除，魅力值扣除
                    if ($cgold > 0) {
                        $giftarr = array();
                        $giftM = M('gift_list');
                        $giftarr['uid'] = $payInfo['uid'];
                        $giftarr['time'] = array('gt', $ptime);
                        $resgift = $giftM->where($giftarr)->order('time desc')->select();
                        if ($resgift) {

                            foreach ($resgift as $k1 => $v1) {
                                if ($cgold > 0) {
                                    $cgold = $cgold - $v1['totalprice'];
                                    //扣除魅力值
                                    $money = $v1['totalprice'] * 0.65 * 0.2;
                                    $mmoney = M('m_money');
                                    $mmon = $mmoney->where(array('uid' => $v1['touid']))->find();
                                    $resm = array();
                                    $resm['totalmoney'] = $mmon['totalmoney'] - $money;
                                    $resm['leftmoney'] = $mmon['leftmoney'] - $money;
                                    $mmoney->where(array('uid' => $v1['touid']))->save($resm);
                                    $ConsumeM = M('consume');
                                    //删除该用户送礼物的消费记录并备份
                                    $res = $ConsumeM->where(array('paytime' => $v1['time']))->find();
                                    if ($res) {
                                        $consumeD = M('consume_del');
                                        unset($res["paytime"]);
                                        $res['deltime'] = $deltime;
                                        $consumeD->add($res);
                                        $ConsumeM->where(array('paytime' => $v1['time']))->delete();
                                    }
                                    $pmoneyM = M('p_money');
                                    //删除被送礼物用户所增长魅力值记录，并备份
                                    $resp = $pmoneyM->where(array('paytime' => $v1['time']))->find();
                                    if ($resp) {
                                        $pmoneyD = M('p_money_del');

                                        $resp['paytime'] = $deltime;
                                        $pmoneyD->add($resp);
                                        $pmoneyM->where(array('paytime' => $v1['time']))->delete();
                                    }
                                } else {
                                    $cgold = 0;
                                    break;
                                }
                            }
                        }
                    }

                }
            } //订单是用户给别人充值金币
            else {
                $lgold = $userM->where(array('uid' => $payInfo['to_uid']))->getField('gold');
                $rgold = $lgold - $payInfo['days'];
                $userM->updateOne(array('uid' => $payInfo['to_uid']), array('gold' => $rgold));
                //清除缓存
                $this->set_user_field($payInfo['to_uid'], 'gold', $rgold);
                //$this->get_user($payInfo['to_uid'],1);
            }
            //删除删除订单、VIP以及金币结束
        }

        echo '<script language="JavaScript">
			window.location.href="' . $_SERVER['HTTP_REFERER'] . '";
			</script>';

    }

    public function haiyuanorder()
    {
        $oid = $_GET['oid'] ? $_GET['oid'] : 183;
        $PayM = new RechargeModel();
        $userM = new UserBaseModel();
        $PayDelM = new RechargeDelModel();
        $payInfo = $PayDelM->getOne(array('id' => $oid));
        if ($payInfo) {
            unset($payInfo["id"]);
            unset($payInfo["memo"]);
            $PayM->addOne($payInfo);
            $PayDelM->delOne(array('id' => $oid));

            //订单是用户给自己充值会员
            if ($payInfo['type'] == 1) {
                $lviptime = $userM->where(array('uid' => $payInfo['uid']))->getField('viptime');
                $rviptime = $lviptime + ($payInfo['days'] * 24 * 60 * 60);
                $rest = $userM->updateOne(array('uid' => $payInfo['uid']), array('viptime' => $rviptime,'vipgrade'=>1));
                //更新缓存
                if ($rest) {
                    $this->get_user($payInfo['uid'], 1);
                }
                //增添对应聊天用户的魅力值
                $pmoney = M('p_money');
                $res_pmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>1))->find();
                if($res_pmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_pmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] +$res_pmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] + $res_pmoney['money'];
                    $mmoney->where(array('uid' =>$res_pmoney['uid']))->save($resm);
                }
                //增添对应聊天用户推荐人的魅力值
                $res_tmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>8))->find();
                if($res_tmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_tmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] +$res_tmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] + $res_tmoney['money'];
                    $mmoney->where(array('uid' =>$res_tmoney['uid']))->save($resm);
                }
                //增添对应聊天用户推荐人代理商的魅力值
                $res_dmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>9))->find();
                if($res_dmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_dmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] +$res_dmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] + $res_dmoney['money'];
                    $mmoney->where(array('uid' =>$res_dmoney['uid']))->save($resm);
                }
            } //订单是用户给他人充值会员
            elseif ($payInfo['type'] == 2) {
                $lviptime = $userM->where(array('uid' => $payInfo['to_uid']))->getField('viptime');
                $rviptime = $lviptime + ($payInfo['days'] * 24 * 60 * 60);
                $rest = $userM->updateOne(array('uid' => $payInfo['to_uid']), array('viptime' => $rviptime,'vipgrade=>1'));
                //清除缓存
                if ($rest) {
                    $this->get_user($payInfo['to_uid'], 1);
                }
                //增添对应聊天用户的魅力值
                $pmoney = M('p_money');
                $res_pmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>1))->find();
                if($res_pmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_pmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] +$res_pmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] + $res_pmoney['money'];
                    $mmoney->where(array('uid' =>$res_pmoney['uid']))->save($resm);
                }
                //增添对应聊天用户推荐人的魅力值
                $res_tmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>8))->find();
                if($res_tmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_tmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] +$res_tmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] + $res_tmoney['money'];
                    $mmoney->where(array('uid' =>$res_tmoney['uid']))->save($resm);
                }
                //增添对应聊天用户推荐人代理商的魅力值
                $res_dmoney=$pmoney->where(array('translateid'=>$payInfo['translateid'],'type'=>9))->find();
                if($res_dmoney){
                    $mmoney = M('m_money');
                    $mmon = $mmoney->where(array('uid' => $res_dmoney['uid']))->find();
                    $resm = array();
                    $resm['totalmoney'] = $mmon['totalmoney'] +$res_dmoney['money'];
                    $resm['leftmoney'] = $mmon['leftmoney'] + $res_dmoney['money'];
                    $mmoney->where(array('uid' =>$res_dmoney['uid']))->save($resm);
                }
            } //订单是用户给自己充值金币
            elseif ($payInfo['type'] == 3) {
                $lgold = $userM->where(array('uid' => $payInfo['uid']))->getField('gold');
                $rgold = $lgold + $payInfo['days'];
                $rest = $userM->updateOne(array('uid' => $payInfo['uid']), array('gold' => $rgold));
                //清除缓存
                if ($rest) {
                    $this->set_user_field($payInfo['uid'], 'gold', $rgold);
                }
            } //订单是用户给别人充值金币
            else {
                $lgold = $userM->where(array('uid' => $payInfo['to_uid']))->getField('gold');
                $rgold = $lgold + $payInfo['days'];
                $rest = $userM->updateOne(array('uid' => $payInfo['to_uid']), array('gold' => $rgold));
                //清除缓存
                if ($rest) {
                    $this->set_user_field($payInfo['to_uid'], 'gold', $rgold);
                }

            }
        }

        echo '<script language="JavaScript">
			window.location.href="' . $_SERVER['HTTP_REFERER'] . '";
			</script>';
        //header('Location:/adm/orderlist');

    }
}

?>
