<?php
class UserController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->Lan = $this->LangSet("zh-cn");
		//$this->sendsysmsg("383712", "系统通知");
	}
	/**
	 * 获取自定义数组内容
	 */
	protected function get_attrconf($showtype=1){
	    $path = WR.'/userdata/cache/attr/';
	    $cache_name = 'attradms'.$showtype;
	    //F($cache_name,NULL,$path);
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $Attr = new AttrModel();
	        $AttrValue = new AttrValueModel();
	        $AttrList = $Attr->getList(array("showtype"=>$showtype));
	        $res = array();
	        foreach ($AttrList as $k=>$v){
	            $option = array();
	            $AttrValueList = $AttrValue->getList(array("uppercode"=>$v["code"]));
	            foreach ($AttrValueList as $attrv){
	                $option[$attrv["id"]] = $attrv["code"];
	            }
	            $res[] = array(
	                "field"=>$v["code"],
	                "name"=>$v["code"],
	                "type"=>$v["type"],
	                "option"=>$option
	            );
	        }
	        F($cache_name,$res,$path);
	    }
	    
	    foreach ($res as $k=>$v){
	        $res[$k]["name"] = $this->L($res[$k]["name"]);
	        foreach($v["option"] as $k1=>$v1){
	            $res[$k]["option"][$k1] = $this->L($res[$k]["option"][$k1]);
	        }
	    }
	    return $res;
	}
	public function L($name=null, $value=null) {
	    return $this->Lan->L($name,$value);
	}
	public function LangSet($langs){
	   
	    $file   =  C("LANG_PATH").$langs.'.txt';
	    
	    $cachefile   =  C("LANG_PATH")."cache/".$langs.'.php';
	    
	    if(!file_exists($cachefile)||(filemtime($file)>filemtime($cachefile))){
	        $TranslateM = new TranslateModel();
	        $Wdata = array();
	        $Wdata["lang"]=$langs;
	        $ret = $TranslateM->getList($Wdata);
	        $temp = array();
	        foreach ($ret as $k=>$v){
	            $temp[$v["code"]]=$v['content'];
	        }
	        
	        $temp = "<?php return ".var_export($temp, true).";";
	        mkdirs(dirname($cachefile));
	        file_put_contents($cachefile, $temp);
	    }
	    
	    import("Api.lib.Behavior.CheckLangBehavior");
	    $lang = new CheckLangBehavior();
	    
	    $lang->run($langs);
	   
	    return $lang;
	}

	public function adduser(){
        $this->name = '添加用户';
        if($_POST["gender"]) {
            if ($_POST["age"]) {
                $basedata['age'] = $_POST["age"] ? $_POST["age"] : 1;
            }
            if ($_POST["gender"]) {
                $basedata['gender'] = $_POST["gender"] ? $_POST["gender"] : 1;
            }

            if (isset($_POST['nickname'])) {
                $basedata['nickname'] = $_POST['nickname'];
            } else {
                //注册时  男性用户默认昵称为“男士”女的默认为“女士”
                if ($basedata['gender'] == 1) {
                    $basedata['nickname'] = $this->L("NANSHENG");
                } else {
                    $basedata['gender'] = 2;
                    $basedata['nickname'] = $this->L("NVSHENG");
                }
            }
            $basedata['user_type'] = $_POST["usertype"] ? $_POST["usertype"] : 1;
            if($basedata['user_type']==3||$basedata['user_type']==9||$basedata['user_type']==10||$basedata['user_type']==11){
                $basedata['vipgrade']=rand(2,3);
                $basedata['vip']=rand(1000,2000);
                $basedata['gold']=rand(30,2000);
                $basedata['viptime']=time()+60*60*24*$basedata['vip'];
                $basedata['puid']=$_POST['puid']?$_POST['puid']:10000;
                if ($_POST["issuiliao"]==1) {
                    $basedata['issuiliao'] =1;
                }
                //删除类型3用户缓存，重新读取数据库
                $redis = $this->redisconn();
                $redis->del('get_yingxiao_user-zx-status-1');
                $redis->del('get_yingxiao_user-zx-status-2');
			}

            $basedata['product'] = $_POST["product"] ? $_POST["product"] : 23110;

            $User_baseM = new UserBaseModel();
            $timenow = time();
            $basedata['password'] = rand(20000, 99999);
            $basedata['regtime'] = $timenow;
            $basedata['logintime'] = $timenow;
            $data=$User_baseM->addOne($basedata);
            if($data){
                if($basedata['product'] == 25100 && $basedata['gender']==2){

                    M('compere_info')->add(array("uid"=>$data));
                }
                $error=1;
                $msg="添加成功！";
            }
        }
        $this->error=$error;
        $this->msg=$msg;
        $this->action = "/Adms/User/adduser.html";
        $this->display("adduser");


	}
	//添加第三方推荐用户
	public function add_three_user(){
		if($_GET['id']){
            $this->name = '修改第三方用户';
            $msglist=M('admin_three_user')->where(array('id'=>$_GET['id']))->find();
            $this->msglist=$msglist;
		}else{
            $this->name = '添加第三方用户';
		}

        if($_POST["name"]) {
        	$basedata['name'] = $_POST['name'];
            $basedata['password'] =$_POST["password"]?$_POST["password"]:123456;//初始密码
            $basedata['productids'] =implode("|",$_POST["productids"]);
            $basedata['type'] =$_POST['type'];
            if($_POST["id"]){
                $data=M('admin_three_user')->where(array('id'=>$_POST["id"]))->save($basedata);
            }else{
                $data=M('admin_three_user')->data($basedata)->add();
			}
            if($data){
            	if($_POST["id"]){
                    $msg="修改成功！";
                    redirect('three_userlist.html');
				}else{
                    $msg="添加成功！";
				}
                $error=1;

            }
        }
        //获取平台号
        $productids=M('admin_three_user')->where(array('id'=>$_GET['id']))->getField('productids');
        $Info["productids"] =explode("|",$productids);

        $getproduct1=M('products')->select();
        foreach($getproduct1 as $k=>$v){
            $getproduct[$k]['id']=$v['product'];
            if(in_array((int)$v['product'],$Info["productids"])){
                $getproduct[$k]["checked"] = "checked=\"checked\"";
            }
        }
        $this->getproduct=$getproduct;
        $this->error=$error;
        $this->msg=$msg;

        $this->action = "/Adms/User/add_three_user.html";
        $this->display();

	}
	//第三方用户列表
    public function three_userlist(){
        $this->name = '第三方用户列表';
        $ret = M('admin_three_user')->select();
        //添加所属用户魅力值总和

        foreach($ret as $k=>$v){
            $ret[$k]['allmoney']=0;
            $ret[$k]['leftmoney']=0;
            $Wdata=array();
            //$uids[$k]=$v['id'];
            $Wdata['puid']=$v['id'];
           $uids[$k]=M('user_base')->where($Wdata)->getField('uid',true);

           foreach($uids[$k] as $k1=>$v1){
               $allmoney[$k1]=M('m_money')->where(array('uid'=>$v1))->find();
               $ret[$k]['allmoney']=$ret[$k]['allmoney']+$allmoney[$k1]['totalmoney'];
               $ret[$k]['leftmoney']=$ret[$k]['leftmoney']+$allmoney[$k1]['leftmoney'];
           }
        }
        $this->DataList = $ret;
        $this->get = $_GET;
        $this->action = "/Adms/User/three_userlist.html";
        $this->display();
    }
	public function AddPay(){
		$this->name = '用户充值'; // 进行模板变量赋值
		$error = 0;
		if($_POST["uid"]){
			$uid=$_POST["uid"];
			$type=$_POST["type"]?$_POST["type"]:1;
			$UserInfo = $this->get_user($uid);
			if($UserInfo['id']){
				$days = $_POST["days"] ? $_POST["days"] : "30";
				$translateid = $_POST["translateid"] ? $_POST["translateid"] : "测试订单";
				$money = $_POST["money"] ? $_POST["money"] : "0.00";
				$paytime = time();
				$InData = array();
				$InData['uid'] = $uid;
				$InData['days'] = $days;
				$InData['translateid'] = $translateid;
				$InData['phonetime'] = format_time($paytime);//手机时间
				$InData['paytime'] = $paytime;//服务器时间
				$InData['type'] = $type;
				$InData['to_uid'] = 0;
				$InData['money'] = $money;//充值金额计算
				$InData['productid'] = 0;
				$InData['purchasetoken'] = 0;
				$InData['packagename'] = 0;
				$InData['isauto'] = 0;
				$InData['product'] = $product;
				$InData['ischeck'] = 0;
				$PayBaseM = new PayModel();
				$payid =$PayBaseM->addOne($InData);
				if($payid){
					$error=1;
					$msg="添加成功！";
					$this->get_dayNum($uid,$days);
					$this->get_user($uid,1);//
					file_get_contents("http://47.91.136.204:81/Adms/View/Setusercache?uid=".$uid);
					file_get_contents("http://47.89.47.92:81/Adms/View/Setusercache?uid=".$uid);
					$UserYixiangM = new UserYixiangModel();
					$UserYixiangM->delOne(array('uid'=>$uid));
				}else{
					$error=1;
					$msg="添加失败，请重新输入！";					
				}
			}else{
				$error=1;
				$msg="用户不存在！";
			}
		}
		$this->error=$error;
		$this->msg=$msg;
		$this->action = "/Adms/User/AddPay.html";
		$this->display("AddPay");
	
	}

	//用户信息
	public function userInfo(){
	    $this->name = '用户资料'; // 进行模板变量赋值
	    $this->status = C("status");
	    $Type = $_GET["type"] ? $_GET["type"] :"base";
        $urlshang = $_SERVER['HTTP_REFERER'];
        
        if($urlshang){
        	$resj=strpos($urlshang,'userinfo');
        	if($resj){
        	    $url=F('LASTURL'.$this->AdminUser["id"]);
			}else{
                $url=$urlshang;
                F('LASTURL'.$this->AdminUser["id"],$url);
			}

		}
		//清空缓存
		if($_GET["clearcache"]==1){
		    $this->get_user($_GET["uid"],1);
		}
	    if($_GET["uid"]){
        	//禁止发信
			if($_GET["setusertype"]){
				if($_GET["setusertype"]==6){
                    $data['user_type'] = '6';
                    $user=M('user_base')->where(array('uid'=>$_GET['uid']))->save($data);
                    $this->get_user($_GET["uid"],1);
				}elseif($_GET["setusertype"]==8){
                    $data['user_type'] = '8';
                    //将该手机注册的所有账号封停
                    $phoneid=$_GET["phoneid"];
                    if($phoneid){
                        $uids=M('user_base')->where(array('phoneid'=>$phoneid))->getField('uid',true);
                        foreach($uids as $key=>$values){
                            M('user_base')->where(array('uid'=>$values))->save($data);
                            $this->get_user($values,1);
                        }
                    }else{
                        M('user_base')->where(array('uid'=>$_GET["uid"]))->save($data);
                        $this->get_user($_GET["uid"],1);
                    }


                }
				else{
                    $data['user_type'] = '1';
                    $user=M('user_base')->where(array('uid'=>$_GET['uid']))->save($data);
                    $this->get_user($_GET["uid"],1);
				}
			}
	        $attrconf = $this->get_attrconf();
	        $userCache = $this->get_diy_user_field($_GET["uid"],"*");
	        $extinfo = array();
	        foreach ($attrconf as $k=>$v){
	            if($userCache[$v['field']]){
	                if($userCache[$v['field']]){
	                    if($v["type"]==1){
	                        $extinfo[]=array(
	                            'name'=>$v["name"],
	                            'value'=>$v["option"][$userCache[$v['field']]],
	                        );
	                    }else{
	                        $userValue = explode("|", $userCache[$v['field']]);
	                        $str = "";
	                        foreach ($userValue as $v1){
	                            if($str){
	                                $str.="|".$v["option"][$v1];
	                            }else{
	                                $str=$v["option"][$v1];
	                            }
	                        }
	                        
	                        $extinfo[]=array(
	                            'name'=>$v["name"],
	                            'value'=>$str,
	                        );

	                    }
	                }else{
	                    $extinfo[]=array(
	                        'name'=>$v["name"],
	                        'value'=>$userCache[$v['field']],
	                    );
	                }
	                
	            }
	        }

            $attrconfa = array_column($attrconf,NULL,'field');
            foreach($attrconfa as $ka=>$va){
                if(in_array($ka,$userCache)){
                    $name=$ka.'name';
                    $userCache[$name]=$va['option'][$userCache[$ka]];
                }
            }
	        //聊天信息
	        if($Type==msglist){
                //实例化消息对象
                //将服务器反应时间延长到1200秒
                ini_set('max_execution_time', '12000000000000');
                //实例化礼物对象
                $msgc=array_unique($this->getChatbox($_GET["uid"]));
                //循环遍历信箱对象，获取聊天用户
                foreach($msgc as $k1=>$v1){
                   /* $data=$this->getChatRecord($_GET["uid"],$v1);
                    $length=count($data);
                        $result = json_decode($data[0], true);
                        if ($length == 1 & $result['message']['msgtype'] == 'hello') {
                            continue;
                        }*/
                    $res=$this->get_diy_user_field($v1,'nickname|head');
                    $msglist[$k1]['uid']=$v1;
                    $msglist[$k1]['nickname']=$res['nickname'];
                    $msglist[$k1]['head']=$res['head']['url'];

                }
            }
            //打招呼信息
            if($Type==sayhello){
                ini_set('max_execution_time', '12000000000000');
                $msgc=$this->getChatbox($_GET["uid"]);
                //循环遍历信箱对象，获取聊天用户
                foreach($msgc as $k1=>$v1){
                    $data=$this->getChatRecord($_GET["uid"],$v1);
                    $length=count($data);
                    $result = json_decode($data[0], true);
                    if ($length == 1 & $result['message']['msgtype'] == 'hello') {
                        $res=$this->get_diy_user_field($v1,'nickname|head');
                        $msglist[$k1]['uid']=$v1;
                        $msglist[$k1]['nickname']=$res['nickname'];
                        $msglist[$k1]['head']=$res['head']['url'];
                        $res=$this->get_diy_user_field($result['from'],'nickname');
                        $msglist[$k1]['msg'][0]['nickname']=$res['nickname'];
                        $msglist[$k1]['msg'][0]['type']='hello';
                        $msglist[$k1]['msg'][0]['uid']=$result['from'];
                        $msglist[$k1]['msg'][0]['sendtime']=$result['sent'];
                        $msglist[$k1]['msg'][0]['content']=$result['message']['content'];
                    }
                }
            }
			//动态信息
			 if($Type==dynamic){
                 $Page = $_GET["Page"] ? $_GET["Page"] : 1;
                 $PageSize = 20;
                 $DynamicM = new DynamicModel();
                 $Wdata = array();
                 $Wdata['uid']=$_GET["uid"];
                 $ret = $DynamicM->getListPage($Wdata, $Page, $PageSize,"time");
                 foreach($ret["list"] as $k=>$v){
                     $ret["list"][$k]["user"]=$this->get_diy_user_field($v["uid"],"uid|head|nickname");
                     if($v["type"]==3){
                         if($v["ids"]){
                             $ret["list"][$k]['meiti'] = $this->get_videodynamic($v["ids"]);
                         }
                     }elseif($v["type"]==2){
                         $ids = explode("|", $v["ids"]);
                         foreach ($ids as $k1=>$v1){
                             if($v1)
                                 $ret["list"][$k]['meiti'][$k1] = $this->get_photodynamic($v1);
                         }
                     }
                 }
                 $this->msgurl=C ("IMAGEURL");
                 $this->DataList = $ret["list"];
                 $all_page = ceil($ret['totalCount']/$PageSize);
                 $ret["all_page"] = $all_page;
                 $this->Pages = $this->GetPages($ret);
                 $this->get = $_GET;
             }
			//修改个人信息
			if($Type=='modifyUserInfo'){
				$userinfo=$this->get_user($_GET["uid"]);
                $attrconf = array_column($attrconf,NULL,'field');
				$this->attrconf=$attrconf;
                $this->action = "/Adms/User/modifyUser.html";
			}
            //所分享的人
            if($Type=='shareuid'){
                $userinfo=$this->get_user($_GET["uid"]);
                $shareuids= explode(",", $userinfo['shareuid']);
                $this->shareuid=$shareuids;
            }

            if($Type=='online'){
                $CompereTime = new CompereTime2Model();
                $date = date("Ymd");
                $map = array("ontime"=>$date);
                $uid = $_GET['uid'];
                if(isset($_GET['dateStart'])&&$_GET['dateStart']!=""){
                    $ontime = str_replace("-",'',$_GET['dateStart']);
                    $map['ontime']=$ontime;
                    $date = $ontime;
                }

                $utime = $CompereTime->getList(array("uid"=>$uid,"ontime"=>$date));
                foreach($utime as &$value){
                    $value['ontime'] = '';
                    if($value['endtime'] != 0 && $value['endtime'] > $value['startime']){
                        $time = $value['endtime'] - $value['startime'];
                        $value['ontime'] = ceil($time/60)."分钟";
                    }
                    $value['startime'] = date("Y-m-d H:i:s",$value['startime']);
                    $value['endtime'] = $value['endtime'] == 0 ?"在线中":date("Y-m-d H:i:s",$value['endtime']);
                }
                $this->online=$utime;
            }

            if($Type=='oncallback'){
                file_get_contents("http://".$_SERVER['HTTP_HOST']."/Adms/User/reply.html?token=99123&uid=".$_GET['uid']);
                $info = M("reply")->where(array("uid"=>$_GET['uid']))->find();
                //计算一条回复率
                $call['call_1'] = ceil($info['news_1']/$info['newsall']*100)."%";
                $call['call_2'] = ceil($info['news_2']/$info['newsall']*100)."%";
                $call['call_3'] = ceil($info['news_3']/$info['newsall']*100)."%";
                //人数回复率
                $call['prople'] = ceil($info['reply_user']/$info['userall']*100)."%";
               $this->all_call = $call;
            }


             //获取魅力值信息
             $money=M('m_money')->where(array('uid'=>$_GET["uid"]))->find();
	    }
        $this->gifturl1=C ("IMAGEURL");
	    $this->extinfo = $extinfo;
	    if($userCache['viptime']!=0){
            $userCache['viptime']=$userCache['viptime']+60*60*24;
        }
	    $this->UserInfo = $userCache;
        $txkey=M('txmy')->where(array('product'=>$userCache['product']))->getField('miyao');
        $tentxunstatus = tencent_onlinestate(array($_GET["uid"]),$txkey);
        $onlineState=$tentxunstatus[0]['State'];
/*		$onlineState=$this->get_onlineState($_GET["uid"],$userCache);*/
		if($_REQUEST['ttest']==1){
            dump($tentxunstatus);
			dump($onlineState);
			exit;
		}
	    $this->onlineState=$onlineState;
        $this->msglist = $msglist;
	    $this->Type = $Type;
	    $this->url=$url;
	    $this->money=$money;

	    $this->display();
	}

    /*
* 获取聊天记录
* @from 消息发送者id
* @to 消息接受者id
* @Num 获取的数量
*
* 返回值，指定长度的包含聊天记录的数组
*/
    public function getChatRecord($from, $to)
    {
        //$redis = $this->redisconn();
        //$keyName = 'rec:' . $this->getRecKeyName($from, $to);
        //echo $keyName;
        //$recList = $redis->listGet($keyName, 0,-1);
        //$recList=array();
        //if(empty($recList)){
            $m=M('msg');
            $sql="SELECT `reidsvalue` FROM `t_msg` WHERE (uid=\"$from\" AND touid=\"$to\") or (uid=\"$to\" AND touid=\"$from\")ORDER BY `sendtime` desc  LIMIT 600";
            $recvalue =$m->query($sql);
            /*foreach($recvalue as $k=>$v){
                if($v['reidsvalue']!=null){
                    $recList[$k]=$v['reidsvalue'];
                }
            }*/


        $recvalue=array_reverse($recvalue);

            //数据不再储存缓存中
            /*if(!empty($recList)){
                foreach($recList as $k=>$v){
                    $this->setChatRecord($from,$to,$v);
                }
            }*/
        //}

        return $recvalue;
    }
    /*生成聊天记录的键名，即按大小规则将两个数字排序
    * @from 消息发送者id
    * @to 消息接受者id
    *
    *
    */
    private function getRecKeyName($from, $to)
    {
        return ($from > $to) ? $to . '_' . $from : $from . '_' . $to;
    }

    public function getChatbox($uid){
        $msgboxM=new MsgBoxModel();
        $redis = $this->redisconn();
        $keyName = 'recbox:' .$uid ;
        //echo $keyName;
        $recList = $redis->listGet($keyName, 0,-1);
        if(empty($recList)){
            $msga=$msgboxM->getListbyfield(array('uid'=>$uid),"touid");
            $msga=$msga?$msga:array();
            $msgb=$msgboxM->getListbyfield(array('touid'=>$uid),"uid");
            $msgb=$msgb?$msgb:array();
            $msgc=array_unique(array_merge($msga,$msgb));
            $recList=array_slice($msgc,0,100);
             if(!empty($recList)) {
                 foreach ($recList as $k => $v) {
                     $this->setchatbox($uid, $v);
                 }
             }
        }
        return $recList;
    }
    public function putchartobox(){
        //将服务器时间延长到1200秒反应时间
        ini_set('max_execution_time', '1200');
        $uid=$_POST['uid'];//自己的uid
        $touid=$_POST['touid'];//对方的uid
        $res=array();
        $data=$this->getChatRecord($uid,$touid);
        foreach($data as $k=>$v){
            if($v['reidsvalue']!=null){
                $res[$k]=json_decode($v['reidsvalue'],true);

               // $resa=$this->get_diy_user_field($res[$k]['from'],'nickname');
                //$res[$k]['nickname']=$resa['nickname'];
                if( $res[$k]['message']['msgtype']=='gift'||$res[$k]['message']['msgtype']=='askgift'){
                    $res[$k]['gifturl']=$res[$k]['message']['gift']['url'];
                    $res[$k]['gifttitle']=$res[$k]['message']['gift']['title'];
                }
            }
        }

        $reuslt = array(
            'status' =>'1',
            'message' => "",
            'data' => $res,
        );
        exit(json_encode ($reuslt));
    }
    /*
   *从数据库中调取记录存入缓存中
   * 这里用的redis存储是list数据类型
   * 两个人的聊天用一个list保存
   *
   * @from 消息发送者id
   * @meassage 消息内容
   * @to 消息接受者id
   *
   * 返回值，当前聊天的总聊天记录数
   */
    public function setChatRecord($from, $to, $value)
    {
        $redis = $this->redisconn();
        //生成json字符串
        $keyName = 'rec:' . $this->getRecKeyName($from, $to);
        //echo $keyName;
        $lang = $redis->listSize($keyName);
        if ($lang >= 1000) {
            $redis->listPop($keyName, 1);
        }
        $res = $redis->listPush($keyName, $value,1);
        $redis->setListKeyExpire($keyName,60*60*24*7);
        return $value;
    }
    //将聊天列表记录在redis中
    public function setchatbox($uid,$touid){
        $redis = $this->redisconn();
        //生成json字符串
        $keyNamea = 'recbox:' . $uid;
        $keyNameb = 'recbox:' . $touid;
        //echo $keyName;
        $langa = $redis->listSize($keyNamea);
        if ($langa >= 500) {
            $redis->listPop($keyNamea, 1);
        }
        $res = $redis->listPush($keyNamea, $touid,1);
        $redis->setListKeyExpire($keyNamea,60*60*24*7);

        $langb = $redis->listSize($keyNameb);
        if ($langb >= 500) {
            $redis->listPop($keyNameb, 1);
        }
        $resb = $redis->listPush($keyNameb, $uid);
        $redis->setListKeyExpire($keyNameb,60*60*24*7);
        return $res;
    }


	public function UserList(){
		$this->name = '用户列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		$this->status = C("status");
		$Wdata = array();
		if($_GET['user_type']&&$_GET['user_type']!="")
			$Wdata['user_type']=$_GET['user_type'];
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
        if(isset($_GET['product'])&&$_GET['product']!=""){
            $Wdata['product']=$_GET['product'];
		}
        if(isset($_GET['issuiliao'])&&$_GET['issuiliao']!=""){
            $Wdata['issuiliao']=$_GET['issuiliao'];
        }
        if(isset($_GET['isvip'])&&$_GET['isvip']!=""){
            if($_GET['isvip']==1){
                $Wdata['viptime']=array('gt',time());
            }
            if($_GET['isvip']==2){
                $Wdata['viptime']=array(array('eq',0),array('lt',time()-60*60*24), 'or');
            }
            if($_GET['isvip']==3){
                $Wdata['viptime']=array(array('gt',0),array('lt',time()-60*60*24)) ;
            }
        }
        if(isset($_GET['agegrade'])&&$_GET['agegrade']!=""){
                $Wdata['agegrade']=$_GET['agegrade'];
        }
        if(isset($_GET['istuijian'])&&$_GET['istuijian']!=""){
            if($_GET['istuijian']==1){
                $Wdata['beshareuid']=array('NEQ',' ');
            }

        }
        if(isset($_GET['issuiliao'])&&$_GET['issuiliao']!=""){
            $Wdata['issuiliao']=$_GET['issuiliao'];
        }
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword = $_GET['kerword'];
			$Wdata['uid|nickname|version|systemversion|phoneid|product']=array('like',"%".$kerword."%");
		}
		if(isset($_GET['dateStart'])&$_GET['dateStart']!=""&$_GET['dateEnd']==""){
            $regtime=strtotime($_GET['dateStart']);
            $Wdata['regtime']=array('GT',$regtime);
        }
        if(isset($_GET['dateEnd'])&&$_GET['dateEnd']!=""&$_GET['dateStart']==""){
            $regtime=strtotime($_GET['dateEnd']);
            $Wdata['regtime']=array('LT',$regtime);
        }
        if(isset($_GET['dateEnd'])&&$_GET['dateEnd']!=""&$_GET['dateStart']!=""){
            $regtime=strtotime($_GET['dateEnd']);
            $regstarttime=strtotime($_GET['dateStart']);
            $Wdata['regtime']=array('between',array($regstarttime,$regtime));;
        }



		$UserBaseM = new UserBaseModel();

		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$PageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
	
		foreach($ret['listSearch'] as $k=>$v){
			
			$userCache = $this->get_user($v["uid"]);
			
			$ret['listSearch'][$k]=array_merge($ret['listSearch'][$k],$userCache);
			
			
		} 
		//Dump($ret);exit;
		//Dump($this->get_user(137295));exit;
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
        $this->issuiliao = $_GET['issuiliao'];
        $this->isvip=$_GET['isvip'];
        $this->agegrade=$_GET['agegrade'];
        $this->istuijian=$_GET['istuijian'];
		$this->action = "/Adms/User/userlist.html";
		$this->display();
	
	}

	//用户类型为2的采集用户
    public function caijiUserList(){
        $this->name = '用户列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        if($_POST['page']){
            $Page=$_POST['page'];
        }
        $PageSize = 20;
        $this->status = C("status");
        $Wdata = array();
            $Wdata['user_type']=2;
        if(isset($_GET['gender'])&&$_GET['gender']!="")
            $Wdata['gender']=$_GET['gender'];
        if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
            $kerword = $_GET['kerword'];
            $Wdata['uid|nickname|version|systemversion|phoneid|product']=array('like',"%".$kerword."%");
        }
        $UserBaseM = new UserBaseModel();
        $ret = $UserBaseM->getlistOrder($Wdata,$Page,$PageSize);
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        foreach($ret['listSearch'] as $k=>$v){
            $userCache = $this->get_user($v["uid"]);
            $ret['listSearch'][$k]=array_merge($ret['listSearch'][$k],$userCache);
        }
        if($_POST['uid']) {
            $uid=$_POST['uid'];
            $count=count($ret['listSearch']);
            for($i=0;$i<$count;$i++){
                if($ret['listSearch'][$i]['uid']==$uid){
                    $counta=$i;
                    break;
                }
            }
            if(isset($counta)){
                $neckuid=$ret['listSearch'][$counta+1]['uid'];
                if($neckuid){
                    $url="caijiuserinfo.html?uid=".$neckuid."&page=".$Page;
                }else{
                    $url="caijiuserlist.html?Page=".$Page;
                }

                redirect($url);
            }
        }else{

        }
        //Dump($ret);exit;
        //Dump($this->get_user(137295));exit;
        $this->DataList = $ret;
        $this->page=$Page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = "/Adms/User/caijiuserlist.html";
        $this->display();

    }
    //采集用户更改内容
	public function caijiuserinfo(){
        $this->name = '采集用户信息修改'; // 进行模板变量赋值
        $attrconf = $this->get_attrconf();
        $userinfo=$this->get_diy_user_field($_GET["uid"],"*");
        $page=$_GET["page"];
        $attrconf = array_column($attrconf,NULL,'field');
        foreach($attrconf as $k=>$v){
            if(in_array($k,$userinfo)){
                $name=$k.'name';
                $userinfo[$name]=$v['option'][$userinfo[$k]];
            }
        }

        $this->attrconf=$attrconf;
        $this->UserInfo =$userinfo;
        $this->page=$page;
        $this->action = "/Adms/User/modifyUser.html";
        $this->actiona = "caijiuserlist?Page=".$page;
        $this->display();

	}
	//更改用户个人信息
	public  function modifyUser(){
        $jump=$_POST['jump'];
        if(isset($_POST['nickname'])&&$_POST['nickname']!="")
            $Wdata['nickname']=$_POST['nickname'];
        if(isset($_POST['password'])&&$_POST['password']!="")
            $Wdata['password']=$_POST['password'];
        if(isset($_POST['mood'])&&$_POST['mood']!="")
            $Wdatab['mood']=$_POST['mood'];
        if(isset($_POST['age'])&&$_POST['age']!="")
            $Wdata['age']=$_POST['age'];
        if(isset($_POST['height'])&&$_POST['height']!="")
            $Wdata['height']=$_POST['height'];
        if(isset($_POST['weight'])&&$_POST['weight']!="")
            $Wdata['weight']=$_POST['weight'];
        if(isset($_POST['income'])&&$_POST['income']!="")
            $Wdata['income']=$_POST['income'];
        if(isset($_POST['marriage'])&&$_POST['marriage']!="")
            $Wdata['marriage']=$_POST['marriage'];
        if(isset($_POST['education'])&&$_POST['education']!="")
            $Wdata['education']=$_POST['education'];
        if(isset($_POST['education'])&&$_POST['education']!="")
            $Wdata['education']=$_POST['education'];
        if(isset($_POST['work'])&&$_POST['work']!="")
            $Wdata['work']=$_POST['work'];
        if(isset($_POST['blood'])&&$_POST['blood']!="")
            $Wdatab['blood']=$_POST['blood'];
        if(isset($_POST['wantchild'])&&$_POST['wantchild']!="")
            $Wdatab['wantchild']=$_POST['wantchild'];
        if(isset($_POST['friendsfor'])&&$_POST['friendsfor']!="")
            $Wdatab['friendsfor']=$_POST['friendsfor'];
        if(isset($_POST['cohabitation'])&&$_POST['cohabitation']!="")
            $Wdatab['cohabitation']=$_POST['cohabitation'];
        if(isset($_POST['dateplace'])&&$_POST['dateplace']!="")
            $Wdatab['dateplace']=$_POST['dateplace'];
        if(isset($_POST['lovetimes'])&&$_POST['lovetimes']!="")
            $Wdatab['lovetimes']=$_POST['lovetimes'];
        if(isset($_POST['house'])&&$_POST['house']!="")
            $Wdatab['house']=$_POST['house'];
        if(isset($_POST['car'])&&$_POST['car']!="")
            $Wdatab['car']=$_POST['car'];
        if(isset($_POST['exoticlove'])&&$_POST['exoticlove']!="")
            $Wdatab['exoticlove']=$_POST['exoticlove'];
        if(isset($_POST['sexual'])&&$_POST['sexual']!="")
            $Wdatab['sexual']=$_POST['sexual'];
        if(isset($_POST['livewithparents'])&&$_POST['livewithparents']!="")
            $Wdatab['livewithparents']=$_POST['livewithparents'];
        if(isset($_POST['charactertype'])&&$_POST['charactertype']!="")
            $Wdatab['charactertype']=$_POST['charactertype'];
        if(isset($_POST['personalitylabel'])&&$_POST['personalitylabel']!="")
            $Wdatab['personalitylabel']=$_POST['personalitylabel'];
        if(isset($_POST['liketype'])&&$_POST['liketype']!="")
            $Wdatab['liketype']=$_POST['liketype'];
        if(isset($_POST['glamour'])&&$_POST['glamour']!="")
            $Wdatab['glamour']=$_POST['glamour'];
        if(isset($_POST['hobby'])&&$_POST['hobby']!="")
            $Wdatab['hobby']=$_POST['hobby'];

        $uid=$_POST['uid'];
        if(!empty($Wdata)&&$uid){
            $UserBaseM = new UserBaseModel();
            $modifydata=$UserBaseM->updateOne(array('uid'=>$uid),$Wdata);
            //删除缓存
        }
        if(!empty($Wdatab)&&$uid){
            $userextendM=new UserExtendModel();
            $modifydatb=$userextendM->updateOne(array('uid'=>$uid),$Wdatab);
        }
        if($modifydata==1 || $modifydatb==1 ){
            $this->get_user($uid,1);
        }
        redirect($jump);
	}
	//删除采集用户
    public function delcaijiuser(){
        $uid=$_GET['uid'];
        if($uid){
            $this->del_user($_GET['uid']);
            $this->get_user($uid,1);
            redirect("caijiuserlist.html?Page=".$_GET['page']);
        }
}
//将相册图片设为头像
    public function setphoto(){
        $uid = $_POST['uid'];
        $id = $_POST['id'];
        if($uid&&$id){
            $UserPhotoM = new PhotoModel();
            $UserPhotoM->updateOne(array('uid'=>$uid,"type"=>"2"),array("type"=>"1"));
            $data=$UserPhotoM->updateOne(array('uid'=>$uid,"id"=>$id),array("type"=>"2"));
            //更新头像缓存
            $this->get_user_ico($uid,1);
            $this->get_user_ico_all($uid,1);
            //更新相册缓存
            $this->get_user_photo($uid,1);
            $this->get_user_photo_all($uid,1);
            //$this->get_user($uid,1);//更新用户缓存信息
        }
        if($data){
            $reuslt = array(
                'status' =>'1',
                'message' => "已设为头像",
                'data' => '',
            );
            exit(json_encode ($reuslt));
        }
        $reuslt = array(
            'status' =>'1',
            'message' => "设置头像失败",
            'data' => '',
        );
        exit(json_encode ($reuslt));

    }
	public function UserShenhe(){
		$this->name = '用户列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		
		$Wdata = array();
		
		$Wdata['user_type']=2;
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerwordarr = explode(" ",$_GET['kerword']);
			$kerword = $_GET['kerword'];
			if(in_array($kerwordarr[0],C("_PRODUCTS"))){
				$Wdata['product']=$kerwordarr[0];
				if($kerwordarr[1]){
					$kerword = $kerwordarr[1];
				}else{
					$kerword = 0;
				}
			}
			if($kerword)
			$Wdata['uid|nick_name|channelid|pid|phonetype']=array('like',"%".$kerword."%");
			
		}
		if($_GET['channelid']&&$_GET['channelid']!="")
			$Wdata['channelid']=$_GET['channelid'];
		if(isset($_GET['dateStart'])&&$_GET['dateStart']!="")
		$Wdata["regtime"]=array(array('egt',$_GET["dateStart"]),array('elt',$_GET["dateEnd"]));
		$UserBaseM = new UserBaseModel();

		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$pageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
	
		foreach($ret['listSearch'] as $k=>$v){
			
			$ret['listSearch'][$k]=$this->get_user($v["uid"]);
			$ret['listSearch'][$k]["token"] = $v["token"];
			$ret['listSearch'][$k]["password"] = $v["password"];
			$ret['listSearch'][$k]["account"] = $v["account"];
			$ret['listSearch'][$k]["user_type"] = $v["user_type"];
			$ret['listSearch'][$k]["regtime"] =date("Y/m/d H:i:s",strtotime($v["regtime"]));
			$photo_num = 0;
			if(is_array($ret['listSearch'][$k]["listImage"])){
				$photo_num = count($ret['listSearch'][$k]["listImage"]);
			}
			$ret['listSearch'][$k]["photo_num"] = $photo_num;
			
		} 
		//Dump($ret);exit;
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Adms/User/UserShenhe.html";
		$this->display("UserShenhe");
	
	}
	public function MsgList(){
		$this->name = '用户信息'; // 进行模板变量赋值
				$uid = $_GET['uid'] ? $_GET['uid'] : 0;
		$myid = $uid;
		$msgboxlistkey = 'msgboxlist_'.$myid;	
		
		$boxlistall = $this->redis->lRange($msgboxlistkey, 0, -1);
		$BoxList = array();
		foreach($boxlistall as $k=>$v){
			$boxinfo = array();
			$boxinfo = object_array(json_decode($this->redis->get("msgboxlist_value_".$v)));
			if($boxinfo){
				$to_uid = $boxinfo['to_uid'];
				$touinfo = $this->get_user($to_uid);
				
				$uinfo = $this->get_user($myid);
				$touinfo["nickName"] = $touinfo["nickName"]."(".$to_uid.")";
				$uid_str = $myid."_".$to_uid;
				$msglistkey = 'msglist_'.$uid_str;	
				$Temp["boxinfo"] = $boxinfo;
				$Temp["toUname"] = $touinfo["nickName"];
				$msglist = $this->redis->lRange($msglistkey,0, -1);
				$Temp["boxlist"] = array();
				foreach($msglist as $k1=>$v1){
					$msg = object_array(json_decode($v1));
					$tuserinfo = $this->get_user($msg['uid']);
					$Temp["boxlist"][$k1]["nickName"]=$tuserinfo["nickName"]."(".$tuserinfo['id'].")";
					$Temp["boxlist"][$k1]["content"]=$msg['content'];
					$Temp["boxlist"][$k1]["time"]=date("Y-m-d H:i:s",$msg['sys_createdate']);
					
				}
				
			}
			$BoxList[] = $Temp;
		}
		//Dump($BoxList);exit;
		$this->BoxList = $BoxList;
		$this->display("MsgList");
	}
	public function SetUserProduct(){
			$uid = $_GET['uid'] ? $_GET['uid'] : 0;
			$product = $_GET['product'] ? $_GET['product'] : 10108;
			if($uid){
				$where = array('uid'=>$uid);
				$User_baseM = new UserBaseModel();
				$UserInfo = $User_baseM->updateOne($where,array("product"=>$product));
				$this->get_user($uid,1);//更新用户缓存信息
				file_get_contents("http://47.91.136.204:81/Adms/View/Setusercache?uid=".$uid);
				file_get_contents("http://47.89.47.92:81/Adms/View/Setusercache?uid=".$uid);
			}
			header('Location:'.$_SERVER['HTTP_REFERER']);
	}
	//删除用户
	public function deluser(){

		$uid=$_GET['uid'];
		if($uid){
            $this->del_user($_GET['uid']);
            header('Location:'.$_SERVER['HTTP_REFERER']);
		}

	}
    //删除第三方用户
    public function delthreeuser(){

        $id=$_GET['id'];
        if($id){
           M('admin_three_user')->where(array('id'=>$id))->delete();
            redirect('three_userlist.html');
        }

    }

	//找回用户
	public function finduser(){
        $this->name = '用户找回'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $this->status = C("status");

        $Wdata = array();
        if($_GET['user_type']&&$_GET['user_type']!="")
            $Wdata['user_type']=$_GET['user_type'];
        if(isset($_GET['gender'])&&$_GET['gender']!="")
            $Wdata['gender']=$_GET['gender'];
        if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
            $kerword = $_GET['kerword'];
            $Wdata['uid|nickname']=array('like',"%".$kerword."%");
        }
        $UserBaseBakM = new UserBaseBakModel();

        $ret = $UserBaseBakM->getlistOrder($Wdata,$Page,$PageSize);

        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;

        foreach($ret['listSearch'] as $k=>$v){

            $userCache = $this->get_user($v["uid"]);

            $ret['listSearch'][$k]=array_merge($ret['listSearch'][$k],$userCache);


        }
        //Dump($ret);exit;
        //Dump($this->get_user(137295));exit;
        $this->DataList = $ret;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = "/Adms/User/finduser.html";
        $this->display();

	}

	//用户还原
	public function user_rbk(){
        if($_GET['uid'])
            $this->rbk_user($_GET['uid']);
        header('Location:'.$_SERVER['HTTP_REFERER']);
	}
	//还原用户方法
    private function rbk_user($id){
        $uid=array();
        $uid['uid']=$id;
        //实例化基本用户表模型
        $UserBaseM = new UserBaseModel();
        //实例化基本用户备份表模型
        $UserBaseBakM=new UserBaseBakModel();
        //实例化基本用户扩展表模型
        $UserExtendM = new UserExtendModel();
        //实例化基本用户扩展备份表模型
        $UserExtendBakM=new UserExtendBakModel();
        $data=$UserBaseBakM->selOne ($uid);
        $datak=$UserExtendBakM->selOne ($uid);
        $result=$UserBaseM->selOne ($uid);
        //查询源表中是否有相同uid，有的话执行更新操作
        if($result){
            $UserBaseM->updateAll ($data,$uid);
        }
        //执行插入操作
        $insert=$UserBaseM->insOne ($data);
        //扩展表执行插入操作
		if($datak!=null){
            $insertk=$UserExtendM->insOne ($datak);
            if($insertk){
                $UserExtendBakM->delOne ($uid);
            }else{
                echo "插入失败";
            }
		}

        if($insert){
            $UserBaseBakM->delOne ($uid);
        }else{
            echo "插入失败";
        }

    }

	//删除用户方法
	private function del_user($id){
		$uid=array();
		$uid['uid']=$id;
		//实例化基本用户表模型
       $UserBaseM = new UserBaseModel();
        //实例化基本用户备份表模型
        $UserBaseBakM=new UserBaseBakModel();
        //实例化基本用户扩展表模型
        $UserExtendM = new UserExtendModel();
        //实例化基本用户扩展备份表模型
        $UserExtendBakM=new UserExtendBakModel();

        $data=$UserBaseM->selOne ($uid);
        $datak=$UserExtendM->selone($uid);

		//执行插入操作
		$insert=$UserBaseBakM->insOne($data);
        if($insert){
            $UserBaseM->delOne ($uid);
        }else{
            echo "插入失败";
        }
        //扩展表执行插入操作
        if($datak!=null){
            $insertk=$UserExtendBakM->insOne ($datak);
            if($insertk){
                $UserExtendM->delOne ($uid);
            }else{
                echo "插入失败";
            }
		}


	}

	/*
	 * 删除图片 /Adm/deleteImg
	*/
	public function deleteImg(){
		$imgid = $_POST['id'];
		$Deldata = array();
		$Deldata['id'] = $imgid;
		$UserPhotoM = new PhotoModel();
		$imgs = $UserPhotoM->getOne($Deldata);
		$uid = $imgs['uid'];
		/**
		 删除数据库
		 */
		 $UserPhotoM->delOne($Deldata);
		/*/**
			删除文件
		 */
        _unlink(WR.$imgs['url']);
        DelOss($imgs['url']);
        //删除相册文件缓存
        //更新头像缓存
        $this->get_user_ico($uid,1);
        $this->get_user_ico_all($uid,1);
        //更新相册缓存
        $this->get_user_photo($uid,1);
        $this->get_user_photo_all($uid,1);
        $reuslt = array(
            'status' =>'1',
            'message' => "已删除图片",
            'data' => '',
        );
        exit(json_encode ($reuslt));

	}
	//清理用户
	public function ClearUser(){
		$this->name = '删除用户'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		
		$Wdata = array();
		if($_GET['user_type']&&$_GET['user_type']!="")
			$Wdata['user_type']=$_GET['user_type'];
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
		if(isset($_GET['kerword'])&&$_GET['kerword']!="")
			$Wdata['uid|nick_name|channelid']=array('like',"%".$_GET['kerword']."%");
		if($_GET['channelid']&&$_GET['channelid']!="")
			$Wdata['channelid']=$_GET['channelid'];
		if(isset($_GET['dateStart'])&&$_GET['dateStart']!="")
		$Wdata["regtime"]=array(array('egt',$_GET["dateStart"]),array('elt',$_GET["dateEnd"]));
		$UserBaseM = new UserBaseModel();
		
		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$PageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
	
		foreach($ret['listSearch'] as $k=>$v){
			$ret['listSearch'][$k]=$this->get_user($v["uid"]);
			$ret['listSearch'][$k]["token"] = $v["token"];
			$ret['listSearch'][$k]["password"] = $v["password"];
			$ret['listSearch'][$k]["account"] = $v["account"];
			$ret['listSearch'][$k]["user_type"] = $v["user_type"];
			$photo_num = 0;
			if(is_array($ret['listSearch'][$k]["listImage"])){
				$photo_num = count($ret['listSearch'][$k]["listImage"]);
			}
			$ret['listSearch'][$k]["photo_num"] = $photo_num;
		}
		$this->getsurl = http_build_query($_GET);
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Adms/User/ClearUser.html";
		$this->display("ClearUser");
	}
	//清理用户
	public function DelAllUser(){
		$this->name = '批量删除用户'; // 进行模板变量赋值
		echo 1;exit;
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 100;
		
		$Wdata = array();
		if($_GET['user_type']&&$_GET['user_type']!="")
			$Wdata['user_type']=$_GET['user_type'];
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
		if(isset($_GET['kerword'])&&$_GET['kerword']!="")
			$Wdata['uid|nick_name|channelid']=array('like',"%".$_GET['kerword']."%");
		if($_GET['channelid']&&$_GET['channelid']!="")
			$Wdata['channelid']=$_GET['channelid'];
		if(isset($_GET['dateStart'])&&$_GET['dateStart']!="")
		$Wdata["regtime"]=array(array('egt',$_GET["dateStart"]),array('elt',$_GET["dateEnd"]));
		
		//$Wdata['product']=array('neq',"10008");
		//$Wdata['photo_number'] = array('gt',3);//照片数量大于三张的不删除
		//$Wdata['user_type'] = 1;//只删除注册用户
		$UserBaseM = new UserBaseModel();
		
		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$PageSize);
		//Dump($ret);exit;
		$all_page = ceil($ret['totalCount']/$PageSize);
		
		foreach($ret['listSearch'] as $k=>$v){
			$this->del_user($v["uid"]);
		}
		
		$getargc = $_GET;
		if($Page==1){
			$q_page = $all_page;
			$q_totalCount = $ret['totalCount'];
			$getargc["q_page"]=$all_page;
			$getargc["q_totalCount"]=$ret['totalCount'];
			$getargc["Page"]=2;
			$getargc["q_wancheng"] = $PageSize;
		}else{
			$getargc["q_wancheng"]=$getargc["q_wancheng"]+$PageSize;
			if($getargc["q_wancheng"]>$getargc["q_totalCount"]){
				$getargc["q_wancheng"] = $getargc["q_totalCount"];
			}
			$getargc["Page"]=$getargc["Page"]+1;
		}
		if($getargc["Page"]>$getargc["q_page"]){
			$this->yiwancheng = 100;
		}else{
			$this->yiwancheng = $getargc["q_wancheng"]/$getargc["q_totalCount"]*100;
		}
		
		$this->msg = '删除进度,共'.$getargc["q_totalCount"]."条;已删除".$getargc["q_wancheng"]; // 进行模板变量赋值
		$this->getsurl = http_build_query($getargc);
		$this->DataList = $ret;
		$this->get = $_GET;
		$this->action = "/Adms/User/DelAllUser.html";
		$this->display("DelAllUser");
	}
	public function UserListOnline(){
		$this->name = '用户列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		
		$Wdata = array();
		if($_GET['user_type']&&$_GET['user_type']!="")
			$Wdata['user_type']=$_GET['user_type'];
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
		if(isset($_GET['kerword'])&&$_GET['kerword']!="")
			$Wdata['uid|nick_name|channelid']=array('like',"%".$_GET['kerword']."%");
		if($_GET['channelid']&&$_GET['channelid']!="")
			$Wdata['channelid']=$_GET['channelid'];
		if(isset($_GET['dateStart'])&&$_GET['dateStart']!="")
		$Wdata["regtime"]=array(array('egt',$_GET["dateStart"]),array('elt',$_GET["dateEnd"]));
		$UserBaseM = new UserBaseModel();
		$Wdata['onlinestate']=1;
		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$pageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
	
		foreach($ret['listSearch'] as $k=>$v){
			$ret['listSearch'][$k]=$this->get_user($v["uid"]);
			$ret['listSearch'][$k]["token"] = $v["token"];
			$ret['listSearch'][$k]["password"] = $v["password"];
			$ret['listSearch'][$k]["account"] = $v["account"];
			$ret['listSearch'][$k]["user_type"] = $v["user_type"];
			$photo_num = 0;
			if(is_array($ret['listSearch'][$k]["listImage"])){
				$photo_num = count($ret['listSearch'][$k]["listImage"]);
			}
			$ret['listSearch'][$k]["photo_num"] = $photo_num;
		}
		
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Adms/User/UserListOnline.html";
		$this->display("UserList");
	
	}

	//会员充值
	public function recharge()
    {
        $this->name = '会员充值'; // 进行模板变量赋值
        $Wdata = array();
        if ($_POST["uid"]&&$_POST["count"]) {
            $uid = $_POST["uid"];
            $paytype = $_POST['type'];
            if($_POST['type1']){
                $huiyuantype = $_POST['type1'];
            }
            $count = $_POST['count'];//充值天数或金币数量
            $paytime = time();
            $reData = array();
            $reData['uid'] = $uid;
            $reData['days'] = $count;
            $reData['paytime'] = $paytime;//服务器时间
            $reData['translateid'] = "测试订单";
            $reData['money'] = '100';
            if($paytype==1){
                $reData['type'] =3;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
            }else{
                $reData['type'] =1;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
            }

            $UserBaseM = new UserBaseModel();
            $RechargeM = new RechargeModel();
            $p_moneyM=M('p_money');
            $m_moneyM=M('m_money');


            $data = $UserBaseM->getone (array('uid' => $uid));
            if ($data) {
                if ($paytype == 1) {
                    $Wdata['gold'] = $count + $data['gold'];
                    $reData['vipgold'] = $count + $data['gold'];
                    $this->set_user_field($uid,'gold',$Wdata['gold']);

                    $charid=$RechargeM->addOne($reData);
                    if($huiyuantype){
                        $Wdata['vipgrade']=$huiyuantype;
                    }
                    $res = $UserBaseM->updateOne (array('uid' => $uid), $Wdata);
                } elseif($paytype == 2) {
                    $Wdata['viptime'] = $count * 60 * 60 * 24;
                    $rtime = $data['viptime'];
                    if ($rtime == 0||$rtime<time()) {
                        $Wdata['viptime'] = $Wdata['viptime'] + time ();

                    }else {
                        $Wdata['viptime'] = $Wdata['viptime'] + $rtime;
                    }
                    $this->set_user_field($uid,'viptime',$Wdata['viptime']);
                    $reData['vipgold'] =$Wdata['viptime'] ;


                    $charid=$RechargeM->addOne($reData);
                    if($huiyuantype){
                        $Wdata['vipgrade']=$huiyuantype;
                    }
                    $res = $UserBaseM->updateOne (array('uid' => $uid), $Wdata);

                }else{
                    //充值魅力值
                    $pmoneydata=array();
                    $pmoneydata['to_uid']='10002';
                    $pmoneydata['uid']=$uid;
                    $pmoneydata['type']='14';
                    $pmoneydata['paytime']=time();
                    $pmoneydata['money']=$count;
                    $p_moneyM->data($pmoneydata)->add();
                    $oldmoney=$m_moneyM->where(array('uid'=>$uid))->find();
                    $mmoneydata=array();
                    $mmoneydata['totalmoney']=$oldmoney['totalmoney']+$count;
                    $mmoneydata['leftmoney']=$oldmoney['leftmoney']+$count;
                    if(empty($oldmoney)){
                        $mmoneydata['uid']=$uid;
                        $res = $m_moneyM->data($mmoneydata)->add();
                    }else{
                        $res = $m_moneyM->where(array('uid'=>$uid))->save($mmoneydata);
                    }
                    if($res){
                        //发送系统通知
                        $content='系统已成功给您添加了'.$count.'魅力值，请查收！';
                        $argv = array();
                        $argv['url'] = "http://127.0.0.1:81/userauth/sendtzmsg/?uid=" . $uid . "&content=" . $content;
                        $argv = base64_encode(json_encode($argv));
                        $command = "/usr/bin/php " . WR . "/openurl.php " . $argv . " > /dev/null &";
                        system($command);
                    }

                }

                if ($res) {
                    $this->get_user($uid,1);
                    $reuslt = array(
                        'status' => '1',
                        'message' => "充值成功",
                        'data' => '',
                    );

                    exit(json_encode ($reuslt));
                } else {
                    $reuslt = array(
                        'status' => '2',
                        'message' => "充值失败",
                        'data' => '',
                    );
                    exit(json_encode ($reuslt));
                }
            }else{
                $reuslt = array(
                    'status' => '3',
                    'message' => "没有找到相关的uid",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
        $this->action = "/Adms/User/recharge.html";
        $this->display ();

    }
//获取视频
    public function get_videodynamic($id,$reset='0'){
        $path = WR.'/userdata/cache/dynamic/video/';
        $cache_name = 'videoinfo_'.$id;
        if(F($cache_name,'',$path) && $reset == 0){
            $Info = F($cache_name,'',$path);
        }else{
            $VideoDynamicM = new VideoDynamicModel();
            $videoTemp = $VideoDynamicM->getOne(array("id"=>$id));
            $Info=array(
                "id"=>$videoTemp["id"],
                "ltime"=>$videoTemp["ltime"],
                "url"=>C("IMAGEURL").$videoTemp["url"],
                "imageurl"=>C("IMAGEURL").$videoTemp["imageurl"],
                "status"=>$videoTemp["status"]
            );
            F($cache_name,$Info,$path);
        }
        return $Info;
    }
    //获取动态图片
    public function get_photodynamic($id,$reset='0'){
        $path = WR.'/userdata/cache/dynamic/photo/';
        $cache_name = 'photoinfo_'.$id;
        if(F($cache_name,'',$path) && $reset == 0){
            $Info = F($cache_name,'',$path);
        }else{
            $PhotoDynamicM = new PhotoDynamicModel();
            $Temp = $PhotoDynamicM->getOne(array("id"=>$id));
            $Info=array(
                "id"=>$Temp["id"],
                "url"=>C("IMAGEURL").$Temp["url"],
                "thumbnaillarge"=>C("IMAGEURL").$Temp["url"],
                "thumbnailsmall"=>C("IMAGEURL").$Temp["url"],
                "status"=>$Temp["status"],
                "seetype"=>$Temp["seetype"]
            );
            F($cache_name,$Info,$path);
        }
        return $Info;
    }
    //动态审核通过后重新驳回
	public function judge(){
 		$Wdata = array();
        if ($_POST["status"]&&$_POST["id"]) {
            $status = $_POST["status"];
            $id=$_POST["id"];
            $uid=$_POST["uid"];
            $dynamicM= new DynamicModel();
            $PhotoDynamicM = new PhotoDynamicModel();
            $VideoDynamicM = new VideoDynamicModel();
            if($status==3){
            	$res1=$dynamicM->getOne(array('id'=>$id));
				$type=$res1['type'];
				if($type==2){
                   $photod= $PhotoDynamicM->where(array('danamicid'=>$id))->select();
                   foreach($photod as $k=>$v){
                       DelOss($v['url']);
				   }
                    $PhotoDynamicM->where(array('danamicid'=>$id))->delete();
				}
				if($type==3){
					$videod=$VideoDynamicM->getOne(array('danamicid'=>$id));
                    DelOss($videod['url']);
                    $res1=$VideoDynamicM->where(array('danamicid'=>$id))->delete();
				}
                $res=$dynamicM->delOne(array('id'=>$id));
		}
		if($res) {
            $reuslt = array(
                'status' => '1',
                'message' => "更新成功",
                'data' => '',
            );
        }
		//删除已经通过的动态，也需要将存在redis中的缓存删除，然后重新读取数据库
            $redis=$this->redisconn();
			$redis->del('dynamic');
        }
        exit(json_encode ($reuslt));
	}

	//获取在线用户
	public function getonlineStatelist()
    {
        import("Extend.Library.ORG.Util.Page");
        $this->name = '在线用户'; // 进行模板变量赋值
        $this->status = C("status");
        //获取平台号
		$getproduct1=M('products')->select();
		foreach($getproduct1 as $k=>$v){
            $getproduct[]=$v['product'];
		}
		$this->getproduct=$getproduct;
    	$arr['gender']= $_GET['gender']?$_GET['gender']:2;
    	$arr['product']= $_GET['product']?$_GET['product']:20110;
    	$arr['user_type']= $_GET['user_type']?$_GET['user_type']:3;
        $arr['line_type']= $_GET['line_type']?$_GET['line_type']:0;//0:全部 1：online 2:pushonline
        $arr['time_type']= $_GET['time_type'];//0:全部 1：10分钟以内 2:1小时以内3：5小时以内
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize =15;

        $onlinelist=$this->get_onlinelist($arr['product'],$arr['user_type'],$arr['gender'],$arr['line_type']);
        if($arr['time_type']&&$arr['line_type']==2){
            $ret=array();
            $reta=array();
            foreach($onlinelist as $k=>$v){
                $reta[$k]=$this->get_user($v);
                if($_REQUEST['pget']==1){
                    dump($arr['time_type']);
                    exit();
				}
                if($arr['time_type']==1){
                    if($reta[$k]['logintime']>=time()-60*10){
                        $ret[$k]=$reta[$k];
                    }
                }elseif ($arr['time_type']==2){
                    if($reta[$k]['logintime']>=time()-60*60){
                        $ret[$k]=$reta[$k];
                    }
                }elseif ($arr['time_type']==3){
                    if($reta[$k]['logintime']>=time()-60*60*5){
                        $ret[$k]=$reta[$k];
                    }
                }elseif ($arr['time_type']==4) {
                    if ($reta[$k]['logintime'] >= time() - 60 * 60 * 24) {
                        $ret[$k] = $reta[$k];
                    }
                }
                else{
                    if($_REQUEST['pget']==3){
                        dump($arr['time_type']);
                        exit();
                    }
                    $ret[$k]=$reta[$k];
                }
            }
            $lang=count($ret);
            $page_count = ceil($lang/$PageSize);
            $retArray = array();
            $retArray['totalCount'] = $lang;
            $retArray['pageSize'] = $PageSize;
            $retArray['pageNum'] = $Page;
            $ret=array_slice($ret,($Page-1)*$PageSize,$PageSize);
            $retArray['list'] = $ret;
            $this->Pages = $this->GetPages($retArray);
		}else{

            $lang=count($onlinelist);
            $page_count = ceil($lang/$PageSize);
            $retArray = array();
            $retArray['totalCount'] = $lang;
            $retArray['pageSize'] = $PageSize;
            $retArray['pageNum'] = $Page;
            $onlinelist=array_slice($onlinelist,($Page-1)*$PageSize,$PageSize);
            $retArray['list'] = $onlinelist;
            $this->Pages = $this->GetPages($retArray);
            $ret=array();
            $reta=array();
            foreach($onlinelist as $k=>$v){
                $ret[$k]=$this->get_user($v);
            }
		}
        $this->DataList = $ret;
        $this->gender=$arr['gender'];
        $this->product=$arr['product'];
        $this->line_type=$arr['line_type'];
        $this->user_type=$arr['user_type'];
        $this->time_type=$arr['time_type'];
        $this->display();
    }

    public  function  issuiliao(){
        $redis = $this->redisconn();
		$uid=$_POST['uid'];
		$type=$_POST['type'];
		if($type==1){
			//开启随聊
                $user1=M('user_base')->where(array('uid'=>$uid))->save(array('issuiliao'=>1));
                $this->set_user_field($uid,'issuiliao',1);
            	$redis->listRemove('suiliaouidlist',$uid);
            	$redis->listPush('suiliaouidlist',$uid);
                if($user1){
                    $reuslt = array(
                        'status' =>'1',
                        'message' => "已开启随聊",
                        'data' => '',
                    );
                    exit(json_encode ($reuslt));
                }
            }elseif($type==0){
			//关闭随聊
                $user1=M('user_base')->where(array('uid'=>$uid))->save(array('issuiliao'=>0));
                $this->set_user_field($uid,'issuiliao',0);
            	$redis->listRemove('suiliaouidlist',$uid);
                if($user1){
                    $reuslt = array(
                        'status' =>'0',
                        'message' => "已关闭随聊",
                        'data' => '',
                    );
                    exit(json_encode ($reuslt));
                }
            }

	}
//老推广用户可提现魅力值
	public function  canmakemoeny(){
        $this->name = '老推广用户可提现魅力值'; // 进行模板变量赋值
		$res=array();
		$users=M('user_base')->where(array('user_type'=>3))->select();
		$allmoney=0;
		foreach($users as $k=>$v){
            $leftmoney=M('m_money')->where(array('uid'=>$v['uid']))->getField('leftmoney');
            if($leftmoney===null){
                continue;
            }else{
                $res[$k]['uid']=$v['uid'];
                $res[$k]['username']=$v['nickname'];
                $res[$k]['leftmoney']=$leftmoney;
			}
            $allmoney=$allmoney+$leftmoney;
		}
        $this->DataList = $res;
		$this->allmoney=$allmoney;
        $this->display();
	}



	public function reply(){

        //将服务器反应时间延长到1200秒
        //ini_set('max_execution_time', '12000000000000');
        //先将用户存入数据库
        $uid = $_GET["uid"];
        $resly = M("reply")->where(array("uid"=>$uid))->find();
        if(empty($resly)){
            M("reply")->add(array("uid"=>$uid));
        }
        $msgc=array_unique($this->getChatbox($uid));
        $beginThismonth = mktime(0, 0, 0, date('m'), 1, date('Y'));//月初时间戳
        //rsort($msgc);
        $outhuids = array();
        $outhuids_2 = array();
        $news_1 = array();
        $news_2 = array();
        $news_3 = array();
        $newsall = array();
        foreach($msgc as $k1=>$v1){
            //$data=$this->getChatRecord($_GET['uid'],$v1);
            $from = $v1;
            $to = $uid;
            $m=M('msg');
            $sql="SELECT `reidsvalue` FROM `t_msg` WHERE (uid=\"$from\" AND touid=\"$to\")ORDER BY `sendtime` desc  LIMIT 100";
            var_dump($sql);die;
            //$data =$m->query($sql);
            var_dump($data);die;
//            foreach($data as $v){
//                $json_undata= json_decode($v['reidsvalue'],true);
//                $userinfo=$this->get_user($json_undata["from"]);
//                //查询发送者uid 是否是vip
//                $sent = $json_undata['sent'];
//                if(!$sent){
//                    continue;
//                }
//
//                if($userinfo['viptime'] < time()){
//                    $endtime = $sent + 30;
//                }else{
//                    $endtime = $sent + 60;
//                }
//
//                $compere_time2 = M("compere_time2");
//                //查询消息期间内是否在线
//                $sqltime="SELECT `id` FROM `t_compere_time2` WHERE uid=\"$to\"  AND startime < $sent AND endtime > $sent";
//                sleep(0.1);
//                $restime =$compere_time2->query($sqltime);
//                if(!empty($restime)){//如果是在线期间
//                    //M("reply")->where(array("uid"=>$uid))->setInc("newsall");//总消息加1
//                    array_push($newsall,1);
//                    //吧当前有效对方uid放入数组中
//                    $outhuids[$v1] = $v1;// 在线期间内有多少人给他发消息总人数
//
//                    //查询在当前时间以内是否有回复
//
//                    $sql="SELECT `reidsvalue` FROM `t_msg` WHERE (uid=\"$to\" AND touid=\"$from\" AND sendtime >=\"$sent\" AND sendtime<=\"$endtime\")";
//                    $res =$m->query($sql);
//                    if(!empty($res)){
//                        $outhuids_2[$v1] = $v1;//在线期间内回复发消息的总人数
//                    }
//                    //var_dump($res);
//                    if(count($res) >= 1){
//                        //M("reply")->where(array("uid"=>$uid))->setInc("news_1");//1条回复数+1
//                        array_push($news_1,1);
//                    }
//                    if(count($res) >= 2){
//                        //M("reply")->where(array("uid"=>$uid))->setInc("news_2");//2条回复数+1
//                        array_push($news_2,1);
//                    }
//                    if(count($res) >= 3){
//                        //M("reply")->where(array("uid"=>$uid))->setInc("news_3");//3条回复数+1
//                        array_push($news_3,1);
//                    }
//
//                }
//            }
        }
        $save = array(
            "newsall"=>count($newsall),
            "news_1"=>count($news_1),
            "news_2"=>count($news_2),
            "news_3"=>count($news_3),
            "userall"=>count($outhuids),
            "reply_user"=>count($outhuids_2)
        );
        M("reply")->where(array("uid"=>$uid))->save($save);
    }

}

?>
