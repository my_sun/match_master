<?php
use Think\Controller;
class ShoppingmallController extends BaseAdmsController
{
    public function __construct()
    {
        parent::__construct();
        $this->basename = '商品分类-'; // 进行模板变量赋值
        $this->cachepath = WR . '/userdata/cache/face/';
        $this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";

    }
    
    //商品分类列表
    public function classification()
    {
        $this->name = $this->basename . '列表'; // 进行模板变量赋值

        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 12;
        $Classification = new ClassificationModel();
        //$status = include WR . "/userdata/publicvar/status.php";
        //$this->switch_type = $status["gift"]["type"];
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|name|status'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $Classification->getListPage ($Wdata, $Page, $PageSize);
        $this->msgurl=C ("IMAGEURL");
        $this->DataList= $ret["list"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);

        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display ();
    }

    //商品分类添加
    public function classificationadd()
    {
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        $status = C("status");
        if($_POST){
            if($_POST["name"]){
                $Classification = new ClassificationModel();
                $map = array();
                $map["name"] = $_POST["name"];
                $Classification->addOne($map);
                //连接redis
            $redis = new RedisModel();
            //设置key
            $key = "classification_key";
            // 删除缓存
            $redis->delete($key);
                $this->tip = $_POST["name"]."已添加";
                header('Location:classification.html');
            }else{
                $this->tip = "不能为空！";
            }
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }

    //分类状态
    public function updatestatus()
    {
        $Classification = new ClassificationModel();
        if ($_POST) {
            if ($_POST["gid"]) {
                $map=array();
                $map['status']=$_POST["status"];
                $return=array();
                $return['id']=$_POST["gid"];
                //连接redis
            $redis = new RedisModel();
            //设置key
            $key = "classification_key";
            // 删除缓存
            $redis->delete($key);
                 $result=$Classification->updateAll ($map,$return);
                 if($result){
                     $content= array(
                         'status' => 1,
                         'gstatus'=>$map['status'],
                         'message' => '新增成功'
                     );
                     exit(json_encode ($content));
                 }else{
                     $content= array(
                         'status' => 2,
                         'message' => '修改失败！'
                     );
                     exit(json_encode ($content));
                 }
                //return json_encode ($map);
            }
        }
    }

    public function classificationedit(){

        $this->name = $this->basename.'修改分类'; // 进行模板变量赋值
        $Classification  = new ClassificationModel();
        $id = $_REQUEST['id'];
        if($id){
            if($_POST){
                if($_POST["id"]){
                    $map=array();
                    $where = array();
                    $map['id']=$id;
                    $where["name"] =$_POST["name"];
                    $this->msgurl=C ("IMAGEURL");
                    $tempT = $Classification->getOne($_POST["id"]);
                    //连接redis
            $redis = new RedisModel();
            //设置key
            $key = "classification_key";
            // 删除缓存
            $redis->delete($key);
                    if($tempT){
                        $Classification->updateOne($map,$where);
                    }else{
                        $this->tip = "错误！";
                    }
                }
                header('Location:classification.html');
                $this->tip = "已保存！";

            }
            $where = array();
            $where["id"] = $id;
            $temp= $Classification->getList($where);

            $Info = array();

            foreach($temp[0] as $k=>$v){
                $Info[$k]=$v;
            }
            $this->Info = $Info;
            $this->msgurl=C ("IMAGEURL");
            // Dump( $TranslateList);exit;
            //$this->code = $code;
        }else{
            echo "error";exit;
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }

    // /*
    //  * 删除
    //  */
    // public function del(){

    //     $id =$_REQUEST["id"];
    //     if($id){
    //         $Classification = new ClassificationModel();
    //         $map = array();
    //         $map["id"] = $id;
    //         $Classification->delOne($map);
    //     }
    //     header('Location:'.$_SERVER['HTTP_REFERER']);
    // }

}


