<?php

/**
 * Created by PhpStorm.
 * User: 亮亮
 * Date: 2018/4/24
 * Time: 10:21
 */
class FaceController extends BaseAdmsController
{
    public function __construct ()
    {
        parent::__construct ();
        $this->basename = '表情-'; // 进行模板变量赋值
        $this->cachepath = WR . '/userdata/cache/face/';
        $this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";

    }

    public function lists ()
    {
        $this->name = $this->basename . '列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 12;
        $Attr = new FaceModel();
        //$status = include WR . "/userdata/publicvar/status.php";
        //$this->switch_type = $status["gift"]["type"];
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|title|product'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $Attr->getListPage ($Wdata, $Page, $PageSize);
        $this->msgurl=C ("IMAGEURL");
        $this->DataList= $ret["list"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        //Dump($this->ListData);exit;
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display ();
    }


    public function add(){
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        $status = C("status");
        //$this->usertypestatus = $status["gift"]["user_type"];
        if($_POST){
            if(!empty($_FILES)) {
                $PutOssarr=$this->_imgupload();
            }
            if($_POST["title"]&&$_POST["price"]){
                $Attr = new FaceModel();
                $map = array();
                $map["title"] = $_POST["title"];
                $map["code"] = $_POST["code"];
                /*$AttrValue = $Attr->getOne($map);
                if($AttrValue){
                    $this->tip = "code或title不能重复！";
                }else{*/
                    $map["title"] = $_POST["title"];
               /* $map["giftid"] = $_POST["giftid"];*/
                    $map["content"] = $_POST["content"];
                    $map["product"] = $_POST["product"];
                    $map["price"] = $_POST["price"];
                    $map["code"] = $_POST["code"];
                    $map["url"] =  $PutOssarr;
                    //delFile(CHCHEPATH_GIFTLIST);
                   //删除redis缓存
                   // $redis=$this->redisconn();
                   // $redis->del('giftlist_10008');

                    $Attr->addOne($map);
                $redis = $this->redisconn();
                $redis->del('facelist_10008');
                    $this->tip = $_POST["title"]."已添加";
                    $this->translate_add($map["code"], $map["title"]);
                    header('Location:applist.html');




            }else{
                $this->tip = "不能为空！";
            }
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }
    /*
	 * 删除
	 */
    public function del(){

        $id =$_REQUEST["id"];
        $img=$_REQUEST["mg"];
        $code=$_REQUEST["code"];
        if($id){
            $AttrValueM = new FaceModel();
            $map = array();
            $map["id"] = $id;
            $AttrValueM->delOne($map);
            //删除翻译
            $this->translate_del($code);
            //delFile(CHCHEPATH_GIFTLIST);
            $redis = $this->redisconn();
            $redis->del('facelist_10008');

            DelOss($img);
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }

    //修改
    public function edit(){

        $this->name = $this->basename.'修改表情'; // 进行模板变量赋值
        $GiftM  = new FaceModel();
        $id = $_REQUEST['id'];
        $code=$_REQUEST["code"];
        if($id){
            if($_POST){
                //delFile(CHCHEPATH_GIFTLIST);
                //删除redis缓存
                $redis=$this->redisconn();
                $redis->del('facelist_10008');
                //删除翻译
                $this->translate_del($code);
                if($_POST["id"]){
                    if(!empty($_FILES)) {
                        $PutOssarr=$this->_imgupload();
                        if(!$PutOssarr){
                            $PutOssarr=$_POST["tmpimg"];
                        }/*else{
                            DelOss($_POST["tmpimg"]);

                        }*/
                    }

                           $map=array();
                            $where = array();
                           $map['id']=$id;
                        $where["code"] = $code;
                        $where["title"] =$_POST["title"];
                        $where["product"] =$_POST["product"];
                        $where["price"] =$_POST["price"];
                        /*$where["giftid"] =$_POST["giftid"];*/
                        $where["url"] =$PutOssarr;
                       // $where["url"] =$PutOssarr[0];
                    $this->msgurl=C ("IMAGEURL");
                        $tempT = $GiftM->getOne($_POST["id"]);
                        if($tempT){
                            //添加翻译
                            $this->translate_add($where["code"], $where["title"]);
                            $GiftM->updateOne($map,$where);
                        }else{
                            $this->tip = "错误！";
                        }
                    }
                    header('Location:applist.html');
                    $this->tip = "已保存！";

                }else{
                    $this->tip = "错误！";
                }


            $where = array();
            $where["id"] = $id;
            $temp= $GiftM->getList($where);

            $Info = array();

            foreach($temp[0] as $k=>$v){
                $Info[$k]=$v;
            }


            $this->Info = $Info;
            $this->msgurl=C ("IMAGEURL");
            // Dump( $TranslateList);exit;
            //$this->code = $code;
        }else{
            echo "error";exit;
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }
    //销售状态修改
    public function updatestatus()
    {
        $GiftM = new GiftModel();
        if ($_POST) {
            if ($_POST["gid"]) {
                $map=array();
                $map['status']=$_POST["status"];
                $return=array();
                $return['id']=$_POST["gid"];
                 $result=$GiftM->updateAll ($map,$return);
                 if($result){
                     $content= array(
                         'status' => 1,
                         'gstatus'=>$map['status'],
                         'message' => '新增成功'
                     );
                     exit(json_encode ($content));
                 }else{
                     $content= array(
                         'status' => 2,
                         'message' => '修改失败！'
                     );
                     exit(json_encode ($content));
                 }
                //return json_encode ($map);
            }
        }
    }

    //图片上传方法
    protected function _upload($savePath) {
        import("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir($savePath);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize =10485760;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg();
            if($_REQUEST['ptesst']==1){
                dump($return);
            }
            //Push_data($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            //$uploadList = $uploadList[0];
            //$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
            //$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

            return $uploadList;
            //import("Extend.Library.ORG.Util.Image");
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            //Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
            //$_POST['image'] = $uploadList[0]['savename'];
        }

    }
    //图片上传封装
    public function _imgupload(){
        $filetype = "image";
        $saveUrl = $this->msgfileurl . $filetype . "/";
        $savePath = WR . $saveUrl;
        //echo $savePath;exit;
        $uploadList = $this->_upload ($savePath);
        if (!empty($uploadList)) {
            // $UserPhotoM = new PhotoModel();
            $PutOssarr = array();
            $returnurlarr = array();
            foreach ($uploadList as $k => $v) {
                $PutOssarr[] = $saveUrl . $v['savename'];
                $returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
            }
            PutOss ($PutOssarr);
            // print_r ($PutOssarr);exit;
            $return = array();
            $return["data"]["url"] = $PutOssarr[0];
            return $PutOssarr[0];
        }
    }


    //礼物名称转化为code

    public function conversion(){
        $Attr = new FaceModel();
        $TranslateM  = new TranslateModel();
        $M=M('face');
        //查询出未添加code的礼物title
       $res=$M->query("select title from t_face where code is null or code=''");
        foreach($res as $k=>$v){
            $nStr = PinyinController::tradition2simple($v['title']);   //将繁体转化为简体
            $string[$v['title']] = PinyinController::get1($nStr, 'utf-8');//将简体转化为拼音
        }
        //循环遍历将code存入数据库并添加到翻译
        foreach($string as $k1=>$v1){
            $Attr->updateOne(array(title=>$k1),array(code=>$v1));

            $tran = array();
            $where["code"] = strtoupper($v1);
            $tran["code"] = strtoupper($v1);
            $tran["content"] = $k1;
            $tempT = $TranslateM->getOne($where);
            if($tempT){
                continue;
                // return $TranslateM->updateOne($where,array("content"=>$tran["content"]));
            }else{
                $tran["lang"]="zh-tw";
                $TranslateM->addOne($tran);
            }
        }

        //print_r($string);
        header('Location:'.$_SERVER['HTTP_REFERER']);

    }

}







