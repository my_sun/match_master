<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/27
 * Time: 17:25
 */

class WithdrawalController extends BaseAdmsController
{
    public function __construct()
    {
        parent::__construct();

    }

    //提现订单列表
    public function lists()
    {
        $this->name = '提现列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 200;
        $withd = new PMoneyModel();
        $Wdata = array();
        if(isset($_GET['type'])){
            if($_GET['type']==1){
                $Wdata['_string'] ='type=4 OR type=5 OR type=6 OR type=7';
            }elseif($_GET['type']==2){
                $Wdata['_string'] ='type=4 OR type=6 OR type=7';
            }else{
                $Wdata['_string'] ='type=5';
            }
        }else{
            $Wdata['_string'] = 'type=4 OR type=5 OR type=6 OR type=7';
        }
        if(isset($_GET['is_tixian'])){
            if($_GET['is_tixian']==1){
                $Wdata['is_tixian'] =1;
            }elseif($_GET['is_tixian']==2){
                $Wdata['is_tixian'] ='2';
            }else{
                $Wdata['is_tixian'] ='3';
            }
        }
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $withd->getListPage($Wdata, $Page, $PageSize);
        $allmoney=0;
        foreach ($ret["list"] as $k => $v) {

            $ret["list"][$k]['realmoney'] = $v['money'] * 0.01 * 6.5;

            //需求:用户类型1提现在原来基础上*0.05
            $userinfo=$this->get_user($v['uid']);
            if($userinfo['user_type']==1){
                $ret["list"][$k]['realmoney'] = $v['money'] * 0.01 * 6.5;
            }
            if($v['account_type']==4){
                $ret["list"][$k]['realmoney'] = $v['money']* 0.2;
                //需求:用户类型3魅力值可以兑换钻石，兑换钻石数为魅力值*1.7
                if($userinfo['user_type']==3){
                    $ret["list"][$k]['realmoney'] = $v['money']* 1.7;
                }
            }
            $msgmon = M('m_money')->where(array('uid' => $v['uid']))->find();
            //获取奖励魅力值
            $ret["list"][$k]['awardmoney']=$v['awardmoney'];
            $ret["list"][$k]['awardrealmoney']=$v['awardmoney']* 0.01 * 6.5;
            $ret["list"][$k]['allrealmoney']=$v['awardmoney']* 0.01 * 6.5+$v['money'] * 0.01 * 6.5;
            $ret["list"][$k]['username']=$msgmon['commonpay']?$msgmon['commonpay']:" ";
            $ret["list"][$k]['phonenum']=$msgmon['otherpay']?$msgmon['otherpay']:" ";

            $allmoney=$allmoney+$v['money'];
        }
        $datamsg = M('m_money');
        $this->ListData = $ret["list"];
        $this->is_tixian = $_GET['is_tixian'];
        $this->type = $_GET['type'];
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->allmoney=$allmoney;
        $this->display();
    }
    //魅力值排行
    public function paihang()
    {
        $yue = $_GET["yue"] ? $_GET["yue"] : date('Y-m');
        $this->name = $yue.'月排行'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $gender = $_GET["gender"] ? $_GET["gender"] : 0;
        $PageSize = 200;
        $PMoneyM = new PMoneyModel();
        $Wdata = array();
        $Wdata['type']=array('in',array(1,2,3,12,13));
        $Wdata['money'] = array('gt',0);
        
        
        if($yue){
            $yueArr = $this->getShiJianChuo($yue);
            $Wdata["paytime"]=array(array('egt',$yueArr["begin"]),array('elt',$yueArr["end"]));
        }
        if($_GET["gender"]){
            if($_GET["gender"]==1){
                $Wdata['gender'] = $_GET["gender"];
            }else{
                $Wdata['gender'] = 2;
            }
            
        }
        $ret = $PMoneyM->getListGroupOrder($Wdata);
        //echo $PMoneyM->getLastSql();
        //Dump($ret);exit;
        $qallmoney=0;
        foreach ($ret as $k=>$v){
            $ret[$k]["user"]=$this->get_user($v["uid"]);
            $qallmoney=$qallmoney+$v['allmoney'];
        }


        
        $yuelist = array();
        $currentTime = time();
        $cyear = floor(date("Y",$currentTime));
        $cMonth = floor(date("m",$currentTime));
        for($i=0;$i<6;$i++){
            $nMonth = $cMonth-$i;
            $cyear = $nMonth == 0 ? ($cyear-1) : $cyear;
            $nMonth = $nMonth <= 0 ? 12+$nMonth : $nMonth;
            $yuelist[]= $cyear."-".$nMonth;
        }
        
        $this->yuelist=array_reverse($yuelist);
        $this->ListData = $ret;
        $this->qallmoney = $qallmoney;
        $this->gender =$gender;
        $this->yue = $yue;
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display();
    }
   protected function getShiJianChuo($nianyue=0){
       $arr = explode("-", $nianyue);
       $nian = $arr[0];
       $yue = $arr[1];
        if(empty($nian) || empty($yue)){
            $now = time();
            $nian = date("Y",$now);
            $yue =  date("m",$now);
        }
        $time['begin'] = mktime(0,0,0,$yue,1,$nian);
        $time['end'] = mktime(23,59,59,($yue+1),0,$nian);
        return $time;
    }
    //魅力值记录
    public function meilizhijilu()
    {
        $this->name = '魅力值记录'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 30;
        $PMoneyM = new PMoneyModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['to_uid|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        if (isset($_GET['moneytype']) && $_GET['moneytype'] != "") {
            if($_GET['moneytype']==2){
                $Wdata['type']=array('in',array(2,8));
            }elseif($_GET['moneytype']==1){
                $Wdata['type']=1;
            }elseif($_GET['moneytype']==3){
                $Wdata['type']=3;
            }elseif($_GET['moneytype']==12){
                $Wdata['type']=12;
            }elseif($_GET['moneytype']==13){
                $Wdata['type']=13;
            }elseif($_GET['moneytype']==15){
                $Wdata['type']=15;
            }
        }else{
            $Wdata['type']=array('in',array(1,2,3,8,12,13,14,15));
        }
        if(isset($_GET['dateStart'])&$_GET['dateStart']!=""&$_GET['dateEnd']==""){
            $regtime=strtotime($_GET['dateStart']);
            $Wdata['paytime']=array('GT',$regtime);
        }
        if(isset($_GET['dateEnd'])&&$_GET['dateEnd']!=""&$_GET['dateStart']==""){
            $regtime=strtotime($_GET['dateEnd']);
            $Wdata['paytime']=array('LT',$regtime);
        }
        if(isset($_GET['dateEnd'])&&$_GET['dateEnd']!=""&$_GET['dateStart']!=""){
            $regtime=strtotime($_GET['dateEnd']);
            $regstarttime=strtotime($_GET['dateStart']);
            $Wdata['paytime']=array('between',array($regstarttime,$regtime));;
        }

        $Wdata['money'] = array('gt',0);
        $ret = $PMoneyM->getListPage($Wdata, $Page, $PageSize);
        
       // Dump($ret);exit;
        $this->ListData = $ret["list"];
        $this->moneytype=$_GET['moneytype'];
        $this->status = $Wdata['is_tixian'];
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->actionName =  ACTION_NAME;
        $this->display();
    }
    public function delmeilizhi(){
        $id = $_GET['id'] ? $_GET['id'] : 0;
        $PMoneyM = new PMoneyModel();
        $PMoneyInfo = $PMoneyM->getOne(array('id'=>$id));
        //Dump($PeiliaoMoneyInfo);exit;
        if($PMoneyInfo){
            $this->m_money_dec($PMoneyInfo["uid"], $PMoneyInfo["money"]);
            $PMoneyDelM = new PMoneyDelModel();
            unset($PMoneyInfo["id"]);
            $PMoneyDelM->addOne($PMoneyInfo);
            $PMoneyM->delOne(array('id'=>$id));
        }
        echo '<script language="JavaScript">
			window.location.href="'.$_SERVER['HTTP_REFERER'].'";
			</script>';
        //header('Location:/adm/orderlist');
        
    }
    //魅力值找回
    public function meilizhijiludel()
    {
        $this->name = '魅力值找回'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 30;
        $PMoneyDelM = new PMoneyDelModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['to_uid|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['type']=array('in',array(1,2,3,8));
        $Wdata['money'] = array('gt',0);
        $ret = $PMoneyDelM->getListPage($Wdata, $Page, $PageSize);
        
        // Dump($ret);exit;
        $this->ListData = $ret["list"];
        $this->status = $Wdata['is_tixian'];
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->actionName =  ACTION_NAME;
        $this->display("meilizhijilu");
    }
    public function zhaohuid(){
        $id = $_GET['id'] ? $_GET['id'] : 0;
        $PMoneyM = new PMoneyModel();
        $PMoneyDelM = new PMoneyDelModel();
        $PeiliaoMoneyInfo = $PMoneyDelM->getOne(array('id'=>$id));
        //Dump($PeiliaoMoneyInfo);exit;
        if($PeiliaoMoneyInfo){
            $this->m_money_add($PeiliaoMoneyInfo["uid"], $PeiliaoMoneyInfo["money"]);
            unset($PeiliaoMoneyInfo["id"]);
            $PMoneyM->addOne($PeiliaoMoneyInfo);
            $PMoneyDelM->delOne(array('id'=>$id));
        }
        echo '<script language="JavaScript">
			window.location.href="'.$_SERVER['HTTP_REFERER'].'";
			</script>';
        //header('Location:/adm/orderlist');
        
    }
    public function judge()
    {
        $Wdata = array();
        if ($_POST["is_tixian"] && $_POST["id"]) {
            $status = $_POST["is_tixian"];
            $id = $_POST["id"];
            $uid = $_POST["uid"];
            $paytime=$_POST["paytime"];
            $userinfo = $this->get_user($uid);
            $puids=M('admin_three_user')->getField('id',true);
            $money = $_POST['money'];
            $awardmoney=$_POST['awardmoney'];
            $type=$_POST['type'];
            $withd = new PMoneyModel();
            $userbeseM=new UserBaseModel();
            $msgmon = M('m_money')->where(array('uid' => $uid))->find();

            $res = $withd->updateOne(array('id' => $id), array('is_tixian' => $status));
            if ($res) {
                if(in_array($uid,$puids)){
                    $threeuser=M('p_money')->where(array('pid'=>$uid,'type'=>10,'paytime'=>$paytime))->getField('id',true);
                    foreach($threeuser as $k=>$v){
                        if($status == 3){
                            $withd->updateOne(array('id' => $v), array('is_tixian' =>4));
                        }else{
                            $withd->updateOne(array('id' => $v), array('is_tixian' => $status));
                        }
                    }
                    if(!$threeuser){
                        $wdata['type']=array('in',array(8,9));
                        $wdata['pid']=$uid;
                        $wdata['paytime']=$paytime;
                        $threeshereuser=M('p_money')->where($wdata)->getField('id',true);
                        foreach($threeshereuser as $k=>$v){
                            if($status == 3){
                                $withd->updateOne(array('id' => $v), array('is_tixian' =>4));
                            }else{
                                $withd->updateOne(array('id' => $v), array('is_tixian' => $status));
                            }
                        }
                    }
                }else{
                    if ($status == 3) {
                        $datamoney = array();
                        $datamoney['leftmoney'] = $msgmon['leftmoney'] + $money;
                        $datamoney['awardmoney'] = $msgmon['awardmoney'] + $awardmoney;
                        M('m_money')->where(array('uid' => $uid))->save($datamoney);
                    }/*else{
                        if($type==4){
                            $lgold=$userbeseM->where(array('uid'=>$uid))->getField('gold');
                            if($userinfo["user_type"]==3){
                                $rgold=$lgold+$money*0.01*25*4;
                            }else{
                                $rgold=$lgold+$money*0.01*25*2;
                            }
                            $msgs=$userbeseM->updateOne(array('uid'=>$uid),array('gold'=>$rgold));
                            if($msgs){
                                $this->get_user($uid,1);
                                $this->sendsysmsg($uid, "魅力值轉換金幣已通過審核");
                            }
                        }
                    }*/
                }
                $reuslt = array(
                    'status' => '1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            } else {
                $reuslt = array(
                    'status' => '2',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            }
        }

    }

    public function updatepaihangone(){
        $redis = $this->redisconn();
        $keyexitone=$redis->exists('getranklist-1');
        if($keyexitone){
            $redis->del($keyexitone);
        }
        $time=time();
        $p=date('d', $time);
        $PMoneyM = new PMoneyModel();
        $Wdata = array();
        // $Wdata['t_p_money.product'] = $product;
        $Wdata['type'] = array('in', array(1,2,3,12));
        $Wdata['money'] = array('gt', 0);
        $yueArr = $this->getShiJianChuo();
        $Wdata["paytime"] = array(array('egt', $yueArr["begin"]), array('elt', $yueArr["end"]));
        $ret = $PMoneyM->getListGroupOrder($Wdata);
        foreach ($ret as $k => $v) {
            $res[$k]["user"] = $this->get_user($v["uid"]);
            $res[$k]["uid"] = $v["uid"];
            $res[$k]["allmoney"]=round($v["allmoney"]);
        }
        $highmoneyone=$res[0]["allmoney"];
        $highmoneythree=$res[2]["allmoney"];
        $highmoneyfive=$res[4]["allmoney"];
        $users=array('378994','378992','378988');
        $resb=array();
        foreach($users as $k1=>$v1){
            $resb[$k1]["user"] = $this->get_user($v1);
            $resb[$k1]["uid"] = $v1;
            $redis_fakeuid='fakeuid-'.$v1;
            if($p=='01'){
                $redis->del($redis_fakeuid);
            }
            if($k1==0){
                if($redis->exists($redis_fakeuid)){
                    $fakemoney=$redis->get($redis_fakeuid);
                    $gap_money=$highmoneyone-$fakemoney;
                    if($gap_money>0){
                        if($gap_money>1000){
                            $resb[$k1]["allmoney"] =rand($highmoneyone-1000,$highmoneyone+1000);
                        }else{
/*                            $resb[$k1]["allmoney"] =rand($highmoneyone-1000,$highmoneyone+1000);*/
                            $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+1000);
                        }
                    }else{
//                        $resb[$k1]["allmoney"] =rand($highmoneyone-1000,$highmoneyone+1000);

                 $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+500);
                    }

                }else{
                    $resb[$k1]["allmoney"] =rand($highmoneyone,$highmoneyone+1000);

                }
                $redis->set($redis_fakeuid,$resb[$k1]["allmoney"]);
            }elseif($k1==1){

                if($redis->exists($redis_fakeuid)){
                    $fakemoney=$redis->get($redis_fakeuid);
                    $gap_money=$highmoneythree-$fakemoney;
                    if($gap_money>0){
                        if($gap_money>1000){
                            $resb[$k1]["allmoney"] =rand($highmoneythree-1000,$highmoneythree+1000);
                        }else{
/*                            $resb[$k1]["allmoney"] =rand($highmoneythree-1000,$highmoneythree+1000);*/

                               $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+1000);
                        }
                    }else{
                       $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+500);
/*                        $resb[$k1]["allmoney"] =rand($highmoneythree-1000,$highmoneythree+1000);*/

                    }

                }else{
                    $resb[$k1]["allmoney"] =rand($highmoneythree,$highmoneythree+1000);

                }
                $redis->set($redis_fakeuid,$resb[$k1]["allmoney"]);
            }elseif($k1==2){
                if($redis->exists($redis_fakeuid)){
                    $fakemoney=$redis->get($redis_fakeuid);
                    $gap_money=$highmoneyfive-$fakemoney;
                    if($gap_money>0){
                        if($gap_money>1000){
                            $resb[$k1]["allmoney"] =rand($highmoneyfive-1000,$highmoneyfive+1000);
                        }else{
                            $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+1000);
/*                            $resb[$k1]["allmoney"] =rand($highmoneyfive-1000,$highmoneyfive+1000);*/

                        }
                    }else{
                        $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+500);
/*                        $resb[$k1]["allmoney"] =rand($highmoneyfive-1000,$highmoneyfive+1000);*/

                    }

                }else{
                    $resb[$k1]["allmoney"] =rand($highmoneyfive,$highmoneyfive+1000);

                }
                $redis->set($redis_fakeuid,$resb[$k1]["allmoney"]);
            }

        }
        $res=array_merge($resb,$res);
        $date = array_column($res, 'allmoney');
        array_multisort($date,SORT_DESC ,$res);
        /*if($p==1){
            $res=array();
        }*/
        $redis_rankinglist=json_encode($res);
        $getres=$redis->set('getranklist-1',$redis_rankinglist,0,0,60*60*12);
        //加入日志中，以便查看
        log_result(json_encode($res,true),'checkpaihang');
    }

    //统计主播在线时长 2分钟请求一次
    public function getupcompere(){





        $txkey=M('txmy')->where(array('product'=>25100))->getField('miyao');

        //查询所有用户主播id
        $uids = M("compere_info")->select();
        $useronlinelist = array_column($uids,"uid");
        $tentxunstatus = tencent_onlinestate($useronlinelist,$txkey);
        foreach($tentxunstatus as $ival){
            if ($ival["State"] == "Online") {
                $onlineid = M("compere_time")->where(array("uid"=>$ival['To_Account'],"endtime"=>0))->getField("id");
                if(empty($onlineid)){
                    $info = array(
                        "startime"=>time(),
                        "ontime" => date("Ymd"),
                        "uid"=>$ival['To_Account']
                    );
                    M("compere_time")->add($info);

                    $nocall = array(
                        "uid"=>$ival['To_Account'],
                        "ontime" => date("Ymd")
                    );
                    $oncallid = M("compere_nocall")->where(array("uid"=>$ival['To_Account'],"ontime"=>date("Ymd")))->getField("id");
                    if(empty($oncallid)){
                        M("compere_nocall")->add($nocall);
                    }

                }



            }
            if($ival["State"] == "Offline" || $ival["State"] == "PushOnline"){
                $onlineid = M("compere_time")->where(array("uid"=>$ival['To_Account'],"endtime"=>0))->getField("id");
                if(!empty($onlineid)){
                    M("compere_time")->where(array("id"=>$onlineid))->save(array("endtime"=>time()));
                }
            }


        }




        print "";
        print date("Y-m-d H:i:s");
        print "<br>";
//<!--JS 页面自动刷新 -->
        print ("<script type=\"text/javascript\">");
        print ("function fresh_page()");
        print ("{");
        print ("window.location.reload();");
        print ("}");
        print ("setTimeout('fresh_page()',10000);");
        print ("</script>");

    }


    //统计主播在线时长 1分钟请求一次
    public function getupcompere2(){

        $txkey=M('txmy')->where(array('product'=>24112))->getField('miyao');

        //查询所有用户主播id
        $where = array(
            "user_type"=>3,
            'product'=>24112
        );
        $uids = M("user_base")->where($where)->select();
        $useronlinelist = array_column($uids,"uid");
        $tentxunstatus = tencent_onlinestate($useronlinelist,$txkey);
        foreach($tentxunstatus as $ival){
            if ($ival["State"] == "Online") {
                $onlineid = M("compere_time2")->where(array("uid"=>$ival['To_Account'],"endtime"=>0))->getField("id");
                if(empty($onlineid)){
                    $info = array(
                        "startime"=>time(),
                        "ontime" => date("Ymd"),
                        "uid"=>$ival['To_Account']
                    );
                    M("compere_time2")->add($info);


                }



            }
            if($ival["State"] == "Offline" || $ival["State"] == "PushOnline"){
                $onlineid = M("compere_time2")->where(array("uid"=>$ival['To_Account'],"endtime"=>0))->getField("id");
                if(!empty($onlineid)){
                    M("compere_time2")->where(array("id"=>$onlineid))->save(array("endtime"=>time()));
                }
            }


        }




        print "";
        print date("Y-m-d H:i:s");
        print "<br>";
//<!--JS 页面自动刷新 -->
        print ("<script type=\"text/javascript\">");
        print ("function fresh_page()");
        print ("{");
        print ("window.location.reload();");
        print ("}");
        print ("setTimeout('fresh_page()',10000);");
        print ("</script>");

    }

    //查询在线状态 腾讯

    public function getoneuser(){

        $product = $_GET['product']?$_GET['product']:25100;
        $uid = $_GET['uid'];
        $txkey=M('txmy')->where(array('product'=>$product))->getField('miyao');
        //查询所有用户主播id
        $uids = array(array("uid"=>$uid));

        $useronlinelist = array_column($uids,"uid");

        $tentxunstatus = tencent_onlinestate($useronlinelist,$txkey);


        dump($tentxunstatus);die;

    }




    //爬虫获取新浪的实时汇率记录   每分钟更新一次
    public function getrate(){

        $uri = 'http://hq.sinajs.cn/rn=list=fx_scnyhkd';
        $uri_hkd = 'http://hq.sinajs.cn/rn=list=fx_shkdcny';
        $info_cny = file_get_contents($uri);
        $info_hkd = file_get_contents($uri_hkd);
        $ic_cny = iconv("GBK","UTF-8",$info_cny);
        $ic_hkd = iconv("GBK","UTF-8",$info_hkd);

        $arr = explode(",",$ic_cny);
        $arr_hkd = explode(",",$ic_hkd);
        $redis = $this->redisconn();
        $money = $redis->get("init_rate_money_cny");
        $money_hkd = $redis->get("init_rate_money_hkd");
        $p = 50;//设置收益

        if(false){
            $redis->set("init_rate_money_cny",0);
            $redis->set("init_rate_money_hkd",0);
            $redis->set("that_cnyhkd",0);
            $redis->set("hkd_money",0);
            $redis->set("cny_money",0);
            $redis->set("that_hkdcny",0);
        }

        if(empty($money) && empty($money_hkd)){
            $money = 1000000;//初始值
            $redis->set("init_rate_money_cny",$money);
        }
        $init_rate_money_cny = $redis->get("init_rate_money_cny");
        $init_rate_money_hkd = $redis->get("init_rate_money_hkd");
        if($init_rate_money_cny){//如果当前是人民币账户    则计算这次兑换港元+100 是否大于等于 上次价格的兑换
            var_dump("当前人民币账户:".$init_rate_money_cny);
            //获取当前人民币兑港元汇率得出金额
            $m = $init_rate_money_cny* $arr[1];
            var_dump("-----当前兑换港元:".$m);
            //获取上次人民币兑港元汇率得出金额
            $that_cnyhkd  = $redis->get("that_cnyhkd");//人民币1：HKD 价格
            if(empty($that_cnyhkd)){
                $redis->set("that_cnyhkd",$arr[1]);
            }else{

                $hkd_money = $redis->get("hkd_money");
                var_dump("-----上次兑换港元:".$hkd_money);
                $if = $m - $hkd_money;
               if($if >= $p){
                   //保存此次交易价格
                   $redis->set("hkd_money",$m);
                   $redis->set("cny_money",$init_rate_money_cny);
                    //促成交易 //将rmb兑换成港币 存入数据库
                   $redis->set("init_rate_money_cny",0);
                   $redis->set("init_rate_money_hkd",$m);
                   $date = date("Y-m-d H:i:s",time());
                   M("ratedemo")->add(array("cny_hkd"=>$arr[1],'hkd'=>$m,'cny'=>$init_rate_money_cny,'ctime'=>$date,'money'=>$if));

                   //保存此次汇率
                   //$redis->set("that_cnyhkd",$arr[1]);
                   //$redis->set("that_hkdcny",$arr_hkd[1]);
               }else{
                   //保存此次汇率
//                   $redis->set("that_cnyhkd",$arr[1]);
//                   $redis->set("that_hkdcny",$arr_hkd[1]);
               }



            }


        }elseif ($init_rate_money_hkd){
            var_dump("当前港币账户：".$init_rate_money_hkd);
            //获取当前港元---人民币 汇率得出金额
            $m = $init_rate_money_hkd* $arr_hkd[1];
            var_dump("-----当前汇率可换人民币:".$m);
            //获取上次人民币兑港元汇率得出金额
            $that_hkdcny = $redis->get("that_hkdcny");//人民币1：HKD 价格

            if(empty($that_hkdcny)){
                $redis->set("that_hkdcny",$arr_hkd[1]);
            }else{

                $cny_money = $redis->get("cny_money");
                var_dump("-----上次兑换人民币:".$cny_money);
                $if = $m - $cny_money;
                if($if > $p){

                    //保存此次交易价格
                    $redis->set("cny_money",$m);
                    $redis->set("hkd_money",$init_rate_money_hkd);
                    //促成交易 //将rmb兑换成港币 存入数据库
                    $redis->set("init_rate_money_hkd",0);
                    $redis->set("init_rate_money_cny",$m);
                    $date = date("Y-m-d H:i:s",time());
                    M("ratedemo")->add(array("hkd_cny"=>$arr_hkd[1],'cny'=>$m,'hkd'=>$init_rate_money_hkd,'ctime'=>$date,'money'=>$if));

                    //$redis->set("that_cnyhkd",$arr[1]);
                    //$redis->set("that_hkdcny",$arr_hkd[1]);

                }else{
                    //保存此次汇率
//                    $redis->set("that_cnyhkd",$arr[1]);
//                    $redis->set("that_hkdcny",$arr_hkd[1]);
                }

            }

        }

        //将当前实时获取的价格存入 redis  that_cnyhkd
//        $redis = $this->redisconn();
//        if($arr[1]){
//            $redis->set("that_cnyhkd",$arr[1]);
//        }









        print "----------------------";
        print date("Y-m-d H:i:s");
        print "<br>";
//<!--JS 页面自动刷新 -->
        print ("<script type=\"text/javascript\">");
        print ("function fresh_page()");
        print ("{");
        print ("window.location.reload();");
        print ("}");
        print ("setTimeout('fresh_page()',10000);");
        print ("</script>");
    }


    //后台进行交易计算




}