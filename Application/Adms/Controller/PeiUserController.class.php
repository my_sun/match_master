<?php
class PeiUserController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
					
	}
	
	/**
	 * 用户列表测试用
	 */
	public function PaiHang(){
		$Wdata = array();
		$PeiUserM = new PeiUserModel();
		$PeiGuanlianM = new PeiGuanlianModel();
		$PeiliaoMoneyM = new PeiliaoMoneyModel();		
		
		$moneys_all = $PeiliaoMoneyM->sum_money(array());
		
		$ret = $PeiUserM->getListPage($Wdata,$pageNum,$pageSize,$order);
		$arsort = array();
		$peilist = array();
		foreach($ret as $k=>$v){
			//$puserlist = $PeiGuanlianM->getList(array("pid"=>$v["pid"]));
			$all_moneys = 0; 
			$weiti_moneys = 0; 
			$yiti_moneys = 0; 
			$today_moneys = 0; 
			
			//foreach($puserlist as $k1=>$v1){
				$where = array();
				$where['pid']=$v["pid"];
				$all_money = $PeiliaoMoneyM->sum_money($where);
				$all_moneys = $all_money;
				
				$weiti_money_where = $where;
				$weiti_money_where['is_tixian'] = 0;
				$weiti_money = $PeiliaoMoneyM->sum_money($weiti_money_where);
				$weiti_moneys =$weiti_money;
				
				$yiti_money_where = $where;
				$yiti_money_where['is_tixian'] = 1;
				$yiti_money = $PeiliaoMoneyM->sum_money($yiti_money_where);
				$yiti_moneys = $yiti_money;
				
				$today_money_where = $where;
				$today_money_where['paytime'] = array('egt',strtotime(date("Y-m-d 00:00:00")));
				$today_money = $PeiliaoMoneyM->sum_money($today_money_where);
				$today_moneys = $today_money;
				//$ret[$k]['userlist'][$k1] = $v1['uid'];
				//$peilist[$v["pid"]]['userlist'][$k1]=$this->get_user($v1['uid']);
			//}
			$peilist[$v["pid"]] = $v;
			$peilist[$v["pid"]]['all_money'] = $all_moneys;
			$peilist[$v["pid"]]['weiti_money'] = $weiti_moneys;
			$peilist[$v["pid"]]['yiti_money'] = $yiti_moneys;
			$peilist[$v["pid"]]['today_money'] = $today_moneys;
			$arsort[$v["pid"]]=$weiti_moneys;
			
		}
		arsort($arsort);
		foreach($arsort as $k=>$v){
			$arsort[$k] = $peilist[$k];
		}
		//Dump($arsort);exit;
		$this->PeiList = $arsort;
		$this->display("PaiHang");
		
		
	}
	public function Index(){
		$this->name = 'Login'; // 进行模板变量赋值
		
	
	}
	public function Peiuserlist(){
		$pid = $_GET['pid']?$_GET['pid']:619774356;
		$PeiGuanlianM = new PeiGuanlianModel();
		$puserlist = $PeiGuanlianM->getList(array("pid"=>$pid));
		$res = array();
		foreach($puserlist as $k=>$v){
				$uinfo = $this->get_user($v['uid']);
				$res[] = $uinfo;
		}
		
		$this->PeiList = $res;
		$this->display("Peiuserlist");
	}
	public function Peilist(){
		$this->name = '陪聊列表'; // 进行模板变量赋值
		$PeiUserM = new PeiUserModel();
		$Peilist = $PeiUserM->getList(array());
		//Dump($Peilist);exit;
		$this->PeiList = $Peilist;
		$this->display("Peilist");
	}
	public function edituser(){
		$this->name = '修改信息'; // 进行模板变量赋值
		$uid=$_GET["uid"];

	}
	public function setproduct(){
		$product=$_GET["product"];
		$uid=$_GET["uid"];
		if($uid){
			if($product=="10008"){
				$product="10108";
			}else{
				$product="10008";
			}
			$where = array('uid'=>$uid);
			$User_baseM = new UserBaseModel();
			$UserInfo = $User_baseM->updateOne($where,array("product"=>$product));
			$this->get_user($uid,1);//更新用户缓存信息
			file_get_contents("http://47.91.136.204:81/Adms/View/Setusercache?uid=".$uid);
			file_get_contents("http://47.89.47.92:81/Adms/View/Setusercache?uid=".$uid);
		}
		header('Location:'.$_SERVER['HTTP_REFERER']);

	}
	public function userlist(){
		$this->name = '用户列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		
		$Wdata = array();
		$Wdata['user_type']=3;
		if(isset($_GET['gender'])&&$_GET['gender']!="")
			$Wdata['gender']=$_GET['gender'];
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerwordarr = explode(" ",$_GET['kerword']);
			$kerword = $_GET['kerword'];
			if(in_array($kerwordarr[0],C("_PRODUCTS"))){
				$Wdata['product']=$kerwordarr[0];
				if($kerwordarr[1]){
					$kerword = $kerwordarr[1];
				}else{
					$kerword = 0;
				}
			}
			if($kerword)
			$Wdata['uid|nick_name|channelid|pid|phonetype|language|version|country']=array('like',"%".$kerword."%");
			
		}
		if($_GET['channelid']&&$_GET['channelid']!="")
			$Wdata['channelid']=$_GET['channelid'];
		if(isset($_GET['dateStart'])&&$_GET['dateStart']!="")
		$Wdata["regtime"]=array(array('egt',$_GET["dateStart"]),array('elt',$_GET["dateEnd"]));
		$UserBaseM = new UserBaseModel();

		$ret = $UserBaseM->getlistOrder($Wdata,$Page,$pageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
	
		foreach($ret['listSearch'] as $k=>$v){
			
			$ret['listSearch'][$k]=$this->get_user($v["uid"]);
			$ret['listSearch'][$k]["token"] = $v["token"];
			$ret['listSearch'][$k]["password"] = $v["password"];
			$ret['listSearch'][$k]["account"] = $v["account"];
			$ret['listSearch'][$k]["user_type"] = $v["user_type"];
			$ret['listSearch'][$k]["regtimeint"] =date("Y/m/d H:i:s",$v["regtimeint"]);
			$photo_num = 0;
			if(is_array($ret['listSearch'][$k]["listImage"])){
				$photo_num = count($ret['listSearch'][$k]["listImage"]);
			}
			$ret['listSearch'][$k]["photo_num"] = $photo_num;
			$Peiidkey = "Peiid_".$v["uid"];
			$pid = $this->redis->get($Peiidkey);
			$ret['listSearch'][$k]["Peiid"]=$pid;

		} 
		//Dump($this->get_user(137295));exit;
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Adms/PeiUser/Peiuserlist.html";
		$this->display("userlist");
	}
	public function AddPeiUser(){
		$this->name = '添加陪聊'; // 进行模板变量赋值
		$this->action = '/Adms/PeiUser/AddPeiUser.html';
		if($_POST["pid"]){
			$password = trim($_POST["password"]) ? trim($_POST["password"]) : "123456";
			$AddArray["pid"]=trim($_POST["pid"]);
			$AddArray["password"]=$password ;
			$AddArray["payinfo"]=$_POST["payinfo"];
			$AddArray["uname"]=$_POST["uname"];
			$PeiUserM = new PeiUserModel();
			$pid = $PeiUserM->addOne($AddArray);
			if($pid){
				header("Location:/Adms/PeiUser/Peilist");
			}
		}

		$this->display("AddPeiUser");
	}
	public function EditPeiUser(){
		$this->name = '修改信息'; // 进行模板变量赋值
		$this->action = '/Adms/PeiUser/EditPeiUser.html';
		$this->type = 'EditPeiUser';
		$Pid=$_REQUEST["pid"];
		$PeiUserM = new PeiUserModel();
		//Dump($_POST);exit;
		if($_POST["pid"]){
			$WhereArray["pid"]=trim($_POST["pid"]);
			$EditArray["password"]=trim($_POST["password"]);
			$EditArray["payinfo"]=$_POST["payinfo"];
			$EditArray["uname"]=$_POST["uname"];
			
			$pid = $PeiUserM->updateOne($WhereArray,$AddArray);
			
				header("Location:/Adms/PeiUser/Peilist");
			
		}
		
			$WhereArray["pid"]=trim($Pid);
			$PeiInfo = $PeiUserM->getOne($WhereArray);
			
		$this->PeiInfo = $PeiInfo;
		$this->Pid = $Pid;
		$this->display("AddPeiUser");
	}
	public function ticheng(){
		$this->name = '提成列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		
		$Wdata = array();

		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword = $_GET['kerword'];
			$Wdata['uid|to_uid|pid|translateid']=array('like',"%".$kerword."%");
		}

		$PeiliaoMoneyM = new PeiliaoMoneyModel();

		$ret = $PeiliaoMoneyM->getListPage($Wdata,$Page,$pageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
		
		foreach($ret['list'] as $k=>$v){
			
			$ret['list'][$k]["UserInfo"]=$this->get_user($v["uid"]);
			$ret['list'][$k]["ToUserInfo"]=$this->get_user($v["to_uid"]);
			
	
		} 
		//Dump($ret);exit;
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Adms/PeiUser/ticheng.html";
		$this->display("ticheng");
	
	}
	public function delticheng(){
		$id = $_GET['id'] ? $_GET['id'] : 0;
		$PeiliaoMoneyM = new PeiliaoMoneyModel();
		$PeiliaoMoneyInfo = $PeiliaoMoneyM->getOne(array('id'=>$id));
		//Dump($PeiliaoMoneyInfo);exit;
		if($PeiliaoMoneyInfo){
			$PeiliaoMoneyDelM = new PeiliaoMoneyDelModel();
			unset($PeiliaoMoneyInfo["id"]);
			$PeiliaoMoneyDelM->addOne($PeiliaoMoneyInfo);
			$PeiliaoMoneyM->delOne(array('id'=>$id));
		}
		echo '<script language="JavaScript">
			window.location.href="'.$_SERVER['HTTP_REFERER'].'";
			</script>';
		//header('Location:/adm/orderlist');

	}	
	public function zhaohui(){
		$this->name = '已删除提成列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		
		$Wdata = array();

		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword = $_GET['kerword'];
			$Wdata['uid|to_uid|pid']=array('like',"%".$kerword."%");
		}

		$PeiliaoMoneyDelM = new PeiliaoMoneyDelModel();

		$ret = $PeiliaoMoneyDelM->getListPage($Wdata,$Page,$pageSize);
		
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
		
		foreach($ret['list'] as $k=>$v){
			
			$ret['list'][$k]["UserInfo"]=$this->get_user($v["uid"]);
			$ret['list'][$k]["ToUserInfo"]=$this->get_user($v["to_uid"]);
			
	
		} 
		
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Adms/PeiUser/zhaohuiticheng.html";
		$this->display("zhaohui");
	
	}
	public function zhaohuid(){
		$id = $_GET['id'] ? $_GET['id'] : 0;
		$PeiliaoMoneyM = new PeiliaoMoneyModel();
		$PeiliaoMoneyDelM = new PeiliaoMoneyDelModel();
		$PeiliaoMoneyInfo = $PeiliaoMoneyDelM->getOne(array('id'=>$id));
		//Dump($PeiliaoMoneyInfo);exit;
		if($PeiliaoMoneyInfo){
			
			unset($PeiliaoMoneyInfo["id"]);
			$PeiliaoMoneyM->addOne($PeiliaoMoneyInfo);
			$PeiliaoMoneyDelM->delOne(array('id'=>$id));
		}
		echo '<script language="JavaScript">
			window.location.href="'.$_SERVER['HTTP_REFERER'].'";
			</script>';
		//header('Location:/adm/orderlist');

	}		
}

?>
