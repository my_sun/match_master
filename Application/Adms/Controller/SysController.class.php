<?php
class SysController extends BaseAdmsController
{
    public function __construct()
    {
        parent::__construct();

    }

    public function updatesvncode()
    {
        $this->display();
    }

    /**
     * 数据导出
     * @param array $title 标题行名称
     * @param array $data 导出数据
     * @param string $fileName 文件名
     * @param string $savePath 保存路径
     * @param $type   是否下载  false--保存   true--下载
     * @return string   返回文件全路径
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     */
    public function exportExcel($title = array(), $data = array(), $fileName = '', $savePath = './', $isDown = false)
    {
        include(WR . '/lib/PHPExcel/Classes/PHPExcel.php');
        $obj = new PHPExcel();

        //横向单元格标识
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ');

        $obj->getActiveSheet(0)->setTitle('sheet名称');   //设置sheet名称
        $_row = 1;   //设置纵向单元格标识
        if ($title) {
            $_cnt = count($title);
            $obj->getActiveSheet(0)->mergeCells('A' . $_row . ':' . $cellName[$_cnt - 1] . $_row);   //合并单元格
            //$obj->setActiveSheetIndex(0)->setCellValue('A'.$_row, '数据导出：'.date('Y-m-d H:i:s'));  //设置合并后的单元格内容
            $_row++;
            $i = 0;
            foreach ($title AS $v) {   //设置列标题
                $obj->setActiveSheetIndex(0)->setCellValue($cellName[$i] . $_row, $v);
                $i++;
            }
            $_row++;
        }

        //填写数据
        if ($data) {
            $i = 0;
            foreach ($data AS $_v) {
                $j = 0;
                foreach ($_v AS $_cell) {
                    $obj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i + $_row), $_cell);
                    $j++;
                }
                $i++;
            }
        }

        //文件名处理
        if (!$fileName) {
            $fileName = uniqid(time(), true);
        }

        $objWrite = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');

        if ($isDown) {   //网页下载
            header('pragma:public');
            header("Content-Disposition:attachment;filename=$fileName.xls");
            $objWrite->save('php://output');
            exit;
        }

        $_fileName = iconv("utf-8", "gb2312", $fileName);   //转码
        $_savePath = $savePath . $_fileName . '.xlsx';
        $objWrite->save($_savePath);

        return $savePath . $fileName . '.xlsx';
    }

    public function downpinglun()
    {

        header("Content-type: text/html; charset=utf-8");
        $Caiji_path = WR . '/userdata/cache/pinlun/';
        $name = $_GET["name"];
        //echo file_get_contents($Caiji_path.$name);
        $tempContent = read_file_ByLine($Caiji_path . $name);
        $newcontent = array();
        foreach ($tempContent as $v) {
            //echo "<p>".$v."</p>";
            $newcontent[][] = $v;
        }
        $this->exportExcel(false, $newcontent, '评论' . date('Y-m-d H:i:s'), './', true);
    }

    public function viewpinglun()
    {
        header("Content-type: text/html; charset=utf-8");
        $Caiji_path = WR . '/userdata/cache/pinlun/';
        $name = $_GET["name"];
        //echo file_get_contents($Caiji_path.$name);
        $tempContent = read_file_ByLine($Caiji_path . $name);
        foreach ($tempContent as $v) {
            echo "<p>" . $v . "</p>";
        }
    }

    //采集评论$web_root = WR . "/userdata/cache/pinlun/";
    public function Caijipinglun()
    {
        header("Content-type: text/html; charset=utf-8");

        ini_set('memory_limit', '128M');
        /*
         * 翻译插件
         */
        //插件路径定义
        define("MEDIAWIKI_PATH", WR . "/mediawiki/");
        require WR . '/zh-tw.class.php';
        $zhtw = $_GET["lang"] ? $_GET["lang"] : 'zh-tw';
        //$zhtw ='en';
        //$zhtw ='zh-cn';
        $Caiji_path = WR . '/userdata/cache/pinlun/';
        if ($_GET["delname"]) {
            unlink($Caiji_path . $_GET["delname"]);
        }
        $not_strarr = array(
            'qb',
            '助手',
            '应用宝',
            '江西',
            '666',
            '游戏',
            '雷锋',
            '北京',
            '实名',
            '收费',
            '比',
            '骗',
            '中国',
            'logo',
            '老家',
            '问答',
            'se片',
            '闪退',
            '卫星',
            '神器',
            '亿',
            '红包',
            '回答',
            '假',
            '钱',
            '垃圾',
            '网站',
            '机器',
            '安利',
            '差评',
            'U嫁',
            '老乡',
            '网页',
            '直播',
            "QQ",
            "威信",
            "啪啪",
            "看片",
            "微信",
            "同城约泡站",
            "威心",
            "出售",
            "威@信"
        );

        $this->name = '采集评论'; // 进行模板变量赋值
        $this->error = 0;
        $this->action = "/Adms/Sys/Caijipinglun.html";
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $comment_contex = $_GET["comment_contex"] ? $_GET["comment_contex"] : 0;
        $pkname = $_GET["appid"] ? $_GET["appid"] : 0;
        $yicaiji = $_GET["yicaiji"] ? $_GET["yicaiji"] : 0;
        $pagesize = 10;
        $appid = $_GET["appid"];//?$_GET["appid"] :1105405779
        $comments = 0;
        if ($appid) {

            //if(!$pkname){
            $pkurl = "http://mapp.qzone.qq.com/cgi-bin/mapp/mapp_info?type=appinfo&appid={$appid}&packageName={$pkname}&platform=touch&network_type=unknown&resolution=360x640";
            $pkstr = file_get_contents($pkurl);
            //$pkstr = substr($pkstr,0,strlen($pkstr)-1);
            $pktemp = json_decode($pkstr);
            $pktemp = object_array($pktemp);
            if ($pktemp["app"][0]["pName"]) {
                $pkname = $pktemp["app"][0]["pName"];
                $appid = $pktemp["app"][0]["id"];
            }

            //}

            $appid_path = $Caiji_path . $appid . $zhtw . ".txt";
            if ($Page == 1) {
                file_put_contents($appid_path, "");
            }
            $url_gets = array();
            $url_gets["type"] = "myapp_all_comment";
            $url_gets["appid"] = $appid;
            $url_gets["pkgname"] = $pkname;
            $url_gets["pageNo"] = $Page;
            $url_gets["pageSize"] = $pagesize;
            $url_gets["need_score"] = "5";
            $url_gets["platform"] = "touch";
            $url_gets["network_type"] = "unknown";
            $url_gets["resolution"] = "360x640";
            if ($comment_contex) {
                $url_gets["comment_contex"] = $comment_contex;
            }
            $url = "http://mapp.qzone.qq.com/cgi-bin/mapp/mapp_getcomment?";
            $url .= http_build_query($url_gets);

            $str = file_get_contents($url);
            $str = substr($str, 0, strlen($str) - 1);
            $temp = json_decode($str);
            $temp = object_array($temp);
            //Dump($temp);exit;
            $pinlun = $temp["data"]["yyb_comment"];
            $comments = $pinlun["comments"];

            if ($comments) {
                foreach ($comments as $k => $v) {
                    $content = $v["content"];
                    $is_write = 1;
                    if ($v["score"] > 4) {
                        foreach ($not_strarr as $nostr) {
                            if (strstr($content, $nostr) != FALSE) {
                                $is_write = 0;
                                continue;
                            }
                        }
                        if ($is_write == 1) {
                            $content = str_replace("应用", "程式", $content);
                            $content = str_replace("软件", "程式", $content);
                            $content = str_replace("婚恋", "交友", $content);
                            $content = str_replace("app", "程式", $content);
                            $content = str_replace("APP", "程式", $content);
                            $content = str_replace("App", "程式", $content);
                            $content = json_encode($content);
                            $content = preg_replace("/\\\u[ed][0-9a-f]{3}\\\u[ed][0-9a-f]{3}/", "", $content);//替换成*
                            $content = json_decode($content);
                            if ($content) {
                                if ($zhtw == "zh-tw") {
                                    $content = MediaWikiZhConverter::convert($content, "zh-tw");

                                    $content = str_replace("臺", "台", $content);
                                    $content = str_replace("壹", "一", $content);
                                    $content = str_replace("挺", "蠻", $content);
                                } elseif ($zhtw == "en") {
                                    $content = str_replace("☺", "", $content);
                                    // Dump($content);
                                    $content = TranslateAnysay($content, $zhtw);
                                    //Dump($content);
                                    if (!$content) {
                                        echo "fanyi error";
                                        exit;
                                    }
                                    if ($content == 400) {
                                        $content = 0;
                                    }
                                } elseif ($zhtw == "cn-tw") {
                                    if ($k % 2 == 0) {
                                        $content = MediaWikiZhConverter::convert($content, "zh-tw");
                                        $content = str_replace("臺", "台", $content);
                                        $content = str_replace("壹", "一", $content);
                                        $content = str_replace("挺", "蠻", $content);
                                    }
                                }
                            }
                            if ($content) {
                                if (strstr(file_get_contents($appid_path), $content) === FALSE) {
                                    file_put_contents($appid_path, $content . "\r\n", FILE_APPEND);
                                    $yicaiji = $yicaiji + 1;
                                }

                            }
                        }
                    }
                    //echo $v["content"];exit;
                }
                $ret["comment_contex"] = $pinlun["comment_contex"];
                $ret["totalCount"] = $pinlun["vote5Count"];
                $ret["pageNum"] = $Page;
                $ret["all_page"] = ceil($pinlun["vote5Count"] / $pagesize);
                $this->Pages = $this->GetPages($ret);

                $myurl = 'http://' . $_SERVER['HTTP_HOST'] . ":81" . $_SERVER['PHP_SELF'];
                $url_arges = $_GET;
                $url_arges["Page"] = $Page + 1;
                $url_arges["comment_contex"] = $pinlun["comment_contex"];
                $url_arges["yicaiji"] = $yicaiji;
                $url_arges["lang"] = $zhtw;
                $myurl .= "?" . http_build_query($url_arges);

                $this->myurl = $myurl;
            } else {
                $this->msg = "没有数据";
                $this->error = 1;
            }
            $this->yicaiji = $yicaiji;
        } else {
            $web_root = $Caiji_path;
            mkdirs($web_root);
            $this->files = tree($web_root);
        }
        $this->comments = $comments;
        $this->display("caijipinglun");
    }

    //清除缓存
    public function ClearCache()
    {
        if ($_GET["name"] == 'runtime') {
            $web_root = WR . "/userdata/Runtime";
            $command = "/bin/rm -rf " . $web_root . " > /dev/null &";
            system($command);
            $this->tip = "已删除！";
        }
        //删除一周前的聊天记录
        if ($_GET["name"] == 'delcharcord') {
            $msgM = new MsgModel();
            $msgboxM = new MsgBoxModel();
            $rtime = time();
            $ltime = strtotime("-1 week");
            $time['sendtime'] = array('elt', $ltime);
            $data = $msgM->where($time)->delete();
            if ($data) {
                $dataa = $msgboxM->where($time)->delete();
            }
            if ($data & $dataa) {
                $this->tip = "聊天记录已删除！";
            } else {
                $this->tip = "聊天记录失败！";
            }


            //header("location:/Adms/sys/log");
        }

        if ($_POST["name"]) {
            $name = $_POST["name"];
            $namearr = explode('-', $name);
            $name = $namearr[0];
            //批量删除
            if (is_array($_POST["name"])) {
                $name = array_values($_POST["name"]);
                for ($i = 0; $i < count($name); $i++) {
                    $name[$i] = explode('-', $name[$i])[0];
                    if ($name[$i] == 'reids_dynamic') {
                        $redis = $this->redisconn();
                        $redis->del('dynamic');
                        $reuslt = array(
                            'status' => '1',
                            'message' => "删除成功",
                            'data' => '',
                        );
                    } else {
                        $web_root = WR . "/userdata/cache/" . $name[$i];
                        $command = "/bin/rm -rf " . $web_root . " > /dev/null &";
                        system($command);
                        if (!file_exists($web_root)) {
                            $reuslt = array(
                                'status' => '1',
                                'message' => "删除成功",
                                'data' => '',
                            );
                        } else {
                            $reuslt = array(
                                'status' => '2',
                                'message' => "删除失败",
                                'data' => '',
                            );
                        }
                    }
                }
                exit(json_encode($reuslt));
            } else {
                if ($name == 'reids_dynamic') {
                    $redis = $this->redisconn();
                    $redis->del('dynamic_boy');
                    $redis->del('dynamic_girl');
                    $reuslt = array(
                        'status' => '1',
                        'message' => "删除成功",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } elseif ($name == 'reids_switch') {
                    $redis = $this->redisconn();
                    $redis->del('switch1');
                    $redis->del('switch2');
                    $redis->del('switch3');
                    $reuslt = array(
                        'status' => '1',
                        'message' => "删除成功",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } elseif ($name == 'giftlist') {

                    $redis = $this->redisconn();
                    $product = array('23138', '23110', '20113', '20111', '24110', '20114', '24111');
                    foreach ($product as $k => $v) {
                        $redis->del('giftlist_' . $v);
                    }

                    $reuslt = array(
                        'status' => '1',
                        'message' => "删除成功",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } elseif ($name == 'redis_videochar') {
                    $redis = $this->redisconn();
                    $redis->del('Videochar_match_girl');
                    $redis->del('Videochar_match_boy');
                    $reuslt = array(
                        'status' => '1',
                        'message' => "删除成功",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } elseif ($name == 'redis_suiliaouids') {
                    $redis = $this->redisconn();
                    $redis->del('suiliaouidlist');
                    $reuslt = array(
                        'status' => '1',
                        'message' => "删除成功",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } elseif ($name == 'redis_registlist') {
                    $redis = $this->redisconn();
                    $product = array('23138', '23110', '20113', '20111', '24110', '20114');
                    foreach ($product as $k => $v) {
                        $redis->del('userregistlist-' . $v);
                    }
                    $reuslt = array(
                        'status' => '1',
                        'message' => "删除成功",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                }elseif ($name == 'redis_useronlinelist') {
                        $redis = $this->redisconn();
                        $redis->del('get_yingxiao_user-zx-status-1');
                        $redis->del('get_yingxiao_user-zx-status-2');
                    $reuslt = array(
                        'status' => '1',
                        'message' => "删除成功",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                }elseif ($name == 'redis_recommendwordlist') {
                    $redis = $this->redisconn();
                    $redis->del('recommend-word');
                    $reuslt = array(
                        'status' => '1',
                        'message' => "删除成功",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                }
                elseif ($name == 'redis_betteruser') {
                    $redis = $this->redisconn();
                    $product = array('23138', '23110', '20113', '20111', '24110', '20114', '24111');
                    foreach ($product as $k => $v) {
                        $redis->del('betteruser-' . $v);
                    }
                    $reuslt = array(
                        'status' => '1',
                        'message' => "删除成功",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } elseif ($name == 'redis_getranklist') {
                    $redis = $this->redisconn();
                    $product = array('23138', '23110', '20113', '20111', '24110', '20114', '24111');
                    foreach ($product as $k => $v) {
                        $redis->del('getranklist-1-' . $v);
                        $redis->del('getranklist-2-' . $v);
                    }
                    $reuslt = array(
                        'status' => '1',
                        'message' => "删除成功",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } else {
                    $web_root = WR . "/userdata/cache/" . $name;
                    $command = "/bin/rm -rf " . $web_root . " > /dev/null &";
                    system($command);
                    if (!file_exists($web_root)) {
                        $reuslt = array(
                            'status' => '1',
                            'message' => "删除成功",
                            'data' => '',
                        );
                        exit(json_encode($reuslt));
                    } else {
                        $reuslt = array(
                            'status' => '2',
                            'message' => "删除失败",
                            'data' => '',
                        );
                        exit(json_encode($reuslt));
                    }
                }

            }
        }

        $this->name = '清空缓存'; // 进行模板变量赋值
        $this->error = 0;
        $status = C('status');
        $this->DataList = $status['cache'];
        $this->display();
    }

    public function Caijishuju()
    {
        $this->name = '采集管理'; // 进行模板变量赋值

        $this->display("ClearCache");
    }

    public function ErrorLogs()
    {
        $this->name = '错误日志'; // 进行模板变量赋值
        $type = $_GET["type"] ? $_GET["type"] : "1";
        $product = $_GET["product"] ? $_GET["product"] : "10008";
        $this->product = $product;
        if ($product == "10108") {
            $host = "http://47.89.47.92:81";
        } else {
            $host = "http://47.91.136.204:81";
        }
        if ($type == 1) {
            $url = $host . "/myapi.php?token=J1OzGfBhCOGmzrMO1AemrM&type=1";
            $files = object_array(json_decode(file_get_contents($url)));
            $this->files = $files;
            $this->display();
        } elseif ($type == 2) {
            $url = $host . "/myapi.php?token=J1OzGfBhCOGmzrMO1AemrM&type=" . $type;
            $url .= "&name=" . urlencode($_GET["name"]);

            echo "<pre>";
            echo file_get_contents($url);
            echo "</pre>";
        } elseif ($type == 3) {
            $url = $host . "/myapi.php?token=J1OzGfBhCOGmzrMO1AemrM&type=" . $type;
            $url .= "&name=" . urlencode($_GET["name"]);
            echo file_get_contents($url);
            header('Location:/Adms/Sys/ErrorLogs?product=' . $product);
        } elseif ($type == 4) {
            $url = $host . "/myapi.php?token=J1OzGfBhCOGmzrMO1AemrM&type=2";
            $url .= "&name=" . urlencode($_GET["name"]);
            $str = file_get_contents($url);

            header('Content-Type:text/html'); //指定下载文件类型
            header('Content-Disposition: attachment; filename="' . $_GET["name"] . '"'); //指定下载文件的描述
            header('Content-Length:' . strlen($str)); //指定下载文件的大小
            echo $str;

        }
        //Dump($files);exit;

    }

    //国家管理
    public function countrycharge()
    {
        $this->name = '国家管理列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 12;
        $coun = new CountryModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['name_zh|name'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $coun->getListPage($Wdata, $Page, $PageSize);
        $this->DataList = $ret["list"];
        $all_page = ceil($ret['totalCount'] / $PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        //Dump($this->ListData);exit;
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display();
    }

    //价钱管理
    public function payprice()
    {
        $this->name = '价钱管理列表'; // 进行模板变量赋值
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['common'] = $kerword;
            }
        }
        if (isset($_GET['gender']) && $_GET['gender'] != "") {
            $gender = $_GET['gender'];
            if ($gender) {
                $Wdata['gender'] = $gender;
            }
        }
        if (isset($_GET['recharvalue']) && $_GET['recharvalue'] != "") {
            $recharvalue = $_GET['recharvalue'];
                $Wdata['status'] = $recharvalue;
        }else{
            $recharvalue=1;
            $Wdata['status']=1;
        }
        $payprice = M('payprice');
        $datalist= $payprice->where('type=1 or type=2 or type=5')->where($Wdata)->select();
        $datalist=f_order($datalist,'order',1);
            $this->DataList =$datalist;
        //  $coun = new CountryModel();
        //$Wdata = array();
        $this->get = $_GET;
        $this->recharvalue=$recharvalue;
        $this->gender=$gender;
        $this->action = __ACTION__ . ".html";
        $this->display();
    }

    //修改增添价格
    public function editprice()
    {
        $payprice = M('payprice');
        if ($_GET['id']) {
            $msg = "正在进行修改操作！";
            $payprice = M('payprice');
            $priceinfo = $payprice->where(array('id' => $_GET['id']))->find();
            $this->Info = $priceinfo;
        } else {
            $msg = "正在进行添加操作！";
        }

        if ($_POST["grade"]) {
            $basedata['grade'] = $_POST["grade"] ? $_POST["grade"] : 1;
        }
        if ($_POST["gender"]) {
            $basedata['gender'] = $_POST["gender"] ? $_POST["gender"] : 1;
        }
        if ($_POST["order"]) {
            $basedata['order_pre'] = $_POST["order"] ? $_POST["order"] : 1;
        }
        if ($_POST["product"]) {
            $basedata['common'] = $_POST["product"] ? $_POST["product"] : '';
        }
        if ($_POST["type"]) {
            $basedata['type'] = $_POST["type"] ? $_POST["type"] : 1;
        }
        if ($_POST["status"]) {
            $basedata['status'] = $_POST["status"] ? $_POST["status"] : 1;
        }
        if (isset($_POST['price'])) {
            $basedata['price'] = $_POST['price'];
        }
        if (isset($_POST['days'])) {
            $basedata['days'] = $_POST['days'];
        }
        if (isset($_POST['isopen'])) {
            $basedata['isopen'] = $_POST['isopen'];
        }
        if (isset($_POST['is_native'])) {
            $basedata['is_native'] = $_POST['is_native'];
        }
        if (isset($_POST['istrue'])) {
            $basedata['istrue'] = $_POST['istrue'];
        }
        if (isset($_POST['paykey'])) {
            $basedata['paykey'] = $_POST['paykey'];
        }
        if ($_POST['id']) {
            $data = $payprice->where(array('id' => $_POST['id']))->save($basedata);
            if ($data) {
                $msg = "修改成功！";
            }
        } elseif ($_POST) {
            $data = $payprice->add($basedata);
            if ($data) {
                $msg = "添加成功！";
            }

        }
        $product = M('products')->select();
        if ($priceinfo) {
            $priceproduct = $priceinfo['common'];
            foreach ($product as $k => $v) {
                if ($v['product'] == $priceproduct) {
                    $product[$k]["checked"] = "checked=\"checked\"";
                }
            }
        }

        $this->msg = $msg;
        $this->product = $product;
        $this->action = "/Adms/Sys/editprice.html";
        $this->display();
    }

    //删除价格
    public function delprice()
    {
        $payprice = M('payprice');
        if ($_GET['id']) {
            $payprice->where(array('id' => $_GET['id']))->delete();
        }
        redirect('/Adms/Sys/payprice');
    }


//status 状态修改
    public function updatestatus()
    {
        $CounM = new CountryModel();
        if ($_POST) {
            if ($_POST["gid"]) {
                $map = array();
                $map['status'] = $_POST["status"];
                $return = array();
                $return['id'] = $_POST["gid"];
                $result = $CounM->updateAll($map, $return);
                if ($result) {
                    $content = array(
                        'status' => 1,
                        'gstatus' => $map['status'],
                        'message' => '修改成功'
                    );
                    exit(json_encode($content));
                } else {
                    $content = array(
                        'status' => 2,
                        'message' => '修改失败！'
                    );
                    exit(json_encode($content));
                }
                //return json_encode ($map);
            }
        }
    }

    //操作日志
    public function log()
    {
        $this->name = '操作日志列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['other_id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $PageSize = 30;
        $adminlog = new AdminLogModel();
        //清除所有数据
        if ($_GET["status"] == 1) {
            $adminlog->where('1')->delete();
            header("location:/Adms/sys/log");
        }
        //清除一周之前的数据
        if ($_GET["status"] == 2) {
            $rtime = time();
            $ltime = strtotime("-1 week");
            $time['create_time'] = array('elt', $ltime);
            $data = $adminlog->where($time)->delete();
            header("location:/Adms/sys/log");
        }

        $ret = $adminlog->getListPage($Wdata, $Page, $PageSize);
        $adminuser = M("Admin_user");
        foreach ($ret["list"] as $k => $v) {

            $username = $adminuser->where(array("id" => $v["admin_id"]))->getField('name');
            $ret["list"][$k]['username'] = $username;

        }
        $this->DataList = $ret["list"];
        $all_page = ceil($ret['totalCount'] / $PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        //Dump($this->ListData);exit;
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display();
    }

    //谷歌验证开关
    public function order_switchlist()
    {
        $orderndata = M('order_switch');
        $alldata = $orderndata->select();
        foreach($alldata as $k=>$v){
            if($v['orderjson']!=null){
                $alldata[$k]['orderjson']=$v['orderjson'];
            }
        }
        $this->DataList = $alldata;
        $this->get = $_GET;
        $this->display();
    }

    public function orderswitchaddin()
    {
        $id = $_POST['id'];
        $isopenin = $_POST['isopenin'];
        if ($isopenin == 1) {
            //加入
            $orderdataa = M('order_switch')->where(array('id' => $id))->save(array('isopenin' => 1));
            if ($orderdataa) {
                $reuslt = array(
                    'status' => '1',
                    'message' => "已开启更新",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            }
        } elseif ($isopenin == 0) {
            //取消加入
            $versiondata = M('order_switch')->where(array('id' => $id))->save(array('isopenin' => 0));

            if ($versiondata) {
                $reuslt = array(
                    'status' => '0',
                    'message' => "已关闭更新",
                    'data' => '',
                );
                exit(json_encode($reuslt));
            }
        }


    }
    //添加
    public function orderswitchadd(){
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        if($_POST){
            $err=0;
            $msg="";

            if(!$_POST["product"]){
                $err = 2;
                $msg.="平台号不能为空<br />";
            }
            if($err==0){

                $map = array();
                $map["product"] = $_POST["product"];
                $AttrValue = M('order_switch')->where($map)->find();
                if($AttrValue){
                    $this->tip = "平台号已经存在！";
                }else{
                    $map["product"] =$_POST["product"];
                    $map["orderjson"] =$_POST["orderjson"];
                    $map["isopenin"] =$_POST["isopenin"];
                    M('order_switch')->data($map)->add();
                    $this->tip = $_POST["product"]."已添加";
                    header('Location:order_switchlist.html');
                }

            }else{
                $this->tip = $msg;
            }
        }
        $this->display();
    }

//修改
    //修改增添价格
    public function orderswitchedit()
    {
        $payprice = M('order_switch');
        if ($_GET['id']) {
            $msg = "正在进行修改操作！";
            $priceinfo = $payprice->where(array('id' => $_GET['id']))->find();
            $this->Info = $priceinfo;
        } else {
            $msg = "正在进行添加操作！";
        }

        $id=$_GET['id'];
        if ($_POST["product"]) {
            $basedata['common'] = $_POST["product"] ? $_POST["product"] : '';
        }
        if ($_POST["orderjson"]) {
            $basedata['orderjson'] = $_POST["orderjson"] ? $_POST["orderjson"] : '';
        }
        if (isset($_POST['isopenin'])) {
            $basedata['isopenin'] = $_POST['isopenin'];
        }
        if ($_POST['id']) {
            $data = $payprice->where(array('id' => $_POST['id']))->save($basedata);
            if ($data) {
                $msg = "修改成功！";
                header('Location:order_switchlist.html');
            }
        } elseif ($_POST) {
            $data = $payprice->add($basedata);
            if ($data) {
                $msg = "添加成功！";
            }

        }
        $this->id = $id;
        $this->msg = $msg;
        $this->action = "/Adms/Sys/orderswitchedit.html";
        $this->display("orderswitchadd");
    }
    /*
	 * 删除
	 */
    public function orderswitchdel(){

        $code =$_REQUEST["id"];
        if($code){
            $AttrValueM = M('order_switch');
            $map = array();
            $map["id"] = $code;
            $AttrValueM->where($map)->delete();
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }



    //官网帮助文件的上传修改删除
    public function helplist()
    {
        $helpndata = M('helpword');
        $helpdata = $helpndata->select();
        $this->DataList = $helpdata;
        $this->get = $_GET;
        $this->display();
    }
    //添加
    public function helpadd(){
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        if($_POST){
            $err=0;
            $msg="";
            if($err==0){
                $map = array();
                    $map["title"] =$_POST["title"];
                    $map["content"] =$_POST["content"];
                    M('helpword')->data($map)->add();
                    $this->tip = "已添加";
                    header('Location:helplist.html');

            }else{
                $this->tip = $msg;
            }
        }
        $this->display();
    }

//修改
    //修改增添价格
    public function helpedit()
    {
        $helpword = M('helpword');
        if ($_GET['id']) {
            $msg = "正在进行修改操作！";
            $helpwordinfo = $helpword->where(array('id' => $_GET['id']))->find();
            $this->Info = $helpwordinfo;
        } else {
            $msg = "正在进行添加操作！";
        }

        $id=$_GET['id'];
        if ($_POST["title"]) {
            $basedata['title'] = $_POST["title"] ? $_POST["title"] : '';
        }
        if ($_POST["content"]) {
            $basedata['content'] = $_POST["content"] ? $_POST["content"] : '';
        }
        if ($_POST['id']) {
            $data = $helpword->where(array('id' => $_POST['id']))->save($basedata);
            if ($data) {
                $msg = "修改成功！";
                header('Location:helplist.html');
            }
        } elseif ($_POST) {
            $data = $helpword->add($basedata);
            if ($data) {
                $msg = "添加成功！";
            }
        }
        $this->id = $id;
        $this->msg = $msg;
        $this->action = "/Adms/Sys/helpedit.html";
        $this->display("helpadd");
    }
    /*
	 * 删除
	 */
    public function helpdel(){

        $code =$_REQUEST["id"];
        if($code){
            $AttrValueM = M('helpword');
            $map = array();
            $map["id"] = $code;
            $AttrValueM->where($map)->delete();
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }
    //每半个小时统计下普通男用户在线人数
    public function getonlinecount(){
        $onlineM=M('onlinecount');
        $productsM  = new ProductsModel();
        $products=$productsM->getAll();
        $time=date('H:i', time());

        $data=array();
        $datathree=array();
        $orderdata=array();
        $datagirl=array();
         //用户类型为1男用户
        $data['time']=$time;
        $datathree['time']=$time;
        $orderdata['time']=$time;

        foreach($products as $k1=>$v1) {
            $product = $v1['product'];
            $data['product'] = $product;
            $datathree['product'] = $product;
            $orderdata['product'] = $product;
            $onlinelist = $this->get_onlinelist($product, 1, 1, 1);
            $count = count($onlinelist);
            $data['count'] = $count;
            $data['usertype'] = 1;
            $data['type'] = 1;
            $data['gender'] = 1;
            $exitcount = $onlineM->where(array('time' => $time, 'product' => $product, 'usertype' => 1, 'type' => 1, 'gender' => 1))->select();
            if ($exitcount) {
                $onlineM->save($data);
            } else {
                $onlineM->data($data)->add();
            }

            //用户为1的女用户

            $datagirl['time']=$time;
            $datagirl['product']=$product;
            $onlinelistgirl=$this->get_onlinelist($product,1,2,1);
            $girlcount=count($onlinelistgirl);
            $datagirl['count']=$girlcount;
            $datagirl['usertype']=1;
            $datagirl['type']=1;
            $datagirl['gender']=2;
            $exitcountgirl=$onlineM->where(array('time'=>$time,'product'=>$product,'usertype'=>1,'type'=>1,'gender'=>2))->select();
            if($exitcountgirl){
                $onlineM->save($datagirl);
            }else{
                $onlineM->data($datagirl)->add();
            }



            //用户类型为3女用户
            $onlinelistthree=$this->get_onlinelist($product,3,2,1);
            $datathree['count']=count($onlinelistthree);
            $datathree['usertype']=3;
            $datathree['type']=1;
            $exitcountthree=$onlineM->where(array('time'=>$time,'product'=>$product,'usertype'=>3,'type'=>1))->select();
            if($exitcountthree){
                $onlineM->save($datathree);
            }else{
                $onlineM->data($datathree)->add();
            }
            //统计订单个数
            $orderdata['type']=2;
            $endshijianchuo=time();
            $beginshijianchuo=$endshijianchuo-60*30;
            $sql='select * from t_recharge where product='. $product .' and paytime<'. $endshijianchuo .' and paytime>'.$beginshijianchuo ;
            $result=M()->query($sql);
            $orderdata['count']=count($result);
            $exitcountorder=$onlineM->where(array('time'=>$time,'product'=>$product,'type'=>2))->select();
            if($exitcountorder){
                $onlineM->save($orderdata);
            }else{
                $onlineM->data($orderdata)->add();
            }





        }
        echo 1;

    }


    //排序
    public function payprice_ajax(){

        if(!empty($_POST['paixu']) && !empty($_POST['id'])){
            $payprice = M('payprice');
            $payinfo = $payprice->where(array("id"=>$_POST['id']))->find();
            $is_order = $payprice->where(array("grade"=>$payinfo['grade'],"common"=>$payinfo['common'],"order_pre"=>$_POST['paixu']))->find();
            if(empty($is_order)){
                $payprice->where(array('id' => $_POST['id']))->save(array("order_pre"=>$_POST['paixu']));
            }else{
                echo "1";
            }

        }

    }





}
?>
