<?php
class GoodsController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();	
	}
	/*
	 *商品列表
	*/
	public function GoodsList(){
		$this->name = '商品列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		
		$WhereArr = array();
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword=$_GET['kerword'];
			$WhereArr['title|content']=array('like',"%".$kerword."%");
		}
		$GoodsM = new GoodsModel();
		$ret = $GoodsM->getListPage($WhereArr,$pageNum,$pageSize);
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;
		
		$this->DataList = $ret;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = "/Adms/Goods/GoodsList.html";
		$this->display("GoodsList");
	}
	/*
	 *商品列表
	*/
	public function AddGoods(){
		$this->name = '添加商品'; // 进行模板变量赋值
		if($_POST["title"]){
			
			$goodsimg = $this->uploadimg();
			
			
			$GoodsM = new GoodsModel();
			$InArray = array();
			$InArray["title"] = $_POST["title"];
			$InArray["price"] = $_POST["price"];
			$InArray["content"] = $_POST["content"];
			$InArray["status"] = 0;
			$InArray["goodsimg"] = $goodsimg;
			$ret = $GoodsM->addOne($InArray);
			header("Location:/Adms/Goods/GoodsList");
		}
		$this->action = "/Adms/Goods/AddGoods.html";
		$this->display("AddGoods");
	}
	/*
	 *商品修改
	*/
	public function EditGoods(){
		$this->name = '添加商品'; // 进行模板变量赋值
		$gid = $_GET["gid"];
		$Where["gid"] = $gid;
		$GoodsM = new GoodsModel();
		$GoodInfo = $GoodsM->getOne($Where);
		if($_POST["title"]){
			/*
			$goodsimg = $this->uploadimg();
			$InArray = array();
			$InArray["title"] = $_POST["title"];
			$InArray["price"] = $_POST["price"];
			$InArray["content"] = $_POST["content"];
			$InArray["status"] = 0;
			$InArray["goodsimg"] = $goodsimg;
			$ret = $GoodsM->addOne($InArray);
			header("Location:/Adms/Goods/GoodsList");
			*/
		}
		$this->GoodInfo = $GoodInfo;
		$this->action = "/Adms/Goods/EditGoods.html";
		$this->display("EditGoods");
	}
		/*
	 *商品删除
	*/
	public function delGoods(){
		$this->name = '添加商品'; // 进行模板变量赋值
		if($_GET["gid"]){

			
			
			$GoodsM = new GoodsModel();
			
			$ret = $GoodsM->delOne(array("gid"=>$_GET["gid"]));
			header("Location:/Adms/Goods/GoodsList");
		}
	}
	//上传图片
	public function uploadimg(){

		$path = WR.'/UserData/'.'cache/goodsimg/';
		$url = '/UserData/'.'cache/goodsimg/';
		mkdirs($path);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize = 32922000;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg');
		//设置附件上传目录
		$upload->savePath = $path;
		$upload->saveRule =time().".jpg";
		
		if (!$upload->upload()) {
			return 0;
		} else {
			PutOss($url .$upload->saveRule);
			return $url .$upload->saveRule;
		}
	}
	//修改状态
	public function updatestatus(){
		$this->name = '商品列表'; // 进行模板变量赋值
		$gid = $_POST["gid"];
		$status = $_POST["status"];
		if($gid){
			$GoodsM = new GoodsModel();
			$WhereArr["gid"] = $gid;
			$ret = $GoodsM->updateOne($WhereArr,array("status"=>$status));
		}
		$result["msg"] = "ok";
		$result["Succeed"] = 1;
		echo json_encode($result);exit;
	}
	/*
	 *礼物兑换商品
	*/
	public function GiftBuy(){
		$data = $this->Api_recive_date;
		$myid = decode($data['token']);
		$pageNum = $data['pageNum'] ? $data['pageNum'] : 1;
		$pageSize = 10;
		file_put_contents(WR.'/UserData/logs/pay/GiftBuy_'.date("Y-m-d H:i:s", time()).".text",json_encode($_POST));
		
		
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray['isSucceed'] = 1;
		$RetArray['msg'] = "ok";
	
		Push_data($RetArray);
	}
}

?>