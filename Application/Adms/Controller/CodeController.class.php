<?php
use Think\Controller;
class codeController extends Controller
{
    private $dic = array(
        0=>'0',    1=>'1',    2=>'2',    3=>'3',    4=>'4',    5=>'5',    6=>'6',    7=>'7',    8=>'8',    9=>'9',
        10=>'A',    11=>'B',    12=>'C',    13=>'D',    14=>'E',    15=>'F',    16=>'G',    17=>'H',
        18=>'J',    19=>'K',    20=>'L',    21=>'M',    22=>'N',    23=>'P',    24=>'Q',    25=>'R',
        26=>'S',    27=>'T',    28=>'U',    29=>'V',    30=>'W',    31=>'X',    32=>'Y',    33=>'Z'
    );
    //ID
    //private $ids;
    //长度
    //private $format = 8;

    public function encodeID($int, $format=8) {
        $dics = $this->dic;
        //print_r($dics);exit;
        $dnum = 36; //进制数
        $arr = array ();
        $loop = true;
        while ($loop) {
            $arr[] = $dics[bcmod($int, $dnum)];
            $int = bcdiv($int, $dnum, 0);
            //echo $int.'<br/>';
            if ($int == '0') {
                $loop = false;
            }
        }
        if (count($arr) < $format)
            $arr = array_pad($arr, $format, $dics[0]);

        return implode('', array_reverse($arr));
    }

    public function decodeID($ids) {
        $dics = $this->dic;
        $dnum = 36; //进制数
        //键值交换
        $dedic = array_flip($dics);
        //去零
        $id = ltrim($ids, $dics[0]);
        //反转
        $id = strrev($id);
        $v = 0;
        for ($i = 0, $j = strlen($id); $i < $j; $i++) {
            $v = bcadd(bcmul($dedic[$id {
            $i }
            ], bcpow($dnum, $i, 0), 0), $v, 0);
        }
        return $v;
    }

    public function shengchengmima(){
        //$no = intval($_POST['no']);
        $no = intval($_POST['no']);
        $number=intval($_POST['number']);
        $qianliangwei=intval($_POST['qianliangwei']);
        if($no==0 || $no>=1785793904896){
            echo '起始不能为0或者起始值超出限制！';exit;
        }

        S('shangyici',$no+$number);
        $numberlist=array();

        for($i=$no;$i<=$no+$number-1;$i++){
            $numberlist[][] = $qianliangwei.$this->encodeID($i,6);
        }
        shuffle($numberlist);
        create_xls($numberlist,'卡密');
        exit(json_encode ($no+$number));
        /*$card_pre = '755';
        $card_vc = substr(md5($card_pre.$card_no),0,2);
        $card_vc = strtoupper($card_vc);
        echo $card_no;*/
    }

    public function shangyici(){
        $value = S('shangyici');
        if($value==null){
            $value=0;
        }
        echo $value;
    }


}

