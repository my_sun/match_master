<?php
class CaijiController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->Lan = $this->LangSet("zh-tw");
		$this->path = PATH_IMPORT."user/";
	}
	public function del_user($uid){
	    $UserBaseM = new UserBaseModel();
	    $UserBaseM->delOne(array("uid"=>$uid));
	    $UserextM = new UserExtendModel();
	    $UserextM->delOne(array("uid"=>$uid));
	    $PhotoM =new PhotoModel();
	    $PhotoM->delOne(array("uid"=>$uid));
	    $UserSeeM = new UserSeeModel();
	    $UserSeeM->delOne(array("uid"=>$uid));
	    $DynamicCommentM =  new DynamicCommentModel();
	    $DynamicCommentM->delOne(array("uid"=>$uid));
	    $DynamicM =  new DynamicModel();
	    $DynamicM->delOne(array("uid"=>$uid));
	    $DynamicSupportM =  new DynamicSupportModel();
	    $DynamicSupportM->delOne(array("uid"=>$uid));
	    $PhotoDynamicM =  new PhotoDynamicModel();
	    $PhotoDynamicM->delOne(array("uid"=>$uid));
	    $UserFollowM =  new UserFollowModel();
	    $UserFollowM->delOne(array("uid"=>$uid));
	    $VideoDynamicM =  new VideoDynamicModel();
	    $VideoDynamicM->delOne(array("uid"=>$uid));
	    $VideoM =  new VideoModel();
	    $VideoM->delOne(array("uid"=>$uid));
	    $AudioM =  new AudioModel();
	    $AudioM->delOne(array("uid"=>$uid));
	    //$this->get_user($uid,1);

	    return 1;
	}
	public function chedishanchu(){
	    $uid = $_POST["uid"];
	    $this->del_user($uid);
	    echo 1;
	}
	public function ruku(){
	    $uid = $_POST["uid"];
	    $UserBaseM = new UserBaseModel();
	    $UserBaseM->updateOne(array("uid"=>$uid), array("user_type"=>2));
	    
	    echo 1;
	}
	public function shanchu(){
	    $uid = $_POST["uid"];
	    $UserBaseM = new UserBaseModel();
	    $UserBaseM->updateOne(array("uid"=>$uid), array("user_type"=>6));
	    
	    echo 1;
	}
	public function UserList(){
	    //Dump($_SERVER);exit;
	    $this->name = '用户列表'; // 进行模板变量赋值
	    $Page = $_GET["Page"] ? $_GET["Page"] : 1;
	    $PageSize = 30;
	    $this->status = C("status");
	    
	    $Wdata = array();
	    $Wdata['user_type']=5;
	        if(isset($_GET['gender'])&&$_GET['gender']!="")
	            $Wdata['gender']=$_GET['gender'];
	            if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
	                $kerword = $_GET['kerword'];
	                $Wdata['uid|nickname']=array('like',"%".$kerword."%");
	            }
	            $UserBaseM = new UserBaseModel();
	            
	            $ret = $UserBaseM->getlistOrder($Wdata,$Page,$PageSize);
	            
	            $all_page = ceil($ret['totalCount']/$PageSize);
	            $ret["all_page"] = $all_page;
	            
	            foreach($ret['listSearch'] as $k=>$v){
	                
	                $userCache = $this->get_user($v["uid"]);
	                $userCache["photos"] = $this->get_user_photo($v["uid"]);
	                $ret['listSearch'][$k]=array_merge($ret['listSearch'][$k],$userCache);
	                
	                
	            }
	            //Dump($ret);exit;
	            //Dump($this->get_user(137295));exit;
	            $this->DataList = $ret;
	            $this->Pages = $this->GetPages($ret);
	            $this->get = $_GET;
	            $this->action = "/Adms/Caiji/userlist.html";
	            $this->display();
	            
	}
	public function huishou(){
	    //Dump($_SERVER);exit;
	    $this->name = '用户列表'; // 进行模板变量赋值
	    $Page = $_GET["Page"] ? $_GET["Page"] : 1;
	    $PageSize = 30;
	    $this->status = C("status");
	    
	    $Wdata = array();
	    $Wdata['user_type']=6;
	    if(isset($_GET['gender'])&&$_GET['gender']!="")
	        $Wdata['gender']=$_GET['gender'];
	        if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
	            $kerword = $_GET['kerword'];
	            $Wdata['uid|nickname']=array('like',"%".$kerword."%");
	        }
	        $UserBaseM = new UserBaseModel();
	        
	        $ret = $UserBaseM->getlistOrder($Wdata,$Page,$PageSize);
	        
	        $all_page = ceil($ret['totalCount']/$PageSize);
	        $ret["all_page"] = $all_page;
	        
	        foreach($ret['listSearch'] as $k=>$v){
	            
	            $userCache = $this->get_user($v["uid"]);
	            $userCache["photos"] = $this->get_user_photo($v["uid"]);
	            $ret['listSearch'][$k]=array_merge($ret['listSearch'][$k],$userCache);
	            
	            
	        }
	        //Dump($ret);exit;
	        //Dump($this->get_user(137295));exit;
	        $this->DataList = $ret;
	        $this->Pages = $this->GetPages($ret);
	        $this->get = $_GET;
	        $this->action = __ACTION__.".html";
	        $this->display();
	        
	}
	public function lists(){
	    $this->name = '新增采集'; // 进行模板变量赋值
	    $this->action = __ACTION__.".html";
	    if($_GET["decaijikey"]){
	        unlink($this->path.$_GET["decaijikey"].".php");
	    }
	    $caijiLIst = dir_time($this->path);
	    $listDate = array();
	    foreach ($caijiLIst as $k=>$v){
	        $caijikey = basename($v,".php");
	        $CaijiConfig = require_once $this->path.$caijikey.".php";
	        $listDate[] =array(
	            "keyname"=>$caijikey,
	            "name"=>$CaijiConfig["name"],
	            "time"=>date("Y-m-d H:i:s",filemtime($this->path.$v))
	        ); 
	    }
	    $this->listDate = $listDate;
	    $this->display();
	}
	public function import(){
	    $this->name = '新增采集'; // 进行模板变量赋值
	    $this->action = __ACTION__.".html";
	    if($_FILES){
	        $caijiconfig = $_FILES["caijiconfig"];
	        mkdirs($this->path);
	        $file_content = file_get_contents($caijiconfig['tmp_name']);
	        $caijikey = md5($file_content);
	        file_put_contents($this->path.$caijikey.".php", $file_content);
	        header('Location:applist.html?caijikey='.$caijikey);
	    }
	    $this->display();
	}
	protected function get_rund_arr($array){
	    $key = array_rand($array);
	    $temp = array();
	    $temp["key"] = $key;
	    $temp["value"] = $array[$key];
	    return $temp;
	}
	protected function get_rund_num($array){
	    $count = count($array);
	    $start = 2;
	    $end = $count-3;
	    $numrand = rand($start, $end);
	    $ids = "";
	    
	    for($num=1;$num<=$numrand;$num++){
	        $res = $this->get_rund_arr($array);
	        unset($array[$res["key"]]);
	        $array = array_merge($array);
	        if($ids){
	            $ids = $ids ."|". $res["value"];
	        }else{
	            $ids = $res["value"];
	        }
	    }
	    return $ids;
	}
	/*
	 * 更新用户属性
	 */
	public function update_user_value(){
	    $attrconf = $this->get_attrconf();
	    $User_extendM = new UserExtendModel();
	    $User_baseM = new UserBaseModel();
	    $page = $_GET['page'] ? $_GET['page'] : 1;//总当前页
	    $where = array();
	    $where["gender"]="2";
	    $where["user_type"]="2";
	    $where['weight']  = array('gt',602);
	    $pageSize=200;
	    $res = $User_baseM->getlistOrder($where, $page,$pageSize);
	   //Dump($res);exit;
	    $userlist = $res["listSearch"];
	   
	    foreach ($userlist as $k=>$v){
	        if($v["uid"]){
	            $InUserInfo = array();
	            $InUserInfo["gender"] = $v["gender"];
	            $InUserInfo["blood"] = $attrconf["blood"][array_rand($attrconf["blood"])];
	            $InUserInfo["education"] = $attrconf["education"][array_rand($attrconf["education"])];
	            $InUserInfo["income"] = $attrconf["income"][array_rand($attrconf["income"])];
	            $InUserInfo["hobby"] = $this->get_rund_num($attrconf["hobby"]);
	            $InUserInfo["marriage"] = $attrconf["marriage"][array_rand($attrconf["marriage"])];
	            $InUserInfo["constellation"] = $attrconf["constellation"][array_rand($attrconf["constellation"])];
	            $InUserInfo["wantchild"] = $attrconf["wantchild"][array_rand($attrconf["wantchild"])];
	            $InUserInfo["work"] = $attrconf["work"][array_rand($attrconf["work"])];
	            $InUserInfo["friendsfor"] = $attrconf["friendsfor"][array_rand($attrconf["friendsfor"])];
	            $InUserInfo["cohabitation"] = $attrconf["cohabitation"][array_rand($attrconf["cohabitation"])];
	            $InUserInfo["dateplace"] = $attrconf["dateplace"][array_rand($attrconf["dateplace"])];
	            $InUserInfo["lovetimes"] = $attrconf["lovetimes"][array_rand($attrconf["lovetimes"])];
	            $InUserInfo["charactertype"] = $attrconf["charactertype"][array_rand($attrconf["charactertype"])];
	            $InUserInfo["house"] = $attrconf["house"][array_rand($attrconf["house"])];
	            $InUserInfo["car"] = $attrconf["car"][array_rand($attrconf["car"])];
	            $InUserInfo["exoticlove"] = $attrconf["exoticlove"][array_rand($attrconf["exoticlove"])];
	            $InUserInfo["sexual"] = $attrconf["sexual"][array_rand($attrconf["sexual"])];
	            $InUserInfo["livewithparents"] = $attrconf["livewithparents"][array_rand($attrconf["livewithparents"])];
	            $InUserInfo["personalitylabel"] = $this->get_rund_num($attrconf["personalitylabel"]);
	            $InUserInfo["liketype"] = $this->get_rund_num($attrconf["liketype"]);
	            $InUserInfo["glamour"] = $this->get_rund_num($attrconf["glamour"]);
	            //echo $this->get_rund_num($attrconf["glamour"]);exit;
	            $tempage = array_slice($attrconf["age"],4,13);
	            
	            //if(!$InUserInfo["age"]){
	            $InUserInfo["age"] = $tempage[array_rand($tempage)];
	            //}
	           
	            if($InUserInfo["gender"]==2){
	                $attrconfheight = array_slice($attrconf["height"],11,15);
	            }else{
	                $attrconfheight = array_slice($attrconf["height"],26,36);
	            }
	            //if(!$InUserInfo["height"]){
	            $InUserInfo["height"] = $attrconfheight[array_rand($attrconfheight)];
	            //}
	            
	            if($InUserInfo["gender"]==2){
	                $attrconfweight = array_slice($attrconf["weight"],0,18);
	            }else{
	                $attrconfweight = array_slice($attrconf["weight"],23,43);
	            }
	            
	            //if(!$InUserInfo["weight"]){
	            $InUserInfo["weight"] = $attrconfweight[array_rand($attrconfweight)];
	            //}
	            //Dump($InUserInfo);
	            $InUserInfo = $this->get_UserInfofied($InUserInfo);
	            
	            $basedata =$InUserInfo["base"];
	            $extdata = $InUserInfo["ext"];
	            $upw = array();
	            $upw["uid"] = $v["uid"];
	            $User_baseM->updateOne($upw, $basedata);
	            $User_extendM->updateOne($upw, $extdata);
	        }
	    }
	    
	    $countpage = ceil($res["totalCount"]/$res["pageSize"]);
	    
	    if($page<=$countpage){
	        $page=$page+1;
	       echo "正在导入.......";
	        echo '<script language="JavaScript">
                                                function myrefresh()
                                                {
                                                window.location="update_user_value.html?page='.$page.'";
                                                }
                                                setTimeout("myrefresh()",1000);
                                                </script> ';
	    }else{
	       echo "已完成";
	    }
	   
	}
	/*
	 * 导入第一步
	 */
	public function importrun(){
	    $caijiURL = '/userdata/caiji/'.date('Ymd',time())."/";
	    $photosurl = $caijiURL."photos/";
	    $videosurl = $caijiURL."video/";
	    ini_set('max_execution_time', '1000000');
	    header("Content-type: text/html; charset=utf-8"); 
	    //插件路径定义
	    define("MEDIAWIKI_PATH", WR."/mediawiki/");
	    require WR.'/zh-tw.class.php';
	    $User_extendM = new UserExtendModel();
	    $User_baseM = new UserBaseModel();
	    $UserPhotoM = new PhotoModel();
	    $page = $_GET['page'] ? $_GET['page'] : 1;//总当前页
	    $yiwancheng = $_GET['yiwancheng'] ? $_GET['yiwancheng'] : 0;//已导入
	    $attrconf = $this->get_attrconf();
	    
	    //Dump($regionlist[array_rand($regionlist)]);
	    //Dump($regionlist);exit;
	    $this->name = '配置用户列表'; // 进行模板变量赋值
	    $this->action = "importrun.html";
	    if($_REQUEST["caijikey"]){
	        
	        $caijikey = $_REQUEST["caijikey"];
	        $CaijiConfig = require $this->path.$caijikey.".php";
	        
	        if(isset($CaijiConfig["country"])){
	            $country = $CaijiConfig["country"];
	        }else{
	            $country = "TW";
	        }
	        $regionlist = $this->get_regionlist($country);
	        
	        //页码
	        $countpage = $CaijiConfig["countpage"];
	        $pagefield = $CaijiConfig["pagefield"];//页码字段
	        //用户列表
	        $listurl = $CaijiConfig["listurl"];
	        $listargc = $CaijiConfig["listargc"];
	        $listargc = str_replace("#page#",$page,$listargc);
	        $requerytype = $CaijiConfig["requerytype"];
	        //用户信息
	        $infourl = $CaijiConfig["infourl"];
	        $infoargc = $CaijiConfig["infoargc"];
	        if(isset($CaijiConfig["headers"])){
	           $headers = $CaijiConfig["headers"];
	        }else{
	            $headers=FALSE;
	        }
	        $str = $this->get_url_content($listurl,$listargc,$requerytype,$headers);
	        
	        $temp = object_array(json_decode($str));
	        $userlist = $this->purefield($temp,$CaijiConfig["userlistfield"]);
	        
	       
	        Dump($userlist);exit;
	       
	        if(empty($userlist)){
	           // echo "列表数据为空！";exit;
	        }
	        
	        foreach ($userlist as $k=>$v){
	            $InUserInfo = array();
	            $tempuser = $v;
	            $caijiuid = $this->purefield($tempuser,$CaijiConfig["userfield"]["uid"]);
	            if($infourl&&$infourl!=""){
	                $tempinfoargc = str_replace("#uid#",$caijiuid,$infoargc);
	               
	                $struser = $this->get_url_content($infourl,$tempinfoargc,$requerytype,$headers);
	                $tempuser = object_array(json_decode($struser));
	                $tempuser = $this->purefield($tempuser,$CaijiConfig["userinfofield"]);
	                $tempuser = array_merge($v,$tempuser); 
	                
	                
	                
	            }
	            $head = $this->purefield($tempuser,$CaijiConfig["headfield"]);
	            $photoslist = $this->purefield($tempuser,$CaijiConfig["photoslistfield"]);
	            
	            if(isset($CaijiConfig["photobaseurl"])){
	                $photobaseurl = $CaijiConfig["photobaseurl"];
	            }else{
	                $photobaseurl = "";
	            }
	            if(!$head){
	               // echo "头像url配置错误";exit;
	            }
	            if(!$photoslist){
	                //echo "相册url配置错误";exit;
	            }
	           
	            
	            
	            //echo count($photoslist)."<br />";
	            if(count($photoslist)>1){
	                foreach ($CaijiConfig["userfield"] as $uk=>$uv){
	                    if($uk!="uid"){
	                        $tempcontent = $this->purefield($tempuser,$uv);
    	                    if(in_array($uk, array("nickname","mood"))){
    	                        $tempcontent = MediaWikiZhConverter::convert($tempcontent, "zh-tw");
    	                    }
    	                    
    	                    $InUserInfo[$uk] = filterEmoji(trim($tempcontent));
	                    }
	                }
	                
	                $InUserInfo["gender"] = Caiji_set_gender($InUserInfo["gender"]);
	                $InUserInfo["nickname"] = str_replace(PHP_EOL, '',$InUserInfo["nickname"]);
	                //Dump($tempuser);exit;
	                $exituser = $User_baseM->getOne(array('nickname'=>$InUserInfo["nickname"]));
	                //echo $InUserInfo["nickname"]."<br />";
	                if(empty($exituser)&&!empty($InUserInfo["nickname"])){
	                    
	                    $InUserInfo["user_type"] = "5";//采集用户
	                   
	                    $InUserInfo["country"] = $country;
	                   
	                    $InUserInfo["area"] = $regionlist[array_rand($regionlist)];
	                    $InUserInfo["blood"] = $attrconf["blood"][array_rand($attrconf["blood"])];
	                    $InUserInfo["education"] = $attrconf["education"][array_rand($attrconf["education"])];
	                    $InUserInfo["income"] = $attrconf["income"][array_rand($attrconf["income"])];
	                    $InUserInfo["hobby"] = $this->get_rund_num($attrconf["hobby"]);
	                    $InUserInfo["marriage"] = $attrconf["marriage"][array_rand($attrconf["marriage"])];
	                    $InUserInfo["constellation"] = $attrconf["constellation"][array_rand($attrconf["constellation"])];
	                    $InUserInfo["wantchild"] = $attrconf["wantchild"][array_rand($attrconf["wantchild"])];
	                    $InUserInfo["work"] = $attrconf["work"][array_rand($attrconf["work"])];
	                    $InUserInfo["friendsfor"] = $attrconf["friendsfor"][array_rand($attrconf["friendsfor"])];
	                    $InUserInfo["cohabitation"] = $attrconf["cohabitation"][array_rand($attrconf["cohabitation"])];
	                    $InUserInfo["dateplace"] = $attrconf["dateplace"][array_rand($attrconf["dateplace"])];
	                    $InUserInfo["lovetimes"] = $attrconf["lovetimes"][array_rand($attrconf["lovetimes"])];
	                    $InUserInfo["charactertype"] = $attrconf["charactertype"][array_rand($attrconf["charactertype"])];
	                    $InUserInfo["house"] = $attrconf["house"][array_rand($attrconf["house"])];
	                    $InUserInfo["car"] = $attrconf["car"][array_rand($attrconf["car"])];
	                    $InUserInfo["exoticlove"] = $attrconf["exoticlove"][array_rand($attrconf["exoticlove"])];
	                    $InUserInfo["sexual"] = $attrconf["sexual"][array_rand($attrconf["sexual"])];
	                    $InUserInfo["livewithparents"] = $attrconf["livewithparents"][array_rand($attrconf["livewithparents"])];
	                    $InUserInfo["personalitylabel"] = $this->get_rund_num($attrconf["personalitylabel"]);
	                    $InUserInfo["liketype"] = $this->get_rund_num($attrconf["liketype"]);
	                    $InUserInfo["glamour"] = $this->get_rund_num($attrconf["glamour"]);
	                    //echo $this->get_rund_num($attrconf["glamour"]);exit;
	                    $tempage = array_slice($attrconf["age"],4,13);
	                    
	                    //if(!$InUserInfo["age"]){
	                    $InUserInfo["age"] = $tempage[array_rand($tempage)];
	                    //}
	                    
	                    if($InUserInfo["gender"]==2){
	                        $attrconfheight = array_slice($attrconf["height"],11,15);
	                    }else{
	                        $attrconfheight = array_slice($attrconf["height"],26,36);
	                    }
	                    //if(!$InUserInfo["height"]){
	                    $InUserInfo["height"] = $attrconfheight[array_rand($attrconfheight)];
	                    //}
	                    
	                    if($InUserInfo["gender"]==2){
	                        $attrconfweight = array_slice($attrconf["weight"],0,18);
	                    }else{
	                        $attrconfweight = array_slice($attrconf["weight"],23,43);
	                    }
	                    
	                    //if(!$InUserInfo["weight"]){
	                    $InUserInfo["weight"] = $attrconfweight[array_rand($attrconfweight)];
	                    //}
	                    
	                    $InUserInfo = $this->get_UserInfofied($InUserInfo);
	                    
	                    $basedata =$InUserInfo["base"];
	                    $extdata = $InUserInfo["ext"];
	                    if(!empty($basedata)){
	                        $timenow = time();
	                        $basedata['password'] = rand(20000,99999);
	                        $basedata['regtime'] = $timenow;
	                        $basedata['logintime'] =$timenow;
	                        $uid = $User_baseM->addOne($basedata);
	                        
	                        if(!empty($extdata)&&$uid){
	                            $extdata["uid"] = $uid;
	                            $User_extendM->addOne($extdata);
	                        }
	                        
	                        if($uid){
	                           
	                            createDir(WR.$photosurl);
	                           
	                            $PutOssarr = array();
	                            foreach ($photoslist as $pk=>$pv){
	                                $photoUrl = $photobaseurl.$pv[$CaijiConfig["photosfield"]["url"]];
	                                
	                                if(!in_array($photoUrl, $CaijiConfig["fitsimgs"])){
	                                    $photocontent = file_get_contents($photoUrl);
	                                    if($photocontent){
	                                        $savename = md5($photocontent).".jpg";
	                                        $savepath = $photosurl.$savename;
	                                        if(!file_exists($savepath)){
	                                            file_put_contents(WR.$savepath,$photocontent);
	                                            $Indata = array();
	                                            $Indata['uid'] = $uid;
	                                            $Indata['type'] = 1;//2头像，1相册
	                                            $Indata['url'] = $savepath;
	                                            $Indata['time'] = time();
	                                            $Indata['status'] = 1;
	                                            $ret = $UserPhotoM->addOne($Indata);
	                                            $PutOssarr[]=$savepath;
	                                        }
	                                    }
	                                }
	                            }
	                            
	                            //采集头像 start
	                            if(is_array($head)){
	                                $head = $head[$CaijiConfig["photosfield"]["url"]];
	                            }
	                            if($head){
	                                $photoUrl = $photobaseurl.$head;
	                               // echo $photoUrl."<br />";
	                                if(!in_array($photoUrl, $CaijiConfig["fitsimgs"])){
	                                    $photocontent = file_get_contents($photoUrl);
	                                    if($photocontent){
	                                        $savename = md5($photocontent).".jpg";
	                                        $savepath = $photosurl.$savename;
	                                        file_put_contents(WR.$savepath,$photocontent);
	                                        $Indata = array();
	                                        $Indata['uid'] = $uid;
	                                        $Indata['type'] = 2;//2头像，1相册
	                                        $Indata['url'] = $savepath;
	                                        $Indata['time'] = time();
	                                        $Indata['status'] = 1;
	                                        $ret = $UserPhotoM->addOne($Indata);
	                                        $PutOssarr[]=$savepath;
	                                    }
	                                }
	                            }
	                            //采集头像 end
	                            //采集视频 start
	                            if(function_exists('Caiji_get_video')){
	                                $videlist = Caiji_get_video($tempuser,$CaijiConfig);
	                                if(!empty($videlist)){
	                                    createDir(WR.$videosurl);
	                                    foreach ($videlist as $vurl){
	                                        $tempcontent = file_get_contents($vurl["url"]);
	                                        if($tempcontent){
	                                            $savename = md5($tempcontent).".mp4";
	                                            $savepath = $videosurl.$savename;
	                                            $photosavepath = $videosurl.$savename.".jpg";
	                                            file_put_contents(WR.$savepath,$photocontent);
	                                            $this->getVideoCover(WR.$savepath, WR.$photosavepath);//保存视频第一帧图片
	                                            $ltime=$this->getTime(WR.$savepath);//获取视频时长
	                                            $VideoM = new VideoModel();
	                                            $Indata = array();
	                                            $Indata['uid'] = $uid;
	                                            $Indata['type'] = 1;//
	                                            $Indata['ltime'] = $ltime;//
	                                            $Indata['imageurl'] = $photosavepath;//
	                                            $Indata['url'] = $savepath;
	                                            $Indata['time'] = time();//
	                                            $Indata['status'] = 1;
	                                            $Indata['memo'] = $vurl["memo"];
	                                            $ret = $VideoM->addOne($Indata);
	                                            $PutOssarr[]=$photosavepath;
	                                            $PutOssarr[]=$savepath;
	                                        }
	                                    }
	                                }
	                            }
	                            //采集视频 end
	                            
	                            if($PutOssarr){
	                                
	                                PutOss($PutOssarr);
	                            }
	                            
	                           // echo $uid;exit;
	                            
	                        }
	                        $yiwancheng = $yiwancheng + 1;
	                        usleep(100);
	                    }
	                    //echo $uid;exit;
	                    
	                }
	                
	            }
	        }
	        //Dump($userlist);exit;
    	   if($page<=$countpage){
    	            $this->yiwancheng = ceil($page/$countpage*100);
    	            $page=$page+1;
    	            $this->tip = "正在导入中，请不要关闭浏览器,已导入{$yiwancheng}条数据.........(".$this->yiwancheng."%)";
    	            $this->javascript='<script language="JavaScript">
                                                function myrefresh()
                                                {
                                                window.location="importrun.html?page='.$page.'&caijikey='.$caijikey.'&yiwancheng='.$yiwancheng.'";
                                                }
                                                setTimeout("myrefresh()",1000);
                                                </script> ';
    	  }else{
    	            $this->tip = "导入已完成，共导入{$yiwancheng}.........(100%)";
    	 }
    	}else{
    	        echo"<script>alert('配置错误');history.go(-1);</script>";
    	}
	    $this->display();
	}
	private function purefield($arr,$str){
	    $userlistfieldArr = explode(".", $str);
	    if(count($userlistfieldArr)>1){
	        $ret = $arr;
	        foreach ($userlistfieldArr as $k=>$v){
	            $ret = $ret[$v];
	        }
	    }else{
	        $ret = $arr[$str];
	    }
	    return $ret;
	}
	public function L($name=null, $value=null) {
	    return $this->Lan->L($name,$value);
	}
	public function get_languagecode($reset=0){
	    $path = WR.'/userdata/cache/language/';
	    $cache_name = 'languagelistcode';
	    if(F($cache_name,'',$path) && $reset == 0){
	        $newarr = F($cache_name,'',$path);
	    }else{
	        $where = array();
	        $LanguageM = new LanguageModel();
	        $res = $LanguageM->getList($where);
	        $newarr = array();
	        foreach($res as $v){
	            $newarr[] =$v["code"];
	        }
	        F($cache_name,$newarr,$path);
	    }
	    return $newarr;
	}
	public function LangSet($langs){
	    
	    $languagecode = $this->get_languagecode();
	    if(!in_array($langs, $languagecode)||$langs==""){
	        $langs = "zh-tw";
	    }
	    $file   =  C("LANG_PATH").$langs.'.txt';
	    $cachefile   =  C("LANG_PATH")."cache/".$langs.'.php';
	    if(!file_exists($cachefile)||(filemtime($file)>filemtime($cachefile))){
	        $TranslateM = new TranslateModel();
	        $Wdata["lang"]=$langs;
	        $ret = $TranslateM->getAll($Wdata);
	        $temp = array();
	        foreach ($ret as $k=>$v){
	            $temp[$v["code"]]=$v['content'];
	        }
	        
	        $temp = "<?php return ".var_export($temp, true).";";
	        
	        file_put_contents($cachefile, $temp);
	    }
	    import("Api.lib.Behavior.CheckLangBehavior");
	    $lang = new CheckLangBehavior();
	    
	    $lang->run($langs);
	    
	    return $lang;
	}
	public function ico(){
	    $url="http://newtaiwan.oss-cn-hongkong.aliyuncs.com/data/UserData/images/90/190/90/190/490/408990/1523438552.jpg?x-oss-process=image/resize,m_lfit,w_400,limit_0/auto-orient,1/quality,q_81";
	    $url="/5.jpg";
	    header('Location:'.$url);
	}
	
	/**
	 * 获取自定义数组内容
	 */
	protected function get_attrconf($showtype=1){
	    $path = WR.'/userdata/cache/attr/';
	    $cache_name = 'caijiattr'.$showtype;
	    F($cache_name,NULL,$path);
	    if(F($cache_name,'',$path)){
	        
	        $res = F($cache_name,'',$path);
	    }else{
	        
	        $Attr = new AttrModel();
	        $AttrValue = new AttrValueModel();
	        $AttrList = $Attr->getList(array("showtype"=>$showtype));
	        $res = array();
	        foreach ($AttrList as $k=>$v){
	            $option = array();
	            $AttrValueList = $AttrValue->getList(array("uppercode"=>$v["code"]));
	            foreach ($AttrValueList as $attrv){
	                $res[$v["code"]][] = $attrv["id"];
	            }
	            //$res[$v["code"]] =$option;
	        }
	        F($cache_name,$res,$path);
	    }
	    
 	    
	    return $res;
	}
	protected function get_regionlist($id,$reset=0){
	    $path = WR.'/userdata/cache/area/';
	    $cache_name = 'caijiregionlist_'.$id;
	    if(F($cache_name,'',$path) && $reset == 0){
	        $res = F($cache_name,'',$path);
	    }else{
	        $where = array("country_codeiso"=>$id,"level"=>1);
	        $RegionM = new RegionModel();
	        $res = $RegionM->getList($where);
	        
	        F($cache_name,$res,$path);
	    }
	    $newarr = array();
	    foreach($res as $v){
	        $newarr[] =$v["id"];
	    }
	    
	    return $newarr;
	}
	//获得视频文件的缩略图
	public function getVideoCover($file,$name,$time=1) {
	    if(empty($time))$time = '1';//默认截取第一秒第一帧
	    $str = "/usr/local/bin/ffmpeg -i ".$file." -y -f mjpeg -ss 3 -t ".$time." -s 400x400 ".$name;
	    $result = system($str);
	    return $result;
	}
	
	//获得视频文件的总长度时间和创建时间
	public function getTime($file){
	    $vtime = exec("/usr/local/bin/ffmpeg -i ".$file." 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//");//总长度
	    //$ctime = date("Y-m-d H:i:s",filectime($file));//创建时间
	    return $vtime;
	}
	private function get_UserInfofied($data){
	    $Userdata = array();
	    $baseField = array(
	        "nickname"=>"nickname",
	        "email"=>"email",
	        "gender"=>"gender",
	        "age"=>"age",
	        "height"=>"height",
	        "weight"=>"weight",
	        "area"=>"area",
	        "income"=>"income",
	        "marriage"=>"marriage",
	        "education"=>"education",
	        "work"=>"work",
	        "password"=>"password",
	        "product"=>"product",
	        "phoneid"=>"phoneid",
	        "country"=>"country",
	        "language"=>"language",
	        "version"=>"version",
	        "platformnumber"=>"platformnumber",
	        "fid"=>"fid",
	        "systemversion"=>"systemversion",
	        "user_type"=>"user_type"
	    );
	    $baseFieldKey = array_keys($baseField);
	    
	    $ExtField = array(
	        "mood"=>"mood",
	        "friendsfor"=>"friendsfor",
	        "cohabitation"=>"cohabitation",
	        "dateplace"=>"dateplace",
	        "lovetimes"=>"lovetimes",
	        "charactertype"=>"charactertype",
	        "hobby"=>"hobby",
	        "wantchild"=>"wantchild",
	        "house"=>"house",
	        "car"=>"car",
	        "line"=>"line",
	        "tinder"=>"tinder",
	        "wechat"=>"wechat",
	        "facebook"=>"facebook",
	        "phonenumber"=>"phonenumber",
	        "channelid"=>"channelid",
	        "phonetype"=>"phonetype",
	        "blood"=>"blood",
	        "isopen"=>"isopen",
	        "exoticlove"=>"exoticlove",
	        "sexual"=>"sexual",
	        "livewithparents"=>"livewithparents",
	        "personalitylabel"=>"personalitylabel",
	        "liketype"=>"liketype",
	        "glamour"=>"glamour"
	    );
	    $ExtFieldKey = array_keys($ExtField);
	    
	    foreach($data as $k=>$v){
	        $str = preg_replace('/($s*$)|(^s*^)/m', '',$v);
	        if($str!=""){
	            if(in_array($k, $baseFieldKey)){
	                $Userdata["base"][$baseField[$k]] = $str;
	            }elseif(in_array($k, $ExtFieldKey)){
	                $Userdata["ext"][$ExtField[$k]] = $str;
	            }
	        }
	    }
	    return $Userdata;
	}
	
	private function jiexi($str){
		return object_array(json_decode(base64_decode(base64_decode($str))));
	}

	public function get_url_content($url,$data,$type="POST",$headers=FALSE){
	    if(is_array($data)){
		  $query = http_build_query($data); 
	    }else{
	        $query = $data;
	    }
	    if($type=="GET"){
	       return file_get_contents($url."?".$query);
	    }else{
	        if($headers==FALSE){
	            $headers = array(
	                'Content-type:application/x-www-form-urlencoded'
	            );
	        }
    		$argc = parse_url($query, PHP_URL_PATH);
    		
    		$aa=stristr($url, 'https');//查找字符串http
    		if ($aa != false) {
    		    $result = curl_post_https($url,$argc,$headers);
    		}else {
    		    $result = curl_post_http($url,$argc,$headers);
    		}
    		return $result;
	    }
	}
	public function get_url_curl(){
	    $argc = "is_same_city=0&gender=FeMale&pageSize=20&pageNo=1";
	    $argc = parse_url($argc, PHP_URL_PATH);
	    Dump($argc);
	    $url= "http://www.api.daili.xunyunet.com/users/encounters";
	    $headers = array(
	        'session_id: ece11841-a8f9-443e-8b11-429cdb27e4ab',
	        'device_id: 860410035690163',
	    );
	    
	    //初始化
	    $curl = curl_init();
	    //设置抓取的url
	    curl_setopt($curl, CURLOPT_URL, $url);
	    //设置头文件的信息作为数据流输出
	    curl_setopt($curl, CURLOPT_HEADER, 0);
	    //设置获取的信息以文件流的形式返回，而不是直接输出。
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 0);
	    
	    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	    //设置post方式提交
	   curl_setopt($curl, CURLOPT_POST, 1);
	    //设置post数据
	   $post_data = $argc;
	    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
	    //执行命令
	    $data = curl_exec($curl);
	    //关闭URL请求
	    curl_close($curl);
	    //显示获得的数据
	    print_r($data);exit;
	}
}

?>
