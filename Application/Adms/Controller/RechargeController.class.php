<?php

use Omnipay\Omnipay;

class RechargeController extends BaseAdmsController
{
    private $appId;
    private $appKey;

    public function __construct()
    {
        parent::__construct();
        $this->appId = 'LiMiDa';
        $this->appKey = 'HenanLimidaWangluokejigongsi';
    }
//检测订单状态
    public function chackpayone()
    {
        $this->name = '单个订单验证'; // 进行模板变量赋值
        $this->action = "/Adms/Recharge/chackpayone.html";
        if ($_GET['kerword']) {
            $translateid = $_GET['kerword'];
            $authcode = M('mycard_recharge')->where(array('translateid' => $translateid))->getField('authcode');
            $config = [
                'appId' => $this->appId,
                'appKey' => $this->appKey
            ];
            $gateway = Omnipay::create('MyCard');
            $gateway->initialize($config);
            //$gateway->setTestMode('test');
            $response = $gateway->fetchTransaction(['token' => $authcode])->send();
// further functions below
            $types = $response->isSuccessful();
            $typesa2 = $response->getMessage();
            $typeid = $response->getTransactionId();
            $typesa5 = $response->getAmount();
            $typesa4 = $response->getCurrency();
            $typesa3 = $response->getCardNumber();     // card number
            $typesa1 = $response->getData();
            if ($types == false) {
                $msg = 'false--' . $typesa2;
            } elseif ($types == 1) {
                $msg = '交易成功';
            }

            // message response from MyCard query api

            $this->error = 1;
            $this->msg = $msg;
            // output RAW data
        } else {
            $this->display("checkpayone");
        }
        $this->display("checkpayone");
    }
//订单列表
    public function paylist()
    {
        $this->name = 'mycard订单列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $where = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $where['uid|translateid'] = array('like', "%" . $_GET['kerword'] . "%");
        }
        if (isset($_GET['dateStart']) && $_GET['dateStart'] != "")
            $where["paytime"] = array(array('egt', $_GET["dateStart"]), array('elt', $_GET["dateEnd"]));
        if (isset($_GET['ischeck']) && $_GET['ischeck'] != "") {
            $where["ischeck"] = $_GET["ischeck"];
        }
        $RechargeM = new MycardRechargeModel();
        $DataList = $RechargeM->getListPage($where, $Page, $PageSize);
        foreach ($DataList['list'] as $k => $v) {
            $DataList['list'][$k]["user"] = $this->get_user($v["uid"]);
            $DataList['list'][$k]["money"] = $DataList['list'][$k]["money"]/100;
        }
        $this->action = "/Adms/Recharge/paylist.html";
        $this->Pages = $this->GetPages($DataList);
        $this->DataList = $DataList;
        $this->get = $_GET;
        $this->display("paylist");

    }
    //删除24小时前无效订单
    public function delmycardorder(){
        $timebefore=time()-60*60*24;
        $res['paytime']=array('LT',$timebefore);
        $res['ischeck']=0;
        $res['mycardorders']=M('mycard_recharge')->where($res)->select();
        $res['mycardorderamount']=M('mycard_recharge')->where($res)->delete();
        //将删除的个数以及订单存放在日志记录中
        //将数组转变为json
      $data=json_encode($res,true);
        //加入日志中，以便查看
        log_result($data,'delmycardorder');
        echo 'success';
    }


}

?>
