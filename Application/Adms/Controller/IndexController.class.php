<?php
use Think\Controller;
class IndexController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}
	
	public function Index(){
	    
		$this->name = '首页'; // 进行模板变量赋值
		$listChongjine = array();
		$listTongjiReg = array();
		$listChongShuliang = array();
		$listTongjiChongzhilv = array();

        $productsM  = new ProductsModel();
        $products=$productsM->getAll();
		foreach($products as $k=>$v){
		    
		    //今天充值金额

		    $TodayPayMoney = $this->Chongjine(0,$v['product']);
		    //zuanshi
            $TodayPayMoneyzs = $this->Chongjinezs(0,$v['product']);
            //vip
            $TodayPayMoneyvip = $this->Chongjinevip(0,$v['product']);

		    $listChongjine[$v['product']] = $TodayPayMoney["Count"];
            $listChongjinezs[$v['product']] = $TodayPayMoneyzs["Count"];
            $listChongjinevip[$v['product']] = $TodayPayMoneyvip["Count"];
            $mycardtodaypay=$this->Chongjine(0,'mycard');
            $mycardtodaypayzs=$this->Chongjinezs(0,'mycard');
            $mycardtodaypayvip=$this->Chongjinevip(0,'mycard');

            $paypaltodaypay=$this->Chongjine(0,'paypal');
            $paypaltodaypayzs=$this->Chongjinezs(0,'paypal');
            $paypaltodaypayvip=$this->Chongjinevip(0,'paypal');


		    //今天注册量
		    $TodyReg = $this->TongjiReg(0,$v['product']);
		    $listTongjiReg[$v['product']] = $TodyReg["Count"];
		    //今天充值量
		    $TodayPay = $this->ChongShuliang(0,$v['product']);
		    $listChongShuliang[$v['product']]=$TodayPay["Count"];
            $mycardchongshu=$this->ChongShuliang(0,'mycard');
            $paypalchongshu=$this->ChongShuliang(0,'paypal');





		    //今天充值率
		    $Todaypaylv = $this->TongjiChongzhilv(0,$v['product']);
		    $listTongjiChongzhilv[$v['product']] = $Todaypaylv["Count"];
		}
		//
		$this->listChongjine = $listChongjine;
        $this->listChongjinezs = $listChongjinezs;
        $this->listChongjinevip = $listChongjinevip;
        $this->mycardtodaypay = $mycardtodaypay['Count'];
        $this->mycardtodaypayzs = $mycardtodaypayzs['Count'];
        $this->mycardtodaypayvip = $mycardtodaypayvip['Count'];
        $this->paypaltodaypay =$paypaltodaypay['Count'];
        $this->paypaltodaypayzs =$paypaltodaypayzs['Count'];
        $this->paypaltodaypayvip =$paypaltodaypayvip['Count'];
		$this->listTongjiReg = $listTongjiReg;
		$this->listChongShuliang = $listChongShuliang;
        $this->paypalchongshu=$paypalchongshu;
        $this->mycardchongshu=$mycardchongshu;
		$this->listTongjiChongzhilv = $listTongjiChongzhilv;
		
		//Dump($this->menu);exit;
		$this->display();
	}
    public function Income(){

        $this->name = '首页'; // 进行模板变量赋值
        $this->display();
    }
    public function Incomelv(){
        $this->name = '充值率'; // 进行模板变量赋值
        $this->display();
    }
    public function Incomezc(){
        $this->name = '注册统计'; // 进行模板变量赋值
        $this->display();
    }
    public function onlinecountlist(){
        $this->name = '在线人数统计'; // 进行模板变量赋值
        $this->display();
    }
	public function main(){
		$this->name = '首页'; // 进行模板变量赋值s
		$this->display();
	}
	public function RegAreaTongji(){

		$cachename1 = "RegAreaTongji";
		$cache1=S($cachename1);//设置缓存标示
		// 判断是否有这个查询缓存    
		if(!$cache1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
			$areaList = get_conf('areaListGuowai',"traditional");//居住地只显示一级
			$areas = array();
			$areaQita = array();
			$areaQita["name"] = "其他";
			$areaQita["value"] = 0;
			foreach($areaList as $k=>$v){
			$UserBaseM = new UserBaseModel();
			$Wdata = array();
			$Wdata["area"] = $v['id'];
			$Wdata["product"] = "10008";
			$ret = $UserBaseM->GetCount($Wdata);
			if($ret){
				if($ret<110){
					$areaQita["value"]+=$ret;
				}else{
					$temp['value'] = $ret;
					$temp['name'] = $v['name'];
					$areas[] = $temp;
				}
			}
				
			}
			$areas[]=$areaQita;
			$option = array();
			$option["tooltip"]["trigger"] = "item";
			$option["tooltip"]["formatter"] = "{a} <br/>{b} : {c} ({d}%)";
			
			
			$option["series"][0]["name"] = "地区";
			$option["series"][0]["type"] = "pie";
			$option["series"][0]["radius"] = "80%";
			//$option["series"][0]["center"] = array("50%","60%");
			$option["series"][0]["data"] = $areas;
			$option["series"][0]["itemStyle"]["emphasis"] = array('shadowBlur'=>10,'shadowOffsetX'=>0,'shadowColor'=>'rgba(0, 0, 0, 0.5)');
			

			$cache1 = json_encode($option);
			S($cachename1,$cache1,60*60*8); //设置缓存的生存时间 
		}
				echo $cache1;exit;
	}
	//统计一周注册量
	public function TongjiYiZhouReg(){
        $day=$_POST['days'];
		$days=array();
		for ($x=-$day+1; $x<=0; $x++) {
		  $days[]=$x;
		}
        $productsM  = new ProductsModel();
        $products=$productsM->getAll();
		$res = array();
		$res["tooltip"]["trigger"] = "axis";
		$res["yAxis"]["type"] = "value";
		$res["yAxis"]["axisLabel"]["formatter"] = "{value}";


        foreach($products as $k1=>$v1) {
            $res["series"][$k1]["type"]="line";
            $res["series"][$k1]["name"]=$v1['product'];

		foreach($days as $v){
                $tongji1 = $this->TongjiReg($v, $v1['product']);
                $temp["x"] = date("m/d", $tongji1["days"]);
                $temp["y"] = intval($tongji1["Count"]);
                $res["xAxis"][$k1]["data"][] = $temp["x"];
                $res["series"][$k1]["data"][] = $temp["y"];
            }
		}
		//Dump(json_encode($res));exit;
		echo json_encode($res);
		
	}
    //统计一周注册量（男）
    public function TongjiYiZhouRegnan(){
        $day=$_POST['days']?$_POST['days']:7;
        $days=array();
        for ($x=-$day+1; $x<=0; $x++) {
            $days[]=$x;
        }
        $productsM  = new ProductsModel();
        $products=$productsM->getAll();


        $res = array();
        $res["tooltip"]["trigger"] = "axis";
        $res["yAxis"]["type"] = "value";
        $res["yAxis"]["axisLabel"]["formatter"] = "{value}";


        foreach($products as $k1=>$v1) {
            $res["series"][$k1]["type"]="line";
            $res["series"][$k1]["name"]=$v1['product'];

            foreach($days as $v){
                $tongji1 = $this->TongjiRegnan($v, $v1['product']);
                $temp["x"] = date("m/d", $tongji1["days"]);
                $temp["y"] = intval($tongji1["Count"]);
                $res["xAxis"][$k1]["data"][] = $temp["x"];
                $res["series"][$k1]["data"][] = $temp["y"];
            }
        }
        //Dump(json_encode($res));exit;
        echo json_encode($res);

    }
    //统计一周注册量（女）
    public function TongjiYiZhouRegnv(){
        $day=$_POST['days']?$_POST['days']:7;
        $days=array();
        for ($x=-$day+1; $x<=0; $x++) {
            $days[]=$x;
        }
        $productsM  = new ProductsModel();
        $products=$productsM->getAll();
       /* $days = array(-8,-7,-6,-5,-4,-2,-1,0);*/
        $res = array();
        $res["tooltip"]["trigger"] = "axis";
        $res["yAxis"]["type"] = "value";
        $res["yAxis"]["axisLabel"]["formatter"] = "{value}";


        foreach($products as $k1=>$v1) {
            $res["series"][$k1]["type"]="line";
            $res["series"][$k1]["name"]=$v1['product'];

            foreach($days as $v){
                $tongji1 = $this->TongjiRegnv($v, $v1['product']);
                $temp["x"] = date("m/d", $tongji1["days"]);
                $temp["y"] = intval($tongji1["Count"]);
                $res["xAxis"][$k1]["data"][] = $temp["x"];
                $res["series"][$k1]["data"][] = $temp["y"];
            }
        }
        //Dump(json_encode($res));exit;
        echo json_encode($res);

    }
	//统计一天注册量
	public function TongjiReg($days="0",$product="10008"){
        $redis = $this->redisconn();
		if($days=="0"){
		    $dateStart = strtotime(date("Y-m-d"));             
			$dateEnd = strtotime("+1 day");
		}else{
			$enddays=$days+1;
			$dateStart = strtotime($days." day");
			$dateEnd = strtotime($enddays." day");
			
		}
		$cachename1 = "getUserRegTongjizong-".$days.'-'.$product;

		if($days=="0"){
			$time=60*2;
		}else{
			$time=60*60*8;
		}
		// 判断是否有这个查询缓存
          $keyexit=$redis->exists($cachename1);
		if(!$keyexit){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
			$sql=' select uid from t_user_base where product= ' . $product .' and user_type=1 and regtime> '.$dateStart.' and regtime< '. $dateEnd;
			$sqldata=M()->query($sql);
			$tatalcount=count($sqldata);
			$res["days"] = $dateStart;
			$res["Count"] = $tatalcount;
            $redis_userinfo=json_encode($res);
            $redis->set($cachename1,$redis_userinfo,0,0,$time);//设置缓存的生存时间
		}else{
            $result=$redis->get($cachename1);
            $res=json_decode($result,true);
        }
		return $res;
	}
    //统计一天注册量(男)
    public function TongjiRegnan($days="0",$product="10008"){
        if($days=="0"){
            $dateStart = strtotime(date("Y-m-d"));
            $dateEnd = strtotime("+1 day");
        }else{
            $enddays=$days+1;
            $dateStart = strtotime($days." day");
            $dateEnd = strtotime($enddays." day");

        }
        //$cachename1 = "getUserRegTongjinan".$days.'-'.$product;
        //$cache1=S($cachename1);//设置缓存标示
        /*if($days=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }*/
        //$cache1=0;
        // 判断是否有这个查询缓存
       // if(1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $sql=' select uid from t_user_base where product= ' . $product .' and user_type=1 and gender=1 and regtime> '.$dateStart.' and regtime< '. $dateEnd;
            $sqldata=M()->query($sql);
            $tatalcount=count($sqldata);
            $res["days"] = $dateStart;
            $res["Count"] =$tatalcount;
            //$cache1 = $res;
            //S($cachename1,$cache1,$time); //设置缓存的生存时间
      //  }
       // $res=$cache1;

        return $res;
    }
    //统计一天注册量(女)
    public function TongjiRegnv($days="0",$product="10008"){

        if($days=="0"){
            $dateStart = strtotime(date("Y-m-d"));
            $dateEnd = strtotime("+1 day");
        }else{
            $enddays=$days+1;
            $dateStart = strtotime($days." day");
            $dateEnd = strtotime($enddays." day");

        }

        //$cachename1 = "getUserRegTongjinv-".$days.'-'.$product;
       // $cache1=S($cachename1);//设置缓存标示
        /*if($days=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }*/
        // 判断是否有这个查询缓存
        //if(1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)

            $sql=' select uid from t_user_base where product= ' . $product .' and user_type=1 and gender=2 and regtime> '.$dateStart.' and regtime< '. $dateEnd;
            $sqldata=M()->query($sql);
            $tatalcount=count($sqldata);
            $res["days"] = $dateStart;
            $res["Count"] = $tatalcount;
            //$cache1 = $res;
            //S($cachename1,$cache1,$time); //设置缓存的生存时间
      //  }
        //$res=$cache1;

        return $res;
    }
	//统计一周充值率
	public function TongjiYiZhouCzlv(){
				/*$days=array();
		for ($x=-29; $x<=0; $x++) {
		  $days[]=$x;
		} */
        $days = array(-8,-7,-6,-5,-4,-2,-1,0);
		$res = array();
        $productsM  = new ProductsModel();
        $products=$productsM->getAll();
		$res["tooltip"]["trigger"] = "axis";
		$res["yAxis"]["type"] = "value";
		$res["yAxis"]["axisLabel"]["formatter"] = "{value}%";
        foreach($products as $k1=>$v1) {
            $res["series"][$k1]["type"] = "line";
            $res["series"][$k1]["name"] = $v1['product'];

            foreach ($days as $v) {
                $tongji1 = $this->TongjiChongzhilv($v,$v1['product']);

                $temp["x"] = date("m/d", $tongji1["days"]);
                $temp["y"] = $tongji1["Count"];
                $res["xAxis"][$k1]["data"][] = $temp["x"];
                $res["series"][$k1]["data"][] = $temp["y"];
            }
        }
		echo json_encode($res);
	}
	//统计一天充值率
	public function TongjiChongzhilv($days="0",$product=10008){
		
		if($days=="0"){
			$dateStart = strtotime(date("Y-m-d"));             
			$dateEnd = strtotime(date("Y-m-d",strtotime("+1 day")));
		}else{
			$enddays=$days+1;
			$dateStart = strtotime(date("Y-m-d",strtotime($days." day")));
			$dateEnd = strtotime(date("Y-m-d",strtotime($enddays." day")));
			
		}
			$where = array();
			$PayM = new RechargeModel();
			$where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
			$where["translateid"] = array('neq',"测试订单");
			$where["product"] = $product;
		$cachename1 = "getUserRegTongji_".md5(json_encode($where));
		$cache1=S($cachename1);//设置缓存标示
		if($days=="0"){
			$time=60*2;
		}else{
			$time=60*60*8;
		}
		//$cache1=0;
		// 判断是否有这个查询缓存    
		if(!$cache1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
			$DataList = $PayM->getListPage($where,1,10);
			$TodyReg = $this->TongjiReg($days,$product);
			$czlv = round($DataList["totalCount"]/$TodyReg["Count"]*100,2);
			
			$res["days"] = $dateStart;
			$res["Count"] = $czlv;
			$cache1 = $res;
			S($cachename1,$cache1,$time); //设置缓存的生存时间 
		}
		$res=$cache1;

		return $res;
	}
	//统计一天充值量
	public function ChongShuliang($days="0",$product=10008){
		
		if($days=="0"){
			$dateStart = strtotime(date("Y-m-d"));             
			$dateEnd = strtotime(date("Y-m-d",strtotime("+1 day")));
		}else{
			$enddays=$days+1;
			$dateStart = strtotime(date("Y-m-d",strtotime($days." day")));
			$dateEnd = strtotime(date("Y-m-d",strtotime($enddays." day")));
			
		}
			$where = array();
			$PayM = new RechargeModel();
			$where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
			//$where["translateid"] = array('neq',"测试订单");
        if($product=='mycard'){
            $mycardpay=M('mycard_recharge');
            $where['ischeck']=1;
            $sum_count=$mycardpay->where($where)->count();
            return $sum_count;
        }
        if($product=='paypal'){
            $paypalpay=M('recharge');
            $where['status']=5;//状态为5是paypal支付
            $sum_count=$paypalpay->where($where)->count();
            return $sum_count;
        }



			$where["product"] = $product;
        $where['status']=1;
		$cachename1 = "ChongShuliang_".md5(json_encode($where));
		$cache1=S($cachename1);//设置缓存标示
		if($days=="0"){
			$time=60*2;
		}else{
			$time=60*60*8;
		}
		//$cache1=0;
		// 判断是否有这个查询缓存    
		if(!$cache1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
			$DataList = $PayM->getListPage($where,1,10);
			$res["days"] = $dateStart;
			$res["Count"] = $DataList["totalCount"];
			$cache1 =  $res;
			
			S($cachename1,$cache1,$time); //设置缓存的生存时间 
		}
		$res=$cache1;
		
		return $res;
	}
	
	//统计一天充值金额
	public function Chongjine($days="0",$product=10008){
		
		if($days=="0"){
			$dateStart = strtotime(date("Y-m-d"));             
			$dateEnd = strtotime(date("Y-m-d",strtotime("+1 day")));
		}else{
			$enddays=$days+1;
			$dateStart = strtotime(date("Y-m-d",strtotime($days." day")));
			$dateEnd = strtotime(date("Y-m-d",strtotime($enddays." day")));
			
		}
			$where = array();
			$where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
			//$where["translateid"] = array('neq',"测试订单");

		if($product=='mycard'){
            $mycardpay=M('mycard_recharge');
            $where['ischeck']=1;
            $sum_moneya=$mycardpay->where($where)->sum("money");
            $sum_money=$sum_moneya/100;
            $ret=ceil($sum_money);
            $res["days"] = $dateStart;
            $res["Count"] = $ret;
            return $res;
		}
        if($product=='paypal'){
            $paypalpay=M('recharge');
            $where['status']=5;//状态为5是paypal支付
            $sum_moneya=$paypalpay->where($where)->sum("money");
            $sum_money=$sum_moneya/100;
            $ret=ceil($sum_money);
            $res["days"] = $dateStart;
            $res["Count"] = $ret;
            return $res;
        }

			$where["product"] = $product;
        $where['status']=1;//状态为1是谷歌支付
		$cachename1 = "Chongjine_".md5(json_encode($where));
		$cache1=S($cachename1);//设置缓存标示
		if($days=="0"){
			$time=60*2;
		}else{
			$time=60*60*8;
		}
		// 判断是否有这个查询缓存    
		if(!$cache1){//$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
		    $PayM = new RechargeModel();
			$sum_money = $PayM->sum_money($where);
			$cache1 =ceil($sum_money);
			//S($cachename1,$cache1,$time); //设置缓存的生存时间
		}
		$ret=$cache1;
		$res["days"] = $dateStart;
		$res["Count"] = $ret;
		return $res;
	}
    //统计一天vip充值金额
    public function Chongjinevip($days="0",$product=10008){

        if($days=="0"){
            $dateStart = strtotime(date("Y-m-d"));
            $dateEnd = strtotime(date("Y-m-d",strtotime("+1 day")));
        }else{
            $enddays=$days+1;
            $dateStart = strtotime(date("Y-m-d",strtotime($days." day")));
            $dateEnd = strtotime(date("Y-m-d",strtotime($enddays." day")));

        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        //$where["translateid"] = array('neq',"测试订单");

        if($product=='mycard'){
            $mycardpay=M('mycard_recharge');
            $where['ischeck']=1;
            $where['type']=1;
            $sum_moneya=$mycardpay->where($where)->sum("money");
            $sum_money=$sum_moneya/100;
            $ret=ceil($sum_money);
            $res["days"] = $dateStart;
            $res["Count"] = $ret;
            return $res;
        }
        if($product=='paypal'){
            $paypalpay=M('recharge');
            $where['type']=1;
            $where['status']=5;//状态为5是paypal支付
            $sum_moneya=$paypalpay->where($where)->sum("money");
            $sum_money=$sum_moneya/100;
            $ret=ceil($sum_money);
            $res["days"] = $dateStart;
            $res["Count"] = $ret;
            return $res;
        }

        $where["product"] = $product;
        $where['status']=1;//状态为1是谷歌支付
        $where['type']=1;
        $cachename1 = "Chongjine_".md5(json_encode($where));
        $cache1=S($cachename1);//设置缓存标示
        if($days=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }
        // 判断是否有这个查询缓存
        if(!$cache1){//$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $PayM = new RechargeModel();
            $sum_money = $PayM->sum_money($where);
            $cache1 =ceil($sum_money);
            //S($cachename1,$cache1,$time); //设置缓存的生存时间
        }
        $ret=$cache1;
        $res["days"] = $dateStart;
        $res["Count"] = $ret;
        return $res;
    }
    //统计一天钻石充值金额
    public function Chongjinezs($days="0",$product=10008){

        if($days=="0"){
            $dateStart = strtotime(date("Y-m-d"));
            $dateEnd = strtotime(date("Y-m-d",strtotime("+1 day")));
        }else{
            $enddays=$days+1;
            $dateStart = strtotime(date("Y-m-d",strtotime($days." day")));
            $dateEnd = strtotime(date("Y-m-d",strtotime($enddays." day")));

        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        //$where["translateid"] = array('neq',"测试订单");

        if($product=='mycard'){
            $mycardpay=M('mycard_recharge');
            $where['ischeck']=1;
            $where['type']=3;
            $sum_moneya=$mycardpay->where($where)->sum("money");
            $sum_money=$sum_moneya/100;
            $ret=ceil($sum_money);
            $res["days"] = $dateStart;
            $res["Count"] = $ret;
            return $res;
        }
        if($product=='paypal'){
            $paypalpay=M('recharge');
            $where['type']=3;
            $where['status']=5;//状态为5是paypal支付
            $sum_moneya=$paypalpay->where($where)->sum("money");
            $sum_money=$sum_moneya/100;
            $ret=ceil($sum_money);
            $res["days"] = $dateStart;
            $res["Count"] = $ret;
            return $res;
        }

        $where["product"] = $product;
        $where['status']=1;//状态为1是谷歌支付
        $where['type']=3;
        $cachename1 = "Chongjine_".md5(json_encode($where));
        $cache1=S($cachename1);//设置缓存标示
        if($days=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }
        // 判断是否有这个查询缓存
        if(!$cache1){//$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $PayM = new RechargeModel();
            $sum_money = $PayM->sum_money($where);
            $cache1 =ceil($sum_money);
            //S($cachename1,$cache1,$time); //设置缓存的生存时间
        }
        $ret=$cache1;
        $res["days"] = $dateStart;
        $res["Count"] = $ret;
        return $res;
    }
	//统计一天充值金额
	public function AllChongjine($days="0"){
		
		if($days=="0"){
			$dateStart = strtotime(date("Y-m-d"));             
			$dateEnd = strtotime(date("Y-m-d",strtotime("+1 day")));
		}else{
			$enddays=$days+1;
			$dateStart = strtotime(date("Y-m-d",strtotime($days." day")));
			$dateEnd = strtotime(date("Y-m-d",strtotime($enddays." day")));
			
		}
			$where = array();
			
			$where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
			$where["translateid"] = array('neq',"测试订单");
			
		$cachename1 = "AllChongjine".md5(json_encode($where));
		$cache1=S($cachename1);//设置缓存标示
		if($days=="0"){
			$time=60*2;
		}else{
			$time=60*60*8;
		}
		// 判断是否有这个查询缓存    
		if(!$cache1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
			$PayM = new RechargeModel();
			$sum_money = $PayM->sum_money($where);	
			$cache1 =   ceil($sum_money);
			
			S($cachename1,$cache1,$time); //设置缓存的生存时间 
		}
		$ret=$cache1;
		$res["days"] = $dateStart;
		$cur = convertCurrency("TWD", "CNY");
		$res["Count"] = round($ret*0.7*$cur); 
		
		return $res;
	}
	//收入统计
	public function ShowruTongji(){
		$days=array();
		for ($x=-29; $x<=0; $x++) {
		  $days[]=$x;
		} 
		//$days = array(-8,-7,-6,-5,-4,-2,-1,0);
		$res = array();
		$res["tooltip"]["trigger"] = "axis";
		$res["yAxis"]["type"] = "value";
		$res["yAxis"]["axisLabel"]["formatter"] = "{value}";
		$res["series"][0]["name"]="10008";
		$res["series"][0]["type"]="line";
		foreach($days as $v){
			$tongji1=$this->AllChongjine($v);
			
			$temp["x"]=date("m/d",$tongji1["days"]);
			$temp["y"]=intval($tongji1["Count"]);
			$res["xAxis"][0]["data"][]=$temp["x"];
			$res["series"][0]["data"][]=$temp["y"];
		}
		
		//Dump(json_encode($res));exit;
		echo json_encode($res);
	}
	//统计一月充值金额
	public function AllChongjineYiyue($yue="0",$product=0){
	    header("Content-type: text/html; charset=utf-8"); 
		if($yue=="0"){
			$dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));             
			$dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y"))));
		}else{
			$yue=-$yue;
			$enddays=$yue-1;
			$dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m")-$yue,1,date("Y"))));
			$dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m")-$enddays ,0,date("Y"))));
			/* echo "dateStart:".date("Y-m-d H:i:s",$dateStart);
			echo "<br />";
			echo "dateEnd:".date("Y-m-d H:i:s",$dateEnd);
			echo "<br />"; */
		}
			$where = array();
			$where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
			$where["translateid"] = array('neq',"测试订单");
			if($product){
			    $where["product"] = $product;
			}
		$cachename1 = "AllChongjineYiyue".$yue;
		$cache1=S($cachename1);//设置缓存标示
		if($yue=="0"){
			$time=60*2;
		}else{
			$time=60*60*8;
		}
		//$cache1=0;
		// 判断是否有这个查询缓存    
		if(1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $PayM = new RechargeModel();
            $mycardpay=M('mycard_recharge');
            $mycardwhere=array();
            $mycardwhere["paytime"]=$where["paytime"];
            $mycardwhere['ischeck']=1;
            $mycardallmoney=$mycardpay->where($mycardwhere)->sum('money');
            $mycardallmoney=$mycardallmoney/100;
			$sum_money = $PayM->sum_money($where);
            $sum_money=$sum_money+$mycardallmoney;
			//echo $sum_money."<br />";
			$res["days"] = $dateStart;
			//$cur = convertCurrency("USD", "CNY");
			$cur=1;
			$res["Count"] = round($sum_money*0.65*$cur); 
			//echo $res["Count"]."<br />";
			$cache1 =   $res;
			S($cachename1,$cache1,$time); //设置缓存的生存时间 
		}
		$res=$cache1;
		
		
		
		return $res;
	}
    //统计一月充值金额
    public function AllChongjineYiyuebili($yue="0",$product=0){
        header("Content-type: text/html; charset=utf-8");
        if($yue=="0"){
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y"))));
        }else{
            $yue=-$yue;
            $enddays=$yue-1;
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m")-$yue,1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m")-$enddays ,0,date("Y"))));
            /* echo "dateStart:".date("Y-m-d H:i:s",$dateStart);
            echo "<br />";
            echo "dateEnd:".date("Y-m-d H:i:s",$dateEnd);
            echo "<br />"; */
        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        $where["translateid"] = array('neq',"测试订单");
        if($product){
            $where["product"] = $product;
        }
        $cachename1 = "AllChongjineYiyue".$yue;
        $cache1=S($cachename1);//设置缓存标示
        if($yue=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }
        //$cache1=0;
        // 判断是否有这个查询缓存
        if(1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $PayM = new RechargeModel();
            $mycardpay=M('mycard_recharge');
            $mycardwhere=array();
            $mycardwhere["paytime"]=$where["paytime"];
            $mycardwhere['ischeck']=1;
            $mycardallmoney=$mycardpay->where($mycardwhere)->sum('money');
            $mycardallmoney=$mycardallmoney/100;
            $sum_money = $PayM->sum_money($where);
            $sum_money=$sum_money+$mycardallmoney;
            //echo $sum_money."<br />";
            $res["days"] = $dateStart;
            //$cur = convertCurrency("USD", "CNY");
            $cur=1;
            $res["Count"] = round($sum_money*0.65*$cur);
            //echo $res["Count"]."<br />";
            $cache1 =   $res;
            S($cachename1,$cache1,$time); //设置缓存的生存时间
        }
        $res=$cache1;



        return $res;
    }

    //统计一月vip充值金额
    public function AllChongjineYiyuevip($yue="0",$product=0){
        header("Content-type: text/html; charset=utf-8");
        if($yue=="0"){
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y"))));
        }else{
            $yue=-$yue;
            $enddays=$yue-1;
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m")-$yue,1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m")-$enddays ,0,date("Y"))));
            /* echo "dateStart:".date("Y-m-d H:i:s",$dateStart);
            echo "<br />";
            echo "dateEnd:".date("Y-m-d H:i:s",$dateEnd);
            echo "<br />"; */
        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        $where["translateid"] = array('neq',"测试订单");
        $where['type']=1;
        if($product){
            $where["product"] = $product;
        }
        $cachename1 = "AllChongjineYiyue".$yue;
        $cache1=S($cachename1);//设置缓存标示
        if($yue=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }
        //$cache1=0;
        // 判断是否有这个查询缓存
        if(1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $PayM = new RechargeModel();
            //////////

            ////
            $mycardpay=M('mycard_recharge');
            $mycardwhere=array();
            $mycardwhere["paytime"]=$where["paytime"];
            $mycardwhere['ischeck']=1;
            $mycardwhere['type']=1;
            $mycardallmoney=$mycardpay->where($mycardwhere)->sum('money');
            $mycardallmoney=$mycardallmoney/100;
            $sum_money = $PayM->sum_money($where);
            $sum_money=$sum_money+$mycardallmoney;
            //echo $sum_money."<br />";
            $res["days"] = $dateStart;
            //$cur = convertCurrency("USD", "CNY");
            $cur=1;
            $res["Count"] = round($sum_money*0.65*$cur);
            //echo $res["Count"]."<br />";
            $cache1 =   $res;
            S($cachename1,$cache1,$time); //设置缓存的生存时间
        }
        $res=$cache1;



        return $res;
    }
    //统计一月liwu充值金额
    public function AllChongjineYiyuelw($yue="0",$product=0){
        header("Content-type: text/html; charset=utf-8");
        if($yue=="0"){
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y"))));
        }else{
            $yue=-$yue;
            $enddays=$yue-1;
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m")-$yue,1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m")-$enddays ,0,date("Y"))));

        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        $where["translateid"] = array('neq',"测试订单");
        $where['type']=3;
        if($product){
            $where["product"] = $product;
        }
        $cachename1 = "AllChongjineYiyue".$yue;
        $cache1=S($cachename1);//设置缓存标示
        if($yue=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }
        //$cache1=0;
        // 判断是否有这个查询缓存
        if(1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $PayM = new RechargeModel();
            $mycardpay=M('mycard_recharge');
            $mycardwhere=array();
            $mycardwhere["paytime"]=$where["paytime"];
            $mycardwhere['ischeck']=1;
            $mycardwhere['type']=2;
            $mycardallmoney=$mycardpay->where($mycardwhere)->sum('money');
            $mycardallmoney=$mycardallmoney/100;
            $sum_money = $PayM->sum_money($where);
            $sum_money=$sum_money+$mycardallmoney;
            //echo $sum_money."<br />";
            $res["days"] = $dateStart;
            //$cur = convertCurrency("USD", "CNY");
            $cur=1;
            $res["Count"] = round($sum_money*0.65*$cur);
            //echo $res["Count"]."<br />";
            $cache1 =   $res;
            S($cachename1,$cache1,$time); //设置缓存的生存时间
        }
        $res=$cache1;



        return $res;
    }
	//收入统计按月
	public function ShowruTongjiYuebili(){
        $PMoneyM = new PMoneyModel();
       $days=array();
		for ($x=-11; $x<=0; $x++) {
            $days[] = $x;
        }
        $res = array();
        foreach($days as $v){
            $tongji1=$this->AllChongjineYiyue($v);
            $Wdata=array();
            $Wdata['type']=array('in',array(1,2,3,12,13));
            $Wdata['money'] = array('gt',0);
            if($v){
                if($v=="0"){
                    $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));
                    $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y"))));
                }else{
                    $yue=-$v;
                    $enddays=$yue-1;
                    $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m")-$yue,1,date("Y"))));
                    $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m")-$enddays ,0,date("Y"))));
                }
            }else{
                $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));
                $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y"))));
            }
                $Wdata["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));

            $ret = $PMoneyM->getListGroupOrder($Wdata);
            $qallmoney=0;
            foreach ($ret as $k1=>$v1){
                $qallmoney=$qallmoney+$v1['allmoney'];
            }

            $temp["x"]=date("m月",$tongji1["days"]);
            $allmbfmoney=$qallmoney/100;
            $allmbfamoney=$allmbfmoney/$tongji1["Count"];
            $temp["y"]=intval($allmbfamoney*100);
            $res["xAxis"][]=$temp["x"];
            $res["series"][]=$temp["y"];
        }
        //Dump($res);
        exit(json_encode ($res));

	}

    //收入统计按月
    public function ShowruTongjiYue(){

        $days=array();
        for ($x=-11; $x<=0; $x++) {
            $days[] = $x;
        }
        $res = array();
        foreach($days as $v){
            $tongji1=$this->AllChongjineYiyue($v);
            $temp["x"]=date("m月",$tongji1["days"]);
            $temp["y"]=intval($tongji1["Count"]);
            $res["xAxis"][]=$temp["x"];
            $res["series"][]=$temp["y"];
        }
        //Dump($res);
        exit(json_encode ($res));

    }
    //收入统计按月
    public function ShowruTongjiYuevip(){

        $days=array();
        for ($x=-11; $x<=0; $x++) {
            $days[] = $x;
        }
        $res = array();
        foreach($days as $v){
            $tongji1=$this->AllChongjineYiyuevip($v);
            $temp["x"]=date("m月",$tongji1["days"]);
            $temp["y"]=intval($tongji1["Count"]);
            $res["xAxis"][]=$temp["x"];
            $res["series"][]=$temp["y"];
        }
        //Dump($res);
        exit(json_encode ($res));

    }
    //收入统计按月
    public function ShowruTongjiYuelw(){

        $days=array();
        for ($x=-11; $x<=0; $x++) {
            $days[] = $x;
        }
        $res = array();
        foreach($days as $v){
            $tongji1=$this->AllChongjineYiyuelw($v);
            $temp["x"]=date("m月",$tongji1["days"]);
            $temp["y"]=intval($tongji1["Count"]);
            $res["xAxis"][]=$temp["x"];
            $res["series"][]=$temp["y"];
        }
        //Dump($res);
        exit(json_encode ($res));

    }

    //收入统计按月mycard
    public function ShowruTongjiYuemc(){

        $days=array();
        for ($x=-11; $x<=0; $x++) {
            $days[] = $x;
        }
        $res = array();
        foreach($days as $v){
            $tongji1=$this->AllChongjineYiyuemc($v);
            $temp["x"]=date("m月",$tongji1["days"]);
            $temp["y"]=intval($tongji1["Count"]);
            $res["xAxis"][]=$temp["x"];
            $res["series"][]=$temp["y"];
        }
        //Dump($res);
        exit(json_encode ($res));

    }

    //收入统计按月paypal
    public function ShowruTongjiYuepp(){

        $days=array();
        for ($x=-11; $x<=0; $x++) {
            $days[] = $x;
        }
        $res = array();
        foreach($days as $v){
            $tongji1=$this->AllChongjineYiyuepp($v);
            $temp["x"]=date("m月",$tongji1["days"]);
            $temp["y"]=intval($tongji1["Count"]);
            $res["xAxis"][]=$temp["x"];
            $res["series"][]=$temp["y"];
        }
        //Dump($res);
        exit(json_encode ($res));

    }
    //统计一月充值金额mycard
    public function AllChongjineYiyuemc($yue="0",$product=0){
        header("Content-type: text/html; charset=utf-8");
        if($yue=="0"){
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y"))));
        }else{
            $yue=-$yue;
            $enddays=$yue-1;
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m")-$yue,1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m")-$enddays ,0,date("Y"))));
            /* echo "dateStart:".date("Y-m-d H:i:s",$dateStart);
            echo "<br />";
            echo "dateEnd:".date("Y-m-d H:i:s",$dateEnd);
            echo "<br />"; */
        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        $where["translateid"] = array('neq',"测试订单");
        if($product){
            $where["product"] = $product;
        }
        $cachename1 = "AllChongjineYiyue".$yue;
        $cache1=S($cachename1);//设置缓存标示
        if($yue=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }
        //$cache1=0;
        // 判断是否有这个查询缓存
        if(1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $PayM = new RechargeModel();
            $mycardpay=M('mycard_recharge');
            $mycardwhere=array();
            $mycardwhere["paytime"]=$where["paytime"];
            $mycardwhere['ischeck']=1;
            $mycardallmoney=$mycardpay->where($mycardwhere)->sum('money');
            $mycardallmoney=$mycardallmoney/100;
            //$sum_money = $PayM->sum_money($where);
            $sum_money=$mycardallmoney;
            //echo $sum_money."<br />";
            $res["days"] = $dateStart;
            //$cur = convertCurrency("USD", "CNY");
            $cur=1;
            $res["Count"] = round($sum_money*0.65*$cur);
            //echo $res["Count"]."<br />";
            $cache1 =   $res;
            S($cachename1,$cache1,$time); //设置缓存的生存时间
        }
        $res=$cache1;
        return $res;
    }

    //统计一月充值金额paypal
    public function AllChongjineYiyuepp($yue="0",$product=0){
        header("Content-type: text/html; charset=utf-8");
        if($yue=="0"){
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m"),date("t"),date("Y"))));
        }else{
            $yue=-$yue;
            $enddays=$yue-1;
            $dateStart = strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m")-$yue,1,date("Y"))));
            $dateEnd = strtotime(date("Y-m-d H:i:s",mktime(23,59,59,date("m")-$enddays ,0,date("Y"))));
            /* echo "dateStart:".date("Y-m-d H:i:s",$dateStart);
            echo "<br />";
            echo "dateEnd:".date("Y-m-d H:i:s",$dateEnd);
            echo "<br />"; */
        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        $where["translateid"] = array('neq',"测试订单");
        $where["status"] =5;
        if($product){
            $where["product"] = $product;
        }
        $cachename1 = "AllChongjineYiyue".$yue;
        $cache1=S($cachename1);//设置缓存标示
        if($yue=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }
        //$cache1=0;
        // 判断是否有这个查询缓存
        if(1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $PayM = new RechargeModel();

            $sum_money = $PayM->sum_money($where);
            $res["days"] = $dateStart;
            //$cur = convertCurrency("USD", "CNY");
            $cur=1;
            $res["Count"] = round($sum_money*0.65*$cur);
            //echo $res["Count"]."<br />";
            $cache1 =   $res;
            S($cachename1,$cache1,$time); //设置缓存的生存时间
        }
        $res=$cache1;
        return $res;
    }


    //用户当日转化率
    public function dataingcharge($days="0",$product=10008){
        if($days=="0"){
            $dateStart = strtotime(date("Y-m-d"));
            $dateEnd = strtotime(date("Y-m-d",strtotime("+1 day")));
        }else{
            $enddays=$days+1;
            $dateStart = strtotime(date("Y-m-d",strtotime($days." day")));
            $dateEnd = strtotime(date("Y-m-d",strtotime($enddays." day")));
        }
        $where = array();
        $where["paytime"]=array(array('egt',$dateStart),array('elt',$dateEnd));
        $where["translateid"] = array('neq',"测试订单");
        $where["type"] =1;
        $where["product"] = $product;
        $cachename1 = "getUserRegTongji_".md5(json_encode($where));
        $cache1=S($cachename1);//设置缓存标示
        if($days=="0"){
            $time=60*2;
        }else{
            $time=60*60*8;
        }
        //$cache1=0;
        // 判断是否有这个查询缓存
        if(!$cache1){ //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            /* if(1){*/
            $sql='select uid from t_recharge where type=1 and product= ' . $product . '  and translateid!=\'测试订单\' and paytime> '. $dateStart . ' and paytime<'. $dateEnd .' and
                uid IN (SELECT uid from t_user_base where user_type=1 and product='. $product . ' and regtime>'. $dateStart . ' and regtime<' . $dateEnd . ')';
            $data=M()->query($sql);

            if(!empty($data)){
                $DataList["totalCount"]=count($data);
            }else{
                $DataList["totalCount"]=0;
            }

            $TodyReg = $this->TongjiReg($days,$product);
            $czlv = round($DataList["totalCount"]/$TodyReg["Count"]*100,2);

            $res["days"] = $dateStart;
            $res["Count"] = $czlv;
            $cache1 = $res;
            S($cachename1,$cache1,$time); //设置缓存的生存时间
        }
        $res=$cache1;
        return $res;
    }
    //统计一周zh充值率
    public function TongjiYiZhouCzzhlv(){
        ini_set('max_execution_time', '100000000');
        $days = array(-8,-7,-6,-5,-4,-2,-1,0);
        $res = array();
        $productsM  = new ProductsModel();
        $products=$productsM->getAll();
        $res["tooltip"]["trigger"] = "axis";
        $res["yAxis"]["type"] = "value";
        $res["yAxis"]["axisLabel"]["formatter"] = "{value}%";
        foreach($products as $k1=>$v1) {
            $res["series"][$k1]["type"] = "line";
            $res["series"][$k1]["name"] = $v1['product'];

            foreach ($days as $v) {
                $tongji1 = $this->dataingcharge($v,$v1['product']);

                $temp["x"] = date("m/d", $tongji1["days"]);
                $temp["y"] = $tongji1["Count"];
                $res["xAxis"][$k1]["data"][] = $temp["x"];
                $res["series"][$k1]["data"][] = $temp["y"];
            }
        }
        echo json_encode($res,true);
    }

    //统计储存个数（用户注册后1天内/3天内/7天内/15天内/30天内/90天内充值会员个数）

    public function storagegeshu(){
        //bushizaizheli
        $days=array(1,3,7,15,30,90);
        $res = array();
        foreach($days as $v){
            $tongji1=$this->storagegeshutj($v);
            if($v==3){
                $temp["x"]=$days[0].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
			}elseif($v==7){
                $temp["x"]=$days[1].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]+$res["series"][1]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
			}elseif($v==15){
                $temp["x"]=$days[2].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]+$res["series"][1]+$res["series"][2]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
            }elseif($v==30){
                $temp["x"]=$days[3].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]+$res["series"][1]+$res["series"][2]+$res["series"][3]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
            }elseif($v==90){
                $temp["x"]=$days[4].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]+$res["series"][1]+$res["series"][2]+$res["series"][3]+$res["series"][4]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
            }
			else{
                $temp["x"]=$v.'天之内';
                $temp["y"]=intval($tongji1);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
			}

        }
        //Dump($res);
        exit(json_encode ($res));
    }
    public function storagegeshutj($day=1){
        header("Content-type: text/html; charset=utf-8");
        $sql ='select uid FROM t_recharge where type=1 AND translateid!=\'测试订单\' and (paytime- ' . 60*60*24*$day .')<(select regtime from t_user_base where uid=t_recharge.uid) GROUP BY uid';
		$res = M()->query($sql);
		return $result['Count']=count($res);
	}

	//统计留存率个数 （用户最后登陆时间减去用户注册时间大于1天/3天/7天/15天/30天/90天/180天/365天 个数）
    public function storageliucun(){
        //bushizaizheli
        $days=array(3,7,15,30,90,180,365);
        $res = array();
        foreach($days as $v){
            $tongji1=$this->storageliucungs($v);
            if($v==7){
                $temp["x"]=$days[0].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
            }elseif($v==15){
                $temp["x"]=$days[1].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]+$res["series"][1]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
            }elseif($v==30){
                $temp["x"]=$days[2].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]+$res["series"][1]+$res["series"][2]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
            }elseif($v==90){
                $temp["x"]=$days[3].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]+$res["series"][1]+$res["series"][2]+$res["series"][3]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
            }elseif($v==180){
                $temp["x"]=$days[4].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]+$res["series"][1]+$res["series"][2]+$res["series"][3]+$res["series"][4]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
            }elseif($v==365){
                $temp["x"]=$days[5].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["series"][0]+$res["series"][1]+$res["series"][2]+$res["series"][3]+$res["series"][4]+$res["series"][5]);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
            }
            else{
                $temp["x"]=1 . '-' . $v.'天之间';
                $temp["y"]=intval($tongji1);
                $res["xAxis"][]=$temp["x"];
                $res["series"][]=$temp["y"];
            }

        }
        //Dump($res);
        exit(json_encode ($res));
    }
    public function storageliucungs($day=1){
        header("Content-type: text/html; charset=utf-8");
        $sql ='select uid from t_user_base where logintime<(regtime+' . 60*60*24*$day  .  ') GROUP BY uid';
        $res = M()->query($sql);
        return $result['Count']=count($res);
    }
//留存率 （用户留存率，用户最后登陆时间减去用户注册时间大于1天/3天/7天/15天/30天/90天/180天/365天  除以总注册个数 ）
    public function storageliucunlv(){
        //bushizaizheli

		//用户注册总数
        $sql ='select uid from t_user_base where user_type=1 GROUP BY uid';
        $res = M()->query($sql);
        $allcount=count($res);
        $days=array(3,7,15,30,90,180,365);
        $res = array();
        foreach($days as $v){
            $tongji1=$this->storageliucungs($v);
            if($v==7){
                $temp["x"]=$days[0].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["a"][0]);
                $res["xAxis"][]=$temp["x"];
                $res["a"][]=$temp["y"];
                $res["series"][]=$temp["y"]/$allcount*100;
            }elseif($v==15){
                $temp["x"]=$days[1].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["a"][0]+$res["a"][1]);
                $res["xAxis"][]=$temp["x"];
                $res["a"][]=$temp["y"];
                $res["series"][]=$temp["y"]/$allcount*100;
            }elseif($v==30){
                $temp["x"]=$days[2].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["a"][0]+$res["a"][1]+$res["a"][2]);
                $res["xAxis"][]=$temp["x"];
                $res["a"][]=$temp["y"];
                $res["series"][]=$temp["y"]/$allcount*100;
            }elseif($v==90){
                $temp["x"]=$days[3].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["a"][0]+$res["a"][1]+$res["a"][2]+$res["a"][3]);
                $res["xAxis"][]=$temp["x"];
                $res["a"][]=$temp["y"];
                $res["series"][]=$temp["y"]/$allcount*100;
            }elseif($v==180){
                $temp["x"]=$days[4].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["a"][0]+$res["a"][1]+$res["a"][2]+$res["a"][3]+$res["a"][4]);
                $res["xAxis"][]=$temp["x"];
                $res["a"][]=$temp["y"];
                $res["series"][]=$temp["y"]/$allcount*100;
            }elseif($v==365){
                $temp["x"]=$days[5].'-'.$v.'天之间';
                $temp["y"]=intval($tongji1)-intval($res["a"][0]+$res["a"][1]+$res["a"][2]+$res["a"][3]+$res["a"][4]+$res["a"][5]);
                $res["xAxis"][]=$temp["x"];
                $res["a"][]=$temp["y"];
                $res["series"][]=$temp["y"]/$allcount*100;
            }
            else{
                $temp["x"]=1 . '-' . $v.'天之间';
                $temp["y"]=intval($tongji1);
                $res["xAxis"][]=$temp["x"];
                $res["a"][]=$temp["y"];
                $res["series"][]=$temp["y"]/$allcount*100;
            }

        }
        //Dump($res);
        exit(json_encode ($res));
    }

    //统计普通男用户每半小时的在线人数
    public function onlinecount(){

        ini_set('max_execution_time', '100000000');
        $days = array('00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30',
            '06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00',
            '11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00',
           '17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30');
        $res = array();
        $productsM  = new ProductsModel();
        $products=$productsM->getAll();
        $res["tooltip"]["trigger"] = "axis";
        $res["yAxis"]["type"] = "value";
        $res["yAxis"]["axisLabel"]["formatter"] = "{value}";
        foreach($products as $k1=>$v1) {
            $res["series"][$k1]["type"] = "line";
            $res["series"][$k1]["name"] = $v1['product'];

            foreach ($days as $v) {
                $tongji1 = $this->onlinecounttime($v,$v1['product']);
                $temp["x"] = $v;
                $temp["y"] = $tongji1;
                $res["xAxis"][$k1]["data"][] = $temp["x"];
                $res["series"][$k1]["data"][] = $temp["y"];
            }
        }
        echo json_encode($res,true);
    }
    public function onlinecounttime($time,$product){
	    $onlinecountM=M('onlinecount');
	    $count=$onlinecountM->where(array('time'=>$time,'product'=>$product,'usertype'=>1,'type'=>1,'gender'=>1))->getField('count');
	    if(!$count){
	        $count=0;
        }
        return $count;
    }
    //统计普通女用户每半个小时在线人数
    //统计普通男用户每半小时的在线人数
    public function onlinecountgirl(){

        ini_set('max_execution_time', '100000000');
        $days = array('00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30',
            '06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00',
            '11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00',
            '17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30');
        $res = array();
        $productsM  = new ProductsModel();
        $products=$productsM->getAll();
        $res["tooltip"]["trigger"] = "axis";
        $res["yAxis"]["type"] = "value";
        $res["yAxis"]["axisLabel"]["formatter"] = "{value}";
        foreach($products as $k1=>$v1) {
            $res["series"][$k1]["type"] = "line";
            $res["series"][$k1]["name"] = $v1['product'];

            foreach ($days as $v) {
                $tongji1 = $this->onlinecounttimegirl($v,$v1['product']);
                $temp["x"] = $v;
                $temp["y"] = $tongji1;
                $res["xAxis"][$k1]["data"][] = $temp["x"];
                $res["series"][$k1]["data"][] = $temp["y"];
            }
        }
        echo json_encode($res,true);
    }
    public function onlinecounttimegirl($time,$product){
        $onlinecountM=M('onlinecount');
        $count=$onlinecountM->where(array('time'=>$time,'product'=>$product,'usertype'=>1,'type'=>1,'gender'=>2))->getField('count');
        if(!$count){
            $count=0;
        }
        return $count;
    }

    //统计陪聊女用户每半小时的在线人数
    public function onlinecountthree(){

        ini_set('max_execution_time', '100000000');
        $days = array('00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30',
            '06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00',
            '11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00',
            '17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30');
        $res = array();
        $productsM  = new ProductsModel();
        $products=$productsM->getAll();
        $res["tooltip"]["trigger"] = "axis";
        $res["yAxis"]["type"] = "value";
        $res["yAxis"]["axisLabel"]["formatter"] = "{value}";
        foreach($products as $k1=>$v1) {
            $res["series"][$k1]["type"] = "line";
            $res["series"][$k1]["name"] = $v1['product'];

            foreach ($days as $v) {
                $tongji1 = $this->onlinecounttimethree($v,$v1['product']);
                $temp["x"] = $v;
                $temp["y"] = $tongji1;
                $res["xAxis"][$k1]["data"][] = $temp["x"];
                $res["series"][$k1]["data"][] = $temp["y"];
            }
        }
        echo json_encode($res,true);
    }
    public function onlinecounttimethree($time,$product){
        $onlinecountM=M('onlinecount');
        $count=$onlinecountM->where(array('time'=>$time,'product'=>$product,'usertype'=>3,'type'=>1))->getField('count');
        if(!$count){
            $count=0;
        }
        return $count;
    }

    //统计订单每半小时的数量
    public function ordercount(){

        ini_set('max_execution_time', '100000000');
        $days = array('00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30',
            '06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00',
            '11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00',
            '17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30');
        $res = array();
        $productsM  = new ProductsModel();
        $products=$productsM->getAll();
        $res["tooltip"]["trigger"] = "axis";
        $res["yAxis"]["type"] = "value";
        $res["yAxis"]["axisLabel"]["formatter"] = "{value}";
        foreach($products as $k1=>$v1) {
            $res["series"][$k1]["type"] = "line";
            $res["series"][$k1]["name"] = $v1['product'];

            foreach ($days as $v) {
                $tongji1 = $this->ordercounteach($v,$v1['product']);
                $temp["x"] = $v;
                $temp["y"] = $tongji1;
                $res["xAxis"][$k1]["data"][] = $temp["x"];
                $res["series"][$k1]["data"][] = $temp["y"];
            }
        }
        echo json_encode($res,true);
    }
    public function ordercounteach($time,$product){
        $onlinecountM=M('onlinecount');
        $count=$onlinecountM->where(array('time'=>$time,'product'=>$product,'type'=>2))->getField('count');
        if(!$count){
            $count=0;
        }
        return $count;
    }
}

?>
