<?php
class CelueController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->basename = '地区-'; // 进行模板变量赋值
	}
	public function updatelang(){
	    $SysMsgM = new SysMsgModel();
	    $Wdata = array();
	    $Page = $_GET["Page"] ? $_GET["Page"] : 1;
	    $PageSize = 20;
	    $action = __ACTION__.".html";
	    $Wdata["lang"]="zh-tw";
	    $Wdata['type']=array('in',array(1,4,5,6));
	    $Wdata["subtype"]="1";
	    $ret = $SysMsgM->getListPage($Wdata,$Page,$PageSize);
	    //Dump($ret);exit;
	    $languagecode = $this->get_languagecode();
	    unset($languagecode[0]);
	    foreach($ret["list"] as $k=>$v){
	        unset($v["id"]);
	        $v["lang"] = "none";
	        $sysmsgarr = $v;
	         /* foreach($languagecode as $lang){
	            if($lang){  */
	                $lang = "en";
	                $sysmsgarr["lang"] = $lang;
	                $sysmsgarr["value"] = TranslateMicrosoft($v["value"],$lang,"zh-tw");
	                if(!$sysmsgarr["value"]){
	                    echo "fanyi err";exit;
	                }
	                //Dump($sysmsgarr["value"]);exit;
	                $where = $sysmsgarr;
	                unset($where["value"]);
	                unset($where["starttime"]);
	                unset($where["endtime"]);
	                 $msginfo = $SysMsgM->getOne($where);
	                if($msginfo){
	                    $SysMsgM->updateOne(array("id"=>$msginfo["id"]),$sysmsgarr);
	                }else{
	                    $SysMsgM->addOne($sysmsgarr);
	                } 
	               // Dump($sysmsgarr);
	            /*  }
	            
	        }  */
	        //exit;
	    }
	   // Dump($ret);exit;
	    $start = ($Page-1)*$PageSize;
	    $pagecount = ceil($ret["totalCount"]/$PageSize);;
	    $this->yiwancheng = ceil($Page/$pagecount*100);
	    
	    if($Page<=$pagecount){
	        $Page=$Page+1;
	        $this->tip = "正在导入中，请不要关闭浏览器,共".$ret["totalCount"]."条.........(".$this->yiwancheng."%)";
	        $this->javascript='<script language="JavaScript">
                                        function myrefresh()
                                        {
                                        window.location="'.$action.'?&Page='.$Page.'";
                                        }
                                        setTimeout("myrefresh()",1000);
                                        </script> ';
	    }else{
	        
	        $this->tip = "导入完成，请返回";
	    }
	    //Dump($ret);
	    $this->display();
	}
	public function Lists(){
	    $this->name = '翻译列表'; // 进行模板变量赋值
	    $Page = $_GET["Page"] ? $_GET["Page"] : 1;
	    $PageSize = 20;
	    $this->action = __ACTION__.".html";
	    $this->lang = C("status");
	    $Wdata = array();
	    if($_GET['lang']&&$_GET['lang']!="")
	        $Wdata['lang']=$_GET['lang'];
	        
	        if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
	            $kerword = $_GET['kerword'];
	            $Wdata['msgkey|value|lang']=array('like',"%".$kerword."%");
	        }
	        $SysMsgM = new SysMsgModel();
	        
	        $ret = $SysMsgM->getListPage($Wdata,$Page,$PageSize);
	        
	        $all_page = ceil($ret['totalCount']/$PageSize);
	        $ret["all_page"] = $all_page;
	        
	        foreach($ret['list'] as $k=>$v){
	            $ret['list'][$k]["lang_name"] = $this->lang["lang"]["code"][$v];
	        }
	        
	        $this->DataList = $ret;
	        $this->Pages = $this->GetPages($ret);
	        
	        $this->get = $_GET;
	        $this->display();
	}
	public function Edit(){
	    $this->name = $this->basename.'修改翻译'; // 进行模板变量赋值
	    $status = C("status");
	    $langCode = $status["lang"]["code"];
	    $TranslateM  = new TranslateModel();
	    $code = $_REQUEST['code'];
	    if($code){
    	    if($_POST){
    	        if($_POST["code"]){
    	            $langList = $_POST["lang"];
    	            foreach($langList as $k=>$v){
    	                $where = array();
    	                $where["code"] = $code;
    	                $where["lang"] = $k;
    	               
    	                $tempT = $TranslateM->getOne($where);
    	                if($tempT){
    	                    $TranslateM->updateOne($where,array("content"=>$v));
    	                }else{
    	                    $where["content"] = $v;
    	                    $TranslateM->addOne($where);
    	                }
    	            }
    	            header('Location:Edit.html?code='.$code);
    	            $this->tip = "已保存！";
    	            
    	        }else{
    	            $this->tip = "错误！";
    	        }
    	    }
    	    
    	    $where = array();
    	    $where["code"] = $code;
    	    $temp= $TranslateM->getList($where);
    	    $newTemp = array();
    	   
    	    foreach($temp as $tk=>$tv){
    	        $newTemp[$tv["lang"]]=$tv;
    	    }
    	    $TranslateList = array();
    	    foreach($langCode as $k=>$v){
    	        $TranslateList[$k]["langname"] = $v;
    	        $TranslateList[$k]["content"] = $newTemp[$k]["content"];
    	        $TranslateList[$k]["langcode"] = $k;
    	    }
    	    
    	    $this->TranslateList = $TranslateList;
    	   // Dump( $TranslateList);exit;
    	    $this->code = $code;
	    }else{
	        echo "error";exit;
	    }
	    $this->action =  __ACTION__.".html";
	    $this->display("add");
	}	
	public function clear(){
	    $this->name = '清空缓存'; // 进行模板变量赋值
	    $this->action = __ACTION__.".html";
	    $this->lang = C("status");
	    if($_POST["reset"]=="1"){
    	    $cachefile   =  C("LANG_PATH")."cache";
    	    delFile($cachefile);
    	    $this->tip="缓存已清空！";
	    }
	    $this->display();
	}
	public function import(){
	    $this->name = '翻译列表'; // 进行模板变量赋值
	    $Page = $_GET["Page"] ? $_GET["Page"] : 1;
	    $PageSize = 20;
	    $this->action = "import.html";
	    $status = C("status");
	    if($_FILES["mfile"]){
	        $langPath = WR."/userdata/cache/import/celue/";
	        mkdirs($langPath);
	        $Str = file_get_contents($_FILES["mfile"]["tmp_name"]);
	        $tempfilename = md5($Str);
	        $tempphpfile = $langPath . $tempfilename;
	        $sysName = basename($_FILES["mfile"]["name"]);
	       
	        if($Str){
	            $tempContent = read_file_ByLine($_FILES["mfile"]["tmp_name"]);
	            $msgInfotemp = $tempContent[0];
	            unset($tempContent[0]);
	            $msgInfotemp = explode("|", $msgInfotemp);
	            foreach ($msgInfotemp as $v){
	                $tempv = explode("=", $v);
	                $msgInfo[$tempv[0]] = $tempv[1];
	            }
	            
	            if(!$msgInfo["lang"]||!$msgInfo["subtype"]||!$msgInfo["priority"]||!$msgInfo["type"]){
	                echo "解析头信息错误";exit;
	            }
	            if($msgInfo["time"]){
	                $timeArr = explode("-", $msgInfo["time"]);
	                $starttime = $timeArr[0];
	                $endtime = $timeArr[1];
	            }
	            foreach ($tempContent as $ck=>$cv){
	                $cvArr = explode("=", $cv);
	                $num = count(explode(".", $cvArr[0]));
	                if($cvArr[1]){
	                    $InData = array();
	                    $InData['name'] = $sysName;
	                    $InData['name_md5'] = md5($sysName);
	                    $InData['msgkey'] = $cvArr[0];
	                    $InData['levle'] = $num;
	                    $InData['value'] = $cvArr[1];
	                    $InData['type'] = $msgInfo["type"];
	                    $InData['priority'] = $msgInfo["priority"];
	                    $InData['lang'] = $msgInfo["lang"];
	                    $InData['subtype'] = $msgInfo["subtype"];
	                    $InData['starttime'] = $starttime;
	                    $InData['endtime'] = $endtime;
	                    $Intemp[] = $InData;
	                }
	            }
	            $listData = array(
	                "data"=>$Intemp,
	                "page"=>1,
	                "count"=>count($Intemp),
	                "pagesize"=>$PageSize
	            );
	            file_put_contents($tempphpfile, json_encode($listData));
	            header('Location:importOne.html?name='.$tempfilename);
	        }
	        
	            $this->tip = "文件错误！请重新上传文件";
	        
	    }
	    //Dump($this->lang);exit;
	    $this->display();
	}
	private function foreach_dir($dir){
	    $files = array();
	    if(@$handle = opendir($dir)) { //注意这里要加一个@，不然会有warning错误提示：）
	        while(($file = readdir($handle)) !== false) {
	            if($file != ".." && $file != ".") { //排除根目录；
	                if(is_dir($dir."/".$file)) { //如果是子文件夹，就进行递归
	                    $files[$file] = $this->foreach_dir($dir."/".$file);
	                } else { //不然就将文件的名字存入数组；
	                    if(pathinfo($file, PATHINFO_EXTENSION)=="txt"){
	                       $files[] = $file;
	                    }
	                }
	                
	            }
	        }
	        closedir($handle);
	        return $files;
	    }
	}
	public function importOne(){
	    header("Content-type: text/html; charset=utf-8");   
	    $this->name = '翻译列表'; // 进行模板变量赋值
	    $Page = 1;
	    $PageSize = 100;
	    $this->action = "importOne.html";
	    $status = C("status");
	    $this->langCode = $status["lang"]["code"];
	    $langCodekey = array_keys($this->langCode);
	    $getLcode = $_GET["code"];
	    if($_GET["name"]){
	        $tempphpfile = WR."/userdata/cache/import/celue/".$_GET["name"];
	        if(file_exists($tempphpfile)){
	            $Str = file_get_contents($tempphpfile);
	            $listData = object_array(json_decode($Str));
	            $Page=$listData["page"];
	            $listData["page"]=$listData["page"]+1;
	        }else{
	          echo '导入错误';exit;   
	        }
	        //写入文件
	        file_put_contents($tempphpfile, json_encode($listData));
	        $SysMsgM = new SysMsgModel();
	        $dataList = $listData["data"];
	        $start = ($Page-1)*$PageSize;
	        $pagecount = ceil($listData["count"]/$PageSize);
	        $dataList = array_slice($dataList,$start,$PageSize);
	        $this->yiwancheng = ceil($Page/$pagecount*100);
	        
	        if($Page<=$pagecount){
	            foreach ($dataList as $k=>$v){
	                $WData = $v;
	                unset($InData['priority']);
	                unset($InData['name']);
	                unset($InData['value']);
	                
	               $exist=$SysMsgM->getOne($WData);
	               if($exist){
	                   $SysMsgM->updateOne(array("id"=>$exist["id"]),$v);
	                   $sysmsgid = $exist["id"];
	               }else{
	                   $sysmsgid =$SysMsgM->addOne($v);
	               }
	            }
	            
	            $Page=$Page+1;
	            $this->tip = "正在导入中，请不要关闭浏览器.........(".$this->yiwancheng."%)";
	            $this->javascript='<script language="JavaScript">
                                        function myrefresh()
                                        {
                                            window.location="importOne.html?page='.$Page.'&name='.$_GET["name"].'";
                                        }
                                    setTimeout("myrefresh()",1000);
                                   </script> ';
	        }else{
	            unlink($tempphpfile);
	            $this->tip = "导入完成，请返回";
	        }
	    }else{
	        $this->tip = "文件错误！请重新上传文件";
	    }
	    //Dump($this->lang);exit;
	    $this->display();
	}
	
}

?>
