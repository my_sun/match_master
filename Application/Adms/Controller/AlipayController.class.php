<?php
/**
 * 微信支付
 */
class AlipayController extends BaseAdmsController
{
    public function __construct()
    {
        parent::__construct();

    }

    //跳转到手机app支付
    public function pay()
    {
        Vendor('alisdk.wappay.service.AlipayTradeService');
        Vendor('alisdk.wappay.buildermodel.AlipayTradeWapPayContentBuilder');
        $config = C('ALIPAY_CONFIG');
        //商户订单号，商户网站订单系统中唯一订单号，必填
        //$out_trade_no = $_POST['WIDout_trade_no'];
        if ($_POST['WIDtotal_amount']) {
            $out_trade_no = '27201' . date('YmdHis', time());
            //订单名称，必填
            $subject = $_POST['WIDsubject'];
            //付款金额，必填
            $total_amount = $_POST['WIDtotal_amount'];
            //$total_amount = '0.01';

            //商品描述，可空
            $body = $_POST['WIDbody'];

            //超时时间
            $timeout_express = "1m";

            $payRequestBuilder = new \AlipayTradeWapPayContentBuilder();
            $payRequestBuilder->setBody($body);
            $payRequestBuilder->setSubject($subject);
            $payRequestBuilder->setOutTradeNo($out_trade_no);
            $payRequestBuilder->setTotalAmount($total_amount);
            $payRequestBuilder->setTimeExpress($timeout_express);

            $payResponse = new \AlipayTradeService($config);
            $result = $payResponse->wapPay($payRequestBuilder, $config['return_url'], $config['notify_url']);

            return;
        } else {
            $this->action = __ACTION__ . ".html";
            $this->display();
        }

    }
        //支付宝跳转扫码支付
    public function codepay(){
        if ($_POST['WIDtotal_amount']) {
            $config = C('ALIPAY_CONFIG');
            Vendor('alisdk.wappay.service.AlipayTradeService');
            Vendor('alisdk.wappay.buildermodel.AlipayTradePagePayContentBuilder');
            //商户订单号，商户网站订单系统中唯一订单号，必填
            $out_trade_no = trim($_POST['WIDout_trade_no']);
            //订单名称，必填
            $subject = trim($_POST['WIDsubject']);
            //付款金额，必填
            $total_amount = trim($_POST['WIDtotal_amount']);
            //商品描述，可空
            $body = trim($_POST['WIDbody']);
            //构造参数
            $payRequestBuilder = new AlipayTradePagePayContentBuilder();
            $payRequestBuilder->setBody($body);
            $payRequestBuilder->setSubject($subject);
            $payRequestBuilder->setTotalAmount($total_amount);
            $payRequestBuilder->setOutTradeNo($out_trade_no);

            $aop = new AlipayTradeService($config);

            /**
             * pagePay 电脑网站支付请求
             * @param $builder 业务参数，使用buildmodel中的对象生成。
             * @param $return_url 同步跳转地址，公网可以访问
             * @param $notify_url 异步通知地址，公网可以访问
             * @return $response 支付宝返回的信息
             */
            $response = $aop->pagePay($payRequestBuilder, $config['return_url'], $config['notify_url']);

            //输出表单
            var_dump($response);
        }else{
            $this->display('codepay');
        }
    }

    //跳转到手机app支付异步回调
    public function notify_url()
    {
        Vendor('alisdk.wappay.service.AlipayTradeService');
        $config = C('ALIPAY_CONFIG');
        $arr = $_POST;
        $alipaySevice = new AlipayTradeService($config);
        $alipaySevice->writeLog(var_export($_POST, true));
        $result = $alipaySevice->check($arr);

        /* 实际验证过程建议商户添加以下校验。
        1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
        2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
        3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
        4、验证app_id是否为该商户本身。
        */
        if ($result) {//验证成功
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //请在这里加上商户的业务逻辑程序代

            //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
            //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
            //商户订单号
            $out_trade_no = $_POST['out_trade_no'];
            //支付宝交易号

            $trade_no = $_POST['trade_no'];
            //交易状态
            $trade_status = $_POST['trade_status'];

            if ($_POST['trade_status'] == 'TRADE_FINISHED') {

                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //请务必判断请求时的total_amount与通知时获取的total_fee为一致的
                //如果有做过处理，不执行商户的业务程序

                //注意：
                //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
            } else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //请务必判断请求时的total_amount与通知时获取的total_fee为一致的
                //如果有做过处理，不执行商户的业务程序
                //注意：
                //付款完成后，支付宝系统发送该交易状态通知
            }
            //——请根据您的业务逻辑来编写程序

            echo "success";        //请不要修改或删除

        } else {
            //验证失败
            echo "fail";    //请不要修改或删除
        }

    }





}