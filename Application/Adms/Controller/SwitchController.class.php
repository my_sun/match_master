<?php
class SwitchController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->basename = '地区-'; // 进行模板变量赋值
		$this->cachepath = WR.'/userdata/cache/switch/';
	}
	
	public function lists(){
		$this->name = $this->basename.'列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		$Attr = new SwitchModel();
		$status = C("status");
		$this->switch_type = $status["switch"]["type"];
		//$AttrValue = new AttrValueModel();
		$Wdata = array();
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword = $_GET['kerword'];
			if($kerword){
				$Wdata['code|name']=array('like',"%".$kerword."%");
			}
		}
		
		$ret = $Attr->getListPage($Wdata,$Page,$PageSize);
		
		$this->ListData = $ret["list"];
		$this->Pages = $this->GetPages($ret);
		
		//Dump($this->ListData);exit;
		$this->get = $_GET;
		$this->action = __ACTION__.".html";
		$this->display();
	}
	public function Add(){
        if($_GET['id']){
            $this->name = $this->basename.'修改'; // 进行模板变量赋值
            $msglist=M('switch')->where(array('id'=>$_GET['id']))->find();
            $this->msglist=$msglist;
        }else{
            $this->name = $this->basename.'添加'; // 进行模板变量赋值
        }

		$status = C("status");
		$this->usertypestatus = $status["user"]["user_type"];
		$productsM  = new ProductsModel();
        $Attr = new SwitchModel();
		$this->products=$productsM->getAll();
		if($_POST){
		    if($_POST["name"]&&$_POST["key"]){
		            $map["name"] = $_POST["name"];
		            $map["value"] = $_POST["value"];
		            $map["type"] = $_POST["type"];
		            $map["usertype"] = $_POST["usertype"];
		            $map["product"] = $_POST["product"];
				if($_POST["id"]){
                    $data=$Attr->updateOne(array('id'=>$_POST["id"]),$map);
                    $this->tip = $_POST["name"]."已更改";
				}else{
                    $map = array();
                    $map["key"] = $_POST["key"];
                    $map["type"] = $_POST["type"];
                    $AttrValue = $Attr->getOne($map);
                    if($AttrValue) {
                        $this->tip = "key不能重复！";
                    }else{
                        $Attr->addOne($map);
                        $this->tip = $_POST["name"]."已添加";
					}
				}
                    $redis=$this->redisconn();
                    $redis->del('switch1');
                    $redis->del('switch2');
                    $redis->del('switch3');
		            //添加翻译
                    $this->translate_add($map["key"], $map["name"]);

		        }
		        
		    }

		$this->action =  __ACTION__.".html";
		$this->display();
	}	
	
	/*
	 * 删除
	 */
	public function del(){
	    
	    $code =$_REQUEST["id"];
	    $key=$_REQUEST["key"];
	    if($code){
	        $AttrValueM = new SwitchModel();
	        $map = array();
	        $map["id"] = $code;
	        $AttrValueM->delOne($map);
	        //delFile($this->cachepath);
            $redis=$this->redisconn();
            $redis->del('switch1');
            $redis->del('switch2');
            $redis->del('switch3');
	          //删除翻译
            $this->translate_del($key);
	    }
	    header('Location:'.$_SERVER['HTTP_REFERER']);
	}
	public function Edit(){
		$this->name = $this->basename.'修改'; // 进行模板变量赋值
	}	

	public function Ajax(){
		$type = $_POST["type"];
		switch ($type){
			case "updata_status":
				$id = (int)$_POST["id"];
				$status = $_POST["status"];
				if(is_integer($id)){
					$CountryM = new CountryModel();
					$CountryM->updateOne(array("id"=>$id), array("status"=>$status));
                    $redis=$this->redisconn();
                    $redis->del('switch1');
                    $redis->del('switch2');
                    $redis->del('switch3');
					$result["msg"] = "ok";
					$result["Succeed"] = 1;
					echo json_encode($result);exit;
				}else{
					$result["msg"] = "ID错误".$id;
					$result["Succeed"] = 0;
					echo json_encode($result);exit;
				}
				break; 
		}
		
	}	
}

?>
