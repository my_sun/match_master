<?php

/**
 * Created by PhpStorm.
 * User: 亮亮
 * Date: 2018/4/24
 * Time: 10:21
 */
class SensitiveController extends BaseAdmsController
{
    public function __construct ()
    {
        parent::__construct ();
        $this->basename = '敏感词-'; // 进行模板变量赋值
        $this->cachepath=CHCHEPATH_SENSITIVE;
    }

    public function lists ()
    {
        $this->name = $this->basename . '列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 50;
        $Sens = new SensitiveWordModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|name'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $Sens->getListPage ($Wdata, $Page, $PageSize);
        $this->DataList= $ret["list"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display ();
    }
    public function add(){
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        if($_POST){
            if($_POST['name']) {
                $Sens = new SensitiveWordModel();
                $map = explode("|", $_POST['name']);
                //同时添加到阿里后台
                $checktextC= new ChecktextController();
                foreach ($map as $k => $v) {
                    $AttrValue = $Sens->getOne(array('name' => $v));
                    if ($AttrValue) {
                        continue;
                    }
                    $Sens->addOne(array('name' => $v));
                    $checktextC->createKeyword(5309,$v);
                }

                //删除缓存
                deldir($this->cachepath);
                $this->tip = "已添加完毕！";
            }else{
                $this->tip = "name不能为空！";
            }
            $this->action =  __ACTION__.".html";
            $this->display();
            }else{
            $this->display();
            }
    }
    /*
	 * 删除
	 */
    public function del(){
        $id =$_REQUEST["id"];
        $code=$_REQUEST["code"];
        if($id){
            $Sens = new SensitiveWordModel();
            $map = array();
            $map["id"] = $id;
            $Sens->delOne($map);
            //删除翻译
            $this->translate_del($code);
            //$redis=$this->redisconn();
           // $redis->del('gift_10008'.$id);
            //删除翻译
            deldir($this->cachepath);
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }
    //名称转化为code,并添加翻译
   /* public function conversion(){
        $Sens = new SensitiveWordModel();
        $TranslateM  = new TranslateModel();
        $M=M('sensitive_word');
        //查询出未添加code的礼物title
       $res=$M->query("select name from t_sensitive_word where code is null or code=''");
        foreach($res as $k=>$v){
            $nStr = PinyinController::tradition2simple($v['name']);   //将繁体转化为简体
            $string[$v['name']] = PinyinController::get1($nStr, 'utf-8');//将简体转化为拼音
        }
        //循环遍历将code存入数据库并添加到翻译
        foreach($string as $k1=>$v1){
            $Sens->updateOne(array('name'=>$k1),array('code'=>$v1));
            $tran = array();
            $where["code"] = strtoupper($v1);
            $tran["code"] = strtoupper($v1);
            $tran["content"] = $k1;
            $tempT = $TranslateM->getOne($where);
            if($tempT){
                continue;
                // return $TranslateM->updateOne($where,array("content"=>$tran["content"]));
            }else{
                $tran["lang"]="zh-tw";
                $TranslateM->addOne($tran);
            }
        }

        //print_r($string);
        header('Location:'.$_SERVER['HTTP_REFERER']);

    }*/

}







