<?php
class TranslateController extends BaseAdmsController {

	public function __construct(){
		parent::__construct();
		$this->basename = '地区-'; // 进行模板变量赋值
	}

	public function nottranslate(){
	    $this->name = '翻译列表'; // 进行模板变量赋值
	    $Page = $_GET["Page"] ? $_GET["Page"] : 1;
	    $PageSize = 20;
	    $this->action = __ACTION__.".html";
	    $this->lang = C("status");
	    $Wdata = array();
	    
	    $TranslateM = new TranslateModel();
	    $sql="SELECT code,COUNT(*) as count,content FROM  t_translate GROUP BY  code HAVING COUNT(*) <5";
	    $res = $TranslateM->query($sql);
	    foreach ($res as $v){
	        $this->translate_edit($v["code"],$v["content"]);
	    }
        delFile(WR.'/userdata/lang/cache/');
	    Dump($res);
	    //exit;
        header('Location:'.$_SERVER['HTTP_REFERER']);

	}
	/*
	 * 修改翻译
	 */
	protected function translate_edit($code,$content){
	    
	    $languagecode = $this->get_languagecode();
	    $TranslateM  = new TranslateModel();
	    $tran = array();
	    $where["code"] = strtoupper($code);
	    $tran["code"] = strtoupper($code);
	    $tran["content"] = $content;
	    $templ = $TranslateM->getOne($where);
	    if($templ&&$code&&$content){
	        
	        foreach($languagecode as $v){
	            $tempv = TranslateMicrosoft($content,$v['id']);
	            if($tempv){
	                $tran["content"] = $tempv;
	            }
	            $tran["lang"] = $v['id'];
	            $where["lang"] = $v['id'];
	            $tempT = $TranslateM->getOne($where);
	            if($tempT){
	                $TranslateM->updateOne($where,array("content"=>$tran["content"]));
	            }else{
	               $TranslateM->addOne($tran);
	            }
	        }
	    }
	        return true;
	    
	}
	public function lists(){
	    $this->name = '翻译列表'; // 进行模板变量赋值
	    $Page = $_GET["Page"] ? $_GET["Page"] : 1;
	    $PageSize = 20;
	    $this->action = __ACTION__.".html";
	    $this->lang = C("status");
	    $Wdata = array();
	    if($_GET['lang']&&$_GET['lang']!="")
	        $Wdata['lang']=$_GET['lang'];
	        
	        if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
	            $kerword = $_GET['kerword'];
	            $Wdata['code|content']=array('like',"%".$kerword."%");
	        }
	        $TranslateM = new TranslateModel();
	        
	        $ret = $TranslateM->getListPage($Wdata,$Page,$PageSize);
	        //Dump($ret);exit;
	        $all_page = ceil($ret['totalCount']/$PageSize);
	        $ret["all_page"] = $all_page;
	        
	        foreach($ret['list'] as $k=>$v){
	            $ret['list'][$k]["lang_name"] = $this->lang["lang"]["code"][$v];
	        }
	        
	        $this->DataList = $ret;
	        $this->Pages = $this->GetPages($ret);
	        
	        $this->get = $_GET;
	        $this->display();
	}
	public function edit(){
	    $this->name = $this->basename.'修改翻译'; // 进行模板变量赋值
	    $status = C("status");
	    $langCode = $status["lang"]["code"];
	    $TranslateM  = new TranslateModel();
	    $code = $_REQUEST['code'];
	    if($code){
    	    if($_POST){
    	        if($_POST["code"]){
    	            $langList = $_POST["lang"];
    	            foreach($langList as $k=>$v){
    	                $where = array();
    	                $where["code"] = $code;
    	                $where["lang"] = $k;
    	               
    	                $tempT = $TranslateM->getOne($where);
    	                if($tempT){
    	                    $TranslateM->updateOne($where,array("content"=>$v));
    	                }else{
    	                    $where["content"] = $v;
    	                    $TranslateM->addOne($where);
    	                }
    	            }
    	            header('Location:Edit.html?code='.$code);
    	            $this->tip = "已保存！";
    	            
    	        }else{
    	            $this->tip = "错误！";
    	        }
    	    }
    	    
    	    $where = array();
    	    $where["code"] = $code;
    	    //$where["lang"] = "zh-tw";
    	   
    	    $temp= $TranslateM->getList($where);
    	    
    	    $newTemp = array();
    	    
    	    foreach($temp as $tk=>$tv){
    	        $newTemp[$tv["lang"]]=$tv;
    	    }
    	    $TranslateList = array();
    	    foreach($langCode as $k=>$v){
    	        $TranslateList[$k]["langname"] = $v;
    	        $TranslateList[$k]["content"] = $newTemp[$k]["content"];
    	        $TranslateList[$k]["langcode"] = $k;
    	    }
    	    
    	    $this->TranslateList = $TranslateList;
    	   // Dump( $TranslateList);exit;
    	    $this->code = $code;
	    }else{
	        echo "error";exit;
	    }
	    $this->action =  __ACTION__.".html";
	    $this->display("add");
	}	
	public function clear(){
	    $this->name = '清空缓存'; // 进行模板变量赋值
	    $this->action = __ACTION__.".html";
	    $this->lang = C("status");
	    if($_POST["reset"]=="1"){
    	    $cachefile   =  C("LANG_PATH")."cache";
    	    delFile($cachefile);
    	    $this->tip="缓存已清空！";
	    }
	    $this->display();
	}
	public function import(){
	    $this->name = '翻译列表'; // 进行模板变量赋值
	    $Page = $_GET["Page"] ? $_GET["Page"] : 1;
	    $PageSize = 20;
	    
	    $this->action = "import.html";
	    $status = C("status");
	    $this->langCode = $status["lang"]["code"];
	    
	    if($_FILES["langfile"]){
	        $langPath = WR."/userdata/lang/";
	        $langCodekey = array_keys($this->langCode);
	        $langPathFile = $langPath.$langPath;
	        $tempfilePath = $langPath.$_FILES["langfile"]["name"];
	        $fileName = $_FILES["langfile"]["name"];
	        $fileName = basename($fileName,".txt");
	        if(in_array($fileName, $langCodekey)){
	            
	            copy($_FILES["langfile"]["tmp_name"],$tempfilePath);
	            unlink($_FILES["langfile"]["tmp_name"]);
	            header('Location:importOne.html?code='.$fileName);
	            exit;
	            
	        }else{
	            $this->tip = "语言文件名称错误！请重新上传文件";
	        }
	        
	        
	    }
	    //Dump($this->lang);exit;
	    $this->display();
	}
	public function importOne(){
	    $this->name = '翻译列表'; // 进行模板变量赋值
	    $Page = $_GET["page"] ? $_GET["page"] : 1;
	    $PageSize = 100;
	    
	    $this->action = "importOne.html";
	    $status = C("status");
	    $this->langCode = $status["lang"]["code"];
	    $langCodekey = array_keys($this->langCode);
	    $getLcode = $_GET["code"];
	    if($getLcode&&in_array($getLcode, $langCodekey)){
	        $langPath = WR."/userdata/lang/".$getLcode.".txt";
	        $tempphpfile = WR."/userdata/cache/langimport/".$getLcode.".temp.php";
	        if(file_exists($tempphpfile)){
	            $Str = file_get_contents($tempphpfile);
	            $listData = object_array(json_decode($Str));
	            $Page=$listData["page"];
	            $listData["page"]=$listData["page"]+1;
	        }else{
	            
	            $file = fopen($langPath, "r");
	            $langArr=array();
	            $i=0;
	            //输出文本中所有的行，直到文件结束为止。
	            while(! feof($file))
	            {
	                $tempcontent = fgets($file);
	                $tempcontent = yang_gbk2utf8($tempcontent);
	                if(strpos($tempcontent,"=") === false){     //使用绝对等于
	                    //不包含
	                    
	                }else{
	                    $tempcontent = explode("=", $tempcontent);
	                    $temp = str_replace(PHP_EOL, '', $tempcontent[1]);
	                    //包含
	                    $langArr[]= array(
	                        "code"=>trim(strtoupper($tempcontent[0])),
	                        "lang"=>$getLcode,
	                        "content"=>$temp,
	                    );//fgets()函数从文件指针中读取一行
	                }
	                
	            }
	            fclose($file);
	            //Dump($langArr);exit;
	            $listData = array(
	                "langArr"=>$langArr,
	                "page"=>1,
	                "count"=>count($langArr),
	                "pagesize"=>$PageSize
	            );
	            
	            
	        }
	        //写入文件
	        file_put_contents($tempphpfile, json_encode($listData));
	        
	        
	        $TranslateM = new TranslateModel();
	        $langArr = $listData["langArr"];
	        $start = ($Page-1)*$PageSize;
	        $pagecount = ceil($listData["count"]/$PageSize);
	        $langArr = array_slice($langArr,$start,$PageSize);
	        $this->yiwancheng = ceil($Page/$pagecount*100);
	        
	        if($Page<=$pagecount){
	            
	            foreach ($langArr as $k=>$v){
	                $where = $v;
	                $tcontent = $where["content"];
	                unset($where["content"]);
	                $tempT = $TranslateM->delOne($where);
	                //if($tempT){
	                //    $TranslateM->updateOne($where,array("content"=>$tcontent));
	               // }else{
	                  $TranslateM->addOne($v);
	              //  }
	            }
	            
	            $Page=$Page+1;
	            $this->tip = "正在导入中，请不要关闭浏览器,共".$listData["count"]."条.........(".$this->yiwancheng."%)";
	            $this->javascript='<script language="JavaScript">
                                        function myrefresh()
                                        {
                                        window.location="/Adms/Translate/importOne.html?code='.$getLcode.'&page='.$Page.'";
                                        }
                                        setTimeout("myrefresh()",1000);
                                        </script> ';
	        }else{
	            unlink($tempphpfile);
	            $this->tip = "导入完成，请返回";
	        }
	        
	    }else{
	        $this->tip = "语言文件名称错误！请重新上传文件";
	    }
	    //Dump($this->lang);exit;
	    $this->display();
	}
	
}

?>
