<?php
class WebmsgController extends BaseAdmsController
{

    public function index()
    {
        $this->name = '文章列表';
        $Article = M('article');
        $articles = $Article->select();
        foreach ($articles as $key => $value) {
            $articles[$key]['admin'] = M('admin_user')->where('id', $value['admin_id'])->getField('name');
            $articles[$key]['edit_admin'] = M('admin_user')->where('id', $value['edit_admin_id'])->getField('name');
            $articles[$key]['content'] = base64_decode($value['content']);
            $articles[$key]['catename']=M('article_cate')->where('id',$value['article_cate_id'])->getField('name');
        }
        $this->assign('articles', $articles);
        $this->display();
    }

    public function msgedit(){
        if ($_GET['id'] || I('post.id')) {
            $id = $_GET['id'];
            //post收到id为修改文章
            if (I('post.')) {
                $rules = array(
                    array('title', 'require', '标题必须填！'), //默认情况下用正则进行验证
                    array('content', 'require', '内容不能为空！'),
                );
                $Article = M('article'); // 实例化article对象
                //自动验证
                if (!$Article->validate($rules)->create()) {
                    // 如果创建失败 表示验证没有通过 输出错误提示信息
                    $reuslt = array(
                        'code' => '2',
                        'msg' => $Article->getError(),
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } else {
                    // 验证通过 可以进行其他数据操作
                    $data = array();
                    //设置修改人
                    $adminuser=session("AdminUser");
                    $data['edit_admin_id'] =$adminuser['id'];
                    $data['update_time'] = time();
                    $data['title'] = I('post.title');
                    $id = I('post.id');
                    $data['description'] = I('post.description');
                    $data['content'] = base64_encode(I('post.content'));
                    $data['article_cate_id'] = I('post.article_cate_id') ? I('post.article_cate_id') : 1;
                    $res = $Article->where(array('id' => $id))->save($data);
                    if ($res) {
                        $reuslt = array(
                            'code' => '1',
                            'msg' => '修改文章成功',
                            'url' => "/adms/webmsg/index"
                        );
                        exit(json_encode($reuslt));
                    }
                }
            } else {
                 $articlecate = M('article_cate')->where(array('id'=>$id))->find();
                $articleall=M('article_cate')->select();
                $cates_all = $this->catelist($articleall);
               $this->assign('cates',$cates_all);
                if (!empty($articlecate)) {
                    $this->assign('cate', $articlecate);
                }
                $article = M('article')->where('id', $id)->find();
                if (!empty($article)) {
                    $article['content'] = base64_decode($article['content']);
                    $this->assign('article', $article);
                    $this->display();
                } else {
                    $reuslt = array(
                        'code' => '2',
                        'msg' =>"所传id不正确！",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                }

            }
        } else {
            if (I('post.')) {
                //设置验证规则
                $rules = array(
                    array('title', 'require', '标题必须填！'), //默认情况下用正则进行验证
                    array('content', 'require', '内容不能为空！'),
                );
                $Article = M('article'); // 实例化article对象
                //自动验证
                if (!$Article->validate($rules)->create()) {
                    // 如果创建失败 表示验证没有通过 输出错误提示信息
                    $reuslt = array(
                        'code' => '2',
                        'msg' => $Article->getError(),
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } else {
                    // 验证通过 可以进行其他数据操作
                    $data = array();
                    //设置创建人
                    $adminuser=session("AdminUser");
                    $data['admin_id'] =$adminuser['id'];
                    //设置修改人
                    $data['edit_admin_id'] = $data['admin_id'];
                    $data['create_time'] = time();
                    $data['update_time'] = $data['create_time'];
                    $data['title'] = I('post.title');
                    $data['description'] = I('post.description');
                    $data['content'] = base64_encode(I('post.content'));
                    $data['article_cate_id'] = I('post.article_cate_id') ? I('post.article_cate_id') : 1;
                    $res = $Article->add($data);
                    if ($res) {
                        $reuslt = array(
                            'code' => '1',
                            'msg' => '新增文章成功',
                            'url' => "/adms/webmsg/index"
                        );
                        exit(json_encode($reuslt));
                    }
                }

            }
            $Articlecate = M('article_cate');
            $articles = $Articlecate->select();
            $cates = $this->catelist($articles);
            $this->assign('cates', $cates);
            $this->display();


        }

    }
    public function delete(){
        $id=I('post.id');
        if(false == M('article')->where(array('id'=>$id))->delete()) {
            $reuslt = array(
                'code' => '2',
                'msg' => '删除该文章失败！',
                'url' => "/adms/webmsg/index"
            );
            exit(json_encode($reuslt));
        } else {
            $reuslt = array(
                'code' => '1',
                'msg' => '删除该文章成功！',
                'url' => "/adms/webmsg/index"
            );
            exit(json_encode($reuslt));
        }
    }
    public function catelist($cate,$id=0,$level=0){
        static $cates = array();
        foreach ($cate as $value) {
            if ($value['pid']==$id) {
                $value['level'] = $level+1;
                if($level == 0)
                {
                    $value['str'] = str_repeat('<i class="fa fa-angle-double-right"></i> ',$value['level']);
                }
                elseif($level == 2)
                {
                    $value['str'] = '&emsp;&emsp;&emsp;&emsp;'.'└ ';
                }
                else
                {
                    $value['str'] = '&emsp;&emsp;'.'└ ';
                }
                $cates[] = $value;
                $this->catelist($cate,$value['id'],$value['level']);
            }
        }
        return $cates;
    }

}

?>
