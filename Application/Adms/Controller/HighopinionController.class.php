<?php
class HighopinionController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		
	}
	/*
	 * 送会员审核 /Adm/deleteImg
	*/
	public function lists(){
		$this->name = '送会员审核列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		$HighopinionM = new HighopinionModel();
		$Wdata = array();
		  if (isset($_GET['type']) && $_GET['type'] != "") {
              $Wdata['type'] = $_GET['type'];
          }
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['status']=0;
        $ret = $HighopinionM->getListPage($Wdata, $Page, $PageSize);
        $res=array();
        foreach($ret["list"] as $k=>$v){
            if($v['type']==1||$v['type']==7){
                $ids = explode("|", $v['url']);
                foreach($ids as $k1=>$v1){
                    $url=M('photo_highopinion')->where(array('id'=>$v1))->getField('url');
                    $ret["list"][$k]['urls'][$k1]=C('IMAGEURL').$url;
                }
            }
            $ret["list"][$k]['isvideo']=$this->get_user($v['uid'])['isvideo'];
            $ret["list"][$k]['isaudio']=$this->get_user($v['uid'])['isaudio'];
        }
		$this->DataList = $ret["list"];
        $this->type=$_GET['type'];
        $this->msgurl=C ("IMAGEURL");
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = __ACTION__.".html";
		$this->display();
	}
    /*
     *送会员二次审核/Adm/deleteImg
    */
    public function seclists(){
        $this->name = '照片二次审核'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $photoM = new PhotoModel();
        $Wdata = array();
        if($_GET['type']&&$_GET['type']!=""){
            switch($_GET['type']) {
                case 1:
                case 2:
                    $Wdata['type']=2;
                    break;
                case 3:
                case 4:
                    $Wdata['type']=1;
                    break;
                case "":
                    $Wdata=array();
                    break;
            }
        }
        if($_GET['type1']&&$_GET['type1']!=""){
            if($_GET['type1']==1){
                $Wdata['status_second']=1;
            }
            if($_GET['type1']==2){
                $Wdata['status_second']=2;
            }
        }
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }

        $Wdata['status']=1;
        $ret = $photoM->getListPage($Wdata, $Page, $PageSize);
        $sexboy=array();
        $sexgirl=array();
        foreach($ret["list"] as $k1=>$v1){
            $sex=$this->get_diy_user_field($v1['uid'],'gender');
            if($sex['gender']==1){
                $sexboy[]=$ret["list"][$k1];
            }else{
                $sexgirl[]=$ret["list"][$k1];
            }
        }
        if($_GET['type']==2||$_GET['type']==4){
            $ret["list"]=$sexgirl;
        }elseif($_GET['type']==1||$_GET['type']==3){
            $ret["list"]=$sexboy;
        }
        $this->DataList = $ret["list"];
        $this->type=$_GET['type'];
        $this->type1=$_GET['type1'];
        $this->msgurl=C ("IMAGEURL");
        //Dump($this->DataList);exit;
        //$this->status = $status;
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }
    /*
     * 好评审核 /Adms/judge
    */
	public function judge()
{
    $Wdata = array();

    if ($_POST["status"] && $_POST["id"]) {
        $uid = $_POST["uid"];
        $status = $_POST["status"];
        $type=$_POST['type'];
        $id = $_POST["id"];
        $userextendM=new UserExtendModel();
        $userbaseM=new UserBaseModel();
        $highopinionM = new HighopinionModel();
        $userinfo=$this->get_user($uid);
         switch ($type) {
             case "1":
                 $dataoppinion="ishighopinion";
                 break;
             case "2":
                 $dataoppinion="ishighopinionvideo";
                 break;
             case "3":
                 $dataoppinion="ishighopinionaudio";
                 break;
             case "4":
                 $dataoppinion="ishighopiniondata";
                 break;
             case "5":
                 $dataoppinion="ishighopinionphoto";
                 break;
             case "6":
                 $dataoppinion="ishighopiniondynamic";
                 break;
             case "7":
                 $dataoppinion="ishighopinionshare";
                 break;
             default:
                 $dataoppinion="ishighopinion";
         }
        if($status==3){
            $url1=$highopinionM->getOne(array('id'=>$id));
            $ids=explode('|',$url1['url']);
            if(!empty($ids)){
                foreach($ids as $k=>$v){
                    $photo_highopinion=M('photo_highopinion');
                    $url=$photo_highopinion->where(array('id'=>$v))->getField('url');
                    DelOss($url);
                    $photo_highopinion->delete();
                }
            }
            $res=$highopinionM->delOne(array('id'=>$id));
            $this->addlog($id,$uid,$status);
            $userextendM->updateOne(array('uid'=>$uid),array($dataoppinion=>3));
            $this->set_user_field($uid,$dataoppinion,3);

        }else{
            $res=$highopinionM->updateOne(array('id'=>$id),array('status'=>$status));
            $userextendM->updateOne(array('uid'=>$uid),array($dataoppinion=>1));
            $this->set_user_field($uid,$dataoppinion,1);
            //增加会员天数
            $viptime =24*60*60*30;
            $rtime=$userinfo["viptime"];
            $vip=$userinfo['vip'];
            if($rtime==0||$rtime<time()){
                $viptime = $viptime+time();
            }else{
                $viptime = $viptime+$rtime;
            }
            $userbaseM->updateOne(array('uid'=>$uid),array('viptime'=>$viptime,'vipgrade'=>1));
            $this->set_user_field($uid,'viptime',$viptime);
            $this->set_user_field($uid,'vip',$vip+30);
            $this->set_user_field($uid,'vipgrade',1);
            $this->addlog($id,$uid,$status);
            //判断与语言是否是中文简体
            if($userinfo['language']!='zh-cn'){
                //将该用户添加到优质用户列表中
                $betteruserlistkey='superusers';
                $redis = $this->redisconn();
                $redis->listRemove($betteruserlistkey, $uid, 0);
                $redis->listPush($betteruserlistkey, $uid);
                $superusers = M('superusers');
                $superusers->data(array('uid'=>$uid))->add();
            }
        }
        if($res){
            $reuslt = array(
                'status' =>'1',
                'message' => "更新成功",
                'data' => '',
            );
            if($status==1){
                $this->sendsysmsg($uid,'您申請的送會員以成功開通，請查收');
            }else{
                //发送通知
                $this->sendsysmsg($uid, "您申請的送會員未能通過審核，請檢查是否符合標準");
            }
            exit(json_encode ($reuslt));
        }else{
            $reuslt = array(
                'status' =>'1',
                'message' => "已更新过了",
                'data' => '',
            );
            exit(json_encode ($reuslt));
        }
    }
}
	public function Ajax_update(){
		$return['code'] = ERRORCODE_201;
		$return['message'] = "参数错误";
		$type = $_POST["type"];
		switch ($type){
			case "shenhe":
				if($_POST["id"]){
					$photoM = new PhotoModel();
					$ret = $photoM->updateOne(array("id"=>$_POST["id"]),array("status"=>1));
					$return = array();
				}
				break;
			case "shanchu":
				if($_POST["id"]){
					$photoM = new PhotoModel();
					$ret = $photoM->updateOne(array("id"=>$_POST["id"]),array("status"=>3));
					$return = array();
				}
				break;
		}
		
		
		Push_data($return);
	}


	public function goodgirlusers(){

        $this->name = '优质女用户列表'; // 进行模板变量赋值
        $redis = $this->redisconn();
        //缓存中储存总数据
        $lang=$redis->listSize('superusers');
        ///////////////
        //如果缓存中不存在，则取数据库最新数据存入
        if ($lang == 0) {
            $superusers = M('superusers');
            $Dres = $superusers->select();
            $res = array();
            foreach ($Dres as $k => $v) {
                $res[$k] =$v['uid'];
                $value = json_encode($res[$k], true);
                $ret[]=$redis->listPush('superusers',$value);
            }
            $result1 = $res;
        } else {
            $result=$redis->listLrange('superusers',0,-1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    $result1[$k1] = json_decode($v1, true);
                }
            }
        }

        ///
      /*  $betteruserlistkeyone='betteruser-20114';
        $betteruserlistkeytwo='betteruser-23110';
        $betteruserlistkeythree='betteruser-24111';
        $betteruserlistkeyfour='betteruser-23138';
        $betteruserlistkeyfive='betteruser-20111';
        $result1=$redis->listGet($betteruserlistkeyone, 0, -1);
        $result2=$redis->listGet($betteruserlistkeytwo, 0, -1);
        $result3=$redis->listGet($betteruserlistkeythree, 0, -1);
        $result4=$redis->listGet($betteruserlistkeyfour, 0, -1);
        $result5=$redis->listGet($betteruserlistkeyfive, 0, -1);
      $result=array_merge($result1,$result2,$result3,$result4,$result5);*/
        $result=array_unique($result1);
        $this->DataList =$result;
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();

    }

    public function delgoodgirl(){
        $redis = $this->redisconn();
	    $uid=$_POST['uid'];
        $betteruserlistkey='superusers';
        $res=$redis->listRemove($betteruserlistkey,$uid);
        if($res){
            //从数据库中删除
            $superusers = M('superusers');
            $superusers->where(array('uid'=>$uid))->delete();
            $reuslt = array(
                'status' => '1',
                'message' => '',
                'data' => '',
            );
        }else{
                $reuslt = array(
                    'status' => '2',
                    'message' => '',
                    'data' => '',
                );

        }
        exit(json_encode ($reuslt));
    }
    public function  add(){
        $redis = $this->redisconn();
        $uid=$_POST['uid'];
        if($uid){
            $betteruserlistkey='superusers';
            $res=$redis->listRemove($betteruserlistkey,$uid);
            $res=$redis->listPush($betteruserlistkey,$uid);

            $superusers = M('superusers');
            $superusers->data(array('uid'=>$uid))->add();
            $this->get = $_GET;
            $this->tip='已添加';
            $this->action = __ACTION__.".html";
        }

        $this->display();
    }
	
}

?>
