<?php
class AdController extends BaseAdmsController
{

    public function index()
    {
        $this->name = 'tab栏列表';
        $adcate = M('ad_cate');
        $ads = $adcate->select();
        $cates = $this->catelist($ads);
        $this->assign('cates', $cates);
        $this->display();
    }
    public function upload()
    {
        $saveurl = '/userdata/adimg/';
        $savePath = WR.$saveurl;
        $uploadList = $this->_upload($savePath);
        $fileurl = $saveurl.$uploadList[0]['savename'];
        
        if($fileurl){
            PutOss($fileurl);
            $reuslt = array(
                'code' => '1',
                'msg' => '成功',
                "fileurl"=>$fileurl
            );
        }else{
            $reuslt = array(
                'code' => '0',
                'msg' => '上传失败',
            );
        }
        exit(json_encode($reuslt));
    }
    
    protected function _upload($savePath) {
        import("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir($savePath);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 3292200;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg();
            Push_data($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            return $uploadList;;
        }
        
    }
    public function msgedit(){
        if ($_GET['id'] || I('post.id')) {
            $id = $_GET['id'];
            //post收到id为修改文章
            if (I('post.')) {
                $rules = array(
                    array('name', 'require', '分类名称必须填！'), //默认情况下用正则进行验证
                    array('pid', 'require', '请选择上级分类！'),
                );
                $adcate = M('ad_cate'); // 实例化ad对象
                //自动验证
                if (!$adcate->validate($rules)->create()) {
                    // 如果创建失败 表示验证没有通过 输出错误提示信息
                    $reuslt = array(
                        'code' => '2',
                        'msg' => $adcate->getError(),
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } else {
                    // 验证通过 可以进行其他数据操作
                    $data = array();
                    //设置修改人
                    $data['name'] = I('post.name');
                    $data['code'] = I('post.code');
                    $data['showinstart'] = I('post.showinstart');
                    
                    $id = I('post.id');
                    $data['pid'] = I('post.pid') ? I('post.pid') :0;
                    $res = $adcate->where(array('id' => $id))->save($data);
                    if ($res) {
                        $reuslt = array(
                            'code' => '1',
                            'msg' => '修改分类成功',
                            'url' => "index"
                        );
                        exit(json_encode($reuslt));
                    }
                }
            } else {//非提交操作
                $adcate = M('ad_cate')->where(array('id'=>$id))->find();
                $adall=M('ad_cate')->select();
                $cates_all = $this->catelist($adall);
               $this->assign('cates',$cates_all);
                if (!empty($adcate)) {
                    $this->assign('cate',$adcate);
                    $this->display();
                } else {
                    $reuslt = array(
                        'code' => '2',
                        'msg' =>"所传id不正确！",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                }

            }
        } else {
            if (I('post.')) {
                //设置验证规则
                $rules = array(
                    array('name', 'require', '分类名称必须填！'), //默认情况下用正则进行验证
                    array('pid', 'require', '请选择上级分类！'),
                );
                $adcate = M('ad_cate'); // 实例化ad对象
                //自动验证
                if (!$adcate->validate($rules)->create()) {
                    // 如果创建失败 表示验证没有通过 输出错误提示信息
                        $reuslt = array(
                        'code' => '2',
                        'msg' => $adcate->getError(),
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } else {
                    // 验证通过 可以进行其他数据操作
                    $data = array();

                    $data['name'] = I('post.name');
                    $data['code'] = I('post.code');
                    $data['pid'] = I('post.pid');
                    $data['showinstart'] = I('post.showinstart');
                    $res = $adcate->add($data);
                    if ($res) {
                        $reuslt = array(
                            'code' => '1',
                            'msg' => '新增分类成功！',
                            'url' => "index"
                        );
                        exit(json_encode($reuslt));
                    }
                }

            }else{
                if(I('get.pid')){
                    $pid=I('get.pid');
                    $this->assign('pid',$pid);
                }
                $adcate = M('ad_cate');
                $ads = $adcate->select();
                $cates = $this->catelist($ads);
                $this->assign('cates', $cates);
                $this->display();
            }



        }

    }
    public function delete(){
        $id=I('post.id');
        if(false == M('ad_cate')->where(array('id'=>$id))->delete()) {
            $reuslt = array(
                'code' => '2',
                'msg' => '删除该文章失败！',
                'url' => "index"
            );
            exit(json_encode($reuslt));
        } else {
            $reuslt = array(
                'code' => '1',
                'msg' => '删除该分类成功！',
                'url' => "index"
            );
            exit(json_encode($reuslt));
        }
    }
    public function catelist($cate,$id=0,$level=0){
        static $cates = array();
        foreach ($cate as $value) {
            if ($value['pid']==$id) {
                $value['level'] = $level+1;
                if($level == 0)
                {
                    $value['str'] = str_repeat('<i class="fa fa-angle-double-right"></i> ',$value['level']);
                }
                elseif($level == 2)
                {
                    $value['str'] = '&emsp;&emsp;&emsp;&emsp;'.'└ ';
                }
                else
                {
                    $value['str'] = '&emsp;&emsp;'.'└ ';
                }
                $cates[] = $value;
                $this->catelist($cate,$value['id'],$value['level']);
            }
        }
        return $cates;
    }
    
    public function adlist()
    {
        $this->name = '文章列表';
        $ad = M('ad');
        $ads = $ad->select();
        foreach ($ads as $key => $value) {
            $ads[$key]['imgsrc']=C("IMAGEURL").$ads[$key]['imgsrc'];
            $ads[$key]['catename']=M('ad_cate')->where(array('id'=>$value['cate_id']))->getField('name');
        }
        $this->assign('articles', $ads);
        $this->display();
    }
    
    public function adedit(){
        $productsM  = new ProductsModel();
        $this->products=$productsM->getAll();
        //Dump($_FILES);
        //Dump(I('post.'));exit;
        if ($_GET['id'] || I('post.id')) {
            $id = $_GET['id'];
            //post收到id为修改文章
            if (I('post.')) {
                $rules = array(
                    array('title', 'require', '标题必须填！'), //默认情况下用正则进行验证
                    array('content', 'require', '内容不能为空！'),
                );
                $ad = M('ad'); // 实例化ad对象
                //自动验证
                if (!$ad->validate($rules)->create()) {
                    // 如果创建失败 表示验证没有通过 输出错误提示信息
                    $reuslt = array(
                        'code' => '2',
                        'msg' => $ad->getError(),
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } else {
                    // 验证通过 可以进行其他数据操作
                    $data = array();
                    //设置修改人
                    $adminuser=session("AdminUser");
                    $data['title'] = I('post.title');
                    $id = I('post.id');
                    $data['cate_id'] = I('post.cate_id');
                    $data['url'] = I('post.url');
                    $data['imgsrc'] =I('post.imgsrc');
                    $data['status'] =I('post.status');
                    $data['product'] =I('post.product');
                    $data['cate_id'] = I('post.cate_id') ? I('post.cate_id') : 1;
                    $res = $ad->where(array('id' => $id))->save($data);
                    if ($res) {
                        $this->delrediscache(I('post.product'));
                        $reuslt = array(
                            'code' => '1',
                            'msg' => '修改成功',
                            'url' => "adlist"
                        );
                        exit(json_encode($reuslt));
                    }
                }
            } else {
                $adcate = M('ad_cate')->where(array('id'=>$id))->find();
                $adall=M('ad_cate')->select();
                $cates_all = $this->catelist($adall);
                $this->assign('cates',$cates_all);
                if (!empty($adcate)) {
                    
                    $this->assign('cate', $adcate);
                }
                $ad = M('ad')->where(array('id'=>$id))->find();
                if (!empty($ad)) {
                    $ad["imgsrcshow"]=C("IMAGEURL").$ad['imgsrc'];
                    $this->assign('article', $ad);
                    $this->display();
                } else {
                    $reuslt = array(
                        'code' => '2',
                        'msg' =>"所传id不正确！",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                }
                
            }
        } else {
            if (I('post.')) {
                //设置验证规则
                $rules = array(
                    array('title', 'require', '标题必须填！'), //默认情况下用正则进行验证
                    array('content', 'require', '内容不能为空！'),
                );
                $ad = M('ad'); // 实例化ad对象
                //自动验证
                if (!$ad->validate($rules)->create()) {
                    // 如果创建失败 表示验证没有通过 输出错误提示信息
                    $reuslt = array(
                        'code' => '2',
                        'msg' => $ad->getError(),
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } else {
                    // 验证通过 可以进行其他数据操作
                    $data = array();
                    //设置创建人
                    $data['title'] = I('post.title');
                    $data['cate_id'] = I('post.cate_id');
                    $data['url'] = I('post.url');
                    $data['imgsrc'] =I('post.imgsrc');
                    $data['status'] =I('post.status');
                    $data['product'] =I('post.product');
                    $data['cate_id'] = I('post.cate_id') ? I('post.cate_id') : 1;
                    $data['create_time'] = time();
                    $res = $ad->add($data);
                    if ($res) {
                        $this->delrediscache(I('post.product'));
                        $reuslt = array(
                            'code' => '1',
                            'msg' => '新增成功',
                            'url' => "adlist"
                        );
                        exit(json_encode($reuslt));
                    }
                }
                
            }
            $adcate = M('ad_cate');
            $ads = $adcate->select();
            $cates = $this->catelist($ads);
            $this->assign('cates', $cates);
            $this->display();
            
            
        }
        
    }
    private function delrediscache($product){
        $redis=$this->redisconn();
        $redisStr = "ad_".$product;
        $redisStr1 = "userlistad_".$product;
        $redis->del($redisStr);
        $redis->del($redisStr1);
        return true;
    }
    public function addelete(){
        $id=I('post.id');
        $ad = M('ad')->where(array('id'=>$id))->find();
        
        if(false == M('ad')->where(array('id'=>$id))->delete()) {
            $reuslt = array(
                'code' => '2',
                'msg' => '删除失败！',
                'url' => "adlist"
            );
            exit(json_encode($reuslt));
        } else {
            $this->delrediscache($ad["product"]);
            DelOss($ad["imgsrc"]);
            $reuslt = array(
                'code' => '1',
                'msg' => '删除成功！',
                'url' => "adlist"
            );
            exit(json_encode($reuslt));
        }
    }

}

?>
