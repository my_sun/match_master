<?php
class WebtabController extends BaseAdmsController
{

    public function index()
    {
        $this->name = 'tab栏列表';
        $Articlecate = M('article_cate');
        $articles = $Articlecate->select();
        $cates = $this->catelist($articles);
        $this->assign('cates', $cates);
        $this->display();
    }

    public function msgedit(){
        if ($_GET['id'] || I('post.id')) {
            $id = $_GET['id'];
            //post收到id为修改文章
            if (I('post.')) {
                $rules = array(
                    array('name', 'require', '分类名称必须填！'), //默认情况下用正则进行验证
                    array('pid', 'require', '请选择上级分类！'),
                );
                $Articlecate = M('article_cate'); // 实例化article对象
                //自动验证
                if (!$Articlecate->validate($rules)->create()) {
                    // 如果创建失败 表示验证没有通过 输出错误提示信息
                    $reuslt = array(
                        'code' => '2',
                        'msg' => $Articlecate->getError(),
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } else {
                    // 验证通过 可以进行其他数据操作
                    $data = array();
                    //设置修改人
                    $data['name'] = I('post.name');
                    $data['update_time'] = time();
                    $data['tag'] = I('post.tag');
                    $id = I('post.id');
                    $data['description'] = I('post.description');
                    $data['pid'] = I('post.pid') ? I('post.pid') :0;
                    $res = $Articlecate->where(array('id' => $id))->save($data);
                    if ($res) {
                        $reuslt = array(
                            'code' => '1',
                            'msg' => '修改分类成功',
                            'url' => "/adms/webtab/index"
                        );
                        exit(json_encode($reuslt));
                    }
                }
            } else {//非提交操作
                $articlecate = M('article_cate')->where(array('id'=>$id))->find();
                $articleall=M('article_cate')->select();
                $cates_all = $this->catelist($articleall);
               $this->assign('cates',$cates_all);
                if (!empty($articlecate)) {
                    $this->assign('cate',$articlecate);
                    $this->display();
                } else {
                    $reuslt = array(
                        'code' => '2',
                        'msg' =>"所传id不正确！",
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                }

            }
        } else {
            if (I('post.')) {
                //设置验证规则
                $rules = array(
                    array('name', 'require', '分类名称必须填！'), //默认情况下用正则进行验证
                    array('pid', 'require', '请选择上级分类！'),
                );
                $articlecate = M('article_cate'); // 实例化article对象
                //自动验证
                if (!$articlecate->validate($rules)->create()) {
                    // 如果创建失败 表示验证没有通过 输出错误提示信息
                        $reuslt = array(
                        'code' => '2',
                        'msg' => $articlecate->getError(),
                        'data' => '',
                    );
                    exit(json_encode($reuslt));
                } else {
                    // 验证通过 可以进行其他数据操作
                    $data = array();
                    $adminuser=session("AdminUser");
                    $data['admin_id'] = $adminuser['id'];

                    $data['name'] = I('post.name');
                    $data['create_time'] = time();
                    $data['update_time'] = $data['create_time'];
                    $data['tag'] = I('post.tag');
                    $data['description'] = I('post.description');
                    $data['pid'] = I('post.pid');
                    $res = $articlecate->add($data);
                    if ($res) {
                        $reuslt = array(
                            'code' => '1',
                            'msg' => '新增分类成功！',
                            'url' => "/adms/webtab/index"
                        );
                        exit(json_encode($reuslt));
                    }
                }

            }else{
                if(I('get.pid')){
                    $pid=I('get.pid');
                    $this->assign('pid',$pid);
                }
                $Articlecate = M('article_cate');
                $articles = $Articlecate->select();
                $cates = $this->catelist($articles);
                $this->assign('cates', $cates);
                $this->display();
            }



        }

    }
    public function delete(){
        $id=I('post.id');
        if(false == M('article_cate')->where(array('id'=>$id))->delete()) {
            $reuslt = array(
                'code' => '2',
                'msg' => '删除该文章失败！',
                'url' => "/adms/webtab/index"
            );
            exit(json_encode($reuslt));
        } else {
            $reuslt = array(
                'code' => '1',
                'msg' => '删除该分类成功！',
                'url' => "/adms/webtab/index"
            );
            exit(json_encode($reuslt));
        }
    }
    public function catelist($cate,$id=0,$level=0){
        static $cates = array();
        foreach ($cate as $value) {
            if ($value['pid']==$id) {
                $value['level'] = $level+1;
                if($level == 0)
                {
                    $value['str'] = str_repeat('<i class="fa fa-angle-double-right"></i> ',$value['level']);
                }
                elseif($level == 2)
                {
                    $value['str'] = '&emsp;&emsp;&emsp;&emsp;'.'└ ';
                }
                else
                {
                    $value['str'] = '&emsp;&emsp;'.'└ ';
                }
                $cates[] = $value;
                $this->catelist($cate,$value['id'],$value['level']);
            }
        }
        return $cates;
    }
}

?>
