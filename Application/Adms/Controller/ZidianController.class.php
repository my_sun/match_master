<?php
class ZidianController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->basename = '字典-'; // 进行模板变量赋值
	}
	
	public function Lists(){
		$this->name = $this->basename.'列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 200;
		$Attr = new AttrModel();
		$status = C("status");
		$this->attrtype = $status["zidian"]["type"];
		//$AttrValue = new AttrValueModel();
		$Wdata = array();
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword = $_GET['kerword'];
			if($kerword){
				$Wdata['code|name']=array('like',"%".$kerword."%");
			}
		}
		
		$ret = $Attr->getListPage($Wdata,$Page,$PageSize);
		
		$this->ListData = $ret["list"];
		$this->Pages = $this->GetPages($ret);
		
		//Dump($this->ListData);exit;
		$this->get = $_GET;
		$this->action = __ACTION__.".html";
		$this->display();
	}
	public function Lists_value(){
	    $this->name = $this->basename.'列表'; // 进行模板变量赋值
	    $Page = $_GET["Page"] ? $_GET["Page"] : 1;
	    $code = $_GET["code"] ? $_GET["code"] : 0;
	    $PageSize = 2000;
	    $AttrValue = new AttrValueModel();
	    $Wdata = array();
	    $Wdata["uppercode"] = $code;
	    
	    $ret = $AttrValue->getListPage($Wdata,$Page,$PageSize);
	    
	    $this->ListData = $ret["list"];
	    $this->Pages = $this->GetPages($ret);
	    
	    //Dump($this->ListData);exit;
	    $this->get = $_GET;
	    $this->action = __ACTION__.".html";
	    $this->display();
	}

	public function dele(){
		$id = $_GET["code"];
		$Attr = new AttrModel();
		$Attr->delOne(array('code'=>$id));


		echo "<script>alert('操作成功');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";

	}


	public function update(){
		$this->name = $this->basename.'编辑'; // 进行模板变量赋值
		if($_POST){
			if($_POST["name"]&&$_POST["code"]){
				$Attr = new AttrModel();
				$map = array();

					$map["name"] = $_POST["name"];
					$Attr->updateOne(array("code"=>$_POST["code"]),$map);
					$this->tip = $_POST["name"]."已修改";
					//添加翻译
					$this->translate_add($map["code"], $map["name"]);
					//删除字典redis缓存
					$redis=$this->redisconn();
					$redis->del('attrconf');


			}else{
				$this->tip = "名称和代码不能为空！";
			}
		}

		$Attr = new AttrModel();
		$map["code"] = $_GET["code"];

		$AttrValue = $Attr->getOne($map);
		$this->attrval = $AttrValue;
		$this->action =  __ACTION__.".html";
		$this->display();
	}


	public function updateattr(){
		$AttrValueM = new AttrValueModel();
		if($_POST){
			if($_POST["name"]&&$_POST["id"]){
				$map["name"] = $_POST["name"];
				$AttrValueM->updateOne(array("id"=>$_POST['id']),$map);
				//添加翻译
				//$this->translate_add($code, $map["name"]);

				$this->tip = $_POST["name"]."已修改";
				//删除字典redis缓存
				$redis=$this->redisconn();
				$redis->del('attrconf');
			}
		}


		$map = array();
		$map["id"] = $_GET['id'];
		$this->AttrInfo = $AttrValueM->getOne($map);
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	public function Add(){
		$this->name = $this->basename.'添加'; // 进行模板变量赋值
		if($_POST){
		    if($_POST["name"]&&$_POST["code"]){
		        $Attr = new AttrModel();
		        $map = array();
		        $map["code"] = $_POST["code"];
		        
		        $AttrValue = $Attr->getOne($map);
		        if($AttrValue){
		            $this->tip = "代码(code)不能重复！";
		        }else{
		            $map["name"] = $_POST["name"];
		            $map["type"] = $_POST["type"];
		            $Attr->addOne($map);
		            $this->tip = $_POST["name"]."已添加";
		            //添加翻译
		            $this->translate_add($map["code"], $map["name"]);
		           //删除字典redis缓存
                    $redis=$this->redisconn();
                    $redis->del('attrconf');
		        }
		        
		    }else{
		        $this->tip = "名称和代码不能为空！";
		    }
		}
		$this->action =  __ACTION__.".html";
		$this->display();
	}	
	
	/*
	 * 添加属性
	 */
	public function AddAttr(){
	    $this->name = $this->basename.'添加属性'; // 进行模板变量赋值
	    $uppercode =$_GET["code"]!="" ? $_GET["code"] : $_REQUEST["uppercode"];
	    if($uppercode){
	        $AttrM = new AttrModel();
	        $AttrValueM = new AttrValueModel();
	        $map = array();
	        $map["code"] = $uppercode;
	        $this->AttrInfo = $AttrM->getOne($map);
	        //echo $uppercode;exit;
	        
    	    if($_POST){
    	        if($_POST["name"]&&$_POST["code"]){
    	            $Attr = new AttrModel();
    	            $map = array();
    	            $code = $_POST["code"];
    	            $map["code"] = $code;
    	            
    	            $AttrValue = $AttrValueM->getOne($map);
    	            if($AttrValue){
    	                $this->tip = "代码(code)不能重复！";
    	            }else{
    	                $map["name"] = $_POST["name"];
    	                $map["uppercode"] = $_POST["uppercode"];
    	                $AttrValueM->addOne($map);
    	                //添加翻译
    	                $this->translate_add($code, $map["name"]);
    	                
    	                $this->tip = $_POST["name"]."已添加，可继续添加";
                        //删除字典redis缓存
                        $redis=$this->redisconn();
                        $redis->del('attrconf');
    	            }
    	            
    	        }else{
    	            $this->tip = "名称和代码不能为空！";
    	        }
    	    }
	    }
	    $this->action =  __ACTION__.".html";
	    $this->display();
	}	
	
	/*
	 * 批量添加属性
	 */
	public function AddAttrMore(){
	    $this->name = $this->basename.'添加属性'; // 进行模板变量赋值
	    $uppercode =$_GET["code"]!="" ? $_GET["code"] : $_REQUEST["uppercode"];
	    
	    if($uppercode){
	        $AttrM = new AttrModel();
	        $AttrValueM = new AttrValueModel();
	        $map = array();
	        $map["code"] = $uppercode;
	        $this->AttrInfo = $AttrM->getOne($map);
	        //echo $uppercode;exit;
	        
	        if($_POST){
	            if($_POST["contents"]){
	                $contents = $_POST["contents"];
	                $contents = explode("\r\n", $contents);
	                
	                foreach($contents as $v){
	                    if($v){
    	                    $map = array();
    	                    $code = filter_mark(TranslateMicrosoft($v));
    	                    $code =preg_replace('# #','',$code);
    	                    
    	                    if($code){
    	                        $map["code"] = strtoupper($code);
        	                    $map["name"] = $v;
        	                    $map["uppercode"] = $_POST["uppercode"];
        	                    $AttrValueM->addOne($map);
        	                    $this->translate_add(strtoupper($code), $map["name"]);//添加翻译
    	                    }
    	                    
	                    }
	                }
	                $this->tip = $_POST["contents"]."已添加，可继续添加";
                    //删除字典redis缓存
                    $redis=$this->redisconn();
                    $redis->del('attrconf');
	            }else{
	                $this->tip = "名称和代码不能为空！";
	            }
	        }
	    }
	    $this->action =  __ACTION__.".html";
	    $this->display();
	}	
	/*
	 * 删除属性
	 */
	public function Del_value(){
	    $this->name = $this->basename.'添加属性'; // 进行模板变量赋值
	    $code =$_REQUEST["id"];
	    if($code){
	        $AttrValueM = new AttrValueModel();
	        $map = array();
	        $map["id"] = $code;
	        $AttrValueM->delOne($map);
	        //删除翻译
	        $this->translate_del($code);
            //删除字典redis缓存
            $redis=$this->redisconn();
            $redis->del('attrconf');
	    }
	    header('Location:'.$_SERVER['HTTP_REFERER']);
	}
	public function Edit(){
		$this->name = $this->basename.'修改'; // 进行模板变量赋值
	}	
	public function Del(){
		$this->name = $this->basename.'删除'; // 进行模板变量赋值
	}	
	public function Ajax(){
		$type = $_POST["type"];
		switch ($type){
			case "updata_status":
				$id = (int)$_POST["id"];
				$status = $_POST["status"];
				if(is_integer($id)){
					$CountryM = new CountryModel();
					$CountryM->updateOne(array("id"=>$id), array("status"=>$status));
					$result["msg"] = "ok";
					$result["Succeed"] = 1;
					echo json_encode($result);exit;
				}else{
					$result["msg"] = "ID错误".$id;
					$result["Succeed"] = 0;
					echo json_encode($result);exit;
				}
				break; 
		}
		
	}
}

?>
