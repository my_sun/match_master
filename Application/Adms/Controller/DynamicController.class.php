<?php
class DynamicController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR.'/userdata/cache/dynamic/';
	}
	/*
	 * 动态列表一次审核显示
	*/
	public function lists(){
		$this->name = '动态列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		$DynamicM = new DynamicModel();
		$Wdata = array();
		$type=$_GET['type']?$_GET['type']:'';
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['status']=2;
        $ret = $DynamicM->getListPage($Wdata, $Page, $PageSize,"time");
       foreach($ret['list'] as $k=>$v){
           $ret['list'][$k]['content']=urldecode($v['content']);
       }
        $sexboy=array();
        $sexgirl=array();
		foreach($ret["list"] as $k=>$v){
		    $ret["list"][$k]["user"]=$this->get_diy_user_field($v["uid"],"uid|head|nickname|gender");
		    if($v["type"]==3){
		        if($v["ids"]){
                    $ret["list"][$k]['meiti'] = $this->get_videodynamic($v["ids"]);

		           // $VideoDynamicM->updateOne(array("id"=>$v["ids"]),array("danamicid"=>$v["id"]));
		        }
		    }elseif($v["type"]==2){
		        $ids = explode("|", $v["ids"]);
		        foreach ($ids as $k1=>$v1){
		            if($v1)
                        $ret["list"][$k]['meiti'][$k1] = $this->get_photodynamic($v1);
		               // $PhotoDynamicM->updateOne(array("id"=>$v1),array("danamicid"=>$v["id"]));
		        }
		    }
            if($ret["list"][$k]["user"]['gender']==1){
                $sexboy[]=$ret["list"][$k];
            }else{
                $sexgirl[]=$ret["list"][$k];
            }
		   // $ret["list"][$k]['meiti']=$obj;
		}
        if($type==2){
            $ret["list"]=$sexgirl;
        }elseif($type==1){
            $ret["list"]=$sexboy;
        }
        $this->type=$type;
        $this->msgurl=C ("IMAGEURL");
		$this->DataList = $ret["list"];

        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = __ACTION__.".html";
		$this->display();
	}
    /*
     * 动态列表二次审核显示
    */
    public function seclists(){
        $this->name = '动态二次审核列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $DynamicM = new DynamicModel();
        $Wdata = array();
        if($_GET['type1']&&$_GET['type1']!=""){
            if($_GET['type1']==1){
                $Wdata['status_second']=1;
            }
            if($_GET['type1']==2){
                $Wdata['status_second']=2;
            }
        }
        $type=$_GET['type']?$_GET['type']:'';
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['status']=1;
        $ret = $DynamicM->getListPage($Wdata, $Page, $PageSize,"time");
        $sexboy=array();
        $sexgirl=array();
        foreach($ret["list"] as $k=>$v){
            $ret["list"][$k]["user"]=$this->get_diy_user_field($v["uid"],"uid|head|nickname|gender");
            if($v["type"]==3){
                if($v["ids"]){
                    $ret["list"][$k]['meiti'] = $this->get_videodynamic($v["ids"]);
                }
            }elseif($v["type"]==2){
                $ids = explode("|", $v["ids"]);
                foreach ($ids as $k1=>$v1){
                    if($v1)
                        $ret["list"][$k]['meiti'][$k1] = $this->get_photodynamic($v1);
                }
            }
            if($ret["list"][$k]["user"]['gender']==1){
                $sexboy[]=$ret["list"][$k];
            }else{
                $sexgirl[]=$ret["list"][$k];
            }
        }
        if($type==2){
            $ret["list"]=$sexgirl;
        }elseif($type==1){
            $ret["list"]=$sexboy;
        }
        $this->type=$type;
        $this->type1=$_GET['type1'];
        $this->msgurl=C ("IMAGEURL");
        $this->DataList = $ret["list"];

        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }
	//获取视频
	public function get_videodynamic($id,$reset='0'){
	    $path = WR.'/userdata/cache/dynamic/video/';
	    $cache_name = 'videoinfo_'.$id;
        if($reset==1){
            return F($cache_name,NULL,$path);
        }
	    if(F($cache_name,'',$path) && $reset == 0){
	        $Info = F($cache_name,'',$path);
	    }else{
	        $VideoDynamicM = new VideoDynamicModel();
	        $videoTemp = $VideoDynamicM->getOne(array("id"=>$id));
	        $Info=array(
	            "id"=>$videoTemp["id"],
	            "ltime"=>$videoTemp["ltime"],
	            "url"=>C("IMAGEURL").$videoTemp["url"],
	            "imageurl"=>C("IMAGEURL").$videoTemp["imageurl"],
	            "status"=>$videoTemp["status"]
	        );
	        F($cache_name,$Info,$path);
	   }
	    return $Info;
	}
	//获取动态图片
	public function get_photodynamic($id,$reset='0'){
	    $path = WR.'/userdata/cache/dynamic/photo/';
	    $cache_name = 'photoinfo_'.$id;
        if($reset==1){
            return F($cache_name,NULL,$path);
        }
	    if(F($cache_name,'',$path) && $reset == 0){
	       $Info = F($cache_name,'',$path);
	    }else{
	        $PhotoDynamicM = new PhotoDynamicModel();
	        $Temp = $PhotoDynamicM->getOne(array("id"=>$id));
	        $Info=array(
	            "id"=>$Temp["id"],
	            "url"=>C("IMAGEURL").$Temp["url"],
	            "thumbnaillarge"=>C("IMAGEURL").$Temp["url"],
	            "thumbnailsmall"=>C("IMAGEURL").$Temp["url"],
	            "status"=>$Temp["status"],
	            "seetype"=>$Temp["seetype"]
	        );
	        F($cache_name,$Info,$path);
	    }
	    return $Info;
	}
    /*
     * 动态二次审核 /Adms/secjudge
    */
	public function secjudge(){
        $Wdata = array();
        if ($_POST["status"]&&$_POST["id"]) {
            $status = $_POST["status"];
            $id=$_POST["id"];
            $uid=$_POST["uid"];
            $gender=$this->get_user($uid)['gender'];
            $dynamicM= new DynamicModel();
            $PhotoDynamicM = new PhotoDynamicModel();
            $VideoDynamicM = new VideoDynamicModel();
            if($status==3){
            	$res1=$dynamicM->getOne(array('id'=>$id));
				$type=$res1['type'];
				if($type==2){
                   $photod= $PhotoDynamicM->where(array('danamicid'=>$id))->select();
                   foreach($photod as $k=>$v){
                       DelOss($v['url']);
                       $this->get_photodynamic($v['id'],1);
				   }
                    $PhotoDynamicM->where(array('danamicid'=>$id))->delete();
				}
				if($type==3){
					$videod=$VideoDynamicM->getOne(array('danamicid'=>$id));
                    DelOss($videod['url']);
                    $this->get_videodynamic($videod['id'],1);
                    $res1=$VideoDynamicM->where(array('danamicid'=>$id))->delete();
				}
				//删除评论
                $DynamicCommentM = new DynamicCommentModel();
                $DynamicCommentM->delOne(array("danamicid"=>$id));
                //删除已经通过的动态，也需要将存在redis中的缓存删除，然后重新读取数据库
                $redis=$this->redisconn();
                $data=$dynamicM->getOne(array('id'=>$id));
                $value=json_encode($data,true);
                if($gender==1){
                    $redis->listRemove('dynamic_boy',$id.'_'.$value);
                }else{
                    $redis->listRemove('dynamic_girl',$id.'_'.$value);
                }

                // $redis->del('dynamic');
                //删除动态
                $res=$dynamicM->delOne(array('id'=>$id));
                $this->addlog($id,$uid,$status);
			}else{
                $res=$dynamicM->updateOne(array('id'=>$id),array('status_second'=>$status));
                $this->addlog($id,$uid,$status);
			}
            if($res){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                if($status==3){
                    $this->sendsysmsg($uid, "該動態未通過審核");
				}
                deldir($this->cachepath);//清空动态缓存
                exit(json_encode ($reuslt));
            }else{
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已更新过了",
                    'data' => '',
                );
                deldir($this->cachepath);//清空动态缓存
                exit(json_encode ($reuslt));
            }
        }
	}
    /*
         * 动态审核 /Adms/judge
        */
    public function judge(){
        $Wdata = array();
        if ($_POST["status"]&&$_POST["id"]) {
            $status = $_POST["status"];
            $id=$_POST["id"];
            $uid=$_POST["uid"];
            $gender=$this->get_user($uid)['gender'];
            $dynamicM= new DynamicModel();
            $PhotoDynamicM = new PhotoDynamicModel();
            $VideoDynamicM = new VideoDynamicModel();
            if($status==3){
                $res1=$dynamicM->getOne(array('id'=>$id));
                $type=$res1['type'];
                if($type==2){
                    $photod= $PhotoDynamicM->where(array('danamicid'=>$id))->select();
                    foreach($photod as $k=>$v){
                        DelOss($v['url']);
                        $this->get_photodynamic($v['id'],1);
                    }
                    $PhotoDynamicM->where(array('danamicid'=>$id))->delete();
                }
                if($type==3){
                    $videod=$VideoDynamicM->getOne(array('danamicid'=>$id));
                    DelOss($videod['url']);
                    $this->get_videodynamic($videod['id'],1);
                    $res1=$VideoDynamicM->where(array('danamicid'=>$id))->delete();
                }
                //删除评论
                $DynamicCommentM = new DynamicCommentModel();
                $DynamicCommentM->delOne(array("danamicid"=>$id));
                //删除已经通过的动态，也需要将存在redis中的缓存删除，然后重新读取数据库
                $redis=$this->redisconn();
                $data=$dynamicM->getOne(array('id'=>$id));
                $value=json_encode($data,true);
                $redis->listRemove('dynamic',$id.'_'.$value);
               // $redis->del('dynamic');
                //删除动态
                $res=$dynamicM->delOne(array('id'=>$id));
                $this->addlog($id,$uid,$status);
            }else{
                $res=$dynamicM->updateOne(array('id'=>$id),array('status'=>$status));
                $this->addlog($id,$uid,$status);
                $this->get_dynamiclist(0,0,0,1);//清空动态缓存
            }
            if($res){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                if($status==1){
                    $data=$dynamicM->getOne(array('id'=>$id));
                    $value=json_encode($data,true);
                    $redis=$this->redisconn();

                    if($gender==1){
                        $lang=$redis->listSize('dynamic_boy');
                        if($lang>=1000){
                            $redis->listPop('dynamic_boy',1);
                        }
                        $ret=$redis->listPush('dynamic_boy',$id.'_'.$value);
                    }else{
                        $lang=$redis->listSize('dynamic_girl');
                        if($lang>=1000){
                            $redis->listPop('dynamic_girl',1);
                        }
                        $ret=$redis->listPush('dynamic_girl',$id.'_'.$value);
                    }
                    $this->sendsysmsg($uid, "該動態已通過審核");
                }else{
                    $this->sendsysmsg($uid, "該動態未通過審核");
                }
                deldir($this->cachepath);//清空动态缓存
                exit(json_encode ($reuslt));
            }else{
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已更新过了",
                    'data' => '',
                );
                deldir($this->cachepath);//清空动态缓存
                exit(json_encode ($reuslt));
            }
        }

    }
	/*
	 * 获取动态列表
	 */
	protected function get_dynamiclist($where,$page,$pagesize,$reset=0){
	    $path = $this->cachepath."dynamiclist/";
	    $where["status"]=2;
	    $cache_name = 'dynamiclist'.md5(json_encode($where).$page.$pagesize);
	    if($reset==1){
	        return F($cache_name,NULL,$path);
	        return deldir($path);
	    }
	    if(F($cache_name,'',$path)){
	       $res = F($cache_name,'',$path);
	    }else{
	        $DynamicM = new DynamicModel();
	        $res = $DynamicM->getListPage($where,$page,$pagesize);
	        F($cache_name,$res,$path);
	    }
	    return $res;
	}

	public function Ajax_update(){
		$return['code'] = ERRORCODE_201;
		$return['message'] = "参数错误";
		$type = $_POST["type"];
		switch ($type){
			case "shenhe":
				if($_POST["id"]){
					$photoM = new PhotoModel();
					$ret = $photoM->updateOne(array("id"=>$_POST["id"]),array("status"=>1));
					$return = array();
				}
				break;
			case "shanchu":
				if($_POST["id"]){
					$photoM = new PhotoModel();
					$ret = $photoM->updateOne(array("id"=>$_POST["id"]),array("status"=>3));
					$return = array();
				}
				break;
		}
		Push_data($return);
	}

	//从后台发表动态
    public  function add_dynamic($data)
    {
        $uid = $data['uid'];
        $userinfo = $this->get_user($uid);
        $type = $data["type"] ? $data["type"] : 1;
        $content = $data["content"] ? $data["content"] : "";
        $ltime = $data["ltime"] ? $data["ltime"] : 0;
        $filetype = "image";
        $gender = $userinfo['gender'];
        $dynamicM = new DynamicModel();
        $dynamic = array();
        if ($type == 1) {
            if ($content) {
                $addArr = array();
                $addArr["content"] = $content;
                $addArr["uid"] = $uid;
                $addArr["time"] = time();
                $addArr["type"] = $type;
                $addArr["gender"] = $gender;
                $addArr["status"] = 1;
                $dynamicid = $dynamicM->addOne($addArr);
                $return = array();
                $dynamic = array(
                    "id" => $dynamicid,
                    "content" => $content,
                    "type" => $type,
                    "time" => $addArr["time"],
                    "obj" => "",
                    "comment" => array()
                );
                $this->get_dynamiclist(0, 0, 0, 1);//清空动态缓存
            } else {
                $dynamic=array();
            }
        } else {
            if (!empty($data['url'])) {
                $addArr = array();
                $addArr["content"] = $content;
                $addArr["uid"] = $uid;
                $addArr["time"] = $data['time'];
                $addArr["type"] = $type;
                $addArr["gender"] = $gender;
                $dynamicid = $dynamicM->addOne($addArr);
                $saveUrl = $this->msgfileurl . $filetype . "/";
                $savePath = WR . $saveUrl;
                //echo $savePath;exit;
                $PhotoDynamicM = new PhotoDynamicModel();
                $Indata = array();
                $Indata['danamicid'] = $dynamicid;
                $Indata['uid'] = $uid;
                $Indata['type'] = $type;//1头像，2相册，3消息图片
                $Indata['url'] = $data['url'];

            }
            $ret = $PhotoDynamicM->addOne($Indata);
            $ids = $ret;
            $PutOssarr= $Indata['url'];
            $obj = array(
                "id" => $ret,
                "url" => C("IMAGEURL") . $Indata['url'],
                "thumbnaillarge" => C("IMAGEURL") . $Indata['url'],
                "thumbnailsmall" => C("IMAGEURL") . $Indata['url'],
                "status" => 1,
                "seetype" => 1
            );
            $dynamicM->updateOne(array("id" => $dynamicid), array("ids" => $ids,'status'=>1));
            //PutOss($PutOssarr);
            //Dump($PutOssarr);exit;
            $return = array();
            $dynamic = array(
                "id" => $dynamicid,
                "content" => $content,
                "type" => $type,
                "time" => $addArr["time"],
                "obj" => $obj,
                "comment" => array()
            );

            $this->get_dynamiclist(0, 0, 0, 1);//清空动态缓存
        }
        $return['data']["dynamic"]=$dynamic;
        //将动态放入redis中
        $data_redis=$dynamicM->getOne(array('id'=>$dynamicid));
        $value=json_encode($data_redis,true);
        $redis=$this->redisconn();
        if($gender==1){
            $lang=$redis->listSize('dynamic_boy');
            if($lang>=500){
                $redis->listPop('dynamic_boy',1);
            }
            $ret=$redis->listPush('dynamic_boy',$dynamicid.'_'.$value);
        }else{
            $lang=$redis->listSize('dynamic_girl');
            if($lang>=500){
                $redis->listPop('dynamic_girl',1);
            }
            $ret=$redis->listPush('dynamic_girl',$dynamicid.'_'.$value);
        }


        //Dump($return);
        return $return;

    }


}

?>
