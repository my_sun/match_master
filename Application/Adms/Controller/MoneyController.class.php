<?php
class MoneyController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		
	}
	
	/**
	 * 用户列表测试用
	 */
	public function Fagongzi(){
		$this->name = '提成结算'; // 进行模板变量赋值
		$cur = convertCurrency("TWD", "CNY");
		$type = $_GET["type"] ? $_GET["type"] : 1;
		$Wdata = array();
		$PeiUserM = new PeiUserModel();
		$PeiGuanlianM = new PeiGuanlianModel();
		$PeiliaoMoneyM = new PeiliaoMoneyModel();	
		$dateStart = $_GET["dateStart"] ? $_GET["dateStart"] : 0; 		
		$dateEnd = $_GET["dateEnd"] ? $_GET["dateEnd"] : 0;
		if($dateEnd){
			$Wdata["paytime"]=array(array('egt',strtotime($dateStart)),array('elt',strtotime($dateEnd)));
		}
		
		$Wdata["is_tixian"]="0";
		//$PeiliaoMoneyM->updateOne($Wdata,array("is_tixian"=>1));
		$allmoneys = $PeiliaoMoneyM->sum_money($Wdata);
		$PeiList = $PeiUserM->getListPage(array(),1,100);
		$arsort = array();
		$PeiMoneyarr = array();
		foreach($PeiList as $k=>$v){

			$Wdata["pid"]=$v["pid"];
			$moneys = $PeiliaoMoneyM->sum_money($Wdata);
			
			$moneys = $moneys ? $moneys : 0;
			if($moneys){
			$PeiMoneyarr[$v['pid']] = $v;
			$PeiMoneyarr[$v['pid']]["money"]=round($moneys*$cur);
			$PeiMoneyarr[$v['pid']]["Tai_money"] = round($moneys);

			$arsort[$v['pid']] = $moneys;
			}
		}
		arsort($arsort);
		$ListData = array();
		$i=1;
		$alljiangjin = 0;
		foreach($arsort as $k=>$v){
			$ListData[$i] = $PeiMoneyarr[$k];
			$moneys = $PeiMoneyarr[$k]["Tai_money"];	
			$Jiang_money=0;
			if($i==1){
				$Jiang_money = $moneys*0.5;
				//Dump($PeiMoneyarr[$k]);exit;
			}
			if($i==2){
				$Jiang_money = $moneys*0.3;
			}
			if($i==3){
				$Jiang_money = $moneys*0.2;
			}
			$alljiangjin = $alljiangjin+$Jiang_money;
			//if($Jiang_money){
				$ListData[$i]["Jiang_money"]=round($Jiang_money*$cur);
				$ListData[$i]["Tai_Jiang_money"]=round($Jiang_money);
			//}
			$i++;
		}
		
		  
		$allmoneys = $allmoneys + $alljiangjin;
		$this->Allmoneys = round($allmoneys*$cur);
		$this->TaiAllmoneys = round($allmoneys);
		$this->action = "/Adms/Money/Fagongzi.html";
		$this->ListData = $ListData;
		//Dump($ListData);exit;
		//echo CONTROLLER_NAME;exit;
		$this->display("Fagongzi");
		
		
	}
	public function Index(){
		$this->name = 'Login'; // 进行模板变量赋值
		
	
	}
	public function Peiuserlist(){
		$pid = $_GET['pid']?$_GET['pid']:619774356;
		$PeiGuanlianM = new PeiGuanlianModel();
		$puserlist = $PeiGuanlianM->getList(array("pid"=>$pid));
		$res = array();
		foreach($puserlist as $k=>$v){
				$uinfo = $this->get_user($v['uid']);
				$res[] = $uinfo;
		}
		
		$this->PeiList = $res;
		$this->display("Peiuserlist");
	}


  //计算每月营销以及推广用户的魅力值以及增加的倍数
	public function getbonusevermonth(){
        $m = M('user_base');
        $sql = "SELECT uid,user_type from `t_user_base` where `user_type`=3 or `user_type`=9 OR `user_type`=10 OR `user_type`=5";
        $datauid = $m->query($sql);

        foreach($datauid as $K=>$v){
            $PMoneyM = new PMoneyModel();
            $mmoneyM=M('m_money');
            $Wdata = array();
            $Wdata['type']=array('in',array(1,2,3,12,13));
            $Wdata['money'] = array('gt',0);
            $yue =date('Y-m');
            $yueArr = $this->getShiJianChuo($yue);
            $Wdata["paytime"]=array(array('egt',$yueArr["begin"]),array('elt',$yueArr["end"]));
            $Wdata['uid']=$v['uid'];

            $ret = $PMoneyM->where($Wdata)->field('SUM(money) as allmoney')->find();
            $allmoney=$ret['allmoney'];
            if($allmoney/100*6.5>1000){
                if($v['user_type']==3){
					 if($allmoney/100*6.5<2000){
					 	$awardmoney=$allmoney*1.1-$allmoney;
                         $sus=$mmoneyM->where(array('uid'=>$v['uid']))->setField('awardmoney',$awardmoney);

				}elseif($allmoney/100*6.5<5000){
                         $awardmoney=$allmoney*1.2-$allmoney;
                         $sus=$mmoneyM->where(array('uid'=>$v['uid']))->setField('awardmoney',$awardmoney);
					 }else{
                         $awardmoney=$allmoney*1.3-$allmoney;
                         $sus=$mmoneyM->where(array('uid'=>$v['uid']))->setField('awardmoney',$awardmoney);
					 }
                }
                 if($v['user_type']==5){
                 	if($allmoney/100*6.5>5000){
                        $awardmoney=$allmoney*1.2-$allmoney;
                        $sus=$mmoneyM->where(array('uid'=>$v['uid']))->setField('awardmoney',$awardmoney);
					}
                 }
                if($v['user_type']==9||$v['user_type']==10){
                    if($allmoney/100*6.5>5000){
                        $awardmoney=$allmoney*1.2-$allmoney;
                        $sus=$mmoneyM->where(array('uid'=>$v['uid']))->setField('awardmoney',$awardmoney);
                    }
                }


                if($sus){
                    echo 2;
				}

			}

		}
        echo 1;

	}
    protected function getShiJianChuo($nianyue=0){
        $arr = explode("-", $nianyue);
        $nian = $arr[0];
        $yue = $arr[1];
        if(empty($nian) || empty($yue)){
            $now = time();
            $nian = date("Y",$now);
            $yue =  date("m",$now);
        }
        $time['begin'] = mktime(0,0,0,$yue,1,$nian);
        $time['end'] = mktime(23,59,59,($yue+1),0,$nian);
        return $time;
    }
		
}

?>
