<?php

/**
 * Created by PhpStorm.
 * User: 亮亮
 * Date: 2018/4/24
 * Time: 10:21
 */
class TurntableController extends BaseAdmsController
{
    public function __construct ()
    {
        parent::__construct ();
        $this->basename = '大转盘-'; // 进行模板变量赋值
        $this->cachepath = WR . '/userdata/cache/turntable/';
        $this->msgfileurl = '/userdata/turntable/'.date('Ymd',time())."/";

    }

    public function lists ()
    {
        $this->name = $this->basename . '列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 12;
        $Turntable = new TurntableModel();
        //$status = include WR . "/userdata/publicvar/status.php";
        //$this->switch_type = $status["gift"]["type"];
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|propname|title'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $Turntable->getListPage ($Wdata, $Page, $PageSize);
        $this->msgurl=C ("IMAGEURL");
        $this->DataList= $ret["list"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        //Dump($this->ListData);exit;
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display ();
    }


    public function add(){
        $this->name = $this->basename.'添加'; // 进行模板变量赋值

        if($_POST){
            if(!empty($_FILES)) {
                $PutOssarr=$this->_imgupload();
            }
            if($_POST["title"]){
                $Turntable = new TurntableModel();
                $map = array();
                $map["title"] = $_POST["title"];
                $map["gifttype"] = $_POST["gifttype"];
                $map["propname"] = $_POST["propname"];
                    $map["prop"] = $_POST["prop"];
                $map["money"] = $_POST["money"];
                    $map["img"] =  $PutOssarr;
                $Turntable->addOne($map);
                    $this->tip = $_POST["title"]."已添加";
                    header('Location:applist.html');
            }else{
                $this->tip = "不能为空！";
            }
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }
    /*
	 * 删除
	 */
    public function del(){

        $id =$_REQUEST["id"];
        $img=$_REQUEST["mg"];
        if($id){
            $Turntable = new TurntableModel();
            $map = array();
            $map["id"] = $id;
            $Turntable->delOne($map);
            DelOss($img);
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }

    //修改
    public function edit(){

        $this->name = $this->basename.'修改商品'; // 进行模板变量赋值
        $Turntable = new TurntableModel();
        $id = $_REQUEST['id'];
        $tmpimg=$Turntable->where(array('id'=>$id))->getField('img');
        if($id){
            if($_POST){
                if($_POST["id"]){
                    if(!empty($_FILES)) {
                        $PutOssarr=$this->_imgupload();
                        if(!$PutOssarr){
                            $PutOssarr=$tmpimg;
                        }else{
                            DelOss($tmpimg);
                        }
                    }else{
                        $PutOssarr=$tmpimg;
                    }
                           $map=array();
                            $where = array();
                           $map['id']=$id;
                        $where["title"] =$_POST["title"];
                        $where["gifttype"] =$_POST["gifttype"];
                        $where["propname"] =$_POST["propname"];
                        $where["prob"] =$_POST["prob"];
                    $where["money"] =$_POST["money"];
                        $where["img"] =$PutOssarr;
                       // $where["url"] =$PutOssarr[0];
                    $this->msgurl=C ("IMAGEURL");
                        $tempT = $Turntable->getOne($_POST["id"]);
                        if($tempT){
                            $Turntable->updateOne($map,$where);
                        }else{
                            $this->tip = "错误！";
                        }
                    }
                    header('Location:applist.html');
                    $this->tip = "已保存！";

                }else{
                    $this->tip = "错误！";
                }


            $where = array();
            $where["id"] = $id;
            $temp= $Turntable->getList($where);

            $Info = array();

            foreach($temp[0] as $k=>$v){
                $Info[$k]=$v;
            }


            $this->Info = $Info;
            $this->msgurl=C ("IMAGEURL");
            // Dump( $TranslateList);exit;
            //$this->code = $code;
        }else{
            echo "error";exit;
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }
    //销售状态修改
    public function updatestatus()
    {
        $Turntable = new TurntableModel();
        if ($_POST) {
            if ($_POST["gid"]) {
                $map=array();
                $map['isuse']=$_POST["status"];
                $return=array();
                $return['id']=$_POST["gid"];
                 $result=$Turntable->updateOne ($return,$map);
                 if($result){
                     $content= array(
                         'status' => 1,
                         'gstatus'=>$map['status'],
                         'message' => '新增成功'
                     );
                     exit(json_encode ($content));
                 }else{
                     $content= array(
                         'status' => 2,
                         'message' => '修改失败！'
                     );
                     exit(json_encode ($content));
                 }
                //return json_encode ($map);
            }
        }
    }

    //图片上传方法
    protected function _upload($savePath) {
        import("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir($savePath);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 3292200;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg();
            //Push_data($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            //$uploadList = $uploadList[0];
            //$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
            //$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

            return $uploadList;
            //import("Extend.Library.ORG.Util.Image");
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            //Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
            //$_POST['image'] = $uploadList[0]['savename'];
        }

    }
    //图片上传封装
    public function _imgupload(){
        $filetype = "image";
        $saveUrl = $this->msgfileurl . $filetype . "/";
        $savePath = WR . $saveUrl;
        //echo $savePath;exit;
        $uploadList = $this->_upload ($savePath);
        if (!empty($uploadList)) {
            // $UserPhotoM = new PhotoModel();
            $PutOssarr = array();
            $returnurlarr = array();
            foreach ($uploadList as $k => $v) {
                $PutOssarr[] = $saveUrl . $v['savename'];
                $returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
            }
            PutOss ($PutOssarr);
            // print_r ($PutOssarr);exit;
            $return = array();
            $return["data"]["url"] = $PutOssarr[0];
            return $PutOssarr[0];
        }
    }

}







