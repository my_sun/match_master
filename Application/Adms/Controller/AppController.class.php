<?php
class AppController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
        $this->basename = '上传apk'; // 进行模板变量赋值
        $this->Logofile =  "/userdate/admin/app/logo/";
        $this->Logofileurl = C("IMAGEURL").$this->Logofile;
	}
	/*
	 *App列表
	*/
	public function AppList(){
        $this->name = 'App列表';  // 进行模板变量赋值
        $Page = $_GET['Page'] ? $_GET['Page'] : 1;
        $PageSize = 20;

        $WhereArr = array();
        if (isset($_GET['appname'])&&$_GET['appname']!=""){
            $WhereArr['appname'] = $_GET['appname'];
        }
        if (isset($_GET['version'])&&$_GET['version']!=""){
            $WhereArr['version'] = $_GET['version'];
        }
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword=$_GET['kerword'];
			$WhereArr['apkname']=array('like',"%".$kerword."%");
		}
        $AppM = new AppModel();
        $AppNameM = new AppNameModel();

        $AppNameInfo = $AppNameM->getAll();
        $ret = $AppM->getListPage($WhereArr,$Page,$PageSize);
		$all_page = ceil($ret['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;

		$this->list = array_reverse($ret['list']);
		$this->AppNameInfo = $AppNameInfo;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
        $this->action = "/Adms/App/applist.html";
        $this->display("applist");
	}
	/*
	 *添加App
	*/
    public function add(){
        $AppNameM = new AppNameModel();

        $AppNameInfo = $AppNameM->getAll();
        $this->AppNameInfo = $AppNameInfo;
        $this->action =  __ACTION__.".html";
        $this->display();
    }

    public function create(){
        ini_set('max_execution_time', '1000000');
        $this->name = $this->basename.'添加apk'; // 进行模板变量赋值
        if($_POST){
            $err=0;
            // $msg="";
            // if(!$_POST["apkname"]){
            //     $err = 3;
            //     $msg.="名称不能为空<br />";
            // }
            if($err==0){
                $AppM = new AppModel();
                $map = array();
                if ($_POST["appname"]){
                    $map["appname"] = $_POST["appname"];
                }else{
                    $this->error("请选择app名称");
                }
                if ($_POST["version"]){
                    $map["version"] = $_POST["version"];
                }else{
                    $this->error("请选择版本");
                }
                if ($_POST["apkname"]){
                    $map["apkname"] = $_POST["apkname"];
                }else{
                    $this->error("请填写apk描述");
                }
                $map["addtime"] = time();
                if($_FILES["logo"]['name']&&$_FILES["logo"]["error"]!=4){
                    if($_FILES["logo"]["error"]==0){
                        $txt = pathinfo($_FILES['logo']['name']);
                        if ($txt){
                            $res = $txt['extension'];
                            if ($res != 'apk'){
                                $this->error("只能上传apk文件");
                            }
                        }
                        if(!is_dir(WR."/userdata/apk/"))
                        {
                            mkdir(WR."/userdata/apk/");
                        }
                        $filename = WR."/userdata/apk/".$map["apkname"].".apk";
                        $res=move_uploaded_file($_FILES["logo"]["tmp_name"],$filename);
                        $map["logo"] =  $filename;
                    }else{
                        echo "文件上传错误,code:".$_FILES["logo"]["error"];exit;
                    }
                }else{
                    $this->error("请上传apk文件");
                }
                $AppM->addOne($map);
                header('Location:applist.html');
            }
        }
    }

    /*
     * 删除app
     */
    // public function del(){
    //
    //     $id =$_REQUEST["id"];
    //     if($id){
    //         $AppM = new AppModel();
    //         $map = array();
    //         $map["id"] = $id;
    //         $AppM->delOne($map);
    //
    //     }
    //     header('Location:'.$_SERVER['HTTP_REFERER']);
    // }

    //下载apk
    public function  downloadapk(){
        //判断文件是否存在
        $file_path=WR."/userdata/apk/";
        if(!file_exists($file_path)){
            echo "没有该文件";
            return false;
        }
        //获取apk
        $AppM = new AppModel();
        $data = $AppM->getOne(array("id"=>$_GET['id']));
        $file_name=$data['logo'];
        $file_name = str_replace("/var/www/dingqianqian/",'',$file_name);

        //$filename = "WR.\"/userdata/apk/\"/$file_name.apk";//必须给足权限下载
        $filename = "$file_name";//必须给足权限下载
        header("application/vnd.android.package-archive");
        header('Content-Type: application/octet-stream');
        header("Content-Disposition: attachment; filename=".$filename);
        header("Content-length: " . filesize($filename));
        readfile($filename);
        die;
    }

    //二维码下载apk
    public function  ewmdownloadapk(){
        //判断文件是否存在
        $file_path=WR."/userdata/apk/";
        if(!file_exists($file_path)){
            echo "没有该文件";
            return false;
        }
        //获取apk
        $AppM = new AppModel();
        $data = $AppM->getOne(array("id"=>$_GET['id']));
        $file_name=$data['logo'];
        $file_name = substr($file_name , 35 , 100);

        // $filename = "WR.\"/userdata/apk/\"/$file_name.apk";//必须给足权限下载
        $filename = "$file_name";//必须给足权限下载
        header("application/vnd.android.package-archive");
        header('Content-Type: application/octet-stream');
        header("Content-Disposition: attachment; filename=".$filename);
        header("Content-length: " . filesize($filename));
        readfile($filename);
        die;
    }

    //下载apk
    public function download2(){
            import("Sver.lib.Util.ApkParser");
            $appObj  = new Apkparser();

            header("Content-type:text/html;charset=utf-8");
            $AppM = new AppModel();
            $data = $AppM->getOne(array("id"=>$_GET['id']));
            $file_name=$data['logo'];
            $file_path=WR."/userdata/apk/";


    //首先要判断给定的文件存在与否

//首先要判断给定的文件存在与否

            if(!file_exists($file_path)){
                echo "没有该文件";
                return false;
            }
            $fp=fopen($file_path,"r");
            $file_size=filesize($file_path);

    //下载文件需要用到的头
            Header("Content-type: application/octet-stream");
            Header("Accept-Ranges: bytes");
            Header("Accept-Length:".$file_size);
            Header("Content-Disposition: attachment; filename=".$file_name);
            $buffer=1024;
            $file_count=0;
    //向浏览器返回数据
            while(!feof($fp) && $file_count<$file_size){
                $file_con=fread($fp,$buffer);
                $file_count+=$buffer;
                echo $file_con;
            }
            fclose($fp);
        }
    //上传图片
    public function uploadimg(){

        $path = WR.'/UserData/'.'cache/goodsimg/';
        $url = '/UserData/'.'cache/goodsimg/';
        mkdirs($path);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 32922000;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'apk');
        //设置附件上传目录
        $upload->savePath = $path;
        $upload->saveRule =time().".jpg";

        if (!$upload->upload()) {
            return 0;
        } else {
            PutOss($url .$upload->saveRule);
            return $url .$upload->saveRule;
        }
    }
}

?>
