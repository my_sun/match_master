<?php
class RetdataController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		}

	
	public function returndata(){
		$this->name = '返回数据筛选';
		if(!empty($_POST['user_type_1']) || !empty($_POST['user_type_3_5']) || !empty($_POST['user_type_2'])){
			$allmd5 =  md5(json_encode($_POST));
			$where['product_type'] = $_POST['product_type'];
			$where['page'] = $_POST['page'];
			$where['usertype'] = $_POST['usertype'];
			$wheremd5 =  md5(json_encode($where));
			$user_num = $_POST['user_type_1'] + $_POST['user_type_3_5'] +$_POST['user_type_2'];
			$vip_num = $_POST['uservip'] + $_POST['useroffvip'];
			if($user_num != 100 ){
				$error = 1;
				$msg = "用户类型返回数比例必须100%";
			}elseif($vip_num != 100 ){
				$error = 1;
				$msg = "VIP选项返回数比例必须100%";
			}elseif($where['product_type'] == 24112 && $where['page'] ==2 ){
				$error = 1;
				$msg = "非24112平台暂无推荐页面";
			}else{
				if($_POST['useroffvip'] != 0){
					$line = $_POST['useronline'] + $_POST['userpushline'] +$_POST['useroffline']+$_POST['online'] +$_POST['pushonline'];
					if($line != 100){
						$error = 1;
						$msg = "返回非VIP需设置意向用户";
						$this->error=$error;
						$this->msg=$msg;
						$this->action = "/adms/Retdata/returndata";

						$this->display();die;
					}
				}
				$retda = new RetdataModel();
				$md5_usereturn = $retda->getOne(array("md5_usereturn"=>$allmd5));
				if(!empty($md5_usereturn)){
					$error = 1;
					$msg = "该条件和返回结果，结构已存在，请重新选择。";
				}else{
					$condition_md5 = $retda->getOne(array("condition_md5"=>$wheremd5));
					if($condition_md5){
						$retda->updateOne(array("condition_md5"=>$wheremd5),array("is_success"=>2));
//						$arr = array(
//							"md5_usereturn"=>$allmd5,
//							"json_usereturn"=>json_encode($_POST),
//							"ctime"=>time(),
//							"condition_md5"=>$wheremd5,
//
//						);
						$retda->addOne(array("md5_usereturn"=>$allmd5,"json_usereturn"=>json_encode($_POST),"ctime"=>time(),"condition_md5"=>$wheremd5));
						$error = 1;
						$msg = "已为您切换至最新配置";
					}else{
						$retda->addOne(array("md5_usereturn"=>$allmd5,"json_usereturn"=>json_encode($_POST),"ctime"=>time(),"condition_md5"=>$wheremd5));
						$error = 1;
						$msg = "操作成功！";
					}
				}
			}

		}
		//else{
//			$error = 1;
//			$msg = "请正确填写比例";
//		}
//

		//获取平台号
		$getproduct1=M('products')->select();
		foreach($getproduct1 as $k=>$v){
			$getproduct[$k]['id']=$v['product'];
		}
		$this->getproduct=$getproduct;
		$this->error=$error;
		$this->msg=$msg;
		$this->action = "/adms/Retdata/returndata";

		$this->display();
	}


	public function returndatalist(){
		$yixiang = M("retdata")->order("ctime desc")->select();
		foreach ($yixiang as &$v){
			$usereturn = json_decode($v['json_usereturn'],true);
			$product = implode("，",$usereturn['productids']);
			$v['pro'] = $product;
			$usereturn['page'] = $usereturn['page'] == 1 ? "推荐页面":"附近页面";
			if($usereturn['usertype'] == 1){
				$usereturn['usertype'] = "普通男VIP";
			}else if ($usereturn['usertype'] == 2){
				$usereturn['usertype'] = "普通男非VIP";
			}else if ($usereturn['usertype'] == 3){
				$usereturn['usertype'] = "普通男非VIP意向";
			}else if ($usereturn['usertype'] == 4){
				$usereturn['usertype'] = "普通女VIP";
			}else if ($usereturn['usertype'] == 5){
				$usereturn['usertype'] = "普通女非VIP";
			}else if ($usereturn['usertype'] == 6){
				$usereturn['usertype'] = "普通女非VIP意向";
			}else if ($usereturn['usertype'] == 7){
				$usereturn['usertype'] = "推广男";
			}else if ($usereturn['usertype'] == 8){
				$usereturn['usertype'] = "推广女";
			}
			$usereturn['useronline'] = $usereturn['useronline'] == '' ?0:$usereturn['useronline'];
			$usereturn['useroffline'] = $usereturn['useroffline'] == '' ?0:$usereturn['useroffline'];
			$v['list'] = $usereturn;
		}
		$this->DataList = $yixiang;
		$this->name = '返回数据筛选';
		$this->display();
	}



	public function success(){
		$id = $_GET['id'];
		$md5 = $_GET['md5'];

		M("retdata")->where(array("condition_md5"=>$md5))->save(array("is_success"=>2));
		M("retdata")->where(array("id"=>$id))->save(array("is_success"=>1));
		header('Location: returndatalist.html');
	}
	public function del(){
		$id = $_GET['id'];
		M("retdata")->where(array("id"=>$id))->delete();
		header('Location: returndatalist.html');
	}




	public function setuser(){
		//获取公共配置表数据
		$commSet = M("intercept")->find();

		if(!empty($_POST['id'])){

			$id = $_POST['id'];
			unset($_POST['id']);
			$info = $_POST;
			$resid = M("intercept")->where(array("id"=>$id))->save($info);
			if(!empty($resid)){
				$error=1;
				$msg="保存配置成功！";
				header('Location: setuser.html');
			}else{
				$error=1;
				$msg="您并未进行数据更改！";
			}
			// }

		}
		$this->vodcommset = $commSet;
		$this->error=$error;
		$this->msg=$msg;
		$this->name = '新注册用户消息配置';
		$this->action = "/adms/Retdata/setuser";
		$this->display();
	}




}

?>
