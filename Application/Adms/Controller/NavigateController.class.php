<?php

/**
 * Created by PhpStorm.
 * User: 亮亮
 * Date: 2018/4/24
 * Time: 10:21
 */
class NavigateController extends BaseAdmsController
{
    public function __construct ()
    {
        parent::__construct ();
        $this->basename = '随机目录管理'; // 进行模板变量赋值
    }

    public function lists ()
    {
        $this->name = $this->basename . '列表'; // 进行模板变量赋值
        $vavigate= M('navigation')->select();
        $this->DataList= $vavigate;
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display ();
    }


    public function add(){
        ini_set('max_execution_time', '1000000');
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        if($_POST){
            $err=0;
            $msg="";
            if(!$_POST["title"]){
                $err = 3;
                $msg.="名称不能为空<br />";
            }
            if($err==0){
                $map = array();
                $map["name"] = $_POST["title"];
                $AttrValue = M('navigation')->where(array('name'=>$map["name"]))->find();
                if($AttrValue){
                    $this->tip = "名称已经存在！";
                }else{
                    $map["type"] = $_POST["type"];
                    M('navigation')->data($map)->add();
                    $this->tip = $_POST["title"]."已添加";
                    header('Location:applist.html');
                }

            }else{
                $this->tip = $msg;
            }
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }
    /*
	 * 删除
	 */
    public function del(){
        $code =$_REQUEST["id"];
        if($code){
            M('navigation')->where(array('id'=>$code))->delete();
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }

    //修改
    public function edit(){
        $this->name = $this->basename.'修改名称'; // 进行模板变量赋值
        $id = $_REQUEST['id'];
        if($_REQUEST['title']){
            $name=$_REQUEST['title'];
            $type=$_REQUEST['type'];
            $edit=M('navigation')->where(array('id'=>$id))->save(array('name'=>$name,'type'=>$type));
            if($edit){
                $err = 1;
                $msg="修改成功";
            }else{
                $err = 0;
                $msg="修改失败";
            }
            $this->tip = $msg;
        }

        $this->id = $id;
        $this->action =  __ACTION__.".html";
        $this->display("add");
    }


    //是否显示
    public function isadd(){
        $id=$_POST['id'];
        $type=$_POST['type'];
        if($type==1){
            //加入
            $product=M('navigation')->where(array('id'=>$id))->save(array('type'=>1));
            if($product){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已加入",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }elseif($type==0){
            //取消加入
            $product=M('navigation')->where(array('id'=>$id))->save(array('type'=>0));

            if($product){
                $reuslt = array(
                    'status' =>'0',
                    'message' => "已取消加入",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }

    }

}


