<?php

use Think\Controller;

class BaseAdmsController extends Controller
{
    public $Api_recive_date = array();
    public $lang = 'simplified';
    public $uid = '0';


    public function _initialize()
    {
        if ($_GET["token"] != "99123") {
            $AdminUser = session("AdminUser");
            $this->is_login();
            $supermen=M('auth_group_access')->where(array('group_id'=>1))->getField('uid',true);
            if (!in_array($AdminUser['id'],$supermen)) {
                $auth = new \Think\Auth();
                $rule_name = MODULE_NAME . '/' . CONTROLLER_NAME . '/' . ACTION_NAME;
                $result = $auth->check($rule_name, $AdminUser['id']);
                if (!$result) {
                    $this->error('您没有权限访问');
                }
            }
        }
    }

    public function __construct()
    {
        parent::__construct();
        //$this->is_login();
        $this->AdminUser = cookie("AdminUser");
        // 分配菜单数据
        $nav_data = D('AdminNav')->getTreeData('level', 'order_number,id');

        $assign = array(
            'nav_data' => $nav_data
        );
        //$this->menu = include WR."/userdata/admin/menu.php";
        $this->menu = $nav_data;

        //echo TranslateAnysay("hi");exit;
        //Dump($this->AdminUser);exit;
    }

    public function call_all_product_url($url)
    {
        $allProduct = C("_PRODUCT");
        foreach ($allProduct as $k => $v) {
            $geturl = "http://" . $v . ":81" . $url;
            file_get_contents($geturl);
        }
        return 1;
    }

    public function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }

    public function sendsysmsg($uid, $content, $msgType = 'text')
    {
        $messageBody = array(
            "content" => $content,
            "sendTime" => time(),
        );
        $to = $uid;
        $from = "10002";
        $mybox_id = $from . $to;
        $MyBoxinfo = array();
        $sendmsg = array();
        $sendmsg["touid"] = $to;
        $sendmsg["msgid"] = substr(md5($this->getMillisecond()), 8, 16);//substr(md5(time()), 8, 16)
        $body = array();
        $body["id"] = $sendmsg["msgid"];
        $body["msgType"] = $msgType;
        $body["messageBody"] = $messageBody;
        $body["user"] = $this->get_diy_user_field($from, "uid|gender|age|nickname|vipgrade|vip|gold|head");
        $body["leftgold"] = $this->get_diy_user_field($to, 'gold');
        $sendmsg["msgbody"] = $body;
        $touserinfo = $this->get_diy_user_field($to, "uid|product|version");

        //将消息储存在消息记录中
        $MyBoxinfo['type'] = $msgType;
        $MyBoxinfo['id'] = $mybox_id;
        $MyBoxinfo['content'] = $content;
        $MyBoxinfo['uid'] = $from;
        $MyBoxinfo['touid'] = $to;
        $MyBoxinfo['sendtime'] = time();
        //信箱所需字段信息
        $Msginfo['type'] = $msgType;
        $Msginfo['sendtime'] = $MyBoxinfo['sendtime'];
        $Msginfo['uid'] = $from;
        $Msginfo['touid'] = $to;
        $Msginfo['content'] = $content;
        $Msginfo['box_id'] = $mybox_id;
        $MsgM = new MsgModel();
        $MsgBoxm = new MsgBoxModel();
        $result = $MsgM->addOne($Msginfo);

        //消息列表储存过后将消息储存在信箱列表中，运用redis查询信息列表中是否已有来往信息
        if ($result) {
            //统计用户发送次数start
            $redis = $this->redisconn();
            $redisStrj = "Smsg_" . $to;
            $val = $redis->get($redisStrj);
            $redis->set($redisStrj, $val + 1, 0, 0, 60 * 60 * 24 * 3);
            //统计用户发送次数end

            $redisStr = "Gmsgbox_" . $mybox_id;

            $cachetime = 60 * 60 * 24 * 3;
            $ab['id'] = $mybox_id;
            if ($redis->exists($redisStr)) {
                $MsgBoxm->updateOne1($ab, $MyBoxinfo);
            } else {
                $value = $MsgBoxm->getOne1($ab);
                if ($value) {
                    $MsgBoxm->updateOne1($ab, $MyBoxinfo);
                    $redis->set($redisStr, $mybox_id, 0, 0, $cachetime);
                } else {
                    $MsgBoxm->addOne1($MyBoxinfo);
                    $redis->set($redisStr, $mybox_id, 0, 0, $cachetime);
                }
            }

        }
        //将消息储存在消息记录中end

        //24110版本过度返回腾讯key策略
        $txkey=M('txmy')->where(array('product'=>$touserinfo["product"]))->getField('miyao');
        if ($touserinfo["product"] == 24110) {
            $version = str_replace('.', '', $touserinfo["version"]);
            if ((int)$version > 105) {
                if (isset(C("IMSDKID")['23110'])) {
                    $ret = tencent_sendmsg($sendmsg, C("IMSDKID")['23110']);
                    return $ret;
                } else {
                    return LM_sendmsg($sendmsg);
                }
            } else {
                if ($txkey) {
                    $ret = tencent_sendmsg($sendmsg,$txkey);
                    return $ret;
                } else {
                    return LM_sendmsg($sendmsg);
                }
            }
        } else {
            if ($txkey) {
                $ret = tencent_sendmsg($sendmsg,$txkey);
                return $ret;
            } else {
                return LM_sendmsg($sendmsg);
            }
        }


    }

    //连接redis
    public function redisconn()
    {
        $redis = new RedisModel();
        return $redis;
    }

    //格式化 语言
    public function format_lang($lang)
    {
        if ($lang == "en") {
            return 'english';
        } else {
            return 'traditional';
        }
    }

    public function get_languagecode1($reset = 0)
    {
        $path = CHCHEPATH_LANGUAGE;
        $cache_name = 'languagelistcode';
        if (F($cache_name, '', $path) && $reset == 0) {
            $newarr = F($cache_name, '', $path);
        } else {
            $where = array();
            $LanguageM = new LanguageModel();
            $res = $LanguageM->getList($where);
            $newarr = array();
            foreach ($res as $v) {
                $newarr[] = $v["code"];
            }
            F($cache_name, $newarr, $path);
        }
        return $newarr;
    }

//从redis中获取语言缓存
    public function get_languagecode($reset = 0)
    {
        $redis = $this->redisconn();
        //缓存中储存总数据
        $lang = $redis->listSize('language');
        //如果缓存中不存在，则取数据库最新数据存入
        if ($lang == 0) {
            $where = array();
            $LanguageM = new LanguageModel();
            $res = $LanguageM->getList($where);
            foreach ($res as $k => $v) {
                $res = array(
                    "id" => $v["code"],
                );
                $value = json_encode($res, true);
                $ret[] = $redis->listPush('language', $value);
            }
            $result1 = $res;
        } else {
            $result = $redis->listLrange('language', 0, -1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    $result1[] = json_decode($v1, true);
                }
            }
        }

//Dump($newarr);exit;
        return $result1;
    }

    /*
     * 添加翻译
     */
    public function translate_add($code, $content)
    {
        $languagecode = $this->get_languagecode();
        $TranslateM = new TranslateModel();
        $tran = array();
        $where["code"] = strtoupper($code);
        $tran["code"] = strtoupper($code);
        $tran["content"] = $content;
        $tempT = $TranslateM->getOne($where);
        if ($tempT) {
            return 0;
            // return $TranslateM->updateOne($where,array("content"=>$tran["content"]));
        } else {

            foreach ($languagecode as $v) {
                $tempv = TranslateMicrosoft($content, $v['id']);
                if ($tempv) {
                    $tran["content"] = $tempv;
                }
                $tran["lang"] = $v['id'];
                $TranslateM->addOne($tran);
            }

            return true;
        }
    }

    /*
     * 删除翻译
     */
    public function translate_del($code)
    {
        $TranslateM = new TranslateModel();
        $where["code"] = strtoupper($code);
        return $TranslateM->delOne($where);
    }

    /*
     *
     * 验证用户是否登陆
     */
    public function is_login()
    {
        $Public_controller = array(
            'View',
            'Ajax',
            'Login',
            'Pay'
        );
        if (!in_array(CONTROLLER_NAME, $Public_controller)) {
            if (!session("AdminUser")) {
                header("Location:/Adms/Login/login.html");
                //$this->error('操作失败','/Adms/Index/Login');
                //IndexController::Index();exit;
            }
        }
    }

    //生成分页
    public function GetPages($arges)
    {

        $pre_page = $arges["pageNum"] - 1 ? $arges["pageNum"] - 1 : 1;
        $nex_page = $arges["pageNum"] + 1;
        $arges["all_page"] = $arges["all_page"] ? $arges["all_page"] : ceil($arges["totalCount"] / $arges["pageSize"]);
        if ($arges["all_page"] < 2) {
            return '';
        }
        $url = $_SERVER['PHP_SELF'];
        $url_arges = $_GET;
        if (empty($url_arges)) {
            $url .= "?a=1";
        } else {
            unset($url_arges["Page"]);
            $url .= "?" . http_build_query($url_arges);
        }

        $pages = '<ul class="pagination">';
        $pages .= '<li><a>共' . $arges["totalCount"] . '条数据</a></li>';
        if ($pre_page > 0) {
            $pages .= '<li><a href="' . $url . "&Page=" . $pre_page . '">上一页</a></li>';
        }
        $start = $arges["pageNum"] - 5;
        if ($start < 1) {
            $start = 1;
        }
        $end = $arges["pageNum"] + 5;
        if ($end > $arges["all_page"]) {
            $end = $arges["all_page"];
        }

        for ($x = $start; $x <= $end; $x++) {
            if ($x == $arges["pageNum"]) {
                $pages .= '<li class="active"><a href="' . $url . "&Page=" . $x . '">' . $x . '</a></li>';
            } else {
                $pages .= '<li><a href="' . $url . "&Page=" . $x . '">' . $x . '</a></li>';
            }
        }
        if ($nex_page <= $arges["all_page"]) {
            $pages .= '<li><a href="' . $url . "&Page=" . $nex_page . '">下一页</a></li>';
        }

        $pages .= '</ul> ';
        return $pages;
    }

    /*
      *
     * user_ico 获取用户头像//将用户头像储存在redis中
     */
    public function get_user_ico($uid,$reset=0){
        $redis = $this->redisconn();
        $redisname='getuser-photo-'.$uid;
        if($reset==1){
            $redis->del($redisname);
            return true;
        }
        //首先判断redis中是否存在，不存在则在数据库中取
        $keyexit=$redis->exists($redisname);
        if($keyexit) {
            $result = $redis->get($redisname);
            $res = json_decode($result, true);
        }else {
            $redis = $this->redisconn();
            $UserPhotoM = new PhotoModel();
            $where = array();
            $where['uid'] = $uid;
            $where['type'] = "2";
            $where['status'] = "1";
            $ret = $UserPhotoM->getOne($where);
            if (!$ret) {
                $photos = $this->get_user_photo($uid);
                if ($photos[0]) {
                    $ret = $photos[0];
                    $ret["url"] = str_replace(C("IMAGEURL"), "", $ret["url"]);

                }
            }
            if ($ret) {
                $image = array();
                $image['id'] = $ret["id"];
                $image['url'] = C("IMAGEURL") . $ret["url"];//原图url
                $image['thumbnaillarge'] = C("IMAGEURL") . $ret["url"];//大图url
                $image['thumbnailsmall'] = C("IMAGEURL") . $ret["url"];//小图url
                $image['status'] = $ret["status"];//状态1审核通过;2正在审核3审核未通过已删除
                $image['seetype'] = $ret["seetype"]; //可见级别(1所有用户可见,2会员可见)

            } else {
                $image = array();
                $image['id'] = 1;
                $image['url'] = C("IMAGEURL") . '/1.png';//原图url
                $image['thumbnaillarge'] = C("IMAGEURL") . '/1.png';//大图url
                $image['thumbnailsmall'] = C("IMAGEURL") . '/1.png';//小图url
                $image['status'] = 2;//状态1审核通过;2正在审核3审核未通过已删除
                $image['seetype'] = 1; //可见级别(1所有用户可见,2会员可见)
            }
            $res = $image;
            $redis_photosinfo=json_encode($res);
            $getres=$redis->set($redisname,$redis_photosinfo,0,0,60*60*24);
            if(!$getres){
                return false;
            }

        }
        return $res;
    }
    /*
         *
         * user_ico 获取用户自己的头像（不限制是否通过审核）
         */
    public function get_user_ico_all($uid,$reset=0){
        $redis = $this->redisconn();
        $redisname='getuser-photo-self-'.$uid;
        if($reset==1){
            $redis->del($redisname);
            return true;
        }
        //首先判断redis中是否存在，不存在则在数据库中取
        $keyexit=$redis->exists($redisname);
        if($keyexit) {
            $result = $redis->get($redisname);
            $res = json_decode($result, true);
        }else {
            $UserPhotoM = new PhotoModel();
            $where = array();
            $where['uid'] = $uid;
            $where['type'] = "2";
            $ret = $UserPhotoM->getOne($where);
            if (!$ret) {
                $photos = $this->get_user_photo($uid);
                if ($photos[0]) {
                    $ret = $photos[0];
                    $ret["url"] = str_replace(C("IMAGEURL"), "", $ret["url"]);
                }
            }
            if ($ret) {
                $image = array();
                $image['id'] = $ret["id"];
                $image['url'] = C("IMAGEURL") . $ret["url"];//原图url
                $image['thumbnaillarge'] = C("IMAGEURL") . $ret["url"];//大图url
                $image['thumbnailsmall'] = C("IMAGEURL") . $ret["url"];//小图url
                $image['status'] = $ret["status"];//状态1审核通过;2正在审核3审核未通过已删除
                $image['seetype'] = $ret["seetype"]; //可见级别(1所有用户可见,2会员可见)

            } else {
                $image = array();
                $image['id'] = 1;
                $image['url'] = C("IMAGEURL") . '/1.png';//原图url
                $image['thumbnaillarge'] = C("IMAGEURL") . '/1.png';//大图url
                $image['thumbnailsmall'] = C("IMAGEURL") . '/1.png';//小图url
                $image['status'] = 1;//状态1审核通过;2正在审核3审核未通过已删除
                $image['seetype'] = 1; //可见级别(1所有用户可见,2会员可见)
            }
            $res = $image;
            $redis_photosinfo=json_encode($res);
            $getres=$redis->set($redisname,$redis_photosinfo,0,0,60*60*24);
            if(!$getres){
                return false;
            }
        }
        return $res;
    }
    /*
      *
      * user_photo 获取用户自己的相册（不限制是否通过审核）
      */
    public function get_user_photo_all($uid,$reset=0){
        // $path = CHCHEPATH_USERPHOTOS;
        //$cache_name = 'user_photo_all_'.$uid;
        //if($reset==1){
        //   return F($cache_name,NULL,$path);
        //}
        // if(F($cache_name,'',$path)){
        //    $res = F($cache_name,'',$path);
        // }else{
        $redis = $this->redisconn();
        $redisname='getuser-photos-self-'.$uid;
        if($reset==1){
            $redis->del($redisname);
            return true;
        }
        //首先判断redis中是否存在，不存在则在数据库中取
        $keyexit=$redis->exists($redisname);
        if($keyexit) {
            $result = $redis->get($redisname);
            $res = json_decode($result, true);
        }else{
            $UserPhotoM = new PhotoModel();
            $where = array();
            $image = array();
            $where['uid'] = $uid;
           // $where['type'] = "1";

            $ret = $UserPhotoM->getList($where);
            if($ret){
                foreach($ret as $k=>$v){
                    $image[$k] = array();
                    $image[$k]['id']=$v["id"];
                    $image[$k]['url']=C("IMAGEURL").$v["url"];//原图url
                    $image[$k]['thumbnaillarge']=C("IMAGEURL").$v["url"];//大图url
                    $image[$k]['thumbnailsmall']=C("IMAGEURL").$v["url"];//小图url
                    $image[$k]['status']=$v["status"];//状态1审核通过;2正在审核3审核未通过已删除
                    $image[$k]['seetype'] =$v["seetype"]; //可见级别(1所有用户可见,2会员可见)
                }
            }
            $res = $image;
            $redis_photosinfo=json_encode($res);
            $getres=$redis->set($redisname,$redis_photosinfo,0,0,60*60*24);
            if(!$getres){
                return false;
            }
            //F($cache_name,$res,$path);
        }
        return $res;


    }

    /*
      *
     * user_photo 获取用户相册//将用户相册存入redis缓存中
     */
    public function get_user_photo($uid,$reset=0){
        //$path = CHCHEPATH_USERPHOTOS;
        // $cache_name = 'user_photo_'.$uid;
        // if($reset==1){
        //    return F($cache_name,NULL,$path);
        // }
        /*if(F($cache_name,'',$path)){
            $res = F($cache_name,'',$path);
        }*/
        $redis = $this->redisconn();
        $reidsname='getuser-photos-'.$uid;
        if($reset==1){
            $redis->del($reidsname);
            return true;
        }
        //首先判断redis中是否存在，不存在则在数据库中取
        $keyexit=$redis->exists($reidsname);
        if($keyexit) {
            $result = $redis->get($reidsname);
            $res = json_decode($result, true);
        }else{
            $redis = $this->redisconn();
            $UserPhotoM = new PhotoModel();
            $where = array();
            $image = array();
            $where['uid'] = $uid;
           // $where['type'] = "1";
            $where['status'] = "1";

            $ret = $UserPhotoM->getList($where);
            if($ret){
                foreach($ret as $k=>$v){
                    $image[$k] = array();
                    $image[$k]['id']=$v["id"];
                    $image[$k]['url']=C("IMAGEURL").$v["url"];//原图url
                    $image[$k]['thumbnaillarge']=C("IMAGEURL").$v["url"];//大图url
                    $image[$k]['thumbnailsmall']=C("IMAGEURL").$v["url"];//小图url
                    $image[$k]['status']=$v["status"];//状态1审核通过;2正在审核3审核未通过已删除
                    $image[$k]['seetype'] =$v["seetype"]; //可见级别(1所有用户可见,2会员可见)
                }
            }
            $res = $image;
            $redis_photosinfo=json_encode($res);
            $getres=$redis->set($reidsname,$redis_photosinfo,0,0,60*60*24);
            if(!$getres){
                return false;
            }
            // F($cache_name,$res,$path);
        }
        return $res;
    }

    /*
     *
     * 获取视频认证对象
     */
    public function get_user_video($uid, $reset = 0)
    {
        $path = WR . '/userdata/cache/video/';
        $cache_name = 'get_user_video' . $uid;
        if ($reset == 1) {
            return F($cache_name, NULL, $path);
        }
        if (F($cache_name, '', $path)) {
            $res = F($cache_name, '', $path);
        } else {
            $UserVideoM = new VideoModel();
            $where = array();
            $res = array();
            $where['uid'] = $uid;
            $ret = $UserVideoM->getOne($where);

            if ($ret) {
                $res['id'] = $ret['id'];
                $res['ltime'] = $ret['ltime'];
                $res['url'] = C("IMAGEURL") . $ret["url"];
                $res['imageurl'] = C("IMAGEURL") . $ret["imageurl"];
                $res['status'] = $ret["status"];
            }
            F($cache_name, $res, $path);
        }
        return $res;
    }

    /*
     *
     * 获取语音认证对象
     */
    public function get_user_audio($uid, $reset = 0)
    {

        /*$path = WR . '/userdata/cache/audio/';
        $cache_name = 'get_user_audio' . $uid;
        //F($cache_name,NULL,$path);
        if ($reset == 1) {
            return F($cache_name, NULL, $path);
        }
        if (F($cache_name, '', $path)) {
            $res = F($cache_name, '', $path);
        } else {
            $AudioM = new AudioModel();
            $where = array();
            $res = array();
            $where['uid'] = $uid;
            $ret = $AudioM->getOne($where);

            if ($ret) {
                $res['id'] = $ret['id'];
                $res['uid'] = $ret['uid'];
                $res['ltime'] = $ret['ltime'];
                $res['url'] = C("IMAGEURL") . $ret["url"];
                $res['status'] = $ret["status"];
            }
            F($cache_name, $res, $path);
        }

        return $res;*/
        return true;
    }

    /*
     * 格式化用户对象
     */
    public function format_user_info($user_info)
    {
        if (!$user_info['uid']) {
            return array();
        }
        $uid = $user_info['uid'];
        $res_date = array(
            'uid' => $user_info['uid'] ? $user_info['uid'] : '',//用户id
            'password' => $user_info['password'] ? $user_info['password'] : '',//用户密码
            'gender' => $user_info['gender'] ? $user_info['gender'] : '1',//性别 1是男 2是女
            'user_type' => $user_info['user_type'] ? $user_info['user_type'] : '1',//
            'age' => $user_info['age'] ? $user_info['age'] : '',//年龄
            'nickname' => $user_info['nickname'] ? $user_info['nickname'] : '',//昵称
            'vipgrade' => $user_info['vipgrade'] ? $user_info['vipgrade'] : '0',//会员等级 1是一级 2是二级
            'vip' => $user_info['vip'] ? $user_info['vip'] : '0',//会员天数
            'viptime' => $user_info['viptime'] ? $user_info['viptime'] : "0",//vip到期时间,
            'gold' => $user_info['gold'] ? $user_info['gold'] : '0',//金币数量
            'mood' => $user_info['mood'] ? $user_info['mood'] : '',//交友宣言
            'blood' => $user_info['blood'] ? $user_info['blood'] : '',//血型
            'height' => $user_info['height'] ? $user_info['height'] : "0",//身高
            'weight' => $user_info['weight'] ? $user_info['weight'] : "0",//体重
            'head' => $this->get_user_ico($uid),//头像
            'photos' => array(),//{image对象列表
            'photosnumber' => "0",//相片数量
            'area' => $user_info['area'] ? $user_info['area'] : "0",//居住地
            'income' => $user_info['income'] ? $user_info['income'] : "0",//收入
            'marriage' => $user_info['marriage'] ? $user_info['marriage'] : "0",//婚姻状况
            'education' => $user_info['education'] ? $user_info['education'] : "0",//学历
            'work' => $user_info['work'] ? $user_info['work'] : "0",//工作
            'constellation' => $user_info['constellation'] ? $user_info['constellation'] : "0",//星座
            'friendsfor' => $user_info['friendsfor'] ? $user_info['friendsfor'] : "0",//交友目的
            'cohabitation' => $user_info['cohabitation'] ? $user_info['cohabitation'] : "0",//婚前同居
            'dateplace' => $user_info['dateplace'] ? $user_info['dateplace'] : "0",//期望约会的地方
            'lovetimes' => $user_info['cohabitation'] ? $user_info['lovetimes'] : "0",//恋爱次数
            'charactertype' => $user_info['charactertype'] ? $user_info['charactertype'] : "0",//性格类型(多选,用|分开)
            'hobby' => $user_info['hobby'] ? $user_info['hobby'] : "0",//兴趣爱好(多选,用|分开)
            'wantchild' => $user_info['wantchild'] ? $user_info['wantchild'] : "0",//是否要小孩
            'house' => $user_info['house'] ? $user_info['house'] : "0",//是否有房
            'car' => $user_info['car'] ? $user_info['car'] : "0",//是否有车
            'conditions' => array(),//{ conditions对象}征友条件
            'line' => $user_info['line'] ? $user_info['line'] : "0",//line
            'tinder' => $user_info['tinder'] ? $user_info['tinder'] : "0",//tinder
            'wechat' => $user_info['wechat'] ? $user_info['wechat'] : "0",//wechat
            'facebook' => $user_info['facebook'] ? $user_info['facebook'] : "0",//facebook
            'email' => $user_info['email'] ? $user_info['email'] : "0",//email
            'twitter' => $user_info['twitter'] ? $user_info['twitter'] : "",//email
            'isphonenumber' => $user_info['isphonenumber'] ? $user_info['isphonenumber'] : "0",//手机号是否认证
            'phonenumber' => $user_info['phonenumber'] ? $user_info['phonenumber'] : "",//手机号
            'isvideo' => $user_info['isvideo'] ? $user_info['isvideo'] : "0",//是否视频认证
            'video' => array(),//{video对象}视频认证信息

            'isaudio' => $user_info['isaudio'] ? $user_info['isaudio'] : "0",//是否语音认证
            'audio' => array(),//{audio对象}  语音认证信息
            'issuiliao' => $user_info['issuiliao'] ? $user_info['issuiliao'] : "0",//聊天用户是否开启随聊功能
            'chatsetting' => array(),//聊天设置对象
            'receivedgifts' => array(),//{收到的礼物对象列表}收到的礼物
            'sendgifts' => array(),//{送出的礼物对象列表}
            'usermood' => array(),//用户动态{用户动态对象}
            'fansnumber' => "0",//粉丝数量
            'follownumber' => "0",//我关注的人数量
            'guarduser' => array(),//守护人对象
            'regtime' => $user_info['regtime'],//注册时间
            'logintime' => $user_info['logintime'],//注册时间
            'platformInfo' => "",//客户端信息对象,
            'country' => $user_info['country'] ? $user_info['country'] : "TW",//国家,
            'language' => $user_info['language'] ? $user_info['language'] : "zh-tw",//语言,
            'product' => $user_info['product'] ? $user_info['product'] : "10508",//product,
            'phoneid' => $user_info['phoneid'] ? $user_info['phoneid'] : "0",//phoneid,
            'systemversion' => $user_info['systemversion'] ? $user_info['systemversion'] : "0",//系统版本号
            'version' => $user_info['version'] ? $user_info['version'] : "0",//手机版本号
            'isopen' => $user_info['isopen'] ? $user_info['isopen'] : "1",//1公开联系方式，2不公开,
            'exoticlove' => $user_info['exoticlove'] ? $user_info['exoticlove'] : "",//是否接受异地恋 单选,
            'sexual' => $user_info['sexual'] ? $user_info['sexual'] : "",//婚前性行为 单选,
            'livewithparents' => $user_info['livewithparents'] ? $user_info['livewithparents'] : "",//愿意同父母居住 单选
            'personalitylabel' => $user_info['personalitylabel'] ? $user_info['personalitylabel'] : "",//个性标签 多选
            'liketype' => $user_info['liketype'] ? $user_info['liketype'] : "",//喜欢的类型 多选
            'glamour' => $user_info['glamour'] ? $user_info['glamour'] : "",//魅力部位 多选
            'puid' => $user_info['puid'] ? $user_info['puid'] : "",//puid
            'isnickname'=>$user_info['isnickname'] ? $user_info['isnickname'] : "2",//isnickname是否审核通过1：通过 2：未审核 3：审核未通过
            'isphoto'=>$user_info['isphpto'] ? $user_info['isphpto'] : "0",//isphpto是否是第一次上传头像
            'fcmtoken'=>$user_info['fcmtoken'] ? $user_info['fcmtoken'] : "0",//fcmtoken
            'ishighopinion'=>$user_info['ishighopinion'] ? $user_info['ishighopinion'] : "0",
            'ishighopinionvideo'=>$user_info['ishighopinionvideo'] ? $user_info['ishighopinionvideo'] : "0",
            'ishighopinionaudio'=>$user_info['ishighopinionaudio'] ? $user_info['ishighopinionaudio'] : "0",
            'ishighopinionshare'=>$user_info['ishighopinionshare'] ? $user_info['ishighopinionshare'] : "0",
            'ishighopinionphoto'=>$user_info['ishighopinionphoto'] ? $user_info['ishighopinionphoto'] : "0",
            'ishighopiniondynamic'=>$user_info['ishighopiniondynamic'] ? $user_info['ishighopiniondynamic'] : "0",
            'ishighopiniondata'=>$user_info['ishighopiniondata'] ? $user_info['ishighopiniondata'] : "0",
            'giftsendcanbysee'=>$user_info['giftsendcanbysee']?$user_info['giftsendcanbysee']:'1',//发送礼物是否可见
            'giftgetcanbysee'=>$user_info['giftgetcanbysee']?$user_info['giftgetcanbysee']:'1',//收到礼物是否可见
            'shareuid'=>$user_info['shareuid'],//推荐人
            'restsharecount'=>$user_info['restsharecount'],//wo推荐d人
            'beshareuid'=>$user_info['beshareuid'],//推荐wo的人
            'isfirstrecharge'=>$user_info['isfirstrecharge'],//是否是第一次充值金币
        );
        $res_date["photosnumber"] = count($res_date["photos"]);
        return $res_date;
    }

    //增加魅力值
    public function m_money_add($uid, $money)
    {
        $mmoney = M('m_money');

        $res = $mmoney->where('uid=' . $uid)->setInc('totalmoney', $money);
        $mmoney->where('uid=' . $uid)->setInc('leftmoney', $money);
        return $res;

    }

    //减少魅力值
    public function m_money_dec($uid, $money)
    {
        $mmoney = M('m_money');

        $res = $mmoney->where('uid=' . $uid)->setDec('totalmoney', $money);
        $mmoney->where('uid=' . $uid)->setDec('leftmoney', $money);
        return $res;

    }

    /*
     *
    * user 对象
    */

    public function get_user($uid, $reset = '0')
    {
        if ($uid) {
            $redis = $this->redisconn();
            if ($reset == 1) {
                $redis->del('getuser-' . $uid);
                return true;
            }
            //首先判断redis中是否存在，不存在则在数据库中取
            $keyexit = $redis->exists('getuser-' . $uid);
            if ($keyexit) {
                $result = $redis->get('getuser-' . $uid);
                $UserInfo = json_decode($result, true);
                //如果redis数据库出问题取不出个人信息，则重新读取数据库
                if(!$UserInfo['uid']){
                    $UserInfo=$this->getUserinfoByDatabase($uid);
                }
                //如果redis中没有个人信息缓存，则读取数据库
            } else {
                $UserInfo=$this->getUserinfoByDatabase($uid);
            }
            //获取用户头像
            $UserInfo['head'] = $this->get_user_ico($uid);//头像
            //计算vip天数
            if ($UserInfo["viptime"]) {
                $UserInfo["vip"] = ceil(($UserInfo["viptime"] - time()) / 60 / 60 / 24) + 1;
            }
            if ($UserInfo["user_type"] == 3 && $UserInfo["vip"] < 0) {
                $UserInfo["vip"] = rand(1000, 2000);
            }
            //如果VIP天数小于0天就显示0天
            if($UserInfo["vip"]<=0){
                $UserInfo["vip"]=0;
                if($UserInfo["user_type"]==1){
                    $UserInfo['vipgrade']=0;
                }
            }

            return $UserInfo;
        } else {
            return array();
        }
    }
    //根据uid从数据库中取出个人信息并存放在redis缓存中
    private function getUserinfoByDatabase($uid){
        $redis = $this->redisconn();
        $where = array('uid' => $uid);
        $User_baseM = new UserBaseModel();
        $UserExtendM = new UserExtendModel();
        $UserInfo = $User_baseM->getOne($where);
        $UserInfo1 = $UserExtendM->getOne($where);
        if (!empty($UserInfo1)) {
            $UserInfo = array_merge($UserInfo, $UserInfo1);
        }

        $UserInfo = $this->format_user_info($UserInfo);
        //将从数据库中得到的用户信息储存在redis中，并设置失效为24小时
        if (!$UserInfo) {
            return false;
        }
        $redis_userinfo = json_encode($UserInfo);
        $getres = $redis->set('getuser-' . $uid, $redis_userinfo, 0, 0, 60 * 60 * 24);
        if (!$getres) {
            return false;
        }
        return $UserInfo;
    }

    /*
     *获取精简的用户信息
     *
     */
    public function get_diy_user_field($uid, $field = "uid|head|nickname")
    {
        $usertemp = $this->get_user($uid);
        if ($field == "*") {
            $user = $usertemp;
        } else {
            $field_arr = explode("|", $field);
            $user = array();
            foreach ($usertemp as $k => $v) {
                if (in_array($k, $field_arr)) {
                    $user[$k] = $v;
                }
            }
        }
        //获取相册
        if (isset($user["photos"])) {
            $user["photos"] = $this->get_user_photo($uid);
        }
        //获取语音认证对象
       // if (isset($user["audio"])) {
       //     $user["audio"] = $this->get_user_audio($uid);
       // }
        //获取视频认证对象
       // if (isset($user["video"])) {
      //      $user["video"] = $this->get_user_video($uid);
      //  }
        return $user;
    }

    /**
     * 用用户id生成用户目录
     */
    public function get_userpath($uid, $type)
    {
        return $ret = WR . '/userdata/' . $type . '/' . ($uid % 100) . '/' . ($uid % 200) . '/';
    }

    /*
      *
      * redissetuserfield redis缓存中设置用户单个字段
      */
    public function set_user_field($uid, $field, $value)
    {
        if ($uid) {
            $redis = $this->redisconn();
            //首先判断redis中是否存在，不存在则不需要修改缓存
            $keyexit = $redis->exists('getuser-' . $uid);
            if ($keyexit) {
                $result = $redis->get('getuser-' . $uid);
                $UserInfo = json_decode($result, true);
                $UserInfo[$field] = $value;
                $res = $redis->delete('getuser-' . $uid);
                if (!$res) {
                    return false;
                }
                $redis_userinfo = json_encode($UserInfo);
                $getres = $redis->set('getuser-' . $uid, $redis_userinfo, 0, 0, 60 * 60 * 24);
                if (!$getres) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }
//获取在线列表
    public function get_onlinelist($product,$user_type,$gender,$line_type){
        $redis=$this->redisconn();
        $useronlinelistkey = "useronlinelist_".$product."_".$user_type."_".$gender;

        $useronlinelist = $redis->listGet($useronlinelistkey, 0, 499);
        $txkey=M('txmy')->where(array('product'=>$product))->getField('miyao');
        $tentxunstatus = tencent_onlinestate($useronlinelist,$txkey);

if($_REQUEST['zx']==1){
    $tentxunstatus =array_reverse($tentxunstatus);
    dump($tentxunstatus);
    dump($useronlinelist);
}
        if($line_type==1){
            foreach ($tentxunstatus as $k1=>$v1){
                if($v1["State"]=="Offline"){
                    $redis->listRemove($useronlinelistkey,$v1['To_Account']);
                }
                if($v1["State"]=="Online"){
                    $onlinelist[]=$v1['To_Account'];
                }
            }
        }elseif($line_type==0){
            foreach ($tentxunstatus as $k1=>$v1){
                $onlinelist[]=$v1['To_Account'];
            }
            if($_REQUEST['ttest']==1){
                dump($tentxunstatus);
                dump($onlinelist);
                exit;
            }
        }elseif($line_type==2){
            foreach ($tentxunstatus as $k1=>$v1){
                if($v1["State"]=="Offline"){
                    $redis->listRemove($useronlinelistkey,$v1['To_Account']);
                }
                if($v1["State"]=="PushOnline"){
                    $onlinelist[]=$v1['To_Account'];
                }
            }

        }
        return $onlinelist;
    }
    /*
	 *
	* user_ico 获取用户在线状态
	*/
    public function get_onlineState($uid, $arr = array())
    {
            $onlinestate = 0;
            $onlinelist=$this->get_onlinelist($arr['product'],$arr['user_type'],$arr['gender'],1);
            $pushonlinelist=$this->get_onlinelist($arr['product'],$arr['user_type'],$arr['gender'],2);

        if (in_array($uid, $onlinelist)) {
                $onlinestate = 1;
            }elseif(in_array($uid, $pushonlinelist)){
            $onlinestate = 2;
        }

        return $onlinestate;
    }

    /**
     * 用用户id生成用户目录
     */
    public function get_userurl($uid, $type)
    {
        return $ret = '/userdata/' . $type . '/' . ($uid % 100) . '/' . ($uid % 200) . '/';
    }

    /**
     * 生成管理员操作日志
     */
    function addlog($id, $uid = 0, $status = 0)
    {
        $adminuser = session("AdminUser");//管理员id
        $data['admin_id'] = $adminuser['id'];
        $data['other_id'] = $id ? $id : '';
        $data['uid'] = $uid ? $uid : '';
        $data['status'] = $status ? $status : '';
        $data['ip'] = get_client_ip();//操作ip
        $data['create_time'] = time();//操作时间
        $data['name'] = strtolower(MODULE_NAME . "/" . CONTROLLER_NAME . "/" . ACTION_NAME);
        $url['name'] = strtolower(MODULE_NAME . "/" . CONTROLLER_NAME . "/" . ACTION_NAME);
        $authrule = M("auth_rule"); // 实例化authrule对象
        $data['auth_rule_title'] = $authrule->where($url)->getField('title');
        $admin_log = M("admin_log"); // 实例化admin_log对象
        $admin_log->add($data);
        return true;
    }


}


?>