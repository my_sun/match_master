<?php
class VerifyController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();

	}
	/*
	 * 评论审核显示
	*/
    public function comment(){
        $this->name = '评论审核'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $dynamiccommentM = new DynamicCommentModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        if (isset($_GET['judge']) && $_GET['judge'] != "") {
            $status1 = $_GET['judge'];
            if ($status1) {
                $Wdata['status']=1;
            }
        }
        $Wdata['status']=2;
        $ret = $dynamiccommentM->getListPage($Wdata, $Page, $PageSize);
        //$status = C("status");
        foreach($ret["list"] as $k=>$v){
            $ret["list"][$k]['content']=urldecode($ret["list"][$k]['content']);
        }

        $this->DataList = $ret["list"];
        //Dump($this->DataList);exit;
        //$this->status = $status;
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }
    /*
	 * 评论二次审核显示
	*/
    public function seccomment(){
        $this->name = '评论二次审核'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $dynamiccommentM = new DynamicCommentModel();
        $Wdata = array();
        if($_GET['type1']&&$_GET['type1']!=""){
            if($_GET['type1']==1){
                $Wdata['status_second']=1;
            }
            if($_GET['type1']==2){
                $Wdata['status_second']=2;
            }
        }
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['status']=1;
        $ret = $dynamiccommentM->getListPage($Wdata, $Page, $PageSize);
        $this->DataList = $ret["list"];
        $this->type1=$_GET['type1'];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }
    /*
     * 评论审核
    */
	public function judge(){
        $Wdata = array();
        if ($_POST["status"]&&$_POST["id"]) {
            $status = $_POST["status"];
            $danamicid =$_POST["danamicid"];
            $id=$_POST["id"];
            $dynamiccommentM = new DynamicCommentModel();
            $path =WR.'/userdata/cache/dynamic/comment/';
            //批量操作更新删除
            if(is_array($_POST["id"])){
                $danamicid = array_values($_POST["danamicid"]);
                $id=array_values($_POST["id"]);
                    if($status==3){
                        for($i=0;$i<count($id);$i++){
                            $res=$dynamiccommentM->delOne(array('id'=>$id[$i]));
                            $cache_name = 'comment_'.$danamicid[$i];
                            F($cache_name,NULL,$path);
                            $this->addlog($id[$i],'',$status);
                        }
                    }else{
                        for($i=0;$i<count($id);$i++){
                            $res=$dynamiccommentM->updateOne(array('id'=>$id[$i]),array('status'=>$status));
                            $cache_name = 'comment_'.$danamicid[$i];
                            F($cache_name,NULL,$path);
                            $this->addlog($id[$i],'',$status);
                        }
                    }

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                //$this->get_user($uid,1);
                exit(json_encode ($reuslt));
            }
            //单个操作更新删除
            if($status==3){
                $res=$dynamiccommentM->delOne(array('id'=>$id));
                $this->addlog($id,'',$status);
            }else{
                $res=$dynamiccommentM->updateOne(array('id'=>$id),array('status'=>$status));
                $this->addlog($id,'',$status);
            }
            $cache_name = 'comment_'.$danamicid;
            if($res){

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                 F($cache_name,NULL,$path);
                exit(json_encode ($reuslt));
            }else{
                F($cache_name,NULL,$path);
                $reuslt = array(
                    'status' =>'2',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
	}
    /*
     * 评论二次审核
    */
    public function secjudge(){
        $Wdata = array();
        if ($_POST["status"]&&$_POST["id"]) {
            $status = $_POST["status"];
            $danamicid =$_POST["danamicid"];
            $id=$_POST["id"];
            $uid=$_POST["uid"];
            $dynamiccommentM = new DynamicCommentModel();
            $path =WR.'/userdata/cache/dynamic/comment/';
            //批量操作更新删除
            if(is_array($_POST["id"])){
                $danamicid = array_values($_POST["danamicid"]);
                $id=array_values($_POST["id"]);
                if($status==3){
                    for($i=0;$i<count($id);$i++){
                        $res=$dynamiccommentM->delOne(array('id'=>$id[$i]));
                        $cache_name = 'comment_'.$danamicid[$i];
                        F($cache_name,NULL,$path);
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }else{
                    for($i=0;$i<count($id);$i++){
                        $res=$dynamiccommentM->updateOne(array('id'=>$id[$i]),array('status_second'=>$status));

                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
            //单个操作更新删除
            if($status==3){
                $res=$dynamiccommentM->delOne(array('id'=>$id));
                $this->addlog($id,$uid,$status);
                $cache_name = 'comment_'.$danamicid;
                F($cache_name,NULL,$path);
            }else{
                $res=$dynamiccommentM->updateOne(array('id'=>$id),array('status_second'=>$status));
                $this->addlog($id,$uid,$status);
            }

            if($res){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }else{
                F($cache_name,NULL,$path);
                $reuslt = array(
                    'status' =>'2',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
    }
    /*
         * 视频认证显示
        */
    public function video(){
        $this->name = '视频认证'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 15;
        $videoM = new VideoModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }

    $Wdata['status']=2;
        $ret = $videoM->getListPage($Wdata, $Page, $PageSize);
        //$status = C("status");

        $this->DataList = $ret["list"];
        $this->vidurl=C ("IMAGEURL");
        //$this->status = $status;
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }
    /*
         * 视频二次认证显示
        */
    public function secvideo(){
        $this->name = '视频认证'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 15;
        $videoM = new VideoModel();
        $Wdata = array();
        if($_GET['type1']&&$_GET['type1']!=""){
            if($_GET['type1']==1){
                $Wdata['status_second']=1;
            }
            if($_GET['type1']==2){
                $Wdata['status_second']=2;
            }
        }
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }

        $Wdata['status']=1;
        $ret = $videoM->getListPage($Wdata, $Page, $PageSize);
        //$status = C("status");

        $this->DataList = $ret["list"];
        $this->type1=$_GET['type1'];
        $this->vidurl=C ("IMAGEURL");
        //$this->status = $status;
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }
    /*
             * 视频认证审核
            */
    public function judgevideo(){
        $Wdata = array();
        if ($_POST["status"]&&$_POST["id"]) {
            $uid = $_POST["uid"];
            $status = $_POST["status"];
            $id=$_POST["id"];
            $videoM = new VideoModel();
            $User_baseM = new UserBaseModel();
            //批量删除修改
            if(is_array($_POST["id"])){
                $id=array_values($_POST["id"]);
                $uid=array_values($_POST["uid"]);
                if($status==3) {
                    for ($i = 0; $i < count($id); $i++) {
                        $res = $videoM->delOne(array('id' => $id[$i]), array('status' => $status));
                        $res1 = $User_baseM->updateOne(array('uid' => $uid[$i]), array('isvideo' => $status));
                        //$this->get_user($uid[$i], 1);
                        $this->set_user_field($uid[$i],'isvideo',$status);
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }else {
                    for ($i = 0; $i < count($id); $i++) {
                        $res = $videoM->updateOne(array('id' => $id[$i]), array('status' => $status));
                        $res1 = $User_baseM->updateOne(array('uid' => $uid[$i]), array('isvideo' => $status));
                       // $this->get_user($uid[$i], 1);
                        $this->set_user_field($uid[$i],'isvideo',$status);
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }

                    $reuslt = array(
                        'status' =>'1',
                        'message' => "更新成功",
                        'data' => '',
                    );

                exit(json_encode ($reuslt));
        }
            //单个操作更新删除
            if($status==3){
                $res=$videoM->delOne(array('id'=>$id));
                $res1=$User_baseM->updateOne(array('uid'=>$uid),array('isvideo'=>$status));
                $this->addlog($id,$uid,$status);
            }else{
                $res=$videoM->updateOne(array('id'=>$id),array('status'=>$status));
                $res1=$User_baseM->updateOne(array('uid'=>$uid),array('isvideo'=>$status));
                $this->addlog($id,$uid,$status);
            }
            if($res&&$res1){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                $this->set_user_field($uid,'isvideo',$status);

                exit(json_encode ($reuslt));
            }else{
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
    }
    /*
             * 视频二次认证审核
            */
    public function secjudgevideo(){
        $Wdata = array();
        if ($_POST["status"]&&$_POST["id"]) {
            $uid = $_POST["uid"];
            $status = $_POST["status"];
            $id=$_POST["id"];
            $videoM = new VideoModel();
            $User_baseM = new UserBaseModel();
            //批量删除修改
            if(is_array($_POST["id"])){
                $id=array_values($_POST["id"]);
                $uid=array_values($_POST["uid"]);
                if($status==3) {
                    for ($i = 0; $i < count($id); $i++) {
                        $res = $videoM->delOne(array('id' => $id[$i]), array('status' => $status));
                        $res1 = $User_baseM->updateOne(array('uid' => $uid[$i]), array('isvideo' => $status));
                        //$this->get_user($uid[$i], 1);
                        $this->set_user_field($uid[$i],'isvideo',$status);
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }else {
                    for ($i = 0; $i < count($id); $i++) {
                        $res = $videoM->updateOne(array('id' => $id[$i]), array('status_second' => $status));
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );

                exit(json_encode ($reuslt));
            }
            //单个操作更新删除
            if($status==3){
                $res=$videoM->delOne(array('id'=>$id));
                $res1=$User_baseM->updateOne(array('uid'=>$uid),array('isvideo'=>$status));
                $this->addlog($id,$uid,$status);
            }else{
                $res=$videoM->updateOne(array('id'=>$id),array('status_second'=>$status));
                $this->addlog($id,$uid,$status);
            }
            if($res&&$res1){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                $this->set_user_field($uid,'isvideo',$status);

                exit(json_encode ($reuslt));
            }else{
                //F($cache_name,NULL,$path);
                $this->set_user_field($uid,'isvideo',$status);
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
    }
    /*
         * 语音认证显示
        */
    public function audio(){
        $this->name = '语音认证'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $AudioM = new AudioModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
    $Wdata['status']=2;

        $ret = $AudioM->getListPage($Wdata, $Page, $PageSize);
        //$status = C("status");

        $this->DataList = $ret["list"];
        $this->audurl=C ("IMAGEURL");
        //$this->status = $status;
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }

    /*
         * 语音二次认证显示
        */
    public function secaudio(){
        $this->name = '语音认证'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $AudioM = new AudioModel();
        $Wdata = array();
        if($_GET['type1']&&$_GET['type1']!=""){
            if($_GET['type1']==1){
                $Wdata['status_second']=1;
            }
            if($_GET['type1']==2){
                $Wdata['status_second']=2;
            }
        }
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['status']=1;

        $ret = $AudioM->getListPage($Wdata, $Page, $PageSize);
        //$status = C("status");

        $this->DataList = $ret["list"];
        $this->type1=$_GET['type1'];
        $this->audurl=C ("IMAGEURL");
        //$this->status = $status;
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }

    /*
             * 语音认证审核
            */
    public function judgeaudio(){
        $Wdata = array();
        if ($_POST["status"]&&$_POST["id"]) {
            $uid = $_POST["uid"];
            $status = $_POST["status"];
            $id=$_POST["id"];
            $AudioM = new AudioModel();
            $User_baseM = new UserBaseModel();
            //批量更新删除
            if(is_array($_POST["id"])){
                $id=array_values($_POST["id"]);
                $uid=array_values($_POST["uid"]);
                if($status==3){
                    for($i=0;$i<count($id);$i++){
                        $res=$AudioM->delOne(array('id'=>$id[$i]));
                        $res1=$User_baseM->updateOne(array('uid'=>$uid[$i]),array('isaudio'=>$status));
                        $this->get_user($uid[$i],1);
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }else{
                    for($i=0;$i<count($id);$i++){
                        $res=$AudioM->updateOne(array('id'=>$id[$i]),array('status'=>$status));
                        $res1=$User_baseM->updateOne(array('uid'=>$uid[$i]),array('isaudio'=>$status));
                        $this->get_user($uid[$i],1);
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
            //单个更新删除
            if($status==3){
                $res=$AudioM->delOne(array('id'=>$id));
                $res1=$User_baseM->updateOne(array('uid'=>$uid),array('isaudio'=>$status));
                $this->set_user_field($uid,'isaudio',$status);
                $this->addlog($id,$uid,$status);
            }else{
                $res=$AudioM->updateOne(array('id'=>$id),array('status'=>$status));
                $res1=$User_baseM->updateOne(array('uid'=>$uid),array('isaudio'=>$status));
                $this->set_user_field($uid,'isaudio',$status);
                $this->addlog($id,$uid,$status);
            }
            if($res&&$res1){
                $this->addlog();
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                $this->get_user($uid,1);
                exit(json_encode ($reuslt));
            }else{
                $this->get_user($uid,1);
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
    }
    /*
             * 语音二次认证审核
            */
    public function secjudgeaudio(){
        $Wdata = array();
        if ($_POST["status"]&&$_POST["id"]) {
            $uid = $_POST["uid"];
            $status = $_POST["status"];
            $id=$_POST["id"];
            $AudioM = new AudioModel();
            $User_baseM = new UserBaseModel();
            //批量更新删除
            if(is_array($_POST["id"])){
                $id=array_values($_POST["id"]);
                $uid=array_values($_POST["uid"]);
                if($status==3){
                    for($i=0;$i<count($id);$i++){
                        $res=$AudioM->delOne(array('id'=>$id[$i]));
                        $res1=$User_baseM->updateOne(array('uid'=>$uid[$i]),array('isaudio'=>$status));
                        $this->get_user($uid[$i],1);
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }else{
                    for($i=0;$i<count($id);$i++){
                        $res=$AudioM->updateOne(array('id'=>$id[$i]),array('status_second'=>$status));
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
            //单个更新删除
            if($status==3){
                $res=$AudioM->delOne(array('id'=>$id));
                $res1=$User_baseM->updateOne(array('uid'=>$uid),array('isaudio'=>$status));
                $this->addlog($id,$uid,$status);
            }else{
                $res=$AudioM->updateOne(array('id'=>$id),array('status_second'=>$status));

                $this->addlog($id,$uid,$status);
            }
            if($res&&$res1){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                $this->get_user($uid,1);
                exit(json_encode ($reuslt));
            }else{
                $this->get_user($uid,1);
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
    }


    /*
             * 用户举报审核
            */

    public function report(){
        $this->name = '用户举报'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize =20;
        $UserReportModelM = new UserReportModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        if (isset($_GET['status']) && $_GET['status'] != "") {
            $status = $_GET['status'];
            $Wdata['status']=$status;
        }
        $Wdata['status']=1;
        $ret = $UserReportModelM->getListPage($Wdata, $Page, $PageSize);
        foreach($ret["listYuanfen"] as $k=>$v){
            $ret["listYuanfen"][$k]["user"]=$this->get_diy_user_field($v["uid"],"nickname");
            $ret["listYuanfen"][$k]["touser"]=$this->get_diy_user_field($v["touid"],"nickname|user_type");//$ret["listYuanfen"][$k]["ids"]= explode("|", $v["contentid"]);
            $ret["listYuanfen"][$k]["usertype"]=$this->get_diy_user_field($v["touid"],"user_type")['user_type'];//$ret["listYuanfen"][$k]["ids"]= explode("|", $v["contentid"]);

            $ids= explode("|", $v["contentid"]);
            foreach($ids as $k1=>$v1){
                $attrvalueM = new AttrValueModel();
                $map=array(
                    'id'=>$v1
                );
                $res=$attrvalueM->getOne($map);
                $ret["listYuanfen"][$k]["ids"][$k1]=$res['name'];
            }
            if(empty($ret["listYuanfen"][$k]["ids"][$k1])){
                foreach($ids as $k2=>$v2){
                    if($v2==1){
                        $ret["listYuanfen"][$k]["ids"][$k1]="垃圾营销";
                    }elseif($v2==2){
                        $ret["listYuanfen"][$k]["ids"][$k1]="不实信息";
                    }elseif($v2==3){
                        $ret["listYuanfen"][$k]["ids"][$k1]="违法信息";
                    }
                    elseif($v2==4){
                        $ret["listYuanfen"][$k]["ids"][$k1]="暴力恐怖";
                    }elseif($v2==5){
                        $ret["listYuanfen"][$k]["ids"][$k1]="淫秽色情";
                    }
                    elseif($v2==6){
                        $ret["listYuanfen"][$k]["ids"][$k1]="恶意人身攻击我";
                    }elseif($v2==7){
                        $ret["listYuanfen"][$k]["ids"][$k1]="政治宗教";
                    }elseif($v2==8){
                        $ret["listYuanfen"][$k]["ids"][$k1]="非法欺诈";
                    }elseif($v2==9){
                        $ret["listYuanfen"][$k]["ids"][$k1]="其他";
                    }

                }
            }
            if($v['status']==1){
                if($v['imgurlids']){
                    $idss = explode("|", $v['imgurlids']);
                    foreach($idss as $k2=>$v2){
                        $url=M('photo_report')->where(array('id'=>$v2))->getField('url');
                        $ret["listYuanfen"][$k]["urls"][$k2]=C('IMAGEURL').$url;
                    }
                }
            }

        }
        $this->DataList = $ret["listYuanfen"];
        //$this->audurl=C ("IMAGEURL");
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->status=$_GET['status']?$_GET['status']:'';
        $this->action = __ACTION__.".html";
        $this->display();
    }
    /*
      * 用户建议
     */
    public function service(){
        $this->name = '用户建议'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize =20;
        $letterserviceM = new LetterServiceModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $letterserviceM->getListPage($Wdata, $Page, $PageSize);
         foreach($ret["list"] as $k=>$v) {
             $ret["list"][$k]["user"] = $this->get_diy_user_field($v["uid"], "nickname");
         }
        $this->DataList = $ret["list"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }

    /*
	 * 内心独白审核
	*/
    public function mood(){
        $this->name = '内心独白审核'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $userextendM = new UserExtendModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['uid'] = array('like', "%" . $kerword . "%");
            }
        }
       $Wdata['mood_status_first']=2;
        $Wdata['mood']=array('neq','');
        $ret = $userextendM->getListPage($Wdata, $Page, $PageSize);
        $this->DataList = $ret["listYuanfen"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }
    /*
    * 内心独白二次审核
   */
    public function secmood(){
        $this->name = '内心独白二次审核'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $userextendM = new UserExtendModel();
        $Wdata = array();
        if($_GET['type1']&&$_GET['type1']!=""){
            if($_GET['type1']==1){
                $Wdata['mood_status_second']=1;
            }
            if($_GET['type1']==2){
                $Wdata['mood_status_second']=2;
            }
        }
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['mood_status_first']=1;
        $Wdata['mood']=array('neq','');
        $ret = $userextendM->getListPage($Wdata, $Page, $PageSize);
        $this->DataList = $ret["listYuanfen"];
        $this->type1=$_GET['type1'];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }
    /*
	 * 内心独白审核
	*/
    public function moodjudge(){
        $Wdata = array();
        if ($_POST["status"]&&$_POST["uid"]) {
            $status = $_POST["status"];
            $uid=$_POST["uid"];
            //批量操作更新删除
            if(is_array($_POST["uid"])){

                $uid=array_values($_POST["uid"]);
                if($status==3){
                    for($i=0;$i<count($uid);$i++){
                        //$data['mood']='';
                        $data['mood_status_first']='3';
                        $res=M('user_extend')->where(array('uid'=>$uid[$i]))->save($data);
                        $res=M('user_extend')->where(array('uid'=>$uid[$i]))->setField('mood','');
                       //更新缓存
                        $this->set_user_field($uid[$i],'mood','');
                        $this->set_user_field($uid[$i],'mood_status_first','3');
                        $this->addlog('',$uid[$i],$status);

                    }
                }else{
                    for($i=0;$i<count($uid);$i++){
                        $data['mood_status_first']='1';
                        $res=M('user_extend')->where(array('uid'=>$uid[$i]))->save($data);
                       //更新缓存状态
                        if($res){
                            $this->set_user_field($uid[$i],'mood_status_first','1');
                        }
                        $this->addlog('',$uid[$i],$status);
                    }
                }

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                //$this->get_user($uid,1);
                exit(json_encode ($reuslt));
            }
            //单个操作更新删除
            if($status==3){
                //$data['mood']='';
                $data['mood_status_first']='3';
                $res=M('user_extend')->where(array('uid'=>$uid))->save($data);
                $res=M('user_extend')->where(array('uid'=>$uid))->setField('mood','');
                //更新缓存
                $this->set_user_field($uid,'mood','');
                $this->set_user_field($uid,'mood_status_first','3');
                $this->addlog('',$uid,$status);
            }else{
                $data['mood_status_first']='1';
                $res=M('user_extend')->where(array('uid'=>$uid))->save($data);
                //更新缓存状态
                if($res){
                    $this->set_user_field($uid,'mood_status_first','1');
                }
                $this->addlog('',$uid,$status);
            }
            if($res){

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                //$this->get_user($uid,1);
                exit(json_encode ($reuslt));
            }else{
                // $this->get_user($uid,1);
                $reuslt = array(
                    'status' =>'2',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
    }

    public function judgemooddynamic(){
         if ($_POST["uid"]) {
             $content=$_POST["mood"];
             $uid = $_POST["uid"];
             $data['mood_status_first'] = '1';
             $res = M('user_extend')->where(array('uid' => $uid))->save($data);
             //更新缓存状态
             if ($res) {
                 $this->set_user_field($uid, 'mood_status_first', '1');
             }
             $this->addlog('', $uid, 1);

             //将内心独白放入动态中
             $dynamic=new DynamicController();
             $data_dynamic=array();
             $data_dynamic=array(
                 'type'=>1,//动态类型为上传了图片
                 'content'=>$content,
                 'url'=>'',
                 'uid'=>$uid,
                 'time'=>time()
             );
             $dynamic->add_dynamic($data_dynamic);





             if($res){

                 $reuslt = array(
                     'status' =>'1',
                     'message' => "更新成功",
                     'data' => '',
                 );
                 //$this->get_user($uid,1);
                 exit(json_encode ($reuslt));
             }else{
                 // $this->get_user($uid,1);
                 $reuslt = array(
                     'status' =>'2',
                     'message' => "已更新过了",
                     'data' => '',
                 );
                 exit(json_encode ($reuslt));
             }

         }


    }
    /*
	 * 内心独白二次审核
	*/
    public function secmoodjudge(){
        $Wdata = array();

        if ($_POST["status"]&&$_POST["uid"]) {
            $status = $_POST["status"];
            $uid=$_POST["uid"];
            //批量操作更新删除
            if(is_array($_POST["uid"])){
                $uid=array_values($_POST["uid"]);
                if($status==3){
                    for($i=0;$i<count($uid);$i++){
                        //$data['mood']='';
                        $data['mood_status_first']='3';
                        $res=M('user_extend')->where(array('uid'=>$uid[$i]))->save($data);
                        //更新缓存
                        $this->set_user_field($uid[$i],'mood_status_first','3');
                        $this->addlog('',$uid[$i],$status);
                    }
                }else{
                    for($i=0;$i<count($uid);$i++){
                        $data['mood_status_second']='1';
                        $res=M('user_extend')->where(array('uid'=>$uid[$i]))->save($data);
                        $this->addlog('',$uid[$i],$status);
                    }
                }
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                //$this->get_user($uid,1);
                exit(json_encode ($reuslt));
            }
            //单个操作更新删除
            if($status==3){
                //$data['mood']='';
                $data['mood_status_first']='3';
                $res=M('user_extend')->where(array('uid'=>$uid))->save($data);
                //更新缓存
                $this->set_user_field($uid,'mood_status_first','3');
                $this->addlog('',$uid,$status);
            }else{
                $data['mood_status_second']='2';
                $res=M('user_extend')->where(array('uid'=>$uid))->save($data);
                $this->addlog('',$uid,$status);
            }
            if($res){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }else{
                $reuslt = array(
                    'status' =>'2',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
    }

    /*
	 * 昵称审核列表
	*/
    public function nicknamelist(){
        $this->name = '昵称审核'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $userbaseM = new UserBaseModel();
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['uid'] = array('like', "%" . $kerword . "%");
            }
        }
        $Wdata['isnickname']=2;
        $Wdata['gender']=2;
        $Wdata['nickname']=array('notlike',array('男生%','女生%'),'AND');
        $ret = $userbaseM->getListPage($Wdata, $Page, $PageSize);
        $this->DataList = $ret["listYuanfen"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }
    /*
    * 昵称审核
   */
    public function nicknamejudge(){
        $Wdata = array();
        if ($_POST["status"]&&$_POST["uid"]) {
            $status = $_POST["status"];
            $uid=$_POST["uid"];
            //批量操作更新删除
            if(is_array($_POST["uid"])){

                $uid=array_values($_POST["uid"]);
                if($status==3){
                    for($i=0;$i<count($uid);$i++){
                        $data['isnickname']='3';
                        $data['nickname']='女生';
                        $res=M('user_base')->where(array('uid'=>$uid[$i]))->save($data);
                        //更新缓存
                        $this->set_user_field($uid[$i],'isnickname','3');
                        $this->set_user_field($uid[$i],'nickname','女生');
                        $this->addlog('',$uid[$i],$status);

                    }
                }else{
                    for($i=0;$i<count($uid);$i++){
                        $data['isnickname']='1';
                        $res=M('user_base')->where(array('uid'=>$uid[$i]))->save($data);
                        //更新缓存状态
                        if($res){
                            $this->set_user_field($uid[$i],'isnickname','1');
                        }
                        $this->addlog('',$uid[$i],$status);
                    }
                }
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
            //单个操作更新删除
            if($status==3){

                $data['isnickname']='3';
                $data['nickname']='女生';
                $res=M('user_base')->where(array('uid'=>$uid))->save($data);
                $this->set_user_field($uid,'isnickname','3');
                $this->set_user_field($uid,'nickname','女生');
                $this->addlog('',$uid,$status);
            }else{
                $data['isnickname']='1';
                $res=M('user_base')->where(array('uid'=>$uid))->save($data);
                //更新缓存状态
                if($res){
                    $this->set_user_field($uid,'isnickname','1');
                }
                $this->addlog('',$uid,$status);
            }
            if($res){

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }else{
                $reuslt = array(
                    'status' =>'2',
                    'message' => "已更新过了",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }
    }
    //封号处理
    public function editusertype(){
        $uid=$_GET['touid'];
        $user_type=$_GET['user_type'];
        $data['user_type'] =$user_type;
        $datasave=M('user_base')->where(array('uid'=>$uid))->save($data);
        if($datasave){
            $this->get_user($uid,1);
        }
        redirect('report.html');
    }
    //删除无效举报,图片
    public function delreport(){
        $id=$_GET['id'];
        $UserReportModelM = new UserReportModel();
        $reportinfo=$UserReportModelM->getOne(array('id'=>$id));
        $imgurlids=$reportinfo['imgurlids'];
        $idss = explode("|", $imgurlids);

        //删除图片
        foreach($idss as $k=>$v){
            $url=M('photo_report')->where(array('id'=>$v))->getField('url');
            //删除oss中储存的url
            DelOss($url);
            M('photo_report')->where(array('id'=>$v))->limit('1')->delete();
        }
        $UserReportModelM->where(array('id'=>$id))->delete();
        redirect('report.html');
    }

}

?>
