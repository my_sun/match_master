<?php


class SendmsgController extends BaseAdmsController
{
    public function __construct()
    {
        parent::__construct();
        $this->basename = '版本过度'; // 进行模板变量赋值
    }

    public function lists()
    {
        $this->name = $this->basename; // 进行模板变量赋值
        $Page = $_POST["Page"] ? $_POST["Page"] : 1;
        $PageSize = 20;

        $Wdata = array();
        if ($_POST['text']) {
            $uids = array();
            $text = $_POST['text'];
            $uids = $_POST['uids'];
            if ($_POST['product'] && $_POST['product'] != "") {
                $Wdata['product'] = $_POST['product'];
            }
            if (isset($_POST['version']) && $_POST['version'] != "") {
                $Wdata['version'] = $_POST['version'];
            }
            $UserBaseM = new UserBaseModel();
            $ret = $UserBaseM->getlistOrder($Wdata, $Page, $PageSize);
            foreach ($ret['listSearch'] as $k => $v) {
                $this->sendsysmsg($v['uid'], $text);
                //true;
            }

            $pagenum = $ret['pageNum'];
            $totalpage = ceil($ret['totalCount'] / $ret['pageSize']);

            if ($totalpage <= $pagenum) {
                $reuslt = array(
                    'status' => '1',
                    'message' => "已成功发送",
                    'data' => array(
                        'pagenum' => $pagenum,
                        'totalpage' => $totalpage,
                        'width' => $pagenum * 100 / $totalpage . "%",//进度条样式
                    ),
                );
                exit(json_encode($reuslt));
            } else {
                $reuslt = array(
                    'status' => '0',
                    'message' => "已发送第" . $pagenum . "页,共有" . $totalpage . "页",
                    'data' => array(
                        'pagenum' => $pagenum + 1,
                        'totalpage' => $totalpage,
                        'width' => $pagenum * 100 / $totalpage . "%",//进度条样式

                    ),
                );
                exit(json_encode($reuslt));
            }
        } else {
            $this->display("lists");
        }
    }

    public function sendmsg()
    {
        $this->name = '发送消息'; // 进行模板变量赋值
        $Wdata = array();
        if ($_POST['text']) {
            $uids = array();
            $text = $_POST['text'];
            $uids = $_POST['uids'];

            if (!is_null($uids)) {
                $uids=str_replace(' ','',$uids);
                $uids=explode('|',$uids);
                foreach ($uids as $k => $v) {
                    $this->sendsysmsg($v, $text);
                }
            } else {
                $reuslt = array(
                    'status' => '1',
                    'message' => "请填写要发送的uid",
                    'data' => array(),
                );
                exit(json_encode($reuslt));
            }

            $reuslt = array(
                'status' => '1',
                'message' => "已成功发送",
                'data' => array(),
            );
            exit(json_encode($reuslt));

        } else {
            $this->display("sendmsg");
        }
    }

}


