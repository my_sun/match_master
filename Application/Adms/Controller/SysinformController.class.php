<?php

/**
 * Created by PhpStorm.
 * User: 亮亮
 * Date: 2018/4/24
 * Time: 10:21
 */
class SysinformController extends BaseAdmsController
{
    public function __construct ()
    {
        parent::__construct ();
        $this->basename = '系统消息通知-'; // 进行模板变量赋值
        $this->Logofile =  "/userdate/admin/product/logo/";
        $this->Logofileurl = C("IMAGEURL").$this->Logofile;
    }

    public function lists ()
    {
        $this->name = $this->basename . '列表'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 12;
        $Attr = M('sys_inform');
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|product'] = array('like', "%" . $kerword . "%");
            }
        }
        $ret = $Attr->where ($Wdata)->select();
        $this->msgurl=C ("IMAGEURL");
        $this->DataList= $ret;
        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display ();
    }
    public function add(){
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        if($_POST){
            $err=0;
            $msg="";
            if(!$_POST["content"]){
                $err = 3;
                $msg.="内容不能为空<br />";
            }
            if($err==0){
                $Attr =M('sys_inform');
                $map = array();
                    $map["version"] = $_POST["version"];
                    $map["product"] = $_POST["product"];
                    $map["isopen"] = $_POST["isopen"];
                    $map["content"] = $_POST["content"];
                    $map["overtime"] = $_POST["overtime"];
                    $map["informtime"] = $_POST["informtime"];
                    $map["user_type"] = $_POST["user_type"];
                    $map["gender"] = $_POST["gender"];
                    $map["vipgrade"] = $_POST["vipgrade"];
                    $Attr->data($map)->add();
                    $this->tip = $_POST["product"]."通知消息已添加";
                    header('Location:applist.html');

            }else{
                $this->tip = $msg;
            }
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }
    /*
	 * 删除
	 */
    public function del(){

        $code =$_REQUEST["id"];
        if($code){
            $AttrValueM = M('sys_inform');
            $map = array();
            $map["id"] = $code;
            $AttrValueM->where($map)->delete();
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }

    //修改
    public function edit(){
        $this->name = $this->basename.'修改平台'; // 进行模板变量赋值
        $GiftM  = M('sys_inform');
        $id = $_REQUEST['id'];
        $err = 0;
        if($id){
            if($_POST){
                    if($_POST["id"]&&$err==0){
                        $where=array();
                        $map = array();
                        $where['id']=$id;
                        $map["version"] =$_POST["version"];
                        $map["product"] =$_POST["product"];
                        $map["isopen"] =$_POST["isopen"];
                        $map["content"] =$_POST["content"];
                        $map["overtime"] =$_POST["overtime"];
                        $map["informtime"] =$_POST["informtime"];
                        $map["user_type"] =$_POST["user_type"];
                        $map["gender"] =$_POST["gender"];
                        $map["vipgrade"] =$_POST["vipgrade"];
                        $GiftM->where($where)->save($map);
                    }
                    header('Location:applist.html');
                    $this->tip = "已保存！";
                }
            $where = array();
            $where["id"] = $id;
            $Info= $GiftM->where($where)->find();
            $this->Info = $Info;
        }else{
            echo "error1";exit;
        }
        $this->id = $id;
        $this->action =  __ACTION__.".html";
        $this->display("add");
    }
    //截取中文字符串
    private function mysubstr($str, $start, $len) {
        $tmpstr = "";
        $strlen = $start + $len;
        for($i = 0; $i < $strlen; $i++) {
            if(ord(substr($str, $i, 1)) > 0xa0) {
                $tmpstr .= substr($str, $i, 2);
                $i++;
            } else
                $tmpstr .= substr($str, $i, 1);
        }
        return $tmpstr;
    }
    private function getcountrylist(){
        $coun = new CountryModel();
        return $coun->getList(array("status"=>1));
    }
    private function checkproduct($product){
        $pingtai = substr($product, 2 , 1);
        if(!in_array($pingtai, array(1,2))){
            return false;
        }
        if(strlen($product)!=5){
            return false;
        }
        return true;
    }
    //销售状态修改
    public function updatestatus()
    {
        $GiftM = new GiftModel();
        if ($_POST) {
            if ($_POST["gid"]) {
                $map=array();
                $map['status']=$_POST["status"];
                $return=array();
                $return['id']=$_POST["gid"];
                 $result=$GiftM->updateAll ($map,$return);
                 if($result){
                     $content= array(
                         'status' => 1,
                         'gstatus'=>$map['status'],
                         'message' => '新增成功'
                     );
                     exit(json_encode ($content));
                 }else{
                     $content= array(
                         'status' => 2,
                         'message' => '修改失败！'
                     );
                     exit(json_encode ($content));
                 }
                //return json_encode ($map);
            }
        }
    }

    //图片上传方法
    protected function _upload($savePath) {
        import("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir($savePath);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 3292200;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr,apk');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg();
            //Push_data($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            //$uploadList = $uploadList[0];
            //$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
            //$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

            return $uploadList;
            //import("Extend.Library.ORG.Util.Image");
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            //Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
            //$_POST['image'] = $uploadList[0]['savename'];
        }

    }
    //图片上传封装
    public function _imgupload(){
        $filetype = "image";
        $saveUrl = $this->msgfileurl . $filetype . "/";
        $savePath = WR . $saveUrl;
        //echo $savePath;exit;
        $uploadList = $this->_upload ($savePath);
        if (!empty($uploadList)) {
            // $UserPhotoM = new PhotoModel();
            $PutOssarr = array();
            $returnurlarr = array();
            foreach ($uploadList as $k => $v) {
                $PutOssarr[] = $saveUrl . $v['savename'];
                $returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
            }
            PutOss ($PutOssarr);
            // print_r ($PutOssarr);exit;
            $return = array();
            $return["data"]["url"] = $PutOssarr[0];
            return $PutOssarr[0];
        }
    }

    //是否加入随机目录
    public function isadd(){
        $id=$_POST['id'];
        $type=$_POST['type'];
        if($type==1){
            //加入
            $product=M('products')->where(array('id'=>$id))->save(array('type'=>1));
            if($product){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已加入",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }elseif($type==0){
            //取消加入
            $product=M('products')->where(array('id'=>$id))->save(array('type'=>0));

            if($product){
                $reuslt = array(
                    'status' =>'0',
                    'message' => "已取消加入",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }

    }
    //删除已上传的apk
    public function delApk(){

        if($_POST['product']){
            $product=$_POST['product'];
            $filename = WR."/userdata/apk/".$product.".zip";
            $command = "/bin/rm -rf " . $filename ." > /dev/null &";
            system($command);
            $reuslt = array(
                'status' =>'1',
                'message' => "已删除",
                'data' => '',
            );
            exit(json_encode ($reuslt));
        }else{
            $reuslt = array(
                'status' =>'0',
                'message' => "删除失败，请重新删除",
                'data' => '',
            );
            exit(json_encode ($reuslt));
        }
    }

    //版本更新管理
    public function version_update(){
        $versiondata=M('version_update');
        $alldata=$versiondata->select();
        $this->DataList= $alldata;
        $this->get = $_GET;

        $this->display ();
    }
    //是否开启更新
    public function versionisadd(){
        $id=$_POST['id'];
        $isopen=$_POST['isopen'];
        if($isopen==1){
            //加入
            $versiondata=M('version_update')->where(array('id'=>$id))->save(array('isopen'=>1));
            if($versiondata){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已开启更新",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }elseif($isopen==0){
            //取消加入
            $versiondata=M('version_update')->where(array('id'=>$id))->save(array('isopen'=>0));

            if($versiondata){
                $reuslt = array(
                    'status' =>'0',
                    'message' => "已关闭更新",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }

    }
//版本更新修改
    //修改
    public function versionedit(){

        $id = $_REQUEST['id'];
        $err = 0;
        if($id){
           if($_POST){
                if($_POST["id"]){
                    $where=array();
                    $map = array();
                    $where['id']=$id;
                    $map["product"] =$_POST["product"];
                    $map["version"] =$_POST["version"];
                    $map["isopen"] =$_POST["isopen"];
                    $map["method"] =$_POST["method"];
                    $map["promptword"] =$_POST["promptword"];
                    M('version_update')->where(array('id'=>$_POST["id"]))->save($map);
                }
                    header('Location:version_update.html');
                    $this->tip = "已保存！";
            }
            $where = array();
            $where["id"] = $id;
            $Info= M('version_update')->where(array('id'=>$id))->find();
            $this->Info = $Info;
        }else{
            echo "error1";exit;
        }
        $this->id = $id;
        $this->action =  "versionedit.html";
        $this->display("versionadd");
    }
    //添加
    public function versionadd(){
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        if($_POST){
            $err=0;
            $msg="";

            if(!$_POST["product"]){
                $err = 2;
                $msg.="平台号不能为空<br />";
            }
            if(!$_POST["version"]){
                $err = 3;
                $msg.="版本号不能为空<br />";
            }
            if($err==0){

                $map = array();
                $map["product"] = $_POST["product"];
                $AttrValue = M('version_update')->where($map)->find();
                if($AttrValue){
                    $this->tip = "平台号已经存在！";
                }else{
                    $map["product"] =$_POST["product"];
                    $map["version"] =$_POST["version"];
                    $map["isopen"] =$_POST["isopen"];
                    $map["method"] =$_POST["method"];
                    $map["promptword"] =$_POST["promptword"];
                    M('version_update')->data($map)->add();
                    $this->tip = $_POST["product"]."已添加";
                    header('Location:version_update.html');
                }

            }else{
                $this->tip = $msg;
            }
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }


    //paypal开关管理
    //版本更新管理
    public function paypal_switch(){
        $paypalndata=M('paypal_switch');
        $alldata=$paypalndata->select();
        $this->DataList= $alldata;
        $this->get = $_GET;
        $this->display ();
    }
    //是否开启更新
    public function paypalswitchoutadd(){
        $id=$_POST['id'];
        $isopenout=$_POST['isopenout'];
        if($isopenout==1){
            //加入
            $versiondata=M('paypal_switch')->where(array('id'=>$id))->save(array('isopenout'=>1));
            if($versiondata){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已开启更新",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }elseif($isopenout==0){
            //取消加入
            $versiondata=M('paypal_switch')->where(array('id'=>$id))->save(array('isopenout'=>0));

            if($versiondata){
                $reuslt = array(
                    'status' =>'0',
                    'message' => "已关闭更新",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }

    }
    //
    //是否开启更新
    public function paypalswitchinadd(){
        $id=$_POST['id'];
        $isopenin=$_POST['isopenin'];
        if($isopenin==1){
            //加入
            $versiondata=M('paypal_switch')->where(array('id'=>$id))->save(array('isopenin'=>1));
            if($versiondata){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已开启更新",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }elseif($isopenin==0){
            //取消加入
            $versiondata=M('paypal_switch')->where(array('id'=>$id))->save(array('isopenin'=>0));

            if($versiondata){
                $reuslt = array(
                    'status' =>'0',
                    'message' => "已关闭更新",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
        }

    }

    //修改
    public function paypalswitchdit(){

        $id = $_REQUEST['id'];
        $err = 0;
        if($id){
            if($_POST){
                if($_POST["id"]){
                    $where=array();
                    $map = array();
                    $where['id']=$id;
                    $map["product"] =$_POST["product"];
                    $map["version"] =$_POST["version"];
                    $map["isopenout"] =$_POST["isopenout"];
                    $map["isopenin"] =$_POST["isopenin"];
                    M('paypal_switch')->where(array('id'=>$_POST["id"]))->save($map);
                }
                header('Location:paypal_switch.html');
                $this->tip = "已保存！";
            }
            $where = array();
            $where["id"] = $id;
            $Info= M('paypal_switch')->where(array('id'=>$id))->find();
            $this->Info = $Info;
        }else{
            echo "error1";exit;
        }
        $this->id = $id;
        $this->action =  "paypalswitchdit.html";
        $this->display("paypalswitchadd");
    }

    //添加
    public function paypalswitchadd(){
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        if($_POST){
            $err=0;
            $msg="";

            if(!$_POST["product"]){
                $err = 2;
                $msg.="平台号不能为空<br />";
            }
            if(!$_POST["version"]){
                $err = 3;
                $msg.="版本号不能为空<br />";
            }
            if($err==0){

                $map = array();
                $map["product"] = $_POST["product"];
                $AttrValue = M('paypal_switch')->where($map)->find();
                if($AttrValue){
                    $this->tip = "平台号已经存在！";
                }else{
                    $map["product"] =$_POST["product"];
                    $map["version"] =$_POST["version"];
                    $map["isopen"] =$_POST["isopen"];
                    $map["method"] =$_POST["method"];
                    M('paypal_switch')->data($map)->add();
                    $this->tip = $_POST["product"]."已添加";
                    header('Location:paypal_switch.html');
                }

            }else{
                $this->tip = $msg;
            }
        }
        $this->display();
    }
}


