<?php
use Think\Controller;
class ShoppingmallgoodsController extends BaseAdmsController
{
    public function __construct()
    {
        parent::__construct();
        $this->basename = '商品-'; // 进行模板变量赋值
        $this->cachepath = WR . '/userdata/cache/face/';
        $this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";

    }
    
    //商品列表
    public function goodslist()
    {
        $this->name = $this->basename . '列表'; // 进行模板变量赋值

        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 12;
        $Shoppingmallgoods = new ShoppingmallgoodsModel();
        //$status = include WR . "/userdata/publicvar/status.php";
        //$this->switch_type = $status["gift"]["type"];
        $Wdata = array();
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['goods_id|goods_name'] = array('like', "%" . $kerword . "%");
            }
        }
        if (isset($_GET['classification_id']) && $_GET['classification_id'] != "") {
            $Wdata['classification_id'] = $_GET['classification_id'];
            
        }
        if(isset($_GET['dateStart'])&$_GET['dateStart']!=""&$_GET['dateEnd']==""){
            $regtime=$_GET['dateStart'];
            $Wdata['price']=$regtime;
        }
        if(isset($_GET['dateEnd'])&&$_GET['dateEnd']!=""&$_GET['dateStart']==""){
            $regtime=$_GET['dateEnd'];
            $Wdata['price']=$regtime;
        }
        if(isset($_GET['dateEnd'])&&$_GET['dateEnd']!=""&$_GET['dateStart']!=""){
            $regtime=$_GET['dateEnd'];
            $regstarttime=$_GET['dateStart'];
            $Wdata['price']=array('between',array($regstarttime,$regtime));;
        }

        $Classification = new ClassificationModel();
        $ret = $Shoppingmallgoods->getListPage ($Wdata, $Page, $PageSize);
        foreach ($ret["list"] as $key => $value) {
            
            $where["id"] = $value['classification_id'];
            $temp= $Classification->getOne($where);
            // dump();die;
            $ret["list"][$key]['classification_id'] = $temp['name'];
        }
        // dump($ret["list"]);die;
        $this->msgurl=C ("IMAGEURL");
        $this->DataList= $ret["list"];
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $Classification = new ClassificationModel();
        $temp= $Classification->getAll();

        $Info = array();
        
        foreach($temp as $k=>$v){
            $Info[$k]=$v;
        }

        $this->Info = $Info;

        $this->get = $_GET;
        $this->action = __ACTION__ . ".html";
        $this->display ();
    }

    //商品添加
    public function add()
    {
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        $status = C("status");
        //$this->usertypestatus = $status["gift"]["user_type"];
        if($_POST){
            if(!empty($_FILES)) {
                $PutOssarr=$this->_imgupload();
            }
            if($_POST["goods_name"]&&$_POST["price"]&&$_POST["classification_id"]&&$_POST["days"]&&$_POST["paykey"]){
                $Shoppingmallgoods = new ShoppingmallgoodsModel();
                $map = array();
                $map["goods_name"] = $_POST["goods_name"];
                $map["classification_id"] = $_POST["classification_id"];
                $map["price"] = $_POST["price"];
                $map["type"] = $_POST["type"];
                $map["grade"] = $_POST["grade"];
                $map["paykey"] = $_POST["paykey"];
                $map["days"] = $_POST["days"];
                $map["images"] =  $PutOssarr;
                $Shoppingmallgoods->addOne($map);

                //连接redis
                $redis = new RedisModel();
                //设置key
                $key = "GOODSLISTSS".$map["classification_id"];
                // 删除缓存
                $redis->delete($key);
                $this->tip = $_POST["goods_name"]."已添加";

                header('Location:goodslist.html');

            }else{
                $this->tip = "不能为空！";
            }
        }
        $Classification = new ClassificationModel();
        $temp= $Classification->getAll();

        $Info = array();
        
        foreach($temp as $k=>$v){
            $Info[$k]=$v;
        }

        $this->Info = $Info;
        $this->msgurl=C ("IMAGEURL");
        $this->action =  __ACTION__.".html";
        $this->display();
    }

    //商品状态
    public function updatestatus()
    {
        $Shoppingmallgoods = new ShoppingmallgoodsModel();
        if ($_POST) {
            if ($_POST["gid"]) {
                $map=array();
                $map['status']=$_POST["status"];
                $return=array();
                $return['goods_id']=$_POST["gid"];
                
                $result=$Shoppingmallgoods->updateAll ($map,$return);
                $temp= $Shoppingmallgoods->getOne($return);
                 //连接redis
                    $redis = new RedisModel();
                    //设置key
                    $key = "GOODSLISTSS".$temp['classification_id'];
                    // 删除缓存
                    $redis->delete($key);
                 if($result){
                   
                     $content= array(
                         'status' => 1,
                         'gstatus'=>$map['status'],
                         'message' => '新增成功'
                     );
                     exit(json_encode ($content));
                 }else{
                     $content= array(
                         'status' => 2,
                         'message' => '修改失败！'
                     );
                     exit(json_encode ($content));
                 }
                //return json_encode ($map);
            }
        }
    }

    public function edit(){

        $this->name = $this->basename.'修改分类'; // 进行模板变量赋值
        $Shoppingmallgoods  = new ShoppingmallgoodsModel();
        $id = $_REQUEST['id'];
        if($id){
            if($_POST){
                if($_POST["id"]){
                    if(!empty($_FILES)) {
                        $PutOssarr=$this->_imgupload();
                        // dump($PutOssarr);die;
                        if(!$PutOssarr){
                            $PutOssarr=$_POST["images"];
                        }/*else{
                            DelOss($_POST["tmpimg"]);

                        }*/

                    }else{
                        $PutOssarr=$_POST["images"];
                    }
                    $map = array();
                    $map["goods_name"] = $_POST["goods_name"];
                    $map["classification_id"] = $_POST["classification_id"];
                    $map["price"] = $_POST["price"];
                    $map["type"] = $_POST["type"];
                    $map["grade"] = $_POST["grade"];
                    $map["paykey"] = $_POST["paykey"];
                    $map["days"] = $_POST["days"];
                    $map["images"] =  $PutOssarr;
                    // dump($_POST["id"]);die;
                    $this->msgurl=C ("IMAGEURL");
                    $tempT = $Shoppingmallgoods->getOne($_POST["id"]);
                    $where["goods_id"] = $_POST["id"];
                    if($tempT){
                        $Shoppingmallgoods->updateOne($where,$map);
                         $temp= $Shoppingmallgoods->getOne($where);
                        //连接redis
                        $redis = new RedisModel();
                        //设置key
                        $key = "GOODSLISTSS".$temp['classification_id'];
                        // 删除缓存
                        $redis->delete($key);
                    }else{
                        $this->tip = "错误！";
                    }
                }
                header('Location:goodslist.html');
                $this->tip = "已保存！";

            }
            $Classification = new ClassificationModel();
            $temp= $Classification->getAll();

            $data = array();
            
            foreach($temp as $k=>$v){
                $data[$k]=$v;
            }
            $where = array();
            $where["goods_id"] = $id;
            $temps= $Shoppingmallgoods->getList($where);

            $Info = array();

            foreach($temps[0] as $k=>$v){
                $Info[$k]=$v;
            }
            $this->Info = $Info;
            $this->data = $data;
            $this->msgurl=C ("IMAGEURL");
            // Dump( $TranslateList);exit;
            //$this->code = $code;
        }else{
            echo "error";exit;
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }

    /*
     * 删除
     */
    public function del(){

        $id =$_REQUEST["id"];
        if($id){
            $Classification = new ShoppingmallgoodsModel();
            $map = array();
            $map["goods_id"] = $id;
            $Classification->delOne($map);
             $temp= $Shoppingmallgoods->getOne($map);
            //连接redis
            $redis = new RedisModel();
            //设置key
            $key = "GOODSLISTSS".$temp['classification_id'];
            // 删除缓存
            $redis->delete($key);
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }

    //图片上传方法
    protected function _upload($savePath) {
        import("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir($savePath);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize =10485760;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg();
            if($_REQUEST['ptesst']==1){
                dump($return);
            }
            //Push_data($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            //$uploadList = $uploadList[0];
            //$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
            //$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

            return $uploadList;
            //import("Extend.Library.ORG.Util.Image");
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            //Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
            //$_POST['image'] = $uploadList[0]['savename'];
        }

    }
    //图片上传封装
    public function _imgupload(){
        $filetype = "images";
        $saveUrl = $this->msgfileurl . $filetype . "/";
        $savePath = WR . $saveUrl;
        //echo $savePath;exit;
        $uploadList = $this->_upload ($savePath);
        if (!empty($uploadList)) {
            // $UserPhotoM = new PhotoModel();
            $PutOssarr = array();
            $returnurlarr = array();
            foreach ($uploadList as $k => $v) {
                $PutOssarr[] = $saveUrl . $v['savename'];
                $returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
            }
            PutOss ($PutOssarr);
            // print_r ($PutOssarr);exit;
            $return = array();
            $return["data"]["url"] = $PutOssarr[0];
            return $PutOssarr[0];
        }
    }

}


