<?php
class PhotoController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		
	}
	/*
	 * 照片审核 /Adm/deleteImg
	*/
	public function lists(){
		$this->name = '照片审核列表'; // 进行模板变量赋值
		$Page = $_GET["Page"] ? $_GET["Page"] : 1;
		$PageSize = 20;
		$photoM = new PhotoModel();
		$Wdata = array();
        if($_GET['type']&&$_GET['type']!=""){
           switch($_GET['type']) {
               case 1:
               case 2:
               $Wdata['type']=2;
                   break;
               case 3:
               case 4:
               $Wdata['type']=1;
                   break;
               case "":
                   $Wdata=array();
                   break;
           }
        }
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }

        $Wdata['status']=2;
        $ret = $photoM->getListPage($Wdata, $Page, $PageSize);
        $sexboy=array();
        $sexgirl=array();
        foreach($ret["list"] as $k1=>$v1){
           $sex=$this->get_diy_user_field($v1['uid'],'gender');
           if($sex['gender']==1){
               $sexboy[]=$ret["list"][$k1];
           }else{
               $sexgirl[]=$ret["list"][$k1];
           }
        }
        if($_GET['type']==2||$_GET['type']==4){
            $ret["list"]=$sexgirl;
        }elseif($_GET['type']==1||$_GET['type']==3){
            $ret["list"]=$sexboy;
        }
		$this->DataList = $ret["list"];
        $this->type=$_GET['type'];
        $this->msgurl=C ("IMAGEURL");
		//Dump($this->DataList);exit;
		//$this->status = $status;
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->action = __ACTION__.".html";
		$this->display();
	}
    /*
     * 照片审核二次审核/Adm/deleteImg
    */
    public function seclists(){
        $this->name = '照片二次审核'; // 进行模板变量赋值
        $Page = $_GET["Page"] ? $_GET["Page"] : 1;
        $PageSize = 20;
        $photoM = new PhotoModel();
        $Wdata = array();
        if($_GET['type']&&$_GET['type']!=""){
            switch($_GET['type']) {
                case 1:
                case 2:
                    $Wdata['type']=2;
                    break;
                case 3:
                case 4:
                    $Wdata['type']=1;
                    break;
                case "":
                    $Wdata=array();
                    break;
            }
        }
        if($_GET['type1']&&$_GET['type1']!=""){
            if($_GET['type1']==1){
                $Wdata['status_second']=1;
            }
            if($_GET['type1']==2){
                $Wdata['status_second']=2;
            }
        }
        if (isset($_GET['kerword']) && $_GET['kerword'] != "") {
            $kerword = $_GET['kerword'];
            if ($kerword) {
                $Wdata['id|uid'] = array('like', "%" . $kerword . "%");
            }
        }

        $Wdata['status']=1;
        $ret = $photoM->getListPage($Wdata, $Page, $PageSize);
        $sexboy=array();
        $sexgirl=array();
        foreach($ret["list"] as $k1=>$v1){
            $sex=$this->get_diy_user_field($v1['uid'],'gender');
            if($sex['gender']==1){
                $sexboy[]=$ret["list"][$k1];
            }else{
                $sexgirl[]=$ret["list"][$k1];
            }
        }
        if($_GET['type']==2||$_GET['type']==4){
            $ret["list"]=$sexgirl;
        }elseif($_GET['type']==1||$_GET['type']==3){
            $ret["list"]=$sexboy;
        }
        $this->DataList = $ret["list"];
        $this->type=$_GET['type'];
        $this->type1=$_GET['type1'];
        $this->msgurl=C ("IMAGEURL");
        //Dump($this->DataList);exit;
        //$this->status = $status;
        $all_page = ceil($ret['totalCount']/$PageSize);
        $ret["all_page"] = $all_page;
        $this->Pages = $this->GetPages($ret);
        $this->get = $_GET;
        $this->action = __ACTION__.".html";
        $this->display();
    }
    /*
     * 照片审核 /Adms/judge
    */
	public function judge()
{
    $Wdata = array();
    if ($_POST["status"] && $_POST["id"]) {
        $uid = $_POST["uid"];
        $status = $_POST["status"];
        $type=$_POST['type'];
        $id = $_POST["id"];
        $photoM = new PhotoModel();
        //$product=M('user_base')->where(array('uid'=>$uid))->getField('product');
        //批量操作更新删除
        if (is_array ($_POST["id"])) {
            $id = array_values ($_POST["id"]);
            $uid = array_values ($_POST["uid"]);
            $url = array_values ($_POST["url"]);
            if($status==3){
                for ($i = 0; $i < count ($id); $i++)
                {
                    $res = $photoM->delOne (array('id' => $id[$i]));
                    $this->get_user_photo ($uid[$i], 1);
                    $this->get_user_photo_all ($uid[$i], 1);
                    $this->get_user ($uid[$i], 1);
                    DelOss($url[$i]);
                    if($res){
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }
            }else{
                for ($i = 0; $i < count ($id); $i++)
                {
                    $res = $photoM->updateOne (array('id' => $id[$i]), array('status' => $status));
                    $this->get_user_ico($uid[$i],1);
                    $this->get_user_ico_all($uid[$i],1);
                    $this->get_user_photo ($uid[$i], 1);
                    $this->get_user_photo_all ($uid[$i], 1);
                    $this->get_user ($uid[$i], 1);
                    if($res){
                        $this->addlog($id[$i],$uid[$i],$status);
                    }
                }
            }

            //uid去重
            $uid1=array_unique($uid);
            //每个用户通知一次
            foreach($uid1 as $v){
                if($status==1){
                    $this->sendsysmsg($v, "該照片已通過審核");
                }else{
                    $this->sendsysmsg($v, "該照片未通過審核");
                }
            }

            $reuslt = array(
                'status' => '1',
                'message' => "更新成功",
                'data' => '',
            );

            exit(json_encode ($reuslt));
        }else{

            //单个操作更新删除
            $url1=$photoM->getOne(array('id'=>$id));
            $map['id']=array('neq',$id);//id不等于5
            $map['time']=$url1['time'];
            $photospic=$photoM->where($map)->find();
            if($status==3){
                DelOss($url1['url']);
                if(!empty($photospic)){
                    $res=$photoM->delOne(array('id'=>$photospic['id']));
                }
                $res=$photoM->delOne(array('id'=>$id));
                $this->addlog($id,$uid,$status);
            }else{
                $res=$photoM->updateOne(array('id'=>$id),array('status'=>$status));
               if(!empty($photospic)){
                   $res=$photoM->updateOne(array('id'=>$photospic['id']),array('status'=>$status));
               }
                $this->addlog($id,$uid,$status);
            }
            if($res){

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' =>$photospic['id'],
                );
                if($status==1){
                    $this->sendsysmsg($uid, "該照片已通過審核");
                }else{
                    $this->sendsysmsg($uid, "該照片未通過審核");
                }
                $this->get_user_photo($uid,1);
                $this->get_user_ico($uid,1);
                $this->get_user_ico_all($uid,1);
                $this->get_user_photo_all($uid,1);
                $this->get_user($uid,1);
                exit(json_encode ($reuslt));
            }else{
                $this->get_user_photo($uid,1);
                $this->get_user_ico($uid,1);
                $this->get_user_ico_all($uid,1);
                $this->get_user_photo_all($uid,1);
                 $this->get_user($uid,1);
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已更新过了",
                    'data' => '',
                );
                if($status==1){
                    $this->sendsysmsg($uid, "該照片已通過審核");
                }else{
                    $this->sendsysmsg($uid, "該照片未通過審核");
                }
                exit(json_encode ($reuslt));
            }

        }

    }

}

        //照片通过且发表动态
    public function judgeDynamic()
    {
        $Wdata = array();
        if ($_POST["status"] && $_POST["id"]) {
            $uid = $_POST["uid"];
            $status = $_POST["status"];
            $id = $_POST["id"];
            $photo_url=$_POST["photo_url"];
            $photo_time=$_POST["photo_time"];
            $photoM = new PhotoModel();
            $userinfo=$this->get_user($uid);
            //$photo_url=M('photo')->where(array('id'=>$id))->getField('url');
            //单个通过操作
                $res=$photoM->updateOne(array('id'=>$id),array('status'=>$status));
                $this->get_user_photo ($uid, 1);
                $this->get_user_photo_all ($uid, 1);
            $this->get_user_ico($uid,1);
            $this->get_user_ico_all($uid,1);
            $this->get_user($uid,1);
                $this->addlog($id,$uid,$status);
                //将该图片发表到动态
            $dynamic=new DynamicController();
            $data_dynamic=array(
                'type'=>2,//动态类型为上传了图片
                'content'=>'',
                'url'=>$photo_url,
                'uid'=>$uid,
                'time'=>$photo_time
            );
            $dynamic->add_dynamic($data_dynamic);


            if($res){

                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                if($status==1){
                    $this->sendsysmsg($uid, "該照片已通過審核");
                }else{
                    $this->sendsysmsg($uid, "該照片未通過審核");
                }
                $this->get_user_ico($uid,1);
                $this->get_user_ico_all($uid,1);
                $this->get_user_photo($uid,1);
                $this->get_user_photo_all($uid,1);
                $this->get_user($uid,1);
                exit(json_encode ($reuslt));
            }else{
                $this->get_user_ico($uid,1);
                $this->get_user_ico_all($uid,1);
                $this->get_user_photo($uid,1);
                $this->get_user_photo_all($uid,1);
                $this->get_user($uid,1);
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已更新过了",
                    'data' => '',
                );


                if($status==1){
                    $this->sendsysmsg($uid, "該照片已通過審核");
                }else{
                    $this->sendsysmsg($uid, "該照片未通過審核");
                }
                exit(json_encode ($reuslt));
            }
        }

    }
//照片二次审核
    public function secjudge()
    {
        $Wdata = array();
        if ($_POST["status"] && $_POST["id"]) {
            $uid = $_POST["uid"];
            $status = $_POST["status"];
            $id = $_POST["id"];
            $photoM = new PhotoModel();
            //批量操作更新删除
            if (is_array ($_POST["id"])) {
                $id = array_values ($_POST["id"]);
                $uid = array_values ($_POST["uid"]);
                $url = array_values ($_POST["url"]);
                if($status==3){
                    for ($i = 0; $i < count ($id); $i++)
                    {
                        $res = $photoM->delOne (array('id' => $id[$i]));
                        $this->get_user_photo ($uid[$i], 1);
                        $this->get_user_photo_all ($uid[$i], 1);
                        $this->get_user ($uid[$i], 1);
                        DelOss($url[$i]);
                        if($res){
                            $this->addlog($id[$i],$uid[$i],$status);
                        }
                    }
                }else{
                    for ($i = 0; $i < count ($id); $i++)
                    {
                        $res = $photoM->updateOne (array('id' => $id[$i]), array('status_second' => $status));
                        if($res){
                            $this->addlog($id[$i],$uid[$i],$status);
                        }
                    }
                }
                //uid去重
                $uid1=array_unique($uid);
                //每个用户通知一次
                foreach($uid1 as $v){
                    if($status==3){
                        $this->sendsysmsg($v, "該照片未通過審核");
                    }
                }
                $reuslt = array(
                    'status' => '1',
                    'message' => "更新成功",
                    'data' => '',
                );
                exit(json_encode ($reuslt));
            }
            //单个操作更新删除
            if($status==3){
                $url1=$photoM->getOne(array('id'=>$id));
                DelOss($url1['url']);
                $res=$photoM->delOne(array('id'=>$id));
                $this->get_user_photo($uid,1);
                $this->get_user_photo_all($uid,1);
                //$this->get_user($uid,1);
                $this->addlog($id,$uid,$status);
            }else{
                $res=$photoM->updateOne(array('id'=>$id),array('status_second'=>$status));
                $this->addlog($id,$uid,$status);
            }
            if($res){
                $reuslt = array(
                    'status' =>'1',
                    'message' => "更新成功",
                    'data' => '',
                );
                if($status==3){
                    $this->sendsysmsg($uid, "該照片未通過審核");
                }
                exit(json_encode ($reuslt));
            }else{
                $this->get_user_photo($uid,1);
                $this->get_user_photo_all($uid,1);
                //$this->get_user($uid,1);
                $reuslt = array(
                    'status' =>'1',
                    'message' => "已更新过了",
                    'data' => '',
                );
                if($status==3){
                    $this->sendsysmsg($uid, "該照片未通過審核");
                }
                exit(json_encode ($reuslt));
            }
        }
    }
	public function Ajax_update(){
		$return['code'] = ERRORCODE_201;
		$return['message'] = "参数错误";
		$type = $_POST["type"];
		switch ($type){
			case "shenhe":
				if($_POST["id"]){
					$photoM = new PhotoModel();
					$ret = $photoM->updateOne(array("id"=>$_POST["id"]),array("status"=>1));
					$return = array();
				}
				break;
			case "shanchu":
				if($_POST["id"]){
					$photoM = new PhotoModel();
					$ret = $photoM->updateOne(array("id"=>$_POST["id"]),array("status"=>3));
					$return = array();
				}
				break;
		}
		
		
		Push_data($return);
	}
	
}

?>
