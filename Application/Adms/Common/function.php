<?php
/**
 * 分行读取文件内容
 *
 * @param $file 文件路径
 * @param $line 行数
 * @param $length 指定行返回内容长度
 */
function read_file_ByLine($filePath){
    $return = array(); // 初始化返回
    $file = fopen($filePath, "r");
    //输出文本中所有的行，直到文件结束为止。
    while(! feof($file))
    {
        $tempcontent = fgets($file);
        $temp = trim(str_replace(PHP_EOL, '', $tempcontent));
        if($temp!=""){
            $temp = yang_gbk2utf8($temp);
            
            $return[] = $temp;
        }
    }
    fclose($file);
    return $return;
}
function curl_post_https($url,$data,$headers){ // 模拟提交数据函数
    $curl = curl_init(); // 启动一个CURL会话
    curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1); // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
    curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
    curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
    curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    $tmpInfo = curl_exec($curl); // 执行操作
    if (curl_errno($curl)) {
        echo 'Errno'.curl_error($curl);//捕抓异常
    }
    curl_close($curl); // 关闭CURL会话
    return $tmpInfo; // 返回数据，json格式
}
function curl_post_http($url,$data,$headers){ // 模拟提交数据函数
    //初始化
    $curl = curl_init();
    //设置抓取的url
    curl_setopt($curl, CURLOPT_URL, $url);
    //设置头文件的信息作为数据流输出
    curl_setopt($curl, CURLOPT_HEADER, 0);
    //设置获取的信息以文件流的形式返回，而不是直接输出。
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    //设置post方式提交
    curl_setopt($curl, CURLOPT_POST, 1);
    //设置post数据
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    //执行命令
    $tmpInfo = curl_exec($curl);
    //关闭URL请求
    curl_close($curl);
    return $tmpInfo; // 返回数据，json格式
}
/*
 * 过滤英文标点符号 过滤中文标点符号 代码 
 */
function filter_mark($text){
    if(trim($text)=='')return '';
    $text=preg_replace("/[[:punct:]\s]/",' ',$text);
    $text=urlencode($text);
    $text=preg_replace("/(%7E|%60|%21|%40|%23|%24|%25|%5E|%26|%27|%2A|%28|%29|%2B|%7C|%5C|%3D|\-|_|%5B|%5D|%7D|%7B|%3B|%22|%3A|%3F|%3E|%3C|%2C|\.|%2F|%A3%BF|%A1%B7|%A1%B6|%A1%A2|%A1%A3|%A3%AC|%7D|%A1%B0|%A3%BA|%A3%BB|%A1%AE|%A1%AF|%A1%B1|%A3%FC|%A3%BD|%A1%AA|%A3%A9|%A3%A8|%A1%AD|%A3%A4|%A1%A4|%A3%A1|%E3%80%82|%EF%BC%81|%EF%BC%8C|%EF%BC%9B|%EF%BC%9F|%EF%BC%9A|%E3%80%81|%E2%80%A6%E2%80%A6|%E2%80%9D|%E2%80%9C|%E2%80%98|%E2%80%99|%EF%BD%9E|%EF%BC%8E|%EF%BC%88)+/",' ',$text);
    $text=urldecode($text);
    return trim($text);
} 
function TranslateAnysay($value,$to="en",$from="zh"){
    usleep(1000);
    $to = $to=="ko"?"kor":$to;
    $to = $to=="zh-cn"?"zh":$to;
    $to = $to=="zh-tw"?"cht":$to;
    $to = $to=="ja"?"jp":$to;
    $from = $to=="ko"?"kor":$from;
    $from = $from=="zh-cn"?"zh":$from;
    $from = $from=="zh-tw"?"cht":$from;
    $from = $from=="zh-CHS"?"zh":$from;
    $url="http://api.anysay.cn/anysay/SetApi/AddModel1";
    $guid = com_create_guid();
    $guid = str_replace("{","",$guid);
    $guid = str_replace("}","",$guid);
    $guid = str_replace("-","",$guid);
    $query='{"paras":"'.$guid.'","paras1":"'.$value.'","paras2":"'.$from.'@'.$to.'","paras3":0,"paras4":"","key":"suiyi$!)*#*!)&.trycan","version":"26","type":"1"}';
    //$query=object_array(json_decode($query));
    //Dump($query);
    //$query = http_build_query($query);
    //$argc = parse_url($query, PHP_URL_PATH);
   // echo $argc;
    //初始化
    $curl = curl_init();
    //设置抓取的url
    curl_setopt($curl, CURLOPT_URL, $url);
    //设置头文件的信息作为数据流输出
    curl_setopt($curl, CURLOPT_HEADER, 0);
    //设置获取的信息以文件流的形式返回，而不是直接输出。
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json;charset=utf-8',
        'Content-Length: '.strlen($query),
        'Connection: Keep-Alive',
        'User-agent: Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1',
        'Host: api.anysay.cn',
        'Accept-Encoding: gzip',
    ));
    
   /*  'Content-Length: '.strlen($argc),
    'Connection: Keep-Alive',
    'User-agent: Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1',
    'Host: api.anysay.cn',
    'Accept-Encoding: gzip', */
    //设置post方式提交
    curl_setopt($curl, CURLOPT_POST, 1);
    //设置post数据
    curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
    //执行命令
    $data = curl_exec($curl);
    //关闭URL请求
    curl_close($curl);
    $data = object_array(json_decode($data));
    
    if($data["List"][0]["createtime"]){
        return $data["List"][0]["createtime"];
    }else{
        if($data["Code"]==400){
            return 400;
        }elseif($data["Code"]==2){
            return 400;
        }
        Dump($data);
        return false;
    }
}
/*
 * 微软在线翻译接口
 */
function TranslateNewMicrosoft($value,$to="en",$from="zh-CHS")
{
    $requestBody = array (
        array (
            'Text' => $value,
        ),
    );
    $content = json_encode($requestBody);
    $params = "&to=".$to;
    $key = 'b581e3519a864b50b2007278b7c61d4b';
    $host = "https://api.cognitive.microsofttranslator.com";
    $path = "/translate?api-version=3.0";
    $headers = "Content-type: application/json\r\n" .
        "Content-length: " . strlen($content) . "\r\n" .
        "Ocp-Apim-Subscription-Key: $key\r\n" .
        "X-ClientTraceId: " . com_create_guid() . "\r\n";
    // NOTE: Use the key 'http' even if you are making an HTTPS request. See:
    // http://php.net/manual/en/function.stream-context-create.php
    $options = array (
        'http' => array (
            'header' => $headers,
            'method' => 'POST',
            'content' => $content
        )
    );
    $context  = stream_context_create ($options);
    $result = file_get_contents ($host . $path . $params, false, $context);
    $result = object_array(json_decode($result));
    if($result[0]["translations"][0]["text"]){
        return $result[0]["translations"][0]["text"];
    }else{
        return false;
    }
    #赋予变量
    
}
/*
 * 微软在线翻译接口
 */
function TranslateMicrosoft($value,$to="en",$from="zh-CHS")
{
    $value_code=urlencode($value);
    #首先对要翻译的文字进行 urlencode 处理
    // DF9E54CA96F73F2E289AEC059F407DE8295A6515
    //$appid="DF9E54CA96F73F2E289AEC059F407DE8295A6515";
    $appid="2AD6CC0AEFCEFB68D3BAECB511F6B639A17B065A";
    #您注册的API Key
    $languageurl = "http://api.microsofttranslator.com/v2/Http.svc/Translate?appId=" . $appid ."&text=" .$value_code. "&from=". $from ."&to=" . $to;
    #生成翻译API的URL GET地址
    if(!function_exists('file_get_contents')) {
        $text = file_get_contents($languageurl);
    } else {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt ($ch, CURLOPT_URL, $languageurl);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $text = curl_exec($ch);
        curl_close($ch);
    }
    #调用函数 获取URL打印的值
    preg_match_all("/>(.+)</i",$text,$text_ok,PREG_SET_ORDER);
    #有多种方法，获取翻译结果，我这里直接用正则过滤。
    $ru=$text_ok[0][1];
    if(strpos($ru,'TranslateApiException')===false){
        return $ru;
    }else{
        //echo 1;
        return TranslateAnysay($value,$to,$from);
    }
    #赋予变量
    
}
/**********************
一个简单的目录递归函数
第一种实现办法：用dir返回对象
***********************/
function tree($directory,$files=array()) 
{ 
	$mydir = dir($directory); 
	while($file = $mydir->read())
	{ 
		if((is_dir("$directory/$file")) AND ($file!=".") AND ($file!="..")) 
		{
			$temp["name"] = $file;
			$temp["list"] = tree("$directory/$file"); 
			$files[]=$temp;
		} 
		elseif(($file!=".") AND ($file!="..")){ 
			$temp["name"] = $file;
			$temp["list"] = 0; 
			$files[]=$temp;
		}
	} 
	$mydir->close();
	return $files;
} 
/*删除指定目录下的文件，不删除目录文件夹*/
function delFile($dir)
{
    //删除目录下的文件：
    $dh=opendir($dir);
    
    while ($file=readdir($dh))
    {
        if($file!="." && $file!="..")
        {
            $fullpath=$dir."/".$file;
            
            if(!is_dir($fullpath))
            {
                unlink($fullpath);
            }
            else
            {
                delFile($fullpath);
            }
        }
    }
    
    closedir($dh);
}
?>