<?php
return array("status"=>array(
	"photo"=>array(
		"status"=>array(
				"1"=>"已审",
				"2"=>"未审",
				"3"=>"已删除"
			),
		"type"=>array(
			"1"=>"相册",
			"2"=>"头像",
		),
		"seetype"=>array(
			"1"=>"全部可见",
			"2"=>"会员可见",
		),
	),
	"user"=>array(
		"gender"=>array(
				"1"=>"男",
				"2"=>"女",
			),
		"user_type"=>array(
			"1"=>"普通用户",
			"2"=>"采集用户",
		    "3"=>"优质用户（原推广和老推广人员）",
            "4"=>"测试用户",
            //"5"=>"推广用户",
            "6"=>"禁止发信用户",
            "7"=>"在线客服",
            "8"=>"封号用户",
            "9"=>"营销有底薪",
            "10"=>"营销无底薪",
            "11"=>"1v1"
		),
	    "vipgrade"=>array(
            "0"=>"非会员",
            "1"=>"一级会员",
            "2"=>"二级会员",
            "3"=>"三级会员",
            "4"=>"四级会员",
        ),
        "agegrade"=>array(
            "1"=>"18-25岁",
            "2"=>"25-32岁",
            "3"=>"32-39岁",
            "4"=>"39-50岁",
            "5"=>"50-60岁",
            "6"=>"60岁及以上",
        ),
	),
    "lang"=>array(
	    "code"=>array(
	        "zh-tw"=>"中文（繁體）",
	        "zh-cn"=>"中文(简体)",
	        "en"=>"English",
	        "ja"=>"日本語",
	        "ko"=>"한국어."
	    )
	),
    "switch"=>array(
        "type"=>array(
            "1"=>"注册前",
            "2"=>"每次打开后",
            "3"=>"注册后"
        )
    ),
    "zidian"=>array(
        "type"=>array(
            "1"=>"单选",
            "2"=>"多选"
        )
    ),
    "cache"=>array(
        "0"=>"userinfo-(个人信息缓存)",
        "1"=>"photos-（相册缓存）",
        "2"=>"dynamic-（个人的动态缓存）",
        "3"=>"reids_dynamic-（reids全部动态缓存）",
        "4"=>"audio-（语音认证缓存）",
        "5"=>"country-（国家缓存）",
        "6"=>"follow-（点赞缓存）",
        "7"=>"language-（语言文件缓存）",
        "8"=>"sensitive-（敏感词缓存）",
        "9"=>"giftlist-（礼物缓存）",
        "10"=>"attr-（字典缓存）",
        "11"=>"automsg-",
        "12"=>"alipay-（支付宝支付信息缓存）",
        "13"=>"video-（视频认证缓存）",
        "14"=>"area-（城市缓存）",
        "15"=>"reids_switch-（开关缓存）",
        "16"=>"redis_videochar-",
        '17'=>'redis_registlist-（前端跑马灯信息缓存）',
        '18'=>"redis_betteruser-(优质女用户缓存)",
        "19"=>"redis_getranklist-(魅力值排行榜)",
        "20"=>"redis_useronlinelist-(营销用户缓存)",
        "21"=>"redis_recommendwordlist-(推荐语缓存)",
    ),
    //测试注册phoneid白名单
    "phoneid"=>array(
        "00000000-47ef-4210-0000-000061d0cff0",
        "8A4D57D172A64098A4478BD76A31D9FE",
        "00000000-7ddf-20ac-0000-0000346e978b",
        "00000000-663a-8da4-ffff-ffffc2141209",
        "7e0a64b33458341964c719dace2d1fcd",
        "E070316B18D4423DB3972FE4B60AA253",
        "0175C65DAFEA48868E2BF6916EA73A5E",
        "ffffffff-8ada-bb6c-ffff-ffffeef8be01",
        "991E47CD03FD42EA89256A0E78BBAB04",
        "61ACD369-8E52-4BB9-B6DF-5FFF7AF2762E",
        "E572E27C-9017-4E12-9D8A-A1D84ADCE2CD",
        "00000000-01d6-90e6-0000-00000b6a356a",
        "ffffffff-f2bb-e84b-ffff-ffffeef8be01",
        "00000000-5a9d-152a-0000-00000b6a356a",
        "00000000-3152-a07c-0000-0000069d851e",
        "00000000-6251-dd6d-ffff-ffffb4d58363",
        "00000000-2a5f-6ee8-ffff-ffffeef8be01",
        "ffffffff-ce0d-6e69-ffff-ffffeef8be01",
        "00000000-662c-418a-ffff-ffffeef8be01",
        "ffffffff-9dcf-c827-ffff-ffffeef8be01",
        "ffffffff-9dcf-c827-ffff-ffffef05ac4a",
        "00000000-113c-fe85-ffff-ffffef05ac4a",
        ),
	
));