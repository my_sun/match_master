<?php

/**
 * Created by PhpStorm.
 * User: 亮亮
 * Date: 2018/4/25
 * Time: 17:53
 */
use Think\Model;
class UserExtendBakModel extends Model{
    public function __construct(){
        parent::__construct();
        //$this->copy_table('t_msg',20);
        //$this->alter_table('t_msg_box',20);

    }

    /**
     * 添加一条数据
     * @param array $data
     * @return bool|int
     */
    public function addOne($data){
        if($this->create($data)){
            $id = $this->add();
            if($id === false){
                $this->error = '插入数据错误';
                return false;
            }else{
                return $id;
            }
        }
        return false;
    }
    /**
     * 获取所有记录
     */
    public function getAll(){
        return $this->select();
    }
    /**
     * 条件查询列表
     * @param $map
     * @return mixed
     */
    public function getList($map){
        return $this->where($map)->select();
    }
    /**
     * 条件查询列表分页
     * @param $map
     * @return mixed
     */
    public function getListPage($map,$pageNum,$pageSize){
        import("Extend.Library.ORG.Util.Page");       //导入分页类
        $count  = $this->where($map)->count();    //计算总数
        $Page   = new Page($count,$pageSize,$pageNum);
        //	Dump($Page);exit;
        $data = $this->where($map)->limit($Page->firstRow. ',' . $Page->listRows)->select();
        $retArray = array();
        $retArray['totalCount'] = $count;
        $retArray['pageSize'] = $pageSize;
        $retArray['pageNum'] = $pageNum;
        $retArray['list'] = $data;
        return $retArray;
    }
    /**
     * 根据查询条件获取一条记录
     * @param $map
     * @return mixed
     */
    public function getOne($map){
        return $this->where($map)->find();
    }

    /**
     * 更新一条记录
     * @param $map
     * @param $data
     * @return bool
     */
    public function updateOne($map, $data){
        if($this->create($data)){
            $res = $this->where($map)->save();
            if($res === false){
                $this->error = '更新数据出错';
            }else{
                return $res;   //更新的数据条数
            }
        }
        return false;
    }

    /**
     * 更新表单提交数据记录
     * @param $map
     * @param $data
     * @return bool
     */
    public function updateAll($data, $map){

        $res = $this->where($map)->save($data);
        if($res === false){
            $this->error = '更新数据出错';
        }else{
            return $res;   //更新的数据条数
        }

        return false;
    }
    /**
     * 删除一条记录
     * @param $map
     * @return bool|mixed
     */
    public function delOne($map){
        $res = $this->where($map)->delete();
        if($res === false){
            $this->error = '删除数据出错';
            return false;
        }else{
            return $res;   //删除数据个数
        }
    }
    //查询一条数据
    public function selOne($map){
        $res = $this->where($map)->find();
        if($res === false){
            $this->error = '查询数据出错';
            return false;
        }else{
            return $res;   //查询数据个数
        }
    }
    //插入一条数据
    public function insOne($map){
        $res = $this->data($map)->add();
        if($res === false){
            $this->error = '插入数据出错';
            return false;
        }else{
            return $res;   //插入数据个数
        }
    }



    /**
     * 条件查询列表分页
     * @param $map
     * @return mixed
     */

    /**
     * 条件查询列表分页
     * @param $map
     * @return mixed
     */
    public function getListfield($where,$pageNum,$pageSize,$field="uid"){
        import("Extend.Library.ORG.Util.Page");       //导入分页类
        $count  = $this->where($where)->count();    //计算总数
        $Page   = new Page($count,$pageSize,$pageNum);
        $data = $this->field($field)->where($where)->order("RAND()")->limit($Page->firstRow. ',' . $Page->listRows)->select();

        return $data;

    }
    /**
     * 搜索用户
     * @param $map
     * @return mixed
     */
    public function getlistSearch($where,$pageNum,$pageSize,$map=array()){
        import("Extend.Library.ORG.Util.Page");       //导入分页类
        $count  = $this->where($where)->count();    //计算总数
        $Page   = new Page($count,$pageSize,$pageNum);

        //	Dump($Page);exit;
        $data = $this->where($where)->order("RAND()")->limit($Page->firstRow. ',' . $Page->listRows)->select();
        $retArray = array();
        $retArray['totalCount'] = $count;
        $retArray['pageSize'] = $pageSize;
        $retArray['pageNum'] = $pageNum;
        $retArray['listSearch'] = $data;
        return $retArray;

    }
    /**
     * 搜索用户
     * @param $map
     * @return mixed
     */
    public function peiliaogetlistSearch($where,$pageNum,$pageSize,$order='account asc'){
        import("Extend.Library.ORG.Util.Page");       //导入分页类
        $count  = $this->where($where)->count();    //计算总数
        $Page   = new Page($count,$pageSize,$pageNum);

        //	Dump($Page);exit;
        $data = $this->where($where)->order($order)->limit($Page->firstRow. ',' . $Page->listRows)->select();
        $retArray = array();
        $retArray['totalCount'] = $count;
        $retArray['pageSize'] = $pageSize;
        $retArray['pageNum'] = $pageNum;
        $retArray['listSearch'] = $data;
        return $retArray;

    }
    public function getlistOrder($where,$pageNum,$pageSize=20,$order="uid desc",$map=array()){

        import("Extend.Library.ORG.Util.Page");       //导入分页类
        $count  = $this->where($where)->count();    //计算总数

        $Page   = new Page($count,$pageSize,$pageNum);


        $data = $this->where($where)->order($order)->limit($Page->firstRow. ',' . $Page->listRows)->select();
        $retArray = array();
        $retArray['totalCount'] = $count;
        $retArray['pageSize'] = $pageSize;
        $retArray['pageNum'] = $pageNum;
        $retArray['listSearch'] = $data;
        return $retArray;

    }
    public function GetCount($where){

        return $this->where($where)->count();    //计算总数
    }


}
?>