<?php
class ArrayModel extends BaseModel {
	protected $partition = array(
			/**地区列表**/
		'areaListGuowai' =>array(
				array('id'=>'1','name'=> "Taipeishi"),
				array('id'=>'2','name'=> "Xinbeishi"),
				array('id'=>'3','name'=> "Taoyuanshi"),
				array('id'=>'4','name'=> "Taizhongshi"),
				array('id'=>'5','name'=> "Tainanshi"),
				array('id'=>'6','name'=> "Gaoxiongshi"),
				array('id'=>'7','name'=> "Jilongshi"),
				array('id'=>'8','name'=> "Xinzhushi"),
				array('id'=>'9','name'=> "Jiayishi"),
				array('id'=>'10','name'=> "Xinzhuxian"),
				array('id'=>'11','name'=> "Miaolixian"),
				array('id'=>'12','name'=> "Zhanghuaxian"),
				array('id'=>'13','name'=> "Nantouxian"),
				array('id'=>'14','name'=> "Yunlinxian"),
				array('id'=>'15','name'=> "Jiayixian"),
				array('id'=>'16','name'=> "Pingdongxian"),
				array('id'=>'17','name'=> "Yilanxian"),
				array('id'=>'18','name'=> "Hualianxian"),
				array('id'=>'19','name'=> "Taidongxian"),
				array('id'=>'20','name'=> "Penghuxian"),
				array('id'=>'21','name'=> "XiangGang"),
				array('id'=>'22','name'=> "XinJiaPo"),
				array('id'=>'23','name'=> "MaLaiXiYa"),
				array('id'=>'24','name'=> "FeiLvBing"),
				array('id'=>'25','name'=> "DongPuZhai"),
				array('id'=>'26','name'=> "LaoWo"),
				array('id'=>'27','name'=> "TaiGuo"),
				array('id'=>'28','name'=> "YueNan")
			),
			
			
				'areaList' =>array(
				array('id'=>'1','name'=> "beijing"),
				array('id'=>'2','name'=> "aomen"),
				array('id'=>'3','name'=> "anhui"),
				array('id'=>'4','name'=> "fujian"),
				array('id'=>'5','name'=> "gansu"),
				array('id'=>'6','name'=> "guangdong"),
				array('id'=>'7','name'=> "guangxi"),
				array('id'=>'8','name'=> "guizhou"),
				array('id'=>'9','name'=> "hainan"),
				array('id'=>'10','name'=> "hebei"),
				array('id'=>'11','name'=> "henan"),
				array('id'=>'12','name'=> "heilongjiang"),
				array('id'=>'13','name'=> "hubei"),
				array('id'=>'14','name'=> "hunan"),
				array('id'=>'15','name'=> "jilin"),
				array('id'=>'16','name'=> "jiangsu"),
				array('id'=>'17','name'=> "jiangxi"),
				array('id'=>'18','name'=> "liaoning"),
				array('id'=>'19','name'=> "neimenggu"),
				array('id'=>'20','name'=> "ningxia"),
				array('id'=>'21','name'=> "qinghai"),
				array('id'=>'22','name'=> "shandong"),
				array('id'=>'23','name'=> "shanxi"),
				
				array('id'=>'24','name'=> "shanxi2"),
				array('id'=>'25','name'=> "shanghai"),
				array('id'=>'26','name'=> "sichuan"),
				array('id'=>'27','name'=> "taiwan"),
				array('id'=>'28','name'=> "tianjin"),
				array('id'=>'29','name'=> "xizang"),
				array('id'=>'30','name'=> "xianggang"),
				array('id'=>'31','name'=> "xinjiang"),
				array('id'=>'32','name'=> "yunnan"),
				array('id'=>'33','name'=> "zhejiang"),
				array('id'=>'34','name'=> "chongqing")
				
			),
		/**血型**/
		"bloodList" => array(
			array('id'=>'1' ,'name'=> "A"),
			array('id'=>'2' ,'name'=> "B"),
			array('id'=>'3' ,'name'=> "AB"),
			array('id'=>'4' ,'name'=> "O")
		),
		/**学历**/
		"educationList" => array(
				array('id'=>'1' , 'name'=> "Guozhongyixia"),
				array('id'=>'2' , 'name'=> "Gaozhong"),
				array('id'=>'3' , 'name'=> "Zhuanke"),
				array('id'=>'4' , 'name'=> "Daxue"),
				array('id'=>'5' , 'name'=> "Shuoshiyishang")
		),
		/**收入**/
		"incomeList" => array(
				array('id'=>'1','name' => "<20000"),
				array('id'=>'2','name' => "20000-40000"),
				array('id'=>'3','name' => "40000-60000"),
				array('id'=>'4','name' => "60000-100000"),
				array('id'=>'5','name' => ">100000")
		),
		/**兴趣爱好**/
		"interestList" => array(
				array('id'=>'1','name' => "Shangwang"),
				array('id'=>'2','name' => "Yanjiuqiche"),
				array('id'=>'3','name' => "Yangxiaodongwu"),
				array('id'=>'4','name' => "Sheying"),
				array('id'=>'5','name' => "Kandianying"),
				array('id'=>'6','name' => "Tingyinyue"),
				array('id'=>'7','name' => "Xiezuo"),
				array('id'=>'8','name' => "Gouwu"),
				array('id'=>'9','name' => "Zuoshougongyi"),
				array('id'=>'10','name' => "Zuoyuanyi"),
				array('id'=>'11','name' => "Tiaowu"),
				array('id'=>'12','name' => "Kanzhanlan"),
				array('id'=>'13','name' => "Pengren"),
				array('id'=>'14','name' => "Dushu"),
				array('id'=>'15','name' => "Huihua"),
				array('id'=>'16','name' => "Yanjiujisuanji"),
				array('id'=>'17','name' => "Zuoyundong"),
				array('id'=>'18','name' => "Lvyou"),
				array('id'=>'19','name' => "Wanyouoxi"),
				array('id'=>'20','name' => "Qita")
		),
		/**婚姻状况**/
		"marriageList" => array(
				array('id'=>'1','name' => "Weihun"),
				array('id'=>'2','name' => "Liyi"),
				array('id'=>'3','name' => "Sangou")
		),
		/**星座**/
		"starList" => array(
				array('id'=>'1','name' => "Baiyangzuo"),
				array('id'=>'2','name' => "Jinniuzuo"),
				array('id'=>'3','name' => "Shuangzizuo"),
				array('id'=>'4','name' => "Juxiezuo"),
				array('id'=>'5','name' => "Shizizuo"),
				array('id'=>'6','name' => "Chunvzuo"),
				array('id'=>'7','name' => "Tianpingzuo"),
				array('id'=>'8','name' => "Tianxiezuo"),
				array('id'=>'9','name' => "Sheshouzuo"),
				array('id'=>'10','name' => "Mojiezuo"),
				array('id'=>'11','name' => "Shuipingzuo"),
				array('id'=>'12','name' => "Shuangyuzuo")
		),
		/**是否想要孩子**/
		"wantBabyList" => array(
				array('id'=>'1','name' => "Xiang"),
				array('id'=>'2','name' => "Buxiang"),
				array('id'=>'3','name' => "Haimeixianghao")
		),
		/**工作状况**/
		"workList" => array(
				array('id'=>'1','name' => "Zaixuexiao"),
				array('id'=>'2','name' => "Falv"),
				array('id'=>'3','name' => "IT/Hulianwang"),
				array('id'=>'4','name' => "Yule"),
				array('id'=>'5','name' => "Jinrong"),
				array('id'=>'6','name' => "Zhengfujiguan"),
				array('id'=>'7','name' => "Xiezuo"),
				array('id'=>'8','name' => "Wenhua/Yishu"),
				array('id'=>'9','name' => "Yingshi"),
				array('id'=>'10','name' => "Fangdichan/Jianzhu"),
				array('id'=>'11','name' => "Jixie"),
				array('id'=>'12','name' => "Nengyuan/Huanbao"),
				array('id'=>'13','name' => "Yiliao/Jiankang"),
				array('id'=>'14','name' => "Jiaoyu/Kejian"),
				array('id'=>'15','name' => "Qita")
		)
		,
		/**非法字符替换**/
		"not_str" => array(
				"perdate"
		),
		/**非法字符替换的内容**/
		"yes_str" => array(
				"0"
		)
	);
	
	public function __construct(){
		parent::__construct();
		
		//$this->copy_table('t_user_base',20);
	}
    public function get_var($key1="",$key2=""){
    	if($key1){
    		return $this->partition[$key1];
    	}elseif($key2){
    		return $this->partition[$key1]['$key2'];
    	}else{
    		return $this->partition;
    	}
    }
}
?>