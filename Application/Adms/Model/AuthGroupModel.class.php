<?php

/**
 * 权限规则model
 */
class AuthGroupModel extends BaseModel{

	/**
	 * 传递主键id删除数据
	 * @param  array   $map  主键id
	 * @return boolean       操作是否成功
	 */
	public function deleteDatar($map){
		$result=$this->where($map)->delete();
		if ($result) {
			$group_map=array(
				'group_id'=>$map['id']
			);
			// 删除关联表中的组数据
			$result=D('AuthGroupAccess')->deleteDatar($group_map);
		}
		return true;
	}

    /**
     * 添加数据
     * @param  array $data  添加的数据
     * @return int          新增的数据id
     */
    public function addData($data){
        // 去除键值首尾的空格
        foreach ($data as $k => $v) {
            $data[$k]=trim($v);
        }
        $id=$this->add($data);
        return $id;
    }
    /**
     * 修改数据
     * @param   array   $map    where语句数组形式
     * @param   array   $data   数据
     * @return  boolean         操作是否成功
     */
    public function editData($map,$data){
        // 去除键值首位空格
        foreach ($data as $k => $v) {
            $data[$k]=trim($v);
        }
        $result=$this->where($map)->save($data);
        return $result;
    }
    //根据组id查询组名字
    public function getFieldById($map,$char){

        $result=$this->where($map)->getField($char,true);
        return $result;
    }



}
