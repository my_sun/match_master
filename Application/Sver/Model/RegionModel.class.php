<?php
use Think\Model;
class RegionModel extends Model{
	public function __construct(){
		parent::__construct();
		//$this->copy_table('t_msg',20);
		//$this->alter_table('t_msg_box',20);
		
	}

 
   
  
   /**
    * 添加一条数据
    * @param array $data
    * @return bool|int
    */
   public function addOne($data){   	
   	if($this->create($data)){
   		$id = $this->add();
   		if($id === false){
   			$this->error = '插入数据错误';
   			return false;
   		}else{
   			return $id;
   		}
   	}
   	return false;
   }
   /**
    * 获取所有记录
    */
   public function getAll(){
   	return $this->select();
   }
   /**
    * 条件查询列表
    * @param $map
    * @return mixed
    */
   public function getList($map){
   	return $this->where($map)->select();
   }
   /**
    * 条件查询列表分页
    * @param $map
    * @return mixed
    */
   public function getListPage($map,$pageNum,$pageSize){
   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->where($map)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   //	Dump($Page);exit;
   	$data = $this->where($map)->order('id desc')->limit($Page->firstRow. ',' . $Page->listRows)->select();

	return $data;
	
   }
   /**
    * 根据查询条件获取一条记录
    * @param $map
    * @return mixed
    */
   public function getOne($map){
   	return $this->where($map)->find();
   }
   
   /**
    * 更新一条记录
    * @param $map
    * @param $data
    * @return bool
    */
   public function updateOne($map, $data){
   	if($this->create($data)){
   		$res = $this->where($map)->save();
   		
   		if($res === false){
   			$this->error = '更新数据出错';
   		}else{
   			return $res;   //更新的数据条数
   		}
   	}
   	return false;
   }
   /**
    * 删除一条记录
    * @param $map
    * @return bool|mixed
    */
   public function delOne($map){
   	$res = $this->where($map)->delete();
   	if($res === false){
   		$this->error = '删除数据出错';
   		return false;
   	}else{
   		return $res;   //删除数据个数
   	}
   }
}
?>