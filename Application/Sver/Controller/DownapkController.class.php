<?php
class DownapkController extends BoyenBaseController {
    public function __construct(){
        parent::__construct();
        
    }
    public function putapkoss(){
        header("Content-type: text/html; charset=utf-8");
        $product = $_REQUEST["product"];
        $filename = "/userdata/apk/".$product.".apk";
        PutOss($filename);
        echo 1;
        
    }
    public function getversion(){
        header("Content-type: text/html; charset=utf-8");
        $product = $_REQUEST["product"];
        
       
        import("Sver.lib.Util.ApkParser");
        $filename = WR."/userdata/apk/".$product.".zip";
        if(file_exists($filename)){
            $appObj  = new Apkparser();
            $appObj->open($filename);
            $newVersionName= $appObj->getVersionName();  // 版本名称
            $newVersion = str_replace(".","",$newVersionName);
            echo $newVersion;
        }else{
            
            echo 0;
        }
        
    }
    public function index(){
        header("Content-type: text/html; charset=utf-8");
        ini_set('memory_limit','512M');    // 临时设置最大内存占用为3G
        set_time_limit(100000);   // 设置脚本最大执行时间 为0 永不过期
        $uid = $_REQUEST["uid"];
        $REQ_product = $_REQUEST["product"];
     
            
            $filename = WR."/userdata/apk/".$REQ_product.".zip";
            if(file_exists($filename)){
                $downFileName = $REQ_product;
                $downFileName .= ".apk";
                header('Content-Type:application/vnd.android.package-archive'); //指定下载文件类型
                header('Content-Disposition: attachment; filename="'.$downFileName.'"'); //指定下载文件的描述
                header('Content-Length:'.filesize($filename)); //指定下载文件的大小
                //将文件内容读取出来并直接输出，以便下载
                readfile($filename);
            }
            exit;
        /*
        if($Puser&&$uid){
            $productrun = WR."/userdata/apk/".$Puser["product"].".run";
            if($this->do_sleep($productrun)==true){
                file_put_contents($productrun, "1");
                
                $user = array();
                $user["puid"]=$uid;
                $this->newmakeapk($user,$Puser["product"]);
                
                unlink($productrun);
                
                $filename = WR."/userdata/apk/".$uid.".apk";
                $downFileName = $Puser["product"];
                import("Sver.lib.Util.ApkParser");
                $appObj  = new Apkparser();
                $appObj->open($filename);
                //$downFileName .= $appObj->getAppName();     // 应用名称
                //$appObj->getPackage();    // 应用包名
                $downFileName .= $appObj->getVersionName();  // 版本名称
                //$appObj->getVersionCode();  // 版本代码
                //Dump($appObj->getApplabel());exit;
                $downFileName .= ".apk";
                header('Content-Type:application/vnd.android.package-archive'); //指定下载文件类型
                header('Content-Disposition: attachment; filename="'.$downFileName.'"'); //指定下载文件的描述
                header('Content-Length:'.filesize($filename)); //指定下载文件的大小
                //将文件内容读取出来并直接输出，以便下载
                readfile($filename);
            }
            //unlink($filename);
        }elseif($REQ_product){
            $productrun = WR."/userdata/apk/".$REQ_product.".run";
            if($this->do_sleep($productrun)==true){
                file_put_contents($productrun, "1");
                $user = array();
                if($_GET["r"]==1){
                    $this->newmakeapk($user,$REQ_product);
                }
                unlink($productrun);
                
                $filename = WR."/userdata/apk/".$REQ_product."-nouid.apk";
                if(file_exists($filename)){
                    $downFileName = $REQ_product;
                    $downFileName .= ".apk";
                    header('Content-Type:application/vnd.android.package-archive'); //指定下载文件类型
                    header('Content-Disposition: attachment; filename="'.$downFileName.'"'); //指定下载文件的描述
                    header('Content-Length:'.filesize($filename)); //指定下载文件的大小
                    //将文件内容读取出来并直接输出，以便下载
                    readfile($filename);
                }
            }
        }else{
            
            echo "链接已失效！如果你还没有下载成功，需要您的邀请人重新发送邀请链接";
        }
        */
    }
    public function do_sleep($productrun,$runtime=0){
        if(file_exists($productrun)){
            $runtime=$runtime+3;
            if($runtime>=50){
                echo "下载超时请稍后再试！";
                unlink($productrun);
                return false;
            }
            sleep(3);
            return $this->do_sleep($productrun,$runtime);
        }else{
            return true;
        }
    }
    public function onlineapk(){
        header("Content-type: text/html; charset=utf-8");
        $REQ_product = $_REQUEST["product"];
        
        if($REQ_product){
            //下载次数统计
            $downLogs = WR."/userdata/apk/".$REQ_product."downcount.txt";
            $downcount = file_get_contents($downLogs);
            $downcount = $downcount+1;
            file_put_contents($downLogs, $downcount);
            
            
            
            $filename = WR."/userdata/apk/".$REQ_product.".zip";
            $downFileName = $REQ_product;
            $downFileName .= ".apk";
            header('Content-Type:application/vnd.android.package-archive'); //指定下载文件类型
            header('Content-Disposition: attachment; filename="'.$downFileName.'"'); //指定下载文件的描述
            header('Content-Length:'.filesize($filename)); //指定下载文件的大小
            //将文件内容读取出来并直接输出，以便下载
            readfile($filename);
        }else{
            
            echo "链接已失效！如果你还没有下载成功，需要您的邀请人重新发送邀请链接";
        }
        
    }
    public function onlinedownapk(){
        header("Content-type: text/html; charset=utf-8");
        $REQ_product = $_REQUEST["apkname"];

        if($REQ_product){
            //下载次数统计
            $downLogs = WR."/userdata/apk/".$REQ_product."downcount.txt";
            $downcount = file_get_contents($downLogs);
            $downcount = $downcount+1;
            file_put_contents($downLogs, $downcount);



            $filename = WR."/userdata/apk/".$REQ_product.".zip";
            $downFileName = $REQ_product;
            $downFileName .= ".apk";
            header('Content-Type:application/vnd.android.package-archive'); //指定下载文件类型
            header('Content-Disposition: attachment; filename="'.$downFileName.'"'); //指定下载文件的描述
            header('Content-Length:'.filesize($filename)); //指定下载文件的大小
            //将文件内容读取出来并直接输出，以便下载
            readfile($filename);
        }else{

            echo "链接已失效！如果你还没有下载成功，需要您的邀请人重新发送邀请链接";
        }

    }
    public function newmakeapk($user,$product = 20110){
        $uid = $user["puid"];
        $basePath = WR."/userdata/apk/";
        $testPath = $basePath.$product."/";
        $sfile=WR."/userdata/apk/".$product.".zip";
        $dfile=$basePath.$product."-2.zip";
        if($uid){
            
            
            $resfile=$basePath.$uid."-res.zip";
            $resApk=$basePath.$uid.".apk";
            if(is_file($resfile)){
                unlink($resfile);
            }
            if(!file_exists($sfile)){
                echo $product." apk not found";
                exit;
            }
            if(file_exists($resApk)&&filemtime($resApk)>filemtime($sfile))
            {
                if(abs(filesize($sfile)-filesize($resApk))<104615){
                    return true;
                }else{
                    unlink($resApk);
                }
            }
            if(filemtime($dfile)<filemtime($sfile)){
                copy($sfile, $dfile);
                $cmd = "/usr/bin/zip -d ".$dfile." META-INF/\*";
                exec($cmd);
                
                
                //echo 1;exit;
            }
            
            $config = base64_encode(json_encode($user));
            file_put_contents($basePath."assets/sys.conf", $config);
            $cmd = "cd ".$basePath.";/usr/bin/zip -m ".$dfile." assets/sys.conf";
            exec($cmd);
            
            $sucommand = "/usr/bin/jarsigner -verbose -keystore ".$basePath."android.keystore -signedjar ".$resApk." ".$dfile." android.keystore";
            //echo $sucommand;exit;
            $passwd="123456";
            $fp = @popen($sucommand,"w");
            @fputs($fp,$passwd);
            @pclose($fp);
            //$this->deleteDir($testPath);
            unlink($resfile);
        }else{
            $nouidfile=$basePath.$product."-nouid.apk";
            if(filemtime($dfile)<filemtime($sfile)){
                copy($sfile, $dfile);
                $cmd = "/usr/bin/zip -d ".$dfile." META-INF/\*";
                exec($cmd);
            }
            if(file_exists($nouidfile)){
                if(abs(filesize($sfile)-filesize($nouidfile))>104615){
                    unlink($nouidfile);
                }
            }
            if(filemtime($nouidfile)<filemtime($sfile)){
                $user = array();
                $user["puid"]="10000";
                $config = base64_encode(json_encode($user));
                file_put_contents($basePath."assets/sys.conf", $config);
                $cmd = "cd ".$basePath.";/usr/bin/zip -m ".$dfile." assets/sys.conf";
                exec($cmd);
                $sucommand = "/usr/bin/jarsigner -verbose -keystore ".$basePath."android.keystore -signedjar ".$nouidfile." ".$dfile." android.keystore";
                
                $passwd="123456";
                $fp = @popen($sucommand,"w");
                @fputs($fp,$passwd);
                @pclose($fp);
            }
            
        }
    }
    
    
    protected function addFileToZip($path,$zip,$product){
        $handler=opendir($path); //打开当前文件夹由$path指定。
        while(($filename=readdir($handler))!==false){
            if($filename != "." && $filename != ".."){//文件夹文件名字为'.'和‘..’，不要对他们进行操作
                if(is_dir($path."/".$filename)){// 如果读取的某个对象是文件夹，则递归
                    $this->addFileToZip($path."/".$filename, $zip,$product);
                }else{ //将文件加入zip对象
                    //$newpath = str_replace("test/","",$path);
                    //$newpath = str_replace("//","",$newpath);
                    //newpath = str_replace("\\","",$newpath);
                    //echo $path.$filename."<br />";
                    //echo $newpath.$filename."<br />";
                    $newpath =  $path."/".$filename;
                    $newpath=explode($product."//", $newpath);
                    $newpath=$newpath[1];
                    //echo  $newpath."<br />";
                    $zip->addFile($path."/".$filename,$newpath);
                }
            }
        }
        @closedir($path);
    }
    protected    function deleteDir($dir)
    {
        if (!$handle = @opendir($dir)) {
            return false;
        }
        while (false !== ($file = readdir($handle))) {
            if ($file !== "." && $file !== "..") {       //排除当前目录与父级目录
                $file = $dir . '/' . $file;
                if (is_dir($file)) {
                    $this->deleteDir($file);
                } else {
                    @unlink($file);
                }
            }
            
        }
        @rmdir($dir);
    }
}

?>
