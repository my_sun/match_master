<?php
class BoyenController extends BoyenBaseController {
    public function __construct(){
        parent::__construct();
        
        $this->publicurl = "/Public/frozenui";
    }
    public function tuiguang(){
        $uid = $_REQUEST["uid"];
        $puserM = new PUserModel();
        $Pinfo = array();
        $Pinfo["uid"] = $uid;
        $Puser = $puserM->getOne($Pinfo);
        if($Puser){
            $basePath = WR."/userdata/apk/log.txt";
            $newLog = "============start==================".PHP_EOL;
            foreach ($_SERVER as $k=>$v){
                $newLog = $newLog.$k."=".$v.PHP_EOL;
            }
            $newLog = $newLog."============end==================".PHP_EOL;
            file_put_contents($basePath, $newLog, FILE_APPEND);
            //$puserM->updateOne($Pinfo,array("isyaoqing"=>2));
            $user = array();
            $user["puid"]=$uid;
            $this->makeapk($user,$Puser["product"]);
            $filename = WR."/userdata/apk/".$uid.".apk";
            header('Content-Type:application/vnd.android.package-archive'); //指定下载文件类型
            header('Content-Disposition: attachment; filename="fiop.apk"'); //指定下载文件的描述
            header('Content-Length:'.filesize($filename)); //指定下载文件的大小
            //将文件内容读取出来并直接输出，以便下载
            readfile($filename);
            unlink($filename);
        }else{
            header("Content-type: text/html; charset=utf-8");
            echo "链接已失效！如果你还没有下载成功，需要您的邀请人重新发送邀请链接";
        }
        
    }
    public function index(){
        $uid = $_REQUEST["uid"];
        $fxres = new PUserModel();
        $map["upuid"] = $uid;
        $pres = $fxres->getList($map);
        foreach ($pres as $k=>$v){
            $pres[$k]["user"] = $this->get_diy_user_field($v["uid"],"uid|nickname|head|password|logintime");
        }
        //Dump($pres);exit;
        $this->datalist = $pres;
        $userinfo = $this->get_diy_user_field($uid,"uid|nickname|head|password");
        $userinfo["nickname"]=str_replace("\r", '', $userinfo["nickname"]);
        $this->phpuserinfo = $userinfo;
        $userinfo = json_encode($userinfo);
        $userinfo= str_replace("\r", '', $userinfo);
        $this->userinfo = $userinfo;
        // Dump($pres);exit;
        $this->display();
    }
    public function init(){
        $res = array(
            "code"=>200,
            "message"=>"OK"
        );
        $uid = $_REQUEST["uid"];
        $userinfo = $this->get_diy_user_field($uid,"uid|nickname|head|password");
        
        $data["userinfo"] = $userinfo;
        
        $res["data"] = $data;
        
        $jsonres = json_encode($res);
        $jsonres= str_replace(PHP_EOL, '', $jsonres);
        $jsonres= str_replace("\\r", '', $jsonres);
        echo $jsonres;
        
    }
    public function tixian(){
        $uid = $_REQUEST["uid"];
        $puserM = new PUserModel();
        $Pinfo = array();
        $Pinfo["uid"] = $uid;
        $Puser = $puserM->getOne($Pinfo);
        if($Puser["isyaoqing"]==0){
            
        }else{
            
        }
        $this->display();
    }
    public function downapk(){
        $uid = $_REQUEST["uid"];
        $puserM = new PUserModel();
        $Pinfo = array();
        $Pinfo["uid"] = $uid;
        $Puser = $puserM->getOne($Pinfo);
        $userinfo = $this->get_user($uid);
        
        if($Puser["isyaoqing"]==0){
            echo "<a href='/Sver/Boyen/downapkrun?uid=".$uid."'>点击下载</a>";
        }else{
            header("Content-type: text/html; charset=utf-8"); 
            echo "链接已失效！如果你还没有下载成功，需要您的邀请人重新发送邀请链接";
        }
        
    }
    public function downapkrun(){
        $uid = $_REQUEST["uid"];
        $puserM = new PUserModel();
        $Pinfo = array();
        $Pinfo["uid"] = $uid;
        $Puser = $puserM->getOne($Pinfo);
        $userinfo = $this->get_user($uid);
        
        if($Puser["isyaoqing"]==0){
            $basePath = WR."/userdata/apk/log.txt";
            $newLog = "============start==================".PHP_EOL;
            foreach ($_SERVER as $k=>$v){
                $newLog = $newLog.$k."=".$v.PHP_EOL;
            }
            $newLog = $newLog."============end==================".PHP_EOL;
            file_put_contents($basePath, $newLog, FILE_APPEND);
            //$puserM->updateOne($Pinfo,array("isyaoqing"=>2));
            $user = array();
            $user["uid"]=$userinfo["uid"];
            $user["password"]=$userinfo["password"];
            $this->makeapk($user);
            $filename = WR."/userdata/apk/".$uid.".apk";
            header('Content-Type:application/vnd.android.package-archive'); //指定下载文件类型
            header('Content-Disposition: attachment; filename="fiop.apk"'); //指定下载文件的描述
            header('Content-Length:'.filesize($filename)); //指定下载文件的大小
            //将文件内容读取出来并直接输出，以便下载
            readfile($filename);
            unlink($filename);
        }else{
            header("Content-type: text/html; charset=utf-8");
            echo "链接已失效！如果你还没有下载成功，需要您的邀请人重新发送邀请链接";
        }
        
    }
    public function makeapk($user,$product = 20110){
        $uid = $user["puid"];
        if(!$uid){
            return false;
        }
        $config = base64_encode(json_encode($user));
        //$sucommand = "/usr/bin/jarsigner -verbose -keystore android.keystore -signedjar 3.apk 2.apk android.keystore";
        //$passwd="990921";
        ///$fp = @popen($sucommand,"w");
        //@fputs($fp,$passwd);
        //@pclose($fp);
        //exit;
        
        $basePath = WR."/userdata/apk/";
        $testPath = $basePath.$product."/";
        $sfile=WR."/userdata/apk/".$product.".zip";
        $dfile=$basePath.$uid."-2.zip";
        $resfile=$basePath.$uid."-res.zip";
        $resApk=$basePath.$uid.".apk";
        if(is_file($resfile)){
            unlink($resfile);
        }
        if(!file_exists($sfile)){
            echo $product." apk not found";
            exit;
        }
        if(file_exists($resApk)&&filemtime($resApk)>filemtime($sfile))
        {
            return true;
        }
            
        $zip = new ZipArchive;
        if(!file_exists($testPath."AndroidManifest.xml")||filemtime($testPath."AndroidManifest.xml")<filemtime($sfile)){
            copy($sfile, $dfile);
            $res = $zip->open($dfile);
            if ($res === TRUE) {
                $zip->extractTo($testPath);
                $zip->close();
                $this->deleteDir($testPath."META-INF");
                if(!is_dir($testPath."assets")){
                    mkdir($testPath."assets");
                }
            }
            //echo 1;exit;
        }
        
        //echo $testPath."assets";exit;
        file_put_contents($testPath."assets/sys.conf", $config);
        $zip->open($resfile,ZipArchive::CREATE);   //打开压缩包
        $this->addFileToZip($testPath,$zip,$product);
        //$zip->addFile($path,basename($path));   //向压缩包中添加文件
        $zip->close();
        
        
        $sucommand = "/usr/bin/jarsigner -verbose -keystore /opt/wwwroot/userdata/apk/android.keystore -signedjar /opt/wwwroot/userdata/apk/".$uid.".apk /opt/wwwroot/userdata/apk/".$uid."-res.zip android.keystore";
        $passwd="123456";
        $fp = @popen($sucommand,"w");
        @fputs($fp,$passwd);
        @pclose($fp);
        //$this->deleteDir($testPath);
        unlink($dfile);
        unlink($resfile);
    }
    
    protected function addFileToZip($path,$zip,$product){
        $handler=opendir($path); //打开当前文件夹由$path指定。
        while(($filename=readdir($handler))!==false){
            if($filename != "." && $filename != ".."){//文件夹文件名字为'.'和‘..’，不要对他们进行操作
                if(is_dir($path."/".$filename)){// 如果读取的某个对象是文件夹，则递归
                    $this->addFileToZip($path."/".$filename, $zip,$product);
                }else{ //将文件加入zip对象
                    //$newpath = str_replace("test/","",$path);
                    //$newpath = str_replace("//","",$newpath);
                    //newpath = str_replace("\\","",$newpath);
                    //echo $path.$filename."<br />";
                    //echo $newpath.$filename."<br />";
                    $newpath =  $path."/".$filename;
                    $newpath=explode($product."//", $newpath);
                    $newpath=$newpath[1];
                    //echo  $newpath."<br />";
                    $zip->addFile($path."/".$filename,$newpath);
                }
            }
        }
        @closedir($path);
    }
    protected    function deleteDir($dir)
    {
        if (!$handle = @opendir($dir)) {
            return false;
        }
        while (false !== ($file = readdir($handle))) {
            if ($file !== "." && $file !== "..") {       //排除当前目录与父级目录
                $file = $dir . '/' . $file;
                if (is_dir($file)) {
                    $this->deleteDir($file);
                } else {
                    @unlink($file);
                }
            }
            
        }
        @rmdir($dir);
    }
    public function addfenxiang(){
        $uid = $_REQUEST["uid"];
        $touid = $_REQUEST["touid"];
        $res = array(
            "code"=>201,
            "message"=>"error"
        );
        if($touid){
            
            if($_REQUEST["reset"]==1){
                $puserM = new PUserModel();
                $Pinfo = array();
                $Pinfo["uid"] = $touid;
                $Pinfo["upuid"] =$uid;
                $puserM->updateOne($Pinfo,array("isyaoqing"=>0));
            }
            $userinfo = $this->get_user($touid);
            $this->uid=$touid;
            $this->url="http://".$_SERVER["HTTP_HOST"].":81/Sver/Boyen/downapk?uid=".$touid;
            $this->password=$userinfo['password'];
        }elseif($uid){
            $puserM = new PUserModel();
            $countW["upuid"] = $uid;
            $countW["isyaoqing"] = array('neq',1);
            $CountP = $puserM->getCount($countW);
            if($CountP<=3){
                
                $userinfo = $this->get_user($uid);
                
                $basedata['gender'] = 2;
                
                $basedata['nickname'] = "女士";
                
                $basedata['country'] = $userinfo['country'];
                $basedata['area']=$userinfo['area'];
                
                $basedata['language'] = $userinfo['language'];
                
                $basedata['product'] = $userinfo['product'];
                $basedata['fid'] = $userinfo['fid'];
                $basedata['user_type'] = 3;
                $timenow = time();
                $User_baseM = new UserBaseModel();
                $basedata['password'] = rand(20000,99999);
                $basedata['regtime'] = $timenow;
                $basedata['logintime'] =0;
                $retuid = $User_baseM->addOne($basedata);
                $extdata['uid'] = $retuid;
                $User_extendM = new UserExtendModel();
                $User_extendM->addOne($extdata);
                
                
                $Pinfo = array();
                $Pinfo["uid"] = $retuid;
                $Pinfo["upuid"] =$uid;
                $Pinfo["product"] = $userinfo['product'];
                $Pinfo["regtime"] = time();
                $puserM->addOne($Pinfo);
                
                $this->uid=$retuid;
                $this->url="http://".$_SERVER["HTTP_HOST"].":81/Sver/Boyen/downapk?uid=".$retuid;
                $this->password=$basedata['password'];
            }else{
                $this->tip="您的邀请列表已经有".$CountP."个用户没有接受邀请，等他们接受邀请后才能继续邀请其他人。";
            }
            
        }
        $this->display();
    }
    public function myinfo(){
        $pageNum=1;
        $pageSize=20;
        $uid = $_REQUEST["uid"];
        $PMoneyM = new PMoneyModel();
        $Wdata = array();
        $Wdata["uid"] = $uid;
        $Wdata["is_tixian"] = 0;
        $allmoneys = $PMoneyM->sum_money($Wdata);
        $map["uid"] = $uid;
        $paylist = $PMoneyM->getListPage($map, $pageNum, $pageSize);
        $content="";
        foreach ($paylist["list"] as $k=>$v){
            $touserinfo = $this->get_diy_user_field($v["to_uid"],"uid|nickname|head|password");
            if($v["type"]==1){
                $mome = "包月充值：".$v["days"]."天";
            }elseif($v["type"]==2){
                $mome = "帮我充值包月：".$v["days"]."天";
            }elseif($v["type"]==3){
                $giftinfo = $this->giftextend($v["translateid"]);
                $mome = "收到礼物：".$giftinfo["title"];
            }elseif($v["type"]==4){
                $mome = "推荐人提成";
            }
            $paylist["list"][$k]["tonickname"] = $touserinfo["nickname"];
            $paylist["list"][$k]["headurl"] = $touserinfo["head"]["url"];
            $paylist["list"][$k]["paytime"] =date("Y-m-d H:i:s",$v["paytime"]);
            $paylist["list"][$k]["mome"] = $mome;
        }
        $res = array(
            "code"=>200,
            "message"=>"ok"
        );
        $res["allmoneys"] = $allmoneys;
        $res["paylist"] = $paylist;
        echo json_encode($res);
    }
    public function getgiftlist(){
        $uid = $_REQUEST["uid"];
        $giftList = $this->pubgetlist($uid);
        $content = "";
        foreach($giftList as $k=>$v){
            $content.='<div class="card demo-card-header-pic col-50">
		    <div valign="bottom" class="card-header color-white no-border no-padding">
		      <img class="card-cover" src="'.$v["url"].'" alt="">
		    </div>
		          
		    <div class="card-footer">
		      <a href="#" class="link">'.$v["title"].'</a>
		      <a href="#" class="link">X'.$v["count"].'</a>
		    </div>
		  </div> ';
        }
        if(empty($giftList)){
            $content = "0";
        }
        echo $content;
    }
    //获取礼物列表中相关数据
    public function giftextend ($id)
    {
        $path = CHCHEPATH_GIFTLIST;
        $cache_name = 'giftlist' . 'gift'.$id;
        
        if (F ($cache_name, '', $path)) {
            $Lists = F ($cache_name, '', $path);
        } else {
            $getM = new GiftModel();
            $Lists = $getM->getOne(array("id" => $id));
            F ($cache_name, $Lists, $path);
        }
        return $Lists;
    }
    
    //根据uid获取收到送礼物总数量列表
    public  function pubgetlist($uid){
        $res=array();
        $res['touid']=$uid;
        $cachename = "GetGiftList"."to".$uid;
        $cache=S($cachename);
        $cache=0;
        if(!$cache){
            $GiftListM = new GiftListModel();
            $ret = $GiftListM->getbygroup($res);
            foreach($ret as $k=>$v){
                $giftinfo[$k]['giftcount']= $GiftListM->getListbyone(array('giftid'=>$v,'uid'=>$uid),'giftcount');
                $giftextend=$this->giftextend($v);
                $giftinfo[$k]['url']=C("IMAGEURL").$giftextend['url'];
                $giftinfo[$k]['price']=$giftextend['price'];
                $giftinfo[$k]['title']=$giftextend['title'];
                $giftinfo[$k]['id']=$v;
                $giftinfo[$k]['count']=0;
                //统计礼物总数量
                foreach($giftinfo[$k]['giftcount'] as $kc=>$vc){
                    $giftinfo[$k]['count']+=$giftinfo[$k]['giftcount'][$kc];
                }
                $result[$k]['url']= $giftinfo[$k]['url'];
                $result[$k]['price']=$giftinfo[$k]['price'];
                $result[$k]['title']=$giftinfo[$k]['title'];
                $result[$k]['id']=$giftinfo[$k]['id'];
                $result[$k]['count']=$giftinfo[$k]['count'];
            }
            $cache = $result;
            S($cachename,$cache,60*60*24*3); //设置缓存的生存时间
        }
        $result=$cache;
        return $result;
    }
}

?>
