<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/26
 * Time: 9:23
 */

class IndexController extends WebBaseController
{
    public function __construct(){
        parent::__construct();
    }
    public function index(){

        $this->display();
    }
    public function help_center(){
        $Articlecate = M('article_cate');
        $articles = $Articlecate->where(array('pid'=>5))->select();
        $this->assign('cates', $articles);
        $this->display();
    }
    public function mobile_web(){

        $this->display();
    }
    public function Product_display(){

        $this->display();
    }



}