<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2012 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
/**
 * 语言检测 并自动加载语言包
 */
class CheckLangBehavior {

    // 行为扩展的执行入口必须是run
    public function run($langSet='english'){
        // 检测语言
        $this->checkLanguage($langSet);
      
    }

    /**
     * 语言检查
     * 检查浏览器支持语言，并自动加载语言包
     * @access private
     * @return void
     */
    private function checkLanguage($langSet) {
    	
    	
        // 不开启语言包功能，仅仅加载框架语言文件直接返回
       // if (!C('LANG_SWITCH_ON',null,false)){
        //    return;
       // }
        

       
        // 定义当前语言
        define('LANG_SET',strtolower($langSet));
        

        // 读取应用公共语言包
        $cachefile   =  C("LANG_PATH")."cache/".LANG_SET.'.php';

        if(is_file($cachefile)){
            $this->L(include $cachefile);
        }
    }
   public function L($name=null, $value=null) {
    	static $_lang = array();
    	
    	// 空参数返回所有定义
    	if (empty($name))
    		//return $_lang;
            return '';
    	// 判断语言获取(或设置)
    	// 若不存在,直接返回全大写$name
    	if (is_string($name)) {
    		$name = strtoupper($name);
    		if (is_null($value)){
    			return isset($_lang[$name]) ? $_lang[$name] : $name;
    		}
    		$_lang[$name] = $value; // 语言定义
    		return;
    	}
    	// 批量定义
    	if (is_array($name)){
    		$_lang = array_merge($_lang, array_change_key_case($name, CASE_UPPER));
    	}
    	return;
    }
}
