<?php
return array(
    'NO_LOGIN'=>array(
        "userauth/login",
        "userauth/regist",
        "userauth/getpassword",
        "userauth/getviptime",
        "userauth/gethelpworld",
        "sys/parameterinit",
        "caiji/run",
        "caiji/ico",
        "caiji/ico1",
        "adm/token",
        "sys/clientruntime",
        "public/userheard",
        "automsg/test",
        "automsg/sendxmppmsg",
        "automsg/rundmsg",
        "automsg/reply",
        "automsg/firstsend",
        "automsg/publicsendsysmsg",
        "adm/addarea",
        "automsg/testsendmsg",
        "recharge/paypalreturn",
        "picture/showpicture",
        "picture/collection",
        "picture/picturebox",
        "alipay/webpay",
        "alipay/msg",
        "hmsg/login",
        "hmsg/getmsglist",
        "hmsg/sendmsg",
        "hmsg/all",
        "hmsg/pushmsg",
        "Online/putPushonlineToredis",
        "sys/tximkey",
        "userauth/sendtzmsg",
        "msg/sendhellotothree",
        "recharge/mycardreturnc",
        "recharge/mycardreturn",
        "recharge/purchase",
        "recharge/mycarddfreturnc",
        "recharge/mycarddfreturn",
        "recharge/recharge",
        "chat/getmydata",
        "chat/unsetuidtype1",
        "chat/onlinecallback",
        "chat/userinfo",
        "user/screening",
    ),
    'NO_LOGIN_CONTROLLER'=>array(
        "boyen",
        "adm",
        "automsg",
        "userapi",
	    'shortvideo',
        'shortvodoperate'
        ),
    'ALIPAY_CONFIG'              => array(
        'APPID'                  => '2016091700533196', // 支付宝支付APPID
        'RSAPRIVATEKEY'          => 'MIIEpQIBAAKCAQEA04IVPO/4XyypnXMnn1c8aazHFv/2qUnMYZ2+5s+VfeX9ok8RDQzgbXBUcB5siBWQz01DYZ/ZXq9tpO9vdHxF7ZgcRvBFTRh1TVRgEigIbPCOy2Xg08YjKqgWjKx0ZCX2LMSEQ0G9sddIKOeje11FrVkedamcrgEwvXVFNEJvzrVgIr2VV/KRmJkIAWedn8nhMYd64crpIN9PI8iKA/e67L16HGwUd8/XQAnCKleu8hJE2FnbXEdW0Zxufdwy0ci+VzgCzAcMcD6hpDPxhavewIkUhnfUnag6pNr/QA2VwKbmMLec5csEGqGUBtEgzNmf7fEm9/H1sjxrq7fD/uh38QIDAQABAoIBAQCtjUAAEKis7+j92U/Z36siyzNSHBY6MuQR9W5/1jXEaMQGOvqyyq4dHt8qYMyNnaSFddCkepXSrAA8Jnw+CPsYSohpbR5wdHFMUr/CJjdDrOB+5cnl+98/UU2vgJLA2qV1dotQ5NDS7OO5LNlokNBrvYTQ56w426Ey1NdEWn+EDLGMJvUTqimSRQ2Vd+HEX7sBlsbjF/XWHlNQK4JDvw+J4h9kIj/2T2IHS+PYgA+yrZ0vXUwOgU6oWQqbszSEPkKNLrntrbjZc2bAfEJQPYlEPYXEk6QOt13w5/Kdk4rniM8SYa/GCbpWaizm/uH/Bm8oQTi9wA1ZIiwXI0G4aFQBAoGBAPd5kXASJaCljfaaRB2FrZfCR7f/6vRHqA3PhjcDGIQHf618XRPUM2iAF34Of1sgVn03eOUB36Vg40Ie6Sx3oGPeeuwSLbIniToHTVWquHcsOErqQWkW6fH+GD1C6K72ZuFh33pwmu2Z+zRRACIDLB2H3qJhPrRIORv4p/VQlV5JAoGBANrLVNA4BI6OvJ2nZfXgEEFawF5LOGvBboIUNSK3cJpnev58QH34dxIZEpWWYWP0LJXAvkBogjaJIvTSbhxnx0iEid7CfukQg74Le21o2iXWjJuYSIHBkQ5X+ahyQsuLn1l0DHOdqfRg8U6kn14B7Pc/EpbftApgkgZht7cW7mxpAoGBANouCKYoqZnXO7DFUvZAK8qXE0nKS6ewIR6D7o1DonKMdelKsnjP5b7k51FQkMGTYg0lQeWM4FLad6GMxQBaO0KxG+hTIoxlxNsvIuQ7DkSq7LpHiwGOnRSnc1FXUvMl85ATDMbSzhRaZBk8rIRQDa2/Yu4AFBs3bXoWoGVr9TRZAoGBALSEvt1s2pWkPwr0arOKwt6pOb6S4WJGnh7E/XI/V7CNYHVHnw5QSi2sciDpikEl3QNRm6cYkxbb3vOQCzl0hn/WmsooPv2cmpXac2O59wLxGeHmqywjwMLM4zePznySHk936C4cD3WJ5o60VKkDfbghWf0RZUKKmvCY/zMYwG7BAoGAXoMTaCJ0+nRhPi4pcxa2mpofL0W6h3txzwhv98N8geHV1qHqrlz5Bw/ICJGdhDW9zKOP5n0I3Vxc+qqWTgLWE/3olu+FQPWKcavPk2TY2njrk/hCuumV/sEWAhIc3eqzLu9cSOOYipek3bo61XVIuzWdhtR+shx59AVmOA/Lk7A=', // 开发者私钥去头去尾去回车，一行字符串
        'ALIPAYRSAPUBLICKEY'     => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAofAs/5ZKnkeWMOzKpu6PoQ575IPtKBZxvkK4UGiD32OCGMiCsTYB9SAFI5tP+j+Pv7arkK/b9LDPBWjU8v+AbnSYRBE8vcl6T9i7jDyG8j7BraNz/hD8hav3BALjl9iLN4rWNzW/4TwNyHayW9QDeNQKDU6eBVpUp39JkmMnCYBCVN/+SeFNXoS/MbDgLCEuK6ghEeU3jurXaHxtG+sL09ybRmrI1hrLBIGipZVAJRX42xgTCDsDJRz/PDG/zLo3/jNfhXwKvCMrFmoLuAp2myhJUAIcy9GTRyv7ZvC3K+RrbkZ3MWB6M23AEAljJuFNA5pPHALMxORDpTb1CXzoCQIDAQAB', // 支付宝公钥
        //沙箱版本测试链接
        'GATEWAYURL' => "https://openapi.alipaydev.com/gateway.do",
        //正式版本链接
        //'GATEWAYURL' =>"https://openapi.alipay.com/gateway.do",
        //网页调用支付宝支付回调地址
        'WEB_NOTIFY_URL' => "http://39.104.188.158:81/api/alipay/notify_url",
        'RETURN_URL' => "http://39.104.188.158:81/adms/pay/paylist",
        //app调用支付宝支付回调地址
        'NOTIFY_URL' => "http://39.104.188.158:81/api/alipay/msg",
    ),
);
