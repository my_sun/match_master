<?php
/**
 * 客户端版本
 */

class VersionController extends BaseController
{
    public function __construct ()
    {
        parent::__construct ();
        
    }
    /**
     * 检测是否需要更新版本
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @return data
     */
    public function update(){
        import("Sver.lib.Util.ApkParser");
        $appObj  = new Apkparser();
        $uid = $this->uid;
        $platforminfo=$this->platforminfo;
        $UserInfo = $this->UserInfo;
        if($UserInfo["user_type"]==3){
            $laiguan=C("COMMON_SWITCH_ON");
            if($laiguan){
                $result['Isneedupdate'] = 0;
                $result['message'] ="本次更新新增功能有：。。。。。。！";
                //强制更新
                $product = $platforminfo['product'];
                $version = $platforminfo['version'];
                $version = str_replace('.', '', $version);
                // if (in_array($version, array( "1.1.3"))) {
                if($product=='20114'&$version<113)
                {
                    $filename = WR . "/userdata/apk/" . $product . ".zip";
                    if (file_exists($filename)) {
                        $appObj->open($filename);
                        $newVersionName = $appObj->getVersionName();  // 版本名称
                        $packagename=$appObj->getPackage();//包名
                        //$newVersionName='1.0.5';// 版本名称
                        $newVersionNameN = str_replace('.', '', $newVersionName);
                        $versionN = str_replace('.', '', $version);
                        if ($newVersionNameN > $versionN) {
                            $result['Isneedupdate'] = 1;
                            $result['url'] = 'http://47.52.73.213:81/Sver/Downapk/onlineapk?product=' . $product;//user_type=1时候使用这个
                            $result['googleurl'] ='https://play.google.com/store/apps/details?id='.$packagename;
                        } else {
                            //版本号一致，不需要更新
                            $result['Isneedupdate'] = 0;
                        }
                    } else {
                        //未检测到安装包
                        $result['Isneedupdate'] = 0;
                    }

                }elseif($product=='24111'&$version<105){
                    $filename = WR . "/userdata/apk/" . $product . ".zip";
                    if (file_exists($filename)) {
                        $appObj->open($filename);
                        $newVersionName = $appObj->getVersionName();  // 版本名称
                        $packagename=$appObj->getPackage();//包名
                        //$newVersionName='1.0.5';// 版本名称
                        $newVersionNameN = str_replace('.', '', $newVersionName);
                        $versionN = str_replace('.', '', $version);
                        if ($newVersionNameN > $versionN) {
                            $result['Isneedupdate'] = 1;
                            $result['url'] = 'http://47.52.73.213:81/Sver/Downapk/onlineapk?product=' . $product;//user_type=1时候使用这个
                            $result['googleurl'] ='https://play.google.com/store/apps/details?id='.$packagename;
                        } else {
                            //版本号一致，不需要更新
                            $result['Isneedupdate'] = 0;
                        }
                    } else {
                        //未检测到安装包
                        $result['Isneedupdate'] = 0;
                    }
                }
                else {
                    //不更新
                    $result['Isneedupdate'] = 0;
                }
            }else{
                $result['Isneedupdate'] = 0;
            }

        }elseif($UserInfo["user_type"]==1) {
                $result['Isneedupdate'] = 0;
                $result['message'] ="";
                //强制更新
                $product = $platforminfo['product'];
                $version = $platforminfo['version'];
                $version = str_replace('.', '', $version);

                $updateinfo=M('version_update')->select();

                foreach($updateinfo as $k=>$v){
                    $update_version=str_replace('.', '', $v['version']);
                    if($product==$v['product']&$version<$update_version){
                        if($v['isopen']==0){
                            break;
                        }
                        $filename = WR . "/userdata/apk/" . $product . ".zip";
                        if (file_exists($filename)) {
                            $appObj->open($filename);
                            $newVersionName = $appObj->getVersionName();  // 版本名称
                            $packagename=$appObj->getPackage();//包名
                            //$newVersionName='1.0.5';// 版本名称
                            $newVersionNameN = str_replace('.', '', $newVersionName);
                            $versionN = str_replace('.', '', $version);
                            /*if ($newVersionNameN > $versionN) {*/
                                $result['Isneedupdate'] = $v['method'];
                                $result['url'] = 'http://47.52.73.213:81/Sver/Downapk/onlineapk?product=' . $product;//user_type=1时候使用这个
                                $result['googleurl'] ='https://play.google.com/store/apps/details?id='.$packagename;
                                $result['message']=$v['promptword'];
                            /*} else {
                                //版本号一致，不需要更新
                                $result['Isneedupdate'] = 0;
                            }*/
                        } else {
                            //未检测到安装包
                            $result['Isneedupdate'] = 0;
                        }
                        break;
                    }

                }
                if($product=='24110'){
                    $product='24111';
                    $version='1.0.4';
                    $filename = WR . "/userdata/apk/" . $product . ".zip";
                    if (file_exists($filename)) {
                        $appObj->open($filename);
                        $newVersionName = $appObj->getVersionName();  // 版本名称
                        $packagename=$appObj->getPackage();//包名
                        //$newVersionName='1.0.5';// 版本名称
                        $newVersionNameN = str_replace('.', '', $newVersionName);
                        $versionN = str_replace('.', '', $version);
                        if ($newVersionNameN > $versionN) {
                            $result['Isneedupdate'] = 1;
                            $result['url'] = 'http://47.52.73.213:81/Sver/Downapk/onlineapk?product=' . $product;//user_type=1时候使用这个
                            $result['googleurl'] ='https://play.google.com/store/apps/details?id='.$packagename;
                        } else {
                            //版本号一致，不需要更新
                            $result['Isneedupdate'] = 0;
                        }
                    } else {
                        //未检测到安装包
                        $result['Isneedupdate'] = 0;
                    }
                }


        }
        else{
            //不更新
            $result['Isneedupdate']=0;
        }
            $return['message'] = $this->L("CHENGGONG");
            $return['data'] = $result;

            Push_data($return);
        }




}