<?php
require_once  WR.'/lib/aliyuncs/aliyun-oss-php-sdk/autoload.php';
include_once   WR.'/lib/aliyuncs/aliyun-php-sdk-core/Config.php';
use Green\Request\V20180509 as Green;
use Green\Request\Extension\ClientUploader;
class CheckvideoController extends BaseController
{
     //本地同步视频审核
     public function checkvideo($url){
        //请替换成您的accessKeyId、accessKeySecret
        $iClientProfile = DefaultProfile::getProfile("cn-shanghai", "LTAIR8tjvGtclDfZ", "gMJGibkaW2t44UnflpauLK7rADmdWB");
        DefaultProfile::addEndpoint("cn-shanghai", "cn-shanghai", "Green", "green.cn-shanghai.aliyuncs.com");
        $client = new DefaultAcsClient($iClientProfile);
        $request = new Green\VideoAsyncScanRequest();
        $request->setMethod("POST");
        $request->setAcceptFormat("JSON");

        // 计费按照该处传递的场景进行
        // 一次请求中可以同时检测多个视频，每个视频可以同时检测多个风险场景，计费按照场景和视频截帧数计算
        // 例如：检测2个视频，场景传递porn,terrorism，计费会按照2个视频的截帧总数乘以鉴黄场景的价格加上2个视频的截帧总数乘以暴恐检测场景的费用计算//本地文件先进行上传，然后进行检测
        $task1 = array('dataId' =>  uniqid(),
            'url' => $url
        );
        $request->setContent(json_encode(array("tasks" => array($task1),
            "scenes" => array("porn", "terrorism"))));
        try {
            $response = $client->getAcsResponse($request);
            print_r($response);
            if(200 == $response->code){
                $taskResults = $response->data;
                foreach ($taskResults as $taskResult) {
                    if(200 == $taskResult->code){
                        $taskId = $taskResult->taskId;
                        // 将taskId保存下来，间隔一段时间来轮询结果。具体请参照VideoAsyncScanResultsRequest接口说明
                        print_r($taskId);
                    }else{
                        print_r("task process fail:" + $response->code);
                    }
                }
            }else{
                print_r("detect not success. code:" + $response->code);
            }
        } catch (Exception $e) {
            print_r($e);
        }
   }

   //提交二进制视频审核
   public function checkvideo2($videourl,$scenes){
      $iClientProfile = DefaultProfile::getProfile("cn-shanghai", "LTAIT4HmYwaIgrsq", "TPcfdA6JRFdTL6GLnCj2mHB7cx9IeD");
      DefaultProfile::addEndpoint("cn-shanghai", "cn-shanghai", "Green", "green.cn-shanghai.aliyuncs.com");
      $client = new DefaultAcsClient($iClientProfile);
      $request = new Green\VideoAsyncScanRequest();
      $request->setMethod("POST");
      $request->setAcceptFormat("JSON");

      // 计费按照该处传递的场景进行
      // 一次请求中可以同时检测多个视频，每个视频可以同时检测多个风险场景，计费按照场景和视频截帧数计算
      // 例如：检测2个视频，场景传递porn,terrorism，计费会按照2个视频的截帧总数乘以鉴黄场景的价格加上2个视频的截帧总数乘以暴恐检测场景的费用计算//本地文件先进行上传，然后进行检测
      $uploader = ClientUploader::getVideoClientUploader($client);
      $url = $uploader->uploadFile($videourl);
      $task1 = array('dataId' =>  uniqid(),
          'url' => $url
      );
      $request->setContent(json_encode(array("tasks" => array($task1),
          "scenes" =>$scenes)));
      try {
          $response = $client->getAcsResponse($request);
          // print_r($response);
          if(200 == $response->code){
              $taskResults = $response->data;
              foreach ($taskResults as $taskResult) {
                  if(200 == $taskResult->code){
                      $taskId = $taskResult->taskId;
                      // 将taskId保存下来，间隔一段时间来轮询结果。具体请参照VideoAsyncScanResultsRequest接口说明
                      // print_r($taskId);
                      return $taskResult;
                  }else{
                      print_r("task process fail:" + $response->code);
                  }
              }
          }else{
              print_r("detect not success. code:" + $response->code);
          }
      } catch (Exception $e) {
          print_r($e);
      }
   }

   //提交视频语音进行综合检测
   public function videovoice($videourl){
      $iClientProfile = DefaultProfile::getProfile("cn-shanghai", "LTAIR8tjvGtclDfZ", "gMJGibkaW2t44UnflpauLK7rADmdWB");
      DefaultProfile::addEndpoint("cn-shanghai", "cn-shanghai", "Green", "green.cn-shanghai.aliyuncs.com");
      $client = new DefaultAcsClient($iClientProfile);
      $request = new Green\VideoAsyncScanRequest();
      $request->setMethod("POST");
      $request->setAcceptFormat("JSON");

      # 设置待检测视频，默认一次请求只支持1个task，如果需要开放到更多，请通过工单联系我们。
      # 计费按照该处传递的场景进行
      # 一次请求中可以同时检测多个视频，每个视频可以同时检测多个风险场景，计费按照场景和视频截帧数计算
      # 例如：检测2个视频，场景传递porn,terrorism，计费会按照2个视频的截帧总数乘以鉴黄场景的价格加上2个视频的截帧总数乘以暴恐检测场景的费用计算
      # 如果同时检测视频画面和视频中的语音，视频中的画面还是按照上述示例计费，语音部分按照视频语音的总时长进行计费
      $task1 = array('dataId' =>  uniqid(),
          'url' => $videourl
      );
      $request->setContent(json_encode(array("tasks" => array($task1),
          "scenes" => array("porn", "terrorism"), "audioScenes" => 'antispam')));
      try {
          $response = $client->getAcsResponse($request);
          // print_r($response);
          if(200 == $response->code){
              $taskResults = $response->data;
              foreach ($taskResults as $taskResult) {
                  if(200 == $taskResult->code){
                      $taskId = $taskResult->taskId;
                      // 将taskId保存下来，间隔一段时间来轮询结果。具体请参照VideoAsyncScanResultsRequest接口说明
                      // print_r($taskId);
                      return $taskResult;
                  }else{
                      print_r("task process fail:" + $response->code);
                  }
              }
          }else{
              print_r("detect not success. code:" + $response->code);
          }
      } catch (Exception $e) {
          print_r($e);
      }
   }


}



