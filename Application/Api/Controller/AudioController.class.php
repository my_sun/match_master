<?php
class AudioController extends BaseController {
	public function __construct(){
		parent::__construct();
		//$this->is_login();//检查用户是否登陆
		$this->fileurl = '/userdata/renzheng/'.date('Ymd',time())."/";
	}
	/**
	 * 语音认证
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int ltime 时长
	 * @request file file 语音文件
	 * @return data
	 */
	public function upload(){
		$data = $this->Api_recive_date;
		$ltime = $data["ltime"] ? $data["ltime"] : 0;
		$uid =$this->uid;
		$Checkvoice=new CheckvoiceController();		
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] =  $this->L("SHANGCHUANSHIBAI");
		$saveurl = $this->fileurl.'audio/';
		$savePath = WR.$saveurl;
		createDir($savePath);
		
		if(!empty($_FILES)){
		    
			$uploadList = $this->_upload($savePath);
			if($uploadList){
				$imageurlfile = array();
				$videofile = array();
				
				foreach($uploadList as $k=>$v){
					$videofile = $v;
				}
				$VideoM = new AudioModel();
				$Indata = array();
				$Indata['uid'] = $uid;
				$Indata['type'] = 1;//
				$Indata['ltime'] = $ltime;//
				$Indata['time'] = time();//
				$Indata['url'] = $saveurl.$videofile['savename'];
				$Checkvoice=$Checkvoice->localvoice(WR.$Indata['url']);
				// dump($Checkvoice);die;
				if($Checkvoice->code == 200 & $Checkvoice->msg == "OK"){
					$Indata['status'] = 1;//
					$ret = $VideoM->addOne($Indata);
					PutOss($Indata['url']);
					$this->get_user_audio($uid,1);
	                //更新数据库状态
	                $userinfoM=new UserBaseModel();
	                $userinfoM->updateOne(array('uid'=>$uid),array('isaudio'=>2));
	                //更新缓存
	                $this->set_user_field($uid,'isaudio',2);
					$return = array();
					$this->sendsysmsg($uid, "该语音已通過審核");
					$return['message'] = "该语音已通過審核";
					Push_data($return);
				}
				 $this->sendsysmsg($uid, "该语音未通過審核");
	             _unlink(WR.$Indata['url']);
	             $return['message'] = "语音內容不合格！";
	             Push_data($return);
			}
		}
		 Push_data($return);
	}
	// 文件上传
	protected function _upload($savePath) {
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($savePath);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize = 3292200;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
		//设置附件上传目录
		$upload->savePath = $savePath;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
		$upload->saveRule = "md5content";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			return $uploadList;;
		}

	}
	/**
	 * 删除语音认证
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int id 语音id
	 * @return data
	 */
	public function delete(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id){
			//删除视频
			$VideoM = new VideoModel();
			$VideoInfo = $VideoM->getOne(array("id"=>$id));
			_unlink(WR.$VideoInfo["imageurl"]);
			_unlink(WR.$VideoInfo["url"]);
			DelOss(array($VideoInfo["imageurl"],$VideoInfo["url"]));
			$VideoM->delOne(array("id"=>$id));
			$return = array();
		}
		Push_data($return);
	}

	/**
	 * 邮箱认证
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request string toemail 用户邮箱
	 * @request string title  邮件的主题
	 * @return data
	 */
	public function mailupdate()
	{
		$data = $this->Api_recive_date;
		$uid =$this->uid;
		$platforminfo = $this->platforminfo;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");

		//$platforminfo = $_POST['platforminfo'];//客户端信息对象

		//引入smtp类
		require_once "Smtp.class.php";		

		//******************** 配置信息 ********************************
		$smtpserver = "ssl://smtp.163.com";//SMTP服务器
		$smtpserverport =465;//SMTP服务器端口
		$smtpusermail = "17335711659@163.com";//SMTP服务器的用户邮箱
		$smtpemailto = $data['toemail'];//发送给谁
		$smtpuser = "17335711659@163.com";//SMTP服务器的用户帐号，注：部分邮箱只需@前面的用户名
		$smtppass = "191118zhang";//SMTP服务器的用户密码
		$mailtitle = $data['title'];//邮件主题
		$mailtype = "HTML";//邮件格式（HTML/TXT）,TXT为文本邮件
		//************************ 配置信息 ****************************
		if(empty($smtpemailto)){

			$return = [
				'code' => 4001,
				'message'  => "请输入邮箱"
			];
			Push_data($return);
		}
		if(empty($mailtitle)){

			$return = [
				'code' => 4001,
				'message'  => "请输入邮件主题"
			];
			Push_data($return);
		}
		$res = preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $smtpemailto);
		
		if($res == false){
			$return = [
				'code' => 4002,
				'message'  => "请输入正确的邮箱"
			];
			Push_data($return);
		}

		//连接redis
		$redis = new RedisModel();
		//设置key
		$key = "AUDIOEMAIL_".$uid;

		$values = $redis->get($key);

		$keys = "AUDIOEMAILIP_".$uid;

		$iskey = $redis->exists($keys);

//		if($iskey != 0){
//			$return = [
//				'code' => 4005,
//				'message'  => "你被禁了"
//			];
//			Push_data($return);
//			$this->sendsysmsg($uid, "您被禁了，请24小时后再次重试");
//		}
//
//		if($values >= 3){
//			$redis->setex($keys,1,86400);
//			$return = [
//				'code' => 4005,
//				'message'  => "操作太频繁"
//			];
//			Push_data($return);
//		}

		// 设置认证码
		$ran = rand('100000','999999');

		$rand = $ran;

		unset($ran);
		$title = M("products")->where(array("product"=>$platforminfo['product']))->find();
		$mailcontent = "<h1>"."【".$title['title']."】"."您的认证码是：".$rand."</h1>";//邮件内容

	    $smtp = new Smtp($smtpserver,$smtpserverport,true,$smtpuser,$smtppass);//这里面的一个true是表示使用身份验证,否则不使用身份验证.
		$smtp->debug = false;//是否显示发送的调试信息
		$state = $smtp->sendmail($smtpemailto, $smtpusermail, $mailtitle, $mailcontent, $mailtype);

		if($state==""){

			$return = [
				'code' => 4003,
				'message'  => "请检查正确的邮件"
			];
			Push_data($return);
		}
		$redis->setex($key,$time = 600);
		$kk =$redis->deinc($key);
		//定义成功返回的格式
		$return = [
			'code' => 200,
			'message'  => "发送邮件成功",
			'data'  => array('codes'=>$rand,'toemail'=>$smtpemailto)
		];
		
		Push_data($return);

	}

	/**
	 * 认证邮箱校验认证码
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request string toemail 用户邮箱

	 * @return data
	 */
	public function mailupdatecode()
	{
		$data = $this->Api_recive_date;
		$uid =$this->uid;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");

		$platforminfo = $data['platforminfo'];//客户端信息对象
		$toemail = $data['toemail'];//用户的邮箱

		if($toemail == "" || $platforminfo == ""){

			$return = [
				'code' => 4001,
				'message'  => "参数不能为空"
			];
			Push_data($return);
		}

		$Wdata['email'] = $toemail;

		$UserBase = new UserBaseModel();
		$ret = $UserBase->getOne($Wdata);

		if(!empty($ret)){
			$return = [
				'code' => 4003,
				'message'  => "该邮箱已认证"
			];
			Push_data($return);
			$this->sendsysmsg($uid, "该邮箱已认证");
		}

	    //修改密码
	    $sql =" update t_user_base set verified = 1 , email = '".$toemail."'  where uid = ".$uid;

	    $this->set_user_field($uid,'verified',1);

	    $res = M()->execute($sql);

	    if($res !== false){

	    	$return = [
				'code' => 200,
				'message'  => "认证邮箱成功"
			];
			Push_data($return);
			$this->sendsysmsg($uid, "认证邮箱成功");
	    }else{
	    	$return = [
				'code' => 4003,
				'message'  => "认证邮箱失败"
			];
			Push_data($return);
	    }

	}
	
	/**
	 * 手机认证
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request string phone 用户手机号
	 * @return data
	 */

	public function smsupdate(){
		$data = $this->Api_recive_date;
		$uid =$this->uid;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");

		$product = $data['platforminfo']['product'];//产品号
		$phone = $data['phone'];
		//用户名 AND 密码
		$userName = "cs_v97c0y";
		$pwd = "D5WRcCFQ";

		if(empty($phone)){

			$return = [
				'code' => 4001,
				'message'  => "请输入手机号"
			];
			Push_data($return);
		}
		//生成随机数
		$ran = rand('100000','999999');
		$rand = $ran;
		unset($ran);

		//生成时间
		$time = date("YmdHis");
		$times = $time;
		unset($time);


		//连接redis
		$redis = new RedisModel();
		//设置key
		$key = "AUDIOSENDSMS_".$uid;

		$values = $redis->get($key);

		$keys = "AUDIOSENDSMSIP_".$uid;

		$iskey = $redis->exists($keys);

		if($iskey != 0){
			$return = [
				'code' => 4005,
				'message'  => "你被禁了"
			];
			Push_data($return);
			$this->sendsysmsg($uid, "您被禁了，请24小时后再次重试");
		}

		if($values >= 3){
			$redis->setex($keys,1,86400);
			$return = [
				'code' => 4005,
				'message'  => "操作太频繁"
			];
			Push_data($return);
		}


		if(substr($phone,0,2) == '86'){
			$stamll = $this->sendcnphone($phone,$rand);
			if($stamll->Code == 'OK'){
				$return = [
					'code' => 200,
					'message'  => "发送短信成功",
					'data'  => array('codes'=>$rand,'phone'=>$phone)
				];
				Push_data($return);
			}else{
				$return = [
					'code' => 4003,
					'message'  => "短信发送失败",
				];
				Push_data($return);
			}
		}

		//生成sign签名
		$string = $userName.$pwd.$times;
		$sign = md5($string); 
		
		$url = "http://sms.skylinelabs.cc:20003/sendsmsV2";
		//参数
		$params = array(
			'account' => $userName, //用户名
		    'sign' => $sign, //sign签名
		    'datetime' => $times, //时间戳
		    // 'numbers' => $phone, //手机号 
		    "content" => "【快约会】您的校验码是：".$rand
		);

		$paramstring = http_build_query($params);
		$paramstring = $paramstring.'&numbers='.$phone;

		//get 请求
		$content = $this->juheCurl($url, $paramstring);
		$result = json_decode($content, true);
		$redis->setex($key,$time = 600);
		$kk =$redis->deinc($key);
		// dump($content);
		if($result['status'] == "-9") {
		    $return = [
				'code' => 4002,
				'message'  => "请输入正确的手机号",
			];
			Push_data($return);
		}
		
		if (empty($content)) {
			$return = [
				'code' => 4003,
				'message'  => "短信发送失败",
			];
			Push_data($return);
		}
		$return = [
			'code' => 200,
			'message'  => "发送短信成功",
			'data'  => array('codes'=>$rand,'phone'=>$phone)
		];
		Push_data($return); 
			
	}


	//发送中国手机号
	public function sendcnphone($phone,$code){
		$params = array ();

		// *** 需用户填写部分 ***
		// fixme 必填：是否启用https
		$security = false;

		// fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
		$accessKeyId = "LTAIR8tjvGtclDfZ";
		$accessKeySecret = "gMJGibkaW2t44UnflpauLK7rADmdWB";

		// fixme 必填: 短信接收号码
		$params["PhoneNumbers"] = $phone;

		// fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
		$params["SignName"] = "Circle";

		// fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
		$params["TemplateCode"] = "SMS_181851254";

		// fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
		$params['TemplateParam'] = Array (
			"code" => $code
		);


		// *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
		if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
			$params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
		}

		// 初始化SignatureHelper实例用于设置参数，签名以及发送请求
		$helper = new SignatureHelperController();

		// 此处可能会抛出异常，注意catch
		$content = $helper->request(
			$accessKeyId,
			$accessKeySecret,
			"dysmsapi.aliyuncs.com",
			array_merge($params, array(
				"RegionId" => "cn-hangzhou",
				"Action" => "SendSms",
				"Version" => "2017-05-25",
			)),
			$security
		);

		return $content;
	}


	/**
	 * 手机认证  校验验证码
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request string phone 用户手机号

	 * @return data
	 */
	public function phoneCode()
	{
		$data = $this->Api_recive_date;
		$uid =$this->uid;
		// dump($uid);die;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");

		$platforminfo = $data['platforminfo'];//客户端信息对象
		$phone = $data['phone'];//用户的邮箱

		if(!isset($platforminfo) || empty($platforminfo)){
			$return = [
				'code' => 4001,
				'message'  => "参数不全",
			];
			Push_data($return);
		}
		if(!isset($phone) || empty($phone)){
			$return = [
				'code' => 4002,
				'message'  => "手机号不能为空",
			];
			Push_data($return);
		}

		$sql = " select phonenumber from t_user_extend where phonenumber = ".$phone;

		$ret = M()->query($sql);

		if(!empty($ret)){
			$return = [
				'code' => 4003,
				'message'  => "该手机号已认证"
			];
			Push_data($return);
		}

	    //修改密码
	    $extend =" update t_user_extend set phonenumber = '".$phone."',isphonenumber = 1 where uid = ".$uid;
	    $res = M()->execute($extend);
	    if(!$res){
	    	$extend2 =" update t_user_extend_bak set phonenumber = '".$phone."',isphonenumber = 1 where uid = ".$uid;
	    	$res = M()->execute($extend2);
	    }
	    if($res !== false){
	    	$return = [
				'code' => 200,
				'message'  => "认证手机号成功"
			];
			Push_data($return);
			$this->sendsysmsg($uid, "认证手机号成功");
	    }else{
	    	$return = [
				'code' => 4003,
				'message'  => "认证手机号失败"
			];
			Push_data($return);
	    }
	}




	/**
	 * 请求接口返回内容
	 * @param  string $url [请求的URL地址]
	 * @param  string $params [请求的参数]
	 * @param  int $ipost [是否采用POST形式]
	 * @return  string
	 */
	function juheCurl($url, $params = false, $ispost = 0)
	{
	    $httpInfo = array();
	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	    curl_setopt($ch, CURLOPT_USERAGENT, 'JuheData');
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	    if ($ispost) {
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	        curl_setopt($ch, CURLOPT_URL, $url);
	    } else {
	        if ($params) {
	            curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);
	        } else {
	            curl_setopt($ch, CURLOPT_URL, $url);
	        }
	    }
	    $response = curl_exec($ch);
	    if ($response === FALSE) {
	        //echo "cURL Error: " . curl_error($ch);
	        return false;
	    }
	    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
	    curl_close($ch);
	    return $response;

	}

}

?>
