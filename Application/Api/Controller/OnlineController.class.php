<?php
class OnlineController extends BaseController
{
    public function __construct()
    {

    }

    //设置在线用户
    public function setUserOnline($userinfo)
    {
        $uid = $userinfo["uid"];
        $procuct = $userinfo["product"];


        $user_type = $userinfo["user_type"];
        if (!in_array($user_type, array(1,3,9,10,11,5))) {
            $user_type = 1;
        }

        $gender = $userinfo["gender"];
        if (!in_array($gender, array(1, 2))) {
            $gender = 1;
        }
        $useronlinelistkey = "useronlinelist_" . $procuct . "_" . $user_type . "_" . $gender;
        $redis = $this->redisconn();
        $redis->listRemove($useronlinelistkey, $uid, 0);
        $redis->listPush($useronlinelistkey, $uid);
        $totalCount = $redis->listSize($useronlinelistkey);
        if ($totalCount > 1000) {
            $redis->listPop($useronlinelistkey, 1);
        }
        return true;
    }

    //获取在线用户
    public function getUserOnline($procuct, $user_type, $gender, $pageNum = 1, $pageSize = 30, $type = 1)
    {
        $redis = $this->redisconn();
        $useronlinelistkey = "useronlinelist_" . $procuct . "_" . $user_type . "_" . $gender;
        $useronlinelisttimekey = $useronlinelistkey . "time";
        $alluseronlinelistkey = "useronlinelist";
        $onlinelista=array();
        $pushonlinea=array();

        $ret = array();

        if (!$redis->exists($useronlinelisttimekey)||$type == 2) {//用户类型为2时，只返回状态为'Online'的用户
            $allonlinelist = $redis->listGet($alluseronlinelistkey, 0, -1);
            $useronlinelist = $redis->listGet($useronlinelistkey, 0, 499);



            $IMSDKIDS=M('txmy')->getField('product',true);
            $txkey=M('txmy')->where(array('product'=>$procuct))->getField('miyao');
             if (in_array($procuct, $IMSDKIDS)) {
               // $user_tx_onlinel="user_tx_onlinel_" . $procuct . "_" . $user_type . "_" . $gender;
                //$user_tx_push_onlinel="user_tx_push_onlinel_" . $procuct . "_" . $user_type . "_" . $gender;
               $tentxunstatus = tencent_onlinestate($useronlinelist, $txkey);


                foreach ($tentxunstatus as $k1 => $v1) {
                    if ($v1["State"] == "Offline") {
                        $redis->listRemove($useronlinelistkey, $v1['To_Account']);
                    }
                    if ($v1["State"] == "Online") {
                        $onlinelist[]=$v1['To_Account'];
                        $onlinelista[$k1]['uid']=$v1['To_Account'];
                        $onlinelista[$k1]['state']='online';
                        //$redis->listRemove($user_tx_onlinel, $v1['To_Account'], 0);
                        //$redis->listPush($user_tx_onlinel, $v1['To_Account']);
                    }
                    if ($v1["State"] == "PushOnline") {
                        $pushonlinelist[]=$v1['To_Account'];
                        $pushonlinea[$k1]['uid']=$v1['To_Account'];
                        $pushonlinea[$k1]['state']='pushonline';
                       // $redis->listRemove($user_tx_push_onlinel, $v1['To_Account'], 0);
                      // $redis->listPush($user_tx_push_onlinel, $v1['To_Account']);
                    }
                }
                if($_REQUEST['ppptest']==1){
                    //return $tentxunstatus;
                    dump(count($useronlinelist));
                    dump($useronlinelist);
                    dump($tentxunstatus);
                    dump($onlinelist);
                    dump($onlinelista);
                    dump($pushonlinea);
                }

                if ($type == 2) {//用户类型为2时，只返回状态为'Online'的用户

                    /*foreach ($onlinelist as $ko => $vo) {
                       $onlinelista[$ko]['uid']=$vo;
                        $onlinelista[$ko]['state']='online';
                    }*/


                            /*foreach ($pushonlinelist as $kp => $vp) {
                                $pushonlinea[$kp]['uid']=$vp;
                                $pushonlinea[$kp]['state']='pushonline';
                            }*/
                    $lang = count($onlinelist);
                    $page_count = ceil($lang / $pageSize);
                    $retArray = array();
                    $retArray['totalCount'] = $lang;
                    $retArray['pageSize'] = $pageSize;
                    $retArray['pageNum'] = $pageNum;
                    //$ret["uids"] = array_slice($onlinelist, ($pageNum - 1) * $pageSize, $pageSize);
                    //返回全部在线用户
                    $ret["uids"]=$onlinelista;
                    $ret["totalCount"] = $lang;
                    $ret['pushonline']=$pushonlinea;
                }

            } else {

                foreach ($useronlinelist as $k => $v) {

                    if (!in_array($v, $allonlinelist)) {
                        $redis->listRemove($useronlinelistkey, $v);
                    }
                }
                $redis->set($useronlinelisttimekey, $useronlinelistkey, 0, 0, 60 * 60 * 2);
            }
        }
        if ($type == 2) {//用户类型为2时，只返回状态为'Online'的用户
            return $ret;
        }
        if ($redis->exists($useronlinelistkey)) {
            $totalCount = $redis->listSize($useronlinelistkey);
            $pagestart = $pageNum * $pageSize - $pageSize;
            $pageend = $pagestart + $pageSize - 1;

            $ret["uids"] = $redis->listGet($useronlinelistkey, $pagestart, $pageend);

            //}
            $ret["totalCount"] = $totalCount;
        }


        return $ret;
    }

    //获取在线列表
    public function get_onlinelist($product,$user_type,$gender,$line_type){
        $redis=$this->redisconn();
        $useronlinelistkey = "useronlinelist_".$product."_".$user_type."_".$gender;

        $useronlinelist = $redis->listGet($useronlinelistkey, 0, 499);
        $txkey=M('txmy')->where(array('product'=>$product))->getField('miyao');
        $tentxunstatus = tencent_onlinestate($useronlinelist,$txkey);

        if($line_type==1){
            foreach ($tentxunstatus as $k1=>$v1){
                if($v1["State"]=="Offline"){
                    $redis->listRemove($useronlinelistkey,$v1['To_Account']);
                }
                if($v1["State"]=="Online"){
                    $onlinelist[]=$v1['To_Account'];
                }
            }
        }elseif($line_type==0){
            foreach ($tentxunstatus as $k1=>$v1){
                $onlinelist[]=$v1['To_Account'];
            }
        }elseif($line_type==2){
            foreach ($tentxunstatus as $k1=>$v1){
                if($v1["State"]=="Offline"){
                    $redis->listRemove($useronlinelistkey,$v1['To_Account']);
                }
                if($v1["State"]=="PushOnline"){
                    $onlinelist[]=$v1['To_Account'];
                }
            }

        }
        return $onlinelist;
    }


    //获取随机在线用户
    public function getRandUserOnline($procuct, $user_type, $gender, $uid = 0)
    {

        $redis = $this->redisconn();
        $useronlinelistkey = "useronlinelist_" . $procuct . "_" . $user_type . "_" . $gender;
        $useronlinelisttimekey = $useronlinelistkey . "time";
        $alluseronlinelistkey = "useronlinelist";
        $ret = array();
        if (!$redis->exists($useronlinelisttimekey)) {

            $alluseronlinelistkey = "useronlinelist";
            $allonlinelist = $redis->listGet($alluseronlinelistkey, 0, -1);
            $useronlinelist = $redis->listGet($useronlinelistkey, 0, -1);
            foreach ($useronlinelist as $k => $v) {

                if (!in_array($v, $allonlinelist)) {
                    $redis->listRemove($useronlinelistkey, $v);
                }
            }
            $redis->set($useronlinelisttimekey, $useronlinelistkey, 0, 0, 60 * 15);
        }
        if ($redis->exists($useronlinelistkey)) {
            $totalCount = $redis->listSize($useronlinelistkey);

            $uids = $redis->listGet($useronlinelistkey, 0, -1);
            if ($uid) {
                $puserlistkey = "puserlist_" . $uid;
                $puserlist = $redis->listGet($puserlistkey, 0, -1);
                foreach ($uids as $k1 => $v1) {
                    if (in_array($v1, $puserlist)) {
                        unset($uids[$k1]);
                    }
                }
                $uids = array_values($uids);
            }
            $uid = $uids[rand(0, $totalCount - 1)];
        }


        return $uid;
    }
    //获取在线状态为pushonline的普通男用户，放到redis中，由linux系统定时每小时执行一次，获取最新的数据
    public function putPushonlineToredis()
    {
        $procuct=$_REQUEST['procuct'];
        $redis = $this->redisconn();
        //获取在线的普通男用户
        $useronlinelistkey = "useronlinelist_" . $procuct . "_1_1";
        $useronlinelist = $redis->listGet($useronlinelistkey, 0, 499);
        $IMSDKIDS=M('txmy')->getField('product',true);
        $txkey=M('txmy')->where(array('product'=>$procuct))->getField('miyao');
        if (in_array($procuct, $IMSDKIDS)) {
            $tentxunstatus = tencent_onlinestate($useronlinelist,$txkey);
            foreach ($tentxunstatus as $k1 => $v1) {
                if ($v1["State"] == "PushOnline") {
                    $pushonlinelist[] = $v1['To_Account'];
                }
            }
            $time = time();
            foreach ($pushonlinelist as $k => $v) {
                $l_user = $this->get_user($v);
                if ($l_user['logintime'] > $time - (60 * 60 * 24)) {
                    $pushonline[]=$v;
                }
            }
            if($redis->exists('get-tx-pushonline-'.$procuct)){
                $redis->del('get-tx-pushonline-'.$procuct);
            }
            if(!empty($pushonline)){
                $redis_pushonline=json_encode($pushonline);
                $redis->set('get-tx-pushonline-'.$procuct,$redis_pushonline);
            }
            if($_REQUEST['get_tx_pushonline']==1){
                dump($pushonline);
            }
            exit();
        }
    }
}

?>