<?php
use Think\Controller;
use Think\Request;

class UserController extends BaseController
{

    public function index()
    {
        //$this->redis->delete("useronlinelist_0_1");
        Push_data();

    }

    /**
     * 获取用户列表
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int type 1:推荐用户;2:附近的人;3:视频用户 4：聊天用户随机目录
     * @request string page 分页页码
     * @request string userfield 需要的用户字段默认：uid|head|nickname
     * @return data
     */
    public function search()
    {

        $data = $this->Api_recive_date;
        $type = $data["type"] ? $data["type"] : 1;//1:推荐用户;2:附近的人;3:视频用户 4：聊天用户随机目录
        //基本查询条件
        $UserInfo = $this->UserInfo;//$this->UserInfo;//$this->get_user(493189);//$this->get_user(493412);//
        $baseWhere = array();
        $countryarr = array(
            "TW" => array("HK", "TW", "JP", "MO", "MY", "SG", "TH", "VN", "CN")
        );
        $country = "US";
        //判断是否为国内用户
        //44为中国id
        $countryids = 0;
        if ($countryids == 44) {
            foreach ($countryarr as $k => $v) {
                if (in_array($this->platforminfo["country"], $v)) {
                    $country = $k;
                    break;
                }
            }
        } else {
            foreach ($countryarr as $k => $v) {
                if (in_array($UserInfo["country"], $v)) {
                    $country = $k;
                    break;
                }
            }
        }

        $baseWhere['country'] = $country;
        $baseWhere['gender'] = array('neq', $UserInfo['gender']);//显示异性
        $baseWhere['user_type'] = 2;

        //获取用户信息 查询是否走用户返回筛选

        if($UserInfo['product'] != 25100){
            //查看用户类型
            if($UserInfo['user_type'] == 1){
                if($UserInfo['gender'] == 1){//普通男性用户

                    if($UserInfo['viptime'] >= time()){
                        $usertype = 1;
                    }else{
                        $usertype = 2;
                        //查询是否是意向用户
                        $user_yixiang = M("user_yixiang")->where(array("uid"=>$UserInfo['uid']))->select();
                        if(!empty($user_yixiang)){
                            $usertype = 3;
                        }

                    }
                }elseif ($UserInfo['gender'] == 2){//普通女性用户

                    if($UserInfo['viptime'] >= time()){
                        $usertype = 4;
                    }else{
                        $usertype = 5;
                        $user_yixiang = M("user_yixiang")->where(array("uid"=>$UserInfo['uid']))->select();
                        if(!empty($user_yixiang)){
                            $usertype = 6;
                        }
                    }
                }
            }elseif($UserInfo['user_type'] == 3){
                if($UserInfo['gender'] == 1){//推广男用户
                    $usertype = 7;
                }elseif($UserInfo['gender'] == 2){//推广女用户
                    $usertype = 8;
                }
            }


            //拼接条件赛选
            $where['product_type'] = $UserInfo['product'];
            $where['page'] = (string)($type > 3 ?1:$type);
            $where['usertype'] = (string)$usertype;
            $wheremd5 =  md5(json_encode($where));
            //查询条件
            $retdata = M("retdata")->where(array("condition_md5"=>$wheremd5,"is_success"=>1))->find();
            if(isset($retdata['json_usereturn']) && !empty($retdata)) {
                //按照规则返回数据
                $json_usereturn = json_decode($retdata['json_usereturn'], true);

                $isvip_num = ceil($json_usereturn['user_type_1'] / 100 * 20);
                //计算普通用户vip返回个数
                $vip_num = ceil($json_usereturn['uservip'] / 100 * $isvip_num);

                //计算普通用户非vip返回个数
                $no_vip_num = ceil($json_usereturn['useroffvip'] / 100 * $isvip_num);

                $online_yixiang = 0;
                $pushonline_yixiang = 0;
                $offline_yixiang = 0;
                $online_u = 0;
                $pushonline_u = 0;

                if ($no_vip_num > 1) {
                    //计算在线意向用户返回个数

                        $online_yixiang = ceil($json_usereturn['useronline'] / 100 * $no_vip_num);//online意向
                        $pushonline_yixiang = ceil($json_usereturn['userpushline'] / 100 * $no_vip_num);//pushonline意向
                        $offline_yixiang = ceil($json_usereturn['useroffline'] / 100 * $no_vip_num);//offline 意向
                        $online_u = ceil($json_usereturn['online'] / 100 * $no_vip_num);//online 普通用户
                        $pushonline_u = ceil($json_usereturn['pushonline'] / 100 * $no_vip_num);//pushonline 普通用户

                }

                //var_dump($offline_yixiang);die;
                //计算优质认证用户(原老推广用户和普通推广用户)返回个数
                $user_type_3_5 = ceil($json_usereturn['user_type_3_5'] / 100 * 20);


                $User_baseM = new UserBaseModel();
                $User_yixiang = new UserYixiangModel();
                $pageNum = $data['page'] ? $data['page'] : 1;
                foreach ($json_usereturn['productids'] as $k => $v) {
                    $product = trim($v, " ");
                    //获取普通用户vip
                    $Wdata['product'] = $product;
                    $Wdata['gender'] = array('neq', $UserInfo['gender']);//显示异性
                    $Wdata['viptime'] = array('gt', time());//显示异性
                    $Wdata['user_type'] = 1;
                    if($vip_num == 0){
                        $Wdata_list = array();
                    }else{
                        $Wdata_list[$product] = $User_baseM->getListPage($Wdata, $pageNum, $vip_num);
                    }


                    //获取在线online pushonline意向用户
                    $OnlineCon = new OnlineController();
                    $gender = $UserInfo['gender'] == 1 ? 2 : 1;
                    $commonusers[$product] = $OnlineCon->get_onlinelist($product, 1, $gender, 1);
                    //var_dump($commonusers);die;
                    //获取offline 意向用户
                    $push_offline_yixiang = array();
                    if($offline_yixiang == 0){
                        $Wdata_offline_yixiang = array();
                    }else{
                        $sql='select t_user_yixiang.uid from t_user_yixiang left join t_user_base ON t_user_yixiang.uid=t_user_base.uid where t_user_yixiang.product ='. $product .' and t_user_base.gender!='.$UserInfo['gender']." limit $offline_yixiang";
                        $resultusers=M("user_yixiang")->query($sql);
                        foreach($resultusers as $res){
                            array_push($push_offline_yixiang,$res['uid']);
                        }
                        $Wdata_offline_yixiang[$product] = array_unique($push_offline_yixiang);
                    }
                    //var_dump($Wdata_offline_yixiang);die;
                    //获取推广用户
                    $Wdata['user_type'] = 3;
                    if($user_type_3_5 ==0){
                        $Wdata_list_3 = array();
                    }else{
                        $Wdata_list_3[$product] = $User_baseM->getListPage($Wdata, $pageNum, $user_type_3_5);
                    }




                }

                $UIDS = array();
                $yx_on_push_line = array();
                $noyx_on_push_line = array();
                //uid 处理
                foreach ($commonusers as $v) {
                    if (!empty($v)) {
                        foreach ($v as $val) {
                            //查询是否是意向用户
                            $is_yixiang = M("user_yixiang")->where(array("uid" => $val))->find();
                            if ($is_yixiang) {
                                $yx_on_push_line[] = $val;
                            }else{
                                $noyx_on_push_line[] = $val;
                            }

                        }
                    }

                }
                foreach ($Wdata_list as $v) {
                    if (!empty($v['list'])) {
                        foreach ($v['list'] as $val) {
                            foreach ($val as $bv) {
                                $UIDS[] = $bv;
                            }

                        }
                    }


                }

                foreach ($Wdata_list_3 as $v) {
                    if (!empty($v['list'])) {
                        foreach ($v['list'] as $val) {
                            foreach ($val as $bv) {
                                $UIDS[] = $bv;
                            }

                        }
                    }


                }
                if(!empty($Wdata_offline_yixiang)){
                    foreach($Wdata_offline_yixiang as $v){
                        foreach($v as $bv){
                            $UIDS[] = $bv;
                        }
                    }
                }

                $slice_on_push_line_yx = array_slice($yx_on_push_line, 0,$online_yixiang+$pushonline_yixiang+$online_u);
                $slice_on_push_line_noyx = array_slice($noyx_on_push_line, 0,$online_u+$pushonline_u);

                $marage_uids = array_merge((array)$UIDS, (array)$slice_on_push_line_yx,(array)$slice_on_push_line_noyx);
                if($pageNum > 1){
                    $marage_uids = array();
                }
                if (count($marage_uids) < 20) {
                    $gender = $UserInfo['gender'] == 1 ? 2 : 1;
                    $tmp = $User_baseM->getListPage(array("user_type" => 1,'gender'=>$gender), $pageNum, 20 - count($marage_uids));
                    if (!empty($tmp['list'])) {
                        foreach ($tmp['list'] as $val) {
                            foreach ($val as $bv) {
                                $marage_uids[] = $bv;
                            }

                        }
                    }

                }
                $marage_uids = array_unique($marage_uids);
                if ($marage_uids) {
                    foreach ($marage_uids as $puid) {
                        $tempPUserInfo = $this->get_user($puid);
                        if (!$tempPUserInfo) {
                            continue;
                        }

                        //排除用户没有头像的用户head
                        if ($tempPUserInfo['head']['url'] == 'http://limida.oss-cn-hongkong.aliyuncs.com/data/1.png') {
                            continue;
                        }
                        //end
                        $tempPUserInfo["issayhello"] = $this->isSayHello($puid);
                        $tempPUserInfo["isfollow"] = $this->isFollow($puid);
                        $userfield = $data['userfield'] ? $data['userfield'] : "uid|head|nickname|logintime";
                        $tempPArray = $this->get_user_field($tempPUserInfo, $userfield, $puid);
                        $field_arr = explode("|", $userfield);
                        if (in_array('area', $field_arr)) {
                            //获取用户的国家和地区
                            $pcountryiso = $tempPUserInfo['country'];
                            $pregionid = $tempPUserInfo['area'];

                            $pcountryids = M('products')->where(array('product' => $tempPUserInfo['product']))->getField('countryids');
                            //44为中国id
                            if ($pcountryids == 44) {

                                if ($tempPUserInfo['user_type'] == 2) {
                                    $pcountryiso = M('region')->where(array('id' => $UserInfo['country'], 'level' => 1, 'country_codeiso' => 'CN'))->getField('code');
                                    $pregioncode = M('region')->where(array('id' => $pregionid, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');
                                } else {
                                    $pcountryiso = M('region')->where(array('id' => $pcountryiso, 'level' => 1, 'country_codeiso' => 'CN'))->getField('code');
                                    $pregioncode = M('region')->where(array('id' => $pregionid, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');

                                }

                            } else {
                                $pregionlist = $this->get_regionlist($pcountryiso);
                                foreach ($pregionlist as $k1 => $v1) {
                                    if ($v1['id'] == $pregionid) {
                                        $pregioncode = $v1['name'];
                                        break;
                                    }
                                }
                            }

                            $tempPArray['country'] = $this->L($pcountryiso);
                            $tempPArray['area'] = $this->L($pregioncode);
                            if ($tempPArray['area'] == '') {
                                $pregioncode = M('region')->where(array('id' => 1, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');
                                $tempPArray['area'] = $this->L($pregioncode);
                            }
                        }
                        if (in_array('isonline', $field_arr)) {
                            $tempPArray['isonline'] = 1;
                        }
                        if (in_array('onlinestatus', $field_arr)) {
                            $onlinestatus = rand(1, 3);
                            $tempPArray['onlinestatus'] = $onlinestatus;
                        }
                        $newlistuser[] = $tempPArray;


                    }
                }
                $return = array();
                $return['data']["userlist"] = $newlistuser;
                $page["pagesize"] = 20;
                $page["page"] = $pageNum;
                $page["totalcount"] = $tmp["totalCount"];
                $return['data']["page"] = $page;


                Push_data($return);
            }



        }


        switch ($type) {
            case 1://1:推荐用户
                if ($UserInfo['user_type'] == 3||$UserInfo['user_type'] == 9||$UserInfo['user_type'] == 10||$UserInfo['user_type'] == 11) {

                    $this->_tuijianonlinecommona($baseWhere,$UserInfo['product']);
                }else {
                    $this->_tuijian($baseWhere);
                }
                break;
            case 2://2:附近的人
                $this->_fujin($baseWhere);
                break;
            case 3://3:视频用户
                $this->_shipin($baseWhere);
                break;
            case 4://4：聊天用户随机目录
                if ($UserInfo['user_type'] == 3||$UserInfo['user_type'] == 9||$UserInfo['user_type'] == 10||$UserInfo['user_type'] == 11) {
                    $random_product=$data['product'];
                    $this->_tuijianonlinecommon($baseWhere,$random_product);
                }else{
                    $this->_tuijian($baseWhere);
                }
                break;
            default://1:视频用户
                $this->_fujin($baseWhere);
                break;
        }

    }
    //在线普通用户
    private function _tuijianonlinecommon($baseWhere,$product)
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = $data['pagesize'] ? $data['pagesize'] : 30;
        $userfield = $data['userfield'] ? $data['userfield'] : "uid|head|nickname|logintime";
        $Wdata = $baseWhere;
        $UserInfo = $this->UserInfo;
        $gender = $UserInfo["gender"] == 1 ? 2 : 1;
        //登录保存uid
        $OnlineCon = new OnlineController();
        $dataproduct=M('products')->getField('product',true);
        $countproduct=count($dataproduct);
        $uids=array();

        for($i=0;$i<$countproduct;$i++){
            $uids[$i]=$OnlineCon->getUserOnline($dataproduct[$i], 1, $gender, $pageNum,$pageSize,2);
        }
        if($_REQUEST['ppptestq']==1){
            dump(12345);
            dump(count($uids));
        }

        /*if(count($uids)<100){
            $sjuusers=M('user_base')->where(array('gender'=>1,'user_type'=>1,'vipgrade'=>1))->order('logintime desc')->limit(200)->getField('uid',true);
            foreach($sjuusers as $k=>$v){
                $userinfo=$this->get_user($v);
                $procuct=$userinfo['product'];
                $user_type=1;
                $gender=1;
                $useronlinelistkey = "useronlinelist_" . $procuct . "_" . $user_type . "_" . $gender;
                $redis = $this->redisconn();
                $redis->listPush($useronlinelistkey, $v);
            }
            for($i=0;$i<$countproduct;$i++){
                $uids[$i]=$OnlineCon->getUserOnline($dataproduct[$i], 1, $gender, $pageNum,$pageSize,2);
            }


        }*/


        $dataonline=array();
        $datapushonline=array();
        for($i=0;$i<$countproduct;$i++){
            if(empty($uids[$i]['uids'])||$uids[$i]['uids']==null){
                continue;
            }
            $dataonline=array_merge($dataonline,$uids[$i]['uids']);
        }

        for($i=0;$i<$countproduct;$i++){
            if(empty($uids[$i]['pushonline'])||$uids[$i]['pushonline']==null){
                continue;
            }
            $datapushonline=array_merge($datapushonline,$uids[$i]['pushonline']);
        }

        $dataonline=assoc_unique($dataonline,'uid');


        $cachename1 = "getsearchuser_recentfourusers".$gender;
        $cache1 = S($cachename1);//设置缓存标示
        //$cache1=0;
        if(!$cache1){
            //获取最近四小时内注册的异性用户
            $time=time()-60*60*4;
            $sql='select uid from t_user_base where gender ='. $gender .' and regtime>'.$time;
            $resultusers=M()->query($sql);

            $recentusers=array();
            foreach($resultusers as $k=>$v){
                $recentusers[$k]['uid']=$v['uid'];
                $recentusers[$k]['state']='online';
            }
            S($cachename1, $recentusers, 60 * 5);//设置缓存的生存时间
        }else{
            $recentusers=$cache1;
        }
        $recentusers=array_reverse($recentusers);
        $dataonline=array_merge($recentusers,$dataonline);






        $datapushonline=assoc_unique($datapushonline,'uid');
        $countonline=count($dataonline);
        if($countonline<1000){
            $datapushonline=array_slice($datapushonline,0,1000-$countonline);
            $uids["uids"]=array_merge($dataonline,$datapushonline);
        }else{
            $uids["uids"]=$dataonline;
        }


        $newlistuser = array();
        $totalCount=count($uids["uids"]);
        $uids["uids"] = array_slice($uids["uids"], ($pageNum - 1) * $pageSize, $pageSize);
        if ($uids["uids"]) {
            $onlinelist = $uids["uids"];
            if ($onlinelist) {
                foreach ($onlinelist as $k => $v) {
                    $tempUserInfo = $this->get_user($v['uid']);
                    $tempUserInfo['txstate'] = $v['state'];
                    //$tempUserInfo['txstate']='Online';
                    //24110版本过度
                    if ($product == '24110') {
                        $version = str_replace('.', '', $tempUserInfo["version"]);
                        if ((int)$version < 106) {
                            continue;
                        }
                    }
                    //获取用户的内心独白start
                    if ($tempUserInfo['mood_status_first'] == 3) {

                        $tempUserInfo['mood'] = '';
                    } /*elseif ($tempUserInfo['mood_status_first'] == 2) {
                        //内心独白正在审核中
                        $tempUserInfo['mood'] = '';
                    }*/
                    //获取用户的内心独白end
                    // dump($v['uid']);die;
                    if ($tempUserInfo) {
                        $tempArray = array();
                        $tempUserInfo["issayhello"] = $this->isSayHello($v['uid']);
                        $tempUserInfo["isfollow"] = $this->isFollow($v['uid']);

                        $tempArray = $this->get_user_field($tempUserInfo, $userfield);
                        $newlistuser[] = $tempArray;
                    }
                }
            }
            //shuffle($newlistuser);
        }

        // Push_data($newlistuser);
        $return = array();
        $return['data']["userlist"] = $newlistuser;
        $page["pagesize"] = $pageSize;
        $page["page"] = $pageNum;
        $page["totalcount"] = $totalCount;
        $return['data']["page"] = $page;

        Push_data($return);
    }
    //24110过度取小于1.0.6在线普通用户
    private function _tuijianonlinecommona($baseWhere,$product)
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = $data['pagesize'] ? $data['pagesize'] : 30;
        $userfield = $data['userfield'] ? $data['userfield'] : "uid|head|nickname";
        $Wdata = $baseWhere;
        $UserInfo = $this->UserInfo;
        $gender = $UserInfo["gender"] == 1 ? 2 : 1;
        //登录保存uid
        $OnlineCon = new OnlineController();
        $uids = $OnlineCon->getUserOnline($product, 1, $gender, $pageNum);
        $newlistuser = array();
        $totalCount = $uids["totalCount"] ? $uids["totalCount"] : 0;

        $uidss = $this->uid;
        $userdragblacklist = new UserDragblacklistModel();
        $heimingdan = M('blacklist')->where(array('uid'=>$uidss))->select();
        //查询被拉入黑名单
        // dump($heimingdan);die;
        if ($uids["uids"]) {

            $onlinelist = $uids["uids"];
            foreach ($heimingdan as $key => $value) {
                $onlinelist = array_diff($onlinelist, $value['touids']);
            }

            if ($onlinelist) {
                foreach ($onlinelist as $k => $v) {

                    $tempUserInfo = $this->get_user($v);
                    //24110版本过度
                    if($product=='24110'){
                        $version=str_replace('.','',$tempUserInfo["version"]);
                        if((int)$version>=106) {
                            continue;
                        }
                    }
                    //获取用户的内心独白start
                    if ($tempUserInfo['mood_status_first'] == 3) {

                        $tempUserInfo['mood'] = '';
                    } /*elseif ($tempUserInfo['mood_status_first'] == 2) {
                                //内心独白正在审核中
                                $tempUserInfo['mood'] = '';
                            }*/
                    //获取用户的内心独白end

                    if ($tempUserInfo) {
                        $tempArray = array();
                        $tempUserInfo["issayhello"] = $this->isSayHello($v);
                        $tempUserInfo["isfollow"] = $this->isFollow($v);

                        $tempArray = $this->get_user_field($tempUserInfo, $userfield);

                        $newlistuser[] = $tempArray;
                    }


                }
            }
            //shuffle($newlistuser);
        }

        // Push_data($newlistuser);
        $return = array();
        $return['data']["userlist"] = $newlistuser;
        $page["pagesize"] = $pageSize;
        $page["page"] = $pageNum;
        $page["totalcount"] = $totalCount;
        $return['data']["page"] = $page;

        Push_data($return);
    }

    /*
     *是否打招呼
     */
    private function isSayHello($touid, $uid=0)
    {
        $uid = $uid ? $uid : $this->uid;
        $redis = $this->redisconn();
        $redisStr = "sayhello_" . $uid . "_" . $touid;

        if ($redis->exists($redisStr)) {
            return 1;
        } else {
            return 0;
        }
    }

    /*
     *是否关注 isFollow
     */
    private function isFollow($touid, $uid = 0, $reset = 0)
    {
        $uid = $uid ? $uid : $this->uid;
        if ($touid && $uid) {
            $path = $this->get_userpath($uid, 'cache/follow/');
            $cache_name = 'follow_' . $uid . $touid;
            if ($reset == 1) {
                return F($cache_name, NULL, $path);
            }

            if (F($cache_name, '', $path)) {
                $cacheValue = F($cache_name, '', $path);
            } else {
                $UserFollowM = new UserFollowModel();
                $return = $UserFollowM->getOne(array('touid' => $touid, 'uid' => $uid));

                if ($return) {
                    $cacheValue = 1;
                } else {
                    $cacheValue = 0;
                }
                F($cache_name, $cacheValue, $path);
            }

            return $cacheValue;
        } else {
            return 0;
        }
    }

    private function _tuijian($baseWhere)
    {

        $UserInfo = $this->UserInfo;
        if($UserInfo['user_type'] == 4){//测试用户
            $this->_cstuijian($baseWhere);
        }
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $product = $this->platforminfo['product'];
        //end
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = 15;
        $userfield = $data['userfield'] ? $data['userfield'] : "uid|head|nickname";
        if($UserInfo['gender']==1){
            $gender=2;
        }else{
            $gender=1;
        }
        $newlistuser = array();
        if($product==23138){
            $map=array();
            $map['user_type'] = 3;
            $map['gender']=$baseWhere['gender'];
            $threegirlusers=M('user_base')->where($map)->getField('uid',true);
            //查看是否在线
            $txkey=M('txmy')->where(array('product'=>$product))->getField('miyao');
            $threetentxunstatus = tencent_onlinestate($threegirlusers,$txkey);
            $onlinethreegirlusers=array();
            foreach ($threetentxunstatus as $k1 => $v1) {
                if ($v1["State"] == "Online") {
                    $onlinethreegirlusers[] = $v1['To_Account'];
                }
            }
            //获取在线普通异性用户
            $commonusersonline=$this->get_all_number($gender);
            $onlinethreegirluserscount=count($onlinethreegirlusers);
            $commoncount=2000-$onlinethreegirluserscount;
            $dataxnconmmonuser=M('user_base')->where($baseWhere)->order("RAND()")->limit($commoncount)->getField('uid',true);
            $tatalrealusers=array_merge($onlinethreegirlusers,$commonusersonline,$dataxnconmmonuser);
            if($_REQUEST['csxdtg']==1){
                dump($onlinethreegirlusers);
                dump($commonusersonline);
                dump($dataxnconmmonuser);
            }
            $commonuser = array_slice($tatalrealusers, ($pageNum - 1) * $pageSize, $pageSize);

        }elseif($product==25100){
            $map=array();
            $map['user_type'] = 3;
            $map['gender']=$baseWhere['gender'];
            $threegirlusers=M('user_base')->where($map)->getField('uid',true);
            //查看是否在线
            $txkey=M('txmy')->where(array('product'=>$product))->getField('miyao');
            $threetentxunstatus = tencent_onlinestate($threegirlusers,$txkey);
            $onlinethreegirlusers=array();
            foreach ($threetentxunstatus as $k1 => $v1) {
                if ($v1["State"] == "Online" || $v1["State"] == "PushOnline") {
                    $onlinethreegirlusers[] = $v1['To_Account'];
                }
            }
            //获取在线普通异性用户25100 23138 20114 23110
            $OnlineCon = new OnlineController();
            $commonusers = $OnlineCon->get_onlinelist($product, 1, $gender,1);
//            $commonusers_23138 = $OnlineCon->get_onlinelist('23138', 1, $gender,1);
//            $commonusers_20114 = $OnlineCon->get_onlinelist('20114', 1, $gender,1);
//            $commonusers_23110 = $OnlineCon->get_onlinelist('23110', 1, $gender,1);
//            $commonusers_24111=$OnlineCon->get_onlinelist("24111",1,$gender,1);
//            $commonusers_24112=$OnlineCon->get_onlinelist("24112",1,$gender,1);
//            $commonusers_25100_3 = $OnlineCon->get_onlinelist($product, 3, $gender,1);
//            $commonusers_23138_3 = $OnlineCon->get_onlinelist('23138', 3, $gender,1);
//            $commonusers_20114_3 = $OnlineCon->get_onlinelist('20114', 3, $gender,1);
//            $commonusers_23110_3 = $OnlineCon->get_onlinelist('23110', 3, $gender,1);
//            $commonusers_24111_3 = $OnlineCon->get_onlinelist("24111",3,$gender,1);
//            $commonusers_24112_3 = $OnlineCon->get_onlinelist("24112",3,$gender,1);
//            $commonusers_1 = array_merge((array)$commonusers_25100,(array)$commonusers_20114,(array)$commonusers_24112,(array)$commonusers_23110,(array)$commonusers_24111,(array)$commonusers_23138);
//            $commonusers_3 = array_merge((array)$commonusers_25100_3,(array)$commonusers_20114_3,(array)$commonusers_24112_3,(array)$commonusers_23110_3,(array)$commonusers_24111_3,(array)$commonusers_23138_3);
//            $commonusers = array_merge($commonusers_1,$commonusers_3);

            $commonusersonline = array();
            foreach($commonusers as $k=>$v){
                $commonusersonline[]=$v;
            }
            if($_REQUEST['pppptest']==1){
                dump($commonusersonline);
            }
            $onlinethreegirluserscount=count($onlinethreegirlusers);
            $commoncount=2000-$onlinethreegirluserscount;
            $dataxnconmmonuser=M('user_base')->where($baseWhere)->order("RAND()")->limit($commoncount)->getField('uid',true);
            $tatalrealusers=array_merge($commonusersonline,$onlinethreegirlusers,$dataxnconmmonuser);
            $commonuser = array_slice($tatalrealusers, ($pageNum - 1) * $pageSize, $pageSize);
        }else{
            if($UserInfo['vipgrade']<1){
                $map=array();
                $map['user_type'] = 3;
                $map['gender']=$baseWhere['gender'];
                $threegirlusers=M('user_base')->where($map)->getField('uid',true);



                //查看是否在线
                $txkey=M('txmy')->where(array('product'=>$product))->getField('miyao');
                $threetentxunstatus = tencent_onlinestate($threegirlusers,$txkey);
                $onlinethreegirlusers=array();
                foreach ($threetentxunstatus as $k1 => $v1) {
                    if ($v1["State"] == "Online" || $v1["State"] == "PushOnline") {
                        $onlinethreegirlusers[] = $v1['To_Account'];
                    }
                }
                //获取在线普通异性用户
                $commonusers=$this->get_common_user($product,$gender,$pageNum,$pageSize,2);

                $commonusersonline=array();
                foreach($commonusers as $k=>$v){
                    $userinfoonline=$this->get_user($v['uid']);
                    if($userinfoonline['vipgrade']>=1){
                        $commonusersonline[]=$v['uid'];
                    }
                }
                if($_REQUEST['pppptest']==1){
                    dump($commonusersonline);
                }
                $onlinethreegirluserscount=count($onlinethreegirlusers);
                $commoncount=2000-$onlinethreegirluserscount;
                //获取虚拟用户
                $cachename1 = "getsearchuser_xnjdusers".$baseWhere['gender'];
                //$cache1 = S($cachename1);//设置缓存标示
                $cache1=0;
                if(!$cache1){
                    $dataxnconmmonuser=M('user_base')->where($baseWhere)->order("RAND()")->limit($commoncount)->getField('uid',true);
                    S($cachename1, $dataxnconmmonuser, 60 * 60);//设置缓存的生存时间
                }else{
                    $dataxnconmmonuser=$cache1;
                }
                $tatalrealusers=array_merge($onlinethreegirlusers,$commonusersonline,$dataxnconmmonuser);
                $commonuser = array_slice($tatalrealusers, ($pageNum - 1) * $pageSize, $pageSize);
            }else{
                $commonuser = $this->_tuijiancommonuser($baseWhere, $pageNum, $pageSize, $userfield);
                shuffle($commonuser);
            }
        }
        $uid = $this->uid;
        // dump($uid);die;
        $userdragblacklist = new UserDragblacklistModel();
        $heimingdan =  M('blacklist')->where(array('uid'=>$uid))->select();
        // dump($commonuser);die;
        //查询被拉入黑名单
        $puseronline_c=$commonuser;
        // foreach ($heimingdan as $key => $value) {
        //             $puseronline_c = array_diff($puseronline_c, $value['touid']);
        //     }
        $newArrayisOnlie1 = array();
        $newArrayisOnlie2 = array();
        $newArrayisOnlie3 = array();
        if ($puseronline_c) {
            foreach ($puseronline_c as $puid) {

                // foreach ($heimingdan as $key => $value) {
                //         if( empty($value) || $puid!= $value['touid']){
                // // dump($puid);die;
                //             $tempPUserInfo = $this->get_user($puid);
                //         }
                // }
                if(!empty($heimingdan)){
                    foreach ($heimingdan as $key => $value) {
                        if($value['touids'] != $puid){
                            // dump($value);die;
                            $tempPUserInfo = $this->get_user($puid);
                        }
                    }
                }else{
                    $tempPUserInfo = $this->get_user($puid);
                }

                // dump($tempPUserInfo);die;
                if (!$tempPUserInfo) {
                    continue;
                }
                //排除用户的未通过审核的内心独白start
                if ($tempPUserInfo['mood_status_first'] == 3) {
                    $tempPUserInfo['mood'] = '';
                }
                //排除用户没有头像的用户head
                if ($tempPUserInfo['head']['url'] == 'http://limida.oss-cn-hongkong.aliyuncs.com/data/1.png') {
                    continue;
                }
                //end
                $tempPUserInfo["issayhello"] = $this->isSayHello($puid);
                $tempPUserInfo["isfollow"] = $this->isFollow($puid);
                $tempPArray = $this->get_user_field($tempPUserInfo, $userfield, $puid);
                $field_arr = explode("|", $userfield);
                if (in_array('area', $field_arr)) {
                    //获取用户的国家和地区
                    $pcountryiso = $tempPUserInfo['country'];
                    $pregionid = $tempPUserInfo['area'];

                    $pcountryids = M('products')->where(array('product' => $product))->getField('countryids');
                    //44为中国id
                    if ($pcountryids == 44) {
                        if ($product == 22210) {
                            if ($tempPUserInfo['user_type'] == 2) {
                                $pcountryiso = M('region')->where(array('id' => $pregionid, 'level' => 1, 'country_codeiso' => 'TW'))->getField('code');
                            } else {
                                $pcountryiso = M('region')->where(array('id' => $pcountryiso, 'level' => 1, 'country_codeiso' => 'TW'))->getField('code');
                                $pregioncode = M('region')->where(array('id' => $pregionid, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');
                            }
                        } else {
                            if ($tempPUserInfo['user_type'] == 2) {
                                $pcountryiso = M('region')->where(array('id' => $UserInfo['country'], 'level' => 1, 'country_codeiso' => 'CN'))->getField('code');
                                $pregioncode = M('region')->where(array('id' => $pregionid, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');
                            } else {
                                $pcountryiso = M('region')->where(array('id' => $pcountryiso, 'level' => 1, 'country_codeiso' => 'CN'))->getField('code');
                                $pregioncode = M('region')->where(array('id' => $pregionid, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');

                            }
                        }

                    } else {
                        $pregionlist = $this->get_regionlist($pcountryiso);
                        foreach ($pregionlist as $k1 => $v1) {
                            if ($v1['id'] == $pregionid) {
                                $pregioncode = $v1['name'];
                                break;
                            }
                        }
                    }

                    $tempPArray['country'] = $this->L($pcountryiso);
                    $tempPArray['area'] = $this->L($pregioncode);
                    if ($tempPArray['area'] == '') {
                        $pregioncode = M('region')->where(array('id' => 1, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');
                        $tempPArray['area'] = $this->L($pregioncode);
                    }
                }
                if (in_array('isonline', $field_arr)) {
                    $tempPArray['isonline'] = 1;
                }
                if (in_array('onlinestatus', $field_arr)) {
                    $onlinestatus=rand(1,3);
                    $tempPArray['onlinestatus'] = $onlinestatus;
                }
                //获取是否在线 随机 并保持一致存入redis
                if($product==25100){
                    $redis = $this->redisconn();


                    if(in_array($puid, $commonusersonline)) {
                        if(!in_array($puid, $commonusers)){
                            $ran = 2;//mt_rand(1,3);
                            $redis->set('onlinestatus_25100_'.$puid,$ran,0,0,60*60*24);
                            $keyexitone=$redis->exists('onlinestatus_25100_'.$puid);
                            $tempPArray['isonline_vod'] = $ran;
                            $newArrayisOnlie2[] = $tempPArray;
                            // array_unshift($newlistuser,$tempPArray);

                        }else{
                            $ran = 1;
                            $redis->set('onlinestatus_25100_'.$puid,$ran,0,0,60*60*24);
                            $keyexitone=$redis->exists('onlinestatus_25100_'.$puid);
                            $tempPArray['isonline_vod'] =$ran;
                            $newArrayisOnlie1[] = $tempPArray;
                            //array_unshift($newlistuser,$tempPArray);
                        }

                    }else{
                        $ran = 3;
                        $redis->set('onlinestatus_25100_'.$puid,$ran,0,0,60*60*24);
                        $keyexitone=$redis->exists('onlinestatus_25100_'.$puid);
                        $tempPArray['isonline_vod'] = $ran;
                        $newArrayisOnlie3[] = $tempPArray;
                        // array_push($newlistuser,$tempPArray);
                    }
                    $newlistuser = array_merge($newArrayisOnlie1,$newArrayisOnlie2,$newArrayisOnlie3);
                }else{
                    $newlistuser[] = $tempPArray;
                }



                //$newlistuser[] = $tempPArray;
            }

            $return = array();
            $return['data']["userlist"] = $newlistuser;

        }

        $page["pagesize"] = $pageSize;
        $page["page"] = $pageNum;
        /*$page["totalcount"] = $ret["totalCount"];*/
        $return['data']["page"] = $page;

        Push_data($return);

    }

    //测试推荐：添加用户平台号用户类型等信息
    private function _cstuijian($baseWhere)
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $gender=$baseWhere['gender'];
        $product = $this->platforminfo['product'];
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize =30;
        $userfield = $data['userfield'] ? $data['userfield'] : "uid|head|nickname";
        $UserInfo = $this->UserInfo;

////////////////////////////////////
        $cachename1 = "getsearchuser_tatalrealusers".md5(json_encode($baseWhere)).$product;
        $cache1 = S($cachename1);//设置缓存标示
        //$cache1=0;
        if(!$cache1){
            //获取等级为3的异性用户
            $map=array();
            $map['user_type'] = array('NEQ','7');
            $map['vipgrade']=3;
            $map['gender']=$gender;
            $map['product']=$product;
            $threegirlusers=M('user_base')->where($map)->getField('uid',true);
            //查看是否在线
            $txkey=M('txmy')->where(array('product'=>$product))->getField('miyao');
            $threetentxunstatus = tencent_onlinestate($threegirlusers,$txkey);
            $onlinethreegirlusers=array();
            $PushOnlinethreegirlusers=array();
            foreach ($threetentxunstatus as $k1 => $v1) {
                if ($v1["State"] == "Online") {
                    $onlinethreegirlusers[] = $v1['To_Account'];
                }// || $v1["State"] == "PushOnline"
                if ($v1["State"] == "PushOnline") {
                    $PushOnlinethreegirlusers[] = $v1['To_Account'];
                }
            }
            //获取等级为2的女用户
            $twogirlusers=M('user_base')->where(array('vipgrade'=>2,'gender'=>$gender,'product'=>$product))->getField('uid',true);
            //获取等级为1的普通女用户
            $onegirlusers=M('user_base')->where(array('vipgrade'=>1,'gender'=>$gender,'user_type'=>1,'product'=>$product))->getField('uid',true);
            $realusers=array_merge($twogirlusers,$onegirlusers);
            //查看是否在线
            $tentxunstatus = tencent_onlinestate($realusers, C("IMSDKID")['24110']);
             //   $tentxunstatus = tencent_onlinestate($realusers, C("IMSDKID"));
            $onlinelistcommon=array();
            $PushOnlinelistcommon=array();
            foreach ($tentxunstatus as $k1 => $v1) {
                if ($v1["State"] == "Online") {
                    $onlinelistcommon[] = $v1['To_Account'];
                }
                if ($v1["State"] == "PushOnline") {
                    $PushOnlinelistcommon[] = $v1['To_Account'];
                }
            }
            $realalluser=array_merge($onlinethreegirlusers,$PushOnlinethreegirlusers,$onlinelistcommon,$PushOnlinelistcommon);
            $countrealusers=count($realalluser);
            $commoncount=2000-$countrealusers;
            //获取会员等级为1的虚拟用户
            $baseWhere['vipgrade']=1;
            $dataxnconmmonuser=M('user_base')->where($baseWhere)->order("RAND()")->limit($commoncount)->getField('uid',true);
            $tatalrealusers=array_merge($realalluser,$dataxnconmmonuser);
            S($cachename1, $tatalrealusers, 60 * 10);//设置缓存的生存时间
        }else{
            $tatalrealusers=$cache1;
        }
        $puseronline5 = array_slice($tatalrealusers, ($pageNum - 1) * $pageSize, $pageSize);

/////////////////////////////////////////
        if($_REQUEST['ptestt']==1){
            dump($onlinethreegirlusers);
            dump($PushOnlinethreegirlusers);
            dump($onlinelistcommon);
            dump($PushOnlinelistcommon);
            dump($realalluser);
            dump($puseronline5);
            dump($tentxunstatus);
        }
        $newlistuser = array();
        /*if($UserInfo["gender"]==1){
            //在男用户列表中加入优质女性用户
            $betteruserpageSize=5;
            $betteruser=$this->_betteruser($UserInfo['product'],$pageNum,$betteruserpageSize);
            $countbetteruser=count($betteruser);
            $commonuser = $this->_tuijiancommonuser($baseWhere, $pageNum, $pageSize-$countbetteruser, $userfield);
            $puseronline5=array_merge($commonuser,$betteruser);
        }else{
            $commonuser = $this->_tuijiancommonuser($baseWhere, $pageNum, $pageSize, $userfield);
            $puseronline5=$commonuser;
        }
        shuffle($puseronline5);*/
        if ($puseronline5) {
            foreach ($puseronline5 as $puid) {
                $tempPUserInfo = $this->get_user($puid);
                if (!$tempPUserInfo) {
                    continue;
                }
                //排除用户的未通过审核的内心独白start
                if ($tempPUserInfo['mood_status_first'] == 3) {
                    $tempPUserInfo['mood'] = '';
                }
                //排除用户没有头像的用户head
                if ($tempPUserInfo['head']['url'] == 'http://limida.oss-cn-hongkong.aliyuncs.com/data/1.png') {
                    continue;
                }
                //end
                $tempPUserInfo["issayhello"] = $this->isSayHello($puid);
                $tempPUserInfo["isfollow"] = $this->isFollow($puid);
                $tempPArray = $this->get_user_field($tempPUserInfo, $userfield, $puid);
                //添加额外信息
                $tempPArray['nickname'] = $tempPUserInfo['product'].'-'.$tempPUserInfo['user_type'].'-'.$tempPArray['nickname'];
                $tempPArray['mood'] =$tempPUserInfo['mood'].'-'.$tempPUserInfo['product'].'-'.$tempPUserInfo['user_type'];
                $field_arr = explode("|", $userfield);
                if (in_array('area', $field_arr)) {
                    //获取用户的国家和地区
                    $pcountryiso = $tempPUserInfo['country'];
                    $pregionid = $tempPUserInfo['area'];

                    $pcountryids = M('products')->where(array('product' => $product))->getField('countryids');
                    //44为中国id
                    if ($pcountryids == 44) {
                        if ($product == 22210) {
                            if ($tempPUserInfo['user_type'] == 2) {
                                $pcountryiso = M('region')->where(array('id' => $pregionid, 'level' => 1, 'country_codeiso' => 'TW'))->getField('code');
                            } else {
                                $pcountryiso = M('region')->where(array('id' => $pcountryiso, 'level' => 1, 'country_codeiso' => 'TW'))->getField('code');
                                $pregioncode = M('region')->where(array('id' => $pregionid, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');
                            }
                        } else {
                            if ($tempPUserInfo['user_type'] == 2) {
                                $pcountryiso = M('region')->where(array('id' => $UserInfo['country'], 'level' => 1, 'country_codeiso' => 'CN'))->getField('code');
                                $pregioncode = M('region')->where(array('id' => $pregionid, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');
                            } else {
                                $pcountryiso = M('region')->where(array('id' => $pcountryiso, 'level' => 1, 'country_codeiso' => 'CN'))->getField('code');
                                $pregioncode = M('region')->where(array('id' => $pregionid, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');

                            }
                        }

                    } else {
                        $pregionlist = $this->get_regionlist($pcountryiso);
                        foreach ($pregionlist as $k1 => $v1) {
                            if ($v1['id'] == $pregionid) {
                                $pregioncode = $v1['name'];
                                break;
                            }
                        }
                    }

                    $tempPArray['country'] = $this->L($pcountryiso);
                    $tempPArray['area'] = $this->L($pregioncode);
                    if ($tempPArray['area'] == '') {
                        $pregioncode = M('region')->where(array('id' => 1, 'level' => 2, 'upper_region' => $pcountryiso))->getField('code');
                        $tempPArray['area'] = $this->L($pregioncode);
                    }
                }
                if (in_array('isonline', $field_arr)) {
                    $tempPArray['isonline'] = 1;
                }
                $newlistuser[] = $tempPArray;
            }
            //ksort($newlistuser);
            //shuffle($newlistuser);
            $return = array();
            $return['data']["userlist"] = $newlistuser;

        }
        $page["pagesize"] = $pageSize;
        $page["page"] = $pageNum;
        $return['data']["page"] = $page;
        Push_data($return);

    }

    protected function randinad($newlistuser, $product)
    {

        $where = array();
        $where['status'] = 1;
        $where['product'] = $product;
        $redis = $this->redisconn();
        $redisStr = "userlistad_" . $product;
        //$redis->del($redisStr);
        if ($res = $redis->get($redisStr)) {
            $res = json_decode(base64_decode($res), true);
        } else {

            $alowcid = M('ad_cate')->where(array("showinstart" => 0))->select();

            $cids = array();
            $cids[] = 0;
            $res = array();
            foreach ($alowcid as $cv) {
                $cids[] = $cv["id"];
            }
            $where['cate_id'] = array("in", implode(",", $cids));
            $adlisttemp = M('ad')->where($where)->select();

            $adlist = array();
            if ($adlisttemp) {
                foreach ($adlisttemp as $adv) {
                    $temp = array(
                        "url" => $adv["url"],
                        "imgsrc" => C("IMAGEURL") . $adv["imgsrc"]
                    );

                    $adlist[] = $temp;
                }
            }
            $res = $adlist;
            $redis->set($redisStr, base64_encode(json_encode($res)), 0, 0, 60 * 60 * 24);
        }
        if (count($res) > 2) {
            $idint = array_rand($res, 2);
            $newres[] = $res[$idint[0]];
            $newres[] = $res[$idint[1]];
        } else {

            $newres = $res;
        }
        if (count($newres) > 0) {
            $listkey = array_rand($newlistuser, count($newres));
            if (is_array($listkey)) {
                $newlistuser[$listkey[0]] = $newres[0];
                $newlistuser[$listkey[1]] = $newres[1];
            } else {
                $newlistuser[$listkey] = $newres[0];
            }
        }
        return $newlistuser;
    }

    private function _fujin($baseWhere,$ll = 1)
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $UserInfo = $this->UserInfo;
        $product = $this->platforminfo['product'];
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = $data['pagesize'] ? $data['pagesize'] : 30;
        $userfield = $data['userfield'] ? $data['userfield'] : "uid|head|nickname";
        $wherephoto = isset($baseWhere['wherephoto'])?$baseWhere['wherephoto']:1;
        unset($baseWhere['wherephoto']);
        $Wdata = $baseWhere;
        if($ll == 1){
            $cachename1 = "getsearchuserfujin_" . md5(json_encode($Wdata)) . $pageNum . $pageSize . $userfield;
            $cache1 = S($cachename1);//设置缓存标示
            if (!$cache1) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                $User_baseM = new UserBaseModel();
                $ret = $User_baseM->getListPage($Wdata, $pageNum, $pageSize);
                $list = array();
                if($wherephoto == 3){//过滤有头像的
                    foreach($ret['list'] as $v){
                        //查询是否有值
                        $isphoto = M("photo")->where(array("uid"=>$v['uid']))->select();
                        if(empty($isphoto)){
                            $list[] = $v;
                        }

                    }
                    $ret['list'] = $list;
                }else if($wherephoto == 2){
                    foreach($ret['list'] as $v){
                        //查询是否有值
                        $isphoto = M("photo")->where(array("uid"=>$v['uid'],"type"=>2))->select();
                        if(!empty($isphoto)){
                            $list[] = $v;
                        }

                    }
                    $ret['list'] = $list;
                }
                $cache1 = $ret;
                S($cachename1, $cache1, 60 * 60 * 2); //设置缓存的生存时间
            }
            $ret = $cache1;
        }else{
            $cachename1 = "getsearchuserfujin_" . md5(json_encode($Wdata)) . $pageNum . $pageSize . $userfield;
            $cache1 = S($cachename1);//设置缓存标示
            if (!$cache1) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                $User_baseM = new UserBaseModel();
                $ret = $User_baseM->getListPage2($Wdata, $pageNum, $pageSize);
                $cache1 = $ret;
                S($cachename1, $cache1, 60 * 60 * 2); //设置缓存的生存时间
            }
            $ret = $cache1;
        }

        $uidss = $this->uid;
        $userdragblacklist = new UserDragblacklistModel();
        $heimingdan =  M('blacklist')->where(array('uid'=>$uidss))->select();
        // dump($heimingdan);die;
        $newlistuser = array();
        foreach ($ret['list'] as $k => $v) {
            if(!empty($heimingdan)){
                foreach ($heimingdan as $key => $value) {
                    if($value['touids'] != $v['uid']){
                        // dump($value);die;
                        $tempUserInfo = $this->get_user($v['uid']);
                    }
                }
            }else{
                $tempUserInfo = $this->get_user($v['uid']);
            }


            // dump($tempUserInfo);die;
            //排除用户的未通过审核的内心独白start
            if ($tempUserInfo['mood_status_first'] == 3) {

                $tempUserInfo['mood'] = '';
            }
            //end
            if ($tempUserInfo) {
                $tempArray = array();
                $tempUserInfo["issayhello"] = $this->isSayHello($v['uid']);
                //$UserInfo["isfollow"] = $this->isFollow($v['uid']);
                //设置用户在线状态start
                $cachename = "get_onlinestatus_" . $v['uid'];
                $cache = S($cachename);//设置缓存标示
                if (!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                    $rand = array('1', '2');
                    $rand_keys = array_rand($rand, 1);
                    $tempUserInfo["isonline"] = $rand[$rand_keys];
                    $cache = $rand[$rand_keys];
                    S($cachename, $cache, 60 * 60 * 2);//设置缓存的生存时间
                } else {
                    $tempUserInfo["isonline"] = $cache;
                }
                //设置用户在线状态end
                $tempArray = $this->get_user_field($tempUserInfo, $userfield);

                $tempPArray["mood"]= $tempUserInfo['mood'];
                $newlistuser[] = $tempArray;
            }
        }
        ksort($newlistuser);
        shuffle($newlistuser);
        //$newlistuser = $this->randinad($newlistuser,$product);//随机添加广告
        $return = array();
        if ($pageNum > ceil($ret["totalCount"] / $pageSize)) {
            $return['data']["userlist"] = array();
        } else {
            $return['data']["userlist"] = $newlistuser;
        }
        $product = $this->platforminfo['product'];
        //版本更新，相关平台相关版本无法使用本接口，处在关闭状态
        /*$versioncs = $this->platforminfo['version'];
        $versioncs = str_replace('.', '', $versioncs);

        $versiondata=M('version_update');
        $alldata=$versiondata->select();
        foreach($alldata as $k=>$v){
            $versiondb=str_replace('.', '', $v['version']);
            if($product==$v['product']&$versioncs<$versiondb){
                if($v['isopen']=='1'){
                    $return = array();
                    $return['data']["userlist"] = array();
                    Push_data($return);
                    break;
                }else{
                    break;
                }
            }

        }*/

        $page["pagesize"] = $pageSize;
        $page["page"] = $pageNum;
        $page["totalcount"] = $ret["totalCount"];
        $return['data']["page"] = $page;


        Push_data($return);


    }

    /*
     * 添加3在线用户列表
     */
    private function get_puser($product, $gender, $pageNum = 1, $size = 30)
    {
        //登录保存uid
        $OnlineCon = new OnlineController();
        $uids = $OnlineCon->getUserOnline($product, 3, $gender, $pageNum, $size);
        if (empty($uids["uids"])) {
            return false;
        } else {
            return $uids["uids"];
        }
    }

    //添加普通在线用户
    private function get_common_user($product, $gender, $pageNum = 1, $size = 30,$type=1)
    {
        //登录保存uid
        $OnlineCon = new OnlineController();
        $uids = $OnlineCon->getUserOnline($product, 1, $gender, $pageNum, $size,$type);
        if (empty($uids["uids"])) {
            return false;
        } else {
            return $uids["uids"];
        }
    }

    //获取所有平台在线普通女会员
    public function get_all_number($gender){
        $procuct=$this->platforminfo['product'];
        $time=time();
        $data=array();
        $onlinelist=array();
        $pushonlinelist=array();
        $data['user_type']=1;
        $data['gender']=$gender;
        $data['viptime']=array('EGT',$time);
        $allnumberuid=M('user_base')->where($data)->getField('uid',true);
        //检测uid是否在线
        $txkey=M('txmy')->where(array('product'=>$procuct))->getField('miyao');
        $tentxunstatus = tencent_onlinestate($allnumberuid,$txkey);
        foreach ($tentxunstatus as $k1 => $v1) {
            if ($v1["State"] == "Online") {
                $onlinelist[]=$v1['To_Account'];
            }
            if ($v1["State"] == "PushOnline") {
                $pushonlinelist[]=$v1['To_Account'];
            }
        }
        return  $onlinelist;
    }

    //推荐xn用户
    public function _tuijiancommonuser($baseWhere, $pageNum, $pageSize, $userfield)
    {
        $Wdata = $baseWhere;
        $cachename1 = "getsearchuser_" . md5(json_encode($Wdata)) . $pageNum . $pageSize . $userfield;
        $cache1 = S($cachename1);//设置缓存标示
        $cache1 = 0;
        if (!$cache1) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $User_baseM = new UserBaseModel();
            $ret = $User_baseM->getListPage($Wdata, $pageNum, $pageSize);
            foreach ($ret['list'] as $k => $v) {
                $ret1[] = $v['uid'];
            }
            $cache1 = $ret1;
            S($cachename1, $cache1, 60 * 60 * 2);//设置缓存的生存时间
        }
        return $cache1;
    }
    //获取优质女用户
    private function _betteruser($product,$pageNum,$pageSize){
        $redis = $this->redisconn();
        $betteruserlistkey='betteruser-'.$product;
        $betteruserlist = $redis->listGet($betteruserlistkey, 0, -1);
        $betteruserlist= array_slice($betteruserlist, ($pageNum - 1) * $pageSize, $pageSize);
        return $betteruserlist;

    }

    private function _shipin($baseWhere)
    {
        $data = $this->Api_recive_date;
        $product = $this->platforminfo['product'];
        $uid = $this->uid;
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = $data['pagesize'] ? $data['pagesize'] : 30;
        $userfield = $data['userfield'] ? $data['userfield'] : "uid|head|nickname";
        $Wdata = array();
        $UserInfo = $baseWhere;
        $cachename1 = "getsearchuser_" . md5(json_encode($Wdata)) . $pageNum . $pageSize;
        $cache1 = S($cachename1);//设置缓存标示
        $cache1 = 0;
        if (!$cache1) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $User_baseM = new UserBaseModel();
            $ret = $User_baseM->getListPage($Wdata, $pageNum, $pageSize);
            $cache1 = $ret;
            S($cachename1, $cache1, 60 * 60 * 2); //设置缓存的生存时间
        }
        $ret = $cache1;
        $newlistuser = array();
        foreach ($ret['list'] as $k => $v) {

            $tempUserInfo = $this->get_user($v['uid']);
            if ($tempUserInfo) {
                $tempArray = array();
                $tempUserInfo["issayhello"] = $this->isSayHello($v['uid']);
                $UserInfo["isfollow"] = $this->isFollow($v['uid']);
                $tempArray = $this->get_user_field($tempUserInfo, $userfield);

                $newlistuser[] = $tempArray;
            }
        }
        ksort($newlistuser);
        shuffle($newlistuser);

        // Push_data($newlistuser);die;
        $newlistuser = $this->randinad($newlistuser, $product);//随机添加广告
        $return = array();
        if ($pageNum > ceil($ret["totalCount"] / $pageSize)) {
            $return['data']["userlist"] = array();
        } else {
            $return['data']["userlist"] = $newlistuser;
        }
        $page["pagesize"] = $pageSize;
        $page["page"] = $pageNum;
        $page["totalcount"] = $ret["totalCount"];
        $return['data']["page"] = $page;

        Push_data($return);
    }

    /**
     * 退出登录
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @return data
     */
    public function logout()
    {
        $data = $this->Api_recive_date;
        $token = $data["token"];
        $redis = $this->redisconn();
        $redisStr = "token_" . $token;
        $redis->delete($redisStr);
        Push_data();
    }

    /**
     * 修改密码
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request string password 原密码
     * @request string newpassword 新密码,长度6-20位
     * @return data
     */
    public function modifypassword()
    {
        $platforminfo = $this->platforminfo;
        $password = $this->Api_recive_date['password'];
        $newpassword = $this->Api_recive_date['newpassword'];
        $uid = $this->uid;
        $return = array();
        if ($newpassword == '') {
            $return['code'] = ERRORCODE_201;
            $return['message'] = $this->L("XINMIMABUNENGWEIKONG");
        } else {
            $User_baseM = new UserBaseModel();
            $data = $User_baseM->getOne(array('uid' => $uid));
            if($data['password'] == $password){
                $return['code'] = 4001;
                $return['message'] = "原密码错误";
            }
            if($data['password'] == $newpassword){
                $return['code'] = 4002;
                $return['message'] = "不可以与原密码一致";
            }
            $ret = $User_baseM->updateOne(array('uid' => $uid), array('password' => $newpassword));
            $this->get_user($uid, 1);
            if ($ret) {

            } else {
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L("JIUMIMABUNENGWEIKONG");
            }
        }
        Push_data($return);
    }

    //从缓存中取出地区信息
    protected function get_regionlist($id, $reset = 0)
    {
        $redis = $this->redisconn();
        //缓存中储存总数据
        $lang = $redis->listSize('regionlist_' . $id);
        //如果缓存中不存在，则取数据库最新数据存入
        if ($lang == 0) {
            $where = array("country_codeiso" => $id, "level" => 1);
            $RegionM = new RegionModel();
            $res = $RegionM->getList($where);
            foreach ($res as $k => $v) {
                $res[$k] = array(
                    "id" => $v["id"],
                    "name" => $v["code"]
                );

                $value = json_encode($res[$k], true);
                $ret[] = $redis->listPush('regionlist' . $id, $value);
            }
            $result1 = $res;
            //F($cache_name,$res,$path);
        } else {
            $result = $redis->listLrange('regionlist' . $id, 0, -1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    if ($result[$k1] == null) {
                        continue;
                    }
                    $result1[$k1] = json_decode($v1, true);
                }
            }
        }
        return $result1;
    }

    /**
     * 附近人筛选
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request string isphoto 是否照片
     * @request string heights 身高筛选
     * @request string ages 年龄筛选
     * @return data
     */
    public function screening()
    {

        $data = $this->Api_recive_date;
        //基本查询条件
        $UserInfo = $this->UserInfo;

//        if($data['token'] == "" || $data['platforminfo'] == ""){
//
//            $return = [
//                'code' => 4000,
//                'message'  => "参数不全"
//            ];
//            Push_data($return);
//        }

        $baseWhere = array();
        $countryarr = array(
            "TW" => array("HK", "TW", "JP", "MO", "MY", "SG", "TH", "VN", "CN")
        );
        $country = "US";
        //判断是否为国内用户
        //44为中国id
        $countryids = 0;
        if ($countryids == 44) {
            foreach ($countryarr as $k => $v) {
                if (in_array($this->platforminfo["country"], $v)) {
                    $country = $k;
                    break;
                }
            }
        } else {
            foreach ($countryarr as $k => $v) {
                if (in_array($UserInfo["country"], $v)) {
                    $country = $k;
                    break;
                }
            }
        }

        $age1 = $data['age1'] ?  $data['age1'] : 18;
        $age2 = $data['age2'] ?  $data['age2'] : 70;

        $hei1 = $data['hei1'] ?  $data['hei1'] : 140;
        $hei2 = $data['hei2'] ?  $data['hei2'] : 200;

        $baseWhere['country'] = $country;
        $baseWhere['gender'] = array('NEQ',$UserInfo['gender']);//显示异性



        if($data['isphoto'] == 0){
            $baseWhere['isphoto'] = 0;
            $baseWhere['wherephoto'] = 3;
            $this->_fujin($baseWhere);
        }
        //是否照片 1 不限  2 有照片
        $isphoto = $data['isphoto'] ? $data['isphoto'] : 1;

        //有照片
        if($isphoto == 2){

            $AttrValue = new AttrValueModel();

            $hei1s = $hei1."cm";
            $hei1 = $AttrValue->getOne(array('uppercode'=>'height','name'=>$hei1s));

            $hei2s = $hei2."cm";

            $hei2 = $AttrValue->getOne(array('uppercode'=>'height','name'=>$hei2s));
            // dump($hei2);die;
            $age1s = $age1."岁";

            $age1 = $AttrValue->getOne(array('uppercode'=>'age','name'=>$age1s));

            $age2s = $age2."岁";
            $age2 = $AttrValue->getOne(array('uppercode'=>'age','name'=>$age2s));

            $pageNum = $data['page'] ? $data['page'] : 1;
            $pageSize = $data['pagesize'] ? $data['pagesize'] : 30;

            $ages = $age1['id'].",".$age2['id'];
            // $ages = $age1.",".$age2;
            $heis = $hei1['id'].",".$hei2['id'];
            $baseWhere['wherephoto'] = 2;
            $baseWhere['age'] = array('BETWEEN',$ages);//显示异性
            $baseWhere['height'] = array('BETWEEN',$heis);//显示异性

            $data = $this->_fujin($baseWhere);

        }else{        // 当不限的时候

            $data = $this->_fujin($baseWhere);
        }

    }








}


?>


