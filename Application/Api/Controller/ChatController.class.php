<?php
class ChatController extends BaseController {
    public $girl_listkey = "chat_girl_queue_";
    public $boy_listkey = "chat_boy_queue_";
	public function __construct(){
		parent::__construct();
	}
	/**
	 * 初始化接口
	 *
	 *接收uid进行队列匹配
	 * @return data
	 */
    public function queueinit()
    {
        $data = $this->Api_recive_date;
        $girl_uid = $data['girl_uid'];
        $boy_uid = $data['boy_uid'];
        $type = $data['type'];//1 视频 2语音通话
        $redis = $this->redisconn();
        $girl_listkey = $this->girl_listkey.$type;
        $boy_listkey = $this->boy_listkey.$type;
        //入队列
        if(!empty($girl_uid)){
            $redis->listPush($girl_listkey, $girl_uid,0,1);
        }
        if(!empty($boy_uid)){
            $redis->listPush($boy_listkey, $boy_uid,0,1);
        }
        Push_data();
    }


    /**
     * 获取推送随机聊天
     */
    public function getpushuid(){
        //接受参数条件
        $data = $this->Api_recive_date;
        $girl_uid = $data['girl_uid'];
        $boy_uid = $data['boy_uid'];
        $boy_gold = $data['gold']?$data['gold']:0;
        $type = $data['type'];//1 视频 2语音通话
        $redis = $this->redisconn();
        $data = array();
        if(!empty($girl_uid)){//如果传入女性用户id
            //检测男性用户队列
            $redis = $this->redisconn();
            $boycount = $redis->listSize($this->boy_listkey.$type);
            if($boycount <= 0){//队列里不存在 走筛选在线用户
                //查询当前在线男用户
                //获取在线普通异性用户25100
                $OnlineCon = new OnlineController();
                $commonusers = $OnlineCon->get_onlinelist("25100", 1, 1,1);
                if(!empty($commonusers)){
                    //-------按金币余额sta
                    $UserInfo = array();
//                    foreach($commonusers as $v){
//                        $tempPUserInfo = $this->get_user($v);
//                        $UserInfo[$tempPUserInfo['gold']] = $tempPUserInfo;
//                    }
                    foreach($commonusers as $v){
                        //查看redis当前uid是否已经在通话
                        $redis = $this->redisconn();
                        $getid = $redis->get("getpushuid_".$v);
                        //$redis->delete("getpushuid_".$item['uid']);
                        if($getid == "Y"){
                            //跳出当前循环
                            continue;
                        }
                        $tempPUserInfo = $this->get_user($v);
                        $UserInfo[$tempPUserInfo['uid']] = $tempPUserInfo['gold'];
                    }

                    // 第一层可以理解为从数组中键为0开始循环到最后一个
                    $newArr  = $this->sort_with_keyName($UserInfo,"asc");
                    //var_dump(($newArr));DIE;
                    $max = max($newArr);//获取数组最大值

                    //把数组中最大值作为数组搜索条件 找到最后一个所在位置
                    $tmparr = array();
                    foreach($newArr as $k=>$v){
                        if($v == $max){
                            $tmparr[$k] = $v;
                        }

                    }

                    $scoreInfo =array();
                    foreach($tmparr as $key=>$item){

                        if($item > 0){//金币值大于0 走金币返回规则 优先返回最大的金币

                            if(count($tmparr) == 1){
                                $data['uid'] = $key;
                                $data['utype'] = 2;//拨通方式
                                $redis = $this->redisconn();
                                $redis->set("getpushuid_".$item['uid'],"Y",0,0,3600);
                                $redis->set("getpushuid_".$girl_uid,"Y",0,0,3600);
                                Push_data(array("data"=>$data));exit;
                            }else{
                                //多个用户  金币相同的情况下
                                //按好评 平均分
                                $tempscoreInfo = M("vod_score")->where(array("touid"=>$key))->select();
                                if(empty($tempscoreInfo)){
                                    continue;
                                }
                                //平均分
                                $average = array_sum(array_column($tempscoreInfo,"grade"))/count($tempscoreInfo);

                                $scoreInfo[$average] = $key;

                            }
                        }
                    }
                    //-----按金币余额 end

                    //按好评 平均分

                    krsort($scoreInfo);
                    if(!empty($scoreInfo)){
                        foreach($scoreInfo as $k=>$value){
                            if($k <=0){
                                continue;
                            }
                            $data['uid'] = $value;
                            $data['utype'] = 2;//拨通方式
                            $redis = $this->redisconn();
                            $redis->set("getpushuid_".$value,"Y",0,0,3600);
                            $redis->set("getpushuid_".$girl_uid,"Y",0,0,3600);
                            Push_data(array("data"=>$data));exit;
                        }
                    }
                    //按好评 平均分 end

                    //按vip
                    foreach($commonusers as $v){
                        $getid = $redis->get("getpushuid_".$v);
                        if($getid == "Y"){
                            //跳出当前循环
                            continue;
                        }
                        $vipInfo = $this->get_user($v);
                        if($vipInfo['vipgrade']>0){
                            $data['uid'] = $v;
                            $data['utype'] = 2;//拨通方式
                            $redis = $this->redisconn();
                            $redis->set("getpushuid_".$v,"Y",0,0,3600);
                            $redis->set("getpushuid_".$girl_uid,"Y",0,0,3600);
                            Push_data(array("data"=>$data));exit;
                        }
                    }
                    //按vip end


                    //按财富消费

                    $goldAll =array();
                    foreach($commonusers as $v){
                        $tempgoldInfo = M("p_money_del")->where(array("to_uid"=>$v))->select();
                        $getid = $redis->get("getpushuid_".$v);
                        if($getid == "Y"){
                            //跳出当前循环
                            continue;
                        }
                        if(empty($tempgoldInfo)){
                            continue;
                        }
                        //总消费
                        $goldcount = array_sum(array_column($tempgoldInfo,"money"));

                        $goldAll[$goldcount] = $v;
                    }
                    krsort($goldAll);
                    if(!empty($goldAll)){
                        foreach($goldAll as $k=>$value){
                            if($k <=0){
                                continue;
                            }
                            $data['uid'] = $value;
                            $data['utype'] = 2;//拨通方式\
                            $redis->set("getpushuid_".$value,"Y",0,0,3600);
                            $redis->set("getpushuid_".$girl_uid,"Y",0,0,3600);
                            Push_data(array("data"=>$data));exit;
                        }
                    }
                    //按财富消费end



                    //返回当前在线用户
                    foreach($commonusers as $v){
                        $getid = $redis->get("getpushuid_".$v);
                        if($getid == "Y"){
                            //跳出当前循环
                            continue;
                        }
                        $data['uid'] = $v;
                        $data['utype'] = 2;//拨通方式
                        $redis = $this->redisconn();
                        $redis->set("getpushuid_".$v,"Y",0,0,3600);
                        $redis->set("getpushuid_".$girl_uid,"Y",0,0,3600);
                        Push_data(array("data"=>$data));exit;
                    }
                }else{
                    //如果没有在线用户  则加入队列 进行等待
                    $redis->listPush($this->girl_listkey.$type, $girl_uid);
                    $data['uid'] = '';
                    $data['utype'] = '';
                    Push_data(array("data"=>$data));exit;
                }

                //如果没有在线用户  则加入队列 进行等待
                $redis->listPush($this->girl_listkey.$type, $girl_uid);
                $data['uid'] = '';
                $data['utype'] = '';
                Push_data(array("data"=>$data));exit;

            }else{
                $usid = $redis->listPop($this->boy_listkey.$type, 1);//获取队列中的用户uid
                //同时移除该用户队列
                $redis->listRemove($this->boy_listkey.$type, $usid);
                $data['uid'] = $usid;
                $data['utype'] = 1;//直接连线
                $redis->set("getpushuid_".$usid,"Y",0,0,3600);
                $redis->set("getpushuid_".$girl_uid,"Y",0,0,3600);
                Push_data(array("data"=>$data));exit;
            }

        }elseif(!empty($boy_uid)){//传入男性uid 进行筛选女性用户
            //检测女性用户队列
            $girlcount = $redis->listSize($this->girl_listkey.$type);
            //var_dump($girlcount);DIE;
            if($girlcount <= 0){//队列里不存在 走筛选在线用户
                $OnlineCon = new OnlineController();
                $commonusers = $OnlineCon->get_onlinelist("25100", 1, 2,1);
                //var_dump($commonusers);
                if(!empty($commonusers)){
                    foreach($commonusers as $couid){
                        $getid = $redis->get("getpushuid_".$couid);
                        //$redis->delete("getpushuid_".$couid);
                        if($getid == "Y"){
                            //跳出当前循环
                            continue;
                        }

                        $compereinfo = M("compere_info")->where(array("uid"=>$couid))->find();
                        $platform_id = M("vod_admin")->where(array("id"=>$compereinfo['platform_id']))->find();
                        if($compereinfo['is_signing'] == 1 && $platform_id['company_id'] == 0){
                                //获取女性收费标准
                                if($type == 1){
                                    if($compereinfo['gold_conf'] > $boy_gold){
                                        continue;
                                    }
                                }elseif ($type == 2){
                                    $gold_conf = $compereinfo['gold_conf']/2;
                                    if( $gold_conf > $boy_gold){
                                        continue;
                                    }
                                }
                                $data['uid'] = $compereinfo['uid'];
                                $data['utype'] = 2;
                                $redis = $this->redisconn();
                                $redis->set("getpushuid_".$couid,"Y",0,0,3600);
                                $redis->set("getpushuid_".$boy_uid,"Y",0,0,3600);
                                Push_data(array("data"=>$data));exit;
                        }

                    }

                    foreach($commonusers as $couid){
                        $redis = $this->redisconn();
                        $getid = $redis->get("getpushuid_".$couid);
                        if($getid == "Y"){
                            //跳出当前循环
                            continue;
                        }
                        $compereinfo = M("compere_info")->where(array("uid"=>$couid))->find();
                        if($compereinfo['is_signing'] == 1){
                            //获取女性收费标准
                            if($type == 1){
                                if($compereinfo['gold_conf'] > $boy_gold){
                                    continue;
                                }
                            }elseif ($type == 2){
                                $gold_conf = $compereinfo['gold_conf']/2;
                                if( $gold_conf > $boy_gold){
                                    continue;
                                }
                            }
                            $data['uid'] = $compereinfo['uid'];
                            $data['utype'] = 2;
                            $redis = $this->redisconn();
                            $redis->set("getpushuid_".$couid,"Y",0,0,3600);
                            $redis->set("getpushuid_".$boy_uid,"Y",0,0,3600);
                            Push_data(array("data"=>$data));exit;
                        }

                    }

                    foreach($commonusers as $couid){
                        $redis = $this->redisconn();
                        $getid = $redis->get("getpushuid_".$couid);
                        if($getid == "Y"){
                            //跳出当前循环
                            continue;
                        }
                        $compereinfo = M("compere_info")->where(array("uid"=>$couid))->find();

                        if($compereinfo['is_signing'] == 0){
                            //获取女性收费标准
                            if($type == 1){
                                if($compereinfo['gold_conf'] > $boy_gold){
                                    continue;
                                }
                            }elseif ($type == 2){
                                $gold_conf = $compereinfo['gold_conf']/2;
                                if( $gold_conf > $boy_gold){
                                    continue;
                                }
                            }
                            $data['uid'] = $compereinfo['uid'];
                            $data['utype'] = 2;
                            $redis->set("getpushuid_".$couid,"Y",0,0,3600);
                            $redis->set("getpushuid_".$boy_uid,"Y",0,0,3600);
                            Push_data(array("data"=>$data));exit;
                        }

                    }

                    //如果没有在线用户  则加入队列 进行等待
                    $redis = $this->redisconn();
                    $redis->listPush($this->boy_listkey.$type, $boy_uid);
                    $data['uid'] = '';
                    $data['utype'] = '';
                    Push_data(array("data"=>$data));exit;
                    //按照好评

                }else{
                    //如果没有在线用户  则加入队列 进行等待
                    $redis = $this->redisconn();
                    $redis->listPush($this->boy_listkey.$type, $boy_uid);
                    $data['uid'] = '';
                    $data['utype'] = '';
                    Push_data(array("data"=>$data));exit;
                }
            }else{//优先返回队列用户
                $redis = $this->redisconn();
                $usid = $redis->listPop($this->girl_listkey.$type, 1);//获取队列中的用户uid
                $compereinfo = M("compere_info")->where(array("uid"=>$usid))->find();
                if($type == 1){
                    if($compereinfo['gold_conf'] > $boy_gold){
                        $data['uid'] = '';
                        $data['utype'] = '';
                        Push_data(array("data"=>$data));exit;
                    }
                }elseif ($type == 2){
                    $gold_conf = $compereinfo['gold_conf']/2;
                    if( $gold_conf > $boy_gold){
                        $data['uid'] = '';
                        $data['utype'] = '';
                        Push_data(array("data"=>$data));exit;
                    }
                }
                //同时移除该用户队列
                $redis = $this->redisconn();
                $redis->listRemove($this->girl_listkey.$type, $usid);
                $data['uid'] = $usid;
                $data['utype'] = 1;//直接连线
                $redis->set("getpushuid_".$usid,"Y",0,0,3600);
                $redis->set("getpushuid_".$boy_uid,"Y",0,0,3600);
                Push_data(array("data"=>$data));exit;
            }

        }


    }



    public function sort_with_keyName($arr,$orderby='desc'){
                $new_array = array();
                $new_sort = array();
                foreach($arr as $key => $value){
                    $new_array[] = $value;
                }
            if($orderby=='asc'){
                asort($new_array);
            }else{
                arsort($new_array);
            }
            foreach($new_array as $k => $v){
                foreach($arr as $key => $value){
                    if($v==$value){
                        $new_sort[$key] = $value;
                        unset($arr[$key]);
                        break;
                    }
                }
            }
            return $new_sort;
            }
    /**
     * 聊天评价评分
     *
     */
    public function score()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $touid = $data['touid'];//被评分人uid
        $grade = $data['grade'];//评分星级
        $type = $data['type'];//1视频 2语音
        if(!empty($grade) && !empty($touid)){
            $arr = array(
                "uid"=>$uid,
                "touid"=>$touid,
                "grade"=>$grade,
                "custom"=>isset($data['custom']) ? $data['custom'] :'',
                "sysitem"=>isset($data['sysitem']) ? $data['sysitem']: '',
                "ctime"=>time(),
                "type"=>$type
            );
            M("vod_score")->add($arr);
        }

        Push_data();die;

    }


    //获取系统内置评价短语
    public function getsysitem(){
        $data = $this->Api_recive_date;
        $gender = $data['gender'];//1 男 2女
        $sys = M("vod_private_set")->find();
        if($gender == 1){
            $systemscore = json_decode($sys['systemscore'],true);
        }else{
            $systemscore = json_decode($sys['sysitemscoregirl'],true);
        }
        $info = array();
        $info_arr = array();
        $push_info = array();
        foreach ($systemscore as $key=>$value){
            $grade = substr($key,5,6);
            //$info[]['grade'] = $grade;
            if(!empty($value)){
                $info = explode("|",$value);
            }
            foreach($info as $k=>$v){
                $info_arr[$k]['grade'] = $grade;
                $info_arr[$k]['str'] = $v;
            }
            $push_info[$key] = $info_arr;

        }
        $newpushinfo = array_merge($push_info['grade1'],$push_info['grade2'],$push_info['grade3'],$push_info['grade4'],$push_info['grade5']);
        $data_push = array();
        foreach($newpushinfo as $npush){
            $data_push[$npush['str']] = $npush;
        }
        sort($data_push);
        Push_data(array("data"=>$data_push));
    }



    //拒接统计
    public function comperenocall(){
        $data = $this->Api_recive_date;
        $uid = $data['uid'];
        $type = $data['type'];
        $date = date("Ymd");
        $nocall = new CompereNocallModel();
        $info = $nocall->getOne(array("uid"=>$uid,"ontime"=>$date));

        $where = array();
        //$where['ontime'] = $date;
        switch ($type){
            //1主动发起视频主动挂断
            case 1:
                $where['typeonenum'] = $info['typeonenum'] +1;
                $where['videonum'] = $info['videonum'] +1;
                break;
            case 2://2主动发起语音主动挂断
                $where['typetwonum'] = $info['typetwonum'] +1;
                $where['voicenum'] = $info['voicenum'] +1;
                break;
            //3被动接收视频主动挂断
            case 3:
                $where['typethreenum'] = $info['typethreenum'] +1;
                $where['videonum'] = $info['videonum'] +1;
                break;
            //4被动接受语音主动挂断
            case 4:
                $where['typefournum'] = $info['typefournum'] +1;
                $where['voicenum'] = $info['voicenum'] +1;
                break;
            //5主动发起未接听
            case 5:
                $where['typefivenum'] = $info['typefivenum'] +1;
                break;
            //6被动发起未接听
            case 6:
                $where['typesixnum'] = $info['typesixnum'] +1;
                break;
            //7主动发起对方拒绝未接听
            case 7:
                $where['typesevennum'] = $info['typesevennum'] +1;
                break;
            //被动发起拒绝未接听8
            case 8:
                $where['typeightnum'] = $info['typeightnum'] +1;
                break;
            //回拨
            case 9:
                $where['typeninenum'] = $info['typeninenum'] +1;
                break;
            case 10://女性拨通 男性挂断  计算接通次数
                $where['videonum'] = $info['videonum'] +1;
                break;
            case 11://女性拨通 男性挂断  计算接通次数
                $where['voicenum'] = $info['voicenum'] +1;
                break;
            case 12://男性拨打给女性视频通话，双方通话后，女性挂断了这次视频通话
                $where['videonum'] = $info['videonum'] +1;
                break;
            case 13://男性拨打给女性语音通话，双方通话后，女性挂断了这次语音通话
                $where['voicenum'] = $info['voicenum'] +1;
                break;
            case 14://女性拨打给男性视频通话，双方通话后，男性挂断了这次视频通话
                $where['videonum'] = $info['videonum'] +1;
                break;
            case 15://女性拨打给女性语音通话，双方通话后，男性挂断了这次语音通话
                $where['voicenum'] = $info['voicenum'] +1;
                break;

        }

        $nocall->updateOne(array("uid"=>$uid,"ontime"=>$date),$where);

        Push_data();
    }





    public function instuidtime(){
        $data = $this->Api_recive_date;
        $boyuid = $data['boy_uid'];
        $girl_uid = $data['girl_uid'];
        $type = $data['type'];//1视频 2语音
        $calltime = $data['calltime'];//具体时间
        $card_type = $data['card_type'];//1you  2mei
        $card_gold = 0;
        if($card_type == 1){
            $card_gold = 1;
        }
        $goldtime = $data['goldtime']-$card_gold;//计费时间
        $compereinfo = M("compere_info")->where(array("uid"=>$girl_uid))->find();
        if($type == 1){
            $gold = $compereinfo['gold_conf'] * $goldtime;
        }elseif($type == 2){
            $gold = ($compereinfo['gold_conf']/2) * $goldtime;
        }
        $boygirl_type = $boyuid.$girl_uid.$type;
        $id =  M("tmp_calltime")->where(array("boygirl_type"=>$boygirl_type))->getField("boygirl_type");
        if($id){
            M("tmp_calltime")->where(array("boygirl_type"=>$boygirl_type))->save(array("calltime"=>$calltime,"gold"=>$gold));
        }else{
            M("tmp_calltime")->add(array("boygirl_type"=>$boygirl_type,"calltime"=>$calltime,"gold"=>$gold));
        }
        Push_data();
    }



    public function getuidtime(){
        $data = $this->Api_recive_date;
        $boyuid = $data['boy_uid'];
        $girl_uid = $data['girl_uid'];
        $type = $data['type'];//1视频 2语音
        $boygirl_type = $boyuid.$girl_uid.$type;
        $info = M("tmp_calltime")->where(array("boygirl_type"=>$boygirl_type))->find();
        $com = new CompereInfoModel();
        $com_info = $com->getOne(array("uid"=>$girl_uid));
        //平台抽水
        $talk_gold_pump = $com_info['talk_gold_pump'];//抽水比例
        //女生应得
        $girl_gold = $info['gold'] - ($info['gold']*($talk_gold_pump/100));


        $arr['data'] = array(
            "calltime"=>$info['calltime'],
            "gold"=>$info['gold'],
            "girl_gold"=>$girl_gold
        );
        Push_data($arr);

    }


    //清除redis正在通话的人
    public function unsetuidtype(){
        $data = $this->Api_recive_date;
        $redis = $this->redisconn();
        $redis->delete("getpushuid_".$data['boy_uid']);
        $redis->delete("getpushuid_".$data['girl_uid']);
        Push_data();
    }
    public function unsetuidtype1(){
        $data = $this->Api_recive_date;
        $redis = $this->redisconn();
        $info = M("user_base")->where(array("product"=>25100))->select();
        foreach($info as $value){
            $redis->delete("getpushuid_".$value['uid']);
        }
        Push_data();
    }

    public function unstate(){
        $data = $this->Api_recive_date;
        $uid = $data['uid'];
        $redis = $this->redisconn();
        $Y = $redis->get("getpushuid_".$uid);
        if($Y == "Y"){
            $info['data']['state'] = 2; //通话中
        }else{
            $info['data']['state'] = 1;//空闲中
        }
        Push_data($info);
    }


    /**
     * 用户签到
     */
    public function usersignin(){
        $info = $this->UserInfo;
        $uid = $this->uid?$this->uid:$_POST['uid'];
        $date = date("Ymd");

        $day_7 = time()+(86400*7);

        $a=date("Y",$day_7);
        $b=date("m",$day_7);
        $c=date("d",$day_7);
        $d=date("G",$day_7);
        $e=date("i",$day_7);
        $f=date("s",$day_7);

        $endtime = $a.'年'.$b.'月'.$c.'日'.$d.'时'.$e.'分'.$f.'秒';

        //检测用户是否首签
        $vod_signin = new VodSigninModel();

        $signinfo = $vod_signin->getList(array("uid"=>$uid));
        if(empty($signinfo)){
            $arr = array(
                "uid"=>$uid,
                "signinday"=>1,
                "type"=>1,
                "endtime"=>$date
            );
            $vod_signin->addOne($arr);
            $pushdata = array(
                "uid"=>$uid,
                "videocard"=>1,
                "voicecard"=>1,
                "giftcard"=>3,
                "signinday"=>1,
                "endtime"=>$day_7
            );
            M("vod_pack")->add($pushdata);
            $pushdata['endtime'] = $endtime;
            Push_data(array("data"=>$pushdata));
        }else{
            //不是首签 查询上一次签到时间 是否断签
            $date_prev = date("Ymd",time()-86400);
            $prevsign = $vod_signin->getOne(array("uid"=>$uid,"endtime"=>$date_prev,"type"=>1));

            if(isset($prevsign['signinday']) && $prevsign['signinday'] == 7){
                $prevsign = $vod_signin->getOne(array("uid"=>$uid,"endtime"=>$date_prev,"type"=>2));

            }
            if(!empty($prevsign)){//未断签
                //没有断签 //查看当前是否签到7次 第一轮

                if($prevsign['signinday']!=7 && $prevsign['signinday'] <= 14){

                    $signinday = $prevsign['signinday']+1;
                    //第一轮
                    if($prevsign['type'] == 1){
                        $arr = array(
                            "signinday"=>$signinday,
                            "endtime"=>$date
                        );
                        $vod_signin->updateOne(array("uid"=>$uid,"type"=>$prevsign['type']),$arr);
                        //判断是第几天
                        if($signinday == 1){ //第一天
                            $pushdata = array(
                                "uid"=>$uid,
                                "videocard"=>1,
                                "voicecard"=>1,
                                "giftcard"=>3,
                                "signinday"=>$signinday,
                                "endtime"=>$day_7
                            );
                            M("vod_pack")->add($pushdata);
                            $pushdata['endtime'] = $endtime;
                            Push_data(array("data"=>$pushdata));
                        }elseif($signinday == 2){
                            $pushdata = array(
                                "uid"=>$uid,
                                "videocard"=>1,
                                "giftcard"=>1,
                                "signinday"=>$signinday,
                                "endtime"=>$day_7
                            );
                            M("vod_pack")->add($pushdata);
                            $pushdata['endtime'] = $endtime;
                            Push_data(array("data"=>$pushdata));

                        }elseif($signinday == 3){
                            $pushdata = array(
                                "uid"=>$uid,
                                "voicecard"=>1,
                                "giftcard"=>2,
                                "signinday"=>$signinday,
                                "endtime"=>$day_7
                            );
                            M("vod_pack")->add($pushdata);
                            $pushdata['endtime'] = $endtime;
                            Push_data(array("data"=>$pushdata));
                        }elseif($signinday == 4){
                            $pushdata = array(
                                "uid"=>$uid,
                                "videocard"=>1,
                                "voicecard"=>1,
                                "giftcard"=>3,
                                "signinday"=>$signinday,
                                "endtime"=>$day_7
                            );
                            M("vod_pack")->add($pushdata);
                            $pushdata['endtime'] = $endtime;
                            Push_data(array("data"=>$pushdata));
                        }elseif ($signinday == 5){
                            $pushdata = array(
                                "uid"=>$uid,
                                "videocard"=>1,
                                "giftcard"=>1,
                                "signinday"=>$signinday,
                                "endtime"=>$day_7
                            );
                            M("vod_pack")->add($pushdata);
                            $pushdata['endtime'] = $endtime;
                            Push_data(array("data"=>$pushdata));
                        }elseif ($signinday == 6){
                            $pushdata = array(
                                "uid"=>$uid,
                                "voicecard"=>1,
                                "giftcard"=>2,
                                "signinday"=>$signinday,
                                "endtime"=>$day_7
                            );
                            M("vod_pack")->add($pushdata);
                            $pushdata['endtime'] = $endtime;
                            Push_data(array("data"=>$pushdata));
                        }elseif ($signinday == 7){
                            $pushdata = array(
                                "uid"=>$uid,
                                "videocard"=>2,
                                "voicecard"=>2,
                                "giftcard"=>5,
                                "signinday"=>$signinday,
                                "endtime"=>$day_7
                            );
                            M("vod_pack")->add($pushdata);
                            $pushdata['endtime'] = $endtime;
                            Push_data(array("data"=>$pushdata));
                        }



                    }

                    //第二轮
                    if($prevsign['type'] == 2){
                        $arr = array(
                            "signinday"=>$signinday,
                            "endtime"=>$date
                        );
                        $vod_signin->updateOne(array("uid"=>$uid,"type"=>$prevsign['type']),$arr);
                        if ($signinday == 8 || $signinday == 10 || $signinday == 12){
                            $pushdata = array(
                                "uid"=>$uid,
                                "videocard"=>1,
                                "signinday"=>$signinday,
                                "endtime"=>$day_7
                            );
                            M("vod_pack")->add($pushdata);
                            $pushdata['endtime'] = $endtime;
                            Push_data(array("data"=>$pushdata));
                        }elseif($signinday == 9 || $signinday == 11 || $signinday == 13){
                            $pushdata = array(
                                "uid"=>$uid,
                                "voicecard"=>1,
                                "signinday"=>$signinday,
                                "endtime"=>$day_7
                            );
                            M("vod_pack")->add($pushdata);
                            $pushdata['endtime'] = $endtime;
                            Push_data(array("data"=>$pushdata));
                        }elseif ($signinday == 14){
                            $pushdata = array(
                                "uid"=>$uid,
                                "voicecard"=>1,
                                "videocard"=>1,
                                "signinday"=>$signinday,
                                "endtime"=>$day_7
                            );
                            M("vod_pack")->add($pushdata);
                            $pushdata['endtime'] = $endtime;
                            //第二轮签满   放入用户不显示弹窗
                            M("vod_nosignin")->add(array("uid"=>$uid));
                            Push_data(array("data"=>$pushdata));
                        }

                    }


                }
                if($prevsign['signinday'] == 7){

                    //进行第二轮签到
                    $pushdata = array(
                        "uid"=>$uid,
                        "videocard"=>1,
                        "signinday"=>7,
                        "endtime"=>$day_7
                    );
                    M("vod_pack")->add($pushdata);
                    $pushdata['endtime'] = $endtime;

                    $arr = array(
                        "uid"=>$uid,
                        "signinday"=>1,
                        "type"=>2,
                        "endtime"=>$date
                    );
                    $vod_signin->addOne($arr);
                    Push_data(array("data"=>$pushdata));
                }

            }else{
                //断签处理
                //判断是否是第一轮断签
                if($prevsign['type'] == 1){
                    $arr = array(
                        "uid"=>$uid,
                        "signinday"=>1,
                        "type"=>1,
                        "endtime"=>$date
                    );
                    $vod_signin->addOne($arr);
                    $pushdata = array(
                        "uid"=>$uid,
                        "videocard"=>1,
                        "voicecard"=>1,
                        "giftcard"=>3,
                        "signinday"=>1,
                        "endtime"=>$day_7
                    );
                    M("vod_pack")->add($pushdata);
                    $pushdata['endtime'] = $endtime;
                    Push_data(array("data"=>$pushdata));
                }elseif($prevsign['type'] == 2){
                    $arr = array(
                        "uid"=>$uid,
                        "signinday"=>1,
                        "type"=>2,
                        "endtime"=>$date
                    );

                    $vod_signin->addOne($arr);
                    $pushdata = array(
                        "uid"=>$uid,
                        "videocard"=>1,
                        "signinday"=>1,
                        "endtime"=>$day_7
                    );
                    M("vod_pack")->add($pushdata);
                    $pushdata['endtime'] = $endtime;
                    Push_data(array("data"=>$pushdata));
                }
                $pushdata = array(
                    "uid"=>'',
                    "videocard"=>'',
                    "signinday"=>'',
                    "endtime"=>''
                );
                Push_data(array("data"=>$pushdata));
            }

        }

    }



    //查看用户是否符合签到条件

    public function getsignin(){
        $uid = $_POST['uid']?$_POST['uid']:$this->uid;
        //获取是否存在
        $nosign = M("vod_nosignin")->where(array("uid"=>$uid))->find();
        if(!empty($nosign)){
            Push_data(array("data"=>2));
        }else{
            $vod_signin = new VodSigninModel();
            //反查数据库查看是否连续断签3次
            $date_prev = date("Ymd",time()-86400);
            $prevsign = $vod_signin->getList(array("uid"=>$uid));
            if(count($prevsign) < 3){
                Push_data(array("data"=>1));
            }elseif(count($prevsign) >= 3){
                //查询第三次是否断开
                $isshow = $vod_signin->getOne(array("uid"=>$uid,"endtime"=>$date_prev));
                if(empty($isshow)){
                    Push_data(array("data"=>2));//不显示
                }else{
                    Push_data(array("data"=>1));
                }
            }


        }

    }

    public function issginpush(){
        $uid =$this->uid;
        $content='亲爱哒~你还没有领取今天的签到礼包呢，快来签到吧~连续7天签到更有超丰厚奖励等着你哟~  点我签到。';
        $argv1 = array();
        $argv1['url'] = "http://127.0.0.1:81/userauth/sendtzmsg/?uid=" . $uid . "&content=" . $content;
        $argv1['time'] =5;
        $argv1 = base64_encode(json_encode($argv1));
        $command = "/usr/bin/php " . WR . "/openurl.php " . $argv1 . " > /dev/null &";
        system($command);
        Push_data();
    }


    //查看用户是否签到
    public function getisign(){
        $uid = $this->uid;
        $date_prev = date("Ymd");
        $vod_signin = new VodSigninModel();
        $prevsign = $vod_signin->getOne(array("uid"=>$uid,"endtime"=>$date_prev));
        if(empty($prevsign)){
            $a = $vod_signin->getOne(array("uid"=>$uid,"endtime"=>$date_prev,"type"=>2));
            if($a['signinday'] >= 14){
                Push_data(array("data"=>2));
            }
            if(count($a) > 5){
                Push_data(array("data"=>2));
            }

            Push_data(array("data"=>1));//未签到
        }else{
            $a = $vod_signin->getOne(array("uid"=>$uid,"endtime"=>$date_prev,"type"=>2));
            if($a['signinday'] >= 14){
                Push_data(array("data"=>2));
            }
            if(count($a) > 5){
                Push_data(array("data"=>2));
            }
            Push_data(array("data"=>2));//
        }

    }



    //获取背包卡

    public function getpacklist(){
        $data = $this->Api_recive_date;
        $uid =  $this->uid;
        $type = $data['type'];//1待使用 2已过期


        $pack = M("vod_pack")->where(array("uid"=>$uid))->select();
        $new123 = array();
        $newtype1 = array();
        $card = array();
        $card2 = array();
        $card3 = array();
        $new = array();
        $new2 = array();
        $new3 = array();
        if(!empty($pack)){
            foreach($pack as $k=>$value){
                $day_7 = $value['endtime'];

                $a=date("Y",$day_7);
                $b=date("m",$day_7);
                $c=date("d",$day_7);
                $d=date("G",$day_7);
                $e=date("i",$day_7);
                $f=date("s",$day_7);

                $endtime = $a.'年'.$b.'月'.$c.'日'.$d.'时'.$e.'分'.$f.'秒';

                if($value['endtime'] < time()){

                    if($value['videocard'] != 0){
                        $new[$k]['endtime'] = $endtime;
                        $new[$k]['type'] = "videocard";
                        $new[$k]['count'] = $value['videocard'];
                        //array_push($outarr,$new);
                    }
                    if($value['voicecard'] != 0){
                        $new2[$k]['endtime'] = $endtime;
                        $new2[$k]['type'] = "videocard";
                        $new2[$k]['count'] = $value['voicecard'];
                        //array_push($outarr,$new2);
                    }
                    if($value['giftcard'] != 0){
                        $new3[$k]['endtime'] = $endtime;
                        $new3[$k]['type'] = "giftcard";
                        $new3[$k]['count'] = $value['giftcard'];
                       // array_push($outarr,$new3);
                    }
                    $new123 = array_merge($new,$new2,$new3);
//                    //已过期
//                    array_push($outarr,$value);
                }else{

                    if($value['videocard'] != 0){
                        $card[$k]['endtime'] = $endtime;
                        $card[$k]['type'] = "videocard";
                        $card[$k]['count'] = $value['videocard'];
                        //array_push($outarr,$new);
                    }
                    if($value['voicecard'] != 0){
                        $card2[$k]['endtime'] = $endtime;
                        $card2[$k]['type'] = "voicecard";
                        $card2[$k]['count'] = $value['voicecard'];
                        //array_push($outarr,$new2);
                    }
                    if($value['giftcard'] != 0){
                        $card3[$k]['endtime'] = $endtime;
                        $card3[$k]['type'] = 'giftcard';
                        $card3[$k]['count'] = $value['giftcard'];
                        // array_push($outarr,$new3);
                    }



                    $newtype1 = array_merge($card,$card2,$card3);
//                    $value['endtime'] = $endtime;
//                    array_push($newarr,$value);
                }
            }


        }

        if($type == 1){
            Push_data(array("data"=>$newtype1));
        }elseif ($type == 2){
            Push_data(array("data"=>$new123));
        }else{
            Push_data();
        }


    }



    public function mydata($uid){
        $uid = $uid;
        $day = date("Ymd");
        //查询uid 当天登录记录
        $comperetime = M("compere_time")->where(array("uid"=>$uid,"ontime"=>$day))->select();
        $data = array();
        $time_f3 = 0;
        if(!empty($comperetime)){
            $miao= 0;
            foreach($comperetime as $value){
                    if($value['endtime'] != 0 && $value['endtime'] > $value['startime']){
                        $miao += $value['endtime'] - $value['startime'];
                        $m_20 = $value['endtime'] - $value['startime'];
                        $f_20 = ceil($m_20/60);//20分钟以上 放入数组
                        if($f_20 >= 20 ){
                            $time_f3 += $f_20;
                        }
                    }elseif($value['endtime'] == 0){
                        $miao += time() - $value['startime'];
                    }
            }
            $time_f = ceil($miao/60);//在线时长
        }

        //获取语音通话时长
        $star = strtotime(date('Y-m-d'));
        $end = strtotime(date('Y-m-d',strtotime('+1 day')));


       $talk = M("talk_record")->where(array("girl_uid"=>$uid))->select();
//       $talksql = "select * from t_talk_record where offtime > $star and offtime < $end and girl_uid=".$uid;
//       $talk = M()->query($talksql);
       $videotime = 0;//视频通话时间
       $audiotime = 0;//语音通话时间
       $video_gold = 0;//视频收益
       $audio_gold = 0;//语音收益
       $day_gift = 0;//礼物收益
        $talk_num = 0;//通话主动拨号次数
        $talk_nocall_num = 0;//通话主动挂断次数
        $bnocall = 0;//未接通话次数
        $refuse_call = 0;//未接通话拒绝次数
        $callback = 0;//未接通话回拨次数
       $day_talk = array();

       foreach($talk as $item){
           if($item['offtime'] > $star && $item['offtime'] < $end){
                if($item['type'] == 1){
                    $videotime += $item['time'];
                    $video_gold += $item['girl_in_gold'];
                }
                if($item['type'] ==2){
                    $audiotime += $item['time'];
                    $audio_gold += $item['girl_in_gold'];
                }

                //获取礼物收益
                if(!empty($item['only'])){
                    $gift = M("talk_gift")->where(array("only"=>$item['only']))->find("gift_gold");
                    $day_gift +=$gift['gift_gold'];
                }

                //获取主动拨通次数
               $compere_nocall = M("compere_nocall")->where(array("uid"=>$uid,"ontime"=>$day))->find();
                //通话主动拨号次数
           if(!empty($compere_nocall)){
               $talk_num = $compere_nocall['videonum'] + $compere_nocall['voicenum'];
               $talk_nocall_num = $compere_nocall['typeonenum'] + $compere_nocall['typetwonum'] + $compere_nocall['typethreenum'] + $compere_nocall['typefournum'];
               $bnocall = $compere_nocall['typesixnum'];
               $refuse_call = $compere_nocall['typeightnum'];
               $callback = $compere_nocall['typeninenum'];
           }

           }
       }
        $dataAll = array(
            "time_f"=>$this->secondConversion($time_f),
            "time_f3"=>$this->secondConversion($time_f3),
            "audiotime"=>$this->secondConversion($audiotime),
            "videotime"=>$this->secondConversion($videotime),
            "day_gift"=>$day_gift,
            "video_gold"=>$video_gold,
            "audio_gold"=>$audio_gold,
            "talk_num"=>$talk_num,
            "talk_nocall_num"=>$talk_nocall_num,
            "bnocall"=>$bnocall,
            "callback"=>$callback,
            "refuse_call"=>$refuse_call
        );
        return $dataAll;
    }

    public function mydata2($uid){
        $uid = $uid;
        $day = date("Ymd");
        //查询uid 当天登录记录
        $comperetime = M("compere_time2")->where(array("uid"=>$uid,"ontime"=>$day))->select();
        $data = array();
        $time_f3 = 0;
        if(!empty($comperetime)){
            $miao= 0;
            foreach($comperetime as $value){
                if($value['endtime'] != 0 && $value['endtime'] > $value['startime']){
                    $miao += $value['endtime'] - $value['startime'];
                    $m_20 = $value['endtime'] - $value['startime'];
                    $f_20 = ceil($m_20/60);//20分钟以上 放入数组
                    if($f_20 >= 20 ){
                        $time_f3 += $f_20;
                    }
                }elseif($value['endtime'] == 0){
                    $miao += time() - $value['startime'];
                }
            }
            $time_f = ceil($miao/60);//在线时长
        }




        $dataAll = array(
            "time_f"=>$this->secondConversion($time_f),
            "time_f3"=>$this->secondConversion($time_f3),
        );
        return $dataAll;
    }


    //获取本月数据
    public function mydatamonth($uid){
        $uid = $uid;//$_GET['uid'];
        $beginThismonth = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $endThismonth = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
        $sql = "select * from t_compere_time where startime > $beginThismonth and startime < $endThismonth and uid=".$uid;
        $statday = date("Ymd",$beginThismonth);
        $endday = date("Ymd",$endThismonth);
        $res = M()->query($sql);
        $miao= 0;
        $online_3_day = array();
        foreach($res as $value){
            if($value['endtime'] != 0 && $value['endtime'] > $value['startime']){
                $miao += $value['endtime'] - $value['startime'];
                $m_20 = $value['endtime'] - $value['startime'];
                $f_20 = ceil($m_20/60);//20分钟以上 放入数组
                $v = date("Ymd",$value['endtime']);
                if($f_20 >= 20 ){
                    $online_3_day[$v] += $f_20;
                }

            }elseif($value['endtime'] == 0){
                $miao += time() - $value['startime'];
            }
        }
        $time_f = ceil($miao/60);//在线时长

        $talk = M("talk_record")->where(array("girl_uid"=>$uid))->select();
        $videotime = 0;//视频通话时间
        $audiotime = 0;//语音通话时间
        $video_gold = 0;//视频收益
        $audio_gold = 0;//语音收益
        $day_gift = 0;//礼物收益
        $talk_num = 0;//通话主动拨号次数
        $talk_nocall_num = 0;//通话主动挂断次数
        $bnocall = 0;//未接通话次数
        $refuse_call = 0;//未接通话拒绝次数
        $callback = 0;//未接通话回拨次数
        $day_talk = array();
        foreach($talk as $item){
            if($item['offtime'] > $beginThismonth && $item['offtime'] < $endThismonth){
                if($item['type'] == 1){
                    $videotime += $item['time'];
                    $video_gold += $item['girl_in_gold'];
                }
                if($item['type'] ==2){
                    $audiotime += $item['time'];
                    $audio_gold += $item['girl_in_gold'];
                }

                //获取礼物收益
                if(!empty($item['only'])){
                    $gift = M("talk_gift")->where(array("only"=>$item['only']))->find("gift_gold");
                    $day_gift +=$gift['gift_gold'];
                }



            }
        }
//获取主动拨通次数
        $sql_nocall = "select * from t_compere_nocall where ontime > $statday and ontime < $endday and uid=".$uid;

        $compere = M()->query($sql_nocall);
        //通话主动拨号次数
        if(!empty($compere)){
            foreach ($compere as $compere_nocall){
                $talk_num += $compere_nocall['videonum'] + $compere_nocall['voicenum'];
                $talk_nocall_num += $compere_nocall['typeonenum'] + $compere_nocall['typetwonum'] + $compere_nocall['typethreenum'] + $compere_nocall['typefournum'];
                $bnocall += $compere_nocall['typesixnum'];
                $refuse_call += $compere_nocall['typeightnum'];
                $callback += $compere_nocall['typeninenum'];
            }

        }

        //计算一个月3小时以上在线时间
        $daynum3 = 0;
        $daynum4 = 0;
        foreach($online_3_day as $key=>$value){
            if($value > 180){
                $daynum3 += 1;
            }
            if($value > 240){
                $daynum4 += 1;
            }
        }
        $dataAll = array(
            "time_f"=>$this->secondConversion($time_f),
            "audiotime"=>$this->secondConversion($audiotime),
            "videotime"=>$this->secondConversion($videotime),
            "day_gift"=>$day_gift,
            "video_gold"=>$video_gold,
            "audio_gold"=>$audio_gold,
            "talk_num"=>$talk_num,
            "talk_nocall_num"=>$talk_nocall_num,
            "bnocall"=>$bnocall,
            "callback"=>$callback,
            "refuse_call"=>$refuse_call,
            "daynum3"=>$daynum3,
            "daynum4"=>$daynum4,
        );
        return $dataAll;

    }


    public function secondConversion($second = 0)
    {
        $second = $second *60;
        $newtime = '';
        $d = floor($second / (3600*24));
        $h = floor(($second % (3600*24)) / 3600);
        $m = floor((($second % (3600*24)) % 3600) / 60);
        if ($d>'0') {
            if ($h == '0' && $m == '0') {
                $newtime= $d.'天';
            } else {
                $newtime= $d.'天'.$h.'小时'.$m.'分';
            }
        } else {
            if ($h!='0') {
                if ($m == '0') {
                    $newtime= $h.'小时';
                } else {
                    $newtime= $h.'小时'.$m.'分';
                }
            } else {
                $newtime= $m.'分';
            }
        }
        return $newtime;
    }


    public function getmydata(){
        $uid = $_GET['uid'];
        $this->mydata = $this->mydata($uid);
        $this->monthdata = $this->mydatamonth($uid);
        $this->display();
    }



    //查询是否有体验卡
    public function getiscard(){
        $data = $this->Api_recive_date;
        $uid =  $data['uid'];
        $type = $data['type'];//1视频 2语音 3礼物
        $iscard = M("vod_pack")->where(array("uid"=>$uid))->select();
        if(empty($iscard)){
            Push_data(array("data"=>2));
        }else{
            foreach($iscard as $item){
                if($type == 1){
                    if($item['videocard'] > 0 && $item['endtime'] >time()){
                        Push_data(array("data"=>1));die;
                    }
                }elseif ($type ==2){
                    if($item['voicecard'] > 0 && $item['endtime'] >time()){
                        Push_data(array("data"=>1));die;
                    }
                }elseif ($type ==3){
                    if($item['giftcard'] > 0 && $item['endtime'] >time()){
                        Push_data(array("data"=>1));die;
                    }
                }
            }

            Push_data(array("data"=>2));
        }


    }


    //消耗体验卡
    public function depletecard(){
        $data = $this->Api_recive_date;
        $uid =  $data['uid'];
        $type = $data['type'];//1视频 2语音 3礼物
        $iscard = M("vod_pack")->where(array("uid"=>$uid))->select();

        $tmp_arr = array();
            foreach($iscard as $item){
                if($type == 1){
                    if($item['videocard'] > 0 && $item['endtime'] >time()){
                        $tmp_arr[$item['endtime']] = $item;
                    }
                }elseif ($type ==2){
                    if($item['voicecard'] > 0 && $item['endtime'] >time()){
                        $tmp_arr[$item['endtime']] = $item;
                    }
                }elseif ($type ==3){
                    if($item['giftcard'] > 0 && $item['endtime'] >time()){
                        $tmp_arr[$item['endtime']] = $item;
                    }
                }
        }
            krsort($tmp_arr);

            //取出第一个
            $first = current($tmp_arr);
            if($type ==1 ){

                $data['videocard'] = $first['videocard'] - 1;
            }elseif ($type ==2){

                $data['voicecard'] = $first['voicecard'] - 1;
            }elseif ($type ==3){

                $data['giftcard'] = $first['giftcard'] - 1;
            }
        $where['endtime'] = $first['endtime'];
        $where['uid'] = $uid;
            M("vod_pack")->where($where)->save($data);

        Push_data(array("data"=>1));
    }





    //24112 推广用户回复率
    public function onlinecallback(){
        $uid = $_GET['uid'];
        file_get_contents("http://".$_SERVER['HTTP_HOST']."/Adms/User/reply.html?token=99123&uid=".$_GET['uid']);
        $info = M("reply")->where(array("uid"=>$_GET['uid']))->find();
        //计算一条回复率
        $call['call_1'] = ceil($info['news_1']/$info['newsall']*100)."%";
        $call['call_2'] = ceil($info['news_2']/$info['newsall']*100)."%";
        $call['call_3'] = ceil($info['news_3']/$info['newsall']*100)."%";
        //人数回复率
        $call['prople'] = ceil($info['reply_user']/$info['userall']*100)."%";
        $this->all_call = $call;
        $this->mydata = $this->mydata2($uid);
        $this->display();

    }




    public function userinfo(){
        $uid = $_GET['uid'];
        $info = $this->get_user($uid);

        $country = $this->get_region_citylist($info['country']);
        $crty = $info['country'].'--'.$country[$info['area']+1]['name'];

        //var_dump($info);die;exit;
        $this->crty = $crty;
        //获取身高
        $attr = new AttrValueModel();


        if($info['age']>300){
            $age = $attr->getOne(array('id'=>$info['age']));
            $info['age'] = $age['name']?$age['name']:0;
        }
        //var_dump($info);die;exit;
        $height = $attr->getOne(array('id'=>$info['height']));
        $weight = $attr->getOne(array('id'=>$info['weight']));
        $work = $attr->getOne(array('id'=>$info['work']));
        $income = $attr->getOne(array('id'=>$info['income']));
        $education = $attr->getOne(array('id'=>$info['education']));
        $marriage = $attr->getOne(array('id'=>$info['marriage']));
        //获取照片
        $photo = M("photo")->where(array("uid"=>$uid))->select();
        //查看是否在线
        $txkey=M('txmy')->where(array('product'=>$info['product']))->getField('miyao');
        $uline = tencent_onlinestate(array($uid),$txkey);
        $isonline = "不在线";
        if($uline[0]['State'] == 'Online'){
            $isonline = "在线中";
        }
        $this->height = $height['name']?$height['name']:'暂无';
        $this->weight = $weight['name']?$weight['name']:'暂无';
        $this->work = $work['name']?$work['name']:'暂无';
        $this->education = $education['name']?$education['name']:'暂无';
        $this->income = $income['name']?$income['name']:'暂无';
        $this->marriage = $marriage['name']?$marriage['name']:'暂无';
        $this->photos = $photo;
        $this->info = $info;
        $this->isonline = $isonline;
        $this->display();
    }

    //获取国内地市
    protected function get_region_citylist($id,$reset=0){
        $path = CHCHEPATH_AREA;
        $cache_name = 'regionlist_' . $id;
        if (F($cache_name, '', $path) && $reset == 0) {
            $res = F($cache_name, '', $path);
        }else{
            $code=M('region')->where(array('id'=>$id,'level'=>1,'country_codeiso'=>'CN'))->getField('code');
            $where = array("upper_region"=>$code,"level"=>2);
            $RegionM = new RegionModel();
            $res = $RegionM->getList($where);

            F($cache_name,$res,$path);
        }
        foreach ($res as $k2=>$v2){
            $result1[$k2]["id"]=$v2["id"];
            $result1[$k2]["name"] = $str = trim($this->L($v2["code"]));
        }
        return $result1;
    }




}

?>
