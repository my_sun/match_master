<?php
class HmsgController extends BaseController
{
    public function __construct ()
    {
        parent::__construct ();
        //echo date("Y-m-d",time());exit;
        $this->msgfileurl = '/userdata/msgfile/' . date ('Ymd', time ()) . "/";
        $this->cachepath = WR . '/userdata/cache/msg/';
    }

    /**
     * Hmsg登录
     *
     * @request string puid 用户id
     * @request string password 密码
     * @return data
     */
    public function login()
    {
        $data = $this->Api_recive_date;
        $uid = $data["puid"] ? $data["puid"] : 0;
        Push_data ();
    }
    
    public function all()
    {
        $redis = $this->redisconn();
        $msglist = $redis->listLrange("nomsglist",0, -1);
        shuffle($msglist);
        Dump($msglist);
    }
    /**
     * 获取消息列表
     *
     * @request string puid 用户id
     * @return data
     */
    public function getmsglist()
    {
        $redis = $this->redisconn();
        $msglist = $redis->listLrange("nomsglist",0, -1);
        foreach ($msglist as $k=>$v){
            if(!$redis->get("nomsglisttime_".$v)){
                $redis->listRemove("nomsglist",$v,0);
                unset($msglist[$k]);
            }
        }
        shuffle($msglist);
        $randmsg = $msglist[array_rand($msglist)];
        $redis->listRemove("nomsglist",$randmsg,0);
        
        $randmsgarr = explode("-", $randmsg);
        $return=array();
        $return['data']='';
        $return["code"] =ERRORCODE_203;
        if(count($randmsgarr)>1){
            $uid = $randmsgarr[0];
            $touid = $randmsgarr[1]; 
            
            
            $data = $this->Api_recive_date;
            $selfuid = $this->uid;
            
            $pageNum = $data['page'] ? $data['page'] : 1;
            $pageSize = $data['pagesize'] ? $data['pagesize'] : 15;
            $page = array();
            $page["pagesize"] = $pageSize;
            $page["pageNum"] = $pageNum;
            $Wget = array();
            $Wget['uid'] = $uid;
            $Wget['touid'] = $touid;
            $msg = $this->get_msglist ($Wget, $pageNum, $pageSize);
            $page["totalcount"]= $msg["totalcount"]?$msg["totalcount"]:0;
            $msg1 = array();
            $msg1['uid']=$this->get_diy_user_field($Wget['uid']);
            $msg1['touid']=$this->get_diy_user_field($Wget['touid']);
            
            
            $msg1['list'] = array();
            $temppre = array();
            foreach ($msg['val'] as $k => $v) {
                
                    $msg1['list'][] =$v;
                
            }
            $result = $msg1;
    
            $result['page'] = $page;
            if($pageNum>ceil($page["totalcount"]/$pageSize)){
                $return['data']=$result;
            }else{
                $return['data']= $result;
            }
            $return['message'] = $this->L ("CHENGGONG");
            $return["code"] =ERRORCODE_200;
        }
        Push_data ($return);

    }
    /**
     * 发信
     *
     * @request string puid 用户id
     * @request string msgtype 发信类型:text文本消息
     * @request string content 发信内容
     * @request string uid 发信人用户ID
     * @request string touid 收信人用户ID
     * @return data
     */
    public function sendmsg ()
    {
        $data = $this->Api_recive_date;
        $platforminfo = $this->platforminfo;
        $uid = $data['uid'];
        $times=$data['times'];
        $lastSendTime=$data['lastSendTime'];
        $time=time();
        $timecha=$time-$lastSendTime;
        $MyUserInfo = $this->get_user($uid);
       
        
        $redis = $this->redisconn ();
       
        $redisStrj = "Smsg_" . $uid;
        $resultj = true;
        if ($resultj) {
            $msgtype = $data['msgtype'] ? $data['msgtype'] : "text";//发信类型:text文本消息，voice语音消息，video视频消息，location位置信息，gift礼物消息,image图片
            $content = $data['content'] ? $data['content'] : "";//发信内容//文本时
            $touid = $data['touid'] ? $data['touid'] : "";//收信人用户ID
            $ToUserInfo = $this->get_user ($touid);//收信人用户信息
            $contentint = $data['contentint'] ? $data['contentint'] : "0";//礼物数量、语音、红包金币数量或视频时长
            $contentbody = $data['contentbody'] ? $data['contentbody'] : "";//礼物id、语音url或视频url，图片ur
            $contentsub = $data['contentsub'] ? $data['contentsub'] : "";//视频第一帧图片url
            $datapuid = $data['puid'] ? $data['puid'] : "0";//puid
            $mybox_id = $uid . $touid;
            $tobox_id = $touid . $uid;
            
            //移除聊天列表
            $uidstrs=$touid."-".$uid;
            $redis->listRemove("nomsglist",$uidstrs,0);
            
            $MyBoxinfo = array();
            $ToBoxinfo = array();
            $Msginfo = array();
            switch ($msgtype) {
                //voice语音消息
                case "voice":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content"=>$content,
                        "sendTime"=>time(),
                        "voice"=>array("url"=>$contentbody,"ltime"=>$contentint)
                    );
                    break;
                    //video视频消息
                case "video":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $Msginfo['contentsub'] = $contentsub;
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content"=>$content,
                        "sendTime"=>time(),
                        "video"=>array("url"=>$contentbody,"ltime"=>$contentint,"imageurl"=>$contentsub)
                    );
                    break;
                    //location位置信息
                case "location":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content"=>$content,
                        "sendTime"=>time(),
                        "location"=>array("longitude"=>$contentint,"latitude"=>$contentbody)
                    );
                    break;
                
                    //,image图片
                case "image":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $imageobj = array(
                        "id"=>$contentint,
                        "url"=>$contentbody,
                        "status"=>1,
                        "seetype"=>1
                    );
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content"=>$content,
                        "sendTime"=>time(),
                        "image"=>$imageobj
                    );
                    break;
                
                default:
                    $messageBody = array(
                        "msgtype" => $msgtype,
                    "content"=>$content,
                    "sendTime"=>time()
                    );
                    
                    break;
            }
            
            if($datapuid!="0"){
                $datapuserinfo = $this->get_user($datapuid);
                if($datapuserinfo["user_type"]==3&&$ToUserInfo["user_type"]==1){
                    
                    $MoneyC = new MoneyController();
                    //增加充值会员增加魅力值标记
                   // $MoneyC->set_moneykey($datapuid, $touid);
                    //部分3用户聊天按字符增加魅力值
                    if($times<4){
                        $MoneyC->add_charmoney($datapuserinfo, $ToUserInfo,$content,$redis,$timecha);
                    }

                }
                
            }
            
            //客户端消息对象封装
            $AutomsgCon = new AutomsgController();
            $clientMsgObj = array();
            $clientMsgObj["touid"]=$touid;
            $clientMsgObj["msgid"]=substr(md5($AutomsgCon->getMillisecond()), 8, 16);//substr(md5(time()), 8, 16)
            $body = array();
            $body["id"]=$clientMsgObj["msgid"];
            
            $body["msgType"]=$msgtype;
            $body["user"]=$this->get_diy_user_field($uid,"uid|gender|age|nickname|vipgrade|vip|gold|head");
            
            $body["messageBody"]=$messageBody;
            $clientMsgObj["msgbody"]=$body;
           // Dump($clientMsgObj);
            //Dump(C("IMSDKID"));
            $txkey=M('txmy')->where(array('product'=>$ToUserInfo["product"]))->getField('miyao');
            $ret = tencent_sendmsg($clientMsgObj,$txkey);
            //Dump($ret);
           // $ret = tencent_sendmsg($clientMsgObj,C("IMSDKID")["23110"]);
            //Dump($ret);
            //LM_sendmsg($clientMsgObj);
            
            
            $MyBoxinfo['type'] = $msgtype;
            //if($msgtype==1) {
            
            $MyBoxinfo['id'] = $mybox_id;
            $MyBoxinfo['content'] = $content;
            $MyBoxinfo['uid'] = $uid;
            $MyBoxinfo['touid'] = $touid;
            $MyBoxinfo['country'] = $platforminfo['country '] ? $platforminfo['country '] : 'TW';
            $MyBoxinfo['language'] = $platforminfo['language '] ? $platforminfo['language '] : 'zh-tw';
            $MyBoxinfo['sendtime'] = time ();
            //$where = array('uid'=>$uid);
            
            //信箱所需字段信息
            $Msginfo['type'] = $msgtype;
            $Msginfo['sendtime'] = $MyBoxinfo['sendtime'];
            $Msginfo['language'] = $MyBoxinfo['language'];
            $Msginfo['country'] = $MyBoxinfo['country'];
            $Msginfo['uid'] = $uid;
            $Msginfo['touid'] = $touid;
            $Msginfo['content'] = $content;
            $Msginfo['box_id'] = $mybox_id;
            $MsgM = new MsgModel();
            $MsgBoxm = new MsgBoxModel();
            //将聊天列表记录在redis中
            $this->setchatbox($uid,$touid);
            //将聊天记录储存在redis中
            $reidsvalue=$this->setChatRecord($uid, $touid, $messageBody);
            $Msginfo['reidsvalue']=$reidsvalue;
            $result = $MsgM->addOne ($Msginfo);
            //消息列表储存过后将消息储存在信箱列表中，运用redis查询信息列表中是否已有来往信息
            if ($result) {
                //统计用户发送次数start
                $val = $redis->get ($redisStrj);
                $redis->set ($redisStrj, $val + 1, 0, 0, 60 * 60 * 24 * 3);
                //统计用户发送次数end
                
                $redisStr = "Gmsgbox_" . $mybox_id;
                
                $cachetime = 60 * 60 * 24 * 3;
                $ab['id']=$mybox_id;
                if ($redis->exists ($redisStr)) {
                    $MsgBoxm->updateOne1 ($ab, $MyBoxinfo);
                } else {
                    $value = $MsgBoxm->getOne1 ($ab);
                    if ($value) {
                        $MsgBoxm->updateOne1 ($ab, $MyBoxinfo);
                        $redis->set ($redisStr, $mybox_id, 0, 0, $cachetime);
                    } else {
                        $MsgBoxm->addOne1 ($MyBoxinfo);
                        $redis->set ($redisStr, $mybox_id, 0, 0, $cachetime);
                    }
                }
                
            }
            $return = array();
            $return['message'] = $this->L ("CHENGGONG");
            Push_data ($return);
        }else {
            Push_data (array('message' => $this->L ("请充值！"), 'code' => ERRORCODE_201));
        }
    }
    //获取消息缓存列表
    protected function get_msglist ($Wget, $pageNum, $pageSize, $reset = 0)
    {
        //$path = $this->cachepath . "msglist/";
      //  $cache_name = 'msglist' . md5 (json_encode ($Wget) . $pageNum . $pageSize);
      //  if ($reset == 1) {
      //      return deldir ($path);
      //  }
     //   if (F ($cache_name, '', $path)) {
     //       $msg = F ($cache_name, '', $path);
     //   } else {
        $msgM = new MsgModel();
        $msg = $msgM->getListPage1 ($Wget, $pageNum, $pageSize);
      //      F ($cache_name, $msg, $path);
      //  }
        return $msg;
    }

    /*
   *发送消息时保存聊天记录
   * 这里用的redis存储是list数据类型
   * 两个人的聊天用一个list保存
   *
   * @from 消息发送者id
   * @to 消息接受者id
   * @meassage 消息内容
   *
   * 返回值，当前聊天的总聊天记录数
   */
    public function setChatRecord($from, $to, $message)
    {
        $redis = $this->redisconn();
        $data = array('from' => $from, 'to' => $to, 'message' => $message, 'sent' => time());
        $value = json_encode($data);
        //生成json字符串
        $keyName = 'rec:' . $this->getRecKeyName($from, $to);
        //echo $keyName;
        $lang = $redis->listSize($keyName);
        if ($lang >= 1000) {
            $redis->listPop($keyName, 1);
        }
        $res = $redis->listPush($keyName, $value);
        $redis->setListKeyExpire($keyName,60*60*24*7);
        return $value;

    }
    //将聊天列表记录在redis中
    public function setchatbox($uid,$touid){
        $redis = $this->redisconn();
        //生成json字符串
        $keyNamea = 'recbox:' . $uid;
        $keyNameb = 'recbox:' . $touid;
        //echo $keyName;
        $langa = $redis->listSize($keyNamea);
        if ($langa >= 400) {
            $redis->listPop($keyNamea, 1);
        }
        $res = $redis->listPush($keyNamea, $touid);
        $redis->setListKeyExpire($keyNamea,60*60*24*7);

        $langb = $redis->listSize($keyNameb);
        if ($langb >= 400) {
            $redis->listPop($keyNameb, 1);
        }
        $resb = $redis->listPush($keyNameb, $uid);
        $redis->setListKeyExpire($keyNameb,60*60*24*7);
        return $res;
    }

    /*生成聊天记录的键名，即按大小规则将两个数字排序
    * @from 消息发送者id
    * @to 消息接受者id
    *
    *
    */
    private function getRecKeyName($from, $to)
    {
        return ($from > $to) ? $to . '_' . $from : $from . '_' . $to;
    }


    public function pushmsg(){
        if($_GET['uid']){
            $msg['uid'] = $_GET['uid'];
            $msg['touid'] = $_GET['touid'];
            $msg['box_id'] = $msg['uid'].$msg['touid'];
            $msg['sendtime'] = time();
            $msg['type'] = 'text';
            $msg['content'] = $_GET['content'];

            $json = array(
                "from"=>$_GET['uid'],
                "to"=>$_GET['touid'],
                "message"=>array(
                    "msgtype"=>"text",
                    "content"=>$_GET['content'],
                    "sendTime"=>time()
                ),
                "sent"=>time()
            );
            $msg['reidsvalue'] = json_encode($json);
            $MsgM = new MsgModel();
            $MsgM->addOne($msg);
        }

    }
    

}

?>