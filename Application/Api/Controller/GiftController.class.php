<?php
class GiftController extends BaseController {
	public function __construct(){
		parent::__construct();	
	}
	public function index(){
		$str1="2.5.3";
		$str2="2.5";
		$str2=str_replace(".","",$str2); 
		if($str2<253){
			echo $str2;
		}
	}
	
	public function Lists(){
		$data = $this->Api_recive_date;
		$pageNum=1;
		$pageSize=100;
		$WhereArr = array();
		$WhereArr["status"]=1;
		$product=$data["product"] ? $data["product"] : "10008";
		$cachename = "GiftList";
		$cache=S($cachename); 
		if($_GET["reset"]==1){
			$cache=0;
		}
		if(!$cache){  
			$GiftM = new GiftModel();
			//$ret = $GiftM->getListPage($WhereArr,$pageNum,$pageSize);
            $res['status']=1;
            $ret = $GiftM->getList($res);
			$cache = $ret;
			S($cachename,$cache,60*60*2); //设置缓存的生存时间 
		}
		$ret=$cache;

		$Lists = array();
		foreach($retas as $k=>$v){
			//if($v["product"]==$product){
			//$Lists[]=$this->formatGiftInfo($v);
			//}
            $Lists[$k]['id']=$v['id'];
            $Lists[$k]['price']=$v['price'];
            $Lists[$k]['title']=$v['title'];
            $Lists[$k]['title']=$v['title'];


		}
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray['isSucceed'] = 1;
		$RetArray['msg'] = "ok";
		$RetArray['Lists'] = $Lists;
		Push_data($RetArray);
	}
	
		/*
	 *商品列表
	*/
	public function formatGiftInfo($date){
		$newdate = array();
		$newdate["id"] = $date["id"];
		$newdate["title"] = $date["title"];
		$newdate["img"] = "http://newtaiwan.oss-cn-hongkong.aliyuncs.com/data".$date["img"];
		$newdate["price"] = $date["price"];
		return $newdate;
	}
	/*
	 *收礼物排行榜
	*/
	public function CollectionRank(){
		$cachename = "CollectionRank";
		$cache=S($cachename); 
		//$cache=0;
		if(!$cache){  
			$Model = M();
			$sql="SELECT distinct uid,points,sum(typeid) as he FROM t_points where points=0 group by uid ORDER BY he desc limit 50;";
			$result = $Model->query($sql);
			
			$i=0;
			foreach($result as $k=>$v){
				$userinfo = $this->get_user($v["uid"]);
				if($userinfo["product"]=="10008"){
					if($i>=15){
						break;
					}
					$temp = array();
					$temp["uid"] =$v["uid"];
					$temp["userinfo"] =$userinfo;
					$temp["points"] =$v["he"]*10;
					$Lists[]=$temp;
					$i++;
				}
			}
			$cache = $Lists;
			S($cachename,$cache,60*10); //设置缓存的生存时间 
		}
		$Lists=$cache;
		

		
		$RetArray = array();
		$RetArray['isSucceed'] = 1;
		$RetArray['msg'] = "ok";
		$RetArray['Lists'] = $Lists;
		Push_data($RetArray);
	}
		/*
	 *发送礼物排行榜
	*/
	public function SendRank(){
		$cachename = "SendRankList";
		$cache=S($cachename); 
		//$cache=0;
		if(!$cache){  
			$Model = M();
			$sql="SELECT distinct to_uid,points,sum(typeid) as he FROM t_points where points=0 group by to_uid ORDER BY he desc limit 50;";
			$result = $Model->query($sql);
			
			foreach($result as $k=>$v){
				$userinfo = $this->get_user($v["to_uid"]);
				if($userinfo["product"]=="10008"){
					$temp = array();
					$temp["uid"] =$v["to_uid"];
					$temp["userinfo"] =$userinfo;
					$temp["points"] =$v["he"];
					$Lists[]=$temp;
				}
			}
			$cache = $Lists;
			S($cachename,$cache,60*10); //设置缓存的生存时间 
		}
		$Lists=$cache;
		

		
		$RetArray = array();
		$RetArray['isSucceed'] = 1;
		$RetArray['msg'] = "ok";
		$RetArray['Lists'] = $Lists;
		Push_data($RetArray);
	}
    /**
     * 送出的礼物列表
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @return data
     */
	public  function sendgiftlist(){

	    $id=$this->uid;
        $platforminfo=$this->platforminfo;
        $product=$platforminfo['product'];
	    $res=$this->sendlist($id,$product);


        $return = array();
        $return['message'] = $this->L ("CHENGGONG");
        $return['data'] = $res;
        Push_data($return);
    }

    /**
     * 收到的礼物列表
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @return data
     */
    public  function getgiftlist(){

        $uid=$this->uid;
        $platforminfo=$this->platforminfo;
        $product=$platforminfo['product'];
        $res=$this->getlist($uid,$product);
        $return = array();
        $return['message'] = $this->L ("CHENGGONG");
        $return['data'] = array_reverse($res);
        Push_data($return);
    }


    //根据uid获取发送礼物总数量列表
    public  function pubsendlist($uid,$product){
        $res=array();
        $res['uid']=$uid;
        $cachename = "SendGiftList".$uid;
        $cache=S($cachename);
        if(!$cache){
            $GiftListM = new GiftListModel();
            $PubC=new PublicController();
            $ret = $GiftListM->getbygroup($res);
            foreach($ret as $k=>$v){

                $giftinfo[$k]['giftcount']= $GiftListM->getListbyone(array('giftid'=>$v,'uid'=>$uid),'giftcount');
                $giftextend=$PubC->giftextend($v,$product,$this->platforminfo["language"]);
                $giftinfo[$k]['url']=C("IMAGEURL").$giftextend['url'];
                $giftinfo[$k]['price']=$giftextend['price'];
                $giftinfo[$k]['title']=$giftextend['title'];
                $giftinfo[$k]['id']=$v;
                $giftinfo[$k]['count']=0;
                //统计礼物总数量
                foreach($giftinfo[$k]['giftcount'] as $kc=>$vc){
                    $giftinfo[$k]['count']+=$giftinfo[$k]['giftcount'][$kc];
                }
                $result[$k]['url']= $giftinfo[$k]['url'];
                $result[$k]['price']=$giftinfo[$k]['price'];
                $result[$k]['title']=$giftinfo[$k]['title'];
                $result[$k]['id']=$giftinfo[$k]['id'];
                $result[$k]['count']=$giftinfo[$k]['count'];
            }
            $cache = $result;
            S($cachename,$cache,60*60*24*3); //设置缓存的生存时间
        }
        $result=$cache;

        return $result;
    }

        //根据uid获取收到送礼物总数量列表
    public  function pubgetlist($uid,$product){
        $res=array();
        $res['touid']=$uid;
        $cachename = "GetGiftList"."to".$uid;
        $cache=S($cachename);
        if(!$cache){
            $GiftListM = new GiftListModel();
            $PubC=new PublicController();
            $ret = $GiftListM->getbygroup($res);
            foreach($ret as $k=>$v){
                $giftinfo[$k]['giftcount']= $GiftListM->getListbyone(array('giftid'=>$v,'touid'=>$uid),'giftcount');
                $giftextend=$PubC->giftextend($v,$product,$this->platforminfo["language"]);
                $giftinfo[$k]['url']=C("IMAGEURL").$giftextend['url'];
                $giftinfo[$k]['price']=$giftextend['price'];
                $giftinfo[$k]['title']=$giftextend['title'];
                $giftinfo[$k]['id']=$v;
                $giftinfo[$k]['count']=0;
                //统计礼物总数量
                foreach($giftinfo[$k]['giftcount'] as $kc=>$vc){
                    $giftinfo[$k]['count']+=$giftinfo[$k]['giftcount'][$kc];
                }
                $result[$k]['url']= $giftinfo[$k]['url'];
                $result[$k]['price']=$giftinfo[$k]['price'];
                $result[$k]['title']=$giftinfo[$k]['title'];
                $result[$k]['id']=$giftinfo[$k]['id'];
                $result[$k]['count']=$giftinfo[$k]['count'];
            }
            $cache = $result;
            S($cachename,$cache,60*60*24*3); //设置缓存的生存时间
        }
        $result=$cache;
        return $result;
    }


    ////根据uid获取发送具体礼物列表
    public  function sendlist($uid,$product){
        $res=array();
        $res['uid']=$uid;//$uid;
        $cachename = "SendGiftL".$uid;
        $cache=S($cachename);
       if(!$cache){
            $GiftListM = new GiftListModel();
            $PubC=new PublicController();
            $ret = $GiftListM->getList($res);
            foreach($ret as $k=>$v){
                $giftextend=$PubC->giftextend($ret[$k]['giftid'],$product,$this->platforminfo["language"]);
                $giftinfo[$k]['url']=C("IMAGEURL").$giftextend['url'];
                $giftinfo[$k]['price']=$giftextend['price'];
                $giftinfo[$k]['title']=$giftextend['title'];
                $giftinfo[$k]['id']=$ret[$k]['giftid'];
                $giftinfo[$k]['getuid']=$ret[$k]['touid'];
                $giftinfo[$k]['count']=$ret[$k]['giftcount'];
                $giftinfo[$k]['sendtime']=$ret[$k]['time'];
                $userform=$this->get_diy_user_field($ret[$k]['touid']);
                $giftinfo[$k]['getnickname']=$userform['nickname'];
            }
            $cache = $giftinfo;
            S($cachename,$cache,60*60*24*3); //设置缓存的生存时间
        }
        $result=$cache;

        return $result;
    }

    ////根据uid获取收到具体礼物列表
    public  function getlist($uid,$product){
        $res=array();
        $res['touid']=$uid;
        $cachename = "GetGiftL"."to".$uid;
        $cache=S($cachename);
        if(!$cache){
            $GiftListM = new GiftListModel();
            $PubC=new PublicController();
            $ret = $GiftListM->getList($res);
            foreach($ret as $k=>$v){
                $giftextend=$PubC->giftextend($ret[$k]['giftid'],$product,$this->platforminfo["language"]);
                $giftinfo[$k]['url']=C("IMAGEURL").$giftextend['url'];
                $giftinfo[$k]['price']=$giftextend['price'];
                $giftinfo[$k]['title']=$giftextend['title'];
                $giftinfo[$k]['id']=$ret[$k]['giftid'];
                $giftinfo[$k]['senduid']=$ret[$k]['uid'];
                $giftinfo[$k]['count']=$ret[$k]['giftcount'];
                $giftinfo[$k]['gettime']=$ret[$k]['time'];
                $userform=$this->get_diy_user_field($ret[$k]['uid']);
                $giftinfo[$k]['sendnickname']=$userform['nickname'];
            }
            $cache = $giftinfo;
            S($cachename,$cache,60*60*24*3); //设置缓存的生存时间
        }
        $result=$cache;
       return $result;
    }
    //设置礼物是否可以被查看
    public function canbesee(){
        $data = $this->Api_recive_date;
        $type=$data['type'];
        $status=$data['status'];
        $uid=$this->uid;
        $userinfo=$this->UserInfo;
        //发送礼物
        if($type==1){
                $userextendM=new UserExtendModel();
                $userextendM->updateOne(array('uid'=>$uid),array('giftsendcanbysee'=>$status));
                    $this->set_user_field($uid,'giftsendcanbysee',$status);
        }else{//收到礼物状态更新
                $userextendM=new UserExtendModel();
                $userextendM->updateOne(array('uid'=>$uid),array('giftgetcanbysee'=>$status));
                    //更新缓存
                $this->set_user_field($uid,'giftgetcanbysee',$status);
        }
        $return['message'] = $this->L ("CHENGGONG");
        Push_data($return);
    }
}

?>