<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/27
 * Time: 11:01
 */

class PictureController extends BaseController
{
    public function __construct ()
    {
        parent::__construct ();

    }
    public function showpicture()
    {
        $recdate = $this->Api_recive_date;
        $type = $this->Api_recive_date['type'];
        $type = $type ? $type : 1;
        $baseWhere = array();
        $baseWhere['country'] = 'TW';
        $baseWhere['gender'] = 2;//女用户
        $baseWhere['user_type'] = 2;//类型为2

            $pageNum = $recdate['page'] ? $recdate['page'] : 1;
            $pageSize = $recdate['pagesize'] ? $recdate['pagesize'] : 30;
            $userfield = $recdate['userfield'] ? $recdate['userfield'] : "uid|head|nickname|count";
            $User_baseM = new UserBaseModel();
            $ret = $User_baseM->getListPage($baseWhere, $pageNum, $pageSize);


        $newlistuser = array();
        if ($type == 1) {
            foreach ($ret['list'] as $k => $v) {
                $tempUserInfo = $this->get_user($v['uid']);
                if ($tempUserInfo) {
                    //设置图片点赞数start
                    $cachename = "get_pictureStar_" . $v['uid'];
                    $cache = S($cachename);//设置缓存标示
                    if (!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                        $rand_keys = rand(200, 5000);
                        $tempUserInfo["count"] = $rand_keys;
                        $cache = $rand_keys;
                        S($cachename, $cache, 60 * 60 * 2);//设置缓存的生存时间
                    } else {
                        $tempUserInfo["count"] = $cache;
                    }
                    //设置图片点赞数end
                    $tempArray = $this->get_user_field($tempUserInfo, $userfield);
                    $tempArray1['id'] = $tempArray['uid'];
                    $tempArray1['nickname'] = $tempArray['nickname'];
                    $tempArray1['url'] = $tempArray['head']['url'];
                    $tempArray1['count'] = $tempArray['count'];
                    $newlistuser[] = $tempArray1;
                }
            }
        }
        if($type==2){
            foreach ($ret['list'] as $k => $v) {
                $tempUserInfo = $this->get_user($v['uid']);
                if ($tempUserInfo) {
                    //设置图片点赞数start
                    $cachename = "get_pictureStar_hot_" . $v['uid'];
                    $cache = S($cachename);//设置缓存标示
                    if (!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                        $rand_keys = rand(20000, 30000);
                        $tempUserInfo["count"] = $rand_keys;
                        $cache = $rand_keys;
                        S($cachename, $cache, 60 * 60 * 2);//设置缓存的生存时间
                    } else {
                        $tempUserInfo["count"] = $cache;
                    }
                    //设置图片点赞数end
                    $tempArray = $this->get_user_field($tempUserInfo, $userfield);
                    $tempArray1['id'] = $tempArray['uid'];
                    $tempArray1['nickname'] = $tempArray['nickname'];
                    $tempArray1['url'] = $tempArray['head']['url'];
                    $tempArray1['count'] = $tempArray['count'];
                    $newlistuser[] = $tempArray1;
                }
            }
        }
            ksort($newlistuser);
            shuffle($newlistuser);
            $return = array();
            if ($pageNum > ceil($ret["totalCount"] / $pageSize)) {
                $return['data']["list"] = array();
            } else {
                $return['data']["list"] = $newlistuser;
            }
            $page["pagesize"] = $pageSize;
            $page["page"] = $pageNum;
            $page["totalcount"] = $ret["totalCount"];
            $return['data']["page"] = $page;
            Push_data($return);
    }
    public function collection(){
        import("Extend.Library.ORG.Util.Page");
        $recdate = $this->Api_recive_date;
        $ids=explode("|", $recdate['ids']);
        $pageNum = $recdate['page'] ? $recdate['page'] : 1;
        $pageSize = $recdate['pagesize'] ? $recdate['pagesize'] :10;
        $userfield = $recdate['userfield'] ? $recdate['userfield'] : "uid|head|nickname|count";
        $count=count($ids);
        $Page=new Page($count,$pageSize,$pageNum);
        $map['uid']  = array('in',$ids);
        $user=M('user_base');
        $data =$user->where($map)->order("RAND()")->limit($Page->firstRow. ',' . $Page->listRows)->select();
        $ret = array();
        $ret['totalCount'] = $count;
        $ret['pageSize'] = $pageSize;
        $ret['pageNum'] = $pageNum;
        $ret['list'] = $data;

        foreach ($ret['list'] as $k => $v) {
            $tempUserInfo = $this->get_user($v['uid']);
            if ($tempUserInfo) {
                //设置图片点赞数start
                $cachename = "get_pictureStar_" . $v['uid'];
                $cache = S($cachename);//设置缓存标示
                if (!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                    $rand_keys = rand(5000, 20000);
                    $tempUserInfo["count"] = $rand_keys;
                    $cache = $rand_keys;
                    S($cachename, $cache, 60 * 60 * 2);//设置缓存的生存时间
                } else {
                    $tempUserInfo["count"] = $cache;
                }
                //设置图片点赞数end
                $tempArray = $this->get_user_field($tempUserInfo, $userfield);
                $tempArray1['id'] = $tempArray['uid'];
                $tempArray1['nickname'] = $tempArray['nickname'];
                $tempArray1['url'] = $tempArray['head']['url'];
                $tempArray1['count'] = $tempArray['count'];
                $newlistuser[] = $tempArray1;
            }
        }
        ksort($newlistuser);
        shuffle($newlistuser);
        $return = array();
        if ($pageNum > ceil($ret["totalCount"] / $pageSize)) {
            $return['data']["list"] = array();
        } else {
            $return['data']["list"] = $newlistuser;
        }
        $page["pagesize"] = $pageSize;
        $page["page"] = $pageNum;
        $page["totalcount"] = $ret["totalCount"];
        $return['data']["page"] = $page;
        Push_data($return);
    }
    public function picturebox(){
        import("Extend.Library.ORG.Util.Page");
        $recdate = $this->Api_recive_date;
        $uid=$recdate['id'];
        $pageNum = $recdate['page'] ? $recdate['page'] : 1;
        $pageSize = $recdate['pagesize'] ? $recdate['pagesize'] :10;
        $userfield = $recdate['userfield'] ? $recdate['userfield'] : "uid|head|nickname|count";
        $photoM=M('photo');
        $count=$photoM->where(array('uid'=>$uid))->count();
        $Page=new Page($count,$pageSize,$pageNum);
        $data =$photoM->where(array('uid'=>$uid))->order("RAND()")->limit($Page->firstRow. ',' . $Page->listRows)->select();
        $ret = array();
        $ret['totalCount'] = $count;
        $ret['pageSize'] = $pageSize;
        $ret['pageNum'] = $pageNum;
        $ret['list'] = $data;
        foreach ($ret['list'] as $k => $v) {
            $tempArray1=array();
            $tempUserInfo= $this->get_user($v['uid']);
            if ($tempUserInfo) {
                //设置图片点赞数start
                $cachename = "get_pictureStar_" . $v['uid'].'_'.$v['id'];
                $cache = S($cachename);//设置缓存标示
                if (!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                    $rand_keys = rand(5000, 20000);
                    $tempArray1["count"] = $rand_keys;
                    $cache = $rand_keys;
                    S($cachename, $cache, 60 * 60 * 2);//设置缓存的生存时间
                } else {
                    $tempArray1["count"] = $cache;
                }
                //设置图片点赞数end
                $tempArray = $this->get_user_field($tempUserInfo, $userfield);
                $tempArray1['id'] = $tempArray['uid'];
                $tempArray1['nickname'] = $tempArray['nickname'];
                $tempArray1['url'] = C("IMAGEURL").$v['url'];
                $newlistuser[] = $tempArray1;
            }
        }
        ksort($newlistuser);
        shuffle($newlistuser);
        $return = array();
        if ($pageNum > ceil($ret["totalCount"] / $pageSize)) {
            $return['data']["list"] = array();
        } else {
            $return['data']["list"] = $newlistuser;
        }
        $page["pagesize"] = $pageSize;
        $page["page"] = $pageNum;
        $page["totalcount"] = $ret["totalCount"];
        $return['data']["page"] = $page;
        Push_data($return);
    }

}