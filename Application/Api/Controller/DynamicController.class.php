<?php
/*
 * 用户动态
 */
class DynamicController extends BaseController {
	public function __construct(){
		parent::__construct();
		//$this->is_login();//检查用户是否登陆
		//echo date("Y-m-d",time());exit;
		$this->msgfileurl = '/userdata/dynamicfile/'.date('Ymd',time())."/";
		$this->cachepath = WR.'/userdata/cache/dynamic/';
		
	}
	public function index(){
		echo 0;exit;
	}	
	 /**
     * 发布动态
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int type 发布类型:1: 文字2:图片3：小视频
     * @request file file 如果类型为图片最多可以上传9张，视频只能上传一个，文件必须经客户端压缩处理后再上传，文件大小不能超过5M
     * @request string content 动态文字内容
     * @request int ltime 上传小视频需要传这个参数，视频时长
     * @request file imageurl 上传小视频需要传这个参数，视频第一帧图片文件
     * @return data
     */
	 public function add(){
         $data = $this->Api_recive_date;
         $return=self::add_dynamic($data);
         Push_data($return);
     }

	public  function add_dynamic($data){
		$uid = $this->uid;
		$userinfo=$this->UserInfo;
        $checkimgC=new CheckimgController();
        $checktextC= new ChecktextController();
		$type = $data["type"] ? $data["type"] : 1;
		$content = $data["content"] ? $data["content"] : "";
		$ltime = $data["ltime"] ? $data["ltime"] : 0;
		$filetype = "image";
		$gender=$userinfo['gender'];
		if($type==3){
			$filetype = "video";
		}
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("SHANGCHUANSHIBAI");
		$dynamicM = new DynamicModel();
		$dynamic = array();
		if($type==1){
			if($content){
			    //阿里云安全服务检测文本是否合格
                $contentz=urlencode($content);
                $textdata=$checktextC->checktext($content,array("antispam"));
                if($textdata['antispam']=='pass'){
                    $addArr = array();
                    $addArr["content"] =$contentz;
                    $addArr["uid"] = $uid;
                    $addArr["time"] = time();
                    $addArr["type"] = $type;
                    $addArr["status"] = 1;
                    $addArr["gender"] = $gender;
                    $dynamicid = $dynamicM->addOne($addArr);
                    $return = array();
                    $dynamic = array(
                        "id"=>$dynamicid,
                        "content"=>$content,
                        "type"=>$type,
                        "time"=>$addArr["time"],
                        "obj"=>"",
                        "comment"=>array()
                    );
                    $this->get_dynamiclist(0,0,0,1);//清空动态缓存
                }else{
                    $return = array();
                    $return['code'] = ERRORCODE_201;
                    $return['message'] = $this->L("動態內容不合格，請重新編輯上傳！");
                    Push_data($return);
                }

			}else{
				$return = array();
				$return['code'] = ERRORCODE_201;
				$return['message'] = $this->L("PINGLUNNEIRONGBUNENGWEIKONG");
                Push_data($return);
			}
		}else{

			if(!empty($_FILES)||!empty($data['files'])){
				
				$addArr = array();
				$addArr["content"] = urlencode($content);
				if($addArr["content"]!='') {
                    //阿里云安全服务检测文本是否合格
                    $textdata = $checktextC->checktext($content, array("antispam"));
                    if ($textdata['antispam'] != 'pass') {
                        $return = array();
                        $return['code'] = ERRORCODE_201;
                        $return['message'] = $this->L("動態內容不合格，請重新編輯上傳！");
                        Push_data($return);
                    }
                }
				$addArr["uid"] = $uid;
				$addArr["time"] = time();
				$addArr["type"] = $type;
                $addArr["gender"] = $gender;
                $addArr["status"] = 1;
					$saveUrl = $this->msgfileurl.$filetype."/";
					$savePath = WR.$saveUrl;
					//echo $savePath;exit;
                if(!empty($data['files'])){
                    $uploadList=$data['files'];
                }else{
                    $uploadList = $this->_upload($savePath);
                }
					//Dump($uploadList);exit;
					if(!empty($uploadList)){
						$obj = array();
						$PutOssarr = array();
						$returnurlarr = array();
						$ids = 0;
						if($type==3){
                            $dynamicid = $dynamicM->addOne($addArr);
							$imageurlfile = array();
							$videofile = array();
							foreach($uploadList as $k=>$v){
								if($v["key"]=="imageurl"){
									$imageurlfile = $v;
								}else{
									$videofile = $v;
								}
							}
							$VideoDynamicM = new VideoDynamicModel();
							$Indata = array();
							$Indata['danamicid'] = $dynamicid;
							$Indata['uid'] = $uid;
							$Indata['type'] = $type;
							$Indata['ltime'] = $ltime;
							$Indata['imageurl'] = $saveUrl.$imageurlfile['savename'];//
							$Indata['url'] = $saveUrl.$videofile['savename'];
							$ret = $VideoDynamicM->addOne($Indata);
							$ids = $ret;
							$PutOssarr[] = $Indata['url'];
							$PutOssarr[] = $Indata['imageurl'];
							$obj = array(
								"id"=>$ret,
								"ltime"=>$ltime,
								"url"=>C("IMAGEURL").$Indata['url'],
								"imageurl" =>C("IMAGEURL").$Indata['imageurl'],
								"status"=>1
							);
						}else{
						    //图片鉴黄


                            $dynamicid = $dynamicM->addOne($addArr);
							$ids = array();
							$PhotoDynamicM = new PhotoDynamicModel();
							foreach($uploadList as $k=>$v){
								$Indata = array();
								$Indata['uid'] = $uid;
								$Indata['type'] = $type;//1头像，2相册，3消息图片
                                $Indata['danamicid'] = $dynamicid;
                                if(!empty($data['files'])){
                                    $savePath= $uploadList[$k]['savepath'];
                                    $path=substr($savePath,strpos($savePath, '/userdata'));
                                    $Indata['url'] = $path.$v['savename'];
                                }else{
                                    $Indata['url'] = $saveUrl.$v['savename'];
                                }
                                //鉴定该图片是否为不合格图片
                                //引用ali鉴黄服务
                                $imgdata=$checkimgC->checkerjinzhi(WR.$Indata['url'],array("porn","terrorism"));

                                if($imgdata['porn']=='pass'&$imgdata['terrorism']=='pass' ){
                                    $ret = $PhotoDynamicM->addOne($Indata);
                                    $ids[] = $ret;
                                    $PutOssarr[] = $Indata['url'];
                                    $obj[] = array(
                                        "id"=>$ret,
                                        "url"=>C("IMAGEURL").$Indata['url'],
                                        "thumbnaillarge"=>C("IMAGEURL").$Indata['url'],
                                        "thumbnailsmall"=>C("IMAGEURL").$Indata['url'],
                                        "status"=>1,
                                        "seetype"=>1
                                    );
                                }else{
                                    $this->sendsysmsg($uid, "該动态其中照片未通過審核");
                                    continue;
                                }


							}
							$ids = implode("|", $ids);
						}
						if($ids!=''){
                            $dynamicM->updateOne(array("id"=>$dynamicid), array("ids"=>$ids));
                        }
                        if(!empty($PutOssarr)){
                            PutOss($PutOssarr);
                        }

						//PutOss($PutOssarr);
						//Dump($PutOssarr);exit;
						$return = array();
						$dynamic = array(
							"id"=>$dynamicid,
							"content"=>$content,
							"type"=>$type,
							"time"=>$addArr["time"],
							"obj"=>$obj,
							"comment"=>array()
						);
					}
					$this->get_dynamiclist(0,0,0,1);//清空动态缓存
			}else{
				$return = array();
				$return['code'] = ERRORCODE_201;
				$return['message'] = $this->L("SHANGCHUANDEWENJIANBUNENGWEIKONG");			
			}
		}
		$return['data']["dynamic"]=$dynamic;
		//Dump($return);
        return $return;

	}
	
	// 文件上传
	protected function _upload($savePath) {
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($savePath);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize = 5242880;
        //$upload->maxSize = 6291456;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
		//设置附件上传目录
		$upload->savePath = $savePath;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
		$upload->saveRule = "md5content";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			return $uploadList;;
		}

	}
	
	/**
	 * 删除动态
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int id 动态id
	 * @return data
	 */
	public function delete(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id){
			$return = array();
			$dynamicM = new DynamicModel();
			$where = array("id"=>$id);
			$DyInfo = $dynamicM->getOne($where);
			
			if($DyInfo["type"]==2){
				//删除照片
				$PhotoDynamicM = new PhotoDynamicModel();
				$ids = explode("|", $DyInfo["ids"]);
				foreach ($ids as $v){
					$PhotoInfo = $PhotoDynamicM->getOne(array("id"=>$v));
					_unlink(WR.$PhotoInfo["url"]);
					$this->get_photodynamic($v,1);
					DelOss(array($PhotoInfo["url"]));
				}
				$PhotoDynamicM->delOne(array("danamicid"=>$id));
				
			}elseif($DyInfo["type"]==3){
				//删除视频
				$VideoDynamicM = new VideoDynamicModel();
				$VideoInfo = $VideoDynamicM->getOne(array("id"=>$DyInfo["ids"]));
				_unlink(WR.$VideoInfo["imageurl"]);
				_unlink(WR.$VideoInfo["url"]);
				$this->get_videodynamic($DyInfo["ids"],1);
				DelOss(array($VideoInfo["imageurl"],$VideoInfo["url"]));
				$VideoDynamicM->delOne(array("danamicid"=>$id));
				//Dump($VideoInfo);exit;
			}
			//删除评论
			$DynamicCommentM = new DynamicCommentModel();
			$DynamicCommentM->delOne(array("danamicid"=>$id));
			//删除动态
			$dynamicM->delOne($where);
			$this->get_dynamiclist(0,0,0,1);//清空动态缓存
            //删除已经通过的动态，也需要将存在redis中的缓存删除，然后重新读取数据库
            $redis=$this->redisconn();
            $redis->del('dynamic_boy');
            $redis->del('dynamic_girl');
		}
		Push_data($return);
	}
	/**
     * 发表动态评论
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int id 动态id
     * @request string content 动态文字内容
     * @request int Touid 被评论人的uid
     * @request int clientid 客户端生成评论id
     * @return data
     */
	public function addcomment(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$id = $data["id"] ? $data["id"] : 0;
        $clientid=$data['clientid'];
		$content = $data["content"] ? $data["content"] : "";
        $contentz=urlencode($content);
        //检测评论是否合格
        $checktextC= new ChecktextController();
        $textdata=$checktextC->checktext($content,array("antispam"));

		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id&&$content){

            if($textdata['antispam']=='pass'){
                $return = array();
                $DynamicCommentM = new DynamicCommentModel();
                if(isset($data['touid'])){
                    $addArr = array(
                        "danamicid"=>$id,
                        "content"=>$contentz,
                        "time"=>time(),
                        "uid"=>$uid,
                        "touid"=>$data['touid'],
                        "clientid"=>$clientid,
                        "status"=>1
                    );
                }else{
                    $addArr = array(
                        "danamicid"=>$id,
                        "content"=>$contentz,
                        "time"=>time(),
                        "uid"=>$uid,
                        "clientid"=>$clientid,
                        "status"=>1
                    );
                }
                $dat=array();
                $dat['uid']=$uid;
                $countadd=$DynamicCommentM->where($dat)->count();
                if($countadd>9){
                    $goldcount=$this->UserInfo['gold'];
                    if($goldcount>=1){
                        //扣除用户账户所对应的金币
                        $this->gold_reduce($uid,$uid,1,$goldcount,3);
                        $return['data']['leftgold']=$goldcount-1;
                    }else{
                        $return['code'] = ERRORCODE_201;
                        $return['message'] = $this->L("金币数不足，请充值");
                        Push_data($return);
                    }
                }
                $result=$DynamicCommentM->addOne($addArr);

                if($data['platforminfo']['product'] == "24112"){
	                // ------------------------------------------------------

	                $Dynamic = new DynamicModel();
	                $where['id'] = $id;
	                $retu = $Dynamic->getOne($where);
	                //消息推送
	                $res = $Dynamic->updateOne($where,array('unreadcomments'=>$retu['unreadcomments']+1 , 'unreadmessage'=>$retu['unreadmessage']+1));
					$retu = $Dynamic->getOne($where);

					$contents = [
						'push_type' => "dynamic",
						'data' =>  (object)$retu['unreadmessage']
					];

					$content=json_encode($contents);
					// $this->sendsysmsg($retu['uid'],$content,1005);
			        $argv1 = array();
			        $argv1['url'] = "http://127.0.0.1:81/userauth/sendtzmsg/?uid=" . $retu['uid'] . "&content=" . $content ."&from=10002";
			        $argv1['time'] =1;
			        $argv1 = base64_encode(json_encode($argv1));
			        $command = "/usr/bin/php " . WR . "/openurl.php " . $argv1 . " > /dev/null &";
			        system($command);
			        // ------------------------------------------------------
		    	}

                $this->get_comment($id,1);//清空动态评论

            }else{
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L("評論內容不合格！");
                Push_data($return);
            }

		}
		Push_data($return);
	}
	/**
     * 删除动态评论
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int id 评论id
     * @request int danamicid 动态id
     * @request int clientid 客户端生成的评论id
     * @return data
     */
	public function deletecomment(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$danamicid=$data['danamicid'];
        $clientid=$data['clientid'];
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id){
	
		$return = array();
			$DynamicCommentM = new DynamicCommentModel();
			/*$data = array(
				"id"=>$id,
                '_logic'=> 'or',
				"clientid"=>$clientid,
				"uid"=>$uid
			);*/
            $map['id']=$id;
            $map['clientid']=$clientid;
            $map['_logic'] = 'or';
            $final['_complex'] = $map;
            $final['uid']=$uid;
            //$result=$DynamicCommentM->getOne($data);
            //$danamicid=$result['danamicid'];
			$result=$DynamicCommentM->delOne($final);
			if($result==0){
			    $return['message']='没有更新条数';
            }
			$this->get_comment($danamicid,1);//清空动态评论
		}
		Push_data($return);
	}
	
	/**
     * 动态列表
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int type 1全部动态，2关于我的，3获取指定用户动态 默认值1
     * @request int page 当前页码,默认值1
     * @request string userfield 需要的用户字段默认uid|head|nickname
     * @request int touid Type为3是必传，用户id
     * @return data
     */
	public function dynamiclist(){
		$data = $this->Api_recive_date;
		$platforminfo=$this->platforminfo;
		$uid = $this->uid;
        $UserInfo=$this->UserInfo;
        $gender=$UserInfo['gender'];
		$type = $data["type"] ? $data["type"] : 1;//1全部动态2和我有关的3获取指定用户动态
		$touid = $data["touid"] ? $data["touid"] : 0;//
		$page = $data["page"] ? $data["page"] : 1;//
		$pagesize = $data["pagesize"] ? $data["pagesize"] : 15;//
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		$return = array();

		if($type==2){
				$where = array();
				$where["uid"] = $uid;
				$where["status"]=10;
            $DynamiclistTemp = $this->get_dynamiclist($where,$page,$pagesize);
            
			
		}elseif($type==3){
				$where = array();
				$where["uid"] = $touid;
            $DynamiclistTemp = $this->get_dynamiclist($where,$page,$pagesize);
		}else {
            $where = array();
            if($gender==1){
                $DynamiclistTemp= $this->get_dynamiclist_girl($page, $pagesize);
            }else{
                $DynamiclistTemp= $this->get_dynamiclist_boy($page, $pagesize);
            }
            // dump($DynamiclistTemp);die;
            if ($DynamiclistTemp) {
                foreach ($DynamiclistTemp['zlist'] as $k1 => $v1) {
                    $DynamiclistTemp['list'][$k1]["id"]=substr($v1,0,strpos($v1, '_'));
                    $data1=substr($v1,strpos($v1, '_')+1);
                    $DynamiclistTemp['list'][$k1]=json_decode($data1,true);
                    $DynamiclistTemp['list'][$k1]['content']=urldecode($DynamiclistTemp['list'][$k1]['content']);
                }
            }
            $DynamiclistTemp["pageSize"]=$pagesize;
            $DynamiclistTemp["page"]=$page;
        }

		if(!empty($DynamiclistTemp)){
            $Dynamiclist=$this->dynamictemp($DynamiclistTemp,$uid,$userfield);

		}
		// dump($Dynamiclist);die;

		$isfollow = 0;

		foreach ($Dynamiclist as $key => $value) {

			$id = $value['id'];
			// dump($value);die;
	        $sql = " select uid from t_dynamic where id = ".$id;
	        $touid = M()->query($sql);
			$sql = " select * from t_user_follow where uid = ".$uid." and touid = ".$touid[0]['uid'];
			
			$uids = M()->query($sql);

			if(!empty($uids)){
				$isfollow = 1;
			}
			//是否关注
			$Dynamiclist[$key]['isfollow'] = $isfollow;

			$wheres['uid'] = $uid;		
			$Dynamics = new DynamicModel();
			$retu = $Dynamics->getList($wheres);
			if(!empty($retu)){
				foreach ($retu as $k3 => $v3) {
					if($v3['id'] == $id){
						//未读点赞 
						$Dynamiclist[$key]['supportnumber'] = $v3['unreadpraise'];

						//未读评论
						$Dynamiclist[$key]['commentnumber'] = $v3['unreadcomments'];
					}
				}
			}
		}
		
		$return_data["dynamiclist"]=$Dynamiclist;
		$return_data["page"]=array(
				"pagesize"=>$DynamiclistTemp["pageSize"],
				"page"=>$DynamiclistTemp["page"],
				"totalcount"=>$DynamiclistTemp["totalCount"]
			);

        //版本更新，相关平台相关版本无法使用本接口//现处在关闭状态
       /* $versioncs = $platforminfo['version'];
        $versioncs = str_replace('.', '', $versioncs);
        $product = $platforminfo['product'];

        $versiondata=M('version_update');
        $alldata=$versiondata->select();
        foreach($alldata as $k=>$v){
                $versiondb=str_replace('.', '', $v['version']);
                if($product==$v['product']&$versioncs<$versiondb){
                    if($v['isopen']=='1'){
                        if($product=='24111'||$product=='20114'){
                            $return_data["dynamiclist"]='';
                        }else{
                            $return_data["dynamiclist"]=array();
                        }

                        break;
                }else{
                        break;
                    }
            }

        }*/

		$return["data"] = $return_data;
		//Dump($return);
		Push_data($return);
	}
	//循环遍历动态，加载相片和视频
    public function dynamictemp($DynamiclistTemp,$uid,$userfield){
            foreach($DynamiclistTemp["list"] as $k=>$v){
                $obj = array();
                if($v["type"]==3){
                    if($v["ids"])
                        $obj[] = $this->get_videodynamic($v["ids"]);
                }elseif($v["type"]==2){
                    $ids = explode("|", $v["ids"]);
                foreach ($ids as $v1){
                    if($v1)
                        $obj[] = $this->get_photodynamic($v1);
                }
            }
                $comment = $this->get_comment($v["id"]);
                $support = $this->get_support($v["id"]);
                $issupport =0;
                foreach($support as $sv){
                    if($sv["user"]["uid"]==$uid){
                        $issupport = 1;
                        break;
                    }
                }
                $user = $this->get_user($v["uid"]);
                $user = $this->get_user_field($user,$userfield);
                $user['issayhello']= $this->isSayHello($v['uid']);
                //随机获取近三天的时间戳
                $newtime=time();
                $beforetime=$newtime-60*60*24*3;
                $time=rand($beforetime,$newtime);
                $Dynamiclist[]=array(
                    "id"=>$v["id"],
                    "content"=>$v["content"],
                    "time"=>$time,
                    "type"=>$v["type"],
                    "obj"=>$obj,
                    "comment"=>$comment,
                    "support"=>$support,
                    "issupport"=>$issupport,
                    "user"=>$user,
                    "status"=>$v["status"]
                );
            }
            return $Dynamiclist;
    }
	/*
	 * 获取自己和指定人的动态列表
	 */
	protected function get_dynamiclist($where,$page,$pagesize,$reset=0){
		// $wheres['uid'] = $uid;
	    $path = $this->cachepath."dynamiclist/";
	    // dump(F($cache_name,$res,$path));die;
	    if($where["status"]==10){
	        unset($where["status"]);
	    }else{
	        $where["status"] = 1;
	    }
	    $cache_name = 'dynamiclist'.md5(json_encode($where).$page.$pagesize);
	    if($reset==1){
	        return deldir($path);
	    }
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $DynamicM = new DynamicModel();
	        $res = $DynamicM->getListPage($where,$page,$pagesize);
	        
	        foreach($res['list'] as $k=>$v){
                $res['list'][$k]['content']=urldecode($v['content']);
            }
	        F($cache_name,$res,$path);
	    }
	    return $res;
	}


	//从redis中获取所有人的动态列表缓存
    protected function get_dynamiclist_boy($page,$pagesize){
        $redis=$this->redisconn();
        //$redis->delete('dynamic');
        //缓存中储存总数据
        $lang=$redis->listSize('dynamic_boy');
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang==0){
            $DynamicM = new DynamicModel();
            //限制50条
            $Dres=$DynamicM->where(array('status'=>1))->order('id desc')->limit(1000)->select();
        foreach($Dres as $k1=>$v1){
            //返回男性动态
            $dygender=$this->get_user($v1['uid'])['gender'];
            if($dygender==2){
                continue;
            }
            $value=json_encode($v1,true);
            $id=$v1['id'];
            $ret[]=$redis->listPush('dynamic_boy',$id.'_'.$value,1);
            }
            $lang=$redis->listSize('dynamic_boy');
        }
        //页总数
        $page_count = ceil($lang/$pagesize);
        $page=rand(1,$page_count);
	    $result['zlist']=$redis->listLrange('dynamic_boy',($page-1)*$pagesize,(($page-1)*$pagesize+$pagesize-1));
	    $result['totalCount']=$lang;
       return $result;
    }
    //从redis中获取所有人的动态列表缓存
    protected function get_dynamiclist_girl($page,$pagesize){
        $redis=$this->redisconn();
        //$redis->delete('dynamic');
        //缓存中储存总数据
        $lang=$redis->listSize('dynamic_girl');
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang==0){
            $DynamicM = new DynamicModel();
            //限制50条
            $Dres=$DynamicM->where(array('status'=>1))->order('id desc')->limit(1000)->select();
            foreach($Dres as $k1=>$v1){
                //返回女性动态
                $dygender=$this->get_user($v1['uid'])['gender'];
                        if($dygender==1){
                            continue;
                        }

                $value=json_encode($v1,true);
                $id=$v1['id'];
                $ret[]=$redis->listPush('dynamic_girl',$id.'_'.$value,1);
            }
            $lang=$redis->listSize('dynamic_girl');
        }
        //页总数
        $page_count = ceil($lang/$pagesize);
        $page=rand(1,$page_count);
        $result['zlist']=$redis->listLrange('dynamic_girl',($page-1)*$pagesize,(($page-1)*$pagesize+$pagesize-1));
        $result['totalCount']=$lang;
        return $result;
    }
	//获取评论
	public function get_comment($id,$reset='0'){
	    $path = $this->cachepath."comment/";
	    $cache_name = 'comment_'.$id;
	    if($reset == 1){
	        return F($cache_name,NULL,$path);
	    }
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $DynamicCommentM = new DynamicCommentModel();
	        $temp = $DynamicCommentM->getList(array("danamicid"=>$id));
	        $res = array();
	        foreach($temp as $k=>$v){
	           // if($temp[$k]['status']==1) {
                    $user = $this->get_user ($v["uid"]);
                    $user = $this->get_user_field ($user, "uid|head|nickname");
                    $touser = $this->get_user ($v["touid"]);
                    $touser = $this->get_user_field ($touser, "uid|head|nickname");
                    $res[] = array(
                        "id" => $v["id"],
                        "user" => $user,
                        "touser" => $touser,
                        "content" => urldecode($v["content"]),
                        "time" => $v["time"],
                        "status"=>$temp[$k]['status']
                    );
                //}
	        }
	        F($cache_name,$res,$path);
	    }
        $res=$res;
	    return $res;
	}

	/**
	 * 关注我的人动态
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int page 当前页面
	 * @request string userfield 需要的用户字段默认 uid|head|nickname
	 * @return data
	 */
	public function getfollowdynamic()
	{

		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = 15;
		
		$Wdata = array();
		$Wdata['uid'] = $uid;
		
		$UserFollowM = new UserFollowModel();

		$ret = $UserFollowM->getListPage($Wdata,$pageNum,$pageSize);

		foreach($ret['listUser'] as $k=>$v){
		    $tempUserInfo = $this->get_user($v['touid']);
		    if($tempUserInfo){
		    	// dump($v);die;
		    	$where = array();
				$where["uid"] = $v['touid'];
		    	$res = $this->get_dynamiclist($where,$pageNum,$pageSize);
		    	$return_data["page"]=array(
					"pagesize"=>$res["pageSize"],
					"page"=>$res["page"],
					"totalcount"=>$res["totalCount"]
				);
		    	$Dynamiclist[]=$this->dynamictemp($res,$uid,$userfield);

		    }

			
		}
		
		//去除空数据
		$Dynamiclist = array_filter($Dynamiclist);

		
		foreach ($Dynamiclist as $key => $value) {

			foreach ($value as $key2 => $value2) {
				$return_data["dynamiclist"][] = $value2;
			}
		
		}
		$isfollow = 0;
		if(empty($return_data["dynamiclist"])){
			$return_data["page"]=array(
					"pagesize"=>15,
					"page"=>1,
					"totalcount"=>0
			);
			$return["data"] = $return_data;
			Push_data($return);
		}
		foreach ($return_data["dynamiclist"] as $key => $value) {
			
			$id = $value['id'];

	        $sql = " select uid from t_dynamic where id = ".$id;
	        $touid = M()->query($sql);
	        // dump($touid);die;
			$sql = " select * from t_user_follow where uid = ".$uid." and touid = ".$touid[0]['uid'];
			
			$uids = M()->query($sql);

			if(!empty($uids)){
				$isfollow = 1;
			}
			//是否关注
			$return_data["dynamiclist"][$key]['isfollow'] = $isfollow;
		}
		
		
		$return["data"] = $return_data;
		


		Push_data($return);
		
	}
	//获取点赞
	public function get_support($id,$reset='0'){
	    $path = $this->cachepath."support/";
	    $cache_name = 'support'.$id;
	    if($reset == 1){
	        return F($cache_name,NULL,$path);
	    }
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $DynamicCommentM = new DynamicSupportModel();
	        $temp = $DynamicCommentM->getList(array("danamicid"=>$id));
	        $res = array();
	        foreach($temp as $k=>$v){
	            $user = $this->get_user($v["uid"]);
	            $user = $this->get_user_field($user,"uid|head|nickname|gender");
	            $res[] = array(
	                "id"=>$v["id"],
	                "user"=>$user,
	                "time"=>$v["time"]
	            );
	        }
	        F($cache_name,$res,$path);
	    }
	    return $res;
	  
	}
	//获取视频
	public function get_videodynamic($id,$reset='0'){
		$path = WR.'/userdata/cache/dynamic/video/';
		$cache_name = 'videoinfo_'.$id;
        if($reset==1){
            return F($cache_name,NULL,$path);
        }
		if(F($cache_name,'',$path) && $reset == 0){
			$Info = F($cache_name,'',$path);
		}else{
			$VideoDynamicM = new VideoDynamicModel();
			$videoTemp = $VideoDynamicM->getOne(array("id"=>$id));
			$Info=array(
				"id"=>$videoTemp["id"],
				"ltime"=>$videoTemp["ltime"],
				"url"=>C("IMAGEURL").$videoTemp["url"],
				"imageurl"=>C("IMAGEURL").$videoTemp["imageurl"],
				"status"=>$videoTemp["status"]
			);
			F($cache_name,$Info,$path);
		}
		return $Info;
	}
	//获取动态图片
	public function get_photodynamic($id,$reset='0'){
		$path = WR.'/userdata/cache/dynamic/photo/';
		$cache_name = 'photoinfo_'.$id;
        if($reset==1){
            return F($cache_name,NULL,$path);
        }
		if(F($cache_name,'',$path) && $reset == 0){
			$Info = F($cache_name,'',$path);
		}else{
			$PhotoDynamicM = new PhotoDynamicModel();
			$Temp = $PhotoDynamicM->getOne(array("id"=>$id));
			$Info=array(
				"id"=>$Temp["id"],
				"url"=>C("IMAGEURL").$Temp["url"],
				"thumbnaillarge"=>C("IMAGEURL").$Temp["url"],
				"thumbnailsmall"=>C("IMAGEURL").$Temp["url"],
				"status"=>$Temp["status"],
				"seetype"=>$Temp["seetype"]
			);
			F($cache_name,$Info,$path);
		}
		return $Info;
	}

	/**
     * 动态点赞
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int id 动态id
     * @return data
     */
	public function addsupport(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");

		if($id){
			$return = array();
			$DynamicSupportM = new DynamicSupportModel();
			$addArr = array(
				"danamicid"=>$id,
				"time"=>time(),
				"uid"=>$uid
			);
			$temp = $DynamicSupportM->getOne(array("uid"=>$uid,"danamicid"=>$id));
			if(!$temp){
				$DynamicSupportM->addOne($addArr);
			}
				if($data['platforminfo']['product'] == "24112"){
					// ------------------------------------------------------

	                $Dynamic = new DynamicModel();
	                $where['id'] = $id;
	                $retu = $Dynamic->getOne($where);
	                //消息推送
	                $res = $Dynamic->updateOne($where,array('unreadpraise'=>$retu['unreadpraise']+1 , 'unreadmessage'=>$retu['unreadmessage']+1));
					$retu = $Dynamic->getOne($where);

					$contents = [
						'push_type' => "dynamic",
						'data' =>  (object)$retu['unreadmessage']
					];

					$content=json_encode($contents);
					// $this->sendsysmsg($retu['uid'],$content,1005);
			        $argv1 = array();
			        $argv1['url'] = "http://127.0.0.1:81/userauth/sendtzmsg/?uid=" . $retu['uid'] . "&content=" . $content ."&from=10002";
			        $argv1['time'] =1;
			        $argv1 = base64_encode(json_encode($argv1));
			        $command = "/usr/bin/php " . WR . "/openurl.php " . $argv1 . " > /dev/null &";
			        system($command);
			        // ------------------------------------------------------
		   		}

				$this->get_support($id,1);//清空点赞缓存
		}
		Push_data($return);
	}

	/**
     * 动态取消未读
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @return data
     */
	public function cancelunread()
	{

		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		$where['uid'] = $uid;
		$Dynamic = new DynamicModel();
		$res = $Dynamic->updateOne($where,array('unreadpraise'=>0 , 'unreadmessage'=>0 ,'unreadcomments' => 0));
		if($res !== false){
			$return = [
				'code' => 200,
				'message'  => "ok"
			];
		}
		Push_data($return);

	}

	/**
     * 取消点赞
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int id 动态id
     * @return data
     */
	public function deletesupport(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id){
			$return = array();
			$DynamicSupportM = new DynamicSupportModel();
			$data1 = array(
				"danamicid"=>$id,
				"uid"=>$uid
			);
			$result=$DynamicSupportM->delOne($data1);
			if(!$result){
			    echo "删除失败";
            }

			$this->get_support($id,1);//清空点赞缓存
		}
		Push_data($return);
	}
    private function isSayHello($touid, $uid=0)
    {
        $uid = $uid ? $uid : $this->uid;
        $redis = $this->redisconn();
        $redisStr = "sayhello_" . $uid . "_" . $touid;

        if ($redis->exists($redisStr)) {
            return 1;
        } else {
            return 0;
        }
    }
}
?>
