<?php

/**
 * Created by PhpStorm.
 * User: 亮亮
 * Date: 2018/5/10
 * Time: 14:21
 */
use Think\Controller;
class ConsumeController  extends BaseController {
    public function __construct(){
        parent::__construct();
    }
    /**
     * 虚拟金币支付
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int type 类型2：观看视频
     * @request int money 价格
     * @return data
     */
    public function consume(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $touid = $data['touid'] ? $data['touid'] : "";
        $paytime = time();
        $type = $data['type'] ? $data['type'] : 2;
        $money = $data['money'] ? $data['money'] : "0.00";

        //判断用户是否用足够金币发送礼物
        $GoldCount=$this->UserInfo['gold'];
        $totalprice=$money;
        if($GoldCount>=$totalprice) {

                //扣除用户账户所对应的金币
                $subgold=$this->gold_reduce($uid,$touid,$totalprice,$GoldCount,$type);
                if($subgold){
                    $return = array();
                    $return["data"]["leftgold"] = $GoldCount-$money;
                   //插入数据库成功，扣除金额成功。
                }else{
                    $return = array();
                    $return['code'] = ERRORCODE_501;
                    $return['message'] = $this->L ("扣除金额失败");
                    //插入数据库成功，扣除金额失败。
                }

            }else{
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $this->L ("余额不足，请充值");
        }
        Push_data ($return);
        }



    //获取收益转换（魅力值转换金币）
    public function getmoneyAsgold(){
        $data = $this->Api_recive_date;
        $money = $data['money'];
        if(!empty($money) && $money >= 50 ){
            $VodPrivateSet = new VodPrivateSetModel();
            //获取转换抽水
            $vodpri_res = $VodPrivateSet->getOne('money_as_gold');
            $asgold = $money-($money* ($vodpri_res['money_as_gold']/100));
            $asgold = intval(ceil($asgold));
            Push_data(["data"=>$asgold]);
        }else{
            $return = [
                'code' => 4002,
                'message'  => "魅力值满足兑换条件"
            ];
            Push_data($return);
        }

    }

    //扣除魅力值 增加金币
    public function consumeMoney(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        if(empty($uid)){
            $return = [
                'code' => 4002,
                'message'  => "请登录"
            ];
            Push_data($return);
        }
        $money = $data['money'];
        $gold = $data['gold'];
        //扣除魅力值
        $mmoney = M('m_money');
        $mmon = $mmoney->where(array('uid' => $uid))->find();
        $resm = array();
        if($mmon['totalmoney'] < $money || $mmon['leftmoney'] < $money){
            $return = [
                'code' => 4002,
                'message'  => "没有那么多魅力值，无法兑换"
            ];
            Push_data($return);
        }
        $resm['totalmoney'] = $mmon['totalmoney'] - $money;
        $resm['leftmoney'] = $mmon['leftmoney'] - $money;
        $moneyRes = $mmoney->where(array('uid' => $uid))->save($resm);
        if($moneyRes){
            $redis=$this->redisconn();
            $redis->delete('withdrawal_' . $uid);
        }
        //获取转换抽水
        $VodPrivateSet = new VodPrivateSetModel();
        $vodpri_res = $VodPrivateSet->getOne('money_as_gold');
        $asgold = $money-($money* ($vodpri_res['money_as_gold']/100));
        $asgold = intval(ceil($asgold));
        //增加金币
        if($asgold != $gold){
            $return = [
                'code' => 4002,
                'message'  => "兑换金币数量有所变动"
            ];
            Push_data($return);
        }
        $res = $this->gold_add($uid,$gold);
        if(!empty($res) && !empty($moneyRes)){
            Push_data();
        }else{
            $return = [
                'code' => 4002,
                'message'  => "兑换金币错误"
            ];
            Push_data($return);
        }

    }




}
