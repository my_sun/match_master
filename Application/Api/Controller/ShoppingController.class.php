<?php
/**
 * aiqingdao管理
 */

class ShoppingController extends BaseController
{
    public function __construct ()
    {
        parent::__construct ();
        
    }


     public function treasurebox(){
         $uid = $this->uid;
         $resultdata= M('conversiongift')->where(array('uid'=>$uid))->select();
         if($resultdata){
             $redata=array();
             foreach($resultdata as $k=>$v){
                 $redata[$k]['number']=$v['number'];
                 $redata[$k]['giftlist']=M('prop_shop')->where(array('name'=>$v['propname']))->find();
                 if($v['propname']=='v1'||$v['propname']=='v2'||$v['propname']=='v3'||$v['propname']=='v4'){
                     $redata[$k]['giftlist']['isgopay']=true;
                 }
                 $redata[$k]['giftlist']['img']=C ("IMAGEURL").$redata[$k]['giftlist']['img'];
             }

         }
         $return['data']=$redata;
         $return['message'] = $this->L("CHENGGONG");
         Push_data($return);


     }

     //道具商城

    public function propshop(){
        $data=M('prop_shop')->where(array('isuse'=>1))->select();
        $msgurl=C ("IMAGEURL");
        $platforminfo = $this->platforminfo;
        foreach($data as $k=>$v){
            $data[$k]['img']=$msgurl.$v['img'];
            if($platforminfo['product']==23110 && $v['name'] == 'seemsg'){
                unset($data[$k]);
            }
        }


        $return['message'] = $this->L("CHENGGONG");
        sort($data);
        $return['data']['list'] =$data;
        Push_data($return);

    }
    //兑换道具
    public function conversionprop(){
        $data = $this->Api_recive_date;
        $uid=$this->uid;
        $id=$data['id'];
        $dataa=M('prop_shop')->where(array('id'=>$id))->find();
        $shoppdata=M('conversiongift')->where(array('uid'=>$uid,'propname'=>$dataa['name']))->find();
        $leftmoney=M('m_money')->where(array('uid'=>$uid))->getField('leftmoney');
        if ($leftmoney <$dataa['money'] ) {
            $return['message'] = $this->L("CHENGGONG");
            $return['data']['status'] = 2;
            Push_data($return);
        }
        if($shoppdata) {
                $return['data']['status'] = 1;
                $money=$leftmoney-$dataa['money'];
                M('m_money')->where(array('uid'=>$uid))->setField('leftmoney',$money);
                $giftmsg['number']=$shoppdata['number']+1;
                M('conversiongift')->where(array('uid'=>$uid,'propname'=>$dataa['name']))->save($giftmsg);
                $return['message'] = $this->L("CHENGGONG");
                Push_data($return);

        }else{
            $return['data']['status'] = 1;
            $datay=array();
            $money=$leftmoney-$dataa['money'];
            M('m_money')->where(array('uid'=>$uid))->setField('leftmoney',$money);

            $datay['uid']=$uid;
            $datay['number']=1;
            $datay['propname']=$dataa['name'];
            M('conversiongift')->data($datay)->add();
            $return['message'] = $this->L("CHENGGONG");
            Push_data($return);
        }
    }

     //使用道具
    public function useprop(){
        //开始兑换
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $userinfo=$this->UserInfo;
        $id=$data['id'];
        $dataa=M('prop_shop')->where(array('id'=>$id))->find();
        $shoppdata=M('conversiongift')->where(array('uid'=>$uid,'propname'=>$dataa['name']))->find();
        if($shoppdata) {
            if ($shoppdata['number'] < 1) {
                $return['data']['status'] = 2;
                Push_data (array('message' => $this->L ("沒有該道具，無法使用此功能！"), 'code' => ERRORCODE_201));
            } else {
                $UserBaseM = new UserBaseModel();
                //如果为赠送会员以及vip则再此处理逻辑
                if($shoppdata['propname']=='v1'){
                    //v1黄金会员三天
                    $viptime = 3*24*60*60;
                    $vipgrade=1;
                    $rtime=$this->UserInfo["viptime"];
                    if($rtime==0||$rtime<time()){
                        $viptime = $viptime+time();
                    }else{
                        $viptime = $viptime+$rtime;
                    }
                    if($userinfo['vipgrade']!=0 & $userinfo['vipgrade']>$vipgrade){
                        $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                        $this->set_user_field($uid,'viptime',$viptime);
                    }else{
                        $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
                        $this->set_user_field($uid,'viptime',$viptime);
                        $this->set_user_field($uid,'vipgrade',$vipgrade);
                    }


                }elseif($shoppdata['propname']=='v2'){
                    //v1黄金会员三天
                    $viptime = 3*24*60*60;
                    $vipgrade=2;
                    $rtime=$this->UserInfo["viptime"];
                    if($rtime==0||$rtime<time()){
                        $viptime = $viptime+time();
                    }else{
                        $viptime = $viptime+$rtime;
                    }
                    if($userinfo['vipgrade']!=0 & $userinfo['vipgrade']>$vipgrade){
                        $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                        $this->set_user_field($uid,'viptime',$viptime);
                    }else{
                        $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
                        $this->set_user_field($uid,'viptime',$viptime);
                        $this->set_user_field($uid,'vipgrade',$vipgrade);
                    }

                }elseif($shoppdata['propname']=='v3'){
                    //v1黄金会员三天
                    $viptime = 3*24*60*60;
                    $vipgrade=3;
                    $rtime=$this->UserInfo["viptime"];
                    if($rtime==0||$rtime<time()){
                        $viptime = $viptime+time();
                    }else{
                        $viptime = $viptime+$rtime;
                    }
                    if($userinfo['vipgrade']!=0 & $userinfo['vipgrade']>$vipgrade){
                        $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                        $this->set_user_field($uid,'viptime',$viptime);
                    }else{
                        $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
                        $this->set_user_field($uid,'viptime',$viptime);
                        $this->set_user_field($uid,'vipgrade',$vipgrade);
                    }

                }elseif($shoppdata['propname']=='v4'){
                    $result=$this->gold_add($uid, 100);
                    $this->set_user_field($uid,"gold",$userinfo["gold"]+100);
                }

                $return['data']['status'] = 1;
                $giftmsg['number']=$shoppdata['number']-1;
                M('conversiongift')->where(array('uid'=>$uid,'propname'=>$dataa['name']))->save($giftmsg);
                $return['message'] = $this->L("CHENGGONG");
                Push_data($return);
            }
        }else{
            $return['data']['status'] = 2;
            Push_data (array('message' => $this->L ("沒有該道具，無法使用此功能！"), 'code' => ERRORCODE_201));
        }
    }


    //大转盘接口
    public  function turntable(){
        $data = $this->Api_recive_date;
        $isneedword=$data['isneedword']?$data['isneedword']:0;
        $toopen=$data['toopen']?$data['toopen']:0;
        $return=array();
        $giftlistdazhuanpan=M('turntable')->where(array('isuse'=>1))->select();
        //开始转动
        if($toopen==1){
            $uid = $this->uid;
            $leftmoney=M('user_extend')->where(array('uid'=>$uid))->getField('conversiongift');
            $xhmoney=$giftlistdazhuanpan[0]['money'];
            $gifttype=array();
            $return=array();
            $gailv=array();

            if($leftmoney){
                if($leftmoney<$xhmoney){
                    $return['message'] = $this->L("CHENGGONG");
                    $return['data']['status'] = 2;
                    Push_data($return);
                }else{
                    foreach($giftlistdazhuanpan as $k=>$v){
                        $gailv[$v['propname']]=$v['prob'];
                    }
                      $giftmsg=array();
                      $datay=array();
                    $namezj=get_rand($gailv);
                    if($namezj=='empty'){
                        $return['data']['status'] = 1;
                        $return['data']['gifttype'] = 1;
                        $money=$leftmoney-$xhmoney;
                        M('user_extend')->where(array('uid'=>$uid))->setField('conversiongift',$money);
                    }else{
                        $return['data']['status'] = 1;
                        $gifttype=M('turntable')->where(array('propname'=>$namezj))->getField('gifttype');
                        $return['data']['gifttype'] =$gifttype;
                        $money=$leftmoney-$xhmoney;
                        M('user_extend')->where(array('uid'=>$uid))->setField('conversiongift',$money);
                        $shoppdata=M('conversiongift')->where(array('uid'=>$uid,'propname'=>$namezj))->find();
                        if($shoppdata){
                            $giftmsg['number']=$shoppdata['number']+1;
                            M('conversiongift')->where(array('uid'=>$uid,'propname'=>$namezj))->save($giftmsg);
                        }else{
                            $datay['uid']=$uid;
                            $datay['number']=1;
                            $datay['propname']=$namezj;
                            M('conversiongift')->data($datay)->add();
                        }
                    }

                    $return['message'] = $this->L("CHENGGONG");
                    Push_data($return);


                }
            }else{
                $return['message'] = $this->L("CHENGGONG");
                $return['data']['status'] = 2;
                Push_data($return);
            }
        }else{

            $msgurl=C ("IMAGEURL");
            foreach($giftlistdazhuanpan as $k=>$v){
                $giftlistdazhuanpan[$k]['img']=$msgurl.$v['img'];
            }

            $return['data']['money']=$giftlistdazhuanpan[0]['money'];
            $return['data']['giftlist']=$giftlistdazhuanpan;
            $runderword=array();
            if($isneedword){
                $userlist=M('user_base')->where(array('user_type'=>2))->order("RAND()")->limit(50)->getField('nickname',true);
                foreach($userlist as $k=>$v){
                    $large=count($giftlistdazhuanpan);
                    $i=rand(1,$large-2);
                    $runderword[$k]='恭喜'. $v .'抽中'.$giftlistdazhuanpan[$i]['title']. date("Y-m-d ",time()) ;
                }
                $return['data']['randword']=$runderword;
            }
            $return['message'] = $this->L("CHENGGONG");
            Push_data($return);

        }





    }


    public function homeslideshow(){
            $data=M('slideshow')->where(array('isuse'=>1))->select();
            $msgurl=C ("IMAGEURL");
            foreach($data as $k=>$v){
                $data[$k]['homepictureurl']=$msgurl.$v['homepictureurl'];
            }
            $return['message'] = $this->L("CHENGGONG");
            $return['data']['list'] =$data;
            Push_data($return);
    }
}
