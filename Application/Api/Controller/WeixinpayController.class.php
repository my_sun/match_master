<?php
/**
 * 微信支付
 */
class WeixinpayController extends BaseController {
    public function __construct(){
        //parent::__construct();

    }
    /**
     * notify_url接收页面
     */
    public function notify(){
        // 导入微信支付sdk
        Vendor('Weixinpay.Weixinpay');
        $wxpay=new \Weixinpay();
        $result=$wxpay->notify();
        if ($result) {
            // 验证成功 修改数据库的订单状态等 $result['out_trade_no']为订单号
            $data=$result['attach'];
        }
    }
    public function pay($result){
        // 导入微信支付sdk
        Vendor('Weixinpay.Weixinpay');
        $wxpay=new \Weixinpay();
        // 获取客户端需要用到的数据
        $data=$wxpay->pay($result);
        $return["data"]=$data;
        // 将数据传递给前台
        Push_data($return);
    }

}