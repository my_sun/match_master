<?php
/*
 * 缓存路径配置
 */
define('CHCHEPATH_GIFTLIST',WR.'/userdata/cache/giftlist/');//礼物列表
define('CHCHEPATH_ATTR',WR.'/userdata/cache/attr/');//字典缓存
define('CHCHEPATH_COUNTRY',WR.'/userdata/cache/country/');//国家缓存
define('CHCHEPATH_AREA',WR.'/userdata/cache/area/');//地区缓存
define('CHCHEPATH_LANGUAGE',WR.'/userdata/cache/language/');//语言缓存
define('CHCHEPATH_SWITCH',WR.'/userdata/cache/switch/');//开关缓存
define('CHCHEPATH_USERPHOTOS',WR.'/userdata/cache/photos/');//用户相册缓存，清空某个用户缓存方法 F('user_photo_'.$uid,NULL,CHCHEPATH_USERPHOTOS);
define('CHCHEPATH_USERVIDEO',WR.'/userdata/cache/video/');//视频认证缓存，清空某个用户缓存方法 F('get_user_video'.$uid,NULL,CHCHEPATH_USERPHOTOS);
define('CHCHEPATH_USERAUDIO',WR.'/userdata/cache/audio/');//语音认证缓存，清空某个用户缓存方法 F('get_user_audio'.$uid,NULL,CHCHEPATH_USERPHOTOS);
define('CHCHEPATH_AUTOMSG',WR.'/userdata/cache/automsg/');//
define('CHCHEPATH_PRODUCTS',WR.'/userdata/cache/products/');//products缓存
define('CHCHEPATH_PINFO',WR.'/userdata/cache/pinfo/');//pinfo缓存
define('CHCHEPATH_SENSITIVE',WR.'/userdata/cache/sensitive/');//敏感词缓存
define('ALIPAY_CALLBACK',WR.'/userdata/cache/alipay/');//支付宝app支付回调字段日志

define('REDIS_PRODUCT_INFO','product_info_');//平台信息
return array(
    'APP_SUB_DOMAIN_DEPLOY'   =>    1, // 开启子域名或者IP配置
    'APP_SUB_DOMAIN_RULES'    =>    array(
        "56mm.top"=>"Website/Website",
        "www.56mm.top"=>"Website/Website",
        "web.com"=>"Website/Website",
        /* 域名部署配置
         *格式1: '子域名或泛域名或IP'=> '模块名[/控制器名]';
         *格式2: '子域名或泛域名或IP'=> array('模块名[/控制器名]','var1=a&var2=b&var3=*');
         */
    ),
    /*"IMSDKID"=>array(
/*        "24110"=>"1400141309",
        "24110"=>"1400135411",
        "23110"=>"1400135411",
        "20113"=>"1400135411",
        "20114"=>"1400135411",
        "20111"=>"1400135411",
        "24111"=>"1400135411",
        "23138"=>"1400135411",
        "24112"=>"1400135411",
        "25100"=>"1400135411",
    ),*/
    "IMSDKID"=>array(
        "24110"=>"1400297821",
        "23110"=>"1400297821",
        "20113"=>"1400297821",
        "20114"=>"1400297821",
        "20111"=>"1400297821",
        "24111"=>"1400297821",
        "23138"=>"1400297821",
        "24112"=>"1400297821",
        "25100"=>"1400297821",
    ),

    "IMSDKKEY_1400297817"=>'5a8577ecd90f97d31f7056e82593406af17d34e81c9457056272ec14f44a899f',
    "IMSDKID_KEY"=>'2c19c136596f1177c89f813396eadc7987008022063ad641d5f2c76eeb7e7416',
    "FCM_API_KEY"=>array(
        "24110"=>"AAAAoxtUUzA:APA91bFa-8GCNLwsDnEZ5Ql-bjvUq_dh5K61xIRj4kKpD4vQijOuJhxIwSp1Zf_zDoG_qOjOBECffCZwnqZbh8H--4136uTzbpNAcP-PlMDZ4rZ26JW1M00hSwrG5fT8fjY2MyrFAmYp",
        "23110"=>"AAAAOkp0-_k:APA91bG6IzH_zyBdw3ttwk4xBSXmjQycNsSNl6mgkmqGiNIub1BNlE4xApdYxPa73WQJcJr3Y9tgbjGg4Em8c3e59T5OKIBKamUhlhQzH0XIHQIbxo8y1KY2boo5YXico1ESN8gCEqRD",
        "20114"=>"AAAAOkp0-_k:APA91bG6IzH_zyBdw3ttwk4xBSXmjQycNsSNl6mgkmqGiNIub1BNlE4xApdYxPa73WQJcJr3Y9tgbjGg4Em8c3e59T5OKIBKamUhlhQzH0XIHQIbxo8y1KY2boo5YXico1ESN8gCEqRD",
        "20113"=>"AAAAOkp0-_k:APA91bG6IzH_zyBdw3ttwk4xBSXmjQycNsSNl6mgkmqGiNIub1BNlE4xApdYxPa73WQJcJr3Y9tgbjGg4Em8c3e59T5OKIBKamUhlhQzH0XIHQIbxo8y1KY2boo5YXico1ESN8gCEqRD",
    ),
    "GOOGLE_PAY_KEY"=>array(
        "24111"=>"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq4ZM2s9CCR1HtbKEVR/vvOChfvIaYOsnneQbFG+JD635meDQ6o+vQ8EkM1PiEUvxy+oGo21GzCPJdJKMre7/g8wjYFbe1R5KM2VfE/XhV9Tglu7SA77AADYm/ihyy7U/EYKg7OBSCh+oCI880albiNgYNuyyXiOP0zORDhR9bvhNwhUg6OG8ffXf8o2P960eX2H2cqAMv9Y9lV9GhDSuVmkfRUjAhp01yyTPKNeJ07/0cztRRLLfgFppR5MwakeXm1NxeX/wfEpcLps0gd9cSCNhNhNUHDmJ9XND+MqBZ6gIsFNNZUxxkWj5MqiDaX29cXCJpWwYBPLxpaqlhaWWdwIDAQAB",
        "23138"=>"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjHocMlUms/D18/HSaHzJvBYnWJjUnqiiqiRCmTLHG4pILSGCJDPS+Uta1EgK9/So8lBU781mZTCwwTmmpSiL5a8gHLHrW8BKbaEHI5QTh9ph4sZFD/ebBM1W0BiAYh4wMo1Z6Xan1lzVkOplZlACXhAvlmr7Lio+5N/5qwnvKodQTge6p5CZsu6fLrVyLGfM8NuHoIiSBqAAzqpe8EHOqUfxM+SyjxVjLJ64D3DwJA6PjsqajaspnepfOXV/s+THV2A6YR2rgnTazuikuf4PB/oht41u1EQNFqB+hg0r5y7Ttn4lbcmoDai5YxLuguI8lm3IKSlJbppe6+Sw4krMPwIDAQAB",
        "20114"=>"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv1IfjnX94C4wjOmZ2dtCL/m7yMbw4tcG3KcagRniByzBx1GbjrKxLfuWCSjU9muKZDGLwVbZ+/jQRC9el/D31/TZBl1yob6QIY3ePv56ZtutKdHwQwxL7etRQ3nTG2PMJl4OBemARVXq7qRySuFKGyaHxLxpC5qsd0HTzgnc4rF1RpjWr0ElL5tNmnuaRYiBozOYlESiJ8iTQPidvUxtXABGqTOcVByNJ2lZ3SH7t7vwdiZUu3v6mReo1dyo1uETZdc+XTnxajRzxaYaFvULyvOO1mddCVbj0Gd6hJRxLszkze1qqqbT+a7x62T6rw9LWrAx+hbVc5pFbA957mEDSwIDAQAB",
        "23110"=>"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiCEDo8ApSBcUvvs0ntphZ6H8z6DEpQyJRkszFxj+Mr7PSwq3uJPrn0NOGQ64MWBT2gfJIRdAuS8HjtakJvPsFKbRq8OgB3ZSnQjnd2EVnQSqmPlUW4mpkL2AH3k86zJ/phmHBXhL8mA9wFvrGKV5mTep1fZYk7WR6HL0zsCCee1BAKy1jE3PTi1Q3YKyMxUB4m4brx8Zn96uJKTT/goC8tHbWDWwyJ5bHgaRvuoeQOstcuwunkOtluIyup5hkLvd/JN957mdkOE3P+ePFjOpitVFs1QatVsqXoFNib/BkxscyF2okQkHdGdnzkq4fQffp9MNEYo2SZeuJjCq4h53AQIDAQAB",
        "20113"=>"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs6opaE3gWEfUoDlx8BUmYH4xVEZ8YWS8rv3D3cDvbBVYyyOyCKBIzVp7+Do9e21H1RsGQX0zD8smG8724uwUka6OGMtUcsXC/G8eXJhXnrZ3XpH5AWts3Xxb25uNOIllP9LTVz2iFvTf1JkJSm6lXpjdWyebFU3ElKmLbvMyp1l5buVcAP3MKazoKCeF21CtgT9uWYAi1ZffVpsx/GRwKUPbEAAiVQQhLrXlc4bIGoLByVungGpsMLSweVbldvv58D92zYfGSJcv6+sRzX9AhpbGwJA/GHtgZ9aQlrdGCrhfis9pneD3XTVBONIsO4j76666KMqUCsqQ6FccjiCSywIDAQAB",
    ),

    "URL_ROUTER_ON"=>true,
    'URL_ROUTE_RULES'=>array(
        'downapp/uid/:uid'=> 'sver/downapk/index',
        'downapp/product/:product'=> 'sver/downapk/index',
    ),
    //女性用户注册发消息开关
    "GIRL_SWITCH_ON"=>true,
);

