// XMPP服务器BOSH地址  
var BOSH_SERVICE = 'http://47.75.189.126:7070/http-bind/';  
  
// XMPP连接  
var connection = null;  
  
// 当前状态是否连接  
var connected = false;  
  
// 当前登录的JID  
var jid = "";  
// 当前登录的uid 
var uid = "";  




// 连接状态改变的事件  
function onConnect(status) {  
    console.log(status)  
    if (status == Strophe.Status.CONNFAIL) {  
        alert("连接失败！");  
    } else if (status == Strophe.Status.AUTHFAIL) {  
        alert("登录失败！");  
    } else if (status == Strophe.Status.DISCONNECTED){  
        alert("连接断开！");  
        connected = false;  
    }else if (status == Strophe.Status.CONNECTED) {  
        //alert("连接成功，可以开始聊天了！");  
		mainView.router.loadPage('messages.html');
        connected = true;  
          
        // 当接收到<message>节，调用onMessage回调函数  
        connection.addHandler(onMessage, null, 'message', null, null, null);  
          
        // 首先要发送一个<presence>给服务器（initial presence）  
        connection.send($pres().tree());  
		
    }  
}  
  
// 接收到<message>  
function onMessage(msg) {
    // 解析出<message>的from、type属性，以及body子元素  
    var from = msg.getAttribute('from');  
    var type = msg.getAttribute('type');  
    var elems = msg.getElementsByTagName('body');  
  
    if (type == "chat" && elems.length > 0) {  
        var body = elems[0];  
		   var obj = Strophe.getText(body);
		   var newstr=obj.replace(new RegExp("&quot;","g"),'"'); 
		   //var json = eval( "(" + newstr + ")" );
		   alert(newstr);
		   var json =JSON.parse( newstr );
		   var content = json.messageBody.content;
		   var head = JSON.parse(json.user.head);
		   var tonickname = json.user.nickname;
		   var msgType = json.msgType;
		   
		   if(msgType=="text"){
			   var html = '<div class="message message-with-avatar message-received">';
				   html +='<div class="message-name">'+tonickname+'</div>';
				   html +='<div class="message-text">'+content+'</div>';
				   html +='<div style="background-image:url('+head.url+')" class="message-avatar"></div>';
			   html +='</div>';
			   $$(".messages").append(html);
		   }else if(msgType=="image"){
			   var mimage = json.messageBody.image;
			   var html = '<div class="message message-with-avatar message-received">';
				   html +='<div class="message-name">'+tonickname+'</div>';
				   html +='<div class="message-text"><img src="'+mimage.url+'"></div>';
				   html +='<div style="background-image:url('+head.url+')" class="message-avatar"></div>';
			   html +='</div>';
			   $$(".messages").append(html);
		   }
		   //alert(html);
		 
    }  
    return true;  
}  
$$(document).on('click', '#login', function (e) { 
	// 通过BOSH连接XMPP服务器 
    if(!connected) {
		var login_username = $$("#login_username").val()+"@jiaoyou/web";
		var login_password = $$("#login_password").val();		
            connection = new Strophe.Connection(BOSH_SERVICE);  
            connection.connect(login_username, login_password, onConnect);  
            jid = login_username;  
			var jidarr = jid.split("@");
			uid = jidarr[0];
     }  
});
$(document).ready(function() {  
  

      
    // 发送消息  
    $("#btn-send").click(function() {  
        if(connected) {  
            if($("#input-contacts").val() == '') {  
                alert("请输入联系人！");  
                return;  
            }  
			var msgbody = '{"user":{"uid":"'+uid+'","gender":1,"age":"","nickname":"男生\r","vipgrade":1,"vip":0,"gold":0,"head":"{\"gId\":0,\"id\":\"264901\",\"ltime\":0,\"seetype\":1,\"status\":1,\"thumbnaillarge\":\"http:\/\/10408.oss-cn-hongkong.aliyuncs.com\/data\/userdata\/photos\/42\/142\/383742\/717bfbb4ef0a56c2ab7b9bc617e9f285.jpg\",\"thumbnailsmall\":\"http:\/\/10408.oss-cn-hongkong.aliyuncs.com\/data\/userdata\/photos\/42\/142\/383742\/717bfbb4ef0a56c2ab7b9bc617e9f285.jpg\",\"url\":\"http:\/\/10408.oss-cn-hongkong.aliyuncs.com\/data\/userdata\/photos\/42\/142\/383742\/717bfbb4ef0a56c2ab7b9bc617e9f285.jpg\"}"},"id":1527665318097,"msgType":"text","messageBody":{"sendTime":1527665318,"content":"66"}}';
            // 创建一个<message>元素并发送  
            var msg = $msg({  
                to: $("#input-contacts").val(),   
                from: jid,   
                type: 'chat'  
            }).c("body", null, $("#input-msg").val());  
            connection.send(msg.tree());  
  
            $("#msg").append(jid + ":<br>" + $("#input-msg").val() + "<br>");  
            $("#input-msg").val('');  
        } else {  
            alert("请先登录！");  
        }  
    });  
});  