<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
ini_set("display_errors", "On");
header("Content-type: text/html; charset=utf-8"); 
error_reporting(E_ALL & ~E_NOTICE);
// 应用入口文件
// 检测PHP环境  
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

//错误码定义
define('ERRORCODE_200',200);//请求成功1
define('ERRORCODE_201',201);//操作失败
define('ERRORCODE_202',202);//登录过期，请重新登录
define('ERRORCODE_501',501);//服务器故障，
define('ERRORCODE_203',203);//服务器返回为空，
define('ERRORCODE_204',204);//已被封号，
// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG',true);
define('WR',dirname(__FILE__)); 
// 定义应用目录
define('APP_PATH','./Application/');
define('RUNTIME_PATH',   WR.'/userdata/Runtime/');
//记录日志目录
define('LOG_PATH',   WR.'/userdata/Log/');
if(strlen($_SERVER["PATH_INFO"])>25){
$pathinfo = explode("/", $_SERVER["PATH_INFO"]);
$pathurl = $pathinfo[count($pathinfo)-1];
$pathurl = decodeUrl($pathurl);
    if($pathurl){
        $_SERVER["PATH_INFO"] = "/".$pathurl;
    }
}
//composer的自动加载机制
require __DIR__.'/vendor/autoload.php';
// 引入ThinkPHP入口文件
require './ThinkPHP/ThinkPHP.php';
exit;


function decodeUrl($tex) {
    $key = substr($tex, 0, 2);
    $tex = substr($tex, 2);
    $verity_str = substr($tex, 0, 3);
    $tex = substr($tex, 3);
    if ($verity_str != substr(md5($tex), 0, 3)) {
        //完整性验证失败
        return false;
    }
    $tex = base64_decode($tex);
    $texlen = strlen($tex);
    $reslutstr = "";
    $rand_key=md5($key);
    for($i = 0; $i < $texlen; $i++) {
        $reslutstr.=$tex{$i} ^ $rand_key{$i % 32};
    }
    return $reslutstr;
}
// 亲^_^ 后面不需要任何代码了 就是如此简单
