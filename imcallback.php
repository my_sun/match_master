<?php
header("Content-type: text/html; charset=utf-8");
define('WR',dirname(__FILE__)); 

$config = require('./Application/Common/Conf/cache.config.php');
$dbconfig = require('./Application/Common/Conf/config.php');

$redis = new Redis();
$redis->connect($dbconfig['REDIS_HOST'], 6379); 
$redis->auth($dbconfig['REDIS_PASSWORD']); //设置密码
		
$payLogFile="userdata/hooks.txt";
$body = file_get_contents('php://input');
if(@$_GET["clear"]){
    file_put_contents($payLogFile, "");
}elseif($body){
    if(@$_REQUEST["CallbackCommand"]=="C2C.CallbackAfterSendMsg"){
		file_put_contents($payLogFile, $_REQUEST["CallbackCommand"].$body."\r\n", FILE_APPEND);
		$str = json_decode($body,true);
		PushMsg($str,$config);
	}
	$return = array(
		"ActionStatus"=>"OK",
		"ErrorInfo"=>"",
		"ErrorCode"=>0 // 0为回调成功，1为回调出错
	);
	echo json_encode($return);exit;
}elseif(@$_GET["uid"]){
			
			$touserinfo = getuser($_GET["uid"]);
			 $title = "來自test的新消息";
			 $content="新消息".date("Y-m-d H:i:s",time()+60*60*8);
			 
			 $ret = FCMPush($title,$content,$touserinfo["fcmtoken"],$config["FCM_API_KEY"][$touserinfo["product"]]);
			 if($ret==1){
				 echo $touserinfo["product"]."没有配置FCMAPI";
			 }elseif($ret==2){
				 echo "用户fcmtoken为空";
			 }else{
			     Dump($ret);exit;
			 }
}elseif(@$_GET["onlineuid"]){
	$touserinfo =getuser(@$_GET["onlineuid"]);
    $sdkappid=$config["IMSDKID"][$touserinfo["product"]];
	if($sdkappid){
		$result = tencent_onlinestate(array(@$_GET["onlineuid"]),$sdkappid);
		Dump($result);exit;
	}else{
		echo "该平台没有配置sdkappid";
	}
			exit;	 
			 
}else{
	$myfile = fopen($payLogFile, "r") ;
	$str = fgets($myfile);//fgets为读取一行，行本质是段落
	fclose($myfile);
	$str = json_decode(str_replace("C2C.CallbackAfterSendMsg","",$str),true);
	PushMsg($str,$config);
    Dump($str);
}
 


exit;
function getuser($uid){
	global $redis;
	$user_str = $redis->get('getuser-'.$uid);
	if(!$user_str){
		$user_str = file_get_contents("http://127.0.0.1:81/UserApi/getuser?uid=".$uid);
	}
	$user = json_decode($user_str,true);
	return $user;
}
function PushMsg($str,$config){
	$MsgBody = $str["MsgBody"];
	foreach($MsgBody as $msg){
		$msgdata = $msg["MsgContent"]["Data"];
		 $msgdata=substr($msgdata,2,strlen($msgdata));
		 $msgdata = json_decode(base64_decode($msgdata),true);
		 $touid = $msgdata["touid"];
		
		 if($touid){
			 $touserinfo = getuser($touid);
			 $user = $msgdata["msgbody"]["user"];
			 //判断用户是否在线
			 $sdkappid=$config["IMSDKID"][$touserinfo["product"]];
			 $result = tencent_onlinestate(array($touid),$sdkappid);
			if($result["State"]!="Online"){
				 $title = "來自".$user["nickname"]."的新消息";
				 $content=$msgdata["msgbody"]["messageBody"]["content"];
				 FCMPush($title,$content,$touserinfo["fcmtoken"],$config["FCM_API_KEY"][$touserinfo["product"]]);
			}
		 }
	}
}
/**
 google FCMPush
 */
function FCMPush($Title,$Content,$Token,$API_ACCESS_KEY) {
	
    if(!$API_ACCESS_KEY){
		return 1;
	}
	if(!$Token){
		return 2;
	}
	
    $msg = array(
        'body'  =>  $Content,
        'title' => $Title,
        'icon'  => 'ic_launcher',/*Default Icon*/
        /*'sound' => 'mySound'Default sound*/
    );
    
    $fields = array(
        'to' => $Token,
        'notification' => $msg);
    
    $headers = array(
        'Authorization: key=' . $API_ACCESS_KEY,
        'Content-Type: application/json'
    );
    
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch );
    curl_close( $ch );
	return $result;
}
function tencent_onlinestate($data,$sdkappid){
    
    
    #构造新消息
    $msg = array(
        'To_Account' => $data
        
    );
    #将消息序列化为json串
    $req_data = json_encode($msg);
    
    $service_name = "openim";
    $cmd_name = "querystate";//获取在线用户状态
    # 构建HTTP请求参数，具体格式请参考 REST API接口文档 (http://avc.qcloud.com/wiki/im/)(即时通信云-数据管理REST接口)
    #app基本信息
    $identifier=10000;//管理员
    $usersig = tencent_signature($identifier,$sdkappid);
    
    
    #开放IM https接口参数, 一般不需要修改
    $http_type = 'https://';
    $method = 'post';
    $im_yun_url = 'console.tim.qq.com';
    $version = 'v4';
    $contenttype = 'json';
    $apn = '0';
    $parameter =  "usersig=" . $usersig
    . "&identifier=" . $identifier
    . "&sdkappid=" . $sdkappid
    . "&contenttype=" . $contenttype;
    $url = $http_type . $im_yun_url . '/' . $version . '/' . $service_name . '/' .$cmd_name . '?' . $parameter;
    
    $ret = http_req('https', 'post', $url, $req_data);
    $ret = json_decode($ret,true);
   
    return $ret["QueryResult"];
}
function tencent_signature($uid,$sdkappid)
{
    $private_key_path=WR."/Public/key/private_key_".$sdkappid;
    //echo $private_key_path;
    # 这里需要写绝对路径，开发者根据自己的路径进行调整
    $command = '/var/www/signature'
        . ' ' . escapeshellarg($private_key_path)
        . ' ' . escapeshellarg($sdkappid)
        . ' ' . escapeshellarg($uid);
        //echo $command;
        $ret = exec($command, $out, $status);
        if ($status == -1)
        {
            return null;
        }
        //Dump($out);
        return $out[0];
}
/**
 * 向Rest服务器发送请求
 * @param string $http_type http类型,比如https
 * @param string $method 请求方式，比如POST
 * @param string $url 请求的url
 * @return string $data 请求的数据
 */
function http_req($http_type, $method, $url, $data)
{
    $ch = curl_init();
    if (strstr($http_type, 'https'))
    {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    }
    
    if ($method == 'post')
    {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    } else
    {
        $url = $url . '?' . $data;
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT,100000);//超时时间
    
    try
    {
        $ret=curl_exec($ch);
    }catch(Exception $e)
    {
        curl_close($ch);
        return json_encode(array('ret'=>0,'msg'=>'failure'));
    }
    curl_close($ch);
    return $ret;
}
function Dump($var){
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}
function object_array($array) {
    if(is_object($array)) {
        $array = (array)$array;
    } if(is_array($array)) {
        foreach($array as $key=>$value) {
            $array[$key] = object_array($value);
        }
    }
    return $array;
}
?>