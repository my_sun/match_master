/**
 * Created by Administrator on 2015/7/3.
 */

function trim (str)
{
    str = str.replace(/^\s+/, '');
    for (var i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}

function isPositiveInteger(str)
{
    var re = /^[1-9]+[0-9]*]*$/;
    if(re.test(str)){
        return true;
    }
    return false;
}

$(document).ready(function(){
    $('.payWay .line1 .get').click(function(){
        $(this).addClass('active').siblings().removeClass('active');
        $('.choose').eq($(this).index()-1).show().siblings('.choose').hide();
    })
    $('.payWay .line2 .get').click(function(){
        $(this).addClass('active').parent().siblings().find('.get').removeClass('active');
        $('.payWay .line2 .bank_ways').css('border','1px solid #ddd').find('option[class="default"]').attr('selected',"selected");
    })
    $('.payWay .line2 .bank_ways').click(function(){
        $(this).css("border","1px solid #ef555e");
        $('.payWay .line2 .get').removeClass('active');
    })
    $(".choose label b").not("#otherLable b").click(function(){
        $(".money").text($(this).data("price")+'NT');
		$("#paymoney").val($(this).data("price"));
    })
    $('.line2 label').click(function(){
        $(this).addClass('do').parent().siblings().find('label').removeClass('do');
    })
    //点击下一步按钮
    $("#next_btn").click(function(){
       // submitData();
    });
/*    $("#submit_btn").click(function(){
        submitMobileData();
    });*/


    $(".choose label").click(function(){
        if($("#other").hasClass("do")){
            $("#account_money").attr("disabled",false);
        }else{
            $("#account_money").attr("disabled",true);
        }
    });

    $("#account_money").keyup(function(){
        var priVal = "（自定义金额需大于1000元）";
        var pay_money = $("#account_money").val();
        $(".money").text($(this).val()+'元');
        var color = "#bf3b37";
        if(isNaN(pay_money)){
            $("#tips").css("color","red");
            $("#tips").html("（自定义金额必须为数字）");
        }else{
            $("#tips").css("color",color);
            if(parseInt(pay_money) >= 1000){
                $("#tips").html("（" + Math.floor(parseInt(pay_money)*8888/848 )+ " 钻石）");
            }else{
                $("#tips").html("（自定义金额需大于1000元）");
            }
        }

    });
});

/**
 * 校验支付数据
 */
function submitData(){
    var account = $('#account').val();

    if (!account || (account = trim(account)).length <= 0) {
        alert("未指定充值账号");
        $("#account").focus();
        return;
    }

    if (!isPositiveInteger(account)) {
        alert("指定充值账号不正确");
        $("#account").focus();
        return;
    }
    var buyType = $('.line1 .get');
    if(!buyType.hasClass('active')){
        alert("请选择购买类型")
        return;
    }
    var price = getValue();
    if (!price || (price = trim(price)).length <= 0) {
        alert("未指定充值金额");
        return;
    }

    if (!isPositiveInteger(price)) {
        alert("指定充值金额不正确");
        $("#account_money").focus();
        return;
    }
    if (isCunstomPay()) {
        if(price.length > 0 && parseInt(price) < 1000){
            alert("自定义充值金额需大于1000元");
            $("#account_money").focus();
            return;
        }
    }
    if (parseInt(price) > 100000) {
        alert("自定义充值金额不能大于100000元");
        $("#account_money").focus();
        return;
    }
    var netPayWay = getPayWay();

    if(!netPayWay || (netPayWay = trim(netPayWay)).length <= 0) {
        alert("未指支付方式");
        return;
    }

    var buy = $('.line1 .active').find('b').attr('class');
    console.log(buy)
    if(buy=="vip"){
        //alert('vip')//当购买vip时，执行当前作用域的函数
        if(netPayWay == "zfb"){
            $("#zfb_vip_account").val(account);
            $("#zfb_vip_price").val(price);
            $("#zfb_vip_from").submit();
        }else if(netPayWay == "cft"){
            $("#cft_vip_account").val(account);
            $("#cft_vip_price").val(price);
            $("#cft_vip_from").submit();
        }else if(netPayWay == "banks"){
            $("#banks_vip_account").val(account);
            $("#banks_vip_price").val(price);
            var bank_code =  getBankType();
            $("#banks_vip_type").val(bank_code);
            $("#banks_vip_from").submit();
        }else{
            alert("支付方式选择错误");
            return;
        }
    }else if (buy=="diamond"){
        //alert('dd')
        if(netPayWay == "zfb"){
            $("#zfb_account").val(account);
            $("#zfb_price").val(price);
            $("#zfb_from").submit();
        }else if(netPayWay == "cft"){
            $("#cft_account").val(account);
            $("#cft_price").val(price);
            $("#cft_from").submit();
        }else if(netPayWay == "banks"){
            $("#banks_account").val(account);
            $("#banks_price").val(price);
            var bank_code =  getBankType();
            $("#banks_type").val(bank_code);
            $("#banks_from").submit();
        }else{
            alert("支付方式选择错误");
            return;
        }
    }
}

/**
 * 校验支付方式
 */
function getPayWay(){
    var result = "";
    var payList = $(".line2 label");
    payList.each(function(){
        if($(this).hasClass("do")){
            result = $(this).parent().attr("class");
            return false;
        }
    })
    var payList2 = $('.bank_ways');
    if(payList2.val()!='其他方式'){
        result = payList2.parent().attr('class');
    }
    //alert(result)
    return result;
}

/**
 * 校验支付方式
 */
function getBankType(){
    var result = "";
    var payList = $(".bank_ways");
    payList.each(function(){
        if(payList.val()!='其他方式'){
            result = $(this).find("option:selected").attr('class');
            return false;
        }
    })
    return result;
}

function isCunstomPay(){
    var result = false;
    var payList = $(".choose label b");
    payList.each(function(){
        if($(this).hasClass("do")){
            result = $(this).attr("data-price");
            return false;
        }
    })
    if(result == "other"){
        result = true;
    }else{
        result = false;
    }
    return result;
}

function getValue(){
    var result = "";
    var bList = $(".choose label b");
    bList.each(function(){
        if($(this).hasClass("do")){
            result = $(this).attr("data-price");
            return false;
        }
    })
    if(result == "other"){
        result = $("#account_money").val();
    }
    return result;
}