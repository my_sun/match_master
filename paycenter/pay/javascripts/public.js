/**
 * Created by Administrator on 2015/7/22.
 */


$(function(){
    //脚部二维码
    $("#foot .floor2 .hov").hover(function(){
        $('.floor figure .t-d-code').show();
    },function(){
        $('.floor figure .t-d-code').hide();
    })

    //滚动条置顶
    var $h = $(window).height();
    $(document).scroll(function(){
        var scrolltop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
        if(scrolltop > $h){
            $(".scroll-top").addClass('active');
        }else{
            $(".scroll-top").removeClass('active');;
        }
    });
    $(".scroll-top").click(function(){
        $("html,body").animate({
            scrollTop:0
        },600);
    })

    //左侧导航
    $('.content .subNav li').each(function(i){
        $(this).click(function(){
            $('.content .cot-text').stop(true,true);
            $('.content .subNav li span').removeClass("active").eq(i).addClass("active");
            $('.content .cot-text').fadeOut().stop(true,true).eq(i).fadeIn();
        })
    })
    function getQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
    }
    var init = getQueryString("init");
    if(init == 2){
        $('.content .subNav li').eq(1).trigger("click");
    }else if (init == 3){
        $('.content .subNav li').eq(2).trigger("click");
    }

    //二维码
    $(".cot-text .hov").hover(function(){
        $(this).parent().next().show();
    },function(){
        $(this).parent().next().hide();
    })
    //企业轮播图
    /*$(".slide .figure li").each(function(){

    });
    var slideEl = $(".slide .figure li");
    var slideCotol = $(".slide .control li");
    function autoSlide(){
        var index = slideEl.index($(".slide .figure li.active"));
        if(index ==  slideEl.length -1){
            index = 0;
        }else{
            index++;
        }
        slideEl.removeClass("active").fadeOut().eq(index).fadeIn().addClass("active");
        slideCotol.removeClass("active").eq(index).addClass("active");
    }
    var slideTIme = setInterval(autoSlide,3000);
    slideCotol.each(function(i){
        $(this).on("click",function(){
            clearInterval(slideTIme)
            slideEl.removeClass("active").fadeOut().eq(i).fadeIn().addClass("active");
            slideCotol.removeClass("active").eq(i).addClass("active");

        })
    })*/

    //游戏中心
    $('#game .choose li').each(function(i){
        $(this).click(function(){
            $('#game .cot-text').stop(true,true);
            $(this).addClass("active").siblings().removeClass("active");
            $('#game .cot-text').fadeOut().stop(true,true).eq(i).fadeIn();
        })
    })
    //充值中心
    $('#pay .choose b').each(function(i){
        $(this).click(function(){
            $('#pay .choose b').removeClass('do').eq(i).addClass('do');
        })
    })
    $('#pay .netpay_ways b').each(function(i){
        $(this).click(function(){
            $('#pay .netpay_ways b').removeClass('do').eq(i).addClass('do');
        })
    })

    //footer
    $('.IOS-pic').add('.Android-pic').hover(function(){
        $(this).find('.pic-bg').show();
    },function(){
        $(this).find('.pic-bg').hide();
    })
    $('.Android-pic').hover(function(){
        $(this).find('.list').show();
    },function(){
        $(this).find('.list').hide();
    })
})
