//弹出发自定义消息对话框

function showEditCustomMsgDialog() {
    $('#ecm_form')[0].reset();
    $('#edit_custom_msg_dialog').modal('show');
}
//发送自定义消息

function sendCustomMsg(type='text',url='',id='',gifttype=false) {
    if (!selToID) {
        alert("您还没有好友或群组，暂不能聊天");
        return;
    }
    if (type === 'text') {
        var content = url;
        var jsondata = {
            "touid": touid,
            "msgbody": {
                "msgType": "text",
                "messageBody": {
                    "sendTime": Date.parse(new Date()),
                    "content": content,
                },
                "user": user
            },
        };
    }
    if (type === 'hello') {
        var content = 'Hi！';
        var jsondata = {
            "touid": selToID,
            "msgbody": {
                "msgType": "hello",
                "messageBody": {
                    "sendTime": Date.parse(new Date()),
                    "content": content,
                },
                "user": user
            },
        };
    }
    if(type === 'image'){
        var content = $("#send_msg_text").val();
        var jsondata = {
            "touid": touid,
            "msgbody": {
                "msgType": "image",
                "messageBody": {
                    "sendTime": Date.parse(new Date()),
                    "image":{
                        'url':url,
                        'thumbnaillarge':url,
                        'thumbnailsmall':url,
                        'status':1,
                        'seetype':1,
                    },
                    "content": '【图片】',
                },
                "user": user
            }
        };
    }
    if(type === 'gift'){
        var content = $("#send_msg_text").val();
        var jsondata = {
            "touid": touid,
            "msgbody": {
                'id':id,
                "msgType": "gift",
                "messageBody": {
                    "sendTime": Date.parse(new Date()),
                    "gift":{
                        'id':url.id,
                        'url':url.url,
                        'title':url.title,
                        'price':url.price,
                        'count':1,
                        'demand':gifttype,
                    },
                    "content": '【礼物】',
                },
                "user": user
            }
        };
    }

        var base = new Base64();
        var data="LM"+base.encode(JSON.stringify(jsondata));




    var msgLen = webim.Tool.getStrBytes(data);


    var maxLen, errInfo;
    if (selType == webim.SESSION_TYPE.C2C) {
        maxLen = webim.MSG_MAX_LENGTH.C2C;
        errInfo = "消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)";
    } else {
        maxLen = webim.MSG_MAX_LENGTH.GROUP;
        errInfo = "消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)";
    }
    if (msgLen > maxLen) {
        alert(errInfo);
        return;
    }

    if (!selSess) {
        selSess = new webim.Session(selType, selToID, selToID, friendHeadUrl, Math.round(new Date().getTime() / 1000));
    }
    if(type === 'hello'){
        selSess = new webim.Session(selType, selToID, selToID, friendHeadUrl, Math.round(new Date().getTime() / 1000));
    }
    var msg = new webim.Msg(selSess, true, -1, -1, -1, loginInfo.identifier, 0, loginInfo.identifierNick);
    var custom_obj = new webim.Msg.Elem.Custom(data);
    msg.addCustom(custom_obj);
    //调用发送消息接口
    msg.sending = 1;
    if(type != 'hello'){
        addMsg(msg);
        TalkWords.value ='';
    }
    webim.sendMsg(msg, function(resp) {

        $("#id_" + msg.random).find(".spinner").remove();
        // if (selType == webim.SESSION_TYPE.C2C) {
        //     //私聊时，在聊天窗口手动添加一条发的消息，群聊时，长轮询接口会返回自己发的消息
        //     addMsg(msg);
        // }
        $('#edit_custom_msg_dialog').modal('hide');
    }, function(err) {
        alert(err.ErrorInfo);
    });
}