<?php
define('WR',dirname(__FILE__)); 
/**
 * Created by PhpStorm.
 * User: yuyi
 * Date: 16/12/24
 * Time: 07:01
 */
include WR."/Doc.php";
include WR."/WClass.php";
include WR."/WDoc.php";
include WR."/WFunction.php";
include WR."/WFile.php";
include WR."/wing-file-system/WDir.php";
include WR."/wing-file-system/WFile.php";
include WR."/wing-html/Html.php";
function get_millisecond()
{
    $time = explode(' ', microtime());
    return (float)sprintf('%.0f', (floatval($time[0]) + floatval($time[1])) * 1000);
}
$apiPath = "../../Application/Api/Controller";
$apiFile = "doc/index.html";
if(filemtime ($apiPath)<filemtime ($apiFile)){
    //echo 1;exit;
    echo file_get_contents($apiFile);exit;
}
try {
    $start_time = get_millisecond();

    $app = new \Wing\Doc\Doc(
        $apiPath,
        "doc"
    );
    $app->addExcludePath(array(
        "vendor/*", "Config/*", "config/*",
        "public/*", "database/*", "tests/*"
    ));
    $app->addExcludeFileName(
        array(
            "BaseController.class.php",
            "AdmController.class.php",
            "CaijiController.class.php",
            "OnlineController.class.php",
            "AutomsgController.class.php",
            "MoneyController.class.php",
            "PublicController.class.php",
            "index.html"
        )
    );
    $app->run();

    //echo "完成，耗时" . (get_millisecond() - $start_time) . "毫秒\r\n";
} catch(\Exception $e) {
    echo $e->getMessage();
}