<?php
class AutomsgController extends BaseController {
	public function __construct(){
	    if(!$this->uid){
		  parent::__construct();
	    }
	}
	//获取用户
	public function getrounduser($country1="TW",$product='10001'){
	    $countryarr = array(
	        "TW"=>array("HK","TW","JP","MO","MY","SG","TH","VN","CN")
	    );
	    $country = "US";
	    foreach($countryarr as $k=>$v){
	        if(in_array($country1, $v)){
	            $country=$k;
	            break;
	        }
	    }
	    
	    $where = array();
	    $where['user_type'] = 2;
	    $where['gender'] = 2;//显示异性
	    $where['product'] = $product;//product
	    $where['country'] = $country;//显示异性
	    $cachename1 = "getUsers_".md5(json_encode($where));
	    $cache1=S($cachename1);//设置缓存标示
	   // $cache1=0;
	    // 判断是否有这个查询缓存
	    if(!$cache1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
	        $User_baseM = new UserBaseModel();
	        $cache = $User_baseM->getListfield($where,1,100);
	        
	        $uids = array();
	        foreach($cache as $v){
	            $this->get_user($v["uid"]);
	            $uids[] = $v["uid"];
	        }
	        $cache1=$uids;
	        S($cachename1,$cache1,60*60*2); //设置缓存的生存时间
	    }
	    $uids=$cache1;
	    $uid = $uids[rand(0,count($uids)-1)];
	    return $uid;
	}
	protected function _sendsysmsg($uid,$touid,$sysmsg){
	    $uid_str = $uid.$touid;
	    $redis = $this->redisconn();
	    $msglistkey = "automsglist_".$uid_str;
	    $content = $sysmsg["value"];
	    //$content = "你还得搜啊大家搜爱的还是";
	    preg_match_all('/[a-z]*-[1-9,1-9]*/', $content, $matches);
	    $matches = $matches[0];
	    if(empty($matches)){
    	    // echo $msglistkey;
    	    //信箱所需字段信息
    	    $Msginfo = array();
    	    $Msginfo['type'] = "text";
    	    $Msginfo['sendtime'] = time();
    	    $Msginfo['language'] = "zh-tw";
    	    $Msginfo['country'] ="TW";
    	    $Msginfo['uid'] = $uid;
    	    $Msginfo['touid'] = $touid;
    	    $Msginfo['content'] = $content;
    	    $Msginfo['box_id'] = $uid_str;
    	    $Msginfo['sysmsgid'] = $sysmsg["id"];
    	    $Msginfo['isreply'] = 0;
    	    $redis->listPush($msglistkey, json_encode($Msginfo));
    	    $redis->setListKeyExpire($msglistkey, 60*60*24);
    	    
    	    $sendmsg = array();
    	    $sendmsg["to"]=$uid;
    	    $sendmsg["from"]=$touid;
    	    $sendmsg["msgtype"] = "text";
    	    $sendmsg["messageBody"] = array(
    	        "content"=>$content,
    	        "sendTime"=>time(),
    	    );
    	    //加入长时间不回复监听 start
    	    $url = "http://127.0.0.1:81/automsg/notreply?uid=".$uid."&touid=".$touid;
    	    $argv = array();
    	    $rtime = rand(3*60,8*60);
    	    //$rtime=1;
    	    $argv['url'] = "http://127.0.0.1:212/?type=notrepy&time=".$rtime."&url=".base64_encode($url);
    	    $argv['time'] = 1;
    	    $argv = base64_encode(json_encode($argv));
    	    $command = "/usr/bin/php ".WR."/openurl.php ".$argv." > /dev/null &";
    	    system($command);
    	    //加入长时间不回复监听 end
    	    return $this->sendxmppmsg($sendmsg);
	    }else{
	        /*{
	            "id":"",
	            "msgType":"",//text文本消息，voice语音消息，video视频消息，location位置信息，gift礼物消息,image图片
	            "messageBody":{
	                "sendTime":0,//消息时间
	                "content":"",//文本时
	                "voice":{"url":"","ltime":0},//语音时
	                "video":{"url":"","imageurl":"","ltime":0},//小视频时
	                "location":{"longitude":"", latitude:""},//定位时
	                "gift":{礼物对象  "count":0,"demand":true}//礼物时  count数量， demand true 索要  false发送
	                "image":{图片对象}
	        },
	        "user":{id,头像，昵称}//uid gender age nickname vipgrade vip gold head
	        }*/
	        $one = explode("-", $matches[0]);
	        if($one[0]=="audio"){
	            //语音
	            $type="voice";
	            $sendmsg["messageBody"] = array(
	                "content"=>"",
	                "sendTime"=>time()
	            );
	            $voice["url"] = C ("IMAGEURL"). "/sysmsg/audio/" . $one[1] . ".mp3";
	            $voice["ltime"] =  rand(1,4);
	            $sendmsg["messageBody"]["voice"] = $voice;
	        }elseif($one[0]=="image"){
	            //图片
	            $type="image";
	            $sendmsg["messageBody"] = array(
	                "content"=>"",
	                "sendTime"=>time()
	            );
	            $image = array();
	            $image['id']=1;
	            $image['url']=C ("IMAGEURL"). "/sysmsg/image/" . $one[1] . ".jpg";//原图url
	            $image['thumbnaillarge']=$image['url'];//大图url
	            $image['thumbnailsmall']=$image['url'];//小图url
	            $image['status']=1;//状态1审核通过;2正在审核3审核未通过已删除
	            $image['seetype'] =1; //可见级别(1所有用户可见,2会员可见)	
	            $sendmsg["messageBody"]["image"] = $image;
	        }else{
	            $type="text";
	            
	            $sendmsg["messageBody"] = array(
	                "content"=>$content,
	                "sendTime"=>time(),
	            );
	        }
	        $Msginfo = array();
	        $Msginfo['type'] = $type;
	        $Msginfo['sendtime'] = time();
	        $Msginfo['language'] = "zh-tw";
	        $Msginfo['country'] ="TW";
	        $Msginfo['uid'] = $uid;
	        $Msginfo['touid'] = $touid;
	        $Msginfo['content'] = $content;
	        $Msginfo['box_id'] = $uid_str;
	        $Msginfo['sysmsgid'] = $sysmsg["id"];
	        $Msginfo['isreply'] = 0;
	        $redis->listPush($msglistkey, json_encode($Msginfo));
	        $redis->setListKeyExpire($msglistkey, 60*60*24);
	        $sendmsg["msgtype"] = $type;
	        $sendmsg["to"]=$uid;
	        $sendmsg["from"]=$touid;
	        
	        $this->sendxmppmsg($sendmsg);
	        unset($matches[0]);
	        if(!empty($matches)){
    	        foreach ($matches as $v){
    	            $ptime=rand(7,15);
    	            $one = explode("-", $v);
    	            if($one[0]=="audio"){
    	                $type="voice";
    	                $sendmsg["messageBody"] = array(
    	                    "content"=>"语音信息",
    	                    "sendTime"=>time()+$ptime
    	                );
    	                $voice["url"] = C ("IMAGEURL"). "/sysmsg/audio/" . $one[1] . ".mp3";
    	                $voice["ltime"] =  rand(1,4);
    	                $sendmsg["messageBody"]["voice"] = $voice;
    	            }elseif($one[0]=="image"){
    	                //图片
    	                $type="image";
    	                $sendmsg["messageBody"] = array(
    	                    "content"=>"图片信息",
    	                    "sendTime"=>time()+$ptime
    	                );
    	                $image = array();
    	                $image['id']=1;
    	                $image['url']=C ("IMAGEURL"). "/sysmsg/image/" . $one[1] . ".jpg";//原图url
    	                $image['thumbnaillarge']=$image['url'];//大图url
    	                $image['thumbnailsmall']=$image['url'];//小图url
    	                $image['status']=1;//状态1审核通过;2正在审核3审核未通过已删除
    	                $image['seetype'] =1; //可见级别(1所有用户可见,2会员可见)
    	                $sendmsg["messageBody"]["image"] = $image;
    	            }else{
    	                $type="text";
    	                $sendmsg["messageBody"] = array(
    	                    "content"=>$content,
    	                    "sendTime"=>time()+$ptime,
    	                );
    	            }
    	            $sendmsg["msgtype"] = $type;
    	            $sendmsg["to"]=$uid;
    	            $sendmsg["from"]=$touid;
    	            $argv = array();
    	            $argv['url'] = "http://127.0.0.1:81/automsg/publicsendsysmsg/?msg=".base64_encode(json_encode($sendmsg));
    	            $argv['time'] = $ptime;
    	            $argv = base64_encode(json_encode($argv));
    	            $command = "/usr/bin/php ".WR."/openurl.php ".$argv." > /dev/null &";
    	            system($command);  
    	            
    	            
    	        }
	        }
	        
	    }
	   //Dump($sysmsg);
	   //Dump($Msginfo);
	}
	public function testsendmsg($to,$from,$content="test"){
	    $sendmsg = array();
	    $sendmsg["touid"]=$to;
	    $sendmsg["msgid"]=substr(md5($this->getMillisecond()), 8, 16);//substr(md5(time()), 8, 16)
	    $body = array();
	    $body["id"]=$sendmsg["msgid"];
	    $body["msgType"]="text";
	    $messageBody =  array(
	        "content"=>$content,
	        "sendTime"=>time(),
	    );
	    $body["messageBody"]=$messageBody;
	    $body["user"]=$this->get_diy_user_field($from,"uid|gender|age|nickname|vipgrade|vip|gold|head");;
	    $sendmsg["msgbody"]=$body;
	    echo LM_sendmsg($sendmsg);
	}
	
	public function publicsendsysmsg($msg){
	    $sendmsg = object_array(json_decode(base64_decode($msg)));
	    return $this->sendxmppmsg($sendmsg);
	}
	public function firstsend($uid,$touid){
	    $userinfo = $this->get_user($uid);
	    $languagecode = $this->get_languagecode();
	    $langs = $userinfo["language"];
	    if(!in_array($langs, $languagecode)||$langs==""){
	        $langs = "zh-tw";
	    }
	    $msg =  $this->get_rand_msg($langs);
	    if($msg){
	       $this->_sendsysmsg($uid,$touid,$msg);
	    }
	}
	public function getMillisecond() {
	    list($t1, $t2) = explode(' ', microtime());
	    return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
	}
	
	/*
	 *
	 * 生成信息并发送
	 */
	public function rundmsg($uid,$touid=0){
	    
	    $redis = $this->redisconn();
	    $UserInfo = $this->get_user($uid);//用户
	    if($UserInfo['gender']==2||$UserInfo['product']!="20110"){
	        return 0;
	    }
	    if($touid==0){
	        
	        $touid = $this->getrounduser($UserInfo["country"]);
	    }
	    
	    //$ToUserInfo = $this->get_user($to_uid);//caiji
	    //查询是第几次发消息
	    $uid_str = $uid.$touid;
	    $msglistkey = "automsglist_".$uid_str;
	    
	    if(!$redis->exists($msglistkey)){
	        $recmsgcountkey = "recmsgcount_".$uid;
	        if($redis->exists($recmsgcountkey)){
	            $recmsgcount = $redis->get($recmsgcountkey)+1;
	        }else{
	            $recmsgcount = 1;
	        }
	        $redis->set($recmsgcountkey,$recmsgcount,0,0,60*60*24);
	         $ptime=rand(1*60,2*60);
	         //$ptime=5;
	         $time = $ptime;
	         $argv = array();
	         $argv['url'] = "http://127.0.0.1:81/automsg/firstsend/?uid=".$uid."&touid=".$touid;
	         $argv['time'] = $time;
	         $argv = base64_encode(json_encode($argv));
	         $command = "/usr/bin/php ".WR."/openurl.php ".$argv." > /dev/null &";
	         system($command);  
	         
	       //echo $this->firstsend($uid,$touid);
	        
	        return 1;
	        
	    }
	    else
	    {
	       
	        
	        $msginfo = object_array(json_decode($redis->listGet($msglistkey)));
	        
	        //给用户回信
	        if($msginfo['sysmsgid']&&$msginfo['isreply']==0)
	        {
	            
	            $sysmsg_ret = $this->get_cache_onesys_msg($msginfo['sysmsgid']);
	           
	            /*** 获取1条随机配聊信息 ***/
	            $levle = $sysmsg_ret['levle']+1;
	            if($levle>3){
	                $redisStr = "pmsg_".$uid."_".$touid;
	                if($redis->exists($redisStr)){
	                    return 2;
	                }
	                //登录保存uid
	                $OnlineCon = new OnlineController();
	                $luids = $OnlineCon->getRandUserOnline($UserInfo['product'],3,2);
	                if($luids){
	                    $redis->set($redisStr,$luids,0,0,60*60*24);
	                    $data = $this->Api_recive_date;
	                    $platforminfo = $this->platforminfo;
	                    $myuid = $this->uid;
	                    $MyUserInfo = $this->UserInfo;
	                    $msgtype = $data['msgtype'] ? $data['msgtype'] : "text";//发信类型:text文本消息，voice语音消息，video视频消息，location位置信息，gift礼物消息,image图片
	                    $content = $data['content'] ? $data['content'] : "";//发信内容//文本时
	                    $ToUserInfo = $this->get_user ($touid);//收信人用户信息
	                    $contentint = $data['contentint'] ? $data['contentint'] : "0";//礼物数量、语音、红包金币数量或视频时长
	                    $contentbody = $data['contentbody'] ? $data['contentbody'] : "";//礼物id、语音url或视频url
	                    $contentsub = $data['contentsub'] ? $data['contentsub'] : "";//视频第一帧图片url
	                    
	                    $to = $luids;
	                    $from =$uid;
	                    $sendmsg = array();
	                    $sendmsg["touid"]=$to;
	                    $sendmsg["msgid"]=substr(md5($this->getMillisecond()), 8, 16);//substr(md5(time()), 8, 16)
	                    $body = array();
	                    $body["id"]=$sendmsg["msgid"];
	                    $body["msgType"]=$msgtype;
	                    $messageBody = array(
	                        "content"=>$content,
	                        "sendTime"=>time(),
	                    );;
	                    $body["messageBody"]=$messageBody;
	                    $body["user"]=$this->get_diy_user_field($from,"uid|gender|age|nickname|vipgrade|vip|gold|head");
	                    $body["puser"]=$this->get_diy_user_field($touid,"uid|gender|age|nickname|vipgrade|vip|gold|head");
	                    $sendmsg["msgbody"]=$body;
	                    LM_sendmsg($sendmsg);
	                    return 3;
	                }
	                return 1;
	            }
	            if($UserInfo['vip']>0)
	            {
	                //10%如果是会员10%几率回复策略
	                /* $send_msg_rand = rand(1,10);
	                if($send_msg_rand>3){
	                    return 1;
	                } */
	            }
	             $ptime=rand(7,15);
	            //$ptime=1;
	            $argv = array();
	            $argv['url'] = "http://127.0.0.1:81/automsg/reply/?uid=".$uid."&touid=".$touid;
	            $argv['time'] = $ptime;
	            $argv = base64_encode(json_encode($argv));
	            $command = "/usr/bin/php ".WR."/openurl.php ".$argv." > /dev/null &";
	            system($command);   
	            //$this->reply($uid,$touid);
	            
	            
	            return 1;
	            //推送数据end
	            
	        }
	        
	    }
	    
	    return 0;
	}

	public function notreply($uid,$touid){
	    $userinfo = $this->get_user($uid);
	    $redis = $this->redisconn();
	    $uid_str = $uid.$touid;
	    $msglistkey = "automsglist_".$uid_str;
	    $msgisreplykey = "msgisreply_".$uid_str;
	    $msgisreply = $redis->get($msgisreplykey);//1未回复2已回复
	    if($msgisreply==1){
	        $where = array();
	        $where["levle"]=1;
	        $where["type"]=5;
	        $where['subtype']=1;
	        $where['lang']=$userinfo["language"];
	        $res = $this->get_cache_sys_msg($where);
	        if($res){
	            $msg = $res[array_rand($res,1)];
	            if(!empty($msg)){
	                $tmpmsg = explode('|',$msg["value"]); 
	                
	                
	                 if(count($tmpmsg)>1){
	                     $msg_content = $tmpmsg[0];
	                    unset($tmpmsg[0]);
	                    foreach($tmpmsg as $tv){
	                        if($tv){
    	                        $lenght=ceil(strlen($tv)/2)+4;
    	                        $ptime=$lenght;
    	                        $type="text";
    	                        $sendmsg["messageBody"] = array(
    	                            "content"=>$tv,
    	                            "sendTime"=>time()+$ptime,
        	                        );
        	                    $sendmsg["msgtype"] = $type;
        	                    $sendmsg["to"]=$uid;
        	                    $sendmsg["from"]=$touid;
        	                    $argv = array();
        	                    $argv['url'] = "http://127.0.0.1:81/automsg/publicsendsysmsg/?msg=".base64_encode(json_encode($sendmsg));
        	                    $argv['time'] = $ptime;
        	                    $argv = base64_encode(json_encode($argv));
        	                    $command = "/usr/bin/php ".WR."/openurl.php ".$argv." > /dev/null &";
        	                    system($command);  
	                        }
	                    }
	                }else{
	                    $msg_content = $msg["value"];
	                } 
	                $sendmsg = array();
	                $sendmsg["to"]=$uid;
	                $sendmsg["from"]=$touid;
	                $sendmsg["msgtype"] = "text";
	                $sendmsg["messageBody"] = array(
	                    "content"=>$msg_content,
	                    "sendTime"=>time(),
	                );
	                echo 5;
	                return $this->sendxmppmsg($sendmsg);
	            }
	            echo 3;
	        }
	        echo 4;
	    }else{
	        echo 2;
	    }
	    return 1;
	}
	//给用户回信
	public function reply($uid,$touid){
	    $UserInfo = $this->get_user($uid);//用户
	    $ToUserInfo = $this->get_user($touid);//caiji
	    $redis = $this->redisconn();
	    $uid_str = $uid.$touid;
	    $msglistkey = "automsglist_".$uid_str;
	    $msgisreplykey = "msgisreply_".$uid_str;
	    //$msglist = $redis->lRange($msglistkey,0, -1);
	    
	    $msg = object_array(json_decode($redis->listGet($msglistkey)));
	    
	    if($msg['uid']!=$uid)
	    {
	        
	        return 1;
	    }else
	    {
	        $msgisreply = $redis->get($msgisreplykey);//1未回复2已回复
	        if($msg['sysmsgid']&&$msgisreply==2)
	        {
	            
	            //如果不是第一次，查询消息下级策略
	            $sysMsg_arr = array();
	            $sysmsg_ret = $this->get_cache_onesys_msg($msg['sysmsgid']);
	            
	           
	            /*** 获取1条随机配聊信息 ***/
	            $levle = $sysmsg_ret['levle']+1;
	            $type = $sysmsg_ret['type'];
	            $name_md5 = $sysmsg_ret['name_md5'];
	            $msgkey=$sysmsg_ret['msgkey'];
	            $where = array();
	            $where["levle"]=$levle;
	            $where["type"]=$type;
	            $where["name_md5"]=$name_md5;
	            $where["lang"]=$sysmsg_ret['lang'];
	            $where["msgkey"]=array('like',$msgkey.'.%');
	            
	            $res = $this->get_cache_sys_msg($where);
	            $sysmsg = $res[array_rand($res,1)];
	            
	            //判断配聊信息是否用完，决定是否继续回复
	            if(!$sysmsg["id"]){
	               
	                return 1;
	                exit;
	            }
	            
	            
	            //如果用户已经收到同样的信件就不回复
	            //判断消息是否重复 start
	            $usersysmsgkey = "usersysmsgkey_".$uid;
	            //$this->redis->delete($usersysmsgkey);
	            
	            if($redis->exists($usersysmsgkey)){
	                $usersysmsgids = $redis->get($usersysmsgkey);
	                $usersysmsgids = explode(",",$usersysmsgids);
	                if(in_array($sysmsg['id'], $usersysmsgids)){
	                    echo 0;
	                    return 1;
	                    exit;
	                }else{
	                    $usersysmsgids[] = $sysmsg['id'];
	                    $redis->set($usersysmsgkey,implode(",",$usersysmsgids),0,0,60*60*48);
	                }
	            }else{
	                $usersysmsgids[] = $sysmsg['id'];
	                $redis->set($usersysmsgkey,implode(",",$usersysmsgids),0,0,60*60*48);
	            }
	            
	            //判断消息是否重复 end
	            //判断配聊信息是否用完，决定是否继续回复
	            if($sysmsg){
	                $this->_sendsysmsg($uid,$touid,$sysmsg);
	            }
	            $redis->set($msgisreplykey,1,0,0,60*60);//1未回复2已回复
	        }else
	        {
	            echo 1;
	        }
	    }
	    return 1;
	    exit;
	}
	public function get_rand_msg($lang="zh-tw",$product="10001"){
	    //CHCHEPATH_AUTOMSG
	    $SysMsgM = new SysMsgModel();
	    if($this->get_probability(10)&&$lang=="zh-tw"){
	       $subtype = 2;
	    }else{
	       $subtype = 1;
	    }
	   
	    if($lang=="zh-tw"){
    	    /* 节假日判断 */
    	    $dateNow = date('Ymd', time());
    	    //$dateNow = "20180116";
    	    $map = array();
    	    $map['starttime'] = array("ELT",$dateNow);
    	    $map['endtime'] = array("EGT",$dateNow);
    	    $map['levle']=1;
    	    $map['type']=2;//节假日
    	    $map['subtype']=$subtype;
    	    $cache_name2 = md5(json_encode($map));
    	    $res = S($cache_name2);
    	    if(!$res){
    	        $res = $SysMsgM->getList($map);
    	        $res = $res ? $res : 1;
        	   S($cache_name2,$res,60*60*24);
    	    }
    	    if($res&&is_array($res)){
    	        $msg = $res[array_rand($res,1)];
    	        if(!empty($msg)){
    	            return $msg;
    	        }
    	    }
	    }
	    /*
	     * 周末判断
	     */
	    $w=date('w');
	    $w=1;
	    if($w==0||$w==6){
    	    $map = array();
    	    $levle=1;
    	    $type=6;//周末
    	    $where = array();
    	    $where["levle"]=$levle;
    	    $where["type"]=$type;
    	    $where["lang"]=$lang;
    	    $where['subtype']=$subtype;
    	    $res = $this->get_cache_sys_msg($where);
    	    if($res){
    	        $msg = $res[array_rand($res,1)];
    	        if(!empty($msg)){
    	            return $msg;
    	        }
    	    }
	    }
	    
	    if($lang=="zh-tw"){
    	    /*
    	     * 时间段
    	     */
    	    if($this->get_probability(20)){
        	    $H=date('H');
        	    //$H=2;
        	    $map = array();
        	    $map['starttime'] = array("ELT",$H);
        	    $map['endtime'] = array("EGT",$H);
        	    $map['levle']=1;
        	    $map['type']=3;//节假日
        	    $map['subtype']=$subtype;
        	    $res = $this->get_cache_sys_msg($map);
        	    if($res){
        	        $msg = $res[array_rand($res,1)];
        	        if(!empty($msg)){
        	            return $msg;
        	        }
        	    }
    	    }
	    }
    	    /*
    	     * 通用策略
    	     */
	        $where = array();
	        $where['levle']=1;
	        $where['type']=1;
	        $where['subtype']=$subtype;
	        $where["lang"]=$lang;
	        $res = $this->get_cache_sys_msg($where);
	        if($res){
	            $msg = $res[array_rand($res,1)];
	            if(!empty($msg)){
	                return $msg;
	            }
	        }  
	}	
	/*
	 * 发送xmpp消息
	 * {
	 *  "id":"",
        "msgType":"",//text文本消息，voice语音消息，video视频消息，location位置信息，gift礼物消息,image图片
        "messageBody":{
            "sendTime":0,//消息时间
            "content":"",//文本时
            "voice":{"url":"","ltime":0},//语音时
            "video":{"url":"","imageurl":"","ltime":0},//小视频时
            "location":{"longitude":"", latitude:""},//定位时
            "gift":{礼物对象  "count":0,"demand":true}//礼物时  count数量， demand true 索要  false发送
            "image":{图片对象}
        },
        "user":{id,头像，昵称}//uid gender age nickname vipgrade vip gold head
      }
	 */
	public function sendxmppmsg($argc=false){
	    $argc = $argc ? $argc : $_GET;
	    $to = $argc["to"] ? $argc["to"] : "382511";
	    $from = $argc["from"] ? $argc["from"] : "381903";
	    $sendmsg = array();
	    $sendmsg["touid"]=$to;
	    $sendmsg["msgid"]=substr(md5($this->getMillisecond()), 8, 16);//substr(md5(time()), 8, 16)
	    $body = array();
	    $body["id"]=$sendmsg["msgid"];
	    $body["msgType"]=$argc["msgtype"];
	    $messageBody = $argc["messageBody"];
	    $body["messageBody"]=$messageBody;
	    $body["user"]=$this->get_diy_user_field($from,"uid|gender|age|nickname|vipgrade|vip|gold|head");
	    $sendmsg["msgbody"]=$body;
	    
	    //聊天记录写入本地
	    $data =array();
	    $data["uid"]=$from;
	    $data["touid"]=$to;
	    $data["msgtype"] = $argc["msgtype"] ? $argc["msgtype"] : "text";//发信类型:text文本消息，voice语音消息，video视频消息，location位置信息，gift礼物消息,image图片
	    $data["content"] = $messageBody['content'] ? $messageBody['content'] : "";//发信内容//文本时
	    $data["contentint"] = $data['contentint'] ? $data['contentint'] : "0";//礼物数量、语音、红包金币数量或视频时长
	    $data["contentbody"] = $data['contentbody'] ? $data['contentbody'] : "";//礼物id、语音url或视频url
	    $data["contentsub"] = $data['contentsub'] ? $data['contentsub'] : "";//视频第一帧图片url
	    $this->_writemsg($data);
	    return LM_sendmsg($sendmsg);
	}
	/*
	 * 概率生成
	 */
	protected function get_probability($probability=20){
	    $rands = rand(1,100);
	    if($rands<=$probability){
	        return true;
	    }else{
	        return false;
	    }
	}
	public function updatetime(){
	    //CHCHEPATH_AUTOMSG
	    exit;
	    ini_set('max_execution_time', '1000000');
	    $SysMsgM = new SysMsgModel();
	    $map["type"]=array('in',array("2","3"));
	    //$map["type"]=3;
	    $temp = $SysMsgM->getList($map);
	    foreach ($temp as $v){
	        $arr = explode("-", $v["name"]);
	        $up = array();
	        $up["starttime"]=$arr[0];
	        $up["endtime"]=$arr[1];
	        $SysMsgM->updateOne(array("id"=>$v["id"]),$up);
	    }
	    echo 1;
	    //Dump($arr);
	}
	
	/*
	 *
	 * get_sys_msg
	 */
	public function get_sys_msg($sys_msg_id,$reset='0'){
	    $path = $this->get_userpath($sys_msg_id, 'cache/sysmsg/');
	    $cache_name = 'sysmsginfo_'.$sys_msg_id;
	    if(F($cache_name,'',$path) && $reset == 0){
	        $sysmsg_ret = F($cache_name,'',$path);
	    }else{
	        $SysMsgM = new SysMsgModel();
	        $sysMsg_arr = array();
	        $sysMsg_arr['id'] = $sys_msg_id;
	        $sysmsg_ret = $SysMsgM->getOne($sysMsg_arr);
	        F($cache_name,$sysmsg_ret,$path);
	    }
	    
	    return $sysmsg_ret;
	}
	/*
	 *
	 * get_round_sys_msg
	 */
	public function get_cache_sys_msg($where=array(),$reset='0'){
	    $path = CHCHEPATH_AUTOMSG;
	    $cache_name = 'sysmsg_'.md5(json_encode($where));
	    //F($cache_name,NULL,$path);
	    if(F($cache_name,'',$path) && $reset == 0){
	        $sysmsg = F($cache_name,'',$path);
	    }else{
	        $SysMsgM = new SysMsgModel();
	        $sysmsg = $SysMsgM->getList($where);
	        
	        F($cache_name,$sysmsg,$path);
	    }
	    return $sysmsg;
	} 
	/*
	 *
	 * get_round_sys_msg
	 */
	public function get_cache_onesys_msg($id){
	    $path = CHCHEPATH_AUTOMSG;
	    $cache_name = 'sysmsg_'.$id;
	    if(F($cache_name,'',$path)){
	        $sysmsg = F($cache_name,'',$path);
	    }else{
	        $SysMsgM = new SysMsgModel();
	        $where["id"]=$id;
	        $sysmsg = $SysMsgM->getOne($where);
	        F($cache_name,$sysmsg,$path);
	    }
	    return $sysmsg;
	}
	/*
	 *发信
	 */
	
	public function _writemsg($data)
	{
	    $uid = $data["uid"];
	    $MyUserInfo = $this->get_user($uid);
	    //参数完整性验证start
	    if (!isset($data['touid'])) {
	        return false;
	    }
	    /* if (!isset($data['content'])) {
	     Push_data (array('message' => $this->L ("content字段不能为空"), 'code' => ERRORCODE_501));
	     } */
	    //参数完整性验证end
	    
	    $redis = $this->redisconn ();
	    $redisStrj = "Smsg_" . $uid;
	        $msgtype = $data['msgtype'] ? $data['msgtype'] : "text";//发信类型:text文本消息，voice语音消息，video视频消息，location位置信息，gift礼物消息,image图片
	        $content = $data['content'] ? $data['content'] : "";//发信内容//文本时
	        $touid = $data['touid'] ? $data['touid'] : "";//收信人用户ID
	        $ToUserInfo = $this->get_user ($touid);//收信人用户信息
	        $contentint = $data['contentint'] ? $data['contentint'] : "0";//礼物数量、语音、红包金币数量或视频时长
	        $contentbody = $data['contentbody'] ? $data['contentbody'] : "";//礼物id、语音url或视频url
	        $contentsub = $data['contentsub'] ? $data['contentsub'] : "";//视频第一帧图片url
	        $mybox_id = $uid . $touid;
	        $tobox_id = $touid . $uid;
	        $MyBoxinfo = array();
	        $ToBoxinfo = array();
	        $Msginfo = array();
	        switch ($msgtype) {
	            //voice语音消息
	            case "voice":
	                $Msginfo['contentint'] = $contentint;
	                $Msginfo['contentbody'] = $contentbody;
	                break;
	                //video视频消息
	            case "video":
	                $Msginfo['contentint'] = $contentint;
	                $Msginfo['contentbody'] = $contentbody;
	                $Msginfo['contentsub'] = $contentsub;
	                break;
	                //location位置信息
	            case "location":
	                $Msginfo['contentint'] = $contentint;
	                $Msginfo['contentbody'] = $contentbody;
	                break;
	                //gift礼物消息
	            case "gift":
	                $Msginfo['contentint'] = $contentint;
	                $Msginfo['contentbody'] = $contentbody;
	                
	                //askgift索要礼物消息
	            case "askgift":
	                $Msginfo['contentint'] = $contentint;
	                $Msginfo['contentbody'] = $contentbody;
	                //,image图片
	            case "image":
	                $Msginfo['contentint'] = $contentint;
	                $Msginfo['contentbody'] = $contentbody;
	                break;
	            case "redpacket":
	                $Msginfo['contentint'] = $contentint;
	            default:
	                
	                break;
	        }
	        
	        $MyBoxinfo['type'] = $msgtype;
	        //if($msgtype==1) {
	        
	        $MyBoxinfo['id'] = $mybox_id;
	        $MyBoxinfo['content'] = $content;
	        $MyBoxinfo['uid'] = $uid;
	        $MyBoxinfo['touid'] = $touid;
	        $MyBoxinfo['country'] ='TW';
	        $MyBoxinfo['language'] = 'zh-tw';
	        $MyBoxinfo['sendtime'] = time ();
	        //$where = array('uid'=>$uid);
	        
	        //信箱所需字段信息
	        $Msginfo['type'] = $msgtype;
	        $Msginfo['sendtime'] = $MyBoxinfo['sendtime'];
	        $Msginfo['language'] = $MyBoxinfo['language'];
	        $Msginfo['country'] = $MyBoxinfo['country'];
	        $Msginfo['uid'] = $uid;
	        $Msginfo['touid'] = $touid;
	        $Msginfo['content'] = $content;
	        $Msginfo['box_id'] = $mybox_id;
	        $MsgM = new MsgModel();
	        $MsgBoxm = new MsgBoxModel();
	        $result = $MsgM->addOne ($Msginfo);
	        //消息列表储存过后将消息储存在信箱列表中，运用redis查询信息列表中是否已有来往信息
	        if ($result) {
	            $redisStr = "Gmsgbox_" . $mybox_id;
	            $cachetime = 60 * 60 * 24 * 3;
	            $ab['id']=$mybox_id;
	            if ($redis->exists ($redisStr)) {
	                $MsgBoxm->updateOne1 ($ab, $MyBoxinfo);
	            } else {
	                $value = $MsgBoxm->getOne1 ($ab);
	                if ($value) {
	                    $MsgBoxm->updateOne1 ($ab, $MyBoxinfo);
	                    $redis->set ($redisStr, $mybox_id, 0, 0, $cachetime);
	                } else {
	                    $MsgBoxm->addOne1 ($MyBoxinfo);
	                    $redis->set ($redisStr, $mybox_id, 0, 0, $cachetime);
	                }
	            }
	           return true;
	        }
	        return false;
	}
}

?>