<?php
use Think\Controller;
class RechargeController extends BaseController {
	public function __construct(){
		parent::__construct();
		//write_logs();
	}
	public function recharge(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
        $touid = $data['touid'] ? $data['touid'] : "";
		$userinfo = $this->UserInfo;
		$touserinfo=$this->get_user($touid);
		$product = $userinfo["product"];
		$paytime = time();
		$vipgrade = $data['vipgrade'] ? $data['vipgrade'] : 1;//会员等级
		$type = $data['type'] ? $data['type'] : 1;
		$isauto = $data['isauto'] ? $data['isauto'] : 0;
		$count = $data['count'] ? $data['count'] : "0";//充值天数或金币数量
		$money = $data['money'] ? $data['money'] : "0";
		$translateid = $data['translateid'] ? $data['translateid']: "translateid";
		$productid = $data['productid'] ? $data['productid'] : "productid";
		$purchasetoken = $data['purchasetoken'] ? $data['purchasetoken'] : "purchasetoken";
		$packagename = $data['packagename'] ? $data['packagename'] : "packagename";
		$RechargeM = new RechargeModel();
		$InData = array();
		$InData['uid'] = $uid;
		$InData['days'] = $count;
		$InData['translateid'] = $translateid;
		$InData['paytime'] = $paytime;//服务器时间
		$InData['type'] = $type;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
		$InData['to_uid'] = $touid;
		$InData['money'] = intval($money*100);//充值金额计算
		$InData['productid'] = $productid;
		$InData['purchasetoken'] = $purchasetoken;
		$InData['packagename'] = $packagename;
		$InData['isauto'] = $isauto;
		$InData['product'] = $product;
        $Pushdata = array();
		//if(is_array($ores)){
		if(true){
		    if($type==1){
		        $viptime = $count*24*60*60;
		        $rtime=$this->UserInfo["viptime"];
		        if($rtime==0||$rtime<time()){
		            $viptime = $viptime+time();
		        }else{
		            $viptime = $viptime+$rtime;
		        }
		       //$resultr=$RechargeM->where('id='.$payid)->setField('vipgold',$viptime);
		        $InData['vipgold'] = $viptime;
		    }elseif($type==2){
		        $viptime = $count*24*60*60;
		        $rtime=$touserinfo["viptime"];
		        if($rtime==0){
		            $viptime = $viptime +time();
		        }else{
		            $viptime = $viptime+$rtime;
		        }
		        //$resultr=$RechargeM->where('id='.$payid)->setField('vipgold',$viptime);
		        $InData['vipgold'] = $viptime;
		    }elseif($type==3){
		        $rgold=$this->UserInfo["gold"];
		        $InData['vipgold']=$rgold+$count;
		    }elseif($type==4){
		        $rgold=$touserinfo["gold"];
		        $InData['vipgold']=$rgold+$count;
		    }
				$payid =$RechargeM->addOne($InData);
                $UserBaseM = new UserBaseModel();
                if($type==1){
                    //add money start
                    $MoneyC = new MoneyController();
                    $MoneyC->rechargevip($InData,$payid);
                    //add money end
                    $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
                    //检查该用户充值记录是否在意向表里边存在
                    $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
                    //若存在则删除
                    if($data) {
                        $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
                    }
                    if($resultu!== false){
                        $Pushdata['message'] = $this->L("充值vip天数成功");
                    }else{
                        $Pushdata['code'] = ERRORCODE_201;
                        $Pushdata['message'] = $this->L("充值vip天数不成功");
                    }
                    $this->get_user($uid,1);
                }
                //给别人充值
             elseif($type==2){
                 //add money start
                 $MoneyC = new MoneyController();
                 $MoneyC->rechargevip($InData,$payid);
                 //add money end
                 $resultu=$UserBaseM->updateOne(array("uid"=>$touid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
                    if($resultu){
                        $Pushdata['message'] = $this->L("给他人充值vip天数成功");
                    }else{
                        $Pushdata['code'] = ERRORCODE_201;
                        $Pushdata['message'] = $this->L("给他人充值vip天数不成功");
                    }
                    $this->get_user($touid,1);
            }
                elseif($type==3){
                    //增加金币
                   
                    $result=$this->gold_add($uid, $count);
                    //检查该用户充值记录是否在意向表里边存在
                    $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
                    //若存在则删除
                    if($data) {
                        $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
                    }
                    if($result){
                        $Pushdata['message'] = $this->L("充值金币成功");
                    }else{
                        $Pushdata['code'] = ERRORCODE_201;
                        $Pushdata['message'] = $this->L("充值金币不成功,count字段需传值");
                    }
                    $this->get_user($uid,1);
                }
                elseif($type==4){
                    //帮别人充值金币
                    $result=$this->gold_add($touid, $count);
                    if($result){

                        $Pushdata['message'] = $this->L("给他人充值金币成功");
                    }else{
                        $Pushdata['code'] = ERRORCODE_201;
                        $Pushdata['message'] = $this->L("给他人充值金币不成功,count需传值或touid需传值并正确");
                    }
                    $this->get_user($touid,1);
                }
				
		}else{
				if(strstr($data['translateId'], "GPA.")){
					$RechargeDelM = new RechargeDelModel();
					$RechargeDelM->addOne($InData);
				}
		}
		Push_data($Pushdata);
	}
	/*
	 * ios 充值
	 *          类型.数量.金额*100.vip等级
        jiaoyou.vip.30.2000.1
        jiaoyou.diamonds.300.3000
	 */
	public function rechargeios(){
	    $data = $this->Api_recive_date;
	    $uid = $this->uid;
	    $touid = $data['touid'] ? $data['touid'] : "";
	    $userinfo = $this->UserInfo;
	    $touserinfo=$this->get_user($touid);
	    $product = $userinfo["product"];
	    $paytime = time();
	    $productid = $data['productid'] ? $data['productid'] : "0";
	    $issandbox = $data['issandbox'] ? $data['issandbox'] : "0";
	    $productidArr = explode(".", $productid);
	    
	    if(count($productidArr)<4){
	        $Pushdata['code'] = ERRORCODE_201;
	        $Pushdata['message'] = "productid error";
	        Push_data($Pushdata);
	    }
	    if($productidArr[1]=="vip"){
	        $isauto=1;
	        if($touid){
	            $type=2;
	        }else{
	            $type=1;
	        }
	    }elseif($productidArr[1]=="vipf"){
	        $isauto=0;
	        if($touid){
	            $type=2;
	        }else{
	            $type=1;
	        }
	    }elseif($productidArr[1]=="diamonds"){
	        $isauto=0;
	        if($touid){
	            $type=4;
	        }else{
	            $type=3;
	        }
	    }else{
	        $Pushdata['code'] = ERRORCODE_201;
	        $Pushdata['message'] = "productid error";
	        Push_data($Pushdata);
	    }
	    $vipgrade =$productidArr[3] ? $productidArr[3] : 0;//会员等级
	    $count = $productidArr[2];//充值天数或金币数量
	    $money = $productidArr[3];
	    $translateid = $data['translateid'] ? $data['translateid']: "translateid";
	    $purchasetoken = $data['purchasetoken'] ? $data['purchasetoken'] : "purchasetoken";
	   
	    $InData = array();
	    $InData['uid'] = $uid;
	    $InData['days'] = $count;
	    $InData['translateid'] = $translateid;
	    $InData['paytime'] = $paytime;//服务器时间
	    $InData['type'] = $type;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
	    $InData['to_uid'] = $touid;
	    $InData['money'] = intval($money);//充值金额计算
	    $InData['productid'] = $productid;
	    $InData['purchasetoken'] = $purchasetoken;
	    $InData['isauto'] = $isauto;
	    $InData['product'] = $product;
	    $Pushdata = array();
	    $ores = $this->checkIosorder($InData,$issandbox,$product);
	    if(is_array($ores)){
	        $Pushdata= $this->_addpay($InData,$userinfo,$touserinfo,$vipgrade);
	    }else{
	        $Pushdata['code'] = ERRORCODE_201;
	        $Pushdata['message'] = "验证失败";
	        //if(strstr($data['translateId'], "GPA.")){
	            $RechargeDelM = new RechargeDelModel();
	            $RechargeDelM->addOne($InData);
	       // }
	    }
	    Push_data($Pushdata);
	}
	
	protected function checkIosorder($InData,$issandbox,$product){
	    $receipt = $InData["purchasetoken"];
	    //echo $receipt;
	    //$username = addslashes($_REQUEST['username']);//用户名
	    $tc =  $InData["productid"];//套餐类型
	    //$tc = "com.wbkj.jiaoyou.vip230";
	    
	    /**
	     * 21000 App Store不能读取你提供的JSON对象
	     * 21002 receipt-data域的数据有问题
	     * 21003 receipt无法通过验证
	     * 21004 提供的shared secret不匹配你账号中的shared secret
	     * 21005 receipt服务器当前不可用
	     * 21006 receipt合法，但是订阅已过期。服务器接收到这个状态码时，receipt数据仍然会解码并一起发送
	     * 21007 receipt是Sandbox receipt，但却发送至生产系统的验证服务
	     * 21008 receipt是生产receipt，但却发送至Sandbox环境的验证服务
	     */
	    function acurl($receipt_data, $sandbox=0,$product){
	        //如果是沙盒模式，请求苹果测试服务器,反之，请求苹果正式的服务器
	        if ($sandbox==1) {
	            $endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
	        }
	        else {
	            $endpoint = 'https://buy.itunes.apple.com/verifyReceipt';
	        }
	        if($product==20210){
                $postData = json_encode(
                    array('receipt-data' => $receipt_data,"password"=>"799a76ea26fd4ebf87051d946ce04e84")
                );
            }elseif($product==20211){
                $postData = json_encode(
                    array('receipt-data' => $receipt_data,"password"=>"6e05fa19d462406d897b96536c1709bc")
                );
            }else{
                $postData = json_encode(
                    array('receipt-data' => $receipt_data,"password"=>"799a76ea26fd4ebf87051d946ce04e84")
                );
            }

	        $ch = curl_init($endpoint);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);  //这两行一定要加，不加会报SSL 错误
	        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
	        
	        
	        $response = curl_exec($ch);
	        $errno    = curl_errno($ch);
	        $errmsg   = curl_error($ch);
	        curl_close($ch);
	        $res["response"] = $response;
	        $res["errno"] = $errno;
	        $res["errmsg"] = $errmsg;
	        return $res;
	    }
	    
	    // 请求验证
	    $html = acurl($receipt,'',$product);
	    
	    $data = json_decode($html["response"]);
	    
	    // 如果是沙盒数据 则验证沙盒模式
	    if($data->status=='21007'){
	        // 请求验证
	        $html = acurl($receipt, 1,$product);
	        $data = json_decode($html["response"]);
	        $data->sandbox = '1';
	    }
	    
	    //判断时候出错，抛出异常
	    if ($html["errno"] != 0) {
	        return 0;
	    }
	    //判断返回的数据是否是对象
	    if (!is_object($data)) {
	        return 0;
	    }
	    
	    //判断购买时候成功
	    if (!isset($data->status) || $data->status != 0) {
	        //Dump($data);
	        return 0;
	    }
	    
	    $order = $data->receipt->in_app;//所有的订单的信息
	    $k = count($order) -1;
	    $need = object_array($order[$k]);//需要的那个订单
	    
	    return $need;
	}
	protected function _addpay($InData,$userinfo,$touserinfo,$vipgrade){
	    $type = $InData["type"];
	    $count = $InData["days"];
	    $uid = $userinfo["uid"];
	    $touid = $touserinfo["uid"];
	    $RechargeM = new RechargeModel();
	    if($type==1){
	        $viptime = $count*24*60*60;
	        $rtime=$userinfo["viptime"];
	        if($rtime==0||$rtime<time()){
	            $viptime = $viptime+time();
	        }else{
	            $viptime = $viptime+$rtime;
	        }
	        //$resultr=$RechargeM->where('id='.$payid)->setField('vipgold',$viptime);
	        $InData['vipgold'] = $viptime;
	    }elseif($type==2){
	        $viptime = $count*24*60*60;
	        $rtime=$touserinfo["viptime"];
	        if($rtime==0){
	            $viptime = $viptime +time();
	        }else{
	            $viptime = $viptime+$rtime;
	        }
	        //$resultr=$RechargeM->where('id='.$payid)->setField('vipgold',$viptime);
	        $InData['vipgold'] = $viptime;
	    }elseif($type==3){
	        $rgold=$this->UserInfo["gold"];
	        $InData['vipgold']=$rgold+$count;
	    }elseif($type==4){
	        $rgold=$touserinfo["gold"];
	        $InData['vipgold']=$rgold+$count;
	    }
	    $payid =$RechargeM->addOne($InData);
	    $UserBaseM = new UserBaseModel();
	    if($type==1){
	        //add money start
	        $MoneyC = new MoneyController();
	        $MoneyC->rechargevip($InData,$payid);
	        //add money end
	        $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
            //检查该用户充值记录是否在意向表里边存在
            $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
            //若存在则删除
            if($data) {
                $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
            }
	        if($resultu!== false){
	            $Pushdata['message'] = $this->L("充值vip天数成功");
	        }else{
	            $Pushdata['code'] = ERRORCODE_201;
	            $Pushdata['message'] = $this->L("充值vip天数不成功");
	        }
	        $this->get_user($uid,1);
	    }
	    //给别人充值
	    elseif($type==2){
	       
	        //add money start
	        $MoneyC = new MoneyController();
	        $MoneyC->rechargevip($InData,$payid);
	        //add money end
	        $resultu=$UserBaseM->updateOne(array("uid"=>$touid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
	       
	        if($resultu){
	            $Pushdata['message'] = $this->L("给他人充值vip天数成功");
	        }else{
	            $Pushdata['code'] = ERRORCODE_201;
	            $Pushdata['message'] = $this->L("给他人充值vip天数不成功");
	        }
	        $this->get_user($touid,1);
	    }
	    elseif($type==3){
	        //增加金币
	        //echo $count;exit;
	        $result=$this->gold_add($uid, $count);
            //检查该用户充值记录是否在意向表里边存在
            $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
            //若存在则删除
            if($data) {
                $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
            }
	        if($result){
	            $Pushdata['message'] = $this->L("充值金币成功");
	        }else{
	            $Pushdata['code'] = ERRORCODE_201;
	            $Pushdata['message'] = $this->L("充值金币不成功,count字段需传值");
	        }
	        $this->get_user($uid,1);
	    }
	    elseif($type==4){
	        //帮别人充值金币
	        $result=$this->gold_add($touid, $count);
	        if($result){
	            
	            $Pushdata['message'] = $this->L("给他人充值金币成功");
	        }else{
	            $Pushdata['code'] = ERRORCODE_201;
	            $Pushdata['message'] = $this->L("给他人充值金币不成功,count需传值或touid需传值并正确");
	        }
	        $this->get_user($touid,1);
	    }
	    return $Pushdata;
	}
	public function paypalreturn(){
	    $custom = json_decode(base64_decode($_POST['custom']));
	    $uid=$custom->uid;
        $UserInfo = $this->get_user($uid);
        $product = $UserInfo["product"];
        $paytime = time();
        $vipgrade =$custom->status?$custom->status : 1;//会员等级
        $paytype = $custom->type?$custom->type: 1;//1:viptime2:金币
        $count = $custom->days? $custom->days:"0";//充值天数或金币数量
        $money = $custom->money? $custom->money:"0";

        $RechargeM = new RechargeModel();
        $reData = array();
        $reData['uid'] = $uid;
        $reData['days'] = $count;
        $reData['paytime'] = $paytime;//服务器时间
        $reData['product'] = $product;
        if($paytype==1){
            $reData['type'] =1;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
        }else{
            $reData['type'] =3;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
        }

        $reData['money'] = intval($money*100);//充值金额计算

        if($paytype==1){
            $viptime = $count*24*60*60;
            $rtime=$UserInfo["viptime"];
            if($rtime==0||$rtime<time()){
                $viptime = $viptime+time();
            }else{
                $viptime = $viptime+$rtime;
            }
            $reData['vipgold'] = $viptime;
        }elseif($paytype==2){
            $rgold=$UserInfo["gold"];
            $reData['vipgold']=$rgold+$count;
        }

        $payid =$RechargeM->addOne($reData);
        $UserBaseM = new UserBaseModel();
        if($paytype==1){
            $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
            //检查该用户充值记录是否在意向表里边存在
            $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
            //若存在则删除
            if($data) {
                $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
            }
            if($resultu!== false){
                $Pushdata['message'] = $this->L("充值vip天数成功");
                $Pushdata['data']['leftviptime']=$viptime/(24*60*60);
            }else{
                $Pushdata['code'] = ERRORCODE_201;
                $Pushdata['message'] = $this->L("充值vip天数不成功");

            }
            $this->get_user($uid,1);
        }elseif($paytype==2){
            //增加金币
            $result=$this->gold_add($uid, $count);
            //检查该用户充值记录是否在意向表里边存在
            $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
            //若存在则删除
            if($data) {
                $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
            }
            if($result){
                $Pushdata['message'] = $this->L("充值金币成功");
            }else{
                $Pushdata['code'] = ERRORCODE_201;
                $Pushdata['message'] = $this->L("充值金币不成功,count字段需传值");
            }
            $this->get_user($uid,1);
        }



	    echo 1;
	    return 1;
	}

	public function yixiang(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $user=M('user_yixiang');
        $time=time();
        $data=$user->where(array('uid'=>$uid))->find();
            //如果库中已有记录，则更新字段，count加1.
            if($data){
                $res=array(
                    'count'=>$data['count']+1,
                    'last_time'=>$time
                );
                $ret=$user->where(array('uid'=>$uid))->setField($res);
                if($ret){
                    $return['message'] = $this->L ("CHENGGONG");
                    Push_data($return);
                }else{
                    $return['code'] = ERRORCODE_201;
                    $return['message'] = $this->L ("更新意向表不成功，后台处理");
                    Push_data($return);
                }
                //库中没有记录则插入
            }else{
                $count=1;
                $insert=array();
                $insert['count']=$count;
                $insert['last_time']=$time;
                $insert['uid']=$uid;
                $ins=$user->data($insert)->add();
                if($ins){
                    $return['message'] = $this->L ("CHENGGONG");
                    Push_data($return);
                }else{
                    $return['code'] = ERRORCODE_201;
                    $return['message'] = $this->L ("插入意向表不成功，后台处理");
                    Push_data($return);
                }
            }
    }
}
?>