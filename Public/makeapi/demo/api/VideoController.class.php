<?php
class VideoController extends BaseController {
	public function __construct(){
		parent::__construct();
		//$this->is_login();//检查用户是否登陆
		$this->fileurl = '/userdata/renzheng/'.date('Ymd',time())."/";
	}
	/*
	 * 
	 * 上传照片
	 */
	public function upload(){
		$data = $this->Api_recive_date;
		$ltime = $data["ltime"] ? $data["ltime"] : 0;
		$uid =$this->uid;		
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] =  $this->L("SHANGCHUANSHIBAI");
		$saveurl = $this->fileurl.'video/';
		$savePath = WR.$saveurl;
		createDir($savePath);
		if(!empty($_FILES)){ 
			$uploadList = $this->_upload($savePath);
			if($uploadList){
				$imageurlfile = array();
				$videofile = array();
				foreach($uploadList as $k=>$v){
					if($v["key"]=="imageurl"){
						$imageurlfile = $v;
					}else{
						$videofile = $v;
					}
				}
				$VideoM = new VideoModel();
				$Indata = array();
				$Indata['uid'] = $uid;
				$Indata['type'] = 1;//
				$Indata['ltime'] = $ltime;//
				$Indata['imageurl'] = $saveurl.$imageurlfile['savename'];//
				$Indata['url'] = $saveurl.$videofile['savename'];
				$Indata['time'] = time();//
				$ret = $VideoM->addOne($Indata);
				PutOss(array($Indata['imageurl'],$Indata['url']));
				$this->get_user_video($uid,1);
				$return = array();
			}
		}
		
		Push_data($return);
	}
	// 文件上传
	protected function _upload($savePath) {
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($savePath);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize = 3292200;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
		//设置附件上传目录
		$upload->savePath = $savePath;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
		$upload->saveRule = "md5content";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			return $uploadList;;
		}

	}
	/*
	 * 删除
	 */
	public function delete(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");;
		if($id){
			//删除视频
			$VideoM = new VideoModel();
			$VideoInfo = $VideoM->getOne(array("id"=>$id));
			_unlink(WR.$VideoInfo["imageurl"]);
			_unlink(WR.$VideoInfo["url"]);
			DelOss(array($VideoInfo["imageurl"],$VideoInfo["url"]));
			$VideoM->delOne(array("id"=>$id));
			$return = array();
		}
		Push_data($return);
	}
}

?>