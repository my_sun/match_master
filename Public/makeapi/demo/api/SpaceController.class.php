<?php
class SpaceController extends BaseController {
	public function __construct(){
		parent::__construct();
		//$this->is_login();//检查用户是否登陆
		//$this->Api_recive_date;
	
		
	}
	
	/**
	 * 上传语音
	 * 
	 */
	public function uploadaudio(){
		//header("Content-type: text/html; charset=utf-8");
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$audiotime = $data["audiotime"];	
		$user_path = $this->get_userpath($uid, 'images').$uid.'/myaudio/';
		mkdirs($user_path);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize = 32922000;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'mp3,arm');
		//设置附件上传目录
		$upload->savePath = $user_path;
		$upload->saveRule =time().".mp3";
		
		if (!$upload->upload()) {
				$RetArray['isSucceed'] = 0;
				$RetArray['msg'] = $upload->getErrorMsg();
		} else {
			$RetArray['url'] = 'http://newtaiwan.oss-cn-hongkong.aliyuncs.com/data'.$this->get_userurl($uid, 'images').$uid.'/myaudio/' . $upload->saveRule;
			$RetArray['isSucceed'] = 1;
			$RetArray['msg'] = "ok";
			$audiourl=$this->get_userurl($uid, 'images').$uid.'/myaudio/' .$upload->saveRule;
			$UseraudioM = new AudioModel();
			$AddData = array();
			$AddData["uid"]=$uid;
			$AddData["audiourl"]=$audiourl;
			$AddData["audiotime"]=$audiotime;
			$AddData["addtime"]=time();
			$res = $UseraudioM->addOne($AddData);
			PutOss($audiourl);
		}	
	
		Push_data($RetArray);
	}
	/*
	 *已收到礼物列表
	*/
	public function mygiftslist(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$pageNum = $data['pageNum'] ? $data['pageNum'] : 1;
		$pageSize = 30;
		
		$WhereArr = array();
		$WhereArr['to_uid'] = $myid;
		$WhereArr['type'] = 1;
		$PointsBaseM = new PointsModel();
		$res = $PointsBaseM->getListPage($WhereArr,$pageNum,$pageSize);
		
		$gifts = array();
		$allmoney=0;
		foreach($res["list"] as $k=>$v){
			if($v["typeid"]){
				$gifts[$v["typeid"]]=$gifts[$v["typeid"]]+$v["typecount"];
				$allmoney+=$v["typecount"]*$v["points"];
			}
		}
		$list = array();
		
		foreach($gifts as $k=>$v){
			$temp["giftid"]=$k;
			$temp["gifCounts"]=$v;
			$list[] = $temp;
		}
		
		$allmoney=ceil($allmoney/2);
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray["list"] = $list;
		$RetArray["allmoney"] = $allmoney;
		$RetArray['isSucceed'] = 1;
		$RetArray['msg'] = "ok";
	
		Push_data($RetArray);
	}
	/*
	 *送出的礼物列表
	*/
	public function sendgiftslist(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$pageNum = $data['pageNum'] ? $data['pageNum'] : 1;
		$pageSize = 30;
		
		$WhereArr = array();
		$WhereArr['uid'] = $myid; 
		$WhereArr['type'] = 1;
		$PointsBaseM = new PointsModel();
		$res = $PointsBaseM->getListPage($WhereArr,$pageNum,$pageSize);
		$gifts = array();
		foreach($res["list"] as $k=>$v){
			if($v["typeid"]){
				$gifts[$v["typeid"]]=$gifts[$v["typeid"]]+$v["typecount"];
			}
		}
		$list = array();
		foreach($gifts as $k=>$v){
			$temp["giftid"]=$k;
			$temp["gifCounts"]=$v;
			$list[] = $temp;
		}
		
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray["list"] = $list;
		$RetArray['isSucceed'] = 1;
		$RetArray['msg'] = "ok";
	
		Push_data($RetArray);
	}
	/**
	 * 1.0.0.11 获取最近访客 /space/getseemelist
	 **/
	public function getseemelist(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = 15;
		$Wdata['uid'] = $myid;
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		
		$UserSeeM = new UserSeeModel();
		$ret = $UserSeeM->getListPage($Wdata,$pageNum,$pageSize);
		
		foreach($ret['listSeeMe'] as $k=>$v){
			
			$ret['listSeeMe'][$k] = array();
			$userinfo = $this->get_user($v['touid']);
			
			$userinfo = $this->get_user_field($userinfo,$userfield);
			$userinfo['issayhello'] = MsgController::isSayHello($v['touid']);
			$ret['listSeeMe'][$k]['user'] =$userinfo;
			
		}
		$return = array();
		$userlist = $ret['listSeeMe'];
		$return["data"]["userlist"] = $userlist;
		$page["pagesize"] = $pageSize;
		$page["page"] = $pageNum;
		$page["totalcount"] = $ret["totalCount"];
		$return["data"]["page"] = $page;
		
		
		Push_data($return);
	}
	
	public function userinfo(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$otherId = $data['touid'];

		$Adddata = array();
		$Adddata['touid'] = $otherId;
		$Adddata['uid'] = $myid;
		
		$User_seeM = new UserSeeModel();
		$return = $User_seeM->getOne($Adddata);
		if($return){
			$ret = $User_seeM->updateOne($Adddata,array('count'=>$return['count']+1,'time'=>time()));
		}else{
			$Adddata['time'] = time();
			$ret = $User_seeM->addOne($Adddata);
		}
		
		
		$TouserInfo = $this->get_diy_user_field($otherId,"*");
		
		if($TouserInfo['uid']){
		    $TouserInfo['issayhello'] = MsgController::isSayHello($otherId);//是否打过招呼，1->已打招呼, 0->未打招呼,
		    $TouserInfo['isfollow'] = $this->isFollow($otherId);//是否关注 isFollow，1->已, 0->未

            //设置用户在线状态start
            $cachename= "get_onlinestatus_".$TouserInfo['uid'];
            $cache=S($cachename);//设置缓存标示
            if(!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                $rand=array('1','2');
                $rand_keys=array_rand($rand,1);
                $TouserInfo["isonline"]=$rand[$rand_keys];
                $cache= $rand[$rand_keys];
                S($cachename,$cache,60*60*2);//设置缓存的生存时间
            }
            $TouserInfo["isonline"]=$cache;
            //设置用户在线状态end

            if($TouserInfo["user_type"]==2){
			    $TouserInfo['receivedgifts'] = $this->getliaopubgetlist($otherId);//获取收到的礼物
                $countryiso=$this->country;
            }else{
			    $GiftC = new GiftController();
			    $TouserInfo['receivedgifts'] = $GiftC->pubgetlist($otherId);//获取收到的礼物
                $countryiso=$TouserInfo['country'];
			}

            //获取用户的国家和地区
            $regionid=$TouserInfo['area'];
            $reg=$this->get_regionlist($countryiso);
            foreach($reg as $k=>$v){
                if($v['id']==$regionid){
                    $regioncode=$v['name'];
                    break;
                }
            }
            //获取用户的魅力值start
            $redis=$this->redisconn();
            if($redis->exists('withdrawal_'.$otherId)) {
                $red = $redis->get('withdrawal_' . $otherId);
                $withd= json_decode($red, true);
                $withdrawalmoney=$withd['totalmoney'];
            }else{
                $res = M('m_money')->where(array('uid' => $otherId))->find();
                if ($res) {
                    $ret = array();
                    $ret['totalmoney'] = intval($res['totalmoney']);
                    $ret['leftmoney'] = intval($res['leftmoney']);
                    $ret['realmoney'] = intval($res['leftmoney'] *0.03* 0.2);
                    $ret['username']=$res['commonpay'];
                    $ret['phonenum']=$res['otherpay'];
                    $ret['zhifubao']=$res['zhifubao'];
                    $ret['weixin']=$res['weixin'];
                    $ret['paypal']=$res['paypal'];
                }else {
                    $ret = array();
                    $ret['totalmoney'] = 0;
                    $ret['leftmoney'] = 0;
                    $ret['realmoney'] = 0;
                }
                //界面提示信息
                $ret['message']='提示：每月仅可提现一次，魅力值≥500才可进行提现。';
                //界面魅力值大小限制信息
                $ret['count_mes']=500;
                $redisStr = "withdrawal_" . $otherId;
                $withd = json_encode($ret);
                $redis->set($redisStr, $withd, 0, 0, C("SESSION_TIMEOUT"));
                $withdrawalmoney=$ret['totalmoney'];
            }
            //获取用户的魅力值end

			//用户获取自己的信息
			if($myid==$otherId){
			    $TouserInfo["photos"] = $this->get_user_photo_all($otherId);
			    $TouserInfo["head"] = $this->get_user_ico_all($otherId);
			    $privilege["sendaudio"]=0;
			    $privilege["sendvideo"]=0;
			    $privilege["sendimage"]=0;
			    $privilege["diyposition"]=0;
			    $privilege["guanliyonghu"]=0;
			    $privilege["tuijian"]=0;
			    switch ($TouserInfo["vipgrade"]){
			        case 1:
			            //黄金会员
			            
			            break;
			        case 2:
			            //铂金会员
			            $privilege["sendaudio"]=1;
			            $privilege["sendimage"]=1;
			            break;
			        case 3:
			            //钻石会员
			            $privilege["sendaudio"]=1;
			            $privilege["sendvideo"]=1;
			            $privilege["sendimage"]=1;
			            break;
			        default:
			            
			            break;
			    }
			    if($TouserInfo["user_type"]==3){
			        $TouserInfo["vipgrade"] = 3;
			        $privilege["diyposition"]=1;
			        $privilege["guanliyonghu"]=1;
			        $privilege["tuijian"]=1;
			        $privilege["tuijianurl"]="Sver/boyen/index?uid=";
			    }
			    $returnret["data"]['privilege'] = $privilege;
			}
			$returnret["data"]['user'] = $TouserInfo;
            $returnret["data"]['user']['country']=$this->L($countryiso);
            $returnret["data"]['user']['area']=$this->L($regioncode);
            $returnret["data"]['user']['totalmoney']=$withdrawalmoney;

		}else{
			$returnret=array();
			$returnret ['code'] = ERRORCODE_201;
			$returnret ['message'] = $this->L("YONGHUBUCUNZAI");
		}

		Push_data($returnret);
	}
	protected function get_rundarr_key($array){
	    $count = count($array);
	    $start = 3;
	    $end = $count-10;
	    $numrand = rand($start, $end);
	    $ids = "";
	    
	    for($num=1;$num<=$numrand;$num++){
	        $key = array_rand($array);
	        $ids[]=$array[$key];
	        unset($array[$key]);
	        $array = array_merge($array);
	    }
	    return $ids;
	}
	//根据uid获取收到礼物列表
	public  function getliaopubgetlist($uid){
	    
	    
	    $res=array();
	    $res['touid']=$uid;
	    $cachename = "GetGiftList"."to".$uid;
	    $cache=S($cachename);
	    if(!$cache){
	        $product=10008;
	        $path = CHCHEPATH_GIFTLIST;
	        $cache_name = 'giftlist'.$product;
	        
	        if(F($cache_name,'',$path)){
	            
	            $gids = F($cache_name,'',$path);
	        }else{
	            
	            $getM = new GiftModel();
	            $Lists = $getM->getList(array("product"=>$product,"status"=>1));
	            $gids = array();
	            foreach ($Lists as $k=>$v){
	                $gids[] = $v["id"];
	            }
	            F($cache_name,$gids,$path);
	        }
	        $gids=$this->get_rundarr_key($gids);
	        $GiftListM = new GiftListModel();
	        $MsgC=new MsgController();
	        foreach($gids as $k=>$v){
	            $giftextend=$MsgC->giftextend($v);
	            
	            $result[$k]['url']= C("IMAGEURL").$giftextend['url'];
	            $result[$k]['price']=$giftextend['price'];
	            $result[$k]['title']=$giftextend['title'];
	            $result[$k]['id']=$v;
	            $result[$k]['count']=rand(2, 300);
	        }
	        $cache = $result;
	        S($cachename,$cache,60*60*24*3); //设置缓存的生存时间
	    }
	    $result=$cache;
	    return $result;
	}
	/*
	 * 获取我的信息 /space/myInfo
	 */
	public function wodexinxi(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$pushData = array();
		$pushData['user'] = $this->get_user($uid);
		$pushData['isSucceed'] = '1';
		$pushData ['msg'] = 'Succeed';
		//write_logs(json_encode($pushData));
		Push_data($pushData);
	}
		/*
	 *是否关注 isFollow
	*/
	public function isFollow($touid,$uid=0,$reset=0){
		$uid = $uid ? $uid : $this->uid;
		if($touid&&$uid){
		    $path = $this->get_userpath($uid, 'cache/follow/');
		    $cache_name = 'follow_'.$uid.$touid;
		    if($reset==1){
		        return F($cache_name,NULL,$path);
		    }
		    
		    if(F($cache_name,'',$path)){
		        $cacheValue = F($cache_name,'',$path);
		    }else{
		        $UserFollowM = new UserFollowModel();
		        $return = $UserFollowM->getOne(array('touid'=>$touid,'uid'=>$uid));
		        
		        if($return){
		            $cacheValue = 1;
		        }else{
		            $cacheValue = 0;
		        }
		        F($cache_name,$cacheValue,$path);
		    }
		    
		    return $cacheValue;
		}else{
		  return 0;
		}
	}
	/*
	 *关注 /space/follow
	*/
	public function follow(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$Adddata = array();
		$Adddata['touid'] = $data['touid'];
		$Adddata['uid'] = $myid;
		
	
		/***
		 * 返回信息定义
		*/
		$RetArray = array();

		$RetArray['message'] = $this->L("YIGUANZHU");
	
		$UserFollowM = new UserFollowModel();
	
		$return = $UserFollowM->getOne($Adddata);
	
		if($return){
			$RetArray['code'] = ERRORCODE_201;
			$RetArray['message'] = $this->L("YIJINGGUANZHUGUOLE");
	
		}else{
			$Adddata['time'] = time();
			$ret = $UserFollowM->addOne($Adddata);
			$this->isFollow($Adddata['touid'],$Adddata['uid'],1);
		}
		Push_data($RetArray);
	}
	/*
	 * 取消关注 /space/canfollow
	*/
	public function canfollow(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$Deldata = array();
		$Deldata['touid'] = $data['touid'];
		$Deldata['uid'] = $myid;
	
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray['message'] = $this->L("CHENGGONG");
	
		$UserFollowM = new UserFollowModel();
	
	
		$return = $UserFollowM->delOne($Deldata);

		    $this->isFollow($Deldata['touid'],$Deldata['uid'],1);
	    
		Push_data($RetArray);
	}
/*
	 *  查看我所关注的人/user/getfollowlist
	 */
	public function getfollowlist(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = 15;
		
		$Wdata = array();
		$Wdata['uid'] = $uid;
		
		$UserFollowM = new UserFollowModel();
		
		
		$ret = $UserFollowM->getListPage($Wdata,$pageNum,$pageSize);
	
		foreach($ret['listUser'] as $k=>$v){
		    $tempUserInfo = $this->get_user($v['touid']);
		    if($tempUserInfo){
    		    $tempArray = $this->get_user_field($tempUserInfo,$userfield);
                //随机获取用户在线状态
                $field_arr = explode("|", $userfield);
                if(in_array('isonline',$field_arr)) {
                    //设置用户在线状态start
                    $cachename = "get_onlinestatus_" . $v['touid'];
                    $cache = S($cachename);//设置缓存标示
                    if (!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                        $rand = array('1', '2');
                        $rand_keys = array_rand($rand, 1);
                        $TouserInfo["isonline"] = $rand[$rand_keys];
                        $cache = $rand[$rand_keys];
                        S($cachename, $cache, 60 * 60 * 2);//设置缓存的生存时间
                    }
                    $tempArray["isonline"] = $cache;
                    //设置用户在线状态end
                }
    			$ret['listUser'][$k] = array();
    			$ret['listUser'][$k] = $tempArray;
		    }
			
		}
		$return["data"]['userlist'] = $ret['listUser'];
		$return['message'] = $this->L("CHENGGONG");
		$page["pagesize"] = $pageSize;
		$page["page"] = $pageNum;
		$page["totalcount"] = $ret["totalCount"];
		$return["data"]["page"] = $page;
		Push_data($return);

	}
/*
	 *  查看关注我的人/user/getfollowmelist
	 */
	public function getfollowmelist(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = 15;
		
		$Wdata = array();
		$Wdata['touid'] = $uid;
		
		$UserFollowM = new UserFollowModel();
		
		
		$ret = $UserFollowM->getListPage($Wdata,$pageNum,$pageSize);
	
		foreach($ret['listUser'] as $k=>$v){
			$tempUserInfo = $this->get_user($v['uid']);
			if($tempUserInfo){
			    $tempArray = $this->get_user_field($tempUserInfo,$userfield);
			    $ret['listUser'][$k] = array();
			    $ret['listUser'][$k] = $tempArray;
			}
			
		}
		$return["data"]['userlist'] = $ret['listUser'];
		$return['message'] = $this->L("CHENGGONG");
		$page["pagesize"] = $pageSize;
		$page["page"] = $pageNum;
		$page["totalcount"] = $ret["totalCount"];
		$return["data"]["page"] = $page;
		Push_data($return);

	}
	/*
	 * 拉黑 /space/dragblack
	*/
	public function dragblack(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$Adddata = array();
		$Adddata['touid'] = $data['touid'];
		$Adddata['uid'] = $myid;
		
	
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray['message'] = $this->L("CHENGGONG");
	
		$UserDragblacklistM = new UserDragblacklistModel();
	
	
		$return = $UserDragblacklistM->getOne($Adddata);
	
		if($return){
			//$RetArray['isSucceed'] = 0;
			//$RetArray['msg'] = "已经关注过了";

		}else{
			$Adddata['time'] = time();
			$ret = $UserDragblacklistM->addOne($Adddata);
		}
		Push_data($RetArray);
	}
	/*
	 * 取消拉黑 /space/canceldragBlack
	*/
	public function candragblack(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$Deldata = array();
		$Deldata['touid'] = $data['touid'];
		$Deldata['uid'] = $myid;
	
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray['message'] = $this->L("CHENGGONG");
	
		$UserDragblacklistM = new UserDragblacklistModel();
	
	
		$return = $UserDragblacklistM->delOne($Deldata);
	
		Push_data($RetArray);
	}
	
	/*
	 * 举报 /space/report
	*/
	public function report(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$Adddata = array();
		$Adddata['touid'] = $data['touid'];
		$Adddata['uid'] = $myid;
		$Adddata['contentid'] = $data['id'];
		$Adddata['content'] = $data['content']?$data['content']:'';
        $Adddata['status'] = $data['status']?$data['status']:1;
		$Adddata['time'] = time();
	    if($data['status']==2){

            $Adddata['touid'] = $this->uid;
        }
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray['message'] = $this->L("CHENGGONG");
		$UserReportModelM = new UserReportModel();
			$ret = $UserReportModelM->addOne($Adddata);
		Push_data($RetArray);
	}
	/*
	 *投诉、建议、给客服留言 /space/service
	 */
	public function service(){
	    $data = $this->Api_recive_date;
	    $myid = $this->uid;
	    $type = $data['type'] ? $data['type'] :1;
	    $Adddata = array();
	    $Adddata['content'] = $data['content'];
	    $Adddata['type'] = $data['type'];
	    $Adddata['uid'] = $myid;
	    $Adddata['time'] = time();
	    
	    /***
	     * 返回信息定义
	     */
	    $RetArray = array();
	    $RetArray['message'] = $this->L("CHENGGONG");
	    $LetterServiceM = new LetterServiceModel();
	     $ret = $LetterServiceM->addOne($Adddata);
	    Push_data($RetArray);
	}
    //从缓存中取出地区信息
    protected function get_regionlist($id,$reset=0){
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->listSize('regionlist_'.$id);
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang==0){
            $where = array("country_codeiso"=>$id,"level"=>1);
            $RegionM = new RegionModel();
            $res = $RegionM->getList($where);
            foreach($res as $k=>$v){
                $res[$k] = array(
                    "id"=>$v["id"],
                    "name"=>$v["code"]
                );

                $value=json_encode($res[$k],true);
                $ret[]=$redis->listPush('regionlist'.$id,$value);
            }
            $result1=$res;
            //F($cache_name,$res,$path);
        }else{
            $result=$redis->listLrange('regionlist'.$id,0,-1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    if($result[$k1]==null){
                        continue;
                    }
                    $result1[$k1]=json_decode($v1,true);
                }
            }
        }
        return $result1;
    }
}

?>