<?php
class MoneyController extends BaseController {
	public function __construct(){
		//parent::__construct();
		
	}
	/*
	 * 充值会员
	 */
	public function rechargevip($rechargeInfo,$payid){
	    
	    $uid = $rechargeInfo["uid"];
	    $days = $rechargeInfo["days"];
	    $translateid = $rechargeInfo["translateid"];
	    $paytime = $rechargeInfo["paytime"];
	    $type = $rechargeInfo["type"];
	    $to_uid = $rechargeInfo["to_uid"];
	    $money = $rechargeInfo["money"];
	    $product = $rechargeInfo["product"];
	    $data["days"] = $days;
	    $data["translateid"] = $translateid;
	    $data["paytime"] = $paytime;
	    $data["payid"] = $payid;
	    $data["type"] = $type;
	    
	    $data["product"] = $product;
	    
	    if($type==1){
	        $puid = $this->get_moneykey($uid);
	        
	        if($puid){
    	        //自己充值
    	        $PMoneyM = new PMoneyModel();
    	        $data["money"] = $money*0.65*0.1;
    	        $data["to_uid"] = $uid;
    	        $data["uid"] = $puid;
    	        
    	        $PMoneyM->addOne($data);
	        }
	    }elseif($type==2){
	        //帮别人充值
	        $PMoneyM = new PMoneyModel();
	        $data["to_uid"] = $uid;
	        $data["uid"] = $to_uid;
	        $data["money"] = $money*0.65*0.25;
	        $PMoneyM->addOne($data);
	    }
	    return 1;
	}
	/*
	 *送礼物
	 */
	public function sendgift($giftinfo,$UserInfo,$touid,$contentint){
	    
	    $uid = $UserInfo["uid"];
	    $translateid = $giftinfo["id"];
	    $paytime = time();
	    $type = 3;
	    $money = $giftinfo["price"]*$contentint;
	    $url=$giftinfo['url'];
	    $product = $UserInfo["product"];
	    $data["days"] = $contentint;
	    $data["translateid"] = $translateid;
	    $data["paytime"] = $paytime;
	    $data["payid"] = $translateid;
	    $data["type"] = $type;
	    
	    $data["product"] = $product;
	    $pinfo = $this->get_Pinfo($touid);
	    if($type==3){
	        $PMoneyM = new PMoneyModel();
	        /* $h = intval(date("H"));
    	         if ($h > 23 || $h <= 4){
    	            $money = $money*0.65*0.3;
    	        }else{
    	            $money = $money*0.65*0.2;
    	        } */
    	        $money = $money*0.6;
    	        if($pinfo["upuid"]){
    	            $upmoney = $money*0.1;
    	            $money = $money-$upmoney;
    	            $updata = array();
    	            $updata["days"] = $contentint;
    	            $updata["translateid"] = $translateid;
    	            $updata["paytime"] = $paytime;
    	            $updata["payid"] = $translateid;
    	            $updata["type"] =4;
    	            $updata["product"] = $product;
    	            $updata["money"] = $upmoney;
    	            $updata["to_uid"] = $uid;
    	            $updata["uid"] = $pinfo["upuid"];
    	            $PMoneyM->addOne($updata);
    	        }
	           
	            $data["money"] = $money;
	            $data["to_uid"] = $uid;
	            $data["uid"] = $touid;
	            //礼物图片
                $data["account"] =$url;
	            $PMoneyM->addOne($data);
	            //增加魅力值的同时需删除redis中的缓存
            $redis=$this->redisconn();
            $redis->delete('withdrawal_'.$touid);
            $redis->delete('withdrawal_order'.$touid);
	            //向魅力值记录表插入更新魅力值等数据开始。
              $mmoney=M('m_money');
            $mmon=$mmoney->where(array('uid'=>$touid))->find();
            $dmon=array();
              if($mmon){
                  $dmon['totalmoney']=$mmon['totalmoney']+$money;
                  $dmon['leftmoney']=$mmon['leftmoney']+$money;
                  $dmon['lasttime']=time();
                  $mmoney->where(array('uid'=>$touid))->save($dmon);
              }else{
                  $dmon['uid']=$touid;
                  $dmon['totalmoney']=$money;
                  $dmon['leftmoney']=$money;
                  $dmon['lasttime']=time();
                  $mmoney->add($dmon);
              }
            //向魅力值记录表插入更新魅力值等数据结束。
	    }
	    return 1;
	}
	public function set_moneykey($uid,$touid){
	    $moneykey = "moneykey_".$touid;
	    //echo $moneykey;
	    $redis = $this->redisconn();
	    $redis->listRemove($moneykey, $uid,1);
	    $redis->listPush($moneykey, $uid);
	    return $redis->setListKeyExpire($moneykey, 60*60*24);
	}
	public function get_moneykey($uid){
	    $moneykey = "moneykey_".$uid;
	    //echo $moneykey;
	    $redis = $this->redisconn();
	    return $redis->listGet($moneykey);
	}
	public function get_Pinfo($uid,$reset=0){
	    $path =CHCHEPATH_PINFO;
	    $cache_name = 'pinfo_'.$uid;
	    if($reset==1){
	        return F($cache_name,NULL,$path);
	    }
	    if(F($cache_name,'',$path) && $reset == 0){
	        $res = F($cache_name,'',$path);
	    }else{
	        $pM = new PUserModel();
	        $res = $pM->getOne(array("uid"=>$uid));
	        F($cache_name,$res,$path);
	    }
	    return $res;
	}
}

?>