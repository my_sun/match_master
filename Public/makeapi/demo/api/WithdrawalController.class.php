<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/6/27
 * Time: 11:01
 */

class WithdrawalController extends BaseController
{
    public function __construct ()
    {
        parent::__construct ();
        
    }
    //魅力值信息 /withdrawal/index
    public function index(){
        $uid = $this->uid;
        //$redis=$this->redisconn();
        //if($redis->exists('withdrawal_'.$uid)){
           // $red=$redis->get('withdrawal_'.$uid);
          //  $return['data'] =json_decode($red,true);
      //  }else {
            $res = M('m_money')->where(array('uid' => $uid))->find();
            if ($res) {
                $ret = array();
                $ret['totalmoney'] = intval($res['totalmoney']);
                $ret['leftmoney'] = intval($res['leftmoney']);
                $ret['realmoney'] = intval($res['leftmoney'] *0.03* 0.2);
                $ret['username']=$res['commonpay'];
                $ret['phonenum']=$res['otherpay'];
                $ret['zhifubao']=$res['zhifubao'];
                $ret['weixin']=$res['weixin'];
                $ret['paypal']=$res['paypal'];
            } else {
                $ret = array();
                $ret['totalmoney'] = 0;
                $ret['leftmoney'] = 0;
                $ret['realmoney'] = 0;
            }
            //界面提示信息
            $ret['message']='提示：每月仅可提现一次，魅力值≥500才可进行提现。';
            //界面魅力值大小限制信息
            $ret['count_mes']=500;
            $redisStr = "withdrawal_" . $uid;
            $withd = json_encode($ret);
            //$redis->set($redisStr, $withd, 0, 0, C("SESSION_TIMEOUT"));
            $return['message'] = $this->L("CHENGGONG");
            $return['data'] = $ret;
        //}
            Push_data($return);
        }

    //魅力值提现 /withdrawal/draw
    public function draw(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $ret=array();
        $ret['uid']=$uid;
        $ret['type']=4;
        $ret['is_tixian']=1;
        $ret['account_type'] = $data["account_type"]?$data["account_type"]:1;//账户类型
        if($data["account"]){
            $ret['account']= $data["account"];//账户编号
        }else{
            $return['code'] = ERRORCODE_201;
            $return['message'] = $this->L ("需要传入账户信息！！");
        }
        if(!$data["username"]){
            $return['code'] = ERRORCODE_201;
            $return['message'] = $this->L ("需要传入真实姓名信息！！");
        }
        $ret['money']= $data["draw_money"];//提取魅力值
        $ret['paytime']= time();
        $beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));
        $mmoney=M('m_money')->where(array('uid'=>$uid))->find();
        //查看截止到当前时间订单列表中是否有提现记录
        $mmo=array();
        $mmo['paytime']=array('between',array($beginThismonth,$ret['paytime']));
        $mmo['uid']=$uid;
        $mmo['type']=4;
        $mmo['is_tixian']=array('neq',3);
        $mmoneylist=M('p_money')->where($mmo)->find();
        //首先判断该用户是否有权限提现：1，账户魅力值大于500 2：用户每月只能提现一次
        if($mmoney&&$ret['money']>=500){
            if($mmoneylist){
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L ("对不起,本月已经提现一次");
                Push_data($return);
                }else{
                if($mmoney['leftmoney']<$ret['draw_money']){
                    $return['code'] = ERRORCODE_201;
                    $return['message'] = $this->L ("对不起,提取的魅力值大于剩下的魅力值");
                    Push_data($return);
                }else{
                    //添加订单的同时需删除订单缓存和魅力值信息缓存
                    $redis = $this->redisconn();
                    $redis->del('withdrawal_order' . $uid);
                    $redis->del('withdrawal_' . $uid);
                    //向记录中添加
                    M('p_money')->add($ret);
                    //向魅力值信息表插入更新账户信息以及减少对应的魅力值
                    $countm=array();
                    $countm['commonpay']=$data["username"];
                    $countm['otherpay']=$data['phonenum'];
                    $countm['leftmoney']=$mmoney['leftmoney']-$ret['money'];
                    if($ret['account_type']==1){
                        $countm['weixin']=$ret['account'];
                    }elseif($ret['account_type']==2){
                        $countm['zhifubao']=$ret['account'];
                    }elseif($ret['account_type']==3){
                        $countm['paypal']=$ret['account'];
                    }
                    M('m_money')->where(array('uid'=>$uid))->save($countm);
                    $return['message'] = $this->L ("订单已提交");
                    Push_data($return);
                }
            }
        }else{
            $return['code'] = ERRORCODE_201;
            $return['message'] = $this->L ("对不起,账户魅力值不足500");
            Push_data($return);
        }
    }
    public function order(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = $data['pagesize'] ? $data['pagesize'] : 10;
        //$redis = $this->redisconn();
        //设置list缓存过期时间为一天
       // $redis->setListKeyExpire('withdrawal_order'.$uid,C("SESSION_TIMEOUT"));
        //缓存中储存总数据
       // $lang=$redis->listSize('withdrawal_order'.$uid);
       /* if ($lang == 0) {*/
            if (1) {
                $pmoneyM=new PMoneyModel();
                $Wdata =array();
                $Wdata['uid']=$uid;
                $ret=$pmoneyM->getListPage($Wdata,$pageNum,$pageSize);

                $res=$ret['list'];
            //历史订单查询根据时间排序后限定查询六条。
            //$res = M('p_money')->where(array('uid' => $uid))->order('paytime desc')->limit(16)->select();
            $result=array();
            if ($res) {
                foreach($res as $k=>$v){
                    $result[$k]['time']=$v['paytime'];
                    if($v['type']==3){
                        $userinfo=$this->get_diy_user_field($v['to_uid']);
                        $result[$k]['type']=3;//类型为礼物
                        $result[$k]['nickname']=$userinfo['nickname'];
                        //礼物数量
                        $result[$k]['count']=$v['days'];
                        //增加的魅力值
                        $result[$k]['add_money']=$v['money'];
                        //礼物图片
                        $result[$k]['gift_img']=C ("IMAGEURL").$v['account'];
                    }
                    if($v['type']==4){
                        $result[$k]['type']=4;//类型为提现记录
                        $result[$k]['account_type']=$v['account_type'];
                        //提现账户
                        $result[$k]['account']=$v['account'];
                        //提取的魅力值
                        $result[$k]['draw_money']=$v['money'];
                        //提现状态
                        $result[$k]['status']=$v['is_tixian'];
                        //提取金额
                        $result[$k]['realmoney']=intval($v['money'] *0.03* 0.2);
                    }
                }
                if($pageNum>ceil($ret["totalCount"]/$pageSize)){
                    $return['data']['list']=array();
                }else{
                $return['data']['list'] = $result;}
                $page["pagesize"] = $pageSize;
                $page["page"] = $pageNum;
                $page["totalcount"] = $ret["totalCount"];
                $return['data']["page"] = $page;
                $return['message'] = $this->L("CHENGGONG");
            } else {
                $return['data'] ='';
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L("没有历史提现或礼物信息");
            }
               // $value = json_encode($return, true);
               // $ret[]=$redis->listPush('withdrawal_order'.$uid,$value);
        }
        //else{
            //$result=$redis->listLrange('withdrawal_order'.$uid,0,-1);
           /* if ($result) {
                    $result1= json_decode($result[0], true);
                $return['data']=$result1['data'];
                $return['message']=$result1['message'];
                }*/
      //  }
        Push_data($return);

    }



}