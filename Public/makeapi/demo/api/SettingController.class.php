<?php
class SettingController extends BaseController {
	public function __construct(){
		parent::__construct();
	}
	
	/*
	 * 上传个人信息 modifyuserinfo
	 */
	public function modifyuserinfo(){
		$data = $this->Api_recive_date;

		$userinfo = $this->get_UserInfo($data);
		
		$basedata =$userinfo["base"];
		
		/* if(isset($data['nickname']))//updata
		$basedata['nickname'] = $data['nickname'];
		if(isset($data['email']))//updata
		$basedata['email'] = $data['email'];
		if(isset($data['gender']))//updata
		$basedata['gender'] = $data['gender'];
		if(isset($data['age']))//updata
		$basedata['age'] = $data['age'];
		if(isset($data['height']))//updata
		$basedata['height'] = $data['height'];
		if(isset($data['weight']))//updata
		$basedata['weight'] = $data['weight'];
		if(isset($data['area']))//updata
		$basedata['area'] = $data['area'];
		if(isset($data['income']))//updata
		$basedata['income'] = $data['income'];
		if(isset($data['marriage']))//updata
		$basedata['marriage'] = $data['marriage'];
		if(isset($data['education']))//updata
		$basedata['education'] = $data['education'];
		if(isset($data['work']))//updata
		$basedata['work'] = $data['work']; */
		
		
		$extdata = $userinfo["ext"];
		/* if(isset($data['mood']))//updata
		$extdata['mood'] = $data['mood'];	
		if(isset($data['friendsfor']))//updata
		$extdata['friendsfor'] = $data['friendsfor'];
		if(isset($data['cohabitation']))//updata
		$extdata['cohabitation'] = $data['cohabitation'];
		if(isset($data['dateplace']))//updata
		$extdata['dateplace'] = $data['dateplace'];
		if(isset($data['lovetimes']))//updata
		$extdata['lovetimes'] = $data['lovetimes'];
		if(isset($data['charactertype']))//updata
		$extdata['charactertype'] = $data['charactertype'];
		if(isset($data['hobby']))//updata
		$extdata['hobby'] = $data['hobby'];
		if(isset($data['wantchild']))//updata
		$extdata['wantchild'] = $data['wantchild'];
		if(isset($data['house']))//updata
		$extdata['house'] = $data['house'];
		if(isset($data['car']))//updata
		$extdata['car'] = $data['car'];
		if(isset($data['line']))//updata
		$extdata['line'] = $data['line'];
		if(isset($data['tinder']))//updata
		$extdata['tinder'] = $data['tinder'];
		if(isset($data['tinder']))//updata
		$extdata['tinder'] = $data['tinder'];
		if(isset($data['wechat']))//updata
		$extdata['wechat'] = $data['wechat'];
		if(isset($data['facebook']))//updata
		$extdata['facebook'] = $data['facebook']; */
		$uid = $this->uid;	
		if(!empty($basedata)){
			$User_baseM = new UserBaseModel();
			$ret = $User_baseM->updateOne(array('uid'=>$uid),$basedata);
		}
		if(!empty($extdata)){
			$User_extendM = new UserExtendModel();
			$ret = $User_extendM->updateOne(array('uid'=>$uid),$extdata);
			if(!$ret){
				$extinfo = $User_extendM->getOne(array('uid'=>$uid));
				if(!$extinfo){
					$extdata["uid"] = $uid;

					$User_extendM->addOne($extdata);
				}
			}
		}
		$return['message'] = $this->L("SHEZHICHENGGONG");
		$this->get_user($uid,1);//更新用户缓存信息
		//$return["data"]['user'] = $this->get_user($uid);
		Push_data($return);
		
	}
    private function get_UserInfo($data){
        $Userdata = array();
        $baseField = array(
            "nickname"=>"nickname",
            "email"=>"email",
            "gender"=>"gender",
            "age"=>"age",
            "height"=>"height",
            "weight"=>"weight",
            "area"=>"area",
            "income"=>"income",
            "marriage"=>"marriage",
            "education"=>"education",
            "work"=>"work",
            "password"=>"password",
            "product"=>"product",
            "phoneid"=>"phoneid",
            "country"=>"country",
            "language"=>"language",
            "version"=>"version",
            "platformnumber"=>"platformnumber",
            "fid"=>"fid",
            "systemversion"=>"systemversion"
        );
        $baseFieldKey = array_keys($baseField);
        
        $ExtField = array(
            "mood"=>"mood",
            "friendsfor"=>"friendsfor",
            "cohabitation"=>"cohabitation",
            "dateplace"=>"dateplace",
            "lovetimes"=>"lovetimes",
            "charactertype"=>"charactertype",
            "hobby"=>"hobby",
            "wantchild"=>"wantchild",
            "house"=>"house",
            "car"=>"car",
            "line"=>"line",
            "tinder"=>"tinder",
            "twitter"=>"twitter",
            "wechat"=>"wechat",
            "facebook"=>"facebook",
            "phonenumber"=>"phonenumber",
            "channelid"=>"channelid",
            "phonetype"=>"phonetype",
            "blood"=>"blood",
            "isopen"=>"isopen",
            "exoticlove"=>"exoticlove",
            "sexual"=>"sexual",
            "livewithparents"=>"livewithparents",
            "personalitylabel"=>"personalitylabel",
            "liketype"=>"liketype",
            "glamour"=>"glamour"
        );
        $ExtFieldKey = array_keys($ExtField);
        
        foreach($data as $k=>$v){
            $str = preg_replace('/($s*$)|(^s*^)/m', '',$v);  
            if($str!=""){
                if(in_array($k, $baseFieldKey)){
                    $Userdata["base"][$baseField[$k]] = $str;
                }elseif(in_array($k, $ExtFieldKey)){
                    $Userdata["ext"][$ExtField[$k]] = $str;
                }
            }
        }
        return $Userdata;
    }
}

?>