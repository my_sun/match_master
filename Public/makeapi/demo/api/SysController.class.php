<?php
class SysController extends BaseController {
	public function __construct(){
		parent::__construct();
		//$this->is_login();//检查用户是否登陆
		//$this->Api_recive_date;
	}
    public function parameterinit(){
        //$type初始化类型:1注册前;2打开后;3注册后
        $recdate = $this->Api_recive_date;
    	$platformInfo = $this->Api_recive_date['platformInfo'];
    	$type = $this->Api_recive_date['type'];//初始化类型:1登录前;2登录后;3注册后
    	$type = $type ? $type : 1;
    	$uid = $this->uid;
    	$publicdatatime = $this->Api_recive_date['publicdatatime'];
		$data = array();
		$isshow = array();
		$userinfolistobj = array();
		$publicdata = array();
		$serverpublicdatatime = $this->get_publicdatatime();//国家列表md5	
		
    	if($type==1){
    	    //1注册前
    	    $publicdata["countrylist"] = $this->get_countrylist();//国家列表
    	    $publicdata["userinfolistobj"] = $this->get_attrconf();

    	    $publicdata["publicdatatime"] = $serverpublicdatatime;
    	    $data["publicdata"] = $publicdata;
    	    $data["isshow"] = $this->get_switch(1);
    	    
    	}elseif ($type==3){
    	    //3注册后
    	    $this->is_login();//检查用户是否登陆
    	    $userInfo = $this->UserInfo;
    	    
    	    //地区
    	    $areadata["area"] = $this->get_regionlist($userInfo["country"]);
    	    $areadata["areatime"] = $this->get_regionlisttime($userInfo["country"]);
    	    $data["areadata"] = $areadata;
    	    
    	    //礼物
    	    $giftlist["giftlist"] = $this->get_giftlistls();
    	    $giftlist["giftlisttime"] = $this->get_giftlisttime();
    	    $data["giftlist"] = $giftlist;
    	    $data["isshow"] = array_merge($this->get_switch(2),$this->get_switch(3));
    	}else{
    	    //2打开后
    	    $this->is_login();//检查用户是否登陆
    	    //token 续期
    	    $redis=$this->redisconn();
    	    $redisStr = "token_".$this->Api_recive_date["token"];
    	    $redis->set($redisStr,$this->uid,0,0,C("SESSION_TIMEOUT"));
    	    
    	    $userInfo = $this->UserInfo;
    	    $areatime = $recdate["areatime"];
    	    $serverareatime = $this->get_regionlisttime($userInfo["country"]);
    	    if($serverareatime!=$areatime){
    	        $areadata["area"] = $this->get_regionlist($userInfo["country"]);
    	        $areadata["areatime"] = $serverareatime;
    	    }else{
    	        $areadata["area"] = array();
    	        $areadata["areatime"] = $serverareatime;
    	    }
    	    if($serverpublicdatatime!=$publicdatatime){
    	        $publicdata["userinfolistobj"] = $this->get_attrconf();
    	        $publicdata["countrylist"] = $this->get_countrylist();//国家列表
    	        $publicdata["publicdatatime"] = $serverpublicdatatime;
    	    }else{
    	        $publicdata["userinfolistobj"] = array();
    	        $publicdata["countrylist"] = array();
    	        $publicdata["publicdatatime"] = $serverpublicdatatime;
    	    }
    	    $data["areadata"] = $areadata;
    	    $data["publicdata"] = $publicdata;
    	    
    	    //礼物
    	    $giftlisttime = $recdate["giftlisttime"];
    	    $servergiftlisttime = $this->get_giftlisttime();
    	    
    	    if($giftlisttime!=$servergiftlisttime){
    	        $giftlist["giftlist"] = $this->get_giftlistls();
    	    }else{
    	        $giftlist["giftlist"] = array();
    	    }
    	    $giftlist["giftlisttime"] = $servergiftlisttime;
    	    $data["giftlist"] = $giftlist;
    	    
    	    $data["isshow"] = array_merge($this->get_switch(1),$this->get_switch(2));

    	    //敏感词汇
            $sensitivetime = $recdate["sensitivetime"];
            $serversensitivetime = $this->get_sensitivetime();
            if($sensitivetime!=$serversensitivetime){
                $sensitive["sensitiveword"] = $this->get_sensitive();
            }else{
                $sensitive["sensitiveword"] = array();
            }
            $sensitive["sensitivetime"] = $serversensitivetime;
            $data["sensitive"] = $sensitive;
    	}
    	$push_data = array();
    	$push_data["data"] = $data;
    	Push_data($push_data);
    }
    /**
     * 运行时间
     */
    public function clientruntime(){
        $date = $this->Api_recive_date;
        $exetime = $date["exetime"] ? $date["exetime"] : 0;
        $action = $date["action"] ? $date["action"] : 0;
        $action = urldecode($action);
        $QueryClientM = new QueryClientModel();
        $argc = base64_encode(json_encode($_POST));
        $data = array(
            "action"=>$action,
            "time"=>$exetime,
            "argc"=>$argc
        );
        $QueryClientM->addOne($data);
        Push_data(array());
    }
    /**
     * 获取开关
     */

    protected function get_switch($showtype=1){
        $user_type=0;
        if($this->uid){
            $user_type = $this->UserInfo["user_type"];
        }

        if($showtype==3){
            $where['type']= array('in','2,3');
        }else{
            $where = array("type"=>$showtype);
        }
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->listSize('switch'.$showtype);
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang==0){
            $SwitchM = new SwitchModel();
            $res = $SwitchM->getList($where);
            $return = array();
            foreach($res as $k=>$v){
                $showThis=1;
                if($user_type!=0&&$v['usertype']){
                    $usertypeArr = explode("|", $v['usertype']);
                    if(!empty($usertypeArr)){
                        if(!in_array($user_type, $usertypeArr)){
                            $showThis=0;
                        }
                    }
                }
                if($showThis==1){
                    $return[$k] = array(
                        "key"=>$v["key"],
                        "value"=>$v["value"],
                        "name"=>$v["name"],

                    );
                }
                $value=json_encode($return[$k],true);
                $ret[]=$redis->listPush('switch'.$showtype,$value);
            }
            $result1=$return;
        }else{
            $result=$redis->listLrange('switch'.$showtype,0,-1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    $result1[$k1]=json_decode($v1,true);
                }
            }
        }
        foreach ($result1 as $k2=>$v2){
            $result1[$k2]["name"] = $this->L($v2["key"]);
        }

        return $result1;
    }

    protected function get_publicdatatime($showtype=1){
        $attrtime = $this->get_attrconftime($showtype);
        $languageTime = $this->get_countrylisttime();
        return md5($attrtime.$languageTime);
    }

    /**
     * 获取自定义数组内容
     */
    protected function get_giftlisttime($product="10008"){
        $path = CHCHEPATH_GIFTLIST;
        $cache_name = 'giftlist'.$this->platforminfo["language"].$product.".php";
        return MD5(date("YmdHis",filemtime($path.$cache_name)));
    }

    //从redis中获取gift列表缓存,不存在则取数据库{哈希表}
    protected function get_giftlisthx($product="10008"){
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->hashLen('giftlist_'.$product);
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang==0){
            $GiftM = new GiftModel();
            $Dres=$GiftM->where(array("product"=>$product,"status"=>1))->select();
            $res = array();
            foreach ($Dres as $k=>$v){
                $res[$k]= array(
                    "id"=>$v["id"],
                    "title"=>$v["code"],
                    /* "title"=>$v["title"],*/
                    "content"=>$v["code"],
                    "price"=>$v["price"],
                    "url"=>C("IMAGEURL").$v["url"],
                    "animationtype"=>$v["animationtype"]
                );
                $id=$v['id'];
               $value=json_encode($res[$k],true);
                //$ret[]=$redis->listPush('giftlist_'.$product,$id.'_'.$value);
                $ret[]=$redis->hashSet('giftlist_'.$product,array($id=>$value));

            }
            $result1=$res;

        }else{
            $result=$redis->hashGet('giftlist_'.$product,null,2);
            //$result=$redis->dump('giftlist_'.$product);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    $result1[$k1]=json_decode($v1,true);
                }
            }
        }

        foreach ($result1 as $k2=>$v2){
            $result1[$k2]["title"] = $this->L($v2["title"]);
            $result1[$k2]["content"] = $this->L($v2["content"]);
        }
        return $result1;
    }
    //从redis中获取gift列表缓存,不存在则取数据库{list表}
    protected function get_giftlistls($product="10008")
    {
        $redis = $this->redisconn();
        //缓存中储存总数据
        $lang=$redis->listSize('giftlist_' . $product);
        //如果缓存中不存在，则取数据库最新数据存入
        if ($lang == 0) {
            $GiftM = new GiftModel();
            $Dres = $GiftM->where(array("product" => $product, "status" => 1))->select();
            $res = array();
            foreach ($Dres as $k => $v) {
                $res[$k] = array(
                    "id" => $v["id"],
                    "title" => $v["code"],
                    /* "title"=>$v["title"],*/
                    "content" => $v["code"],
                    "price" => $v["price"],
                    "url" => C("IMAGEURL") . $v["url"],
                    "animationtype" => $v["animationtype"]
                );
                $value = json_encode($res[$k], true);
                $ret[]=$redis->listPush('giftlist_' . $product,$value);
            }
            $result1 = $res;
        } else {
            $result=$redis->listLrange('giftlist_' . $product,0,-1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    $result1[$k1] = json_decode($v1, true);
                }
            }
        }
        foreach ($result1 as $k2=>$v2){
            $result1[$k2]["title"] = $this->L($v2["title"]);
            $result1[$k2]["content"] = $this->L($v2["content"]);
        }
        return $result1;
    }
    /**
     * 获取自定义数组内容
     */
    protected function get_attrconftime($showtype=1){
        $path = CHCHEPATH_ATTR;
        $cache_name = 'attr'.$showtype.".php";
        return date("YmdHis",filemtime($path.$cache_name));
    }
	/**
	 * 获取自定义数组内容
	 */
    protected function get_attrconf($showtype=1){
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->listSize('attrconf');
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang==0){
            $Attr = new AttrModel();
            $AttrValue = new AttrValueModel();
            $AttrList = $Attr->getList(array("showtype"=>$showtype));
            $res = array();
            foreach ($AttrList as $k=>$v){
                $option = array();
                $AttrValueList = $AttrValue->getList(array("uppercode"=>$v["code"]));
                foreach ($AttrValueList as $attrv){
                    $option[] = array(
                        "id"=>$attrv["id"],
                        "name"=>$attrv["code"]
                    );
                }
                $res[$k] = array(
                    "field"=>$v["code"],
                    "name"=>$v["code"],
                    "type"=>1,
                    "option"=>$option
                );
                $value=json_encode($res[$k],true);
                $ret[]=$redis->listPush('attrconf',$value);
            }
            $result1=$res;
        }else{
            $result=$redis->listLrange('attrconf',0,-1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    $result1[$k1]=json_decode($v1,true);
                }
            }
        }
        foreach ($result1 as $k2=>$v2){
            $result1[$k2]["name"] = $this->L($res[$k2]["name"]);
            foreach($v2["option"] as $k3=>$v3){
                $result1[$k2]["option"][$k3]["name"] = $this->L($v3["name"]);
            }
        }
        return $result1;
    }

	/**
	 * 获取自定义数组内容
	 */
	protected function get_conf($key,$langSet='traditional'){
		$ArrayClass = new ArrayModel();
		$arr = $ArrayClass->get_var($key);
		return $this->get_lang($arr);
	}
	protected function get_lang($var){
		if (is_array($var)){
			foreach($var as $k=>$v){
				if (is_string($v)) {
					$var[$k] = $this->L($v);
				}else{
					if($var[$k]['name']){
						$var[$k]['name'] = $this->L($var[$k]['name']);
					}
				}
			}
			return $var;
		}else{
			return $this->L($var);
		}
	}

    protected function get_language($reset=0){
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->listSize('language');
        //如果缓存中不存在，则取数据库最新数据存入
         if($lang==0){
             $where = array();
             $LanguageM = new LanguageModel();
             $res = $LanguageM->getList($where);
             foreach($res as$k=>$v){
                 $res[$k] = array(
                     "id"=>$v["code"],
                     "name"=>$v["name"]
                 );
                 $value=json_encode($res[$k],true);
                 $ret[]=$redis->listPush('language',$value);
             }
             $result1=$res;
         }else{
             $result=$redis->listLrange('language',0,-1);
             if ($result) {
                 foreach ($result as $k1 => $v1) {
                     $result1[$k1]=json_decode($v1,true);
                 }
             }
		}

		//Dump($newarr);exit;
        return $result1;
    }
    /**
     * 获取自定义数组内容
     */
    protected function get_countrylisttime(){
        $path = CHCHEPATH_COUNTRY;
        $cache_name = 'countrylist'.".php";
        return date("YmdHis",filemtime($path.$cache_name));
    }
    protected function get_countrylist($reset=0){
        $path = CHCHEPATH_COUNTRY;
		$cache_name = 'countrylist';
		if(F($cache_name,'',$path) && $reset == 0){
			$res = F($cache_name,'',$path);
		}else{
			$where = array("status"=>1);
			$CountryM = new CountryModel();	
			$res = $CountryM->getList($where);
			
			F($cache_name,$res,$path);
		}
    	$newarr = array();
		foreach($res as $v){
			$newarr[] = array(
				"id"=>$v["iso"],
				"name"=>$this->L($v["iso"])
			);
		}
		return $newarr;
    }
    public function name(){
    	$id = $this->Api_recive_date['id'];
    	$return['message'] = $this->L("CANSHUYICHANG");
    	$return['code'] = ERRORCODE_201;
    	if($id){
    		$regioninfo = $this->get_regionlist($id);
    		
    		$push_data = array();
    		$push_data["data"]["region"] = $regioninfo;
    	}
    	
    	Push_data($push_data);
    }
    protected function get_regionlisttime($id){
        $path = CHCHEPATH_AREA;
        $cache_name = 'regionlist_'.$id.".php";
        return md5(date("YmdHis",filemtime($path.$cache_name)));
    }

    protected function get_regionlist($id,$reset=0){
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->listSize('regionlist_'.$id);
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang==0){
            $where = array("country_codeiso"=>$id,"level"=>1);
            $RegionM = new RegionModel();
            $res = $RegionM->getList($where);
             foreach($res as $k=>$v){
                 $res[$k] = array(
                     "id"=>$v["id"],
                     "name"=>$v["code"]
                 );

                 $value=json_encode($res[$k],true);
                 $ret[]=$redis->listPush('regionlist'.$id,$value);
             }
            $result1=$res;
            //F($cache_name,$res,$path);
        }else{
            $result=$redis->listLrange('regionlist'.$id,0,-1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    if($result[$k1]==null){
                        continue;
                    }
                    $result1[$k1]=json_decode($v1,true);
                }
            }
        }

        foreach ($result1 as $k2=>$v2){
            $result1[$k2]["name"] = $this->L($v2["name"]);
        }
        return $result1;
    }

    //获取敏感词
    protected function get_sensitive($reset=0){
        $path = CHCHEPATH_SENSITIVE;
        $cache_name = 'sensitiveword';
        if(F($cache_name,'',$path) && $reset == 0){
            $newarr = F($cache_name,'',$path);
        }else{
            $where = array();
            $sensitiveM=M('sensitive_word');
            $res = $sensitiveM->where($where)->select();
            $newarr = array();
            foreach($res as $v){
                $newarr[] = array(
                    "name"=>$v["name"]
                );
            }
            F($cache_name,$newarr,$path);
        }
        return $newarr;
    }
    protected function get_sensitivetime(){
        $path = CHCHEPATH_SENSITIVE;
        $cache_name = 'sensitiveword'.".php";
        return date("YmdHis",filemtime($path.$cache_name));
    }
	/*
	 * 上传屏幕截图
	 */
    public function upload_screenshot(){
        $recdate = $this->Api_recive_date;
        $fileurl = WR.'/userdata/screenshot/'.$this->uid."/";
        if($recdate["file"]){
            createDir($fileurl);
            file_put_contents($fileurl.time()."-file.jpg", base64_decode($recdate["file"]));
            Push_data();
        }
        if(!empty($_FILES)){
            //$_FILES['file']['type'] = 'image/jpeg';
            //Dump($_FILES);exit;
            
            createDir($fileurl);
            $uploadList = $this->_upload($fileurl);
            if(!empty($uploadList)){
             
                
            }
        }
        Push_data();
    }
    // 文件上传
    protected function _upload($savePath) {
        import("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir($savePath);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 3292200;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg();
            Push_data($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            return $uploadList;;
        }
        
    }
}

?>
