<?php
/*
 * 动态
 */
class DynamicController extends BaseController {
	public function __construct(){
		parent::__construct();
		//$this->is_login();//检查用户是否登陆
		//echo date("Y-m-d",time());exit;
		$this->msgfileurl = '/userdata/dynamicfile/'.date('Ymd',time())."/";
		$this->cachepath = WR.'/userdata/cache/dynamic/';
		
	}
	public function index(){
		echo 0;exit;
	}	
	/*
	 * 发布动态DynamicComment
	 */
	public function add(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$type = $data["type"] ? $data["type"] : 1;
		$content = $data["content"] ? $data["content"] : "";
		$ltime = $data["ltime"] ? $data["ltime"] : 0;
		$filetype = "image";
		if($type==3){
			$filetype = "video";
		}
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("SHANGCHUANSHIBAI");
		$dynamicM = new DynamicModel();
		$dynamic = array();
		if($type==1){
			if($content){
				$addArr = array();
				$addArr["content"] = $content;
				$addArr["uid"] = $uid;
				$addArr["time"] = time();
				$addArr["type"] = $type;
				$dynamicid = $dynamicM->addOne($addArr);
				$return = array();
				$dynamic = array(
					"id"=>$dynamicid, 
					"content"=>$content,
					"type"=>$type,
					"time"=>$addArr["time"],
					"obj"=>"",
					"comment"=>array()
				);
				$this->get_dynamiclist(0,0,0,1);//清空动态缓存
			}else{
				$return = array();
				$return['code'] = ERRORCODE_201;
				$return['message'] = $this->L("PINGLUNNEIRONGBUNENGWEIKONG");		
			}
		}else{
			if(!empty($_FILES)){
				
				$addArr = array();
				$addArr["content"] = $content;
				$addArr["uid"] = $uid;
				$addArr["time"] = time();
				$addArr["type"] = $type;
				
				$dynamicid = $dynamicM->addOne($addArr);
					//$_FILES['file']['type'] = 'image/jpeg';
					$saveUrl = $this->msgfileurl.$filetype."/";
					$savePath = WR.$saveUrl;
					//echo $savePath;exit;
					$uploadList = $this->_upload($savePath);
					//Dump($uploadList);exit;
					if(!empty($uploadList)){
						$obj = array();
						$PutOssarr = array();
						$returnurlarr = array();
						$ids = 0;
						if($type==3){
							$imageurlfile = array();
							$videofile = array();
							foreach($uploadList as $k=>$v){
								if($v["key"]=="imageurl"){
									$imageurlfile = $v;
								}else{
									$videofile = $v;
								}
							}
							$VideoDynamicM = new VideoDynamicModel();
							$Indata = array();
							$Indata['danamicid'] = $dynamicid;
							$Indata['uid'] = $uid;
							$Indata['type'] = $type;
							$Indata['ltime'] = $ltime;
							$Indata['imageurl'] = $saveUrl.$imageurlfile['savename'];//
							$Indata['url'] = $saveUrl.$videofile['savename'];
							$ret = $VideoDynamicM->addOne($Indata);
							$ids = $ret;
							$PutOssarr[] = $Indata['url'];
							$PutOssarr[] = $Indata['imageurl'];
							$obj = array(
								"id"=>$ret,
								"ltime"=>$ltime,
								"url"=>C("IMAGEURL").$Indata['url'],
								"imageurl" =>C("IMAGEURL").$Indata['imageurl'],
								"status"=>1
							);
						}else{
							$ids = array();
							$PhotoDynamicM = new PhotoDynamicModel();
							foreach($uploadList as $k=>$v){
								$Indata = array();
								$Indata['danamicid'] = $dynamicid;
								$Indata['uid'] = $uid;
								$Indata['type'] = $type;//1头像，2相册，3消息图片
								$Indata['url'] = $saveUrl.$v['savename'];
								$ret = $PhotoDynamicM->addOne($Indata);
								$ids[] = $ret;
								$PutOssarr[] = $Indata['url'];
								$obj[] = array(
									"id"=>$ret,
									"url"=>C("IMAGEURL").$Indata['url'],
									"thumbnaillarge"=>C("IMAGEURL").$Indata['url'],
									"thumbnailsmall"=>C("IMAGEURL").$Indata['url'],
									"status"=>1,
									"seetype"=>1
								);
							}
							$ids = implode("|", $ids);
						}
						$dynamicM->updateOne(array("id"=>$dynamicid), array("ids"=>$ids));
						PutOss($PutOssarr);
						//Dump($PutOssarr);exit;
						$return = array();
						$dynamic = array(
							"id"=>$dynamicid,
							"content"=>$content,
							"type"=>$type,
							"time"=>$addArr["time"],
							"obj"=>$obj,
							"comment"=>array()
						);
					}
					$this->get_dynamiclist(0,0,0,1);//清空动态缓存
			}else{
				$return = array();
				$return['code'] = ERRORCODE_201;
				$return['message'] = $this->L("SHANGCHUANDEWENJIANBUNENGWEIKONG");			
			}
		}
		$return['data']["dynamic"]=$dynamic;
		//Dump($return);
		Push_data($return);
	}
	
	// 文件上传
	protected function _upload($savePath) {
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($savePath);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize = 5242880;
        //$upload->maxSize = 6291456;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
		//设置附件上传目录
		$upload->savePath = $savePath;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
		$upload->saveRule = "md5content";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			return $uploadList;;
		}

	}
	
		/*
	 * 删除动态delete
	 */
	public function delete(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id){
			$return = array();
			$dynamicM = new DynamicModel();
			$where = array("id"=>$id);
			$DyInfo = $dynamicM->getOne($where);
			
			if($DyInfo["type"]==2){
				//删除照片
				$PhotoDynamicM = new PhotoDynamicModel();
				$ids = explode("|", $DyInfo["ids"]);
				foreach ($ids as $v){
					$PhotoInfo = $PhotoDynamicM->getOne(array("id"=>$v));
					_unlink(WR.$PhotoInfo["url"]);
					DelOss(array($PhotoInfo["url"]));
				}
				$PhotoDynamicM->delOne(array("danamicid"=>$id));
				
			}elseif($DyInfo["type"]==3){
				//删除视频
				$VideoDynamicM = new VideoDynamicModel();
				$VideoInfo = $VideoDynamicM->getOne(array("id"=>$DyInfo["ids"]));
				_unlink(WR.$VideoInfo["imageurl"]);
				_unlink(WR.$VideoInfo["url"]);
				DelOss(array($VideoInfo["imageurl"],$VideoInfo["url"]));
				$VideoDynamicM->delOne(array("danamicid"=>$id));
				//Dump($VideoInfo);exit;
			}
			//删除评论
			$DynamicCommentM = new DynamicCommentModel();
			$DynamicCommentM->delOne(array("danamicid"=>$id));
			//删除动态
			$dynamicM->delOne($where);
			$this->get_dynamiclist(0,0,0,1);//清空动态缓存
		}
		Push_data($return);
	}
	/*
	 * 动态评论 addcomment
	 */
	public function addcomment(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$id = $data["id"] ? $data["id"] : 0;
        $clientid=$data['clientid'];
        //$fileName= WR."/userdata/cache/a.txt";
		$content = $data["content"] ? $data["content"] : "";
        //$content=Filter_word($content,$fileName);
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id&&$content){
			$return = array();
			$DynamicCommentM = new DynamicCommentModel();
            if(isset($data['touid'])){
                $addArr = array(
                    "danamicid"=>$id,
                    "content"=>$content,
                    "time"=>time(),
                    "uid"=>$uid,
                    "touid"=>$data['touid'],
                    "clientid"=>$clientid
                );
            }else{
                $addArr = array(
                    "danamicid"=>$id,
                    "content"=>$content,
                    "time"=>time(),
                    "uid"=>$uid,
                    "clientid"=>$clientid
                );
            }

            $result=$DynamicCommentM->addOne($addArr);
			$this->get_comment($id,1);//清空动态评论
		}
		Push_data($return);
	}
	/*
	 * 动态评论删除 deletecomment
	 */
	public function deletecomment(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$danamicid=$data['danamicid'];
        $clientid=$data['clientid'];
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id){
	
		$return = array();
			$DynamicCommentM = new DynamicCommentModel();
			/*$data = array(
				"id"=>$id,
                '_logic'=> 'or',
				"clientid"=>$clientid,
				"uid"=>$uid
			);*/
            $map['id']=$id;
            $map['clientid']=$clientid;
            $map['_logic'] = 'or';
            $final['_complex'] = $map;
            $final['uid']=$uid;
            //$result=$DynamicCommentM->getOne($data);
            //$danamicid=$result['danamicid'];
			$result=$DynamicCommentM->delOne($final);
			if($result==0){
			    $return['message']='没有更新条数';
            }
			$this->get_comment($danamicid,1);//清空动态评论
		}
		Push_data($return);
	}
	
	/*
	 * dynamiclist
	 */
	public function dynamiclist(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$type = $data["type"] ? $data["type"] : 1;//1全部动态2和我有关的3获取指定用户动态
		$touid = $data["touid"] ? $data["touid"] : 0;//
		$page = $data["page"] ? $data["page"] : 1;//
		$pagesize = $data["pagesize"] ? $data["pagesize"] : 15;//
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		$return = array();

		if($type==2){
				$where = array();
				$where["uid"] = $uid;
				$where["status"]=10;
            $DynamiclistTemp = $this->get_dynamiclist($where,$page,$pagesize);
		}elseif($type==3){
				$where = array();
				$where["uid"] = $touid;
            $DynamiclistTemp = $this->get_dynamiclist($where,$page,$pagesize);
		}else {
            $where = array();
            $DynamiclistTemp= $this->get_dynamiclist1($page, $pagesize);
            if ($DynamiclistTemp) {
                foreach ($DynamiclistTemp['zlist'] as $k1 => $v1) {
                    $DynamiclistTemp['list'][$k1]["id"]=substr($v1,0,strpos($v1, '_'));
                    $data1=substr($v1,strpos($v1, '_')+1);
                    $DynamiclistTemp['list'][$k1]=json_decode($data1,true);
                }
            }
            $DynamiclistTemp["pageSize"]=$pagesize;
            $DynamiclistTemp["page"]=$page;
        }
		if(!empty($DynamiclistTemp)){
            $Dynamiclist=$this->dynamictemp($DynamiclistTemp,$uid,$userfield);
				}
		$return_data["dynamiclist"]=$Dynamiclist;
		$return_data["page"]=array(
				"pagesize"=>$DynamiclistTemp["pageSize"],
				"page"=>$DynamiclistTemp["page"],
				"totalcount"=>$DynamiclistTemp["totalCount"]
			);
		$return["data"] = $return_data;
		//Dump($return);
		Push_data($return);
	}
	//循环遍历动态，加载相片和视频
    public function dynamictemp($DynamiclistTemp,$uid,$userfield){
            foreach($DynamiclistTemp["list"] as $k=>$v){
                $obj = array();
                if($v["type"]==3){
                    if($v["ids"])
                        $obj[] = $this->get_videodynamic($v["ids"]);
                }elseif($v["type"]==2){
                    $ids = explode("|", $v["ids"]);
                    foreach ($ids as $v1){
                        if($v1)
                            $obj[] = $this->get_photodynamic($v1);
                    }
                }
                $comment = $this->get_comment($v["id"]);
                $support = $this->get_support($v["id"]);
                $issupport =0;
                foreach($support as $sv){
                    if($sv["user"]["uid"]==$uid){
                        $issupport = 1;
                        break;
                    }
                }
                $user = $this->get_user($v["uid"]);
                $user = $this->get_user_field($user,$userfield);
                $Dynamiclist[]=array(
                    "id"=>$v["id"],
                    "content"=>$v["content"],
                    "time"=>$v["time"],
                    "type"=>$v["type"],
                    "obj"=>$obj,
                    "comment"=>$comment,
                    "support"=>$support,
                    "issupport"=>$issupport,
                    "user"=>$user,
                    "status"=>$v["status"]
                );
            }
            return $Dynamiclist;
    }
	/*
	 * 获取自己和指定人的动态列表
	 */
	protected function get_dynamiclist($where,$page,$pagesize,$reset=0){
	    $path = $this->cachepath."dynamiclist/";
	    if($where["status"]==10){
	        unset($where["status"]);
	    }else{
	        $where["status"] = 1;
	    }
	    $cache_name = 'dynamiclist'.md5(json_encode($where).$page.$pagesize);
	    if($reset==1){
	        //return F($cache_name,NULL,$path);
	        return deldir($path);
	    }
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $DynamicM = new DynamicModel();
	        $res = $DynamicM->getListPage($where,$page,$pagesize);
	        F($cache_name,$res,$path);
	    }
	    return $res;
	}


	//从redis中获取所有人的动态列表缓存
    protected function get_dynamiclist1($page,$pagesize){
        $redis=$this->redisconn();
        //$redis->delete('dynamic');
        //缓存中储存总数据
        $lang=$redis->listSize('dynamic');
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang==0){
            $DynamicM = new DynamicModel();
            //限制50条
            $Dres=$DynamicM->where(array('status'=>1))->order('time')->limit(150)->select();
        foreach($Dres as $k1=>$v1){
            $value=json_encode($v1,true);
            $id=$v1['id'];
            $ret[]=$redis->listPush('dynamic',$id.'_'.$value);

            
            }
            $lang=$redis->listSize('dynamic');
        }
        //页总数
        $page_count = ceil($lang/$pagesize);
	    $result['zlist']=$redis->listLrange('dynamic',($page-1)*$pagesize,(($page-1)*$pagesize+$pagesize-1));
	    $result['totalCount']=$lang;
       return $result;
    }
	//获取评论
	public function get_comment($id,$reset='0'){
	    $path = $this->cachepath."comment/";
	    $cache_name = 'comment_'.$id;
	    if($reset == 1){
	        return F($cache_name,NULL,$path);
	    }
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $DynamicCommentM = new DynamicCommentModel();
	        $temp = $DynamicCommentM->getList(array("danamicid"=>$id));
	        $res = array();
	        foreach($temp as $k=>$v){
	           // if($temp[$k]['status']==1) {
                    $user = $this->get_user ($v["uid"]);
                    $user = $this->get_user_field ($user, "uid|head|nickname");
                    $touser = $this->get_user ($v["touid"]);
                    $touser = $this->get_user_field ($touser, "uid|head|nickname");
                    $res[] = array(
                        "id" => $v["id"],
                        "user" => $user,
                        "touser" => $touser,
                        "content" => $v["content"],
                        "time" => $v["time"],
                        "status"=>$temp[$k]['status']
                    );
                //}
	        }
	        F($cache_name,$res,$path);
	    }
        $res=$res;
	    return $res;
	}
	//获取点赞
	public function get_support($id,$reset='0'){
	    $path = $this->cachepath."support/";
	    $cache_name = 'support'.$id;
	    if($reset == 1){
	        return F($cache_name,NULL,$path);
	    }
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $DynamicCommentM = new DynamicSupportModel();
	        $temp = $DynamicCommentM->getList(array("danamicid"=>$id));
	        $res = array();
	        foreach($temp as $k=>$v){
	            $user = $this->get_user($v["uid"]);
	            $user = $this->get_user_field($user,"uid|head|nickname");
	            $res[] = array(
	                "id"=>$v["id"],
	                "user"=>$user,
	                "time"=>$v["time"]
	            );
	        }
	        F($cache_name,$res,$path);
	    }
	    return $res;
	  
	}
	//获取视频
	public function get_videodynamic($id,$reset='0'){
		$path = WR.'/userdata/cache/dynamic/video/';
		$cache_name = 'videoinfo_'.$id;
		if(F($cache_name,'',$path) && $reset == 0){
			$Info = F($cache_name,'',$path);
		}else{
			$VideoDynamicM = new VideoDynamicModel();
			$videoTemp = $VideoDynamicM->getOne(array("id"=>$id));
			$Info=array(
				"id"=>$videoTemp["id"],
				"ltime"=>$videoTemp["ltime"],
				"url"=>C("IMAGEURL").$videoTemp["url"],
				"imageurl"=>C("IMAGEURL").$videoTemp["imageurl"],
				"status"=>$videoTemp["status"]
			);
			F($cache_name,$Info,$path);
		}
		return $Info;
	}
	//获取动态图片
	public function get_photodynamic($id,$reset='0'){
		$path = WR.'/userdata/cache/dynamic/photo/';
		$cache_name = 'photoinfo_'.$id;
		if(F($cache_name,'',$path) && $reset == 0){
			$Info = F($cache_name,'',$path);
		}else{
			$PhotoDynamicM = new PhotoDynamicModel();
			$Temp = $PhotoDynamicM->getOne(array("id"=>$id));
			$Info=array(
				"id"=>$Temp["id"],
				"url"=>C("IMAGEURL").$Temp["url"],
				"thumbnaillarge"=>C("IMAGEURL").$Temp["url"],
				"thumbnailsmall"=>C("IMAGEURL").$Temp["url"],
				"status"=>$Temp["status"],
				"seetype"=>$Temp["seetype"]
			);
			F($cache_name,$Info,$path);
		}
		return $Info;
	}

	/*
	 * addsupport
	 */
	public function addsupport(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id){
			$return = array();
			$DynamicSupportM = new DynamicSupportModel();
			$addArr = array(
				"danamicid"=>$id,
				"time"=>time(),
				"uid"=>$uid
			);
			//$temp = $DynamicSupportM->getOne(array("uid"=>$uid,"danamicid"=>$id));
			//if(!$temp){
				$DynamicSupportM->addOne($addArr);
			//}
				$this->get_support($id,1);//清空点赞缓存
		}
		Push_data($return);
	}
	/*
	 * deletesupport
	 */
	public function deletesupport(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id){
			$return = array();
			$DynamicSupportM = new DynamicSupportModel();
			$data1 = array(
				"danamicid"=>$id,
				"uid"=>$uid
			);
			$result=$DynamicSupportM->delOne($data1);
			if(!$result){
			    echo "删除失败";
            }

			$this->get_support($id,1);//清空点赞缓存
		}
		Push_data($return);
	}
}
?>