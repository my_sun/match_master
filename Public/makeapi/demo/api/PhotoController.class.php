<?php
class PhotoController extends BaseController {
	public function __construct(){
		parent::__construct();
		//$this->is_login();//检查用户是否登陆
	}
	//设为头像
	public function setuserhead(){
			$data = $this->Api_recive_date;
			$uid = $this->uid;
			$id = $data['id'];

			if($uid&&$id){
				$UserPhotoM = new PhotoModel();
				$UserPhotoM->updateOne(array('uid'=>$uid,"type"=>"2"),array("type"=>"1"));
				$UserPhotoM->updateOne(array('uid'=>$uid,"id"=>$id),array("type"=>"2"));
				$this->get_user($uid,1);//更新用户缓存信息
			}
		$return = array();
		$return['message'] = $this->L("SHEZHICHENGGONG");
		Push_data($return);
	}
	/*
	 * 上传照片
	 */
	public function upload(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		
		$type = $data["type"] ? $data["type"] : 1;	
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("SHANGCHUANSHIBAI");
		
		if(!empty($_FILES)){
			//$_FILES['file']['type'] = 'image/jpeg';
				//Dump($_FILES);exit;
				$uploadList = $this->_upload();
				$savepath = $this->get_userurl($uid, 'photos').$uid.'/';
				
				if(!empty($uploadList)){
				    $retImage = array();
					$UserPhotoM = new PhotoModel();
					$PutOssarr = array();
					foreach($uploadList as $k=>$v){
						/*
						 * 图片信息写入数据库
						*/
						$Indata = array();
						$Indata['uid'] = $uid;
						$Indata['type'] = $type;//2头像，1相册，3消息图片
						$Indata['url'] = $savepath.$v['savename'];
						$Indata['time'] = time();
						//echo $Indata['url'];
						$PutOssarr[] = $Indata['url'];
						if($type==2){
							//头像
							$rel1=$UserPhotoM->updateOne(array('uid'=>$uid,"type"=>"2"),array("type"=>"1"));
							$ret = $UserPhotoM->addOne($Indata);
                            $this->get_user_photo_all($uid,1);//更新相册缓存
                            $this->get_user_photo($uid,1);//更新相册缓存
							$this->get_user($uid,1);//更新用户缓存信息
						}else{
							$ret = $UserPhotoM->addOne($Indata);
							$this->get_user_photo($uid,1);//更新相册缓存
                            $this->get_user_photo_all($uid,1);//更新相册缓存
						}
						
						$image = array();
						$image['id']=$ret;
						$image['url']=C("IMAGEURL").$Indata["url"];//原图url
						$image['thumbnaillarge']=C("IMAGEURL").$Indata["url"];//大图url
						$image['thumbnailsmall']=C("IMAGEURL").$Indata["url"];//小图url
						$image['status']=2;//状态1审核通过;2正在审核3审核未通过已删除
						$image['seetype'] =1; //可见级别(1所有用户可见,2会员可见)
						$retImage [] = $image;
					}
					PutOss($PutOssarr);
					$return = array();
					$return["data"]=$retImage;
					
				}
		}else{
		    $return['message'] = "没有接收到需要上传的文件！";
		}
		
		Push_data($return);
	}
	/*
	 * 删除图片 /photo/deleteImg
	*/
	public function delete(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$imgid = $data['id'];
		$Deldata = array();
		$Deldata['uid'] = $uid;
		$Deldata['id'] = $imgid;
	
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray['message'] = $this->L("SHANCHUCHENGGONG");
	
		$UserPhotoM = new PhotoModel();
		$imgs = $UserPhotoM->getOne($Deldata);
		
		/**
		 删除数据库
		 */
		$return = $UserPhotoM->delOne($Deldata);
		_unlink(WR.$imgs['url']);
		DelOss($imgs['url']);
		$uinfo = $this->get_user($uid,1);//更新用户缓存信息
        $this->get_user_photo($uid,1);//更新相册缓存\
        $this->get_user_photo_all($uid,1);//更新相册缓存
		Push_data($RetArray);

  }
	// 文件上传
	protected function _upload() {

		$data = $this->Api_recive_date;
		$uid = $this->uid;
		
		$user_path = $this->get_userpath($uid, 'photos').$uid.'/';
		//echo $user_path;exit;
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($user_path);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize = 3292200;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg');
		//设置附件上传目录
		$upload->savePath = $user_path;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
			$upload->saveRule = "md5name";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			//$uploadList = $uploadList[0];
			//$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
			//$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;
			
			return $uploadList;
			//import("Extend.Library.ORG.Util.Image");
			//给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
			//Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
			//$_POST['image'] = $uploadList[0]['savename'];
		}

	}
}

?>