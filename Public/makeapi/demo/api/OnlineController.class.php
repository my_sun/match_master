<?php
class OnlineController extends BaseController {
	public function __construct(){
		
	}
	//设置在线用户
	public function setUserOnline($userinfo){
	    $uid = $userinfo["uid"];
	    $procuct = $userinfo["product"];
	    $subproduct = substr($procuct, 0, 2);
	    if($subproduct=="20"){
	        $procuct="20110";
	    }
	    $user_type = $userinfo["user_type"];
	    if(!in_array($user_type, array(1,3))){
	        $user_type = 1;
	    }
	    
	    $gender = $userinfo["gender"];
	    if(!in_array($gender, array(1,2))){
	        $gender = 1;
	    }
	    $useronlinelistkey = "useronlinelist_".$procuct."_".$user_type."_".$gender;
	    $redis=$this->redisconn();
	    $redis->listRemove($useronlinelistkey,$uid,0);
	    $redis->listPush($useronlinelistkey,$uid);
	    $totalCount = $redis->listSize($useronlinelistkey);
	    if($totalCount>1000){
	        $redis->listPop($useronlinelistkey,1);
	    }
	    return true;
	}
	//获取在线用户
	public function getUserOnline($procuct,$user_type,$gender,$pageNum=1,$pageSize=30){
	    $redis=$this->redisconn();
	    $useronlinelistkey = "useronlinelist_".$procuct."_".$user_type."_".$gender;
	    $useronlinelisttimekey = $useronlinelistkey."time";
	    $alluseronlinelistkey = "useronlinelist";
	    
	    $ret = array();
	    
	     if(!$redis->exists($useronlinelisttimekey)){
	         
	        $alluseronlinelistkey = "useronlinelist";
	        $allonlinelist = $redis->listGet($alluseronlinelistkey, 0, -1);
	        $useronlinelist = $redis->listGet($useronlinelistkey, 0, -1);
	        foreach ($useronlinelist as $k=>$v){
	            
	            if(!in_array($v, $allonlinelist)){
	                $redis->listRemove($useronlinelistkey,$v);
	            }
	        }
	        $redis->set($useronlinelisttimekey,$useronlinelistkey,0,0,60*5);
	    } 
	    if($redis->exists($useronlinelistkey)){
	        $totalCount = $redis->listSize($useronlinelistkey);
	        $pagestart = $pageNum*$pageSize-$pageSize;
	        $pageend = $pagestart+$pageSize-1;
	        $apgecount = ceil($totalCount/$pageSize);
	        if($pageNum<=$apgecount){
	            
	            $ret["uids"] = $redis->listGet($useronlinelistkey, $pagestart, $pageend);
	            
	        }
	        $ret["totalCount"] = $totalCount;
	    }
	    
	    return $ret;
	}
	//获取随机在线用户
	public function getRandUserOnline($procuct,$user_type,$gender){
	    $redis=$this->redisconn();
	    $useronlinelistkey = "useronlinelist_".$procuct."_".$user_type."_".$gender;
	    $uid = false;
	    if($redis->exists($useronlinelistkey)){
	        $totalCount = $redis->listSize($useronlinelistkey);
	        
	        $uids = $redis->listGet($useronlinelistkey, 0, -1);
	        $uid = $uids[rand(0,$totalCount-1)];
	            
	    }
	    
	    return $uid;
	}
}

?>