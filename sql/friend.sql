-- phpMyAdmin SQL Dump
-- version 4.0.10.17
-- https://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2016-11-05 16:49:23
-- 服务器版本: 5.1.73
-- PHP 版本: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `friend`
--

-- --------------------------------------------------------

--
-- 表的结构 `t_answer`
--

CREATE TABLE IF NOT EXISTS `t_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(200) DEFAULT NULL COMMENT '答案内容,',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_array`
--

CREATE TABLE IF NOT EXISTS `t_array` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg`
--

CREATE TABLE IF NOT EXISTS `t_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `box_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `msgType` tinyint(4) DEFAULT '1' COMMENT '消息类型（1->对方文本消息，2->对方语音消息，3->自己文本消息，4->自己语音消息,8->qa问答信,9->小助手问题，10->小助手答案, 11->qa解锁信）,',
  `writeMsgType` tinyint(3) DEFAULT NULL COMMENT '写信类型，参考上述定义值,',
  `qaAnswerId` int(11) DEFAULT '0' COMMENT 'qa问答，答案id,（如果为qa问答则返回该值，默认为0）',
  `content` varchar(200) DEFAULT NULL COMMENT '写信内容',
  `is_read` tinyint(1) DEFAULT '0' COMMENT '1,已读消息，0，未读消息',
  `createDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box`
--

CREATE TABLE IF NOT EXISTS `t_msg_box` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_1`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_2`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_3`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_4`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_5`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_6`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_7`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_8`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_9`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_10`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_11`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_12`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_13`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_13` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_14`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_14` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_15`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_16`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_16` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_17`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_17` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_18`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_18` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_19`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_19` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_msg_box_20`
--

CREATE TABLE IF NOT EXISTS `t_msg_box_20` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isRead` tinyint(1) DEFAULT '0' COMMENT '是否已读，1->已读，0->未读,',
  `type` tinyint(2) DEFAULT '1' COMMENT '"type":消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time":收到新消息的时间,格式：yyyy-MM-dd HH:mm:ss,',
  `language` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `uid_str` varchar(50) DEFAULT NULL,
  `msg` varchar(200) DEFAULT NULL,
  `audioUrl` varchar(200) DEFAULT NULL,
  `audioTime` varchar(10) DEFAULT NULL,
  `sys_msg_id` int(11) DEFAULT '0',
  `user_reply_time` int(11) DEFAULT '0',
  `sys_reply_time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_pay`
--

CREATE TABLE IF NOT EXISTS `t_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(50) DEFAULT NULL COMMENT '语言，参见“国家和语言定义”,',
  `country` varchar(50) DEFAULT NULL COMMENT '国家，参见“国家和语言定义',
  `phoneTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT ':手机local时间，格式为"YYYYMMDD HH:MM:SS",比如"20151223 22:01:59"',
  `userId` int(11) DEFAULT NULL COMMENT '用户id',
  `beanNumber` float DEFAULT NULL COMMENT '豆币数',
  `dayNumber` int(11) DEFAULT NULL COMMENT '包月',
  `appleId` varchar(100) DEFAULT NULL COMMENT '商户id（收款账号）',
  `userAppleId` varchar(100) DEFAULT NULL COMMENT '个人苹果id',
  `payTime` timestamp NULL DEFAULT NULL COMMENT '支付时间',
  `transactionId` varchar(50) DEFAULT NULL COMMENT '订单id（orderId）',
  `token` varchar(50) DEFAULT NULL COMMENT '是loveiosback中传过来的token',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_push_user`
--

CREATE TABLE IF NOT EXISTS `t_push_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'timemsg',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `pushtime` int(11) NOT NULL DEFAULT '0' COMMENT '推送的时间',
  `time` int(11) NOT NULL COMMENT '发出推送的时间',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收消息的用户id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=74 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_question`
--

CREATE TABLE IF NOT EXISTS `t_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(200) DEFAULT NULL COMMENT '问题内容,',
  `strategyType` varchar(50) DEFAULT NULL COMMENT '类型（兴趣爱好、个性特征、生活习惯、恋爱情感、价值观）',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_sys_msg`
--

CREATE TABLE IF NOT EXISTS `t_sys_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `msgkey` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `levle` tinyint(2) NOT NULL DEFAULT '1',
  `value` text COLLATE utf8_unicode_ci,
  `name_md5` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `lang` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'tw',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1996 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_askphoto`
--

CREATE TABLE IF NOT EXISTS `t_user_askphoto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MyID` int(11) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后访问时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base`
--

CREATE TABLE IF NOT EXISTS `t_user_base` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1：普通注册用户；2：采集用户；3：陪聊用户;4:管理员',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100001 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_1`
--

CREATE TABLE IF NOT EXISTS `t_user_base_1` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100761 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_2`
--

CREATE TABLE IF NOT EXISTS `t_user_base_2` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100762 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_3`
--

CREATE TABLE IF NOT EXISTS `t_user_base_3` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100763 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_4`
--

CREATE TABLE IF NOT EXISTS `t_user_base_4` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100764 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_5`
--

CREATE TABLE IF NOT EXISTS `t_user_base_5` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100765 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_6`
--

CREATE TABLE IF NOT EXISTS `t_user_base_6` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100766 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_7`
--

CREATE TABLE IF NOT EXISTS `t_user_base_7` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100767 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_8`
--

CREATE TABLE IF NOT EXISTS `t_user_base_8` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100768 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_9`
--

CREATE TABLE IF NOT EXISTS `t_user_base_9` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100769 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_10`
--

CREATE TABLE IF NOT EXISTS `t_user_base_10` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100770 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_11`
--

CREATE TABLE IF NOT EXISTS `t_user_base_11` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100771 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_12`
--

CREATE TABLE IF NOT EXISTS `t_user_base_12` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100772 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_13`
--

CREATE TABLE IF NOT EXISTS `t_user_base_13` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100753 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_14`
--

CREATE TABLE IF NOT EXISTS `t_user_base_14` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100754 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_15`
--

CREATE TABLE IF NOT EXISTS `t_user_base_15` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100755 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_16`
--

CREATE TABLE IF NOT EXISTS `t_user_base_16` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100756 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_17`
--

CREATE TABLE IF NOT EXISTS `t_user_base_17` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100757 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_18`
--

CREATE TABLE IF NOT EXISTS `t_user_base_18` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100758 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_19`
--

CREATE TABLE IF NOT EXISTS `t_user_base_19` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100759 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_base_20`
--

CREATE TABLE IF NOT EXISTS `t_user_base_20` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(15) NOT NULL COMMENT '密码',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别(0男性，1女性,2其他）',
  `age` tinyint(4) NOT NULL DEFAULT '0' COMMENT '年龄',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `height` smallint(4) DEFAULT '0' COMMENT '身高',
  `weight` smallint(6) DEFAULT '0' COMMENT '体重',
  `regTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `platform` tinyint(4) DEFAULT '0' COMMENT '注册平台号platform （1-->ios ,2-->Android）',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `phoneTime` varchar(20) DEFAULT NULL COMMENT '手机时间（用户所在地当前时间）',
  `product_id` int(10) DEFAULT '0' COMMENT '产品号',
  `product` tinyint(4) DEFAULT NULL COMMENT '1->约爱，2->恋爱神器,3->约会',
  `token` varchar(80) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL COMMENT '渠道号',
  `version` varchar(30) DEFAULT NULL COMMENT '版本号',
  `phonetype` varchar(30) DEFAULT NULL COMMENT '手机型号',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  `pid` varchar(80) DEFAULT NULL COMMENT '手机串号',
  `area` tinyint(4) DEFAULT NULL COMMENT '地区',
  `education` tinyint(2) DEFAULT NULL COMMENT '学历',
  `income` tinyint(2) DEFAULT NULL COMMENT '收入id',
  `marriage` tinyint(2) DEFAULT NULL COMMENT '婚姻状况',
  `monologue` varchar(50) DEFAULT NULL COMMENT '内心独白',
  `work` tinyint(4) DEFAULT NULL COMMENT '职业',
  `blood` tinyint(4) DEFAULT NULL COMMENT '血型',
  `star` tinyint(4) DEFAULT NULL COMMENT '星座',
  `listHobby` varchar(30) DEFAULT NULL COMMENT '"兴趣爱好int","兴趣爱好int"',
  `wantBaby` tinyint(2) DEFAULT NULL COMMENT '是否要小孩',
  `isBeanUser` tinyint(1) DEFAULT '0' COMMENT '1->表示为豆币用户,0->未开通,',
  `isMonthly` tinyint(1) DEFAULT '0' COMMENT '1->表示开通“写信包月”,0->未开通',
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `dayCount` int(11) DEFAULT NULL COMMENT '包月天数',
  `beanCurrencyCount` int(11) DEFAULT NULL COMMENT '豆币数',
  `onlineState` tinyint(1) DEFAULT '0' COMMENT '在线状态，0->不在线，1->在线',
  `loginTime` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `matcher_area` varchar(20) DEFAULT NULL COMMENT '征友地区',
  `matcher_minAge` tinyint(4) DEFAULT '0' COMMENT '征友最小年龄int',
  `matcher_maxAge` tinyint(4) DEFAULT '0' COMMENT '征友最大年龄int',
  `matcher_minHeight` smallint(6) DEFAULT '0' COMMENT '征友最低身高int',
  `matcher_maxHeight` smallint(6) DEFAULT '0' COMMENT '征友最高身高int',
  `matcher_minimumEducation` tinyint(4) DEFAULT '0' COMMENT '征友最低学历 int',
  `matcher_income` tinyint(4) DEFAULT '0' COMMENT '征友收入 int,',
  `locationInfo_addrStr` varchar(50) DEFAULT NULL COMMENT '详细地址信息',
  `locationInfo_province` varchar(50) DEFAULT NULL COMMENT '省份信息',
  `locationInfo_city` varchar(50) DEFAULT '' COMMENT '城市信息',
  `locationInfo_cityCode` tinyint(2) DEFAULT NULL COMMENT '城市代码',
  `locationInfo_district` varchar(50) DEFAULT NULL COMMENT '区县信息',
  `locationInfo_street` varchar(50) DEFAULT NULL COMMENT '街道信息',
  `locationInfo_streetNumber` varchar(50) DEFAULT NULL COMMENT '街道号码',
  `locationInfo_latitude` varchar(50) DEFAULT NULL COMMENT '纬度坐标',
  `locationInfo_longitude` varchar(50) DEFAULT NULL COMMENT '经度坐标,第一个版本只传经度纬度',
  `locationInfo_radius` varchar(50) DEFAULT NULL COMMENT '定位精度:m,',
  `user_type` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `usernameIndex` (`userName`) USING BTREE,
  KEY `ageIndex` (`age`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=100760 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_dragblacklist`
--

CREATE TABLE IF NOT EXISTS `t_user_dragblacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MyID` int(11) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后访问时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_follow`
--

CREATE TABLE IF NOT EXISTS `t_user_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MyID` int(11) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后访问时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_online`
--

CREATE TABLE IF NOT EXISTS `t_user_online` (
  `socket_id` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_photo`
--

CREATE TABLE IF NOT EXISTS `t_user_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `ismain` tinyint(2) DEFAULT NULL COMMENT '3->已设置头像,9->不允许设置头像,10->审核中,',
  `imageurl` varchar(200) DEFAULT NULL COMMENT '原图',
  `thumbnailurl` varchar(200) DEFAULT NULL COMMENT '缩略图',
  `state` tinyint(1) DEFAULT '1' COMMENT ':图片状态,0->close 1->open,',
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19128 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_report`
--

CREATE TABLE IF NOT EXISTS `t_user_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MyID` int(11) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后访问时间',
  `contentId` tinyint(2) DEFAULT '1' COMMENT '访问次数',
  `content` varchar(200) DEFAULT NULL COMMENT '补充说明',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_sayhello`
--

CREATE TABLE IF NOT EXISTS `t_user_sayhello` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MyID` int(11) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后访问时间',
  `count` tinyint(4) DEFAULT '1' COMMENT '访问次数',
  `type` tinyint(2) DEFAULT '1' COMMENT '1->缘分，\r\n2->搜索列表,\r\n3->对方空间,\r\n4->大图浏览,\r\n5->二选一pk页,\r\n6->打招呼弹窗,\r\n7->用户状态（留言）,\r\n8->用户状态（联系他）,\r\n9->附近的人,\r\n10->最近访客,\r\n11->关注列表,\r\n12->消息页促打招呼推荐列表,\r\n13->退出app推荐用户列表,\r\n14->私信页猜你喜欢打招呼,\r\n15->每日推荐url页打招呼,\r\n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=159 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_user_see`
--

CREATE TABLE IF NOT EXISTS `t_user_see` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MyID` int(11) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后访问时间',
  `count` tinyint(4) DEFAULT '1' COMMENT '访问次数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=329 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
