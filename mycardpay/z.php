<!DOCTYPE html>
<html lang="en" style="font-size:16px;">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>鑽石認證</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="public/bootstrap/css/bootstrap.css">
    <?php
    error_reporting(E_ALL || ~E_NOTICE);
    if($_GET['uid']){
        $uid = $_GET['uid'];
    }elseif($_POST['uid']){
        $uid = $_POST['uid'];
    }else{
        $uid ='';
    }

    $res6=array(
        'uid'=>$uid,
        'type'=>2,//类型1：会员2：钻石
        'money'=>526,
        'days'=>90
    );
    $custom6=base64_encode(json_encode($res6));
    ?>
</head>
<style>.content02 .box.actived{display: block;}</style>
<body>

<div class="banner2">
    <div  class="nav">
        <a href="index.php?uid=<?php echo $uid?>" >會員認證</a>
        <a href="#" class="on">鑽石認證</a>
    </div>
</div>
<?php
if($uid)
{

    ?>
    <div class="surplus">
        <input type="hidden"  id="uid" value="<?php echo $uid?>">
        <div style="float: left">
        <img style="margin-top: -25px"  src="image/dim.png">
        <span class="diam">當前賬戶鑽石數量為：</span>
        <span class="number02" id="days"></span>
        </div>
        <span class="days1" style="float: left; margin-left: 60px">支付方式：</span>
        <div class="dropdown" style="float: left; margin-left: 10px">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                MyCard支付
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li ><a href="javascript:;" onclick="check(1)">MyCard支付</a></li>
                <li><a href="javascript:;" onclick="check(2)">PayPal支付</a></li>
            </ul>
        </div>
    </div>
    <?php
}
else
{
    ?>
<div class="balance">
    <form style="margin:0 auto; float: left" action="" id="myform" name="myform" method="post">
        <div class="surplus">
            <span class="days1">请输入您的UID：</span>
            <input  style="width:200px;height:30px;line-height:30px;" type="text" name="uid"class="number" onkeyup="this.value=this.value.replace(/[^\d]/g,'') " onafterpaste="this.value=this.value.replace(/[^\d]/g,'') " id="sub" value=""/>
            <button name="submit" type="submit" class="button" id="but" style="width:100px;height:30px;line-height:30px;">提交</button>

        </div>

    </form>

    <span class="days1" style="float: left; margin-left: 60px">支付方式：</span>
    <div class="dropdown" style="float: left; margin-left: 10px">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            MyCard支付
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li ><a href="javascript:;" onclick="check(1)">MyCard支付</a></li>
            <li><a href="javascript:;" onclick="check(2)">PayPal支付</a></li>
        </ul>
    </div>
</div>
    <?php
}
?>
<!--<div class="balance">
    <span class="days1">请选择您的支付类型：</span>
    <input onchange="check(1)" class="days1" name="sex" type="radio" checked="checked"/>MyCard支付
    <input onchange="check(2)"  class="days1" name="sex" type="radio" />PayPal支付
</div>-->
<div class="tit3"></div>
<?php
if($uid!=''){
    ?>
    <div class="content02 clearfix" id="paypal"  style="display: none" >

        <a href="gopay.php?uid=<?php echo $uid?>&type=2&money=19.99&days=600" class="pa01">
            <span class="money">19.99</span>
            <span class="nts">USD</span>
            <p class="day">鑽石數量600</p>
        </a>
        <a href="gopay.php?uid=<?php echo $uid?>&type=2&money=35.99&days=1200" class="pa02">
            <span class="money">35.99</span>
            <span class="nts">USD</span>
            <p class="day">鑽石數量1200</p>
        </a>
        <a href="gopay.php?uid=<?php echo $uid?>&type=2&money=69.99&days=2400" class="pa03">
            <span class="money">69.99</span>
            <span class="nts">USD</span>
            <p class="day">鑽石數量2400</p>
        </a>
        <a href="gopay.php?uid=<?php echo $uid?>&type=2&money=159.0&days=6000" class="pa04">
            <span class="money">159.0</span>
            <span class="nts">USD</span>
            <p class="day">鑽石數量6000</p>
        </a>
        <a href="gopay.php?uid=<?php echo $uid?>&type=2&money=299.0&days=12000" class="pa05">
            <span class="money">299.0</span>
            <span class="nts">USD</span>
            <p class="day">鑽石數量12000</p>
        </a>
        <a href="gopay.php?uid=<?php echo $uid?>&type=2&money=399.0&days=18000" class="pa06">
            <span class="money">399.0</span>
            <span class="nts">USD</span>
            <p class="day">鑽石數量18000</p>
        </a>
    </div>
    <div class="content02 clearfix" id="mycard" >
        <a href="../recharge/Purchase?uid=<?php echo $uid?>&type=2&money=500&days=500" class="pa01">
            <span class="money">500</span>
            <span class="nts">TWD</span>
            <p class="day">鑽石數量500</p>
        </a>
        <a href="../recharge/Purchase?uid=<?php echo $uid?>&type=2&money=1000&days=1050" class="pa02">
            <span class="money">1000</span>
            <span class="nts">TWD</span>
            <p class="day">鑽石數量1050</p>
        </a>
        <a href="../recharge/Purchase?uid=<?php echo $uid?>&type=2&money=2000&days=2150" class="pa03">
            <span class="money">2000</span>
            <span class="nts">TWD</span>
            <p class="day">鑽石數量2150</p>
        </a>
        <a href="../recharge/Purchase?uid=<?php echo $uid?>&type=2&money=5000&days=5500" class="pa04">
            <span class="money">5000</span>
            <span class="nts">TWD</span>
            <p class="day">鑽石數量5500</p>
        </a>
        <a href="../recharge/Purchase?uid=<?php echo $uid?>&type=2&money=10000.0&days=11200" class="pa05">
            <span class="money">10000</span>
            <span class="nts">TWD</span>
            <p class="day">鑽石數量11200</p>
        </a>
        <a href="../recharge/Purchase?uid=<?php echo $uid?>&type=2&money=300.0&days=300" class="pa06">
            <span class="money">300</span>
            <span class="nts">TWD</span>
            <p class="day">鑽石數量300</p>
        </a>
    </div>

    <?php
}else{
    ?>
    <div class="">
        <p class="day" style="margin-left: 30%;margin-bottom: 10%;">请先输入正确UID，然后显示充值选项！</p>
    </div>

    <?php
}
?>
<div class="use">
    <h4>鉆石的使用：</h4>
    <div>
        <img src="image/lw.png">
        <span> 禮物交流</span>
    </div>
    <div>
        <img src="image/lw.png">
        <span> 非VIP用戶，私信資費為1鉆/條</span>
    </div>
    <div>
        <img src="image/lw.png">
        <span> 動態評論，可免費評論10條，超出计费1鉆/條</span>
    </div>
</div>
<!--<span class="number" id="custom"><?php /*echo $custom1*/?></span>-->
</body>
<script src="public/jquery.min.js"></script>
<script src="public/layer/layer.js"></script>
<script src="public/dialog.js"></script>
<script src="public/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
    window.onload=function(){
        var uid=document.getElementById('uid').value;
        var data = {'uid':uid,'type':2};
        var url = "/Api/Userauth/getviptime";
        $.post(url,data,function(result){
            if(result.status =='1') {
                var days=result.days;
                var gold=result.gold;
                var nickname=result.nickname;
                var djs=document.getElementById('days');
                djs.innerHTML=gold;
            }
            if(result.status =='2') {
                layer.alert('不存在此用户，请核对后再次输入！', {
                    skin: 'layui-layer-molv' //样式类名
                    ,closeBtn: 0
                }, function(){
                    location.href='index.php';
                });
            }
        },'JSON');
    }

    function check(a){
        var paymethod = document.getElementById( "dropdownMenu1");
        if(a=='1'){
            var Mycard = document.getElementById( "mycard");
            var PayPal = document.getElementById( "paypal");
            Mycard.style.display = "block";
            PayPal.style.display = "none";
            paymethod.innerHTML="MyCard支付<span class=\"caret\"></span>";
        }else if(a=='2'){
            var Mycard = document.getElementById( "mycard");
            var PayPal = document.getElementById( "paypal");
            Mycard.style.display = "none";
            PayPal.style.display = "block";
            paymethod.innerHTML="PayPal支付 <span class=\"caret\"></span>";
        }
    };
    /* function ceshi(){
         var custom =document.getElementById('custom').innerHTML;
         var data = {'custom':custom};
         var url = "/Api/Recharge/paypalreturn";
         $.post(url,data,function(result){
             if(result.status =='1') {

             }
         },'JSON');
         window.location.reload();
     }*/

</script>
</html>